# Stay Sharp iOS App


### Short Description

The goal of this project was to create a mobile platform for customers to book appointments at personal care services, like hairdressers, barbershops, nail salons, spas etc. The shop owner would create a virtual shop on the app and also create all the services the shop has to offer. Subsequently customers of the shop can create accounts on the mobile app and then be able to search for shops which offer the services they require and then book an appointment. The mobile application would provide the shop owner with an efficient way to manage their appointments and receive payment while the customers of the shops are offered the opportunity to book appointments at the most convenient time and make payments instantly. For the majority of the time I was the only developer, but for around 4 months the development team consisted of myself as the lead developer and a junior developer, whom I was also mentoring during his time on the project.


## Reflection

In hindsight at the time of working on this project and the point I am currently at as a developer and updates to Swift, I would have approached the project differently. The following includes some of the changes I would make if I were to work on this project again.


1. Architectural Design Pattern: The iOS app was created using MVC which led to problems with overweight controllers. As can be seen from the code a lot of data formatting had to be dumped on the controllers which proved a tad bit difficult to test. I would now definitely go for MVVM, where we have great separation of concerns which allow for easy testability and maintainability.


2. Functional Reactive Programming: This is a combination of both the functional and reactive programming paradigm, hence we have a programming paradigm for reactive programming (asynchronous dataflow programming) using the building blocks of functional programming (e.g. map, reduce, filter). If I had had the chance to implement FRP, instead of Imperative programming, the benefits to the iOS app would include avoiding callback hell, its standard mechanism for error recovery, handling UI interaction/events smoothly without hassle, super easy way to talk to the UI thread and more.


3. Testing: This would involve writing more tests that would include uniting testing, UI testing, performance testing and integrated testing in order to protect the code base against regressions.