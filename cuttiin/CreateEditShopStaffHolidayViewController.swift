//
//  CreateEditShopStaffHolidayViewController.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 11/05/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit
import SwiftSpinner
import SwiftDate
import Alamofire

class CreateEditShopStaffHolidayViewController: UIViewController, UITextViewDelegate {
    
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    var selectedShopID: String?
    var initiatorAction: String?
    var fromDateString: String?
    var fromDateObject: DateInRegion?
    var toDateString: String?
    var toDateObject: DateInRegion?
    var shopTimeZone: String?
    var shopCalendar: String?
    var shopLocale: String?
    var staffFullNameString: String?
    var staffCustomerUniqueID: String?
    var staffUniqueID: String?
    var holidayIsActiveStatus: String?
    var selectedHolidayId: String?
    var holidayRepeatStatus: String?
    var dayOfTheWeek: String?
    
    //edit optionals
    var editDesignation: String?
    
    
    lazy var shopStaffHolidayCloseViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(exitThisView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    lazy var shopStaffHolidayViewIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8holidayshopstaff")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let shopStaffHolidayTitlePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 17)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var saveAndUpdateDoneButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        st.isEnabled = true
        st.isUserInteractionEnabled = true
        return st
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    let deleteButtonTitleLabel: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    lazy var ShopStaffHolidayViewDeleteButton: UISwitch = {
        let st = UISwitch()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.isOn = false
        st.addTarget(self, action: #selector(handleDeletingHoliday(_:)), for: .touchUpInside)
        return st
    }()
    
    // here
    
    let repeatButtonTitleLabel: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    lazy var ShopStaffHolidayViewRepeatButton: UISwitch = {
        let st = UISwitch()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.isOn = false
        st.addTarget(self, action: #selector(handleRepeatHoliday(_:)), for: .touchUpInside)
        return st
    }()
    
    // here
    lazy var holidayTitleTextFieldContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        tcview.layer.masksToBounds = true
        tcview.layer.cornerRadius = 5
        return tcview
    }()
    
    let holidayTitleTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.placeholder = NSLocalizedString("createEditShopStaffHolidayTitle", comment: "Title")
        em.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return em
    }()
    
    lazy var shopAndStaffHolidaySegmentedControl: UISegmentedControl = {
        let ocsegmentcontrol = UISegmentedControl(items: [NSLocalizedString("createEditShopStaffHolidayShopSwitcher", comment: "Shop"), NSLocalizedString("createEditShopStaffHolidayStaffSwitcher", comment: "Staff")])
        ocsegmentcontrol.translatesAutoresizingMaskIntoConstraints = false
        ocsegmentcontrol.tintColor = UIColor(r: 220, g: 220, b: 220)
        ocsegmentcontrol.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: UIControl.State.normal)
        ocsegmentcontrol.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: UIControl.State.selected)
        ocsegmentcontrol.selectedSegmentIndex = 0
        ocsegmentcontrol.addTarget(self, action: #selector(handleSegementedControlSelection(_:)), for: .valueChanged)
        return ocsegmentcontrol
    }()
    
    
    
    let dateSelectButtonsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.isUserInteractionEnabled = true
        return tcview
    }()
    
    lazy var fromDateButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.setTitle(NSLocalizedString("createEditShopStaffHolidayFromButton", comment: "From"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 13)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.tag = 1
        st.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        st.addTarget(self, action: #selector(handleSelectingFromDate(_:)), for: .touchUpInside)
        st.isEnabled = true
        return st
    }()
    
    lazy var toDateButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.setTitle(NSLocalizedString("createEditShopStaffHolidayToButton", comment: "To"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 13)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.tag = 2
        st.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        st.addTarget(self, action: #selector(handleSelectToDate(_:)), for: .touchUpInside)
        st.isEnabled = true
        return st
    }()
    
    let dateFilterButtonsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.isUserInteractionEnabled = true
        tcview.isHidden = true
        return tcview
    }()
    
    lazy var selectStaffButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.setTitle(NSLocalizedString("createEditShopStaffHolidayStaffShowButton", comment: "Staff"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 14)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        st.addTarget(self, action: #selector(handleStaffSelector(_:)), for: .touchUpInside)
        st.isEnabled = true
        st.isHidden = true
        return st
    }()
    
    let holidayShortDescriptionTextField: UITextView = {
        let em = UITextView()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.lightGray
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.text = NSLocalizedString("createEditShopStaffHolidayShortDescriptionEditText", comment: "Short Description")
        em.textAlignment = .justified
        em.isUserInteractionEnabled = true
        em.isEditable = true
        em.isSelectable = true
        em.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        em.layer.masksToBounds = true
        em.layer.cornerRadius = 5
        return em
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        view.addSubview(shopStaffHolidayCloseViewButton)
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        view.addSubview(deleteButtonTitleLabel)
        view.addSubview(repeatButtonTitleLabel)
        view.addSubview(ShopStaffHolidayViewDeleteButton)
        view.addSubview(ShopStaffHolidayViewRepeatButton)
        view.addSubview(holidayTitleTextFieldContainerView)
        view.addSubview(shopAndStaffHolidaySegmentedControl)
        view.addSubview(dateSelectButtonsContainerView)
        view.addSubview(dateFilterButtonsContainerView)
        view.addSubview(holidayShortDescriptionTextField)
        holidayShortDescriptionTextField.delegate = self
        
        if let status = self.initiatorAction{
            switch status {
            case "editHoliday":
                let heighAchorForDeleteButton: CGFloat = 30
                self.shopStaffHolidayTitlePlaceHolder.text = NSLocalizedString("createEditShopStaffHolidayEditHolidayTitle", comment: "Edit Holiday")
                self.saveAndUpdateDoneButton.setTitle(NSLocalizedString("createEditShopStaffHolidaySaveButton", comment: "Save"), for: .normal)
                self.saveAndUpdateDoneButton.addTarget(self, action: #selector(updateHoliday(_:)), for: .touchUpInside)
                setupCreateAndEditContraints(hideHeightDelete: heighAchorForDeleteButton)
                self.repeatButtonTitleLabel.text = NSLocalizedString("createEditShopStaffHolidayDoNotRepeatSwitcher", comment: "do not repeat")
                self.holidayRepeatStatus = "NO"
                break
            case "addHoliday":
                let heighAchorForDeleteButton: CGFloat = 0
                self.shopStaffHolidayTitlePlaceHolder.text = NSLocalizedString("createEditShopStaffHolidayAddHolidayTitle", comment: "Add Holiday")
                self.saveAndUpdateDoneButton.setTitle("Add", for: .normal)
                self.saveAndUpdateDoneButton.addTarget(self, action: #selector(addHoliday(_:)), for: .touchUpInside)
                self.ShopStaffHolidayViewDeleteButton.isHidden = true
                self.deleteButtonTitleLabel.isHidden = true
                setupCreateAndEditContraints(hideHeightDelete: heighAchorForDeleteButton)
                self.repeatButtonTitleLabel.text = NSLocalizedString("createEditShopStaffHolidayDoNotRepeatSwitcher", comment: "do not repeat")
                self.holidayRepeatStatus = "NO"
                break
            default:
                break
            }
        }
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .didReceiveDataShopStaffHolidayEditAdd, object: nil)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if holidayShortDescriptionTextField.textColor == UIColor.lightGray {
            holidayShortDescriptionTextField.text = ""
            holidayShortDescriptionTextField.textColor = UIColor.black
        }
    }
    
    @objc private func onDidReceiveData(_ notification:Notification) {
        
        if let dataBack = notification.userInfo as? [String: AnyObject], let staffId = dataBack["staffCustomerUniqueId"] as? String, let staffName = dataBack["fullName"] as? String, let staffIdentifier = dataBack["staffUniqueID"] as? String {
            self.staffFullNameString = staffName
            self.staffCustomerUniqueID = staffId
            self.staffUniqueID = staffIdentifier
            self.selectStaffButton.setTitle(staffName, for: .normal)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func exitThisView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @objc private func handleSegementedControlSelection(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.dateFilterButtonsContainerView.isHidden = true
            self.selectStaffButton.isHidden = true
        }else {
            self.dateFilterButtonsContainerView.isHidden = false
            self.selectStaffButton.isHidden = false
            
        }
    }
    @objc private func handleSelectingFromDate(_ sender: UIButton){
        sender.pulsate()
        let datePickerView = DatePickerViewController()
        datePickerView.shopStaffHolidayViewController = self
        datePickerView.tagButton = sender.tag
        datePickerView.viewWhoInitiatedAction = "shopStaffView"
        present(datePickerView, animated: false, completion: nil)
    }
    
    @objc private func handleSelectToDate(_ sender: UIButton){
        sender.pulsate()
        let datePickerView = DatePickerViewController()
        datePickerView.shopStaffHolidayViewController = self
        datePickerView.tagButton = sender.tag
        datePickerView.viewWhoInitiatedAction = "shopStaffView"
        present(datePickerView, animated: false, completion: nil)
    }
    
    @objc func dateChanged(dateSelected: Date, buttonTag: Int) {
        if let timeZone = shopTimeZone, let calendar = self.shopCalendar, let locale = self.shopLocale {
            let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: calendar, LocName: locale)
            let currentDate = dateSelected.convertTo(region: region)
            let currentDateString = dateSelected.convertTo(region: region).toFormat("yyyy-MM-dd HH:mm:ss")
            switch(buttonTag){
            case 1:
                self.fromDateButton.setTitle(currentDate.toString(DateToStringStyles.dateTime(.short)), for: .normal)
                self.fromDateObject = currentDate
                self.fromDateString = currentDateString
                
                let englishRegion = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en_US")
                let dateSelectedEnglish = currentDate.convertTo(region: englishRegion)
                self.dayOfTheWeek = dateSelectedEnglish.weekdayName(.default)
                
                if let dateCompare = self.toDateObject, dateCompare.isBeforeDate(currentDate, orEqual: true, granularity: .second) {
                    self.saveAndUpdateDoneButton.isEnabled = false
                    self.view.makeToast(NSLocalizedString("createEditShopStaffHolidayErrorMessageInvalidDate", comment: "The date chosen is invalid"), duration: 2.0, position: .bottom)
                } else {
                    self.saveAndUpdateDoneButton.isEnabled = true
                }
                
                break
            case 2:
                self.toDateButton.setTitle(currentDate.toString(DateToStringStyles.dateTime(.short)), for: .normal)
                self.toDateObject = currentDate
                self.toDateString = currentDateString
                if let dateCompare = self.fromDateObject, dateCompare.isAfterDate(currentDate, orEqual: true, granularity: .second) {
                    self.saveAndUpdateDoneButton.isEnabled = false
                    self.view.makeToast(NSLocalizedString("createEditShopStaffHolidayErrorMessageInvalidDate", comment: "The date chosen is invalid"), duration: 2.0, position: .bottom)
                } else {
                    self.saveAndUpdateDoneButton.isEnabled = true
                }
                break
            default:
                print("invalid entry")
                break
            }
        }
        
    }
    
    @objc private func handleStaffSelector(_ sender: UIButton) {
        if let id = self.selectedShopID {
            let customerShops = ShopStaffViewController()
            customerShops.createEditShopStaffHolidayViewController = self
            customerShops.viewControllerWhoInitiatedAction = "shopStaffHoliday"
            customerShops.shopUniqueID = id
            customerShops.modalPresentationStyle = .overCurrentContext
            present(customerShops, animated: true, completion: nil)
        }
    }
    
    @objc private func addHoliday(_ sender: UIButton){
        
        if self.shopAndStaffHolidaySegmentedControl.selectedSegmentIndex == 0 {
            self.createShopHoliday()
        }else {
            self.createStaffHoliday()
        }
    }
    
    private func getHolidayDataShop(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let holidayID = self.selectedHolidayId  {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    
                    DispatchQueue.global(qos: .background).async {
                        AF.request(self.BACKEND_URL + "getSingleShopHoliday/" + holidayID, method: .post, headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"])
                            .response { (response) in
                                
                                if let jsonData = response.data {
                                    do {
                                        
                                        print(jsonData)
                                        let serverResponse = try JSONDecoder().decode(HolidayShop.self, from: jsonData)
                                        let regionDataShop = DateByUserDeviceInitializer.getRegion(TZoneName: serverResponse.holidayShopDateTimezone, calenName: serverResponse.holidayShopDateCalendar, LocName: serverResponse.holidayShopDateLocale)
                                        if let startDateShop = serverResponse.holidayShopStartDate.toDate("yyyy-MM-dd HH:mm:ss", region: regionDataShop), let endDateShop = serverResponse.holidayShopEndDate.toDate("yyyy-MM-dd HH:mm:ss", region: regionDataShop){
                                            self.fromDateButton.setTitle(startDateShop.toString(DateToStringStyles.dateTime(.short)), for: .normal)
                                            self.toDateButton.setTitle(endDateShop.toString(DateToStringStyles.dateTime(.short)), for: .normal)
                                            self.fromDateObject = startDateShop
                                            self.fromDateString = serverResponse.holidayShopStartDate
                                            
                                            let englishRegion = DateByUserDeviceInitializer.getRegion(TZoneName: serverResponse.holidayShopDateTimezone, calenName: "gregorian", LocName: "en_US")
                                            let dateSelectedEnglish = startDateShop.convertTo(region: englishRegion)
                                            self.dayOfTheWeek = dateSelectedEnglish.weekdayName(.default)
                                            
                                            self.toDateObject = endDateShop
                                            self.toDateString = serverResponse.holidayShopEndDate
                                            self.holidayShortDescriptionTextField.text = serverResponse.holidayShopDescription
                                            self.holidayTitleTextField.text = serverResponse.holidayShopTitle
                                            
                                            if serverResponse.holidayShopIsActive == "YES" {
                                                self.ShopStaffHolidayViewDeleteButton.isOn = true
                                                self.deleteButtonTitleLabel.text = NSLocalizedString("createEditShopStaffHolidayActiveButtonSwitcher", comment: "active")
                                            }
                                            
                                            if serverResponse.holidayShopRepeatStatus == "YES" {
                                                self.ShopStaffHolidayViewRepeatButton.isOn = true
                                                self.repeatButtonTitleLabel.text = NSLocalizedString("createEditShopStaffHolidayRepeatSwitcher", comment: "repeat")
                                                self.holidayRepeatStatus = "YES"
                                            }
                                            
                                        }
                                        
                                    } catch _ {
                                        DispatchQueue.main.async {
                                            if let statusCode = response.response?.statusCode {
                                                switch statusCode {
                                                case 404:
                                                    self.view.makeToast(NSLocalizedString("createEditShopStaffHolidayErrorMessageShopHolidayNotFound", comment: "shop holiday not found"), duration: 2.0, position: .bottom)
                                                case 500:
                                                    self.view.makeToast(NSLocalizedString("createEditShopStaffHolidayErrorMessageAnErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                                default:
                                                    print("sky high")
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                    }
                    
                    
                    
                }
            }
        }
    }
    
    private func getHolidayDataStaff(){
        print("right one")
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let holidayID = self.selectedHolidayId  {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    
                    DispatchQueue.global(qos: .background).async {
                        AF.request(self.BACKEND_URL + "getSingleStaffHoliday/" + holidayID, method: .post, headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"])
                            .response { (response) in
                                
                                if let jsonData = response.data {
                                    do {
                                        
                                        print(jsonData)
                                        let serverResponse = try JSONDecoder().decode(HolidayStaffRequested.self, from: jsonData)
                                        
                                        let staffHolidayDataHold = serverResponse.holidayStaff
                                        let staffCustometDataHold = serverResponse.customer
                                        let staffArrayDataHold = serverResponse.staff
                                        
                                        let regionDataShop = DateByUserDeviceInitializer.getRegion(TZoneName: staffHolidayDataHold.holidayStaffDateTimezone , calenName: staffHolidayDataHold.holidayStaffDateCalendar, LocName: staffHolidayDataHold.holidayStaffDateLocale)
                                        
                                        if let startDateShop = staffHolidayDataHold.holidayStaffStartDate.toDate("yyyy-MM-dd HH:mm:ss", region: regionDataShop), let endDateShop = staffHolidayDataHold.holidayStaffEndDate.toDate("yyyy-MM-dd HH:mm:ss", region: regionDataShop){
                                            
                                            self.fromDateButton.setTitle(startDateShop.toString(DateToStringStyles.dateTime(.short)), for: .normal)
                                            self.toDateButton.setTitle(endDateShop.toString(DateToStringStyles.dateTime(.short)), for: .normal)
                                            self.fromDateObject = startDateShop
                                            self.fromDateString = staffHolidayDataHold.holidayStaffStartDate
                                            
                                            let englishRegion = DateByUserDeviceInitializer.getRegion(TZoneName: staffHolidayDataHold.holidayStaffDateTimezone, calenName: "gregorian", LocName: "en_US")
                                            let dateSelectedEnglish = startDateShop.convertTo(region: englishRegion)
                                            self.dayOfTheWeek = dateSelectedEnglish.weekdayName(.default)
                                            
                                            self.toDateObject = endDateShop
                                            self.toDateString = staffHolidayDataHold.holidayStaffEndDate
                                            self.holidayShortDescriptionTextField.text = staffHolidayDataHold.holidayStaffDescription
                                            
                                            
                                            self.staffFullNameString = staffCustometDataHold.firstName
                                            self.staffCustomerUniqueID = staffArrayDataHold.customer
                                            self.staffUniqueID = staffArrayDataHold.id
                                            self.selectStaffButton.setTitle(staffCustometDataHold.firstName, for: .normal)
                                            
                                            self.holidayTitleTextField.text = staffHolidayDataHold.holidayStaffTitle
                                            
                                            if staffHolidayDataHold.holidayStaffIsActive == "YES" {
                                                self.ShopStaffHolidayViewDeleteButton.isOn = true
                                                self.deleteButtonTitleLabel.text = NSLocalizedString("createEditShopStaffHolidayActiveButtonSwitcher", comment: "active")
                                            }
                                            
                                            if staffHolidayDataHold.holidayStaffRepeatStatus == "YES" {
                                                self.ShopStaffHolidayViewRepeatButton.isOn = true
                                                self.repeatButtonTitleLabel.text = NSLocalizedString("createEditShopStaffHolidayRepeatSwitcher", comment: "repeat")
                                                self.holidayRepeatStatus = "YES"
                                            }
                                        }
                                        
                                    } catch _ {
                                        DispatchQueue.main.async {
                                            if let statusCode = response.response?.statusCode {
                                                switch statusCode {
                                                case 404:
                                                    self.view.makeToast(NSLocalizedString("createEditShopStaffHolidayErrorMessageStaffHolidayNotFound", comment: "staff holiday not found"), duration: 2.0, position: .bottom)
                                                case 500:
                                                    self.view.makeToast(NSLocalizedString("createEditShopStaffHolidayErrorMessageAnErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                                default:
                                                    print("sky high")
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                    }
                    
                    
                    
                }
            }
        }
    }
    
    private func createShopHoliday(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = shopTimeZone, let calendar = self.shopCalendar, let locale = self.shopLocale, let token = decodedCustomer.first?.token, let fromDate = self.fromDateString, let toDate = self.toDateString, let shopID = self.selectedShopID, var description = self.holidayShortDescriptionTextField.text, let titleText = self.holidayTitleTextField.text, let repeatStatus = self.holidayRepeatStatus, let dayWeek = self.dayOfTheWeek {
                
                if description == "Short Description"{
                    description = "not available"
                }
                
                
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: calendar, LocName: locale)
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    
                    let newDate = DateInRegion().toFormat("yyyy-MM-dd HH:mm:ss")
                    
                    let holidayModel: Parameters = [
                        
                        "holidayShopDescription": description,
                        "shop": shopID,
                        "holidayShopStartDate": fromDate,
                        "holidayShopEndDate": toDate,
                        "holidayShopDateCreated": newDate,
                        "holidayShopDateTimezone": timeZone,
                        "holidayShopDateCalendar": calendar,
                        "holidayShopDateLocale": locale,
                        "holidayShopTitle": titleText,
                        "holidayShopRepeatStatus": repeatStatus,
                        "holidayShopDayOfTheWeek": dayWeek,
                        "holidayShopIsActive": "YES"
                    ]
                    
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    SwiftSpinner.show("Loading...")
                    
                    DispatchQueue.global(qos: .background).async {
                        AF.request(self.BACKEND_URL + "createShopHoliday", method: .post, parameters: holidayModel, encoding: JSONEncoding(options: []), headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"])
                            .response { (responseData) in
                                
                                
                                if let _ = responseData.error {
                                    let message = "Error: please try again later"
                                    self.view.makeToast(message, duration: 3.0, position: .bottom)
                                    return
                                }
                                
                                if let jsonData = responseData.data {
                                    do {
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        let _ = try JSONDecoder().decode(HolidayShopCreated.self, from: jsonData)
                                        self.dismiss(animated: true, completion: {
                                            let someDict = ["shopUniqueId" : shopID]
                                            NotificationCenter.default.post(name: .didReceiveDataShopStaffHoliday, object: nil, userInfo: someDict)
                                        })
                                        
                                    } catch _ {
                                        
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        
                                        if let statusCode = responseData.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                self.view.makeToast(NSLocalizedString("createEditShopStaffHolidayErrorMessageShopHolidayNotCreated", comment: "Shop holiday could not be created"), duration: 2.0, position: .bottom)
                                                
                                            case 500:
                                                self.view.makeToast(NSLocalizedString("createEditShopStaffHolidayErrorMessageAnErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                            default:
                                                print("sky high")
                                            }
                                        }
                                    }
                                }
                        }
                    }
                }
            }
        }
    }
    
    private func createStaffHoliday(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = shopTimeZone, let calendar = self.shopCalendar, let locale = self.shopLocale, let token = decodedCustomer.first?.token, let fromDate = self.fromDateString, let toDate = self.toDateString, let shopID = self.selectedShopID, let staffId = self.staffUniqueID, let staffCustomerID = self.staffCustomerUniqueID, var description = self.holidayShortDescriptionTextField.text, let titleText = self.holidayTitleTextField.text, let repeatStatus = self.holidayRepeatStatus, let dayWeek = self.dayOfTheWeek {
                
                if description == "Short Description"{
                    description = "not available"
                }
                
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: calendar, LocName: locale)
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    
                    let newDate = DateInRegion().toFormat("yyyy-MM-dd HH:mm:ss")
                    
                    let holidayModel: Parameters = [
                        
                        "holidayStaffDescription": description,
                        "staff": staffId,
                        "customer": staffCustomerID,
                        "shop": shopID,
                        "holidayStaffStartDate": fromDate,
                        "holidayStaffEndDate": toDate,
                        "holidayStaffDateCreated": newDate,
                        "holidayStaffDateTimezone": timeZone,
                        "holidayStaffDateCalendar": calendar,
                        "holidayStaffDateLocale": locale,
                        "holidayStaffTitle": titleText,
                        "holidayStaffRepeatStatus": repeatStatus,
                        "holidayStaffDayOfTheWeek": dayWeek,
                        "holidayStaffIsActive": "YES"
                    ]
                    
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    SwiftSpinner.show("Loading...")
                    
                    DispatchQueue.global(qos: .background).async {
                        AF.request(self.BACKEND_URL + "createStaffHoliday", method: .post, parameters: holidayModel, encoding: JSONEncoding(options: []), headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"])
                            .response { (responseData) in
                                
                                
                                if let _ = responseData.error {
                                    let message = "Error: please try again later"
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                        self.view.makeToast(message, duration: 3.0, position: .bottom)
                                    }
                                    return
                                }
                                
                                if let jsonData = responseData.data {
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    do {
                                        let _ = try JSONDecoder().decode(HolidayStaffCreated.self, from: jsonData)
                                        self.dismiss(animated: true, completion: {
                                            let someDict = ["shopUniqueId" : shopID]
                                            NotificationCenter.default.post(name: .didReceiveDataShopStaffHoliday, object: nil, userInfo: someDict)
                                        })
                                        
                                    } catch _ {
                                        
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        
                                        if let statusCode = responseData.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                self.view.makeToast(NSLocalizedString("createEditShopStaffHolidayErrorMessageStaffHolidayNotCreated", comment: "Staff holiday could not be created"), duration: 2.0, position: .bottom)
                                                
                                            case 500:
                                                self.view.makeToast(NSLocalizedString("createEditShopStaffHolidayErrorMessageAnErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                            default:
                                                print("sky high")
                                            }
                                        }
                                    }
                                } else {
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                }
                        }
                    }
                }
            }
        }
    }
    
    @objc private func updateHoliday(_ sender: UIButton){
        if let stat = self.editDesignation, stat == "staff" {
            self.updateStaffHoliday()
            
        }
        
        if let stat = self.editDesignation, stat == "shop" {
            self.updateShopHoldiay()
            
        }
    }
    
    private func updateStaffHoliday(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = shopTimeZone, let calendar = self.shopCalendar, let locale = self.shopLocale, let token = decodedCustomer.first?.token, let fromDate = self.fromDateString, let toDate = self.toDateString, let shopID = self.selectedShopID, let staffId = self.staffUniqueID, let staffCustomerID = self.staffCustomerUniqueID, let holidayID = self.selectedHolidayId, let status = self.holidayIsActiveStatus, var description = self.holidayShortDescriptionTextField.text, let titleText = self.holidayTitleTextField.text, let repeatStatus = self.holidayRepeatStatus, let dayWeek = self.dayOfTheWeek {
                
                if description == "Short Description"{
                    description = "not available"
                }
                
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: calendar, LocName: locale)
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    
                    let holidayModel: Parameters = [
                        
                        "holidayStaffDescription": description,
                        "staff": staffId,
                        "customer": staffCustomerID,
                        "shop": shopID,
                        "holidayStaffStartDate": fromDate,
                        "holidayStaffEndDate": toDate,
                        "holidayStaffDateTimezone": timeZone,
                        "holidayStaffDateCalendar": calendar,
                        "holidayStaffDateLocale": locale,
                        "holidayStaffTitle": titleText,
                        "holidayStaffRepeatStatus": repeatStatus,
                        "holidayStaffDayOfTheWeek": dayWeek,
                        "holidayStaffIsActive": status
                    ]
                    
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    SwiftSpinner.show("Loading...")
                    
                    DispatchQueue.global(qos: .background).async {
                        AF.request(self.BACKEND_URL + "updateStaffHoliday/" + holidayID, method: .put, parameters: holidayModel, encoding: JSONEncoding(options: []), headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"])
                            .response { (responseData) in
                                
                                
                                if let _ = responseData.error {
                                    let message = "Error: please try again later"
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                        self.view.makeToast(message, duration: 3.0, position: .bottom)
                                    }
                                    return
                                }
                                
                                if let jsonData = responseData.data {
                                    do {
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        
                                        if let statusCode = responseData.response?.statusCode {
                                            switch statusCode {
                                            case 200:
                                                let _ = try JSONDecoder().decode(ServerMessage.self, from: jsonData)
                                                self.dismiss(animated: true, completion: {
                                                    let someDict = ["shopUniqueId" : shopID]
                                                    NotificationCenter.default.post(name: .didReceiveDataShopStaffHoliday, object: nil, userInfo: someDict)
                                                })
                                                break
                                            case 404:
                                                self.view.makeToast(NSLocalizedString("createEditShopStaffHolidayErrorMessageStaffHolidayNotUpdated", comment: "staff holiday could not be updated"), duration: 2.0, position: .bottom)
                                                break
                                            case 500:
                                                self.view.makeToast(NSLocalizedString("createEditShopStaffHolidayErrorMessageAnErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                                break
                                            default:
                                                print("sky high")
                                                break
                                            }
                                        }
                                        
                                    } catch _ {
                                        
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        
                                        if let statusCode = responseData.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                self.view.makeToast("staff holiday could not be updated", duration: 2.0, position: .bottom)
                                                
                                            case 500:
                                                self.view.makeToast(NSLocalizedString("createEditShopStaffHolidayErrorMessageAnErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                            default:
                                                print("sky high")
                                            }
                                        }
                                    }
                                } else {
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                }
                        }
                    }
                }
            }
        }
    }
    
    private func updateShopHoldiay(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = shopTimeZone, let calendar = self.shopCalendar, let locale = self.shopLocale, let token = decodedCustomer.first?.token, let fromDate = self.fromDateString, let toDate = self.toDateString, let shopID = self.selectedShopID, let holidayID = self.selectedHolidayId, let status = self.holidayIsActiveStatus, var description = self.holidayShortDescriptionTextField.text, let titleText = self.holidayTitleTextField.text, let repeatStatus = self.holidayRepeatStatus, let dayWeek = self.dayOfTheWeek {
                
                if description == "Short Description"{
                    description = "not available"
                }
                
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: calendar, LocName: locale)
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    
                    let holidayModel: Parameters = [
                        
                        "holidayShopDescription": description,
                        "shop": shopID,
                        "holidayShopStartDate": fromDate,
                        "holidayShopEndDate": toDate,
                        "holidayShopDateTimezone": timeZone,
                        "holidayShopDateCalendar": calendar,
                        "holidayShopDateLocale": locale,
                        "holidayShopTitle": titleText,
                        "holidayShopRepeatStatus": repeatStatus,
                        "holidayShopDayOfTheWeek": dayWeek,
                        "holidayShopIsActive": status
                    ]
                    
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    SwiftSpinner.show("Loading...")
                    
                    DispatchQueue.global(qos: .background).async {
                        AF.request(self.BACKEND_URL + "updateShopHoliday/" + holidayID, method: .put, parameters: holidayModel, encoding: JSONEncoding(options: []), headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"])
                            .response { (responseData) in
                                
                                
                                if let _ = responseData.error {
                                    let message = "Error: please try again later"
                                    self.view.makeToast(message, duration: 3.0, position: .bottom)
                                    return
                                }
                                
                                if let jsonData = responseData.data {
                                    do {
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        if let statusCode = responseData.response?.statusCode {
                                            switch statusCode {
                                            case 200:
                                                let _ = try JSONDecoder().decode(ServerMessage.self, from: jsonData)
                                                self.dismiss(animated: true, completion: {
                                                    let someDict = ["shopUniqueId" : shopID]
                                                    NotificationCenter.default.post(name: .didReceiveDataShopStaffHoliday, object: nil, userInfo: someDict)
                                                })
                                                break
                                            case 404:
                                                self.view.makeToast(NSLocalizedString("createEditShopStaffHolidayErrorMessageShopHolidayNotUpdated", comment: "Shop hoilday could not be updated"), duration: 2.0, position: .bottom)
                                                break
                                            case 500:
                                                self.view.makeToast(NSLocalizedString("createEditShopStaffHolidayErrorMessageAnErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                            default:
                                                print("sky high")
                                                break
                                            }
                                        }
                                        
                                    } catch _ {
                                        
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        
                                        if let statusCode = responseData.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                self.view.makeToast(NSLocalizedString("createEditShopStaffHolidayErrorMessageShopHolidayNotUpdated", comment: "Shop hoilday could not be updated"), duration: 2.0, position: .bottom)
                                                
                                            case 500:
                                                self.view.makeToast(NSLocalizedString("createEditShopStaffHolidayErrorMessageAnErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                            default:
                                                print("sky high")
                                            }
                                        }
                                    }
                                }
                        }
                    }
                }
            }
        }
    }
    
    @objc private func handleDeletingHoliday(_ sender: UISwitch) {
        switch sender.isOn {
        case true:
            self.deleteButtonTitleLabel.text = NSLocalizedString("createEditShopStaffHolidayActiveButtonSwitcher", comment: "active")
            self.holidayIsActiveStatus = "YES"
            break
        case false:
            self.deleteButtonTitleLabel.text = NSLocalizedString("createEditShopStaffHolidayDeleteButtonSwitcher", comment: "delete")
            self.holidayIsActiveStatus = "NO"
            break
        }
    }
    
    @objc private func handleRepeatHoliday(_ sender: UISwitch) {
        switch sender.isOn {
        case true:
            self.repeatButtonTitleLabel.text = NSLocalizedString("createEditShopStaffHolidayRepeatSwitcher", comment: "repeat")
            self.holidayRepeatStatus = "YES"
            break
        case false:
            self.repeatButtonTitleLabel.text = NSLocalizedString("createEditShopStaffHolidayDoNotRepeatSwitcher", comment: "do not repeat")
            self.holidayRepeatStatus = "NO"
            break
        }
    }
    
    @objc private func setupCreateAndEditContraints(hideHeightDelete: CGFloat){
        var topDistance: CGFloat = 20
        if UIDevice.current.hasNotch {
            topDistance = 60
        }
        
        shopStaffHolidayCloseViewButton.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        shopStaffHolidayCloseViewButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -15).isActive = true
        shopStaffHolidayCloseViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        shopStaffHolidayCloseViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperInputsContainerView.topAnchor.constraint(equalTo: shopStaffHolidayCloseViewButton.bottomAnchor).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(shopStaffHolidayViewIcon)
        upperInputsContainerView.addSubview(shopStaffHolidayTitlePlaceHolder)
        upperInputsContainerView.addSubview(saveAndUpdateDoneButton)
        
        shopStaffHolidayViewIcon.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        shopStaffHolidayViewIcon.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor).isActive = true
        shopStaffHolidayViewIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        shopStaffHolidayViewIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        saveAndUpdateDoneButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        saveAndUpdateDoneButton.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        saveAndUpdateDoneButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        saveAndUpdateDoneButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        shopStaffHolidayTitlePlaceHolder.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        shopStaffHolidayTitlePlaceHolder.leftAnchor.constraint(equalTo: shopStaffHolidayViewIcon.rightAnchor, constant: 10).isActive = true
        shopStaffHolidayTitlePlaceHolder.rightAnchor.constraint(equalTo: saveAndUpdateDoneButton.leftAnchor).isActive = true
        shopStaffHolidayTitlePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        ShopStaffHolidayViewDeleteButton.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 5).isActive = true
        ShopStaffHolidayViewDeleteButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -12).isActive = true
        ShopStaffHolidayViewDeleteButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        ShopStaffHolidayViewDeleteButton.heightAnchor.constraint(equalToConstant: hideHeightDelete).isActive = true
        
        deleteButtonTitleLabel.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 5).isActive = true
        deleteButtonTitleLabel.rightAnchor.constraint(equalTo: ShopStaffHolidayViewDeleteButton.leftAnchor, constant: -10).isActive = true
        deleteButtonTitleLabel.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.25).isActive = true
        deleteButtonTitleLabel.heightAnchor.constraint(equalToConstant: hideHeightDelete).isActive = true
        
        // here
        ShopStaffHolidayViewRepeatButton.topAnchor.constraint(equalTo: ShopStaffHolidayViewDeleteButton.bottomAnchor, constant: 5).isActive = true
        ShopStaffHolidayViewRepeatButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -12).isActive = true
        ShopStaffHolidayViewRepeatButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        ShopStaffHolidayViewRepeatButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        repeatButtonTitleLabel.topAnchor.constraint(equalTo: deleteButtonTitleLabel.bottomAnchor, constant: 5).isActive = true
        repeatButtonTitleLabel.rightAnchor.constraint(equalTo: ShopStaffHolidayViewDeleteButton.leftAnchor, constant: -10).isActive = true
        repeatButtonTitleLabel.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.25).isActive = true
        repeatButtonTitleLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        // here
        
        holidayTitleTextFieldContainerView.topAnchor.constraint(equalTo: repeatButtonTitleLabel.bottomAnchor, constant: 5).isActive = true
        holidayTitleTextFieldContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        holidayTitleTextFieldContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        holidayTitleTextFieldContainerView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        holidayTitleTextFieldContainerView.addSubview(holidayTitleTextField)
        
        holidayTitleTextField.topAnchor.constraint(equalTo: holidayTitleTextFieldContainerView.topAnchor).isActive = true
        holidayTitleTextField.centerXAnchor.constraint(equalTo: holidayTitleTextFieldContainerView.centerXAnchor).isActive = true
        holidayTitleTextField.widthAnchor.constraint(equalTo: holidayTitleTextFieldContainerView.widthAnchor, constant: -5).isActive = true
        holidayTitleTextField.heightAnchor.constraint(equalTo: holidayTitleTextFieldContainerView.heightAnchor).isActive = true
        
        shopAndStaffHolidaySegmentedControl.topAnchor.constraint(equalTo: holidayTitleTextFieldContainerView.bottomAnchor, constant: 5).isActive = true
        shopAndStaffHolidaySegmentedControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        shopAndStaffHolidaySegmentedControl.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        shopAndStaffHolidaySegmentedControl.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        dateSelectButtonsContainerView.topAnchor.constraint(equalTo: shopAndStaffHolidaySegmentedControl.bottomAnchor, constant: 10).isActive = true
        dateSelectButtonsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        dateSelectButtonsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        dateSelectButtonsContainerView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        dateSelectButtonsContainerView.addSubview(fromDateButton)
        dateSelectButtonsContainerView.addSubview(toDateButton)
        dateSelectButtonsContainerView.addSubview(selectStaffButton)
        
        fromDateButton.topAnchor.constraint(equalTo: dateSelectButtonsContainerView.topAnchor).isActive = true
        fromDateButton.leftAnchor.constraint(equalTo: dateSelectButtonsContainerView.leftAnchor).isActive = true
        fromDateButton.rightAnchor.constraint(equalTo: dateSelectButtonsContainerView.centerXAnchor).isActive = true
        fromDateButton.heightAnchor.constraint(equalTo: dateSelectButtonsContainerView.heightAnchor).isActive = true
        
        toDateButton.topAnchor.constraint(equalTo: dateSelectButtonsContainerView.topAnchor).isActive = true
        toDateButton.rightAnchor.constraint(equalTo: dateSelectButtonsContainerView.rightAnchor).isActive = true
        toDateButton.leftAnchor.constraint(equalTo: dateSelectButtonsContainerView.centerXAnchor, constant: 20).isActive = true
        toDateButton.heightAnchor.constraint(equalTo: dateSelectButtonsContainerView.heightAnchor).isActive = true
        
        dateFilterButtonsContainerView.topAnchor.constraint(equalTo: dateSelectButtonsContainerView.bottomAnchor, constant: 10).isActive = true
        dateFilterButtonsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        dateFilterButtonsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -10).isActive = true
        dateFilterButtonsContainerView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        dateFilterButtonsContainerView.addSubview(selectStaffButton)
        
        
        selectStaffButton.topAnchor.constraint(equalTo: dateFilterButtonsContainerView.topAnchor).isActive = true
        selectStaffButton.centerXAnchor.constraint(equalTo: dateFilterButtonsContainerView.centerXAnchor).isActive = true
        selectStaffButton.widthAnchor.constraint(equalTo: dateFilterButtonsContainerView.widthAnchor, multiplier: 0.5).isActive = true
        selectStaffButton.heightAnchor.constraint(equalTo: dateFilterButtonsContainerView.heightAnchor).isActive = true
        
        holidayShortDescriptionTextField.topAnchor.constraint(equalTo: dateFilterButtonsContainerView.bottomAnchor, constant: 5).isActive = true
        holidayShortDescriptionTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        holidayShortDescriptionTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        holidayShortDescriptionTextField.heightAnchor.constraint(equalToConstant: 70).isActive = true
        
        if let stat = self.editDesignation, stat == "staff" {
            print("called")
            self.shopAndStaffHolidaySegmentedControl.selectedSegmentIndex = 1
            self.handleSegementedControlSelection(self.shopAndStaffHolidaySegmentedControl)
            self.getHolidayDataStaff()
            
        } else {
            print("something went wrong")
        }
        
        if let stat = self.editDesignation, stat == "shop" {
            self.getHolidayDataShop()
            
        }
    }

}
