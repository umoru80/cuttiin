//
//  StaffCustomerList.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 04/03/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation

struct StaffCustomerList: Codable {
    let message: String
    let staff: [Staff]
    let customer: [Customer]
}
