//
//  OrderDetailCustomerViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/3/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import MGStarRatingView
import SwiftDate
import SwiftyJSON
import Alamofire
import SwiftSpinner

class OrderDetailCustomerViewController: UIViewController {
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    // UIColor(r: 215, g: 234, b: 249) // UIColor.clear
    
    var appointmentID:  String?
    var staffID: String?
    var customerID: String?
    var shopID: String?
    var serviceList = [String]()
    var shopOwnerCustomerID: String?
    var customerrCreated: String?
    
    var serviceOne = [ServiceData]()
    var serviceAmountArray = [AppointmentServiceAmountData]()
    var shopCurrency = ""
    
    let viewTitleNameLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("orderDetailViewControllerTitle", comment: "Appointment details")
        ht.font = UIFont(name: "OpenSans-Light", size: 20)
        ht.textColor = UIColor.black
        ht.numberOfLines = 0
        ht.adjustsFontSizeToFitWidth = true
        ht.minimumScaleFactor = 0.1
        ht.baselineAdjustment = .alignCenters
        ht.textAlignment = .left
        return ht
    }()
    
    lazy var closeViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeOrderDetailView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    lazy var customerContanerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var customerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "avatar_icon")
        imageView.setImageColor(color: UIColor(r: 11, g: 49, b: 68))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .center
        imageView.layer.cornerRadius = 30
        imageView.layer.masksToBounds = true
        imageView.backgroundColor = UIColor.white
        return imageView
    }()
    
    let customerNameLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.font = UIFont(name: "OpenSans-Light", size: 15)
        ht.textColor = UIColor.black
        ht.textAlignment = .left
        return ht
    }()
    
    lazy var customerNameLabelLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8CustomerLogo")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let appointmentTimeLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.font = UIFont(name: "OpenSans-Light", size: 15)
        ht.textColor = UIColor.black
        ht.textAlignment = .left
        return ht
    }()
    
    lazy var appointmentTimeLabelLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8timeSpanLogo")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let appointmentNumberOfPeopleLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.font = UIFont(name: "OpenSans-Light", size: 15)
        ht.textColor = UIColor.black
        ht.textAlignment = .left
        return ht
    }()
    
    lazy var appointmentNumberOfPeopleLabelLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8numberofpeople")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let appointmentPriceLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.font = UIFont(name: "OpenSans-Light", size: 15)
        ht.textColor = UIColor.black
        ht.textAlignment = .left
        return ht
    }()
    
    lazy var appointmentPriceLabelLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8cashLogo")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let appointmentIdLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.font = UIFont(name: "OpenSans-Light", size: 15)
        ht.textColor = UIColor.black
        ht.textAlignment = .left
        return ht
    }()
    
    lazy var appointmentIdLabelLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8uniqueid")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let customerNumberButtonLabel: UIButton = {
        let fnhp = UIButton()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 13)
        fnhp.contentHorizontalAlignment = .left
        fnhp.setTitleColor(UIColor.black, for: .normal)
        fnhp.backgroundColor = UIColor.clear
        fnhp.contentHorizontalAlignment = .left
        fnhp.isUserInteractionEnabled = true
        fnhp.addTarget(self, action: #selector(handleMakeCall), for: .touchUpInside)
        return fnhp
    }()
    
    lazy var customerNumberButtonLabelLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8smallmobileNumber")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var customerStaffseperator: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return view
    }()
    
    
    let staffContanerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        return view
    }()
    
    lazy var staffImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "avatar_icon")
        imageView.setImageColor(color: UIColor(r: 11, g: 49, b: 68))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .center
        imageView.layer.cornerRadius = 30
        imageView.layer.masksToBounds = true
        imageView.backgroundColor = UIColor.white
        return imageView
    }()
    
    let staffNameLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.font = UIFont(name: "OpenSans-Light", size: 15)
        ht.textColor = UIColor.black
        ht.textAlignment = .left
        return ht
    }()
    
    lazy var staffNameLabelLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8staffLogo")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let shopNameLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.font = UIFont(name: "OpenSans-Light", size: 15)
        ht.textColor = UIColor.black
        ht.textAlignment = .left
        return ht
    }()
    
    lazy var shopNameLabelLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8smallshopname")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let shopAddressLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.font = UIFont(name: "OpenSans-Light", size: 15)
        ht.textColor = UIColor.black
        ht.textAlignment = .left
        return ht
    }()
    
    lazy var shopAddressLabelLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8smalladdress")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let shopNumberButtonLabel: UIButton = {
        let fnhp = UIButton()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 13)
        fnhp.contentHorizontalAlignment = .left
        fnhp.setTitleColor(UIColor.black, for: .normal)
        fnhp.backgroundColor = UIColor.clear
        fnhp.setTitle("shop mobile number", for: .normal)
        fnhp.contentHorizontalAlignment = .left
        fnhp.isUserInteractionEnabled = true
        fnhp.addTarget(self, action: #selector(handleMakeCall), for: .touchUpInside)
        return fnhp
    }()
    
    lazy var shopNumberButtonLabelLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8smallmobileNumber")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var staffCollectionViewseperator: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return view
    }()
    
    let curvedCollectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        cview.layer.masksToBounds = true
        cview.layer.cornerRadius = 5
        return cview
    }()
    
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customServiceCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()
    
    let buttonContanerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var cancelAppointment: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.setTitle(NSLocalizedString("orderDetailViewControllerCancelButton", comment: "Cancel"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        st.tag = 1
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.backgroundColor = UIColor(r: 235, g: 81, b: 96)
        st.addTarget(self, action: #selector(handleCancelAppointment), for: .touchUpInside)
        return st
    }()
    
    var shopTimeZone: String?
    var shopLocale: String?
    var shopCalender: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        view.addSubview(viewTitleNameLabel)
        view.addSubview(closeViewButton)
        view.addSubview(customerContanerView)
        view.addSubview(customerStaffseperator)
        view.addSubview(staffContanerView)
        view.addSubview(staffCollectionViewseperator)
        view.addSubview(buttonContanerView)
        view.addSubview(curvedCollectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewContriants()
        getCustomerData()
        getStaffCustomer()
        getShopData()
        handleGetAppointmentData()
    }
    
    @objc private func handleCancelAppointment(_ sender: UIButton){
        sender.pulsate()
        
        if let shopTmzone = self.shopTimeZone, let shopCal = self.shopCalender, let shopLoc = self.shopLocale, let appointid = self.appointmentID, let shopOwner = self.shopOwnerCustomerID {
            let reasonCancelView = ReasonForCancellingViewController()
            reasonCancelView.appointmentID = appointid
            reasonCancelView.shopOwnerCustomerID = shopOwner
            reasonCancelView.shopTimeZone = shopTmzone
            reasonCancelView.shopCalender = shopCal
            reasonCancelView.shopLocale = shopLoc
            reasonCancelView.modalPresentationStyle = .overCurrentContext
            present(reasonCancelView, animated: true, completion: nil)
        }
    }
    
    
    @objc private func getCustomerData(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let customerID = self.customerID, let token = decodedCustomer.first?.token {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    
                    SwiftSpinner.show("Loading...")
                    
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    DispatchQueue.global(qos: .background).async {
                        AF.request(self.BACKEND_URL + customerID, method: .get, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    do {
                                        let customerSingleData = try JSONDecoder().decode(CustomerData.self, from: jsonData)
                                        let profileImageFileName = customerSingleData.customer.profileImageName
                                        
                                        AF.request(self.BACKEND_URL + "images/customerImages/" + profileImageFileName, headers: headers).responseData { response in
                                            
                                            switch response.result {
                                            case .success(let data):
                                                let image = UIImage(data: data)
                                                DispatchQueue.main.async {
                                                    self.customerNameLabel.text = customerSingleData.customer.firstName + " " + customerSingleData.customer.lastName
                                                    self.customerNumberButtonLabel.setTitle(customerSingleData.customer.mobileNumber, for: .normal)
                                                    self.customerImageView.image = image
                                                    self.customerImageView.contentMode = .scaleAspectFit
                                                }
                                                break
                                            case .failure(let error):
                                                print(error)
                                                self.view.makeToast(NSLocalizedString("orderDetailViewControllerErrorMessageImageNotFound", comment: "Image not found"), duration: 3.0, position: .bottom)
                                                break
                                            }
                                        }
                                        
                                    } catch _ {
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        print("error in image download")
                                    }
                                }
                        }
                    }
                    
                }
            }
        }
    }
    
    @objc private func getStaffCustomer(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let customerID = self.staffID, let token = decodedCustomer.first?.token {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        SwiftSpinner.hide()
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    DispatchQueue.global(qos: .background).async {
                        AF.request(self.BACKEND_URL + customerID, method: .get, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    do {
                                        let customerSingleData = try JSONDecoder().decode(CustomerData.self, from: jsonData)
                                        let profileImageFileName = customerSingleData.customer.profileImageName
                                        
                                        AF.request(self.BACKEND_URL + "images/customerImages/" + profileImageFileName, headers: headers).responseData { response in
                                            
                                            switch response.result {
                                            case .success(let data):
                                                let image = UIImage(data: data)
                                                DispatchQueue.main.async {
                                                    self.staffNameLabel.text = customerSingleData.customer.firstName + " " + customerSingleData.customer.lastName
                                                    self.staffImageView.image = image
                                                    self.staffImageView.contentMode = .scaleAspectFit
                                                }
                                                break
                                            case .failure(let error):
                                                print(error)
                                                self.view.makeToast(NSLocalizedString("orderDetailViewControllerErrorMessageImageNotFound", comment: "Image not found"), duration: 3.0, position: .bottom)
                                                break
                                            }
                                        }
                                        
                                    } catch _ {
                                        print("Exception caught")
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        
                                    }
                                }
                        }
                    }
                    
                }
            }
        }
    }
    
    @objc private func getShopData(){
        DispatchQueue.global(qos: .background).async {
            
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let shopUniqueID = self.shopID {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            SwiftSpinner.hide()
                            UtilityManager.alertView(view: self)
                            print("error in data request")
                        }
                    } else {
                        
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        AF.request(self.BACKEND_URL + "getsingleshop/" + shopUniqueID, method: .post, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    print("got data back")
                                    do {
                                        let shopData = try JSONDecoder().decode(Shop.self, from: jsonData)
                                        DispatchQueue.main.async {
                                            self.shopNameLabel.text = shopData.shopName
                                            self.shopAddressLabel.text = shopData.shopAddress
                                            self.shopNumberButtonLabel.setTitle(shopData.shopMobileNumber, for: .normal)
                                            self.shopOwnerCustomerID = shopData.shopOwner
                                            
                                            self.shopTimeZone = shopData.shopDateCreatedTimezone
                                            self.shopCalender = shopData.shopDateCreatedCalendar
                                            self.shopLocale = shopData.shopDateCreatedLocale
                                        }
                                        
                                    } catch _ {
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                self.view.makeToast(NSLocalizedString("orderDetailViewControllerErrorMessageShopNotFound", comment: "Shops not found"), duration: 2.0, position: .bottom)
                                            case 500:
                                                self.view.makeToast(NSLocalizedString("orderDetailViewControllerErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                            default:
                                                print("sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
    }
    
    @objc private func handleGetAppointmentData(){
        DispatchQueue.global(qos: .background).async {
            
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let appointmentID = self.appointmentID {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            SwiftSpinner.hide()
                            UtilityManager.alertView(view: self)
                            print("error in data request")
                        }
                    } else {
                        
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        AF.request(self.BACKEND_URL + "getSingleAppointment/" + appointmentID, method: .post, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    print("got data back")
                                    do {
                                        let appointmentData = try JSONDecoder().decode(Appointment.self, from: jsonData)
                                        DispatchQueue.main.async {
                                            
                                            let appointmentRegion = DateByUserDeviceInitializer.getRegion(TZoneName: appointmentData.appointmentDateTimezone, calenName: appointmentData.appointmentDateCalendar, LocName: appointmentData.appointmentDateLocale)
                                            print("date test")
                                            
                                            guard let startDate = appointmentData.appointmentDateStart.toDate("yyyy-MM-dd HH:mm:ss", region: appointmentRegion),
                                                let endDate = appointmentData.appointmentDateEnd.toDate("yyyy-MM-dd HH:mm:ss", region: appointmentRegion) else {
                                                    return
                                            }
                                            
                                            self.customerrCreated = appointmentData.appointmentCustomer
                                            
                                            switch(appointmentData.appointmentStatus){
                                            case "new":
                                                let curr = appointmentData.appointmentCurrency
                                                self.shopCurrency = curr
                                                self.appointmentPriceLabel.text = appointmentData.appointmentPrice + " " + curr
                                                
                                            case "started":
                                                if let intialPrice = Double(appointmentData.appointmentPrice) {
                                                    let curr = appointmentData.appointmentCurrency
                                                    let totalPrice = intialPrice
                                                    let appointmentCustomer = appointmentData.appointmentCustomer
                                                    self.getCancelledAppointment(price: totalPrice, currency: curr, customer: appointmentCustomer)
                                                }
                                                self.cancelAppointment.isHidden = true
                                                break
                                            case "ended":
                                                if let intialPrice = Double(appointmentData.appointmentPrice) {
                                                    let curr = appointmentData.appointmentCurrency
                                                    let totalPrice = intialPrice
                                                    let appointmentCustomer = appointmentData.appointmentCustomer
                                                    self.getCancelledAppointment(price: totalPrice, currency: curr, customer: appointmentCustomer)
                                                }
                                                self.cancelAppointment.isHidden = true
                                                break
                                            case "unpaid":
                                                let curr = appointmentData.appointmentCurrency
                                                self.shopCurrency = curr
                                                self.appointmentPriceLabel.text = appointmentData.appointmentPrice + " " + curr
                                                self.cancelAppointment.isHidden = true
                                                break
                                            case "cancelled":
                                                let curr = appointmentData.appointmentCurrency
                                                self.shopCurrency = curr
                                                self.appointmentPriceLabel.text = appointmentData.appointmentPrice + " " + curr
                                                self.cancelAppointment.isHidden = true
                                                break
                                            case "paid":
                                                let curr = appointmentData.appointmentCurrency
                                                self.shopCurrency = curr
                                                self.appointmentPriceLabel.text = appointmentData.appointmentPrice + " " + curr
                                                self.cancelAppointment.isHidden = true
                                                break
                                            default:
                                                print("new appointment")
                                                break
                                                
                                            }
                                            
                                            let startDateString = startDate.toString(DateToStringStyles.time(.medium))
                                            let endDateString = endDate.toString(DateToStringStyles.time(.medium))
                                            let fulldate = startDateString + " - " + endDateString
                                            self.appointmentTimeLabel.text = fulldate
                                            
                                            self.appointmentIdLabel.text = appointmentData.appointmentShortUniqueID
                                            self.appointmentNumberOfPeopleLabel.text = appointmentData.appointmentServiceQuantity
                                            
                                            for dataAmount in appointmentData.appointmentServiceWithAmount {
                                                let singleServiceAmount = AppointmentServiceAmountData(id: dataAmount.id, appointmentService: dataAmount.appointmentService, appointmentServiceAmount: dataAmount.appointmentServiceAmount)
                                                self.serviceAmountArray.append(singleServiceAmount)
                                                if (appointmentData.appointmentServiceWithAmount.count == self.serviceAmountArray.count && self.serviceAmountArray.count > 0){
                                                    self.handleGetShopServices(serviceAmountData: self.serviceAmountArray)
                                                }
                                            }
                                        }
                                        
                                        
                                    } catch _ {
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                self.view.makeToast(NSLocalizedString("orderDetailViewControllerErrorMessageAppointmentNotFound", comment: "Appointment not found"), duration: 2.0, position: .bottom)
                                            case 500:
                                                self.view.makeToast(NSLocalizedString("orderDetailViewControllerErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                            default:
                                                print("sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
        
    }
    
    @objc private func getCancelledAppointment(price: Double, currency: String, customer: String){
        DispatchQueue.global(qos: .background).async {
            
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                            print("error in data request")
                        }
                    } else {
                        
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        let parameters: Parameters = [
                            "shopCurrency": currency,
                        ]
                        
                        AF.request(self.BACKEND_URL + "getAllCancelledAppointmentByCustomer/" + customer, method: .post, parameters: parameters, encoding: JSONEncoding(options: []), headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"])
                            .response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    print("got data back")
                                    do {
                                        let unpaidData = try JSONDecoder().decode(UnpaidAppointment.self, from: jsonData)
                                        let appointmentCancelData = unpaidData.appointmentCancelled
                                        var chargesArrayHolder = [Double]()
                                        for cancel in appointmentCancelData {
                                            if cancel.appointmentCancelledStatus == "unpaid", let priceDouble = Double(cancel.appointmentCancelledCharges) {
                                                print(priceDouble)
                                                chargesArrayHolder.append(priceDouble)
                                            }
                                            
                                            if(chargesArrayHolder.count == appointmentCancelData.count){
                                                print("got sum complete")
                                                self.appointmentPriceLabel.text = String(chargesArrayHolder.reduce(0, +).roundTo(places: 2) + price ) + " " + currency
                                            }
                                        }
                                        
                                    } catch _ {
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                print("cancelled appointments not found")
                                                self.appointmentPriceLabel.text = String(price) + " " + currency
                                            case 500:
                                                print("An error occurred, please try again later")
                                            default:
                                                print("sky high", statusCode)
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
    }
    
    private func handleGetShopServices(serviceAmountData: [AppointmentServiceAmountData]){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, self.serviceList.count > 0 {
                
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        SwiftSpinner.hide()
                        UtilityManager.alertView(view: self)
                    }
                    //log out process based on location
                } else {
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    let parameters: Parameters = [
                        "shopServiceArray": serviceList
                    ]
                    
                    DispatchQueue.global(qos: .background).async {
                        AF.request(self.BACKEND_URL + "getshopservicesbymanyid/" , method: .post, parameters: parameters, encoding: JSONEncoding(options: []), headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"])
                            .response { (response) in
                                
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    do {
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        
                                        let shopServices = try JSONDecoder().decode([ShopService].self, from: jsonData)
                                        
                                        for shopServiceSingleData in shopServices {
                                            
                                            let serviceImageFileName = shopServiceSingleData.serviceImageName
                                            
                                            AF.request(self.BACKEND_URL + "images/shopServiceImages/" + serviceImageFileName, headers: headers).responseData { response in
                                                
                                                switch response.result {
                                                case .success(let data):
                                                    if let image = UIImage(data: data), let first = serviceAmountData.first(where: {$0.appointmentService == shopServiceSingleData.id}), let numberAmount = Int(first.appointmentServiceAmount){
                                                        let shopService = ServiceData(serviceID: shopServiceSingleData.id, serviceTitle: shopServiceSingleData.serviceTitle, serviceEstimatedTime: shopServiceSingleData.serviceEstimatedTotalTime, serviceCost: shopServiceSingleData.servicePrice, shortDescription: shopServiceSingleData.serviceDescription, category: shopServiceSingleData.serviceCategory, dateCreated: shopServiceSingleData.serviceDateCreated, timezone: shopServiceSingleData.serviceDateCreatedTimezone, calendar: shopServiceSingleData.serviceDateCreatedCalendar, local: shopServiceSingleData.serviceDateCreatedLocale, serviceImage: image, isHiddenByShop: shopServiceSingleData.serviceHiddenByShop, isHiddenByAdmin: shopServiceSingleData.serviceHiddenByAdmin, numberOfCustomer: numberAmount, serviceStaff: shopServiceSingleData.serviceStaff, isAssigned: "NO", shop: shopServiceSingleData.serviceShop)
                                                        self.serviceOne.append(shopService)
                                                        DispatchQueue.main.async {
                                                            self.collectionView.reloadData()
                                                        }
                                                    }
                                                    break
                                                case .failure(let error):
                                                    print(error)
                                                    self.view.makeToast(NSLocalizedString("orderDetailViewControllerErrorMessageImageNotFound", comment: "Image not found"), duration: 3.0, position: .bottom)
                                                    break
                                                }
                                            }
                                        }
                                    } catch _ {
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                self.view.makeToast(NSLocalizedString("orderDetailViewControllerErrorMessageServiceNotFound", comment: "Service was not found"), duration: 3.0, position: .bottom)
                                            case 500:
                                                self.view.makeToast(NSLocalizedString("orderDetailViewControllerErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 3.0, position: .bottom)
                                            default:
                                                print("Sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
        }
    }
    
    @objc func handleMakeCall(sender: UIButton){
        if let mobNUm = sender.titleLabel?.text {
            let newNumber = mobNUm.lowercased().replacingOccurrences(of: " ", with: "")
            if newNumber != "notavailable" {
                if let url = URL(string: "tel://\(newNumber)"), UIApplication.shared.canOpenURL(url) {
                    if #available(iOS 10, *) {
                        UIApplication.shared.open(url)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
                
            }
        }
    }
    
    @objc private func closeOrderDetailView(){
        self.dismiss(animated: true) {
        }
    }
    
    func setupViewContriants(){
        
        var topDistance: CGFloat = 20
        if UIDevice.current.hasNotch {
            topDistance = 60
        }
        
        viewTitleNameLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        viewTitleNameLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        viewTitleNameLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        closeViewButton.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        closeViewButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -12).isActive = true
        closeViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        closeViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        customerContanerView.topAnchor.constraint(equalTo: closeViewButton.bottomAnchor, constant: 10).isActive = true
        customerContanerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        customerContanerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        customerContanerView.heightAnchor.constraint(equalToConstant: 105).isActive = true
        
        customerContanerView.addSubview(customerImageView)
        customerContanerView.addSubview(customerNameLabel)
        customerContanerView.addSubview(customerNameLabelLogo)
        customerContanerView.addSubview(appointmentTimeLabel)
        customerContanerView.addSubview(appointmentTimeLabelLogo)
        customerContanerView.addSubview(appointmentNumberOfPeopleLabel)
        customerContanerView.addSubview(appointmentNumberOfPeopleLabelLogo)
        customerContanerView.addSubview(appointmentPriceLabel)
        customerContanerView.addSubview(appointmentPriceLabelLogo)
        customerContanerView.addSubview(appointmentIdLabel)
        customerContanerView.addSubview(appointmentIdLabelLogo)
        
        customerContanerView.addSubview(customerNumberButtonLabel)
        customerContanerView.addSubview(customerNumberButtonLabelLogo)
        
        customerImageView.topAnchor.constraint(equalTo: customerContanerView.topAnchor, constant: 5).isActive = true
        customerImageView.leftAnchor.constraint(equalTo: customerContanerView.leftAnchor, constant: 5).isActive = true
        customerImageView.widthAnchor.constraint(equalToConstant: 60).isActive = true
        customerImageView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        customerNameLabelLogo.topAnchor.constraint(equalTo: customerContanerView.topAnchor, constant: 2).isActive = true
        customerNameLabelLogo.leftAnchor.constraint(equalTo: customerImageView.rightAnchor, constant: 15).isActive = true
        customerNameLabelLogo.widthAnchor.constraint(equalToConstant: 15).isActive = true
        customerNameLabelLogo.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        customerNameLabel.topAnchor.constraint(equalTo: customerContanerView.topAnchor, constant: 2).isActive = true
        customerNameLabel.leftAnchor.constraint(equalTo: customerNameLabelLogo.rightAnchor, constant: 5).isActive = true
        customerNameLabel.rightAnchor.constraint(equalTo: customerContanerView.rightAnchor, constant: -10).isActive = true
        customerNameLabel.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        appointmentTimeLabelLogo.topAnchor.constraint(equalTo: customerNameLabelLogo.bottomAnchor, constant: 2).isActive = true
        appointmentTimeLabelLogo.leftAnchor.constraint(equalTo: customerImageView.rightAnchor, constant: 15).isActive = true
        appointmentTimeLabelLogo.widthAnchor.constraint(equalToConstant: 15).isActive = true
        appointmentTimeLabelLogo.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        appointmentTimeLabel.topAnchor.constraint(equalTo: customerNameLabel.bottomAnchor, constant: 2).isActive = true
        appointmentTimeLabel.leftAnchor.constraint(equalTo: appointmentTimeLabelLogo.rightAnchor, constant: 5).isActive = true
        appointmentTimeLabel.rightAnchor.constraint(equalTo: customerContanerView.rightAnchor, constant: -10).isActive = true
        appointmentTimeLabel.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        appointmentNumberOfPeopleLabelLogo.topAnchor.constraint(equalTo: appointmentTimeLabelLogo.bottomAnchor, constant: 2).isActive = true
        appointmentNumberOfPeopleLabelLogo.leftAnchor.constraint(equalTo: customerImageView.rightAnchor, constant: 15).isActive = true
        appointmentNumberOfPeopleLabelLogo.widthAnchor.constraint(equalToConstant: 15).isActive = true
        appointmentNumberOfPeopleLabelLogo.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        appointmentNumberOfPeopleLabel.topAnchor.constraint(equalTo: appointmentTimeLabel.bottomAnchor, constant: 2).isActive = true
        appointmentNumberOfPeopleLabel.leftAnchor.constraint(equalTo: appointmentNumberOfPeopleLabelLogo.rightAnchor, constant: 5).isActive = true
        appointmentNumberOfPeopleLabel.rightAnchor.constraint(equalTo: customerContanerView.rightAnchor, constant: -10).isActive = true
        appointmentNumberOfPeopleLabel.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        appointmentPriceLabelLogo.topAnchor.constraint(equalTo: appointmentNumberOfPeopleLabelLogo.bottomAnchor, constant: 2).isActive = true
        appointmentPriceLabelLogo.leftAnchor.constraint(equalTo: customerImageView.rightAnchor, constant: 15).isActive = true
        appointmentPriceLabelLogo.widthAnchor.constraint(equalToConstant: 15).isActive = true
        appointmentPriceLabelLogo.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        appointmentPriceLabel.topAnchor.constraint(equalTo: appointmentNumberOfPeopleLabel.bottomAnchor, constant: 2).isActive = true
        appointmentPriceLabel.leftAnchor.constraint(equalTo: appointmentPriceLabelLogo.rightAnchor, constant: 5).isActive = true
        appointmentPriceLabel.rightAnchor.constraint(equalTo: customerContanerView.rightAnchor, constant: -10).isActive = true
        appointmentPriceLabel.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        appointmentIdLabelLogo.topAnchor.constraint(equalTo: appointmentPriceLabelLogo.bottomAnchor, constant: 2).isActive = true
        appointmentIdLabelLogo.leftAnchor.constraint(equalTo: customerImageView.rightAnchor, constant: 15).isActive = true
        appointmentIdLabelLogo.widthAnchor.constraint(equalToConstant: 15).isActive = true
        appointmentIdLabelLogo.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        appointmentIdLabel.topAnchor.constraint(equalTo: appointmentPriceLabel.bottomAnchor, constant: 2).isActive = true
        appointmentIdLabel.leftAnchor.constraint(equalTo: appointmentIdLabelLogo.rightAnchor, constant: 5).isActive = true
        appointmentIdLabel.rightAnchor.constraint(equalTo: customerContanerView.rightAnchor, constant: -10).isActive = true
        appointmentIdLabel.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        
        
        
        
        
        customerNumberButtonLabelLogo.topAnchor.constraint(equalTo: appointmentIdLabelLogo.bottomAnchor, constant: 2).isActive = true
        customerNumberButtonLabelLogo.leftAnchor.constraint(equalTo: customerImageView.rightAnchor, constant: 15).isActive = true
        customerNumberButtonLabelLogo.widthAnchor.constraint(equalToConstant: 15).isActive = true
        customerNumberButtonLabelLogo.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        customerNumberButtonLabel.topAnchor.constraint(equalTo: appointmentIdLabel.bottomAnchor, constant: 2).isActive = true
        customerNumberButtonLabel.leftAnchor.constraint(equalTo: customerNumberButtonLabelLogo.rightAnchor, constant: 5).isActive = true
        customerNumberButtonLabel.rightAnchor.constraint(equalTo: customerContanerView.rightAnchor, constant: -10).isActive = true
        customerNumberButtonLabel.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        
        
        
        
        customerStaffseperator.topAnchor.constraint(equalTo: customerContanerView.bottomAnchor, constant: 5).isActive = true
        customerStaffseperator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        customerStaffseperator.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        customerStaffseperator.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        staffContanerView.topAnchor.constraint(equalTo: customerStaffseperator.bottomAnchor, constant: 5).isActive = true
        staffContanerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        staffContanerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        staffContanerView.heightAnchor.constraint(equalToConstant: 70).isActive = true
        
        staffContanerView.addSubview(staffImageView)
        staffContanerView.addSubview(staffNameLabel)
        staffContanerView.addSubview(staffNameLabelLogo)
        staffContanerView.addSubview(shopNameLabel)
        staffContanerView.addSubview(shopNameLabelLogo)
        staffContanerView.addSubview(shopAddressLabel)
        staffContanerView.addSubview(shopAddressLabelLogo)
        staffContanerView.addSubview(shopNumberButtonLabel)
        staffContanerView.addSubview(shopNumberButtonLabelLogo)
        
        staffImageView.topAnchor.constraint(equalTo: staffContanerView.topAnchor, constant: 5).isActive = true
        staffImageView.leftAnchor.constraint(equalTo: staffContanerView.leftAnchor, constant: 5).isActive = true
        staffImageView.widthAnchor.constraint(equalToConstant: 60).isActive = true
        staffImageView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        staffNameLabelLogo.topAnchor.constraint(equalTo: staffContanerView.topAnchor, constant: 2).isActive = true
        staffNameLabelLogo.leftAnchor.constraint(equalTo: staffImageView.rightAnchor, constant: 15).isActive = true
        staffNameLabelLogo.widthAnchor.constraint(equalToConstant: 15).isActive = true
        staffNameLabelLogo.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        staffNameLabel.topAnchor.constraint(equalTo: staffContanerView.topAnchor, constant: 2).isActive = true
        staffNameLabel.leftAnchor.constraint(equalTo: staffNameLabelLogo.rightAnchor, constant: 5).isActive = true
        staffNameLabel.rightAnchor.constraint(equalTo: staffContanerView.rightAnchor, constant: -10).isActive = true
        staffNameLabel.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        shopNameLabelLogo.topAnchor.constraint(equalTo: staffNameLabelLogo.bottomAnchor, constant: 2).isActive = true
        shopNameLabelLogo.leftAnchor.constraint(equalTo: staffImageView.rightAnchor, constant: 15).isActive = true
        shopNameLabelLogo.widthAnchor.constraint(equalToConstant: 15).isActive = true
        shopNameLabelLogo.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        shopNameLabel.topAnchor.constraint(equalTo: staffNameLabel.bottomAnchor, constant: 2).isActive = true
        shopNameLabel.leftAnchor.constraint(equalTo: shopNameLabelLogo.rightAnchor, constant: 5).isActive = true
        shopNameLabel.rightAnchor.constraint(equalTo: staffContanerView.rightAnchor, constant: -10).isActive = true
        shopNameLabel.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        shopAddressLabelLogo.topAnchor.constraint(equalTo: shopNameLabelLogo.bottomAnchor, constant: 2).isActive = true
        shopAddressLabelLogo.leftAnchor.constraint(equalTo: staffImageView.rightAnchor, constant: 15).isActive = true
        shopAddressLabelLogo.widthAnchor.constraint(equalToConstant: 15).isActive = true
        shopAddressLabelLogo.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        shopAddressLabel.topAnchor.constraint(equalTo: shopNameLabel.bottomAnchor, constant: 2).isActive = true
        shopAddressLabel.leftAnchor.constraint(equalTo: shopAddressLabelLogo.rightAnchor, constant: 5).isActive = true
        shopAddressLabel.rightAnchor.constraint(equalTo: staffContanerView.rightAnchor, constant: -10).isActive = true
        shopAddressLabel.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        shopNumberButtonLabelLogo.topAnchor.constraint(equalTo: shopAddressLabelLogo.bottomAnchor, constant: 2).isActive = true
        shopNumberButtonLabelLogo.leftAnchor.constraint(equalTo: staffImageView.rightAnchor, constant: 15).isActive = true
        shopNumberButtonLabelLogo.widthAnchor.constraint(equalToConstant: 15).isActive = true
        shopNumberButtonLabelLogo.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        shopNumberButtonLabel.topAnchor.constraint(equalTo: shopAddressLabel.bottomAnchor, constant: 2).isActive = true
        shopNumberButtonLabel.leftAnchor.constraint(equalTo: shopNumberButtonLabelLogo.rightAnchor, constant: 5).isActive = true
        shopNumberButtonLabel.rightAnchor.constraint(equalTo: staffContanerView.rightAnchor, constant: -10).isActive = true
        shopNumberButtonLabel.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        staffCollectionViewseperator.topAnchor.constraint(equalTo: staffContanerView.bottomAnchor, constant: 5).isActive = true
        staffCollectionViewseperator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        staffCollectionViewseperator.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        staffCollectionViewseperator.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        
        buttonContanerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10).isActive = true
        buttonContanerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        buttonContanerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        buttonContanerView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        buttonContanerView.addSubview(cancelAppointment)
        
        cancelAppointment.topAnchor.constraint(equalTo: buttonContanerView.topAnchor).isActive = true
        cancelAppointment.centerXAnchor.constraint(equalTo: buttonContanerView.centerXAnchor).isActive = true
        cancelAppointment.widthAnchor.constraint(equalTo: buttonContanerView.widthAnchor, multiplier: 0.5).isActive = true
        cancelAppointment.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        curvedCollectView.topAnchor.constraint(equalTo: staffCollectionViewseperator.bottomAnchor, constant: 5).isActive = true
        curvedCollectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        curvedCollectView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        curvedCollectView.bottomAnchor.constraint(equalTo: buttonContanerView.topAnchor, constant: -10).isActive = true
        
        curvedCollectView.addSubview(collectView)
        
        collectView.topAnchor.constraint(equalTo: curvedCollectView.topAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: curvedCollectView.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: curvedCollectView.widthAnchor, constant: -10).isActive = true
        collectView.heightAnchor.constraint(equalTo: curvedCollectView.heightAnchor, constant: -10).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
    }

}


