//
//  UnpaidAppointmentData.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 21/04/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit

struct UnpaidAppointmentData {
    var shopID: String
    var shopNameID: String
    var shopMobileNumber: String
    var shopEmail: String
    var shopAddress: String
    var shopLogoImage: UIImage
    var appointmentID: String
    var appointmentShortUniqueID: String
    var appointmentDateStart: String
    var appointmentDateEnd: String
    var appointmentStatus: String
    var appointmentDateCreated: String
    var appointmentDateTimezone: String
    var appointmentDateCalendar: String
    var appointmentDateLocale: String
    var appointmentCancelledID: String
    var appointmentCancelledCharges: String
    var initiator: String
    var appointmentCancelledReason: String
    var appointmentCancelledStatus: String
    var appointmentCancelledTimezone: String
    var appointmentCancelledCalendar: String
    var appointmentCancelledLocale: String
    var appointmentCurrency: String
    
    
    init(
        shopID: String,
        shopNameID: String,
        shopMobileNumber: String,
        shopEmail: String,
        shopAddress: String,
        shopLogoImage: UIImage,
        appointmentID: String,
        appointmentShortUniqueID: String,
        appointmentDateStart: String,
        appointmentDateEnd: String,
        appointmentStatus: String,
        appointmentDateCreated: String,
        appointmentDateTimezone: String,
        appointmentDateCalendar: String,
        appointmentDateLocale: String,
        appointmentCancelledID: String,
        appointmentCancelledCharges: String,
        initiator: String,
        appointmentCancelledReason: String,
        appointmentCancelledStatus: String,
        appointmentCancelledTimezone: String,
        appointmentCancelledCalendar: String,
        appointmentCancelledLocale: String,
        appointmentCurrency: String
        ) {
        
        self.shopID = shopID
        self.shopNameID = shopNameID
        self.shopMobileNumber = shopMobileNumber
        self.shopEmail = shopEmail
        self.shopAddress = shopAddress
        self.shopLogoImage = shopLogoImage
        self.appointmentID = appointmentID
        self.appointmentShortUniqueID = appointmentShortUniqueID
        self.appointmentDateStart = appointmentDateStart
        self.appointmentDateEnd = appointmentDateEnd
        self.appointmentStatus = appointmentStatus
        self.appointmentDateCreated = appointmentDateCreated
        self.appointmentDateTimezone = appointmentDateTimezone
        self.appointmentDateCalendar = appointmentDateCalendar
        self.appointmentDateLocale = appointmentDateLocale
        self.appointmentCancelledID = appointmentCancelledID
        self.appointmentCancelledCharges = appointmentCancelledCharges
        self.initiator = initiator
        self.appointmentCancelledReason = appointmentCancelledReason
        self.appointmentCancelledStatus = appointmentCancelledStatus
        self.appointmentCancelledTimezone = appointmentCancelledTimezone
        self.appointmentCancelledCalendar = appointmentCancelledCalendar
        self.appointmentCancelledLocale = appointmentCancelledLocale
        self.appointmentCurrency = appointmentCurrency
    }
}
