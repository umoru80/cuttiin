//
//  ChoosePhotoViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/13/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import GooglePlaces
import SwiftDate
import Alamofire
import SwiftyJSON
import Toast_Swift
import SwiftSpinner

class ChoosePhotoViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    var companyAddressLong: String?
    var companyAddressLat: String?
    
    var customerOrBarberChoice: String?
    
    let picker = UIImagePickerController()
    
    var customerValues: [String: AnyObject]?
    
    var registerBarberViewController: RegisterBarberViewController?
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    let headerTitle: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("headerTitleTextChoosePhotView", comment: "Welcome")
        ht.font = UIFont(name: "OpenSans-SemiBold", size: 22)
        ht.textColor = UIColor.black
        ht.textAlignment = .center
        return ht
    }()
    
    lazy var selectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "add_photo_smallestAnd")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .center
        imageView.layer.cornerRadius = 100
        imageView.layer.masksToBounds = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showCameraActionOptions)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let textContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    let descriptionTitle: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.font = UIFont(name: "OpenSans-Light", size: 15)
        ht.textColor = UIColor.black
        ht.textAlignment = .center
        return ht
    }()
    
    let descriptionBody: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.numberOfLines = 0
        ht.font = UIFont(name: "OpenSans-Light", size: 13)
        ht.textColor = UIColor.black
        ht.textAlignment = .center
        return ht
    }()
    
    let errorMessagePlaceHolder: UILabel = {
        let emhp = UILabel()
        emhp.translatesAutoresizingMaskIntoConstraints = false
        emhp.font = UIFont(name: "OpenSans-Light", size: 12)
        emhp.textColor = UIColor(r: 23, g: 69, b: 90)
        emhp.adjustsFontSizeToFitWidth = true
        emhp.minimumScaleFactor = 0.1
        emhp.numberOfLines = 0
        emhp.baselineAdjustment = .alignCenters
        emhp.textAlignment = .center
        return emhp
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        picker.delegate = self
        
        if let chosenViewToDisplay = customerOrBarberChoice {
            switch chosenViewToDisplay {
            case "customer":
                navigationItem.title = NSLocalizedString("navigationTitleTextChoosePhotoView", comment: "Registration - Final")
                self.descriptionTitle.text = NSLocalizedString("descriptionTitleTextChoosePhotoViewChangeHold", comment: "Please upload a profile a picture")
                navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("choosePhotoViewNaviationButtonDone", comment: "Done"), style: .done, target: self, action: #selector(handleUpdateProfileWithURL))
            case "shop":
                navigationItem.title = NSLocalizedString("choosePhotoViewNavigationTitle", comment: "Registration - Step 2")
                self.descriptionTitle.text = NSLocalizedString("choosePhotoViewNaviagtionDescriptionTitle", comment: "Please upload a logo for your shop")
                navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("choosePhotoViewNaviationButtonNext", comment: "Next"), style: .done, target: self, action: #selector(handleUpdateBarberProfileWithURL))
            default:
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.descriptionBody.text = NSLocalizedString("descriptionBodyTextChoosePhotoViewChangeHold", comment: "This is will be shown on your profile. You can also update it later by accessing your profile page")
        
        navigationItem.rightBarButtonItem?.isEnabled = false
        view.addSubview(headerTitle)
        view.addSubview(selectImageView)
        view.addSubview(textContainerView)
        view.addSubview(errorMessagePlaceHolder)
        setupInputContainerView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.errorMessagePlaceHolder.text = ""
        
    }
    
    @objc func hideKeyboard(){
        view.endEditing(true)
    }
    
    @objc func handleDissmissView(){
        dismiss(animated: true, completion: nil)
    }
    
    func setupInputContainerView(){
        headerTitle.topAnchor.constraint(equalTo: view.topAnchor, constant: 30).isActive = true
        headerTitle.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        headerTitle.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5).isActive = true
        headerTitle.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        selectImageView.topAnchor.constraint(equalTo: headerTitle.bottomAnchor, constant: 10).isActive = true
        selectImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        selectImageView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        selectImageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        textContainerView.topAnchor.constraint(equalTo: selectImageView.bottomAnchor, constant: 30).isActive = true
        textContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        textContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        textContainerView.heightAnchor.constraint(equalToConstant: 90).isActive = true
        
        textContainerView.addSubview(descriptionTitle)
        textContainerView.addSubview(descriptionBody)
        
        descriptionTitle.topAnchor.constraint(equalTo: textContainerView.topAnchor).isActive = true
        descriptionTitle.centerXAnchor.constraint(equalTo: textContainerView.centerXAnchor).isActive = true
        descriptionTitle.widthAnchor.constraint(equalTo: textContainerView.widthAnchor).isActive = true
        descriptionTitle.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        descriptionBody.topAnchor.constraint(equalTo: descriptionTitle.bottomAnchor).isActive = true
        descriptionBody.centerXAnchor.constraint(equalTo: textContainerView.centerXAnchor).isActive = true
        descriptionBody.widthAnchor.constraint(equalTo: textContainerView.widthAnchor).isActive = true
        descriptionBody.heightAnchor.constraint(equalToConstant: 65).isActive = true
        
        errorMessagePlaceHolder.topAnchor.constraint(equalTo: descriptionBody.bottomAnchor, constant: 10).isActive = true
        errorMessagePlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        errorMessagePlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
        errorMessagePlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
    }
    
    @objc func handleTakePhoto(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = true
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            self.errorMessagePlaceHolder.text = NSLocalizedString("errorMessageNoCameraChoosePhotoView", comment: "Sorry, this device has no camera")
        }
    }
    
    @objc func handleChoosePhoto(){
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    
    
    @objc func showCameraActionOptions(){
        self.errorMessagePlaceHolder.text = ""
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("choosePhotoViewAlertViewCancel", comment: "Cancel"), style: .cancel) { action in
        }
        alertController.addAction(cancelAction)
        
        let takePhotoAction = UIAlertAction(title: NSLocalizedString("choosePhotoViewAlertViewTakePhoto", comment: "Take photo"), style: .default) { action in
            self.handleTakePhoto()
        }
        alertController.addAction(takePhotoAction)
        
        let choosePhotoAction = UIAlertAction(title: NSLocalizedString("choosePhotoViewAlertViewChoosePhoto", comment: "Choose photo"), style: .default) { action in
            self.handleChoosePhoto()
        }
        alertController.addAction(choosePhotoAction)
        
        self.present(alertController, animated: true) {
        }
    }
    
    @objc func handleUpdateProfileWithURL(){
        if let firstName = self.customerValues?["firstName"] as? String, let lastName = self.customerValues?["lastName"], let email = self.customerValues?["email"] as? String, let password = self.customerValues?["password"] ,let profileImage = self.selectImageView.image, let uploadData = profileImage.jpegData(compressionQuality: 0.1) {
            
            let imageName = NSUUID().uuidString
            
            let userTimezone = DateByUserDeviceInitializer.tzone
            let userCalender = "gregorian"
            let locale = "en"
            let regionData = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: locale)
            let newDate = DateInRegion().convertTo(region: regionData).toFormat("yyyy-MM-dd HH:mm:ss")
            let parameters = ["firstName": firstName,
                              "lastName": lastName,
                              "email": email,
                              "profileImageName":"\(imageName).jpeg",
                "password": password,
                "dateCreated": newDate,
                "dateCreatedTimezone": userTimezone,
                "dateCreatedCalendar": userCalender,
                "dateCreatedLocale": "en",
                "authenticationType": "email",
                "shopOwner": "NO",
                "accountDeactivated": "NO",
                "loginDate":newDate,
                "loginTimezone":userTimezone,
                "loginCalendar":userCalender,
                "loginLocale":"en"] as [String : Any]
            SwiftSpinner.show("Loading...")
            DispatchQueue.global(qos: .background).async {
                
                
                
                AF.upload(multipartFormData: { (multipartFormData) in
                    multipartFormData.append(uploadData, withName: "image",fileName: imageName, mimeType: "image/jpg")
                    for (key, value) in parameters {
                        if let stringValue = value as? String {
                            multipartFormData.append(stringValue.data(using: .utf8)!, withName: key)
                        }
                    }
                }, usingThreshold: UInt64.init(), fileManager: .default, to: self.BACKEND_URL + "signup", method: .post, interceptor: nil)
                    .validate(statusCode: 200..<501)
                    .validate(contentType: ["application/json"])
                    .uploadProgress(queue: .main, closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    }).response { response in
                        
                        switch response.result {
                        case .success(let data):
                            DispatchQueue.main.async {
                                SwiftSpinner.hide()
                            }
                            if let jsonData = data {
                                do {let loggedIn = try JSONDecoder().decode(LoggedIn.self, from: jsonData)
                                    let customerId = loggedIn.customer.id
                                    let tokenData = loggedIn.token
                                    let timezone = DateByUserDeviceInitializer.tzone
                                    let shopOwner = loggedIn.customer.shopOwner
                                    let expiresInCheck = loggedIn.expiresIn
                                    let sessionPeriondEndDate = DateInRegion().convertTo(region: regionData) + 7.days
                                    let sessionPeriodEndString = sessionPeriondEndDate.toFormat("yyyy-MM-dd HH:mm:ss")
                                    self.handleCreateUserData(expiresInCheck: expiresInCheck, sessionPeriodEndString: sessionPeriodEndString, customerId: customerId, tokenData: tokenData, timezone: timezone, shopOwner: shopOwner, authType: "email", email: email)
                                    self.moveToNextView()
                                } catch _ {
                                    if let statusCode = response.response?.statusCode {
                                        print("status code: ", statusCode)
                                        switch statusCode {
                                        case 409:
                                            self.view.makeToast(NSLocalizedString("choosePhotoViewErrorMesageUniqueID", comment: "Short unique identifier duplicate, please try again"), duration: 3.0, position: .bottom)
                                        case 500:
                                            self.view.makeToast(NSLocalizedString("choosePhotoViewErrorMesageErrorOccured", comment: "An error occurred, please try again later"), duration: 3.0, position: .bottom)
                                        default:
                                            print("Sky high")
                                        }
                                    }
                                }
                            }
                            break
                        case .failure(let error):
                            DispatchQueue.main.async {
                                SwiftSpinner.hide()
                            }
                            print(error)
                            let message = NSLocalizedString("choosePhotoViewErrorMesageUpdateError", comment: "Update error: please try again later")
                            self.view.makeToast(message, duration: 3.0, position: .bottom)
                            break
                        }
                }
            }
        }
        
    }
    
    //handle createloggedIn details
    @objc func handleCreateUserData(expiresInCheck: String, sessionPeriodEndString: String, customerId: String, tokenData: String, timezone: String, shopOwner: String, authType: String, email: String){
        if expiresInCheck == "7d" {
            let teams = [CustomerToken(expiresIn: sessionPeriodEndString, customerId: customerId, token: "Bearer \(tokenData)", timezone: timezone, shopOwner: shopOwner, authType: authType, email: email, autoShopSwitch: "NO")]
            
            let userDefaults = UserDefaults.standard
            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: teams)
            userDefaults.set(encodedData, forKey: "token")
            userDefaults.synchronize()
        }
    }
    
    @objc func handleUpdateBarberProfileWithURL(){
        
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let customerID = decodedCustomer.first?.customerId, let token = decodedCustomer.first?.token {
                
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    if let shopName = self.customerValues?["shopName"] as? String, let shopEmail = self.customerValues?["email"], let mobileNumber = self.customerValues?["mobileNumber"] as? String, let formAddress = self.customerValues?["address"], let addLong = self.customerValues?["addressLongitude"], let addLat = self.customerValues?["addressLatitude"], let ctry = self.customerValues?["country"], let ctryCode = self.customerValues?["countryCode"], let ctryCurrencyCode = self.customerValues?["countryCurrencyCode"], let shopCate = self.customerValues?["shopCategory"], let profileImage = self.selectImageView.image, let genderSelected = self.customerValues?["shopCustomerGender"], let uploadData = profileImage.jpegData(compressionQuality: 0.1) {
                        
                        let imageName = NSUUID().uuidString
                        
                        let userTimezone = DateByUserDeviceInitializer.tzone
                        let userCalender = "gregorian"
                        let userLocale = "en"
                        let regionData = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: userLocale)
                        let newDate = DateInRegion().convertTo(region: regionData).toFormat("yyyy-MM-dd HH:mm:ss")
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        let parameters = [
                            "shopName": shopName,
                            "shopMobileNumber": mobileNumber,
                            "shopAddress": formAddress,
                            "shopAddressLongitude": addLong,
                            "shopAddressLatitude": addLat,
                            "shopCountryCode": ctryCode,
                            "shopCountry": ctry,
                            "shopCountryCurrencyCode": ctryCurrencyCode,
                            "shopEmail": shopEmail,
                            "shopCustomerGender": genderSelected,
                            "shopDateCreated": newDate,
                            "shopDateCreatedTimezone": userTimezone,
                            "shopDateCreatedCalendar": userCalender,
                            "shopDateCreatedLocale": userLocale,
                            "shopCategory": shopCate
                            ] as [String: Any]
                        SwiftSpinner.show("Loading...")
                        DispatchQueue.global(qos: .background).async {
                            print(parameters)
                            
                            AF.upload(multipartFormData: { (multipartFormData) in
                                multipartFormData.append(uploadData, withName: "image",fileName: imageName, mimeType: "image/jpg")
                                for (key, value) in parameters {
                                    if let stringValue = value as? String {
                                        multipartFormData.append(stringValue.data(using: .utf8)!, withName: key)
                                    }
                                }
                            }, usingThreshold: UInt64.init(), fileManager: .default, to: self.BACKEND_URL + "createShopWithCategory/" + customerID, method: .post, headers: headers, interceptor: nil)
                                .validate(statusCode: 200..<501)
                                .validate(contentType: ["application/json"])
                                .uploadProgress(queue: .main, closure: { (progress) in
                                    print("Upload Progress: \(progress.fractionCompleted)")
                                }).response { response in
                                    
                                    switch response.result {
                                    case .success(let data):
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        if let jsonData = data {
                                            do {
                                                let serverResponse = try JSONDecoder().decode(ShopCreated.self, from: jsonData)
                                                let message = serverResponse.message
                                                self.view.makeToast(message, duration: 3.0, position: .bottom)
                                                self.handleNextSkipAction(shopID: serverResponse.shop.id)
                                            } catch _ {
                                                if let statusCode = response.response?.statusCode {
                                                    switch statusCode {
                                                    case 404:
                                                        print(statusCode)
                                                        self.view.makeToast(NSLocalizedString("choosePhotoViewErrorMesageCustomerDoesNotExist", comment: "Shop not created as customer does not exist"), duration: 3.0, position: .bottom)
                                                    case 409:
                                                        self.view.makeToast(NSLocalizedString("choosePhotoViewErrorMesageDuplicateMobileNumber", comment: "Mobile number or email already exist"), duration: 3.0, position: .bottom)
                                                    case 500:
                                                        self.view.makeToast(NSLocalizedString("choosePhotoViewErrorMesageErrorOccured", comment: "An error occurred, please try again later"), duration: 3.0, position: .bottom)
                                                    default:
                                                        print("Sky high")
                                                    }
                                                }
                                            }
                                        }
                                        break
                                    case .failure(let error):
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        print(error)
                                        let message = NSLocalizedString("choosePhotoViewErrorMesageUpdateError", comment: "Update error: please try again later")
                                        self.view.makeToast(message, duration: 3.0, position: .bottom)
                                        break
                                    }
                            }
                        }
                    }
                    
                    
                }
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            
            selectedImageFromPicker = editedImage
            
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            selectImageView.contentMode = .scaleAspectFit
            selectImageView.image = selectedImage
            navigationItem.rightBarButtonItem?.isEnabled = true
        }
        
        dismiss(animated:true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func moveToNextView(){
        DispatchQueue.main.async {
            SwiftSpinner.hide()
        }
        self.dismiss(animated: true, completion: {
            NotificationCenter.default.post(name: .didReceiveDataMainNavigationController, object: nil)
        })
    }
    
    func handleNextSkipAction(shopID: String){
        weak var pvc = self.presentingViewController
        self.dismiss(animated: true) {
            let openingHoursview = OpeningHoursViewController()
            openingHoursview.shopID = shopID
            self.selectImageView.image = UIImage(named: "add_picture")
            let navController = UINavigationController(rootViewController: openingHoursview)
            pvc?.present(navController, animated: true, completion: nil)
        }
    }

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}
