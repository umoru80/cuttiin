//
//  LoginViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/14/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Navajo_Swift
import FBSDKLoginKit
import Alamofire
import SwiftyJSON
import Toast_Swift
import SwiftDate
import SwiftSpinner

class LoginViewController: UIViewController {
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    var mainViewController = MainNavigationController()
    
    let loginButton = FBLoginButton()
    var welcomeviewholder: WelcomeViewController?
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    lazy var loginViewCloseViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleBackAction)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let loginViewTitlePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("loginTextNavigationTitle", comment: "login")
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    let inputsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let emailHiddenPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        ehp.font = UIFont(name: "OpenSans-Light", size: 10)
        ehp.textColor = UIColor.black
        ehp.isHidden = true
        ehp.adjustsFontSizeToFitWidth = true
        ehp.minimumScaleFactor = 0.1
        ehp.baselineAdjustment = .alignCenters
        ehp.textAlignment = .left
        return ehp
    }() // NSLocalizedString("createAnAccountButtonTitleWelcomeView", comment: "create an accunt")
    
    let emailTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.placeholder = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.keyboardType = .emailAddress
        em.autocapitalizationType = .none
        return em
    }()
    
    let emailSeperatorView: UIView = {
        let esv = UIView()
        esv.translatesAutoresizingMaskIntoConstraints = false
        esv.backgroundColor = UIColor.black
        return esv
    }()
    
    let passwordHiddenPlaceHolder: UILabel = {
        let php = UILabel()
        php.translatesAutoresizingMaskIntoConstraints = false
        php.text = NSLocalizedString("passwordTextLabelAndTextFieldPlaceholderLoginView", comment: "Password")
        php.font = UIFont(name: "OpenSans-Light", size: 10)
        php.textColor = UIColor.black
        php.isHidden = true
        php.adjustsFontSizeToFitWidth = true
        php.minimumScaleFactor = 0.1
        php.baselineAdjustment = .alignCenters
        php.textAlignment = .left
        return php
    }()
    
    let passwordTextField: UITextField = {
        let ps = UITextField()
        ps.translatesAutoresizingMaskIntoConstraints = false
        ps.textColor = UIColor.black
        ps.placeholder = NSLocalizedString("passwordTextLabelAndTextFieldPlaceholderLoginView", comment: "Password")
        ps.font = UIFont(name: "OpenSans-Light", size: 15)
        ps.isSecureTextEntry = true
        return ps
    }()
    
    lazy var showPassordTextFieldButton: UIButton = {
        let sptfb = UIButton()
        sptfb.translatesAutoresizingMaskIntoConstraints = false
        sptfb.isUserInteractionEnabled = true
        sptfb.isEnabled = true
        sptfb.setTitle(NSLocalizedString("showButtonTitleLoginView", comment: "show"), for: .normal)
        sptfb.setTitleColor(UIColor.black, for: .normal)
        sptfb.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 9)
        sptfb.backgroundColor = UIColor.white
        sptfb.addTarget(self, action: #selector(handleShowPasswordText), for: .touchUpInside)
        return sptfb
    }()
    
    let passwordSeperatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return view
    }()
    
    lazy var signInButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        st.setTitle(NSLocalizedString("loginViewLoginButtonTitle", comment: "login"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 15)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.addTarget(self, action: #selector(handleSignInIntoAccount), for: .touchUpInside)
        st.isEnabled = true
        return st
    }()
    
    lazy var forgotPasswordButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitle(NSLocalizedString("forgotPasswordButtonTitleLoginView", comment: "forgot your password?"), for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 13.0)
        st.setTitleColor(UIColor.black, for: .normal)
        st.addTarget(self, action: #selector(handleForgotPassword), for: .touchUpInside)
        return st
    }()
    
    let errorMessagePlaceHolder: UILabel = {
        let emhp = UILabel()
        emhp.translatesAutoresizingMaskIntoConstraints = false
        emhp.font = UIFont(name: "OpenSans-Light", size: 10)
        emhp.textColor = UIColor.black
        emhp.adjustsFontSizeToFitWidth = true
        emhp.minimumScaleFactor = 0.1
        emhp.numberOfLines = 0
        emhp.baselineAdjustment = .alignCenters
        emhp.textAlignment = .center
        return emhp
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        view.addSubview(inputsContainerView)
        view.addSubview(signInButton)
        view.addSubview(forgotPasswordButton)
        view.addSubview(errorMessagePlaceHolder)
        setupInputContainerView()
        setupButtons()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleBackAction(){
        self.hideKeyboard()
        dismiss(animated: true, completion: nil)
    }
    
    //new entry controls
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.emailTextField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.emailTextField.text = ""
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.passwordTextField.text = ""
        self.passwordSeperatorView.backgroundColor = UIColor.black
    }
    
    
    @objc func hideKeyboard(){
        view.endEditing(true)
    }
    
    @objc func handleShowPasswordText(){
        passwordTextField.isSecureTextEntry = false
    }
    
    @objc func handleSignInIntoAccount(){
        
        if let email = self.emailTextField.text, let password = self.passwordTextField.text, !email.isEmpty && !password.isEmpty {
            SwiftSpinner.show("Loading...")
            DispatchQueue.global(qos: .background).async {
                let newDate = DateInRegion().toFormat("yyyy-MM-dd HH:mm:ss")
                let userTimezone = DateByUserDeviceInitializer.tzone
                let userCalender = "gregorian"
                let locale = "en"
                let regionData = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: locale)
                let parameters: Parameters = ["email": email, "password": password, "loginDate": newDate, "loginTimezone": userTimezone, "loginCalendar":userCalender, "loginLocale": locale]
                AF.request(self.BACKEND_URL + "login", method: .post, parameters: parameters)
                .validate(statusCode: 200..<501)
                .validate(contentType: ["application/json"])
                    .responseJSON(completionHandler: { (response) in
                        if let error = response.error {
                            print("zones", error.localizedDescription)
                            DispatchQueue.main.async {
                                SwiftSpinner.hide()
                            }
                            return
                        }
                        
                        switch response.result {
                        case .success( _):
                            //print(data)
                            break
                        case .failure(let error):
                            print(error.localizedDescription)
                            break
                        }
                        
                        if let jsonData = response.data { // response.data
                            DispatchQueue.main.async {
                                SwiftSpinner.hide()
                            }
                            do {
                                let loggedIn = try JSONDecoder().decode(LoggedIn.self, from: jsonData)
                                let customerId = loggedIn.customer.id
                                let tokenData = loggedIn.token
                                let timezone = userTimezone
                                let shopOwner = loggedIn.customer.shopOwner
                                let expiresInCheck = loggedIn.expiresIn
                                let sessionPeriondEndDate = DateInRegion().convertTo(region: regionData) + 7.days
                                let sessionPeriodEndString = sessionPeriondEndDate.toFormat("yyyy-MM-dd HH:mm:ss")
                                self.handleCreateUserData(expiresInCheck: expiresInCheck, sessionPeriodEndString: sessionPeriodEndString, customerId: customerId, tokenData: tokenData, timezone: timezone, shopOwner: shopOwner, authType: "email", email: email)
                                self.hideKeyboard()
                                
                            } catch _ {
                                self.hideKeyboard()
                                if let status = response.response?.statusCode {
                                    switch status {
                                    case 401:
                                        self.view.makeToast(NSLocalizedString("loginViewErrorMessageEmailAndPasswordInccorect", comment: "email and password is incorrect"), duration: 2.0, position: .bottom)
                                    case 404:
                                        self.view.makeToast(NSLocalizedString("loginViewErrorMessageAccountDoesNotExist", comment: "Account does not exist"), duration: 2.0, position: .bottom)
                                    case 500:
                                        self.view.makeToast(NSLocalizedString("loginViewErrorMessageErrorOccured", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                    default:
                                        print("Sky high")
                                        
                                    }
                                }
                            }
                        }
                    })
            }
        } else {
            //self.view.makeToast(shopData.message, duration: 2.0, position: .bottom)
            let messageW = NSLocalizedString("loginViewErrorMessagePleaseInsert", comment: "please insert your ")
            if self.emailTextField.text == "" && self.passwordTextField.text == "" {
              self.view.makeToast( messageW + NSLocalizedString("loginViewErrorMessageEmailAndPassoword", comment: "email and password"), duration: 2.0, position: .bottom)
            } else {
                
                if self.emailTextField.text == "" {
                    self.view.makeToast(messageW + NSLocalizedString("loginViewErrorMessageEmailAtach", comment: "email"), duration: 2.0, position: .bottom)
                }
                
                if self.passwordTextField.text == "" {
                    self.view.makeToast(messageW + NSLocalizedString("loginViewErrorMessagePasswordAttach", comment: "password"), duration: 2.0, position: .bottom)
                }
            }
        }
    }
    
    func handleDismissViewProperly(){
        print("red elbow")
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func handleForgotPassword(){
        let daytimeChoice = ChangePasswordViewController()
        daytimeChoice.modalPresentationStyle = .overCurrentContext
        self.present(daytimeChoice, animated: true, completion: nil)
    }
    
    @objc func emailtextFieldDidChange(){
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.passwordSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.emailHiddenPlaceHolder.isHidden = false
        self.passwordHiddenPlaceHolder.isHidden = true
        
    }
    
    @objc func passwordtextFieldDidChange(){
        self.passwordSeperatorView.backgroundColor = UIColor.black
        self.emailSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.emailHiddenPlaceHolder.isHidden = true
        self.passwordHiddenPlaceHolder.isHidden = false
    }
    
    
    //handle createloggedIn details
    @objc func handleCreateUserData(expiresInCheck: String, sessionPeriodEndString: String, customerId: String, tokenData: String, timezone: String, shopOwner: String, authType: String, email: String){
        if expiresInCheck == "7d" {
            
            let teams = [CustomerToken(expiresIn: sessionPeriodEndString, customerId: customerId, token: "Bearer \(tokenData)", timezone: timezone, shopOwner: shopOwner, authType: authType, email: email, autoShopSwitch: "NO")]
            
            let userDefaults = UserDefaults.standard
            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: teams)
            userDefaults.set(encodedData, forKey: "token")
            userDefaults.synchronize()
            
            self.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: .didReceiveDataWelcomeViewController, object: nil)
            })
        }
    }
    
    func setupInputContainerView(){
        
        var topDistance: CGFloat = 20
        // var bottomDistance: CGFloat = -10
        if UIDevice.current.hasNotch {
            topDistance = 60
            // bottomDistance = -24
        }
        
        upperInputsContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(loginViewCloseViewButton)
        upperInputsContainerView.addSubview(loginViewTitlePlaceHolder)
        
        loginViewCloseViewButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        loginViewCloseViewButton.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        loginViewCloseViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        loginViewCloseViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        loginViewTitlePlaceHolder.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        loginViewTitlePlaceHolder.rightAnchor.constraint(equalTo: loginViewCloseViewButton.leftAnchor).isActive = true
        loginViewTitlePlaceHolder.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor, constant: 10).isActive = true
        loginViewTitlePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        inputsContainerView.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 10).isActive = true
        inputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        inputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        inputsContainerView.heightAnchor.constraint(equalToConstant: 90).isActive = true
        
        
        inputsContainerView.addSubview(emailHiddenPlaceHolder)
        inputsContainerView.addSubview(emailTextField)
        inputsContainerView.addSubview(emailSeperatorView)
        inputsContainerView.addSubview(passwordHiddenPlaceHolder)
        inputsContainerView.addSubview(passwordTextField)
        inputsContainerView.addSubview(showPassordTextFieldButton)
        inputsContainerView.addSubview(passwordSeperatorView)
        
        emailHiddenPlaceHolder.topAnchor.constraint(equalTo: inputsContainerView.topAnchor).isActive = true
        emailHiddenPlaceHolder.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        emailHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        emailHiddenPlaceHolder.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        emailTextField.topAnchor.constraint(equalTo: emailHiddenPlaceHolder.bottomAnchor).isActive = true
        emailTextField.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        emailSeperatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        emailSeperatorView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor).isActive = true
        emailSeperatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        emailSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        passwordHiddenPlaceHolder.topAnchor.constraint(equalTo: emailSeperatorView.bottomAnchor, constant: 5).isActive = true
        passwordHiddenPlaceHolder.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        passwordHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        passwordHiddenPlaceHolder.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        passwordTextField.topAnchor.constraint(equalTo: passwordHiddenPlaceHolder.bottomAnchor).isActive = true
        passwordTextField.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        passwordTextField.rightAnchor.constraint(equalTo: inputsContainerView.rightAnchor, constant: -20).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        showPassordTextFieldButton.bottomAnchor.constraint(equalTo: passwordTextField.bottomAnchor).isActive = true
        showPassordTextFieldButton.rightAnchor.constraint(equalTo: passwordTextField.rightAnchor).isActive = true
        showPassordTextFieldButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        passwordSeperatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        passwordSeperatorView.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor).isActive = true
        passwordSeperatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        passwordSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
    }
    
    func setupButtons(){
        signInButton.topAnchor.constraint(equalTo: inputsContainerView.bottomAnchor, constant: 10).isActive = true
        signInButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        signInButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        signInButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        forgotPasswordButton.topAnchor.constraint(equalTo: signInButton.bottomAnchor, constant: 10).isActive = true
        forgotPasswordButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        forgotPasswordButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -12).isActive = true
        forgotPasswordButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        errorMessagePlaceHolder.topAnchor.constraint(equalTo: forgotPasswordButton.bottomAnchor, constant: 10).isActive = true
        errorMessagePlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        errorMessagePlaceHolder.heightAnchor.constraint(equalToConstant: 40).isActive = true
        errorMessagePlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
    }

}
