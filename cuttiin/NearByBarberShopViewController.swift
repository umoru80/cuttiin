//
//  NearByBarberShopViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/29/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import MGStarRatingView
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftDate
import SwiftSpinner
import MapKit
import CoreLocation

class NearByBarberShopViewController: UIViewController, CLLocationManagerDelegate {
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    var barberShopViewController = BarberShopsViewController()
    let locationManager = CLLocationManager()
    
    var shopsDataArray = [MarkerforMap]()
    var shopsDataArraySecond = [MarkerforMap]()
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    lazy var shopSearchViewIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8shopsearchlogo")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var shopSearchCloseViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(exitThisView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let shopSearchPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = "Shop search"
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 20)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    
    lazy var searchAreaContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    lazy var searchLogoViewIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8searchplaceholder")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let cancelButton: UIButton = {
        let clButton = UIButton()
        clButton.translatesAutoresizingMaskIntoConstraints = false
        clButton.setTitle("cancel", for: .normal)
        clButton.backgroundColor = UIColor.white
        clButton.setTitleColor(UIColor.black, for: .normal)
        clButton.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 14)
        clButton.addTarget(self, action: #selector(handleClearTextField), for: .touchUpInside)
        clButton.isEnabled = true
        return clButton
    }()
    
    let searchBarTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 14)
        em.placeholder = "search"
        em.addTarget(self, action: #selector(handleTextSearchByCharacter(_:)), for: .editingDidBegin)
        em.addTarget(self, action: #selector(handleTextSearchByCharacter(_:)), for: .editingChanged)
        return em
    }()
    
    let curvedCollectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        cview.layer.masksToBounds = true
        cview.layer.cornerRadius = 5
        return cview
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customNearByTopRatedCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        view.backgroundColor = UIColor.white
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        view.addSubview(searchAreaContainerView)
        view.addSubview(curvedCollectView)
        
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjectContriants()
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        locationManager.stopUpdatingLocation()
        let deviceLocValueForDistance: CLLocation = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        self.reverseDeviceLocaleByLocData(locationData: deviceLocValueForDistance)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @objc private func handleClearTextField(){
        self.searchBarTextField.text = "";
        self.shopsDataArray.removeAll()
        self.shopsDataArray = self.shopsDataArraySecond
        //call to get new data
    }
    
    @objc private func handleTextSearchByCharacter(_ sender: UITextField){
        if let characters = sender.text {
            let lowercaseText = characters.lowercased()
            if lowercaseText.count > 0 {
                if shopsDataArray.count < 1 {
                    self.shopsDataArray = self.shopsDataArraySecond
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
                for single in shopsDataArray {
                    if let index = self.shopsDataArray.firstIndex(where: {$0.shopId == single.shopId}) {
                        let fullStringText = single.shopName.lowercased() + " " + single.shopAddress.lowercased()
                        if !fullStringText.contains(lowercaseText) {
                            self.shopsDataArray.remove(at: index)
                            DispatchQueue.main.async {
                                self.collectionView.reloadData()
                            }
                        }
                    }
                    
                }
            } else {
                if shopsDataArray.count < 1 {
                    self.shopsDataArray = self.shopsDataArraySecond
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            }
        }
        
    }
    
    @objc private func exitThisView(){
        self.dismiss(animated: true) {
            let someDict = ["shopUniqueId" : "refresh view please" ]
            NotificationCenter.default.post(name: .didReceiveDataMapViewRefresher, object: nil, userInfo: someDict)
        }
    }
    
    func reverseDeviceLocaleByLocData(locationData: CLLocation){
        let longitude = locationData.coordinate.longitude.description
        let latitude = locationData.coordinate.latitude.description
        let placesApiKey = "AIzaSyCzQUcGTvZo3ZAfDg4SDZWEOvm2mV6Co-Y"
        
        
        AF.request("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&key=" + placesApiKey)
            .validate(statusCode: 200..<500)
            .validate(contentType: ["application/json"])
            .response { response in
                
                if let error = response.error {
                    print("zones", error.localizedDescription)
                    return
                }
                
                var countryName = ""
                if let jsonData = response.data {
                    do {
                        let addressData = try JSONDecoder().decode(GooglePlaceAddressData.self, from: jsonData)
                        let counterVal = addressData.results.count
                        var counterAddCheck = 0
                        for addComp in addressData.results {
                            counterAddCheck = counterAddCheck + 1
                            
                            if let types = addComp.types.first {
                                
                                if types == "country" {
                                    if let countryNames = addComp.addressComponents[safe: 0]?.shortName {
                                        countryName = countryNames
                                    }
                                }
                                
                            }
                            
                            if (counterAddCheck == counterVal) {
                                let countryCodeID = countryName
                                // self.getAllMarkers(deviceLocation: deviceLocation, countryCode: countryCodeID)
                                self.getAllShopData(deviceLocation: locationData, countryCode: countryCodeID)
                            }
                            
                        }
                        
                    } catch _ {
                        print("An error occured")
                    }
                    
                }
        }
    }
    
    private func getAllShopData(deviceLocation: CLLocation, countryCode: String){
        self.shopsDataArray.removeAll()
        self.shopsDataArraySecond.removeAll()
        DispatchQueue.global(qos: .background).async {
            
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        //handle sesssion expiration
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        AF.request(self.BACKEND_URL + "getallshopsByCountryCode/" + countryCode, method: .post, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"])
                            .response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    print("got data back")
                                    do {
                                        let shopArrayData = try JSONDecoder().decode([Shop].self, from: jsonData)
                                        print(shopArrayData.count)
                                        for shop in shopArrayData {
                                            var ratingIneter = 0
                                            if let val = Int(shop.shopRating) {
                                                ratingIneter = val
                                            }
                                            
                                            if let image = UIImage(named: "icon_shop"), let addLong = Double(shop.shopAddressLongitude), let addLati = Double(shop.shopAddressLatitude)  {
                                                
                                                let locValue:CLLocationCoordinate2D = CLLocationCoordinate2DMake(addLati, addLong)
                                                let locValueForDistance: CLLocation = CLLocation(latitude: addLati, longitude: addLong)
                                                let shopDistanceFromDevice = deviceLocation.distance(from: locValueForDistance)
                                                
                                                let singleMapMarker = MarkerforMap(shopId: shop.id, shopName: shop.shopName, shopAddress: shop.shopAddress, shopCordinates: locValue, shopGenderCategory: shop.shopCustomerGender, locationCLL: locValueForDistance, longitudeDouble: addLong, latitudeDouble: addLati, shopLogoImage: image, rating: ratingIneter, shopCurrency: shop.shopCountryCurrencyCode, shopVerification: shop.shopVerificationStatus, shopCategory: shop.shopCategory, shopDistanceFromDevice: shopDistanceFromDevice)
                                                self.shopsDataArray.append(singleMapMarker)
                                                self.shopsDataArraySecond.append(singleMapMarker)
                                                self.shopsDataArray.sort { (first, second) -> Bool in
                                                    first.shopDistanceFromDevice < second.shopDistanceFromDevice
                                                }
                                                self.shopsDataArraySecond.sort { (first, second) -> Bool in
                                                    first.shopDistanceFromDevice < second.shopDistanceFromDevice
                                                }
                                                
                                                DispatchQueue.main.async {
                                                    self.collectionView.reloadData()
                                                }
                                            }
                                        }
                                        
                                    } catch _ {
                                        
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                self.view.makeToast("No Shops available in your country", duration: 2.0, position: .bottom)
                                            case 500:
                                                self.view.makeToast("An error occurred, please try again a little later", duration: 2.0, position: .bottom)
                                            default:
                                                print("sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
    }
    
    @objc func getCategoryDanishTranslation(category: String) -> String {
        switch category {
        case "Barbershop":
            return NSLocalizedString("barberShopViewCategoryBarbershop", comment: "Barbershop")
        case "Hair salon":
            return NSLocalizedString("barberShopViewCategoryHairSalon", comment: "Frisør")
        case "Nail salon":
            return NSLocalizedString("barberShopViewCategoryNailSalon", comment: "Negle salon")
        case "Spa":
            return NSLocalizedString("barberShopViewCategorySpa", comment: "Spa")
        case "Tattoo":
            return NSLocalizedString("barberShopViewCategoryTattoo", comment: "Tatovering")
        case "Massage":
            return NSLocalizedString("barberShopViewCategoryMasssage", comment: "Massage")
        case "Piercings":
            return NSLocalizedString("barberShopViewCategoryPiercing", comment: "Piercinger")
        case "Beauty salon":
            return NSLocalizedString("barberShopViewCategoryBeautySalon", comment: "skønhedssalon")
        default:
            return "Barbershop"
        }
    }
    
    func setupViewObjectContriants(){
        var topDistance: CGFloat = 20
        if UIDevice.current.hasNotch {
            topDistance = 60
        }
        
        upperInputsContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(shopSearchViewIcon)
        upperInputsContainerView.addSubview(shopSearchPlaceHolder)
        upperInputsContainerView.addSubview(shopSearchCloseViewButton)
        
        shopSearchViewIcon.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        shopSearchViewIcon.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor).isActive = true
        shopSearchViewIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        shopSearchViewIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        shopSearchCloseViewButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        shopSearchCloseViewButton.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        shopSearchCloseViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        shopSearchCloseViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        shopSearchPlaceHolder.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        shopSearchPlaceHolder.leftAnchor.constraint(equalTo: shopSearchViewIcon.rightAnchor, constant: 10).isActive = true
        shopSearchPlaceHolder.rightAnchor.constraint(equalTo: shopSearchCloseViewButton.leftAnchor).isActive = true
        shopSearchPlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        searchAreaContainerView.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 10).isActive = true
        searchAreaContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        searchAreaContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        searchAreaContainerView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        searchAreaContainerView.addSubview(searchLogoViewIcon)
        searchAreaContainerView.addSubview(cancelButton)
        searchAreaContainerView.addSubview(searchBarTextField)
        
        searchLogoViewIcon.topAnchor.constraint(equalTo: searchAreaContainerView.topAnchor).isActive = true
        searchLogoViewIcon.leftAnchor.constraint(equalTo: searchAreaContainerView.leftAnchor).isActive = true
        searchLogoViewIcon.widthAnchor.constraint(equalToConstant: 30).isActive = true
        searchLogoViewIcon.heightAnchor.constraint(equalTo: searchAreaContainerView.heightAnchor).isActive = true
        
        cancelButton.topAnchor.constraint(equalTo: searchAreaContainerView.topAnchor).isActive = true
        cancelButton.rightAnchor.constraint(equalTo: searchAreaContainerView.rightAnchor).isActive = true
        cancelButton.widthAnchor.constraint(equalTo: searchAreaContainerView.widthAnchor, multiplier: 0.25).isActive = true
        cancelButton.heightAnchor.constraint(equalTo: searchAreaContainerView.heightAnchor).isActive = true
        
        searchBarTextField.topAnchor.constraint(equalTo: searchAreaContainerView.topAnchor).isActive = true
        searchBarTextField.leftAnchor.constraint(equalTo: searchLogoViewIcon.rightAnchor, constant: 5).isActive = true
        searchBarTextField.rightAnchor.constraint(equalTo: cancelButton.leftAnchor).isActive = true
        searchBarTextField.heightAnchor.constraint(equalTo: searchAreaContainerView.heightAnchor).isActive = true
        
        
        
        curvedCollectView.topAnchor.constraint(equalTo: searchAreaContainerView.bottomAnchor, constant: 10).isActive = true
        curvedCollectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        curvedCollectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -24).isActive = true
        curvedCollectView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        curvedCollectView.addSubview(collectView)
        curvedCollectView.bringSubviewToFront(collectView)
        
        collectView.topAnchor.constraint(equalTo: curvedCollectView.topAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: curvedCollectView.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: curvedCollectView.widthAnchor, constant: -10).isActive = true
        collectView.bottomAnchor.constraint(equalTo: curvedCollectView.bottomAnchor, constant: -5).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
    }
}

class customNearByTopRatedCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let starView = StarRatingView()
    let attribute = StarRatingAttribute(type: .rate,point: 10,spacing: 5, emptyColor: .gray, fillColor: UIColor(r: 244, g: 222, b: 172))
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    lazy var shopNameLabelLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8smallshopname")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let barberShopNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 13)
        fnhp.textColor = UIColor.black
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var shopVerifiedLabelLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8idshopverified")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let barberShopVerifiedPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 13)
        fnhp.textColor = UIColor.black
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var shopAddressLabelLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8smalladdress")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let barberShopAddressPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 11)
        fnhp.textColor = UIColor.black
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let pusherView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    func setupViews(){
        addSubview(barberShopNamePlaceHolder)
        addSubview(shopNameLabelLogo)
        addSubview(shopVerifiedLabelLogo)
        addSubview(barberShopVerifiedPlaceHolder)
        addSubview(barberShopAddressPlaceHolder)
        addSubview(shopAddressLabelLogo)
        addSubview(starView)
        addSubview(pusherView)
        
        starView.configure(attribute, current: 0, max: 5)
        starView.translatesAutoresizingMaskIntoConstraints = false
        starView.isUserInteractionEnabled = false
        
        backgroundColor = UIColor.white
        layer.cornerRadius = 5
        layer.masksToBounds = true
        
        
        addContraintsWithFormat(format: "H:|-10-[v0(20)]-5-[v1]-5-|", views: shopNameLabelLogo, barberShopNamePlaceHolder)
        addContraintsWithFormat(format: "H:|-10-[v0(20)]-5-[v1]-5-|", views: shopVerifiedLabelLogo, barberShopVerifiedPlaceHolder)
        addContraintsWithFormat(format: "H:|-10-[v0(20)]-5-[v1]-5-|", views: shopAddressLabelLogo, barberShopAddressPlaceHolder)
        addContraintsWithFormat(format: "H:|-10-[v0(20)]-5-[v1]-5-|", views: pusherView, starView)
        addContraintsWithFormat(format: "V:|-5-[v0(20)]-5-[v1(20)]-5-[v2(20)]-5-[v3(20)]-5-|", views: shopNameLabelLogo, shopVerifiedLabelLogo, shopAddressLabelLogo, pusherView)
        addContraintsWithFormat(format: "V:|-5-[v0(20)]-5-[v1(20)]-5-[v2(40)]-5-[v3(20)]-5-|", views: barberShopNamePlaceHolder, barberShopVerifiedPlaceHolder, barberShopAddressPlaceHolder, starView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension NearByBarberShopViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.shopsDataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customNearByTopRatedCollectionViewCell
        cell.barberShopNamePlaceHolder.text = self.shopsDataArray[indexPath.row].shopName
        let distance = self.shopsDataArray[indexPath.row].shopDistanceFromDevice / 1000
        cell.barberShopAddressPlaceHolder.text = self.shopsDataArray[indexPath.row].shopAddress + " || " + String(distance.roundTo(places: 1)) + " km"
        let categoryTag = self.getCategoryDanishTranslation(category: shopsDataArray[indexPath.row].shopCategory)
        if (self.shopsDataArray[indexPath.row].shopVerification == "YES") {
            cell.barberShopVerifiedPlaceHolder.text =  categoryTag + " || " + NSLocalizedString("barberShopViewVerified", comment: "verified")
        } else {
            cell.barberShopVerifiedPlaceHolder.text = categoryTag + " || " + NSLocalizedString("barberShopViewNotVerified", comment: "not verified")
        }
        cell.starView.current = CGFloat(integerLiteral: shopsDataArray[indexPath.row].rating)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.dismiss(animated: true) {
            
            let someDict = ["shopUniqueId" : "refresh view please" ]
            NotificationCenter.default.post(name: .didReceiveDataMapViewRefresher, object: nil, userInfo: someDict)
            let latValue: CLLocationDegrees = CLLocationDegrees(self.shopsDataArray[indexPath.row].latitudeDouble)
            let longValue: CLLocationDegrees = CLLocationDegrees(self.shopsDataArray[indexPath.row].longitudeDouble)
            
            self.barberShopViewController.handleMoveCamera(latitude: latValue, longitude: longValue)
        }
        
    }
}
