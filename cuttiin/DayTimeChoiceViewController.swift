//
//  DayTimeChoiceViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/20/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate
import SwiftyJSON
import Alamofire
import SwiftSpinner

//store a list of opening and closing times
class DayTimeChoiceViewController: UIViewController, UITextFieldDelegate {
    var verifyTimeDifference = false
    var specificDayData: String?
    var originalDayData: String?
    var daysOfTheWeek = ["Monday", "Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
    var selectedDaysOfTheWeek = [String]()
    var selectedOpeningTimeForTheDay: String?
    var selectedClosingTimeForTheDay: String?
    var selectedDateCreated: String?
    var currentShopId: String?
    var currentWorkId: String?
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    lazy var DayTimeChoiceViewIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8timeschedule")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var DayTimeChoiceCloseViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(exitDayTimeView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let DayTimeChoicePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 20) // HelveticaNeue-Medium
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    let scrollView: UIScrollView = {
        let v = UIScrollView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.showsVerticalScrollIndicator = false
        v.isScrollEnabled = true
        v.backgroundColor = .clear
        return v
    }()
    
    let scrollViewInnerContaine: UIView = {
        let tpcview = UIView()
        tpcview.translatesAutoresizingMaskIntoConstraints = false
        return tpcview
    }()
    
    
    
    lazy var openCloseSegmentedControl: UISegmentedControl = {
        let ocsegmentcontrol = UISegmentedControl(items: [NSLocalizedString("openSegmentedButtonTextDayTimeView", comment: "Add Open Hours"), NSLocalizedString("closedSegmentedButtonTextDayTimeView", comment: "Delete Open hours")])
        ocsegmentcontrol.translatesAutoresizingMaskIntoConstraints = false
        ocsegmentcontrol.tintColor = UIColor(r: 220, g: 220, b: 220)
        ocsegmentcontrol.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: UIControl.State.normal)
        ocsegmentcontrol.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: UIControl.State.selected)
        ocsegmentcontrol.selectedSegmentIndex = 0
        ocsegmentcontrol.addTarget(self, action: #selector(handleLoginRegisterChange), for: .valueChanged)
        return ocsegmentcontrol
    }()
    
    let timePickerContainerView: UIView = {
        let tpcview = UIView()
        tpcview.translatesAutoresizingMaskIntoConstraints = false
        return tpcview
    }()
    
    let openingTimePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("openAtlabelTextDayTimeView", comment: "OPENS AT")
        fnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        fnhp.textColor = UIColor.black
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let closingTimePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("closedAtlabelTextDayTimeView", comment: "CLOSES AT")
        fnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        fnhp.textColor = UIColor.black
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var openingTimePicker: UIDatePicker = {
        let opentime = UIDatePicker()
        opentime.translatesAutoresizingMaskIntoConstraints = false
        opentime.datePickerMode = .time
        let currentDate = Date()
        opentime.date = currentDate
        opentime.addTarget(self, action: #selector(handleOpenTimePickerChange), for: .valueChanged)
        return opentime
    }()
    
    let openingAndClosingTimeSeperatorView: UIView = {
        let tpcview = UIView()
        tpcview.translatesAutoresizingMaskIntoConstraints = false
        tpcview.backgroundColor = UIColor.white
        return tpcview
    }()
    
    lazy var closingTimePicker: UIDatePicker = {
        let opentime = UIDatePicker()
        opentime.translatesAutoresizingMaskIntoConstraints = false
        opentime.datePickerMode = .time
        let currentDate = Date()
        opentime.date = currentDate + 1.hours
        opentime.addTarget(self, action: #selector(handleClosingTimePickerChange), for: .valueChanged)
        return opentime
    }()
    
    let applyToTextLabel: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("applyToLabelTextDayTimeView", comment: "Apply to")
        fnhp.font = UIFont(name: "OpenSans-Light", size: 13)
        fnhp.textColor = UIColor.black
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let applyToSeperatorView: UIView = {
        let tpcview = UIView()
        tpcview.translatesAutoresizingMaskIntoConstraints = false
        tpcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tpcview
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        cview.layer.cornerRadius = 5
        cview.layer.masksToBounds = true
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.allowsMultipleSelection = true
        cv.register(customCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()
    
    lazy var saveButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitle(NSLocalizedString("dayTimeChoiceViewControllerSaveButton", comment: "Save"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 20)
        st.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.addTarget(self, action: #selector(handleSaveTime(_:)), for: .touchUpInside)
        st.isEnabled = false
        return st
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        view.addSubview(scrollView)
        collectionView.dataSource = self
        collectionView.delegate = self 
        view.addSubview(saveButton)
        setupViewObjectsContriants()
        if let dayData = self.specificDayData {
            self.DayTimeChoicePlaceHolder.text = dayData
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @objc private func exitDayTimeView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupViewObjectsContriants() {
        var topDistance: CGFloat = 20
        if UIDevice.current.hasNotch {
            topDistance = 60
        }
        
        upperInputsContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(DayTimeChoiceViewIcon)
        upperInputsContainerView.addSubview(DayTimeChoicePlaceHolder)
        upperInputsContainerView.addSubview(DayTimeChoiceCloseViewButton)
        
        DayTimeChoiceViewIcon.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        DayTimeChoiceViewIcon.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor).isActive = true
        DayTimeChoiceViewIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        DayTimeChoiceViewIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        DayTimeChoiceCloseViewButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        DayTimeChoiceCloseViewButton.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        DayTimeChoiceCloseViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        DayTimeChoiceCloseViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        DayTimeChoicePlaceHolder.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        DayTimeChoicePlaceHolder.leftAnchor.constraint(equalTo: DayTimeChoiceViewIcon.rightAnchor, constant: 10).isActive = true
        DayTimeChoicePlaceHolder.rightAnchor.constraint(equalTo: DayTimeChoiceCloseViewButton.leftAnchor).isActive = true
        DayTimeChoicePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        
        scrollView.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 5).isActive = true
        scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -5).isActive = true
        
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 600) //528
        
        scrollView.addSubview(scrollViewInnerContaine)
        
        scrollViewInnerContaine.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        scrollViewInnerContaine.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        scrollViewInnerContaine.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        scrollViewInnerContaine.heightAnchor.constraint(equalToConstant: scrollView.contentSize.height).isActive = true
        
        
        scrollViewInnerContaine.addSubview(openCloseSegmentedControl)
        scrollViewInnerContaine.addSubview(timePickerContainerView)
        scrollViewInnerContaine.addSubview(applyToTextLabel)
        scrollViewInnerContaine.addSubview(applyToSeperatorView)
        scrollViewInnerContaine.addSubview(collectView)
        scrollViewInnerContaine.addSubview(saveButton)
        
        
        
        
        openCloseSegmentedControl.topAnchor.constraint(equalTo: scrollViewInnerContaine.topAnchor).isActive = true
        openCloseSegmentedControl.centerXAnchor.constraint(equalTo: scrollViewInnerContaine.centerXAnchor).isActive = true
        openCloseSegmentedControl.widthAnchor.constraint(equalTo: scrollViewInnerContaine.widthAnchor, constant: -48).isActive = true
        openCloseSegmentedControl.heightAnchor.constraint(equalToConstant: 50)
        
        timePickerContainerView.topAnchor.constraint(equalTo: openCloseSegmentedControl.bottomAnchor, constant: 10).isActive = true
        timePickerContainerView.centerXAnchor.constraint(equalTo: scrollViewInnerContaine.centerXAnchor).isActive = true
        timePickerContainerView.widthAnchor.constraint(equalTo: scrollViewInnerContaine.widthAnchor, constant: -12).isActive = true
        timePickerContainerView.heightAnchor.constraint(equalToConstant: 170).isActive = true
        
        timePickerContainerView.addSubview(openingTimePlaceHolder)
        timePickerContainerView.addSubview(closingTimePlaceHolder)
        timePickerContainerView.addSubview(openingTimePicker)
        timePickerContainerView.addSubview(openingAndClosingTimeSeperatorView)
        timePickerContainerView.addSubview(closingTimePicker)
        
        openingTimePlaceHolder.topAnchor.constraint(equalTo: timePickerContainerView.topAnchor).isActive = true
        openingTimePlaceHolder.leftAnchor.constraint(equalTo: timePickerContainerView.leftAnchor).isActive = true
        openingTimePlaceHolder.widthAnchor.constraint(equalTo: timePickerContainerView.widthAnchor, multiplier: 0.5).isActive = true
        openingTimePlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        closingTimePlaceHolder.topAnchor.constraint(equalTo: timePickerContainerView.topAnchor).isActive = true
        closingTimePlaceHolder.leftAnchor.constraint(equalTo: openingTimePlaceHolder.rightAnchor).isActive = true
        closingTimePlaceHolder.widthAnchor.constraint(equalTo: timePickerContainerView.widthAnchor, multiplier: 0.5).isActive = true
        closingTimePlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        openingTimePicker.topAnchor.constraint(equalTo: timePickerContainerView.topAnchor, constant: 15).isActive = true
        openingTimePicker.leftAnchor.constraint(equalTo: timePickerContainerView.leftAnchor).isActive = true
        openingTimePicker.widthAnchor.constraint(equalTo: timePickerContainerView.widthAnchor, multiplier: 0.5, constant: -10).isActive = true
        openingTimePicker.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        openingAndClosingTimeSeperatorView.topAnchor.constraint(equalTo: openingTimePlaceHolder.topAnchor, constant: 90).isActive = true
        openingAndClosingTimeSeperatorView.leftAnchor.constraint(equalTo: openingTimePicker.rightAnchor).isActive = true
        openingAndClosingTimeSeperatorView.rightAnchor.constraint(equalTo: closingTimePicker.leftAnchor).isActive = true
        openingAndClosingTimeSeperatorView.widthAnchor.constraint(equalToConstant: 10) .isActive = true
        openingAndClosingTimeSeperatorView.heightAnchor.constraint(equalToConstant: 3).isActive = true
        
        closingTimePicker.topAnchor.constraint(equalTo: timePickerContainerView.topAnchor, constant: 15).isActive = true
        closingTimePicker.rightAnchor.constraint(equalTo: timePickerContainerView.rightAnchor).isActive = true
        closingTimePicker.widthAnchor.constraint(equalTo: timePickerContainerView.widthAnchor, multiplier: 0.5, constant: -10).isActive = true
        closingTimePicker.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        applyToTextLabel.topAnchor.constraint(equalTo: timePickerContainerView.bottomAnchor, constant: 5).isActive = true
        applyToTextLabel.centerXAnchor.constraint(equalTo: scrollViewInnerContaine.centerXAnchor).isActive = true
        applyToTextLabel.widthAnchor.constraint(equalTo: scrollViewInnerContaine.widthAnchor, constant: -48).isActive = true
        applyToTextLabel.heightAnchor.constraint(equalToConstant: 20)
        
        applyToSeperatorView.leftAnchor.constraint(equalTo: scrollViewInnerContaine.leftAnchor, constant: 24).isActive = true
        applyToSeperatorView.topAnchor.constraint(equalTo: applyToTextLabel.bottomAnchor).isActive = true
        applyToSeperatorView.widthAnchor.constraint(equalTo: applyToTextLabel.widthAnchor).isActive = true
        applyToSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        collectView.topAnchor.constraint(equalTo: applyToSeperatorView.bottomAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: scrollViewInnerContaine.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: scrollViewInnerContaine.widthAnchor, multiplier: 1, constant: -48).isActive = true
        collectView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor, constant: 5).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor, constant: -10).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor, constant: -10).isActive = true
        
        saveButton.topAnchor.constraint(equalTo: collectionView.bottomAnchor, constant: 10).isActive = true
        saveButton.centerXAnchor.constraint(equalTo: scrollViewInnerContaine.centerXAnchor).isActive = true
        saveButton.widthAnchor.constraint(equalTo: scrollViewInnerContaine.widthAnchor, constant: -48).isActive = true
        saveButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        /*DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            if let originalDay = self.originalDayData, let id = self.daysOfTheWeek.index(of: originalDay) {
                let index = IndexPath(item: id, section: 0)
                self.collectionView.scrollToItem(at: index, at: .top, animated: true)
                self.collectionView.selectItem(at: index, animated: false, scrollPosition: UICollectionView.ScrollPosition.top)
                if let selectedCell = self.collectionView.cellForItem(at: index) as? customCollectionViewCell  {
                    selectedCell.thumbnailImageView.image = UIImage(named: "check_box_active")
                    
                    if (!self.selectedDaysOfTheWeek.contains(originalDay)) {
                        self.selectedDaysOfTheWeek.append(originalDay)
                    }
                } else {
                    print("zoom away")
                }
            }
        }*/
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return string == numberFiltered
    }
    
    @objc func handleSaveTime(_ sender: UIButton){
        sender.pulsate()
        
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    
                    let newDate = DateInRegion().toFormat("yyyy-MM-dd HH:mm:ss")
                    self.selectedDateCreated = newDate
                    let userTimezone = DateByUserDeviceInitializer.tzone
                    let userCalender = "gregorian"
                    let userLocal = "en"
                    
                    
                    let englishDateRegion = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: userLocal)
                    let newStrDate = DateInRegion().convertTo(region: englishDateRegion).toFormat("yyyy-MM-dd HH:mm:ss")
                    
                    if self.openCloseSegmentedControl.selectedSegmentIndex == 0 {
                        if let selectedChosenDay = self.originalDayData, let openDate = self.selectedOpeningTimeForTheDay, let closeDate = self.selectedClosingTimeForTheDay, let shopId = self.currentShopId {
                            
                            if !self.selectedDaysOfTheWeek.contains(selectedChosenDay) {
                                self.selectedDaysOfTheWeek.append(selectedChosenDay)
                            }
                            
                            if self.selectedDaysOfTheWeek.count > 0 {
                                let length = self.selectedDaysOfTheWeek.count
                                var counter = 0
                                var workScheduleArray = [Parameters]()
                                for daySelec in selectedDaysOfTheWeek {
                                    counter = counter + 1
                                    let workSchedule: Parameters = ["workhourWeekDay": daySelec,
                                                                    "workhourOpenDate": openDate,
                                                                    "workhourCloseDate": closeDate]
                                    workScheduleArray.append(workSchedule)
                                    
                                    if counter == length && workScheduleArray.count == self.selectedDaysOfTheWeek.count {
                                        let workModel: Parameters = [
                                            "workhourSchedule": workScheduleArray,
                                            "workhourLastUpdated": newStrDate,
                                            "workhourTimezone": userTimezone,
                                            "workhourCalendar": userCalender,
                                            "workhourLocale": userLocal
                                        ]
                                        
                                        let headers: HTTPHeaders = [
                                            "authorization": token
                                        ]
                                        
                                        SwiftSpinner.show("Loading...")
                                        
                                        DispatchQueue.global(qos: .background).async {
                                            AF.request(self.BACKEND_URL + "workhour/" + shopId, method: .post, parameters: workModel, encoding: JSONEncoding(options: []), headers: headers)
                                                .validate(statusCode: 200..<501)
                                                .validate(contentType: ["application/json"])
                                                .response { (responseData) in
                                                    
                                                    
                                                    if let _ = responseData.error {
                                                        let message = "Error: please try again later"
                                                        self.view.makeToast(message, duration: 3.0, position: .bottom)
                                                        return
                                                    }
                                                    
                                                    if let jsonData = responseData.data {
                                                        do {
                                                            DispatchQueue.main.async {
                                                                SwiftSpinner.hide()
                                                            }
                                                            let _ = try JSONDecoder().decode(ServerMessage.self, from: jsonData)
                                                            UserDefaults.standard.set(self.selectedDaysOfTheWeek, forKey: "theOpeningAndClosingTimeWasSet")
                                                            UserDefaults.standard.set(self.daysOfTheWeek, forKey: "workHoursDeleted")
                                                            UserDefaults.standard.set(self.selectedOpeningTimeForTheDay, forKey: "theOpeningTimeWasSet")
                                                            UserDefaults.standard.set(self.selectedClosingTimeForTheDay, forKey: "theClosingTimeWasSet")
                                                            
                                                            let shopId = shopId
                                                            let someDict = ["shopUniqueId" : shopId ]
                                                            NotificationCenter.default.post(name: .didReceiveDataOpeningHours, object: nil, userInfo: someDict)
                                                            self.dismiss(animated: true, completion: nil)
                                                            
                                                        } catch _ {
                                                            
                                                            DispatchQueue.main.async {
                                                                SwiftSpinner.hide()
                                                            }
                                                            
                                                            if let statusCode = responseData.response?.statusCode {
                                                                switch statusCode {
                                                                case 404:
                                                                    self.view.makeToast(NSLocalizedString("dayTimeChoiceViewControllerErrorMessageNotCreated", comment: "Workhour could not be created"), duration: 2.0, position: .bottom)
                                                                case 409:
                                                                    self.view.makeToast(NSLocalizedString("dayTimeChoiceViewControllerErrorMessageAlreadyExist", comment: "Workhour already exist for this shop"), duration: 2.0, position: .bottom)
                                                                    
                                                                case 500:
                                                                    self.view.makeToast(NSLocalizedString("dayTimeChoiceViewControllerErrorMessageErrorOccured", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                                                default:
                                                                    print("sky high")
                                                                }
                                                            }
                                                        }
                                                    }
                                            }
                                        }
                                    }
                                    
                                }
                                
                            }
                        } else {
                            DispatchQueue.main.async {
                                SwiftSpinner.hide()
                            }
                        }
                        
                    }
                    
                }
            }
        }
    }
    
    @objc func handleOpenTimePickerChange(sender: UIDatePicker){
        self.closingTimePicker.minimumDate = sender.date
        self.getTimeDifference()
    }
    
    @objc func handleClosingTimePickerChange(sender: UIDatePicker){
        self.openingTimePicker.maximumDate = sender.date
        self.getTimeDifference()
    }
    
    func getTimeDifference(){
        let openTimeDate = self.openingTimePicker.date
        let closeTimeDate = self.closingTimePicker.date
        
        let userTimezone = DateByUserDeviceInitializer.tzone
        let userCalender = "gregorian"
        let userLocale = "en"
        
        let regionForDate = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: userLocale)
        
        let openDate = openTimeDate.in(region: regionForDate)
        let openDateString = openDate.toFormat("yyyy-MM-dd HH:mm:ss")
        let closeDate = closeTimeDate.in(region: regionForDate)
        let closeDateString = closeDate.toFormat("yyyy-MM-dd HH:mm:ss")
        
        if closeDate.isAfterDate(openDate, granularity: .hour) {
            self.verifyTimeDifference = true
            self.selectedOpeningTimeForTheDay = openDateString
            self.selectedClosingTimeForTheDay = closeDateString
            
            self.saveButton.isEnabled = true
            
        }else {
            self.view.makeToast(NSLocalizedString("dayTimeChoiceViewControllerErrorMessageTimeTooClose", comment: "Opening and Closing times are to close"), duration: 2.0, position: .bottom)
            self.verifyTimeDifference = false
            saveButton.isEnabled = false
            
        }
        
    }
    
    @objc func handleLoginRegisterChange(){
        if openCloseSegmentedControl.selectedSegmentIndex == 1 {
            self.openingTimePicker.isUserInteractionEnabled = false
            self.closingTimePicker.isUserInteractionEnabled = false
            self.collectionView.isUserInteractionEnabled = false
            saveButton.isEnabled = true
            
            self.saveButton.removeTarget(nil, action: nil, for: .allEvents)
            saveButton.addTarget(self, action: #selector(deleteSelectedWorkHourOnSpecificDay), for: .touchUpInside)
            
        }else {
            self.openingTimePicker.isUserInteractionEnabled = true
            self.closingTimePicker.isUserInteractionEnabled = true
            self.collectionView.isUserInteractionEnabled = true
            saveButton.isEnabled = false
            self.saveButton.removeTarget(nil, action: nil, for: .allEvents)
            self.saveButton.addTarget(self, action: #selector(handleSaveTime), for: .touchUpInside)
            self.saveButton.layer.shadowColor = UIColor.white.cgColor
            self.getTimeDifference()
        }
    }
    
    //removes the single day selected from the workhour schedule
    @objc func deleteSelectedWorkHourOnSpecificDay(){
        if let dayDay = self.originalDayData, let workID = self.currentWorkId {
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        let workDay: Parameters = [
                            "workDay": dayDay
                        ]
                        
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        SwiftSpinner.show("Loading...")
                        
                        DispatchQueue.global(qos: .background).async {
                            AF.request(self.BACKEND_URL + "deleteworkhour/" + workID, method: .post, parameters: workDay, headers: headers)
                                .validate(statusCode: 200..<501)
                                .validate(contentType: ["application/json"])
                                .response(completionHandler: { (response) in
                                    if let jsonData = response.data {
                                        do {
                                            DispatchQueue.main.async {
                                                SwiftSpinner.hide()
                                            }
                                            let _ = try JSONDecoder().decode(ServerMessage.self, from: jsonData)
                                            UserDefaults.standard.set(self.selectedDaysOfTheWeek, forKey: "theOpeningAndClosingTimeWasSet")
                                            UserDefaults.standard.set(self.daysOfTheWeek, forKey: "workHoursDeleted")
                                            UserDefaults.standard.set(self.selectedOpeningTimeForTheDay, forKey: "theOpeningTimeWasSet")
                                            UserDefaults.standard.set(self.selectedClosingTimeForTheDay, forKey: "theClosingTimeWasSet")
                                            
                                            if let shopId = self.currentShopId {
                                                let someDict = ["shopUniqueId" : shopId ]
                                                NotificationCenter.default.post(name: .didReceiveDataOpeningHours, object: nil, userInfo: someDict)
                                                self.dismiss(animated: true, completion: nil)
                                            }
                                            
                                        } catch _ {
                                            DispatchQueue.main.async {
                                                SwiftSpinner.hide()
                                            }
                                            
                                            if let statusCode = response.response?.statusCode {
                                                switch statusCode {
                                                case 404:
                                                    self.view.makeToast(NSLocalizedString("dayTimeChoiceViewControllerErrorMessageNotDeleted", comment: "Workhour workday could not be deleted"), duration: 2.0, position: .bottom)
                                                case 500:
                                                    self.view.makeToast(NSLocalizedString("dayTimeChoiceViewControllerErrorMessageErrorOccured", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                                default:
                                                    print("sky high")
                                                }
                                            }
                                        }
                                    }
                                })
                        }
                    }
                }
            }
        }
    }

}

class customCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let thumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.white
        tniv.contentMode = .scaleAspectFit
        tniv.image = UIImage(named: "check_box_inactive")
        return tniv
    }()
    
    let dayOfTheWeek: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.text = "Hello"
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    func setupViews(){
        addSubview(dayOfTheWeek)
        addSubview(thumbnailImageView)
        addSubview(seperatorView)
        
        backgroundColor = UIColor.white
        layer.masksToBounds = true
        layer.cornerRadius = 5
        
        addContraintsWithFormat(format: "H:|-16-[v0]-16-[v1(20)]-16-|", views: dayOfTheWeek, thumbnailImageView)
        addContraintsWithFormat(format: "V:|-16-[v0(20)]-16-[v1(1)]|", views: dayOfTheWeek,seperatorView)
        addContraintsWithFormat(format: "V:|-16-[v0(20)]-16-[v1(1)]|", views: thumbnailImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
