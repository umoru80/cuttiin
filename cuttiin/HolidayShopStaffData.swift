//
//  HolidayShopStaffData.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 12/05/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation
import SwiftDate

struct HolidayShopStaffData {
    var id: String
    var holidayStartDate: String
    var holidayStartDateObject: DateInRegion
    var holidayEndDate: String
    var holidayEndDateObject: DateInRegion
    var holidayDescription: String
    var holidayDateTimezone: String
    var holidayDateCalendar: String
    var holidayDateLocale: String
    var holidayDesignation: String
    var holidayShopId: String
    var holidayActive: String
    var staffName: String
    var holidayTitle: String
    
    init(id: String, holidayStartDate: String,  holidayStartDateObject: DateInRegion, holidayEndDate: String, holidayEndDateObject: DateInRegion, holidayDescription: String, holidayDateTimezone: String, holidayDateCalendar: String, holidayDateLocale: String, holidayDesignation: String, holidayShopId: String, holidayActive: String, staffName: String, holidayTitle: String) {
        
        self.id = id
        self.holidayStartDate = holidayStartDate
        self.holidayStartDateObject = holidayStartDateObject
        self.holidayEndDate = holidayEndDate
        self.holidayEndDateObject = holidayEndDateObject
        self.holidayDescription = holidayDescription
        self.holidayDateTimezone = holidayDateTimezone
        self.holidayDateCalendar = holidayDateCalendar
        self.holidayDateLocale = holidayDateLocale
        self.holidayDesignation = holidayDesignation
        self.holidayShopId = holidayShopId
        self.holidayActive = holidayActive
        self.staffName = staffName
        self.holidayTitle = holidayTitle
        
    }
}
