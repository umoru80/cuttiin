//
//  ShopBillingViewController.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 30/06/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit
import SwiftDate
import Alamofire
import Toast_Swift

class ShopBillingViewController: UIViewController {
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    var shopUniqueID: String?
    
    var shopShortUniqueIDD: String?
    
    var shopBillingsDataArrayData = [ShopBillingData]()
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    lazy var shopBillingShopPicker: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8shoppicker")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(shopBillingShopSelectorView)))
        return imageView
    }()
    
    lazy var shopBillingCloseViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(shopBillingExitThisView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let shopBillingViewTitlePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("shopBillingViewControllerTitle", comment: "Shop billing")
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 20)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    let shopBillingNameTitlePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("shopBillingViewControllerShopName", comment: "Shop name")
        fnhp.font = UIFont(name: "OpenSans-Light", size: 17)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let shopUniqueIDTitlePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 17)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let curvedCollectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        cview.layer.masksToBounds = true
        cview.layer.cornerRadius = 5
        return cview
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.alwaysBounceVertical = true
        cv.backgroundColor = UIColor.clear
        cv.register(shopBillingCollectionViewCell.self, forCellWithReuseIdentifier: "cellBill")
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        view.addSubview(shopBillingNameTitlePlaceHolder)
        view.addSubview(shopUniqueIDTitlePlaceHolder)
        view.addSubview(curvedCollectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewConstriants()
        getShopDetails()
        getAllShopBilling()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .didReceiveDataShopBillingView, object: nil)
        
    }
    
    @objc private func onDidReceiveData(_ notification:Notification) {
        if let dataBack = notification.userInfo as? [String: AnyObject], let shopId = dataBack["shopUniqueId"] as? String {
            self.shopUniqueID = shopId
            
            self.shopBillingsDataArrayData.removeAll()
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            getShopDetails()
            getAllShopBilling()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @objc private func shopBillingShopSelectorView(){
        let customerShops = CustomerShopsViewController()
        customerShops.shopBillingViewController = self
        customerShops.viewControllerWhoInitiatedAction = "shopBilling"
        customerShops.modalPresentationStyle = .overCurrentContext
        present(customerShops, animated: true, completion: nil)
    }
    
    private func getShopDetails(){
        DispatchQueue.global(qos: .background).async {
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let shopUniqueID = self.shopUniqueID {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        AF.request(self.BACKEND_URL + "getsingleshop/" + shopUniqueID, method: .post, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    print("got data back")
                                    do {
                                        let shopData = try JSONDecoder().decode(Shop.self, from: jsonData)
                                        DispatchQueue.main.async {
                                            self.shopBillingNameTitlePlaceHolder.text = shopData.shopName
                                            self.shopUniqueIDTitlePlaceHolder.text = shopData.shortUniqueID
                                            self.shopShortUniqueIDD = shopData.shortUniqueID
                                        }
                                        
                                    } catch _ {
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                self.view.makeToast(NSLocalizedString("shopBillingViewControllerErrorMessageShopNotFound", comment: "Shop not found"), duration: 2.0, position: .bottom)
                                                self.shopBillingNameTitlePlaceHolder.text = ""
                                                self.shopUniqueIDTitlePlaceHolder.text = ""
                                                self.shopShortUniqueIDD = ""
                                            case 500:
                                                self.view.makeToast(NSLocalizedString("shopBillingViewControllerErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                            default:
                                                print("sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
    }
    
    private func getAllShopBilling(){
        DispatchQueue.global(qos: .background).async {
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let shopId = self.shopUniqueID {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        //handle sesssion expiration
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        AF.request(self.BACKEND_URL + "getAllShopBilling/" + shopId, method: .post, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"])
                            .response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    do {
                                        let billArrayData = try JSONDecoder().decode(ShopBillingRequest.self, from: jsonData)
                                        let shopBillingsDataAray = billArrayData.shopBilling
                                        
                                        for singleBill in shopBillingsDataAray {
                                            let regionDataShop = DateByUserDeviceInitializer.getRegion(TZoneName: singleBill.dateCreatedTimezone, calenName: singleBill.dateCreatedCalendar, LocName: singleBill.dateCreatedLocale)
                                            
                                            if let startBilling = singleBill.billingStarted.toDate("yyyy-MM-dd HH:mm:ss", region: regionDataShop), let endBilling = singleBill.billingEnded.toDate("yyyy-MM-dd HH:mm:ss", region: regionDataShop), let dateCreated =  singleBill.dateCreated.toDate("yyyy-MM-dd HH:mm:ss", region: regionDataShop){
                                                
                                                let billDataSingle = ShopBillingData(id: singleBill.id, customer: singleBill.customer, shop: singleBill.shop, shopCountryCurrencyCode: singleBill.shopCountryCurrencyCode, billingStatus: singleBill.billingStatus, billingPaidDate: singleBill.billingPaidDate, billingPaidDateTimezone: singleBill.billingPaidDateTimezone, billingPaidDateCalendar: singleBill.billingPaidDateCalendar, billingPaidDateLocale: singleBill.billingPaidDateLocale, appointmentNewTotalCost: singleBill.appointmentNewTotalCost, appointmentNewTotalAmount: singleBill.appointmentNewTotalAmount, appointmentStartedTotalCost: singleBill.appointmentStartedTotalCost, appointmentStartedTotalAmount: singleBill.appointmentStartedTotalAmount, appointmentEndedTotalCost: singleBill.appointmentEndedTotalCost, appointmentEndedTotalAmount: singleBill.appointmentEndedTotalAmount, appointmentCancelledTotalCost: singleBill.appointmentCancelledTotalCost, appointmentCancelledTotalAmount: singleBill.appointmentCancelledTotalAmount, appointmentPaidTotalCost: singleBill.appointmentPaidTotalCost, appointmentPaidTotalAmount: singleBill.appointmentPaidTotalAmount, billingStarted: singleBill.billingStarted, billingEnded: singleBill.billingEnded, dateCreated: singleBill.dateCreated, dateCreatedTimezone: singleBill.dateCreatedTimezone, dateCreatedCalendar: singleBill.dateCreatedCalendar, dateCreatedLocale: singleBill.dateCreatedLocale, billingStartDateObj: startBilling, billingEndDateObj: endBilling, billingCreatedDateObj: dateCreated)
                                                
                                                self.shopBillingsDataArrayData.append(billDataSingle)
                                                self.shopBillingsDataArrayData.sort { (first, second) -> Bool in
                                                    first.billingCreatedDateObj.isBeforeDate(second.billingCreatedDateObj, granularity: .minute)
                                                }
                                                DispatchQueue.main.async {
                                                    self.collectionView.reloadData()
                                                }
                                            }
                                        }
                                        
                                    } catch _ {
                                        
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                self.view.makeToast(NSLocalizedString("shopBillingViewControllerErrorMessageShopNotFound", comment: "Shop billings not found"), duration: 2.0, position: .bottom)
                                                self.shopBillingsDataArrayData.removeAll()
                                                DispatchQueue.main.async {
                                                    self.collectionView.reloadData()
                                                }
                                            case 500:
                                                self.view.makeToast(NSLocalizedString("shopBillingViewControllerErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                            default:
                                                print("sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
    }
    
    @objc private func shopBillingExitThisView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupViewConstriants(){
        var topDistance: CGFloat = 20
        var bottomDistance: CGFloat = -10
        if UIDevice.current.hasNotch {
            topDistance = 60
            bottomDistance = -20
        }
        
        
        upperInputsContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(shopBillingShopPicker)
        upperInputsContainerView.addSubview(shopBillingViewTitlePlaceHolder)
        upperInputsContainerView.addSubview(shopBillingCloseViewButton)
        
        shopBillingShopPicker.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        shopBillingShopPicker.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor).isActive = true
        shopBillingShopPicker.widthAnchor.constraint(equalToConstant: 45).isActive = true
        shopBillingShopPicker.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        shopBillingCloseViewButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        shopBillingCloseViewButton.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        shopBillingCloseViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        shopBillingCloseViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        shopBillingViewTitlePlaceHolder.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        shopBillingViewTitlePlaceHolder.leftAnchor.constraint(equalTo: shopBillingShopPicker.rightAnchor, constant: 10).isActive = true
        shopBillingViewTitlePlaceHolder.rightAnchor.constraint(equalTo: shopBillingCloseViewButton.leftAnchor).isActive = true
        shopBillingViewTitlePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        shopBillingNameTitlePlaceHolder.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 5).isActive = true
        shopBillingNameTitlePlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        shopBillingNameTitlePlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        shopBillingNameTitlePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        shopUniqueIDTitlePlaceHolder.topAnchor.constraint(equalTo: shopBillingNameTitlePlaceHolder.bottomAnchor, constant: 5).isActive = true
        shopUniqueIDTitlePlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        shopUniqueIDTitlePlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        shopUniqueIDTitlePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        curvedCollectView.topAnchor.constraint(equalTo: shopUniqueIDTitlePlaceHolder.bottomAnchor, constant: 10).isActive = true
        curvedCollectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        curvedCollectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -8).isActive = true
        curvedCollectView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: bottomDistance).isActive = true
        
        curvedCollectView.addSubview(collectView)
        
        collectView.topAnchor.constraint(equalTo: curvedCollectView.topAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: curvedCollectView.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: curvedCollectView.widthAnchor, multiplier: 1, constant: -10).isActive = true
        collectView.bottomAnchor.constraint(equalTo: curvedCollectView.bottomAnchor, constant: -5).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
    }
    
    
    class shopBillingCollectionViewCell: UICollectionViewCell {
        override init(frame: CGRect) {
            super.init(frame: frame)
            setupViews()
        }
        
        let hopBillingStatusPlaceHolder: UILabel = {
            let fnhp = UILabel()
            fnhp.translatesAutoresizingMaskIntoConstraints = false
            fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 15)
            fnhp.textColor = UIColor.black
            fnhp.text = NSLocalizedString("shopBillingViewControllerBillStatus", comment: "Billing status")
            fnhp.textAlignment = .center
            return fnhp
        }()
        
        let billingDetailsTitlePlaceHolder: UILabel = {
            let fnhp = UILabel()
            fnhp.translatesAutoresizingMaskIntoConstraints = false
            fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 15)
            fnhp.textColor = UIColor.black
            fnhp.text = NSLocalizedString("shopBillingViewControllerBillBreakDown", comment: "Biling details break down")
            fnhp.textAlignment = .center
            return fnhp
        }()
        
        let appointNewPlaceHolder: UILabel = {
            let fnhp = UILabel()
            fnhp.translatesAutoresizingMaskIntoConstraints = false
            fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
            fnhp.textColor = UIColor.black
            fnhp.text = NSLocalizedString("shopBillingViewControllerStatusNewCharge", comment: "status 'new' charge")
            fnhp.textAlignment = .left
            return fnhp
        }()
        
        let appointNewAmountPlaceHolder: UILabel = {
            let fnhp = UILabel()
            fnhp.translatesAutoresizingMaskIntoConstraints = false
            fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
            fnhp.textColor = UIColor.black
            fnhp.text = NSLocalizedString("shopBillingViewControllerStatusNewAmount", comment: "status 'new'  amount")
            fnhp.textAlignment = .left
            return fnhp
        }()
        
        let appointStartPlaceHolder: UILabel = {
            let fnhp = UILabel()
            fnhp.translatesAutoresizingMaskIntoConstraints = false
            fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
            fnhp.textColor = UIColor.black
            fnhp.text = NSLocalizedString("shopBillingViewControllerStatusStartedCharge", comment: "status 'started' charge")
            fnhp.textAlignment = .left
            return fnhp
        }()
        
        let appointStartAmountPlaceHolder: UILabel = {
            let fnhp = UILabel()
            fnhp.translatesAutoresizingMaskIntoConstraints = false
            fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
            fnhp.textColor = UIColor.black
            fnhp.text = NSLocalizedString("shopBillingViewControllerStatusStartedAmount", comment: "status 'started' amount")
            fnhp.textAlignment = .left
            return fnhp
        }()
        
        let appointEndPlaceHolder: UILabel = {
            let fnhp = UILabel()
            fnhp.translatesAutoresizingMaskIntoConstraints = false
            fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
            fnhp.textColor = UIColor.black
            fnhp.text = NSLocalizedString("shopBillingViewControllerStatusEndedCharge", comment: "status 'ended' charge")
            fnhp.textAlignment = .left
            return fnhp
        }()
        
        let appointEndAmountPlaceHolder: UILabel = {
            let fnhp = UILabel()
            fnhp.translatesAutoresizingMaskIntoConstraints = false
            fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
            fnhp.textColor = UIColor.black
            fnhp.text = NSLocalizedString("shopBillingViewControllerStatusEndedAmount", comment: "status 'ended' amount")
            fnhp.textAlignment = .left
            return fnhp
        }()
        
        let appointPaidPlaceHolder: UILabel = {
            let fnhp = UILabel()
            fnhp.translatesAutoresizingMaskIntoConstraints = false
            fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
            fnhp.textColor = UIColor.black
            fnhp.text = NSLocalizedString("shopBillingViewControllerStatusPaidCharge", comment: "status 'paid' charge")
            fnhp.textAlignment = .left
            return fnhp
        }()
        
        let appointPaidAmountPlaceHolder: UILabel = {
            let fnhp = UILabel()
            fnhp.translatesAutoresizingMaskIntoConstraints = false
            fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
            fnhp.textColor = UIColor.black
            fnhp.text = NSLocalizedString("shopBillingViewControllerStatusPaidAmount", comment: "status 'paid' amount")
            fnhp.textAlignment = .left
            return fnhp
        }()
        
        let appointUnpaidRecoveredPlaceHolder: UILabel = {
            let fnhp = UILabel()
            fnhp.translatesAutoresizingMaskIntoConstraints = false
            fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
            fnhp.textColor = UIColor.black
            fnhp.text = NSLocalizedString("shopBillingViewControllerStatusUnpaidCharge", comment: "status 'unpaid' charge")
            fnhp.textAlignment = .left
            return fnhp
        }()
        
        let appointUnpaidRecoveredAmountPlaceHolder: UILabel = {
            let fnhp = UILabel()
            fnhp.translatesAutoresizingMaskIntoConstraints = false
            fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
            fnhp.textColor = UIColor.black
            fnhp.text = NSLocalizedString("shopBillingViewControllerStatusUnpaidAmount", comment: "status 'unpaid' amount")
            fnhp.textAlignment = .left
            return fnhp
        }()
        
        let billingDateStartPlaceHolder: UILabel = {
            let fnhp = UILabel()
            fnhp.translatesAutoresizingMaskIntoConstraints = false
            fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
            fnhp.textColor = UIColor.black
            fnhp.text = NSLocalizedString("shopBillingViewControllerFromTitle", comment: "from")
            fnhp.textAlignment = .left
            return fnhp
        }()
        
        let billingDateEndPlaceHolder: UILabel = {
            let fnhp = UILabel()
            fnhp.translatesAutoresizingMaskIntoConstraints = false
            fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
            fnhp.textColor = UIColor.black
            fnhp.text = NSLocalizedString("shopBillingViewControllerToTitle", comment: "to")
            fnhp.textAlignment = .left
            return fnhp
        }()
        
        let billingDateCreatedPlaceHolder: UILabel = {
            let fnhp = UILabel()
            fnhp.translatesAutoresizingMaskIntoConstraints = false
            fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
            fnhp.textColor = UIColor.black
            fnhp.text = NSLocalizedString("shopBillingViewControllerDateCreated", comment: "date created")
            fnhp.textAlignment = .left
            return fnhp
        }()
        
        let TotalAmountPlaceHolder: UILabel = {
            let fnhp = UILabel()
            fnhp.translatesAutoresizingMaskIntoConstraints = false
            fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 15)
            fnhp.textColor = UIColor.black
            fnhp.text = NSLocalizedString("shopBillingViewControllerTotal", comment: "Total")
            fnhp.textAlignment = .right
            return fnhp
        }()
        
        
        
        func setupViews(){
            addSubview(hopBillingStatusPlaceHolder)
            
            addSubview(billingDetailsTitlePlaceHolder)
            
            addSubview(appointNewPlaceHolder)
            addSubview(appointNewAmountPlaceHolder)
            addSubview(appointStartPlaceHolder)
            addSubview(appointStartAmountPlaceHolder)
            addSubview(appointEndPlaceHolder)
            addSubview(appointEndAmountPlaceHolder)
            addSubview(appointPaidPlaceHolder)
            addSubview(appointPaidAmountPlaceHolder)
            addSubview(appointUnpaidRecoveredPlaceHolder)
            addSubview(appointUnpaidRecoveredAmountPlaceHolder)
            
            addSubview(billingDateStartPlaceHolder)
            addSubview(billingDateEndPlaceHolder)
            addSubview(billingDateCreatedPlaceHolder)
            
            addSubview(TotalAmountPlaceHolder)
            
            
            backgroundColor = UIColor.white
            layer.cornerRadius = 5
            layer.masksToBounds = true
            
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: hopBillingStatusPlaceHolder)
            
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: billingDetailsTitlePlaceHolder)
            
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: appointNewPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: appointNewAmountPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: appointStartPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: appointStartAmountPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: appointEndPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: appointEndAmountPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: appointPaidPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: appointPaidAmountPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: appointUnpaidRecoveredPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: appointUnpaidRecoveredAmountPlaceHolder)
            
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: billingDateStartPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: billingDateEndPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: billingDateCreatedPlaceHolder)
            
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: TotalAmountPlaceHolder)
            
            
            addContraintsWithFormat(format: "V:|-10-[v0(20)]-15-[v1(20)]-15-[v2(20)]-5-[v3(20)]-5-[v4(20)]-5-[v5(20)]-5-[v6(20)]-5-[v7(20)]-5-[v8(20)]-5-[v9(20)]-5-[v10(20)]-5-[v11(20)]-15-[v12(20)]-2-[v13(20)]-10-[v14(20)]-15-[v15(20)]-5-|", views: hopBillingStatusPlaceHolder, billingDetailsTitlePlaceHolder, appointNewPlaceHolder, appointNewAmountPlaceHolder, appointStartPlaceHolder, appointStartAmountPlaceHolder, appointEndPlaceHolder, appointEndAmountPlaceHolder, appointPaidPlaceHolder, appointPaidAmountPlaceHolder, appointUnpaidRecoveredPlaceHolder, appointUnpaidRecoveredAmountPlaceHolder, billingDateStartPlaceHolder, billingDateEndPlaceHolder, billingDateCreatedPlaceHolder,  TotalAmountPlaceHolder)
            
        }
        
        override func prepareForReuse() {
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }

}

extension ShopBillingViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.shopBillingsDataArrayData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellBill", for: indexPath) as! shopBillingCollectionViewCell
        
        cell.hopBillingStatusPlaceHolder.text = NSLocalizedString("shopBillingViewControllerBillStatus", comment: "Billing status") + ": " + self.shopBillingsDataArrayData[indexPath.row].billingStatus
        
        let curr = self.shopBillingsDataArrayData[indexPath.row].shopCountryCurrencyCode
        
        cell.appointNewPlaceHolder.text = NSLocalizedString("shopBillingViewControllerStatusNewCharge", comment: "status 'new' charge") + ": " + self.shopBillingsDataArrayData[indexPath.row].appointmentNewTotalCost + " " + curr
        cell.appointNewAmountPlaceHolder.text = NSLocalizedString("shopBillingViewControllerStatusNewAmount", comment: "status 'new'  amount") + ": " + self.shopBillingsDataArrayData[indexPath.row].appointmentNewTotalAmount + " " + NSLocalizedString("shopBillingViewControllerAppointmentTitle", comment: "appointment")
        let new = self.shopBillingsDataArrayData[indexPath.row].appointmentNewTotalCost
        
        
        cell.appointStartPlaceHolder.text = NSLocalizedString("shopBillingViewControllerStatusStartedCharge", comment: "status 'started' charge") + ": " + self.shopBillingsDataArrayData[indexPath.row].appointmentStartedTotalCost + " " + curr
        cell.appointStartAmountPlaceHolder.text = NSLocalizedString("shopBillingViewControllerStatusStartedAmount", comment: "status 'started' amount") + ": " + self.shopBillingsDataArrayData[indexPath.row].appointmentStartedTotalAmount + " " + NSLocalizedString("shopBillingViewControllerAppointmentTitle", comment: "appointment")
        let start = self.shopBillingsDataArrayData[indexPath.row].appointmentStartedTotalCost
        
        
        cell.appointEndPlaceHolder.text = NSLocalizedString("shopBillingViewControllerStatusEndedCharge", comment: "status 'ended' charge") + ": " + self.shopBillingsDataArrayData[indexPath.row].appointmentEndedTotalCost + " " + curr
        cell.appointEndAmountPlaceHolder.text = NSLocalizedString("shopBillingViewControllerStatusEndedAmount", comment: "status 'ended' amount") + ": " + self.shopBillingsDataArrayData[indexPath.row].appointmentEndedTotalAmount + " " + NSLocalizedString("shopBillingViewControllerAppointmentTitle", comment: "appointment")
        let end = self.shopBillingsDataArrayData[indexPath.row].appointmentEndedTotalCost
        
        
        cell.appointPaidPlaceHolder.text = NSLocalizedString("shopBillingViewControllerStatusPaidCharge", comment: "status 'paid' charge") + ": " + self.shopBillingsDataArrayData[indexPath.row].appointmentPaidTotalCost + " " + curr
        cell.appointPaidAmountPlaceHolder.text = NSLocalizedString("shopBillingViewControllerStatusPaidAmount", comment: "status 'paid' amount") + ": " + self.shopBillingsDataArrayData[indexPath.row].appointmentPaidTotalAmount + " " + NSLocalizedString("shopBillingViewControllerAppointmentTitle", comment: "appointment")
        let paid = self.shopBillingsDataArrayData[indexPath.row].appointmentPaidTotalCost
        
        
        cell.appointUnpaidRecoveredPlaceHolder.text = NSLocalizedString("shopBillingViewControllerStatusUnpaidCharge", comment: "status 'unpaid' charge") + ": -" + self.shopBillingsDataArrayData[indexPath.row].appointmentCancelledTotalCost + " " + curr
        cell.appointUnpaidRecoveredAmountPlaceHolder.text = NSLocalizedString("shopBillingViewControllerStatusUnpaidAmount", comment: "status 'unpaid' amount") + ": " + self.shopBillingsDataArrayData[indexPath.row].appointmentCancelledTotalAmount + " " + NSLocalizedString("shopBillingViewControllerAppointmentTitle", comment: "appointment")
        let recover = self.shopBillingsDataArrayData[indexPath.row].appointmentCancelledTotalCost
        
        
        let regionDataShop = DateByUserDeviceInitializer.getRegion(TZoneName: self.shopBillingsDataArrayData[indexPath.row].dateCreatedTimezone, calenName: self.shopBillingsDataArrayData[indexPath.row].dateCreatedCalendar, LocName: self.shopBillingsDataArrayData[indexPath.row].dateCreatedLocale)
        
        if let startBilling = self.shopBillingsDataArrayData[indexPath.row].billingStarted.toDate("yyyy-MM-dd HH:mm:ss", region: regionDataShop), let endBilling = self.shopBillingsDataArrayData[indexPath.row].billingEnded.toDate("yyyy-MM-dd HH:mm:ss", region: regionDataShop), let dateCreated =  self.shopBillingsDataArrayData[indexPath.row].dateCreated.toDate("yyyy-MM-dd HH:mm:ss", region: regionDataShop){
            
            let start = startBilling.toString(DateToStringStyles.date(.short))
            let end = endBilling.toString(DateToStringStyles.date(.short))
            let create = dateCreated.toString(DateToStringStyles.date(.short))
            
            cell.billingDateStartPlaceHolder.text = NSLocalizedString("shopBillingViewControllerFromTitle", comment: "from") + ": " + start
            cell.billingDateEndPlaceHolder.text = NSLocalizedString("shopBillingViewControllerToTitle", comment: "to") + ": " + end
            cell.billingDateCreatedPlaceHolder.text = NSLocalizedString("shopBillingViewControllerDateCreated", comment: "date created") + ": " + create
        }
        
        if let nw = Double(new), let st = Double(start), let ed = Double(end), let pd = Double(paid), let rc = Double(recover) {
            let totalSum = nw + st + ed + pd
            if (rc >= 0) {
                let totalSub = totalSum - rc
                cell.TotalAmountPlaceHolder.text = NSLocalizedString("shopBillingViewControllerTotal", comment: "Total") + ": " + String(totalSub) + " " + curr
            } else {
                cell.TotalAmountPlaceHolder.text = NSLocalizedString("shopBillingViewControllerTotal", comment: "Total") + ": " + String(totalSum) + " " + curr
            }
            
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 450)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let stat = self.shopBillingsDataArrayData[safe: indexPath.row]?.billingStatus, let idData = self.shopShortUniqueIDD {
            if (stat == "unpaid") {
                
                /*let curr = self.shopBillingsDataArrayData[indexPath.row].shopCountryCurrencyCode
                let new = self.shopBillingsDataArrayData[indexPath.row].appointmentNewTotalCost
                let start = self.shopBillingsDataArrayData[indexPath.row].appointmentStartedTotalCost
                let end = self.shopBillingsDataArrayData[indexPath.row].appointmentEndedTotalCost
                let paid = self.shopBillingsDataArrayData[indexPath.row].appointmentPaidTotalCost
                let recover = self.shopBillingsDataArrayData[indexPath.row].appointmentCancelledTotalCost
                
                if let nw = Double(new), let st = Double(start), let ed = Double(end), let pd = Double(paid), let rc = Double(recover) {
                    let totalSum = nw + st + ed + pd
                    if (rc >= 0) {
                        let totalSub = totalSum - rc
                        let mpesaView = MpesaViewController()
                        mpesaView.shopShortUniqueNumber = idData
                        mpesaView.finalPrice = "Total: " + String(totalSub) + " " + curr
                        mpesaView.modalPresentationStyle = .overCurrentContext
                        present(mpesaView, animated: true, completion: nil)
                        
                    } else {
                        
                        let mpesaView = MpesaViewController()
                        mpesaView.shopShortUniqueNumber = idData
                        mpesaView.finalPrice = "Total: " + String(totalSum) + " " + curr
                        mpesaView.modalPresentationStyle = .overCurrentContext
                        present(mpesaView, animated: true, completion: nil)
                    }
                    
                }*/
            }
        }
    }
}
