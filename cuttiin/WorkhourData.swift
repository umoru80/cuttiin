//
//  WorkhourData.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 08/03/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation
import SwiftDate

struct WorkhourData {
    var id: String
    var workhourCloseDate: String
    var workhourOpenDate: String
    var workhourWeekDay: String
    var workhourLastUpdated: String
    var workhourLastUpdatedDateObj: DateInRegion
    var workhourCalendar: String
    var workhourLocale: String
    var workhourTimezone: String
    var shopId: String
    
    init(id: String, workhourCloseDate: String, workhourOpenDate: String, workhourWeekDay: String, workhourLastUpdated: String, workhourLastUpdatedDateObj: DateInRegion, workhourCalendar: String, workhourLocale: String, workhourTimezone: String, shopId: String) {
        
        self.id = id
        self.workhourCloseDate = workhourCloseDate
        self.workhourOpenDate = workhourOpenDate
        self.workhourWeekDay = workhourWeekDay
        self.workhourLastUpdated = workhourLastUpdated
        self.workhourLastUpdatedDateObj = workhourLastUpdatedDateObj
        self.workhourCalendar = workhourCalendar
        self.workhourLocale = workhourLocale
        self.workhourTimezone = workhourTimezone
        self.shopId = shopId
        
    }
}
