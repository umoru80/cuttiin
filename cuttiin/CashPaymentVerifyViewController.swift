//
//  CashPaymentVerifyViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 11/4/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate

class CashPaymentVerifyViewController: UIViewController {
    
    var barberShopUniqueID: String?
    var bookingUUID: String?
    var totalPriceWithCharges: String?
    var shoppingListViewDataObjectConstant = ShoppingListViewController()
    
    
    let statusPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 25)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.text = NSLocalizedString("suceesPlaceHolderTextAcceptDeclineView", comment: "Success!")
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let statusDescriptionPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.text = NSLocalizedString("cashPaymentStatusDescriptionTextAcceptDeclineView", comment: "Transaction completed")
        fnhp.numberOfLines = 0
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let checkImageDescriptionThumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.clear
        tniv.image = UIImage(named: "checkmark_big")
        tniv.contentMode = .scaleAspectFit
        return tniv
    }()
    
    lazy var appointmentsButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        st.setTitle(NSLocalizedString("doneButtonPaymentView", comment: "Done"), for: .normal)
        st.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 30)
        st.tag = 1
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.layer.borderWidth = 2
        st.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        st.addTarget(self, action: #selector(handleShowAppointView), for: .touchUpInside)
        return st
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        view.addSubview(statusPlaceHolder)
        view.addSubview(statusDescriptionPlaceHolder)
        view.addSubview(checkImageDescriptionThumbnailImageView)
        view.addSubview(appointmentsButton)
        setViewContriants()
    }
    
    func setViewContriants(){
        statusPlaceHolder.topAnchor.constraint(equalTo: view.topAnchor, constant: 200).isActive = true
        statusPlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        statusPlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        statusPlaceHolder.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        statusDescriptionPlaceHolder.topAnchor.constraint(equalTo: statusPlaceHolder.bottomAnchor, constant: 10).isActive = true
        statusDescriptionPlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        statusDescriptionPlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -12).isActive = true
        statusDescriptionPlaceHolder.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        checkImageDescriptionThumbnailImageView.topAnchor.constraint(equalTo: statusDescriptionPlaceHolder.bottomAnchor, constant: 30).isActive = true
        checkImageDescriptionThumbnailImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        checkImageDescriptionThumbnailImageView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -72).isActive = true
        checkImageDescriptionThumbnailImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        appointmentsButton.topAnchor.constraint(equalTo: checkImageDescriptionThumbnailImageView.bottomAnchor, constant: 15).isActive = true
        appointmentsButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        appointmentsButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -72).isActive = true
        appointmentsButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    @objc func handleShowAppointView(){
        UserDefaults.standard.set(true, forKey: "theBookingProcessIsComplete")
        self.dismissThisView()
    }
    
    func dismissThisView(){
        self.dismiss(animated: true, completion: nil)
    }

}
