//
//  Staff.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 04/03/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation

struct Staff: Codable {
    let staffService: [String]
    let id, customer, dateCreated, dateCreatedTimezone: String
    let dateCreatedCalendar, dateCreatedLocale, staffHiddenShop, staffHiddenByAdmin: String
    let shop: String
    let v: Int
    
    enum CodingKeys: String, CodingKey {
        case staffService
        case id = "_id"
        case customer, dateCreated, dateCreatedTimezone, dateCreatedCalendar, dateCreatedLocale, staffHiddenShop, staffHiddenByAdmin, shop
        case v = "__v"
    }
}
