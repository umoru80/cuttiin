//
//  RegistrationViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/13/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Navajo_Swift
import SwiftDate
import Alamofire
import SwiftyJSON

class RegistrationViewController: UIViewController {
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    var emailValid = false
    var passwordValid = false
    
    let inputsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let firstNameHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("firstNameLabelandTextFieldLabel", comment: "First Name")
        fnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        fnhp.textColor = UIColor.black
        fnhp.isHidden = true
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let firstNameTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.placeholder = NSLocalizedString("firstNameLabelandTextFieldLabel", comment: "First Name")
        em.addTarget(self, action: #selector(firstNametextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(firstNametextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let firstNameSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor.black
        return fnsv
    }()
    
    let lastNameHiddenPlaceHolder: UILabel = {
        let lnhp = UILabel()
        lnhp.translatesAutoresizingMaskIntoConstraints = false
        lnhp.text = NSLocalizedString("lastNameLabelandTextFieldLabel", comment: "Last Name")
        lnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        lnhp.textColor = UIColor.black
        lnhp.isHidden = true
        lnhp.adjustsFontSizeToFitWidth = true
        lnhp.minimumScaleFactor = 0.1
        lnhp.baselineAdjustment = .alignCenters
        lnhp.textAlignment = .left
        return lnhp
    }()
    
    let lastNameTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.placeholder = NSLocalizedString("lastNameLabelandTextFieldLabel", comment: "Last Name")
        em.addTarget(self, action: #selector(lastNametextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(lastNametextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let lastNameSeperatorView: UIView = {
        let lnsv = UIView()
        lnsv.translatesAutoresizingMaskIntoConstraints = false
        lnsv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return lnsv
    }()
    
    let emailHiddenPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("createAccountEmailTitle", comment: "Email")
        ehp.font = UIFont(name: "OpenSans-Light", size: 10)
        ehp.textColor = UIColor.black
        ehp.isHidden = true
        ehp.adjustsFontSizeToFitWidth = true
        ehp.minimumScaleFactor = 0.1
        ehp.baselineAdjustment = .alignCenters
        ehp.textAlignment = .left
        return ehp
    }()
    
    let emailTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.placeholder = NSLocalizedString("createAccountEmailTitle", comment: "Email")
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.keyboardType = .emailAddress
        em.autocapitalizationType = .none
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(isValidEmail), for: .editingChanged)
        return em
    }()
    
    let emailSeperatorView: UIView = {
        let esv = UIView()
        esv.translatesAutoresizingMaskIntoConstraints = false
        esv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return esv
    }()
    
    let passwordHiddenPlaceHolder: UILabel = {
        let php = UILabel()
        php.translatesAutoresizingMaskIntoConstraints = false
        php.text = NSLocalizedString("passwordLabelTextCustomerRegistrationView", comment: "password must have 8 characters or more with characters and digits")
        php.font = UIFont(name: "OpenSans-Light", size: 10)
        php.textColor = UIColor.black
        php.isHidden = true
        php.adjustsFontSizeToFitWidth = true
        php.minimumScaleFactor = 0.1
        php.baselineAdjustment = .alignCenters
        php.textAlignment = .left
        return php
    }()
    
    let passwordTextField: UITextField = {
        let ps = UITextField()
        ps.translatesAutoresizingMaskIntoConstraints = false
        ps.textColor = UIColor.black
        ps.placeholder = NSLocalizedString("createAccountPasswordTitle", comment: "Password")
        ps.font = UIFont(name: "OpenSans-Light", size: 15)
        ps.isSecureTextEntry = true
        ps.addTarget(self, action: #selector(isValidPassword), for: .editingChanged)
        ps.addTarget(self, action: #selector(passwordtextFieldDidChange), for: .editingDidBegin)
        ps.addTarget(self, action: #selector(passwordtextFieldDidChange), for: .editingChanged)
        return ps
    }()
    
    lazy var showPassordTextFieldButton: UIButton = {
        let sptfb = UIButton()
        sptfb.translatesAutoresizingMaskIntoConstraints = false
        sptfb.isUserInteractionEnabled = true
        sptfb.setTitle(NSLocalizedString("createAccountShowButton", comment: "Show"), for: .normal)
        sptfb.setTitleColor(UIColor(r: 23, g: 69, b: 90), for: .normal)
        sptfb.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 12)
        sptfb.backgroundColor = UIColor.white
        sptfb.addTarget(self, action: #selector(handleShowPasswordText), for: .touchUpInside)
        return sptfb
    }()
    
    let passwordSeperatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return view
    }()
    
    lazy var registerButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        st.setTitle(NSLocalizedString("registerButtonTitleCustomerRegistrationView", comment: "register"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.addTarget(self, action: #selector(handleRegisterAccount), for: .touchUpInside)
        st.isEnabled = true
        return st
    }()
    
    lazy var termsAndConditionsButtonLabel: UILabel = {
        let dtv = UILabel()
        dtv.translatesAutoresizingMaskIntoConstraints = false
        dtv.textAlignment = .center
        dtv.numberOfLines = 0
        dtv.isUserInteractionEnabled = true
        dtv.text = NSLocalizedString("termsAndConditionCustomerRegistrationView", comment: "by hitting Register you accept the Terms and Conditions!")
        dtv.font = UIFont(name: "OpenSans-Light", size: 9.0)
        dtv.textColor = UIColor.black
        dtv.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowWebPopUPWebTermsandCondition)))
        return dtv
    }()
    
    lazy var alreadyAMemberButtonLabel: UILabel = {
        let dtv = UILabel()
        dtv.translatesAutoresizingMaskIntoConstraints = false
        dtv.textAlignment = .center
        dtv.numberOfLines = 0
        dtv.isUserInteractionEnabled = true
        dtv.text = NSLocalizedString("alreadyAMemberButtonTitleCustomerRegistrationView", comment: "already a member? Log in")
        dtv.font = UIFont(name: "OpenSans-Light", size: 11.0)
        dtv.textColor = UIColor.black
        
        dtv.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleLoginAction)))
        return dtv
    }()
    
    let errorMessagePlaceHolder: UILabel = {
        let emhp = UILabel()
        emhp.translatesAutoresizingMaskIntoConstraints = false
        emhp.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        emhp.textColor = UIColor.black
        emhp.adjustsFontSizeToFitWidth = true
        emhp.minimumScaleFactor = 0.1
        emhp.numberOfLines = 0
        emhp.baselineAdjustment = .alignCenters
        emhp.textAlignment = .center
        return emhp
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        navigationItem.title = NSLocalizedString("registerButtonTitleCustomerRegistrationView", comment: "register")
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "closeIcon"), style: .done, target: self, action: #selector(handleBackAction))
        view.addSubview(inputsContainerView)
        view.addSubview(registerButton)
        view.addSubview(termsAndConditionsButtonLabel)
        view.addSubview(alreadyAMemberButtonLabel)
        view.addSubview(errorMessagePlaceHolder)
        setupInputContainerView()
        setupButtons()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleBackAction(){
        self.hideKeyboard()
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.firstNameTextField.becomeFirstResponder()
        checkCustomerAuthentication()
    }
    
    @objc func checkCustomerAuthentication(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isAfterDate(dateNow, orEqual: true, granularity: .second) {
                    dismiss(animated: false, completion: nil)
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.emailTextField.text = ""
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.passwordTextField.text = ""
        self.passwordSeperatorView.backgroundColor = UIColor.black
        self.firstNameTextField.text = ""
        self.firstNameSeperatorView.backgroundColor = UIColor.black
        self.lastNameTextField.text = ""
        self.lastNameSeperatorView.backgroundColor = UIColor.black
    }
    
    @objc private func hideKeyboard(){
        view.endEditing(true)
    }
    
    @objc func handleRegisterAccount(){
        self.errorMessagePlaceHolder.text = ""
        self.hideKeyboard()
        textFieldDidChange()
    }
    
    @objc func handleLoginAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func handleAccestoApp(){
        let mainView = ViewController()
        let navController = UINavigationController(rootViewController: mainView)
        present(navController, animated: true, completion: nil)
    }
    
    @objc func textFieldDidChange(){
        if firstNameTextField.text == "" || lastNameTextField.text == "" || emailTextField.text == "" || passwordTextField.text == "" {
            //Disable button
            self.errorMessagePlaceHolder.text = NSLocalizedString("createAccountErrorMessageIncompleteForm", comment: "incomplete form")
            
        } else {
            //Enable button
            if emailValid == true && passwordValid == true {
                if let firstName = firstNameTextField.text, let lastName = lastNameTextField.text,let emailtext = emailTextField.text, let password = passwordTextField.text  {
                    self.checkAuthenticationType(firstName: firstName, lastName: lastName, emailtext: emailtext, password: password )
                }
            } else {
                if (emailValid == false) {
                    self.errorMessagePlaceHolder.text = NSLocalizedString("createAccountErrorMessageEmailInvalid", comment: "email invalid")
                }
                
                if (passwordValid == false ) {
                    self.errorMessagePlaceHolder.text = NSLocalizedString("createAccountErrorMessagePasswordInvalid", comment: "password invalid")
                }
                
                if (emailValid == false && passwordValid == false ) {
                    self.errorMessagePlaceHolder.text = NSLocalizedString("createAccountErrorMessageEmailPasswordInvalid", comment: "email and password invalid")
                }
                
            }
        }
    }
    
    @objc func firstNametextFieldDidChange(){
        self.firstNameSeperatorView.backgroundColor = UIColor.black
        self.lastNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.emailSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.passwordSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.firstNameHiddenPlaceHolder.isHidden = false
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = true
        self.passwordHiddenPlaceHolder.isHidden = true
    }
    
    @objc func lastNametextFieldDidChange(){
        self.lastNameSeperatorView.backgroundColor = UIColor.black
        self.firstNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.emailSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.passwordSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = false
        self.emailHiddenPlaceHolder.isHidden = true
        self.passwordHiddenPlaceHolder.isHidden = true
    }
    
    @objc func emailtextFieldDidChange(){
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.firstNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.lastNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.passwordSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = false
        self.passwordHiddenPlaceHolder.isHidden = true
        
    }
    
    @objc func passwordtextFieldDidChange(){
        self.passwordSeperatorView.backgroundColor = UIColor.black
        self.firstNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.lastNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.emailSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = true
        self.passwordHiddenPlaceHolder.isHidden = false
    }
    
    @objc func handleShowWebPopUPWebTermsandCondition(){
        guard let url = URL(string: "http://staysharpapp.com/privacy-policy/") else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @objc func handleShowPasswordText(){
        let data = passwordTextField.isSecureTextEntry
        if (data) {
            passwordTextField.isSecureTextEntry = false
        } else {
            passwordTextField.isSecureTextEntry = true
        }
    }
    
    @objc func isValidPassword(){
        let password = passwordTextField.text ?? ""
        let strength = Navajo.strength(ofPassword: password)
        let strengthValue = Navajo.localizedString(forStrength: strength)
        
        if (strengthValue == "Strong" || strengthValue == "Very Strong" || strengthValue == "Reasonable"){
            print("shoes")
            self.passwordValid = true
            switch strengthValue {
            case "Strong":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("passwordStrengthCustomerRegistrationView", comment: "Strong password")
            case "Very Strong":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("veryStrongPasswordStrengthCustomerRegistrationView", comment: "Very Strong password")
            case "Reasonable":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("reasonablePasswordStrengthCustomerRegistrationView", comment: "Reasonable password")
            default:
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("weakPasswordStrengthCustomerRegistrationView", comment: "weak password - password must have 6 characters or more with letters and numbers")
            }
        }else{
            self.passwordValid = false
            switch strengthValue {
            case "Strong":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("passwordStrengthCustomerRegistrationView", comment: "Strong password")
            case "Very Strong":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("veryStrongPasswordStrengthCustomerRegistrationView", comment: "Very Strong password")
            case "Reasonable":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("reasonablePasswordStrengthCustomerRegistrationView", comment: "Reasonable password")
            default:
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("weakPasswordStrengthCustomerRegistrationView", comment: "weak password - password must have 6 characters or more with letters and numbers")
            }
        }
    }
    
    @objc func isValidEmail() {
        let testStr = emailTextField.text ?? ""
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        if result == true {
            self.emailValid = true
            self.emailHiddenPlaceHolder.text = NSLocalizedString("createAccountErrorMessageEmailText", comment: "Email")
        }else{
            self.emailValid = false
            self.emailHiddenPlaceHolder.text = NSLocalizedString("createAccountErrorMessageEmailNotProperlyFormatted", comment: "Email is not properly formatted")
        }
    }
    
    @objc func checkAuthenticationType(firstName: String, lastName: String, emailtext: String, password: String ) {
        DispatchQueue.global(qos: .background).async {
            let parameters: Parameters = ["email": emailtext]
            AF.request(self.BACKEND_URL + "auth", method: .post, parameters: parameters)
                .validate(statusCode: 200..<500)
                .validate(contentType: ["application/json"])
                .response { (responseData) in
                    
                    if let _ = responseData.error {
                        let message =  NSLocalizedString("createAccountErrorMessageErrorTyAgain", comment: "Error: please try again later")
                        self.view.makeToast(message, duration: 3.0, position: .bottom)
                        return
                    }
                    
                    if let jsonData = responseData.data {
                        do {
                            let serverResponse = try JSONDecoder().decode(AuthenticationType.self, from: jsonData)
                            let message = serverResponse.message
                            
                            if ( message == "facebook") {
                                self.errorMessagePlaceHolder.text = NSLocalizedString("createAccountErrorMessageEmailAlreadyExistFacebook", comment: "Email already exist with a facebook authentication")
                            } else {
                                self.errorMessagePlaceHolder.text = NSLocalizedString("createAccountErrorMessageEmailAlreadyExist", comment: "Email already exist")
                            }
                            
                        } catch _ {
                            
                            if let statusCode = responseData.response?.statusCode {
                                switch statusCode {
                                case 404:
                                    self.view.makeToast("email is verified", duration: 2.0, position: .bottom)
                                    let values = ["firstName":firstName, "lastName":lastName, "email":emailtext, "password":password] as [String: AnyObject]
                                    self.registerButton.isEnabled = false
                                    let choosePView = ChoosePhotoViewController()
                                    choosePView.customerOrBarberChoice = "customer"
                                    choosePView.customerValues = values
                                    let navController = UINavigationController(rootViewController: choosePView)
                                    self.present(navController, animated: true, completion: nil)
                                    self.firstNameTextField.text = ""
                                    self.lastNameTextField.text = ""
                                    self.emailTextField.text = ""
                                    self.passwordTextField.text = ""
                                case 500:
                                    self.view.makeToast(NSLocalizedString("createAccountErrorMessageErrorOccured", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                default:
                                    print("sky high")
                                }
                            }
                        }
                    }
            }
        }
    }
    
    func setupInputContainerView(){
        inputsContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        inputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        inputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        inputsContainerView.heightAnchor.constraint(equalToConstant: 180).isActive = true
        
        
        inputsContainerView.addSubview(firstNameHiddenPlaceHolder)
        inputsContainerView.addSubview(firstNameTextField)
        inputsContainerView.addSubview(firstNameSeperatorView)
        inputsContainerView.addSubview(lastNameHiddenPlaceHolder)
        inputsContainerView.addSubview(lastNameTextField)
        inputsContainerView.addSubview(lastNameSeperatorView)
        inputsContainerView.addSubview(emailHiddenPlaceHolder)
        inputsContainerView.addSubview(emailTextField)
        inputsContainerView.addSubview(emailSeperatorView)
        inputsContainerView.addSubview(passwordHiddenPlaceHolder)
        inputsContainerView.addSubview(passwordTextField)
        inputsContainerView.addSubview(showPassordTextFieldButton)
        inputsContainerView.addSubview(passwordSeperatorView)
        
        
        firstNameHiddenPlaceHolder.topAnchor.constraint(equalTo: inputsContainerView.topAnchor).isActive = true
        firstNameHiddenPlaceHolder.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        firstNameHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        firstNameHiddenPlaceHolder.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        firstNameTextField.topAnchor.constraint(equalTo: firstNameHiddenPlaceHolder.bottomAnchor).isActive = true
        firstNameTextField.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        firstNameTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        firstNameTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        firstNameSeperatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        firstNameSeperatorView.topAnchor.constraint(equalTo: firstNameTextField.bottomAnchor).isActive = true
        firstNameSeperatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        firstNameSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        lastNameHiddenPlaceHolder.topAnchor.constraint(equalTo: firstNameSeperatorView.bottomAnchor, constant: 5).isActive = true
        lastNameHiddenPlaceHolder.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        lastNameHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        lastNameHiddenPlaceHolder.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        lastNameTextField.topAnchor.constraint(equalTo: lastNameHiddenPlaceHolder.bottomAnchor).isActive = true
        lastNameTextField.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        lastNameTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        lastNameTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        lastNameSeperatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        lastNameSeperatorView.topAnchor.constraint(equalTo: lastNameTextField.bottomAnchor).isActive = true
        lastNameSeperatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        lastNameSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        emailHiddenPlaceHolder.topAnchor.constraint(equalTo: lastNameSeperatorView.bottomAnchor, constant: 5).isActive = true
        emailHiddenPlaceHolder.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        emailHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        emailHiddenPlaceHolder.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        emailTextField.topAnchor.constraint(equalTo: emailHiddenPlaceHolder.bottomAnchor).isActive = true
        emailTextField.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        emailSeperatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        emailSeperatorView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor).isActive = true
        emailSeperatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        emailSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        passwordHiddenPlaceHolder.topAnchor.constraint(equalTo: emailSeperatorView.bottomAnchor, constant: 5).isActive = true
        passwordHiddenPlaceHolder.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        passwordHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        passwordHiddenPlaceHolder.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        passwordTextField.topAnchor.constraint(equalTo: passwordHiddenPlaceHolder.bottomAnchor).isActive = true
        passwordTextField.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        passwordTextField.rightAnchor.constraint(equalTo: inputsContainerView.rightAnchor, constant: -20).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        showPassordTextFieldButton.bottomAnchor.constraint(equalTo: passwordTextField.bottomAnchor).isActive = true
        showPassordTextFieldButton.rightAnchor.constraint(equalTo: passwordTextField.rightAnchor).isActive = true
        showPassordTextFieldButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        passwordSeperatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        passwordSeperatorView.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor).isActive = true
        passwordSeperatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        passwordSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
    }
    
    func setupButtons(){
        registerButton.topAnchor.constraint(equalTo: inputsContainerView.bottomAnchor, constant: 10).isActive = true
        registerButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        registerButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        registerButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        termsAndConditionsButtonLabel.topAnchor.constraint(equalTo: registerButton.bottomAnchor, constant: 5).isActive = true
        termsAndConditionsButtonLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        termsAndConditionsButtonLabel.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -12).isActive = true
        termsAndConditionsButtonLabel.heightAnchor.constraint(equalToConstant: 25).isActive =  true
        
        alreadyAMemberButtonLabel.topAnchor.constraint(equalTo: termsAndConditionsButtonLabel.bottomAnchor, constant: 5).isActive = true
        alreadyAMemberButtonLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        alreadyAMemberButtonLabel.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        alreadyAMemberButtonLabel.heightAnchor.constraint(equalToConstant: 20).isActive =  true
        
        errorMessagePlaceHolder.topAnchor.constraint(equalTo: alreadyAMemberButtonLabel.bottomAnchor, constant: 5).isActive = true
        errorMessagePlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        errorMessagePlaceHolder.heightAnchor.constraint(equalToConstant: 40).isActive = true
        errorMessagePlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
    }

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
