//
//  BarberShopBarbersAndServicesViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/24/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import MGStarRatingView
import SwiftDate
import Alamofire
import SwiftSpinner
import FBSDKLoginKit
import Zipper

class BarberShopBarbersAndServicesViewController: UIViewController {
    let starView = StarRatingView()
    let attribute = StarRatingAttribute(type: .rate,point: 20,spacing: 5, emptyColor: .black, fillColor: .white)
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    
    var serviceOne = [ServiceData]()
    var serviceCategoryLadies = [ServiceData]()
    var serviceCategoryGents = [ServiceData]()
    var serviceCategoryKids = [ServiceData]()
    var barberlistone = [StaffData]()
    var selectedLadiesGentsKidsButton = "Gents"
    
    var ladiesSelectedList = [String]()
    var gentsSelectedList = [String]()
    var kidsSelectedList = [String]()
    
    
    var selectedServiceOne = [String]()
    var selectedBarberOne = [String]()
    
    var shopServiceImageName = [String]()
    var shopStaffImageName = [String]()
    
    var currentSelectedShop: String?
    var currentSelectedShopCurrency: String?
    var tokenString: String?
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    lazy var staffServiceShopPicker: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8shoppicker")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(staffAndServiceShopSelectorView)))
        return imageView
    }()
    
    lazy var staffServiceCloseViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(staffAndServiceExitThisView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let staffServiceViewTitlePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("barberShopStaffAndServicesTitle", comment: "Staff and Services")
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 20)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    let barberShopHeaderDetailsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 5
        return view
    }()
    
    lazy var selectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8addstaffandservice")
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = UIColor.clear
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleAddingBarber)))
        return imageView
    }()
    
    lazy var addBarberAddServiceButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitle(NSLocalizedString("barberShopStaffAndServicesAddStaffButton", comment: "ADD STAFF"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 15)
        st.contentHorizontalAlignment = .left
        st.addTarget(self, action: #selector(handleAddingBarber), for: .touchUpInside)
        return st
    }()
    
    lazy var serviceBarberSegmentedControl: UISegmentedControl = {
        let ocsegmentcontrol = UISegmentedControl(items: [NSLocalizedString("barberShopStaffAndServicesStaffButtonTabTitle", comment: "STAFF"), NSLocalizedString("barberShopStaffAndServicesServiceButtonTabTitle", comment: "SERVICES")])
        ocsegmentcontrol.translatesAutoresizingMaskIntoConstraints = false
        ocsegmentcontrol.tintColor = UIColor(r: 220, g: 220, b: 220)
        ocsegmentcontrol.selectedSegmentIndex = 0
        ocsegmentcontrol.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: UIControl.State.normal)
        ocsegmentcontrol.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: UIControl.State.selected)
        ocsegmentcontrol.addTarget(self, action: #selector(handleServiceBarberChange), for: .valueChanged)
        return ocsegmentcontrol
    }()
    
    let categoryButtonContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var ladiesButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor.clear
        fbutton.setTitle(NSLocalizedString("barberShopStaffAndServicesLadiesTabButtonZero", comment: "LADIES(0)"), for: .normal)
        fbutton.setTitleColor(UIColor.black, for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 15)
        fbutton.addTarget(self, action: #selector(handleShowLadiesList), for: .touchUpInside)
        return fbutton
    }()
    
    let ladiesButtonSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor.black
        return fnsv
    }()
    
    lazy var gentsButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor.clear
        fbutton.setTitle(NSLocalizedString("barberShopStaffAndServicesGentsTabButtonZero", comment: "GENTS(0)"), for: .normal)
        fbutton.setTitleColor(UIColor.black, for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 15)
        fbutton.addTarget(self, action: #selector(handleShowGentsList), for: .touchUpInside)
        fbutton.isHidden = true
        return fbutton
    }()
    
    let gentsButtonSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor.clear
        return fnsv
    }()
    
    lazy var kidsButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor.clear
        fbutton.setTitle(NSLocalizedString("barberShopStaffAndServicesKidsTabButtonZero", comment: "KIDS(0)"), for: .normal)
        fbutton.setTitleColor(UIColor.black, for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 15)
        fbutton.addTarget(self, action: #selector(handleShowKidsList), for: .touchUpInside)
        return fbutton
    }()
    
    let kidsButtonSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor.clear
        return fnsv
    }()
    
    let curvedCollectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        cview.layer.masksToBounds = true
        cview.layer.cornerRadius = 5
        return cview
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.allowsMultipleSelection = true
        cv.register(customServiceCollectionViewCell.self, forCellWithReuseIdentifier: "cellIdd")
        cv.register(customStaffCollectionViewCell.self, forCellWithReuseIdentifier: "cellIdd2")
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        view.addSubview(barberShopHeaderDetailsContainerView)
        view.addSubview(serviceBarberSegmentedControl)
        view.addSubview(categoryButtonContainerView)
        view.addSubview(curvedCollectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewConstriants()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .didReceiveDataStaffAndServices, object: nil)
        getShopDetails()
        handleGettingShopStaffs()
    }
    
    @objc private func onDidReceiveData(_ notification:Notification) {
        print("road runner")
        if let dataBack = notification.userInfo as? [String: AnyObject], let shopId = dataBack["shopUniqueId"] as? String {
            self.currentSelectedShop = shopId
            getShopDetails()
            handleGettingShopStaffs()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func staffAndServiceExitThisView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func staffAndServiceShopSelectorView(){
        
        let customerShops = CustomerShopsViewController()
        customerShops.staffAndServiceViewController = self
        customerShops.viewControllerWhoInitiatedAction = "staffAndServiceView"
        customerShops.modalPresentationStyle = .overCurrentContext
        present(customerShops, animated: true, completion: nil)
        
    }
    
    @objc private func handleAddingBarber(){
        if let shopID = self.currentSelectedShop {
            let addBarber = AddBarberViewController()
            addBarber.actionStatusForView = "addStaff"
            addBarber.currentShopID = shopID
            addBarber.staffHiddenStatus = "NO"
            addBarber.modalPresentationStyle = .overCurrentContext
            present(addBarber, animated: true, completion: nil)
        }
    }
    
    @objc private func handleAddingService(){
        
        if let id = self.currentSelectedShop {
            let addService = AddServiceViewController()
            addService.actionStatusForView = "addService"
            addService.shopID = id
            addService.modalPresentationStyle = .overCurrentContext
            present(addService, animated: true, completion: nil)
        }
    }
    
    func StarRatingValueChanged(view: StarRatingView, value: CGFloat) {
        // use value
        print(value)
    }
    
    @objc private func getShopDetails(){
        DispatchQueue.global(qos: .background).async {
            
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let shopUniqueID = self.currentSelectedShop {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        AF.request(self.BACKEND_URL + "getsingleshop/" + shopUniqueID, method: .post, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    do {
                                        let shopData = try JSONDecoder().decode(Shop.self, from: jsonData)
                                        self.currentSelectedShopCurrency = shopData.shopCountryCurrencyCode
                                        self.handleGetShopServices()
                                        
                                    } catch _ {
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                print("Shops not found")
                                            case 500:
                                                print("An error occurred, please try again later")
                                            default:
                                                print("sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
    }
    
    @objc private func handleGetShopServices(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let shop = self.currentSelectedShop {
                
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    // handle looged out process
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    self.tokenString = token
                    self.serviceOne.removeAll()
                    self.serviceCategoryGents.removeAll()
                    self.serviceCategoryLadies.removeAll()
                    self.serviceCategoryKids.removeAll()
                    
                    self.selectedServiceOne.removeAll()
                    self.ladiesSelectedList.removeAll()
                    self.gentsSelectedList.removeAll()
                    self.kidsSelectedList.removeAll()
                    print("cleared all")
                    
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                    
                    DispatchQueue.global(qos: .background).async {
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        AF.request(self.BACKEND_URL + "getshopservice/" + shop, method: .post, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"])
                            .response (completionHandler: { (response) in
                                
                                if let error = response.error {
                                    print(error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    
                                    do {
                                        let shopServiceArrayData = try JSONDecoder().decode([ShopService].self, from: jsonData)
                                        for shopService in shopServiceArrayData {
                                            
                                            if (shopService.serviceHiddenByAdmin == "NO"){
                                                let serviceImageName = shopService.serviceImageName
                                                
                                                AF.request(self.BACKEND_URL + "images/shopServiceImages/" + serviceImageName, headers: headers).responseData { response in
                                                    
                                                    switch response.result {
                                                    case .success(let data):
                                                        if let image = UIImage(data: data) {
                                                            switch shopService.serviceCategory {
                                                            case "Gents":
                                                                let serviceDataGents = ServiceData(serviceID: shopService.id, serviceTitle: shopService.serviceTitle, serviceEstimatedTime: shopService.serviceEstimatedTotalTime, serviceCost: shopService.servicePrice, shortDescription: shopService.serviceDescription, category: shopService.serviceCategory, dateCreated: shopService.serviceDateCreated, timezone: shopService.serviceDateCreatedTimezone, calendar: shopService.serviceDateCreatedCalendar, local: shopService.serviceDateCreatedLocale, serviceImage: image, isHiddenByShop: shopService.serviceHiddenByShop, isHiddenByAdmin: shopService.serviceHiddenByAdmin, numberOfCustomer: 0, serviceStaff: shopService.serviceStaff, isAssigned: "NO", shop: shopService.serviceShop)
                                                                self.serviceCategoryGents.append(serviceDataGents)
                                                                self.serviceOne.append(serviceDataGents)
                                                                self.serviceOne.sort { (first, second) -> Bool in
                                                                    first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                }
                                                                self.serviceCategoryGents.sort { (first, second) -> Bool in
                                                                    first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                }
                                                                self.gentsButton.setTitle(NSLocalizedString("barberShopStaffAndServicesGentsTabButton", comment: "GENTS") + "(\(self.serviceCategoryGents.count))", for: .normal)
                                                            case "Ladies":
                                                                let serviceDataLadies = ServiceData(serviceID: shopService.id, serviceTitle: shopService.serviceTitle, serviceEstimatedTime: shopService.serviceEstimatedTotalTime, serviceCost: shopService.servicePrice, shortDescription: shopService.serviceDescription, category: shopService.serviceCategory, dateCreated: shopService.serviceDateCreated, timezone: shopService.serviceDateCreatedTimezone, calendar: shopService.serviceDateCreatedCalendar, local: shopService.serviceDateCreatedLocale, serviceImage: image, isHiddenByShop: shopService.serviceHiddenByShop, isHiddenByAdmin: shopService.serviceHiddenByAdmin, numberOfCustomer: 0, serviceStaff: shopService.serviceStaff, isAssigned: "NO", shop: shopService.serviceShop)
                                                                self.serviceCategoryLadies.append(serviceDataLadies)
                                                                self.serviceCategoryLadies.sort { (first, second) -> Bool in
                                                                    first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                }
                                                                self.ladiesButton.setTitle(NSLocalizedString("barberShopStaffAndServicesLadiesTabButton", comment: "LADIES") + "(\(self.serviceCategoryLadies.count))", for: .normal)
                                                            case "Kids":
                                                                let serviceDataKids = ServiceData(serviceID: shopService.id, serviceTitle: shopService.serviceTitle, serviceEstimatedTime: shopService.serviceEstimatedTotalTime, serviceCost: shopService.servicePrice, shortDescription: shopService.serviceDescription, category: shopService.serviceCategory, dateCreated: shopService.serviceDateCreated, timezone: shopService.serviceDateCreatedTimezone, calendar: shopService.serviceDateCreatedCalendar, local: shopService.serviceDateCreatedLocale, serviceImage: image, isHiddenByShop: shopService.serviceHiddenByShop, isHiddenByAdmin: shopService.serviceHiddenByAdmin, numberOfCustomer: 0, serviceStaff: shopService.serviceStaff, isAssigned: "NO", shop: shopService.serviceShop)
                                                                self.serviceCategoryKids.append(serviceDataKids)
                                                                self.serviceCategoryKids.sort { (first, second) -> Bool in
                                                                    first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                }
                                                                self.kidsButton.setTitle(NSLocalizedString("barberShopStaffAndServicesKidsTabButton", comment: "KIDS") + "(\(self.serviceCategoryKids.count))", for: .normal)
                                                            default:
                                                                print("Sky high")
                                                            }
                                                            
                                                            DispatchQueue.main.async {
                                                                self.collectionView.reloadData()
                                                            }
                                                            
                                                        } else {
                                                            if let image = UIImage(named: "avatar_icon") {
                                                                switch shopService.serviceCategory {
                                                                case "Gents":
                                                                    let serviceDataGents = ServiceData(serviceID: shopService.id, serviceTitle: shopService.serviceTitle, serviceEstimatedTime: shopService.serviceEstimatedTotalTime, serviceCost: shopService.servicePrice, shortDescription: shopService.serviceDescription, category: shopService.serviceCategory, dateCreated: shopService.serviceDateCreated, timezone: shopService.serviceDateCreatedTimezone, calendar: shopService.serviceDateCreatedCalendar, local: shopService.serviceDateCreatedLocale, serviceImage: image, isHiddenByShop: shopService.serviceHiddenByShop, isHiddenByAdmin: shopService.serviceHiddenByAdmin, numberOfCustomer: 0, serviceStaff: shopService.serviceStaff, isAssigned: "NO", shop: shopService.serviceShop)
                                                                    self.serviceCategoryGents.append(serviceDataGents)
                                                                    self.serviceOne.append(serviceDataGents)
                                                                    self.serviceOne.sort { (first, second) -> Bool in
                                                                        first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                    }
                                                                    self.serviceCategoryGents.sort { (first, second) -> Bool in
                                                                        first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                    }
                                                                    self.gentsButton.setTitle(NSLocalizedString("barberShopStaffAndServicesGentsTabButton", comment: "GENTS") + "(\(self.serviceCategoryGents.count))", for: .normal)
                                                                case "Ladies":
                                                                    let serviceDataLadies = ServiceData(serviceID: shopService.id, serviceTitle: shopService.serviceTitle, serviceEstimatedTime: shopService.serviceEstimatedTotalTime, serviceCost: shopService.servicePrice, shortDescription: shopService.serviceDescription, category: shopService.serviceCategory, dateCreated: shopService.serviceDateCreated, timezone: shopService.serviceDateCreatedTimezone, calendar: shopService.serviceDateCreatedCalendar, local: shopService.serviceDateCreatedLocale, serviceImage: image, isHiddenByShop: shopService.serviceHiddenByShop, isHiddenByAdmin: shopService.serviceHiddenByAdmin, numberOfCustomer: 0, serviceStaff: shopService.serviceStaff, isAssigned: "NO", shop: shopService.serviceShop)
                                                                    self.serviceCategoryLadies.append(serviceDataLadies)
                                                                    self.serviceCategoryLadies.sort { (first, second) -> Bool in
                                                                        first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                    }
                                                                    self.ladiesButton.setTitle(NSLocalizedString("barberShopStaffAndServicesLadiesTabButton", comment: "LADIES") + "(\(self.serviceCategoryLadies.count))", for: .normal)
                                                                case "Kids":
                                                                    let serviceDataKids = ServiceData(serviceID: shopService.id, serviceTitle: shopService.serviceTitle, serviceEstimatedTime: shopService.serviceEstimatedTotalTime, serviceCost: shopService.servicePrice, shortDescription: shopService.serviceDescription, category: shopService.serviceCategory, dateCreated: shopService.serviceDateCreated, timezone: shopService.serviceDateCreatedTimezone, calendar: shopService.serviceDateCreatedCalendar, local: shopService.serviceDateCreatedLocale, serviceImage: image, isHiddenByShop: shopService.serviceHiddenByShop, isHiddenByAdmin: shopService.serviceHiddenByAdmin, numberOfCustomer: 0, serviceStaff: shopService.serviceStaff, isAssigned: "NO", shop: shopService.serviceShop)
                                                                    self.serviceCategoryKids.append(serviceDataKids)
                                                                    self.serviceCategoryKids.sort { (first, second) -> Bool in
                                                                        first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                    }
                                                                    self.kidsButton.setTitle(NSLocalizedString("barberShopStaffAndServicesKidsTabButton", comment: "KIDS") + "(\(self.serviceCategoryKids.count))", for: .normal)
                                                                default:
                                                                    print("Sky high")
                                                                }
                                                                DispatchQueue.main.async {
                                                                    self.collectionView.reloadData()
                                                                }
                                                                
                                                            }
                                                        }
                                                        break
                                                    case .failure(let error):
                                                        print(error)
                                                        self.view.makeToast("Image not found", duration: 3.0, position: .bottom)
                                                        if let image = UIImage(named: "avatar_icon") {
                                                            switch shopService.serviceCategory {
                                                            case "Gents":
                                                                let serviceDataGents = ServiceData(serviceID: shopService.id, serviceTitle: shopService.serviceTitle, serviceEstimatedTime: shopService.serviceEstimatedTotalTime, serviceCost: shopService.servicePrice, shortDescription: shopService.serviceDescription, category: shopService.serviceCategory, dateCreated: shopService.serviceDateCreated, timezone: shopService.serviceDateCreatedTimezone, calendar: shopService.serviceDateCreatedCalendar, local: shopService.serviceDateCreatedLocale, serviceImage: image, isHiddenByShop: shopService.serviceHiddenByShop, isHiddenByAdmin: shopService.serviceHiddenByAdmin, numberOfCustomer: 0, serviceStaff: shopService.serviceStaff, isAssigned: "NO", shop: shopService.serviceShop)
                                                                self.serviceCategoryGents.append(serviceDataGents)
                                                                self.serviceOne.append(serviceDataGents)
                                                                self.serviceOne.sort { (first, second) -> Bool in
                                                                    first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                }
                                                                self.serviceCategoryGents.sort { (first, second) -> Bool in
                                                                    first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                }
                                                                self.gentsButton.setTitle(NSLocalizedString("barberShopStaffAndServicesGentsTabButton", comment: "GENTS") + "(\(self.serviceCategoryGents.count))", for: .normal)
                                                            case "Ladies":
                                                                let serviceDataLadies = ServiceData(serviceID: shopService.id, serviceTitle: shopService.serviceTitle, serviceEstimatedTime: shopService.serviceEstimatedTotalTime, serviceCost: shopService.servicePrice, shortDescription: shopService.serviceDescription, category: shopService.serviceCategory, dateCreated: shopService.serviceDateCreated, timezone: shopService.serviceDateCreatedTimezone, calendar: shopService.serviceDateCreatedCalendar, local: shopService.serviceDateCreatedLocale, serviceImage: image, isHiddenByShop: shopService.serviceHiddenByShop, isHiddenByAdmin: shopService.serviceHiddenByAdmin, numberOfCustomer: 0, serviceStaff: shopService.serviceStaff, isAssigned: "NO", shop: shopService.serviceShop)
                                                                self.serviceCategoryLadies.append(serviceDataLadies)
                                                                self.serviceCategoryLadies.sort { (first, second) -> Bool in
                                                                    first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                }
                                                                self.ladiesButton.setTitle(NSLocalizedString("barberShopStaffAndServicesLadiesTabButton", comment: "LADIES") + "(\(self.serviceCategoryLadies.count))", for: .normal)
                                                            case "Kids":
                                                                let serviceDataKids = ServiceData(serviceID: shopService.id, serviceTitle: shopService.serviceTitle, serviceEstimatedTime: shopService.serviceEstimatedTotalTime, serviceCost: shopService.servicePrice, shortDescription: shopService.serviceDescription, category: shopService.serviceCategory, dateCreated: shopService.serviceDateCreated, timezone: shopService.serviceDateCreatedTimezone, calendar: shopService.serviceDateCreatedCalendar, local: shopService.serviceDateCreatedLocale, serviceImage: image, isHiddenByShop: shopService.serviceHiddenByShop, isHiddenByAdmin: shopService.serviceHiddenByAdmin, numberOfCustomer: 0, serviceStaff: shopService.serviceStaff, isAssigned: "NO", shop: shopService.serviceShop)
                                                                self.serviceCategoryKids.append(serviceDataKids)
                                                                self.serviceCategoryKids.sort { (first, second) -> Bool in
                                                                    first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                }
                                                                self.kidsButton.setTitle(NSLocalizedString("barberShopStaffAndServicesKidsTabButton", comment: "KIDS") + "(\(self.serviceCategoryKids.count))", for: .normal)
                                                            default:
                                                                print("Sky high")
                                                            }
                                                            DispatchQueue.main.async {
                                                                self.collectionView.reloadData()
                                                            }
                                                            
                                                        }
                                                        break
                                                    }
                                                }
                                            }
                                        }
                                        
                                    } catch _ {
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                print("Shop serices not found")
                                            case 500:
                                                print("An error occurred, please try again later")
                                            default:
                                                print("sky high")
                                            }
                                        }
                                    }
                                }
                            })
                    }
                    
                }
            }
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    @objc private func handleGettingShopStaffs(){
        print("red one")
        if let UniqueShopID = self.currentSelectedShop {
            DispatchQueue.global(qos: .background).async {
                if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                    let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                    if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token {
                        let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                        let dateNow = DateInRegion()
                        print("expiry")
                        if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                            //self.handleUpdateUserAuthData()
                            DispatchQueue.main.async {
                                UtilityManager.alertView(view: self)
                            }
                        } else {
                            self.barberlistone.removeAll()
                            self.selectedBarberOne.removeAll()
                            DispatchQueue.main.async {
                                self.collectionView.reloadData()
                            }
                            
                            let headers: HTTPHeaders = [
                                "authorization": token
                            ]
                            AF.request(self.BACKEND_URL + "getshopstaff/" + UniqueShopID , method: .post, headers: headers)
                                .validate(statusCode: 200..<500)
                                .validate(contentType: ["application/json"]).response { (response) in
                                    if let error = response.error {
                                        print(error.localizedDescription)
                                        return
                                    }
                                    
                                    if let jsonData = response.data {
                                        do {
                                            let staffVustomerArrayData = try JSONDecoder().decode(StaffCustomerList.self, from: jsonData)
                                            
                                            for shopStaff in staffVustomerArrayData.staff {
                                                
                                                if(shopStaff.staffHiddenByAdmin == "NO") {
                                                    
                                                    if let customer = staffVustomerArrayData.customer.first(where: {$0.id == shopStaff.customer }) {
                                                        let profileImageName = customer.profileImageName
                                                        var ratingIneter = 0
                                                        if let val = Int(customer.customerStaffRating) {
                                                            ratingIneter = val
                                                        }
                                                        
                                                        AF.request(self.BACKEND_URL + "images/customerImages/" + profileImageName, headers: headers).responseData { response in
                                                            
                                                            switch response.result {
                                                            case .success(let data):
                                                                if let image = UIImage(data: data) {
                                                                    
                                                                    let staffData = StaffData(staffID: shopStaff.id, shopID: shopStaff.shop, email: customer.email, firstName: customer.firstName, lastName: customer.lastName, profileImage: image, rating: ratingIneter, staffHiddenShop: shopStaff.staffHiddenShop, staffHiddenByAdmin: shopStaff.staffHiddenByAdmin, dateAccountCreated: shopStaff.dateCreated, timezone: shopStaff.dateCreatedTimezone, calendar: shopStaff.dateCreatedCalendar, local: shopStaff.dateCreatedLocale, serviceStaff: shopStaff.staffService, staffCustomerId: shopStaff.customer, staffCustomerShortId: customer.shortUniqueID)
                                                                    self.barberlistone.append(staffData)
                                                                    self.barberlistone.sort { (first, second) -> Bool in
                                                                        first.firstName.lowercased() < second.firstName.lowercased()
                                                                    }
                                                                } else {
                                                                    if let image = UIImage(named: "avatar_icon"){
                                                                        let staffData = StaffData(staffID: shopStaff.id, shopID: shopStaff.shop, email: customer.email, firstName: customer.firstName, lastName: customer.lastName, profileImage: image, rating: ratingIneter, staffHiddenShop: shopStaff.staffHiddenShop, staffHiddenByAdmin: shopStaff.staffHiddenByAdmin, dateAccountCreated: shopStaff.dateCreated, timezone: shopStaff.dateCreatedTimezone, calendar: shopStaff.dateCreatedCalendar, local: shopStaff.dateCreatedLocale, serviceStaff: shopStaff.staffService, staffCustomerId: shopStaff.customer, staffCustomerShortId: customer.shortUniqueID)
                                                                        self.barberlistone.append(staffData)
                                                                        self.barberlistone.sort { (first, second) -> Bool in
                                                                            first.firstName.lowercased() < second.firstName.lowercased()
                                                                        }
                                                                    }
                                                                }
                                                                break
                                                            case .failure(let error):
                                                                print(error)
                                                                self.view.makeToast(NSLocalizedString("barberShopStaffAndServicesErrorMessageImageNotFound", comment: "Image not found"), duration: 3.0, position: .bottom)
                                                                if let image = UIImage(named: "avatar_icon"){
                                                                    let staffData = StaffData(staffID: shopStaff.id, shopID: shopStaff.shop, email: customer.email, firstName: customer.firstName, lastName: customer.lastName, profileImage: image, rating: ratingIneter, staffHiddenShop: shopStaff.staffHiddenShop, staffHiddenByAdmin: shopStaff.staffHiddenByAdmin, dateAccountCreated: shopStaff.dateCreated, timezone: shopStaff.dateCreatedTimezone, calendar: shopStaff.dateCreatedCalendar, local: shopStaff.dateCreatedLocale, serviceStaff: shopStaff.staffService, staffCustomerId: shopStaff.customer, staffCustomerShortId: customer.shortUniqueID)
                                                                    self.barberlistone.append(staffData)
                                                                    self.barberlistone.sort { (first, second) -> Bool in
                                                                        first.firstName.lowercased() < second.firstName.lowercased()
                                                                    }
                                                                }
                                                                break
                                                            }
                                                            
                                                            DispatchQueue.main.async {
                                                                self.collectionView.reloadData()
                                                            }
                                                            
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            
                                        } catch _ {
                                            
                                            if let statusCode = response.response?.statusCode {
                                                switch statusCode {
                                                case 404:
                                                    print("Shops staffs not found")
                                                case 500:
                                                    print("An error occurred, please try again later")
                                                default:
                                                    print("sky high")
                                                }
                                            }
                                            
                                        }
                                    }
                            }
                        }
                    }
                }
                
                
            }
        }
    }
    
    @objc func handleUpdateUserAuthData(){
        DispatchQueue.main.async {
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let customerId = decodedCustomer.first?.customerId, let shopOwner = decodedCustomer.first?.shopOwner, let timeZone = decodedCustomer.first?.timezone, let authType = decodedCustomer.first?.authType, let email = decodedCustomer.first?.email, let tokenData = decodedCustomer.first?.token, let autoSwitch = decodedCustomer.first?.autoShopSwitch {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let expiredTime = DateInRegion().convertTo(region: region) - 1.seconds
                    let expiredTimeString = expiredTime.toFormat("yyyy-MM-dd HH:mm:ss")
                    
                    let teams = [CustomerToken(expiresIn: expiredTimeString, customerId: customerId, token: tokenData, timezone: timeZone, shopOwner: shopOwner, authType: authType, email: email, autoShopSwitch: autoSwitch)]
                    
                    let userDefaults = UserDefaults.standard
                    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: teams)
                    userDefaults.set(encodedData, forKey: "token")
                    userDefaults.synchronize()
                    LoginManager().logOut()
                    let welcomeviewcontroller = WelcomeViewController()
                    self.present(welcomeviewcontroller, animated: true, completion: nil)
                    
                }
                
            }
        }
    }
    
    @objc func handleServiceBarberChange(){
        if self.serviceBarberSegmentedControl.selectedSegmentIndex == 1 {
            self.addBarberAddServiceButton.removeTarget(nil, action: nil, for: .allEvents)
            self.addBarberAddServiceButton.setTitle(NSLocalizedString("barberShopStaffAndServicesAddServiceButton", comment: "ADD SERVICE"), for: .normal)
            self.addBarberAddServiceButton.addTarget(self, action: #selector(handleAddingService), for: .touchUpInside)
            self.selectedBarberOne.removeAll()
            
            self.categoryButtonContainerView.isHidden = false
            self.categoryButtonContainerViewHeightAnchor?.isActive = false
            self.categoryButtonContainerViewHeightAnchor = categoryButtonContainerView.heightAnchor.constraint(equalToConstant: 40)
            self.categoryButtonContainerViewHeightAnchor?.isActive = true
            
            self.ladiesButton.isHidden = false
            self.ladiesButton.removeTarget(nil, action: nil, for: .allEvents)
            self.ladiesButton.setTitle(NSLocalizedString("barberShopStaffAndServicesLadiesTabButton", comment: "LADIES") + "(\(self.serviceCategoryLadies.count))", for: .normal)
            self.ladiesButton.addTarget(self, action: #selector(handleShowLadiesList), for: .touchUpInside)
            
            self.gentsButton.isHidden = false
            self.gentsButton.setTitle(NSLocalizedString("barberShopStaffAndServicesGentsTabButton", comment: "GENTS") + "(\(self.serviceCategoryGents.count))", for: .normal)
            
            self.kidsButton.isHidden = false
            self.kidsButton.removeTarget(nil, action: nil, for: .allEvents)
            self.kidsButton.setTitle(NSLocalizedString("barberShopStaffAndServicesKidsTabButton", comment: "KIDS") + "(\(self.serviceCategoryKids.count))", for: .normal)
            self.kidsButton.addTarget(self, action: #selector(handleShowKidsList), for: .touchUpInside)
            self.changeButtonCounter()
            
        }else {
            
            self.addBarberAddServiceButton.removeTarget(nil, action: nil, for: .allEvents)
            self.addBarberAddServiceButton.setTitle(NSLocalizedString("barberShopStaffAndServicesAddStaffButton", comment: "ADD STAFF"), for: .normal)
            self.addBarberAddServiceButton.addTarget(self, action: #selector(handleAddingBarber), for: .touchUpInside)
            
            self.selectedServiceOne.removeAll()
            self.ladiesSelectedList.removeAll()
            self.gentsSelectedList.removeAll()
            self.kidsSelectedList.removeAll()
            
            self.categoryButtonContainerView.isHidden = true
            self.categoryButtonContainerViewHeightAnchor?.isActive = false
            self.categoryButtonContainerViewHeightAnchor = categoryButtonContainerView.heightAnchor.constraint(equalToConstant: 0)
            self.categoryButtonContainerViewHeightAnchor?.isActive = true
            
        }
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    @objc func handleShowLadiesList(){
        self.selectedLadiesGentsKidsButton = "Ladies"
        self.ladiesButtonSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
        self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
        if self.serviceCategoryLadies.count > 0 {
            
            self.serviceOne.removeAll()
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            self.serviceOne.append(contentsOf: self.serviceCategoryLadies)
            print("number of items", self.serviceOne.count)
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        } else {
            self.serviceOne.removeAll()
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    @objc func handleShowGentsList(){
        self.selectedLadiesGentsKidsButton = "Gents"
        self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
        self.gentsButtonSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
        if self.serviceCategoryGents.count > 0 {
            
            self.serviceOne.removeAll()
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            self.serviceOne.append(contentsOf: self.serviceCategoryGents)
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        } else {
            self.serviceOne.removeAll()
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
        
    }
    
    @objc func handleShowKidsList(){
        self.selectedLadiesGentsKidsButton = "Kids"
        self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
        self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
        self.kidsButtonSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        
        if self.serviceCategoryKids.count > 0 {
            self.serviceOne.removeAll()
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            self.serviceOne.append(contentsOf: self.serviceCategoryKids)
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
        } else {
            self.serviceOne.removeAll()
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
        
    }
    
    func changeButtonCounter(){
        print(self.selectedLadiesGentsKidsButton)
        switch self.selectedLadiesGentsKidsButton {
        case "Ladies":
            let buttonTitle = "LADIES" + "(\(self.serviceCategoryLadies.count))"
            self.ladiesButton.setTitle(buttonTitle, for: .normal)
            self.selectedLadiesGentsKidsButton = "Ladies"
            self.ladiesButtonSeperatorView.backgroundColor = UIColor.black
            self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
            self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
            
        case "Gents":
            let buttonTitle = "GENTS" + "(\(self.serviceCategoryGents.count))"
            self.gentsButton.setTitle(buttonTitle, for: .normal)
            self.selectedLadiesGentsKidsButton = "Gents"
            self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
            self.gentsButtonSeperatorView.backgroundColor = UIColor.black
            self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
        case "Kids":
            let buttonTitle = "KIDS" + "(\(self.serviceCategoryKids.count))"
            self.kidsButton.setTitle(buttonTitle, for: .normal)
            self.selectedLadiesGentsKidsButton = "Kids"
            self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
            self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
            self.kidsButtonSeperatorView.backgroundColor = UIColor.black
        default:
            print("Sky high")
        }
    }
    
    func handleBarberMoveToNextView(staffCustomerUniqueShortID: String, staffUniqueID: String, shopID: String, staffHiddenStat: String ){
        let addBarber = AddBarberViewController()
        addBarber.actionStatusForView = "editStaff"
        addBarber.StaffcustomerUniqueID = staffCustomerUniqueShortID
        addBarber.staffUniqueID = staffUniqueID
        addBarber.currentShopID = shopID
        addBarber.staffHiddenStatus = staffHiddenStat
        addBarber.modalPresentationStyle = .overCurrentContext
        present(addBarber, animated: true, completion: nil)
    }
    
    func handleServiceMoveToNextView(serviceUniqueID: String, shopID: String, serviceHiddenStat: String, currency: String ){
        let addService = AddServiceViewController()
        addService.actionStatusForView = "editService"
        addService.shopID = shopID
        addService.serviceID = serviceUniqueID
        addService.serviceHiddenStatus = serviceHiddenStat
        addService.shopCurrency = currency
        addService.modalPresentationStyle = .overCurrentContext
        present(addService, animated: true, completion: nil)
    }
    
    var categoryButtonContainerViewHeightAnchor: NSLayoutConstraint?
    
    func setupViewConstriants(){
        var topDistance: CGFloat = 20
        var bottomDistance: CGFloat = -10
        if UIDevice.current.hasNotch {
            topDistance = 60
            bottomDistance = -20
        }
        
        
        upperInputsContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(staffServiceShopPicker)
        upperInputsContainerView.addSubview(staffServiceViewTitlePlaceHolder)
        upperInputsContainerView.addSubview(staffServiceCloseViewButton)
        
        staffServiceShopPicker.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        staffServiceShopPicker.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor).isActive = true
        staffServiceShopPicker.widthAnchor.constraint(equalToConstant: 45).isActive = true
        staffServiceShopPicker.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        staffServiceCloseViewButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        staffServiceCloseViewButton.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        staffServiceCloseViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        staffServiceCloseViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        staffServiceViewTitlePlaceHolder.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        staffServiceViewTitlePlaceHolder.leftAnchor.constraint(equalTo: staffServiceShopPicker.rightAnchor, constant: 10).isActive = true
        staffServiceViewTitlePlaceHolder.rightAnchor.constraint(equalTo: staffServiceCloseViewButton.leftAnchor).isActive = true
        staffServiceViewTitlePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        barberShopHeaderDetailsContainerView.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 5).isActive = true
        barberShopHeaderDetailsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        barberShopHeaderDetailsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5, constant: -24).isActive = true
        barberShopHeaderDetailsContainerView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        barberShopHeaderDetailsContainerView.addSubview(selectImageView)
        barberShopHeaderDetailsContainerView.addSubview(addBarberAddServiceButton)
        
        selectImageView.topAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.topAnchor, constant: 5).isActive = true
        selectImageView.leftAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.leftAnchor, constant: 5).isActive = true
        selectImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        selectImageView.heightAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.heightAnchor, constant: -10).isActive = true
        
        addBarberAddServiceButton.topAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.topAnchor, constant: 5).isActive = true
        addBarberAddServiceButton.leftAnchor.constraint(equalTo: selectImageView.rightAnchor, constant: 10).isActive = true
        addBarberAddServiceButton.rightAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.rightAnchor, constant: 5).isActive = true
        addBarberAddServiceButton.bottomAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.bottomAnchor, constant: -5).isActive = true
        
        serviceBarberSegmentedControl.topAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.bottomAnchor, constant: 10).isActive = true
        serviceBarberSegmentedControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        serviceBarberSegmentedControl.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        serviceBarberSegmentedControl.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        categoryButtonContainerView.topAnchor.constraint(equalTo: serviceBarberSegmentedControl.bottomAnchor, constant: 10).isActive = true
        categoryButtonContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        categoryButtonContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        categoryButtonContainerViewHeightAnchor = categoryButtonContainerView.heightAnchor.constraint(equalToConstant: 0)
        categoryButtonContainerViewHeightAnchor?.isActive = true
        
        categoryButtonContainerView.addSubview(ladiesButton)
        categoryButtonContainerView.addSubview(ladiesButtonSeperatorView)
        categoryButtonContainerView.addSubview(gentsButton)
        categoryButtonContainerView.addSubview(gentsButtonSeperatorView)
        categoryButtonContainerView.addSubview(kidsButton)
        categoryButtonContainerView.addSubview(kidsButtonSeperatorView)
        
        ladiesButton.topAnchor.constraint(equalTo: categoryButtonContainerView.topAnchor).isActive = true
        ladiesButton.leftAnchor.constraint(equalTo: categoryButtonContainerView.leftAnchor).isActive = true
        ladiesButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        ladiesButton.heightAnchor.constraint(equalTo: categoryButtonContainerView.heightAnchor, constant: -2).isActive = true
        
        ladiesButtonSeperatorView.topAnchor.constraint(equalTo: ladiesButton.bottomAnchor).isActive = true
        ladiesButtonSeperatorView.leftAnchor.constraint(equalTo: categoryButtonContainerView.leftAnchor).isActive = true
        ladiesButtonSeperatorView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        ladiesButtonSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        kidsButton.topAnchor.constraint(equalTo: categoryButtonContainerView.topAnchor).isActive = true
        kidsButton.rightAnchor.constraint(equalTo: categoryButtonContainerView.rightAnchor).isActive = true
        kidsButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        kidsButton.heightAnchor.constraint(equalTo: categoryButtonContainerView.heightAnchor, constant: -2).isActive = true
        
        kidsButtonSeperatorView.topAnchor.constraint(equalTo: kidsButton.bottomAnchor).isActive = true
        kidsButtonSeperatorView.rightAnchor.constraint(equalTo: categoryButtonContainerView.rightAnchor).isActive = true
        kidsButtonSeperatorView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        kidsButtonSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        gentsButton.topAnchor.constraint(equalTo: categoryButtonContainerView.topAnchor).isActive = true
        gentsButton.widthAnchor.constraint(equalToConstant: 110).isActive = true
        gentsButton.leftAnchor.constraint(equalTo: ladiesButton.rightAnchor).isActive = true
        gentsButton.rightAnchor.constraint(equalTo: kidsButton.leftAnchor).isActive = true
        gentsButton.heightAnchor.constraint(equalTo: categoryButtonContainerView.heightAnchor, constant: -2).isActive = true
        
        gentsButtonSeperatorView.topAnchor.constraint(equalTo: gentsButton.bottomAnchor).isActive = true
        gentsButtonSeperatorView.widthAnchor.constraint(equalToConstant: 110).isActive = true
        gentsButtonSeperatorView.leftAnchor.constraint(equalTo: ladiesButtonSeperatorView.rightAnchor).isActive = true
        gentsButtonSeperatorView.rightAnchor.constraint(equalTo: kidsButtonSeperatorView.leftAnchor).isActive = true
        gentsButtonSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        self.categoryButtonContainerView.isHidden = true
        
        curvedCollectView.topAnchor.constraint(equalTo: categoryButtonContainerView.bottomAnchor, constant: 5).isActive = true
        curvedCollectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        curvedCollectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -8).isActive = true
        curvedCollectView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: bottomDistance).isActive = true
        
        curvedCollectView.addSubview(collectView)
        
        collectView.topAnchor.constraint(equalTo: curvedCollectView.topAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: curvedCollectView.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: curvedCollectView.widthAnchor, multiplier: 1, constant: -10).isActive = true
        collectView.bottomAnchor.constraint(equalTo: curvedCollectView.bottomAnchor, constant: -5).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
    }
}

class customBarbersBarberShopProfileCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    let barberShopCoverImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "babershop0")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.layer.borderColor = UIColor.clear.cgColor
        imageView.layer.borderWidth = 3
        return imageView
    }()
    
    let textBackgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        return view
    }()
    
    let barberShopServiceLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let barberShopServiceNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let barberShopServiceDescriptionPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let serviceTimeTakenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 11)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let servicePricePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 11)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let thumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.clear
        tniv.contentMode = .scaleAspectFit
        return tniv
    }()
    
    func setupViews(){
        addSubview(barberShopCoverImageView)
        addSubview(textBackgroundView)
        addSubview(seperatorView)
        addSubview(barberShopServiceLogoImageView)
        addSubview(barberShopServiceNamePlaceHolder)
        addSubview(barberShopServiceDescriptionPlaceHolder)
        addSubview(serviceTimeTakenPlaceHolder)
        addSubview(servicePricePlaceHolder)
        addSubview(thumbnailImageView)
        thumbnailImageView.image = UIImage()
        
        addContraintsWithFormat(format: "H:|[v0]|", views: barberShopCoverImageView)
        addContraintsWithFormat(format: "V:|[v0][v1(5)]|", views: barberShopCoverImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: textBackgroundView)
        addContraintsWithFormat(format: "V:|[v0]-5-|", views: textBackgroundView)
        addContraintsWithFormat(format: "H:|-30-[v0(100)]|", views: barberShopServiceLogoImageView)
        addContraintsWithFormat(format: "V:|[v0]-5-|", views: barberShopServiceLogoImageView)
        addContraintsWithFormat(format: "H:|-150-[v0]-30-|", views: barberShopServiceNamePlaceHolder)
        addContraintsWithFormat(format: "V:|-5-[v0(20)][v1]-30-|", views: barberShopServiceNamePlaceHolder,barberShopServiceDescriptionPlaceHolder)
        addContraintsWithFormat(format: "H:|-150-[v0]-30-|", views: barberShopServiceDescriptionPlaceHolder)
        addContraintsWithFormat(format: "H:|-150-[v0(50)][v1(50)]-10-[v2(20)]-5-|", views: serviceTimeTakenPlaceHolder,servicePricePlaceHolder,thumbnailImageView)
        addContraintsWithFormat(format: "V:|-70-[v0(20)]-5-|", views: serviceTimeTakenPlaceHolder)
        addContraintsWithFormat(format: "V:|-70-[v0(20)]-5-|", views: servicePricePlaceHolder)
        addContraintsWithFormat(format: "V:|-70-[v0(20)]-5-|", views: thumbnailImageView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class customBarbersBarberShopProfileCollectionViewBarberSelectedCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    let barberShopBarberCoverImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "babershop0")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.layer.borderColor = UIColor.clear.cgColor
        imageView.layer.borderWidth = 3
        return imageView
    }()
    
    let textBackgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        return view
    }()
    
    let barberShopBarberLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let barberShopBarberNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let barberAvailableFromPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let barberTimeToBeAvailablePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let starView = StarRatingView()
    let attribute = StarRatingAttribute(type: .rate,point: 15,spacing: 5, emptyColor: .black, fillColor: .white)
    
    let thumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.clear
        tniv.contentMode = .scaleAspectFit
        return tniv
    }()
    
    func setupViews(){
        addSubview(barberShopBarberCoverImageView)
        addSubview(textBackgroundView)
        addSubview(seperatorView)
        addSubview(barberShopBarberLogoImageView)
        addSubview(barberShopBarberNamePlaceHolder)
        addSubview(barberAvailableFromPlaceHolder)
        addSubview(barberTimeToBeAvailablePlaceHolder)
        starView.configure(attribute, current: 3, max: 5)
        starView.translatesAutoresizingMaskIntoConstraints = false
        starView.isUserInteractionEnabled = false
        addSubview(starView)
        addSubview(thumbnailImageView)
        
        addContraintsWithFormat(format: "H:|[v0]|", views: barberShopBarberCoverImageView)
        addContraintsWithFormat(format: "V:|[v0][v1(5)]|", views: barberShopBarberCoverImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: textBackgroundView)
        addContraintsWithFormat(format: "V:|[v0]-5-|", views: textBackgroundView)
        addContraintsWithFormat(format: "H:|-30-[v0(100)]|", views: barberShopBarberLogoImageView)
        addContraintsWithFormat(format: "V:|[v0]-5-|", views: barberShopBarberLogoImageView)
        addContraintsWithFormat(format: "H:|-150-[v0]-30-|", views: barberShopBarberNamePlaceHolder)
        addContraintsWithFormat(format: "H:|-150-[v0]-30-|", views: barberAvailableFromPlaceHolder)
        addContraintsWithFormat(format: "H:|-150-[v0]-30-|", views: barberTimeToBeAvailablePlaceHolder)
        addContraintsWithFormat(format: "H:|-170-[v0]-5-[v1(20)]-5-|", views: starView, thumbnailImageView)
        addContraintsWithFormat(format: "V:|-5-[v0(30)][v1(20)][v2(20)][v3(20)]-5-|", views: barberShopBarberNamePlaceHolder,barberAvailableFromPlaceHolder,barberTimeToBeAvailablePlaceHolder,starView)
        addContraintsWithFormat(format: "V:|-70-[v0(20)]-5-|", views: thumbnailImageView)
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension BarberShopBarbersAndServicesViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.serviceBarberSegmentedControl.selectedSegmentIndex == 1 {
            return self.serviceOne.count
        }else {
            return self.barberlistone.count
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if self.serviceBarberSegmentedControl.selectedSegmentIndex == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdd", for: indexPath) as! customServiceCollectionViewCell
            cell.barberShopServiceNamePlaceHolder.text = self.serviceOne[indexPath.row].serviceTitle
            cell.barberShopServiceDescriptionPlaceHolder.text = self.serviceOne[indexPath.row].shortDescription
            cell.servicePricePlaceHolder.text = self.serviceOne[indexPath.row].serviceCost + " " + self.currentSelectedShopCurrency!
            cell.serviceTimeTakenPlaceHolder.text = self.serviceOne[indexPath.row].serviceEstimatedTime + " min"
            cell.barberShopServiceLogoImageView.image = self.serviceOne[indexPath.row].serviceImage
            cell.upButton.isHidden = true
            cell.numberOfCustomerPlaceHolder.isHidden = true
            cell.downButton.isHidden = true
            return cell
        }else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdd2", for: indexPath) as! customStaffCollectionViewCell
            cell.barberShopBarberLogoImageView.image = self.barberlistone[indexPath.row].profileImage
            cell.barberShopBarberNamePlaceHolder.text = self.barberlistone[indexPath.row].firstName
            cell.starView.current = CGFloat(integerLiteral: self.barberlistone[indexPath.row].rating)
            return cell
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if self.serviceBarberSegmentedControl.selectedSegmentIndex == 1 {
            if let cell = self.serviceOne[safe: indexPath.row] {
                let dataReel = cell.shortDescription
                let approximateWidthOfDescription = view.frame.width - 124 // - 5 - 60 // - 5 - 5 - 40 - 5
                let size = CGSize(width: approximateWidthOfDescription, height: 1000)
                let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)]
                let estimatedFrame = NSString(string: dataReel).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
                
                return CGSize(width: collectView.frame.width, height: estimatedFrame.height + 140)
                
            }
            return CGSize(width: collectView.frame.width, height: 110)
        } else {
            return CGSize(width: collectView.frame.width, height: 110)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.serviceBarberSegmentedControl.selectedSegmentIndex == 1 {
            self.selectedServiceOne.removeAll()
            self.ladiesSelectedList.removeAll()
            self.gentsSelectedList.removeAll()
            self.kidsSelectedList.removeAll()
            guard let selSerID =  self.serviceOne[safe: indexPath.row]?.serviceID, let shopString = self.serviceOne[safe: indexPath.row]?.shop, let stat = self.serviceOne[safe: indexPath.row]?.isHiddenByShop, let curr = self.currentSelectedShopCurrency else {
                return
            }
            
            self.handleServiceMoveToNextView(serviceUniqueID: selSerID, shopID: shopString, serviceHiddenStat: stat, currency: curr)
            
        } else {
            
            self.selectedBarberOne.removeAll()
            guard let selSerID =  self.barberlistone[safe: indexPath.row]?.staffCustomerShortId, let shortUniqueID = self.barberlistone[safe: indexPath.row]?.staffID, let shopIDD = self.barberlistone[safe: indexPath.row]?.shopID, let hiddenStatus = self.barberlistone[safe: indexPath.row]?.staffHiddenShop else {
                return
            }
            
            if self.selectedBarberOne.contains(selSerID) {
                print("No Show")
            }else {
                self.selectedBarberOne.append(selSerID)
            }
            
            self.handleBarberMoveToNextView(staffCustomerUniqueShortID: selSerID, staffUniqueID: shortUniqueID, shopID: shopIDD, staffHiddenStat: hiddenStatus)
            
        }
    }
    
    
}


















