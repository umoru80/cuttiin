//
//  ShopServiceCreated.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 18/02/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation

struct ShopServiceCreated: Codable {
    let message: String
    let shopService: ShopService
}

struct ShopService: Codable {
    let v: Int
    let id, serviceCategory, serviceDateCreated, serviceDateCreatedCalendar: String
    let serviceDateCreatedLocale, serviceDateCreatedTimezone, serviceDescription, serviceEstimatedTotalTime: String
    let serviceHiddenByAdmin, serviceHiddenByShop, serviceImageName: String
    let serviceImageURL: String
    let servicePrice, serviceShop: String
    let serviceStaff: [String]
    let serviceTitle: String
    
    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case serviceCategory, serviceDateCreated, serviceDateCreatedCalendar, serviceDateCreatedLocale, serviceDateCreatedTimezone, serviceDescription, serviceEstimatedTotalTime, serviceHiddenByAdmin, serviceHiddenByShop, serviceImageName
        case serviceImageURL = "serviceImageUrl"
        case servicePrice, serviceShop, serviceStaff, serviceTitle
    }
}
