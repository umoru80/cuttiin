//
//  MainNavigationController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/20/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Alamofire
import SwiftyJSON
import Toast_Swift
import SwiftDate

class MainNavigationController: UINavigationController {
    var calledOnce = true
    let network: NetworkManager = NetworkManager.sharedInstance
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        self.navigationBar.isHidden = true
        
        
        NetworkManager.isUnreachable { _ in
            self.view.makeToast("No internet connection", duration: 2.0, position: .bottom)
        }
        
        NetworkManager.isReachable { _ in
            print("naija")
            self.fetchCustomerRole()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .didReceiveDataMainNavigationController, object: nil)
    }
    
    @objc private func onDidReceiveData(_ notification:Notification) {
        print("call made")
        self.fetchCustomerRole()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func fetchCustomerRole() {
        print("hello before options")
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let shopOwner = decodedCustomer.first?.shopOwner, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let autoSwitch = decodedCustomer.first?.autoShopSwitch {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                print("hello there")
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region) {
                    if expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        self.handleAllLogOut()
                        self.perform(#selector(self.showLoginView), with: self, afterDelay: 0.03)
                        
                    } else {
                        print("customer logged in")
                        print(shopOwner)
                        
                        if (shopOwner == "YES") {
                            switch(autoSwitch){
                            case "YES":
                                print("red blue")
                                self.viewControllers.removeAll()
                                let customerController = AppointmentsViewController()
                                self.viewControllers = [customerController]
                                return
                            case "NO":
                                print("red green")
                                self.viewControllers.removeAll()
                                let customerController = BarberShopsViewController()
                                self.viewControllers = [customerController]
                                let someDict = ["shopUniqueId" : "id" ]
                                NotificationCenter.default.post(name: .didReceiveDataCustomerProfileViewRefresher, object: nil, userInfo: someDict)
                                return
                            default:
                                self.handleAllLogOut()
                                self.perform(#selector(self.showLoginView), with: self, afterDelay: 0.03)
                                return
                                
                            }
                        } else {
                            print("customer logged in not shop owner")
                            let customerController = BarberShopsViewController()
                            self.viewControllers = [customerController]
                            let someDict = ["shopUniqueId" : "id" ]
                            NotificationCenter.default.post(name: .didReceiveDataCustomerProfileViewRefresher, object: nil, userInfo: someDict)
                        }
                    }
                } else {
                    print("date data all fucked up")
                    self.handleAllLogOut()
                    self.perform(#selector(self.showLoginView), with: self, afterDelay: 0.03)
                    
                }
                
                
                
            } else {
                print("no auth data")
                self.handleAllLogOut()
                self.perform(#selector(self.showLoginView), with: self, afterDelay: 0.03)
                
            }
            
        } else {
            print("no auth completely soho")
            self.handleAllLogOut()
            self.perform(#selector(self.showLoginView), with: self, afterDelay: 0.03)
        }
    }
    
    @objc func showLoginView(){
        print("wellness")
        let welcomeviewcontroller = WelcomeViewController()
        welcomeviewcontroller.mainViewController = self
        welcomeviewcontroller.modalPresentationStyle = .overCurrentContext
        self.present(welcomeviewcontroller, animated: true, completion: nil)
    }
    
    
    func handleAllLogOut(){
        LoginManager().logOut()
    }
}
