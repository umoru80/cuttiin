//
//  ResettlementViewController.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 21/04/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit
import SwiftDate
import SwiftSpinner
import Alamofire

class ResettlementViewController: UIViewController {
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    var unpaidAppointmentDataArray = [UnpaidAppointmentData]()
    var shopCurrency: String?
    
    let viewTitleNameLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = "Outstanding payments"
        ht.font = UIFont(name: "OpenSans-Light", size: 20)
        ht.textColor = UIColor.black
        ht.numberOfLines = 0
        ht.adjustsFontSizeToFitWidth = true
        ht.minimumScaleFactor = 0.1
        ht.baselineAdjustment = .alignCenters
        ht.textAlignment = .center
        return ht
    }()
    
    lazy var closeViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeOrderDetailView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    lazy var initialDescriptionContanerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let curvedCollectViewHolder: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        cview.layer.cornerRadius = 5
        return cview
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.alwaysBounceVertical = true
        cv.backgroundColor = UIColor.clear
        cv.register(customUnpaidCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()
    
    lazy var paymentSectionSeperator: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return view
    }()
    
    
    
    let totalAmonuntTitleLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = "Total:"
        ht.font = UIFont(name: "OpenSans-Light", size: 20)
        ht.textColor = UIColor.black
        ht.numberOfLines = 0
        ht.adjustsFontSizeToFitWidth = true
        ht.minimumScaleFactor = 0.1
        ht.baselineAdjustment = .alignCenters
        ht.textAlignment = .center
        return ht
    }()
    
    lazy var mpesaLogoViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "mpesalogo")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let paymentDetailsTitleLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = "Payment can be made via M-PESA - Pay Bill"
        ht.font = UIFont(name: "OpenSans-Light", size: 15)
        ht.textColor = UIColor.black
        ht.numberOfLines = 0
        ht.adjustsFontSizeToFitWidth = true
        ht.minimumScaleFactor = 0.1
        ht.baselineAdjustment = .alignCenters
        ht.textAlignment = .center
        return ht
    }()
    
    let businessNumberTitleLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = "Till no: "
        ht.font = UIFont(name: "OpenSans-Light", size: 15)
        ht.textColor = UIColor.black
        ht.numberOfLines = 0
        ht.adjustsFontSizeToFitWidth = true
        ht.minimumScaleFactor = 0.1
        ht.baselineAdjustment = .alignCenters
        ht.textAlignment = .center
        return ht
    }()
    
    let accountNumberTitleLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = "Account no: "
        ht.font = UIFont(name: "OpenSans-Light", size: 15)
        ht.textColor = UIColor.black
        ht.numberOfLines = 0
        ht.adjustsFontSizeToFitWidth = true
        ht.minimumScaleFactor = 0.1
        ht.baselineAdjustment = .alignCenters
        ht.textAlignment = .center
        return ht
    }()

    
    lazy var DoneWithViewButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        st.setTitle("Confirm payment", for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 15)
        st.tag = 1
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.addTarget(self, action: #selector(confirmPayment), for: .touchUpInside)
        return st
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        view.addSubview(viewTitleNameLabel)
        view.addSubview(closeViewButton)
        view.addSubview(initialDescriptionContanerView)
        view.addSubview(curvedCollectViewHolder)
        view.addSubview(DoneWithViewButton)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewContraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    @objc private func closeOrderDetailView(){
        //alert view
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func confirmPayment(){
        let alertController = UIAlertController(title: "Confirm payment", message: "Please insert your mobile number from which you made the payment.", preferredStyle: .alert)
        
        alertController.addTextField { (textField) in
            textField.placeholder = "mobile number"
        }
        
        let action1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
            //func call
        }
        
        let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in
            print("You've pressed cancel");
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func getUnpaidAppointments(){
        DispatchQueue.global(qos: .background).async {
            
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let customer = decodedCustomer.first?.customerId, let curr = self.shopCurrency {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        
                        self.unpaidAppointmentDataArray.removeAll()
                        
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        let parameters: Parameters = [
                            "shopCurrency": curr,
                        ]
                        
                        AF.request(self.BACKEND_URL + "getAllCancelledAppointmentByCustomer/" + customer, method: .post, parameters: parameters, encoding: JSONEncoding(options: []), headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"])
                            .response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    
                                    do {
                                        let unpaindAppointmentDatax = try JSONDecoder().decode(UnpaidAppointment.self, from: jsonData)
                                        
                                        let cancelledAppointmentData = unpaindAppointmentDatax.appointmentCancelled
                                        let appointmentData = unpaindAppointmentDatax.appointment
                                        let shopData = unpaindAppointmentDatax.shop
                                        
                                        var chargesArrayHolder = [Double]()
                                        for cancel in cancelledAppointmentData{
                                            if cancel.appointmentCancelledStatus == "unpaid", let priceDouble = Double(cancel.appointmentCancelledCharges) {
                                                print(priceDouble)
                                                chargesArrayHolder.append(priceDouble)
                                                
                                                if let firstAppointment = appointmentData.first(where: {$0.id == cancel.appointment}), let firstShop = shopData.first(where: {$0.id == firstAppointment.appointmentShop}) {
                                                    
                                                    let shopLogoImageName = firstShop.shopLogoImageName
                                                    print("shopImageName: ", shopLogoImageName)
                                                    print("shopName: ", firstShop.shopName)
                                                    print("shopID: ", firstShop.id)
                                                    
                                                    //71AB9512-B443-4482-AA11-C64A40CE9E70.jpg
                                                    
                                                    AF.request(self.BACKEND_URL + "images/shopImages/" + shopLogoImageName, headers: headers).responseData { response in
                                                        
                                                        switch response.result {
                                                        case .success(let data):
                                                            if let image = UIImage(data: data) {
                                                                
                                                                let unpaidData = UnpaidAppointmentData(shopID: firstShop.id, shopNameID: firstShop.shopName, shopMobileNumber: firstShop.shopMobileNumber, shopEmail: firstShop.shopEmail, shopAddress: firstShop.shopAddress, shopLogoImage: image, appointmentID: firstAppointment.id, appointmentShortUniqueID: firstAppointment.appointmentShortUniqueID, appointmentDateStart: firstAppointment.appointmentDateStart, appointmentDateEnd: firstAppointment.appointmentDateEnd, appointmentStatus: firstAppointment.appointmentStatus, appointmentDateCreated: firstAppointment.appointmentDateCreated, appointmentDateTimezone: firstAppointment.appointmentDateTimezone, appointmentDateCalendar: firstAppointment.appointmentDateCalendar, appointmentDateLocale: firstAppointment.appointmentPaidLocale, appointmentCancelledID: cancel.id, appointmentCancelledCharges: cancel.appointmentCancelledCharges, initiator: cancel.initiator, appointmentCancelledReason: cancel.appointmentCancelledReason, appointmentCancelledStatus: cancel.appointmentCancelledStatus, appointmentCancelledTimezone: cancel.appointmentCancelledTimezone, appointmentCancelledCalendar: cancel.appointmentCancelledCalendar, appointmentCancelledLocale: cancel.appointmentCancelledLocale, appointmentCurrency: firstAppointment.appointmentCurrency)
                                                                self.unpaidAppointmentDataArray.append(unpaidData)
                                                                DispatchQueue.main.async {
                                                                    self.collectionView.reloadData()
                                                                }
                                                                
                                                            } else {
                                                                print("hello no image")
                                                                if let blankImage = UIImage(named: "avatar_icon"){
                                                                    let unpaidData = UnpaidAppointmentData(shopID: firstShop.id, shopNameID: firstShop.shopName, shopMobileNumber: firstShop.shopMobileNumber, shopEmail: firstShop.shopEmail, shopAddress: firstShop.shopAddress, shopLogoImage: blankImage, appointmentID: firstAppointment.id, appointmentShortUniqueID: firstAppointment.appointmentShortUniqueID, appointmentDateStart: firstAppointment.appointmentDateStart, appointmentDateEnd: firstAppointment.appointmentDateEnd, appointmentStatus: firstAppointment.appointmentStatus, appointmentDateCreated: firstAppointment.appointmentDateCreated, appointmentDateTimezone: firstAppointment.appointmentDateTimezone, appointmentDateCalendar: firstAppointment.appointmentDateCalendar, appointmentDateLocale: firstAppointment.appointmentPaidLocale, appointmentCancelledID: cancel.id, appointmentCancelledCharges: cancel.appointmentCancelledCharges, initiator: cancel.initiator, appointmentCancelledReason: cancel.appointmentCancelledReason, appointmentCancelledStatus: cancel.appointmentCancelledStatus, appointmentCancelledTimezone: cancel.appointmentCancelledTimezone, appointmentCancelledCalendar: cancel.appointmentCancelledCalendar, appointmentCancelledLocale: cancel.appointmentCancelledLocale, appointmentCurrency: firstAppointment.appointmentCurrency)
                                                                    self.unpaidAppointmentDataArray.append(unpaidData)
                                                                    DispatchQueue.main.async {
                                                                        self.collectionView.reloadData()
                                                                    }
                                                                }
                                                            }
                                                            break
                                                        case .failure(let error):
                                                            print(error)
                                                            self.view.makeToast("Image not found", duration: 3.0, position: .bottom)
                                                            if let blankImage = UIImage(named: "avatar_icon"){
                                                                let unpaidData = UnpaidAppointmentData(shopID: firstShop.id, shopNameID: firstShop.shopName, shopMobileNumber: firstShop.shopMobileNumber, shopEmail: firstShop.shopEmail, shopAddress: firstShop.shopAddress, shopLogoImage: blankImage, appointmentID: firstAppointment.id, appointmentShortUniqueID: firstAppointment.appointmentShortUniqueID, appointmentDateStart: firstAppointment.appointmentDateStart, appointmentDateEnd: firstAppointment.appointmentDateEnd, appointmentStatus: firstAppointment.appointmentStatus, appointmentDateCreated: firstAppointment.appointmentDateCreated, appointmentDateTimezone: firstAppointment.appointmentDateTimezone, appointmentDateCalendar: firstAppointment.appointmentDateCalendar, appointmentDateLocale: firstAppointment.appointmentPaidLocale, appointmentCancelledID: cancel.id, appointmentCancelledCharges: cancel.appointmentCancelledCharges, initiator: cancel.initiator, appointmentCancelledReason: cancel.appointmentCancelledReason, appointmentCancelledStatus: cancel.appointmentCancelledStatus, appointmentCancelledTimezone: cancel.appointmentCancelledTimezone, appointmentCancelledCalendar: cancel.appointmentCancelledCalendar, appointmentCancelledLocale: cancel.appointmentCancelledLocale, appointmentCurrency: firstAppointment.appointmentCurrency)
                                                                self.unpaidAppointmentDataArray.append(unpaidData)
                                                                DispatchQueue.main.async {
                                                                    self.collectionView.reloadData()
                                                                }
                                                            }
                                                            break
                                                        }
                                                        
                                                    }
                                                    
                                                }
                                                
                                                
                                            }
                                            
                                            if(chargesArrayHolder.count == cancelledAppointmentData.count){
                                                let totalAmount = chargesArrayHolder.reduce(0, +).roundTo(places: 2)
                                                self.totalAmonuntTitleLabel.text = "Total: " + String(totalAmount) + " " + curr
                                            }
                                        }
                                        
                                    } catch _ {
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                print("cancelled appointments not found")
                                            case 500:
                                                print("An error occurred, please try again later")
                                            default:
                                                print("sky high", statusCode)
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
    }
    
    private func setupViewContraints(){
        
        var topDistance: CGFloat = 20
        if UIDevice.current.hasNotch {
            topDistance = 60
        }
        
        viewTitleNameLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        viewTitleNameLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        viewTitleNameLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        closeViewButton.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        closeViewButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -5).isActive = true
        closeViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        closeViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        curvedCollectViewHolder.topAnchor.constraint(equalTo: viewTitleNameLabel.bottomAnchor, constant: 10).isActive = true
        curvedCollectViewHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        curvedCollectViewHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        curvedCollectViewHolder.heightAnchor.constraint(equalToConstant: 250).isActive = true
        
        curvedCollectViewHolder.addSubview(collectView)
        curvedCollectViewHolder.bringSubviewToFront(collectView)
        
        collectView.topAnchor.constraint(equalTo: curvedCollectViewHolder.topAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: curvedCollectViewHolder.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: curvedCollectViewHolder.widthAnchor, constant: -10).isActive = true
        collectView.heightAnchor.constraint(equalTo: curvedCollectViewHolder.heightAnchor, constant: -5).isActive = true
        
        collectView.addSubview(collectionView)
        collectView.bringSubviewToFront(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
        initialDescriptionContanerView.topAnchor.constraint(equalTo: collectView.bottomAnchor, constant: 10).isActive = true
        initialDescriptionContanerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        initialDescriptionContanerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        initialDescriptionContanerView.heightAnchor.constraint(equalToConstant: 310).isActive = true
        
        initialDescriptionContanerView.addSubview(paymentSectionSeperator)
        initialDescriptionContanerView.addSubview(totalAmonuntTitleLabel)
        initialDescriptionContanerView.addSubview(mpesaLogoViewButton)
        initialDescriptionContanerView.addSubview(paymentDetailsTitleLabel)
        initialDescriptionContanerView.addSubview(businessNumberTitleLabel)
        initialDescriptionContanerView.addSubview(accountNumberTitleLabel)
        
        paymentSectionSeperator.topAnchor.constraint(equalTo: initialDescriptionContanerView.topAnchor).isActive = true
        paymentSectionSeperator.centerXAnchor.constraint(equalTo: initialDescriptionContanerView.centerXAnchor).isActive = true
        paymentSectionSeperator.widthAnchor.constraint(equalTo: initialDescriptionContanerView.widthAnchor).isActive = true
        paymentSectionSeperator.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        totalAmonuntTitleLabel.topAnchor.constraint(equalTo: paymentSectionSeperator.bottomAnchor, constant: 10).isActive = true
        totalAmonuntTitleLabel.centerXAnchor.constraint(equalTo: initialDescriptionContanerView.centerXAnchor).isActive = true
        totalAmonuntTitleLabel.widthAnchor.constraint(equalTo: initialDescriptionContanerView.widthAnchor).isActive = true
        totalAmonuntTitleLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        mpesaLogoViewButton.topAnchor.constraint(equalTo: totalAmonuntTitleLabel.bottomAnchor, constant: 10).isActive = true
        mpesaLogoViewButton.centerXAnchor.constraint(equalTo: initialDescriptionContanerView.centerXAnchor).isActive = true
        mpesaLogoViewButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        mpesaLogoViewButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        paymentDetailsTitleLabel.topAnchor.constraint(equalTo: mpesaLogoViewButton.bottomAnchor, constant: 5).isActive = true
        paymentDetailsTitleLabel.centerXAnchor.constraint(equalTo: initialDescriptionContanerView.centerXAnchor).isActive = true
        paymentDetailsTitleLabel.widthAnchor.constraint(equalTo: initialDescriptionContanerView.widthAnchor).isActive = true
        paymentDetailsTitleLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        businessNumberTitleLabel.topAnchor.constraint(equalTo: paymentDetailsTitleLabel.bottomAnchor, constant: 5).isActive = true
        businessNumberTitleLabel.centerXAnchor.constraint(equalTo: initialDescriptionContanerView.centerXAnchor).isActive = true
        businessNumberTitleLabel.widthAnchor.constraint(equalTo: initialDescriptionContanerView.widthAnchor).isActive = true
        businessNumberTitleLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        accountNumberTitleLabel.topAnchor.constraint(equalTo: businessNumberTitleLabel.bottomAnchor, constant: 10).isActive = true
        accountNumberTitleLabel.centerXAnchor.constraint(equalTo: initialDescriptionContanerView.centerXAnchor).isActive = true
        accountNumberTitleLabel.widthAnchor.constraint(equalTo: initialDescriptionContanerView.widthAnchor).isActive = true
        accountNumberTitleLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        DoneWithViewButton.topAnchor.constraint(equalTo: accountNumberTitleLabel.bottomAnchor, constant: 10).isActive = true
        DoneWithViewButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        DoneWithViewButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5).isActive = true
        DoneWithViewButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        self.getUnpaidAppointments()
    }
}

extension ResettlementViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.unpaidAppointmentDataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customUnpaidCollectionViewCell
        cell.shopLogoImageView.image = self.unpaidAppointmentDataArray[indexPath.row].shopLogoImage
        cell.shopNameLabel.text = self.unpaidAppointmentDataArray[indexPath.row].shopNameID
        cell.shopAddressLabel.text = self.unpaidAppointmentDataArray[indexPath.row].shopAddress
        cell.shopMobileNumberLabel.text = self.unpaidAppointmentDataArray[indexPath.row].shopMobileNumber
        cell.appointmentIDLabel.text = self.unpaidAppointmentDataArray[indexPath.row].appointmentShortUniqueID
        let curr = self.unpaidAppointmentDataArray[indexPath.row].appointmentCurrency
        cell.appointmentPriceLabel.text = self.unpaidAppointmentDataArray[indexPath.row].appointmentCancelledCharges + " " + curr
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

class customUnpaidCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    
    lazy var shopLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "avatar_icon")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 45
        imageView.layer.masksToBounds = true
        imageView.backgroundColor = UIColor.white
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    lazy var shopNameLabelLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8smallshopname")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let shopNameLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = "Shop name"
        ht.font = UIFont(name: "OpenSans-Light", size: 13)
        ht.textColor = UIColor.black
        ht.numberOfLines = 0
        ht.adjustsFontSizeToFitWidth = true
        ht.minimumScaleFactor = 0.1
        ht.baselineAdjustment = .alignCenters
        ht.textAlignment = .left
        return ht
    }()
    
    lazy var shopAddressLabelLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8smalladdress")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let shopAddressLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = "Shop address"
        ht.font = UIFont(name: "OpenSans-Light", size: 13)
        ht.textColor = UIColor.black
        ht.numberOfLines = 0
        ht.adjustsFontSizeToFitWidth = true
        ht.minimumScaleFactor = 0.1
        ht.baselineAdjustment = .alignCenters
        ht.textAlignment = .left
        return ht
    }()
    
    lazy var shopMobileNumberLabelLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8smallmobileNumber")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let shopMobileNumberLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = "Shop mobile number"
        ht.font = UIFont(name: "OpenSans-Light", size: 13)
        ht.textColor = UIColor.black
        ht.numberOfLines = 0
        ht.adjustsFontSizeToFitWidth = true
        ht.minimumScaleFactor = 0.1
        ht.baselineAdjustment = .alignCenters
        ht.textAlignment = .left
        return ht
    }()
    
    lazy var appointmentIDLabelLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8uniqueid")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let appointmentIDLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = "Appointment id"
        ht.font = UIFont(name: "OpenSans-Light", size: 13)
        ht.textColor = UIColor.black
        ht.numberOfLines = 0
        ht.adjustsFontSizeToFitWidth = true
        ht.minimumScaleFactor = 0.1
        ht.baselineAdjustment = .alignCenters
        ht.textAlignment = .left
        return ht
    }()
    
    lazy var appointmentPriceLabelLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8cashLogo")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let appointmentPriceLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = "Appointment price"
        ht.font = UIFont(name: "OpenSans-Light", size: 13)
        ht.textColor = UIColor.black
        ht.numberOfLines = 0
        ht.adjustsFontSizeToFitWidth = true
        ht.minimumScaleFactor = 0.1
        ht.baselineAdjustment = .alignCenters
        ht.textAlignment = .left
        return ht
    }()
    
    
    
    func setupViews(){
        
        addSubview(shopLogoImageView)
        addSubview(shopNameLabelLogo)
        addSubview(shopNameLabel)
        addSubview(shopAddressLabelLogo)
        addSubview(shopAddressLabel)
        addSubview(shopMobileNumberLabelLogo)
        addSubview(shopMobileNumberLabel)
        addSubview(appointmentIDLabelLogo)
        addSubview(appointmentIDLabel)
        addSubview(appointmentPriceLabelLogo)
        addSubview(appointmentPriceLabel)
        
        backgroundColor = UIColor.white
        layer.cornerRadius = 5
        layer.masksToBounds = true
        
        addContraintsWithFormat(format: "H:|-5-[v0(90)]-10-[v1(20)]-5-[v2]-5-|", views: shopLogoImageView, shopNameLabelLogo, shopNameLabel)
        addContraintsWithFormat(format: "H:|-5-[v0(90)]-10-[v1(20)]-5-[v2]-5-|", views: shopLogoImageView, shopAddressLabelLogo, shopAddressLabel)
        addContraintsWithFormat(format: "H:|-5-[v0(90)]-10-[v1(20)]-5-[v2]-5-|", views: shopLogoImageView, shopMobileNumberLabelLogo, shopMobileNumberLabel)
        addContraintsWithFormat(format: "H:|-5-[v0(90)]-10-[v1(20)]-5-[v2]-5-|", views: shopLogoImageView, appointmentIDLabelLogo, appointmentIDLabel)
        addContraintsWithFormat(format: "H:|-5-[v0(90)]-10-[v1(20)]-5-[v2]-5-|", views: shopLogoImageView, appointmentPriceLabelLogo, appointmentPriceLabel)
        
        addContraintsWithFormat(format: "V:|-5-[v0(90)]-5-|", views: shopLogoImageView)
        addContraintsWithFormat(format: "V:|-5-[v0(20)]-5-[v1(20)]-5-[v2(20)]-5-[v3(20)]-5-[v4(20)]-5-|", views: shopNameLabelLogo, shopAddressLabelLogo, shopMobileNumberLabelLogo, appointmentIDLabelLogo, appointmentPriceLabelLogo)
        addContraintsWithFormat(format: "V:|-5-[v0(15)]-5-[v1(25)]-5-[v2(15)]-5-[v3(15)]-5-[v4(15)]-5-|", views: shopNameLabel, shopAddressLabel, shopMobileNumberLabel, appointmentIDLabel, appointmentPriceLabel)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
