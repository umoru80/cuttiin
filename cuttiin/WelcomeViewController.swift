//
//  WelcomeViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/12/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import SwiftDate
import Alamofire
import SwiftyJSON
import Toast_Swift
import SwiftSpinner

class WelcomeViewController: UIViewController, LoginButtonDelegate {
    
    let network: NetworkManager = NetworkManager.sharedInstance
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    var mainViewController = MainNavigationController()
    
    lazy var backgroundImageView: UIImageView = {
        let bImageView = UIImageView()
        bImageView.translatesAutoresizingMaskIntoConstraints = false
        bImageView.contentMode = .scaleAspectFill
        bImageView.image = UIImage(named: "stay_sharp_frontpage_bg")
        return bImageView
    }()
    
    let backgroundImageTintCoverView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    let mainLogoImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.image = UIImage(named: "mainLogo")
        tniv.backgroundColor = UIColor.clear
        tniv.contentMode = .scaleToFill
        return tniv
    }()
    
    let lowerInputContainerView: UIView = {
        let licview = UIView()
        licview.translatesAutoresizingMaskIntoConstraints = false
        licview.backgroundColor = UIColor.clear
        return licview
    }()
    
    
    lazy var loginButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        fbutton.setTitle(NSLocalizedString("logInButtonTitleWelcomeView", comment: "login"), for: .normal)
        fbutton.setTitleColor(UIColor.black, for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        fbutton.layer.cornerRadius = 5
        fbutton.layer.masksToBounds = true
        fbutton.addTarget(self, action: #selector(handleShowLoginView), for: .touchUpInside)
        return fbutton
    }()
    
    lazy var createAccountButton: UIButton = {
        let cbutton = UIButton()
        cbutton.translatesAutoresizingMaskIntoConstraints = false
        cbutton.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        cbutton.setTitle(NSLocalizedString("createAnAccountButtonTitleWelcomeView", comment: "create an accunt"), for: .normal)
        cbutton.setTitleColor(UIColor.black, for: .normal)
        cbutton.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        cbutton.layer.cornerRadius = 5
        cbutton.layer.masksToBounds = true
        cbutton.addTarget(self, action: #selector(handleCreatingAnAccount), for: .touchUpInside)
        return cbutton
    }()
    
    let facebookLoginButtonContainerView: UIView = {
        let fBookview = UIView()
        fBookview.translatesAutoresizingMaskIntoConstraints = false
        return fBookview
    }()
    
    lazy var faceBookloginButton: FBLoginButton = {
        let button = FBLoginButton()
        button.permissions = ["email", "public_profile"]
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(NSLocalizedString("facebookButtonTitle", comment: "continue with facebook"), for: .normal)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = UIColor.white
        view.addSubview(backgroundImageView)
        view.addSubview(backgroundImageTintCoverView)
        view.bringSubviewToFront(backgroundImageTintCoverView)
        setupViewObjectsAutoContriaints()
        faceBookloginButton.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .didReceiveDataWelcomeViewController, object: nil)
    }
    
    @objc private func onDidReceiveData(_ notification:Notification) {
        print("call made")
        checkCustomerAuthentication()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkCustomerAuthentication()
        print("login done smoothly")
    }
    
    @objc func checkCustomerAuthentication(){
        print("check auth")
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isAfterDate(dateNow, orEqual: true, granularity: .second) {
                    self.dismiss(animated: true, completion: {
                        NotificationCenter.default.post(name: .didReceiveDataMainNavigationController, object: nil)
                    })
                } else {
                    print("auth failed second")
                }
            } else {
                print("auth failed first")
            }
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        print("Did log out of facebook")
    }
    
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        if let err = error {
            print(err.localizedDescription)
            return
        }
        fetchProfile()
    }
    
    func loginButtonWillLogin(_ loginButton: FBLoginButton) -> Bool {
        return true
    }
    
    @objc func fetchProfile() {
        SwiftSpinner.show("Loading...")
        DispatchQueue.global(qos: .background).async {
            let parameters = ["fields": "email, first_name, last_name, picture.type(large)"]
            GraphRequest(graphPath: "me", parameters: parameters).start(completionHandler: { (connection, result, errror) in
                
                if let error = errror {
                    DispatchQueue.main.async {
                        SwiftSpinner.hide()
                        self.view.makeToast("Facebook auth: " + error.localizedDescription, duration: 3.0, position: .center)
                    }
                    return
                }
                
                //check if customer exist
                if let personalInfo = result as? [String: AnyObject], let email = personalInfo["email"], let firstName = personalInfo["first_name"], let lastName = personalInfo["last_name"], let picture = personalInfo["picture"] as? NSDictionary, let data = picture["data"] as? NSDictionary, let url = data["url"] as? String,  let imageUrl = URL(string: url), let emailString = email as? String  {
                    let newDateFL = DateInRegion().toFormat("yyyy-MM-dd HH:mm:ss")
                    let userTimezone = DateByUserDeviceInitializer.tzone
                    let userCalender = "gregorian"
                    let locale = "en"
                    let regionData = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: locale)
                    
                    let parameters: Parameters = ["email": email, "loginDate": newDateFL, "loginTimezone": userTimezone, "loginCalendar":userCalender, "loginLocale": locale]
                    
                    AF.request(self.BACKEND_URL + "flogin", method: .post, parameters: parameters)
                        .validate(statusCode: 200..<500)
                        .validate(contentType: ["application/json"])
                        .response(completionHandler: { (response) in
                        
                        if let error = response.error {
                            DispatchQueue.main.async {
                                SwiftSpinner.hide()
                            }
                            print(error.localizedDescription)
                            return
                        }
                        
                        if let jsonData = response.data {
                            do {
                                DispatchQueue.main.async {
                                    SwiftSpinner.hide()
                                }
                                let loggedIn = try JSONDecoder().decode(LoggedIn.self, from: jsonData)
                                let customerId = loggedIn.customer.id
                                let tokenData = loggedIn.token
                                let shopOwner = loggedIn.customer.shopOwner
                                let expiresInCheck = loggedIn.expiresIn
                                let sessionPeriondEndDate = DateInRegion().convertTo(region: regionData) + 7.days
                                let sessionPeriodEndString = sessionPeriondEndDate.toFormat("yyyy-MM-dd HH:mm:ss")
                                print("got data back from facebook")
                                self.handleCreateUserData(expiresInCheck: expiresInCheck, sessionPeriodEndString: sessionPeriodEndString, customerId: customerId, tokenData: tokenData, timezone: userTimezone, shopOwner: shopOwner, authType: "facebook", email: emailString)
                                
                                self.dismiss(animated: true, completion: {
                                    NotificationCenter.default.post(name: .didReceiveDataMainNavigationController, object: nil)
                                })
                                
                                
                            } catch let jsonerror {
                                print("error serilaizing json", jsonerror)
                                do {
                                    let data = try Data(contentsOf: imageUrl)
                                    let imageName = NSUUID().uuidString
                                    let newDate = DateInRegion().toFormat("yyyy-MM-dd HH:mm:ss")
                                    let userTimezone = DateByUserDeviceInitializer.tzone
                                    
                                    
                                    if let downloadedImage = UIImage(data: data), let uploadData = downloadedImage.jpegData(compressionQuality: 0.1) {
                                        let parameters = [
                                            "firstName": firstName,
                                            "lastName": lastName,
                                            "email": emailString,
                                            "profileImageName":"\(imageName).jpeg",
                                            "password": "not available",
                                            "dateCreated": newDate,
                                            "dateCreatedTimezone": userTimezone,
                                            "dateCreatedCalendar": userCalender,
                                            "dateCreatedLocale": "en",
                                            "authenticationType": "facebook",
                                            "shopOwner": "NO",
                                            "accountDeactivated": "NO",
                                            "loginDate":newDate,
                                            "loginTimezone":userTimezone,
                                            "loginCalendar":userCalender,
                                            "loginLocale":locale
                                            ] as [String : Any]
                                        
                                        
                                        
                                        
                                        AF.upload(multipartFormData: { (multipartFormData) in
                                            multipartFormData.append(uploadData, withName: "image",fileName: imageName, mimeType: "image/jpg")
                                            for (key, value) in parameters {
                                                if let stringValue = value as? String {
                                                    multipartFormData.append(stringValue.data(using: .utf8)!, withName: key)
                                                }
                                            }
                                        }, usingThreshold: UInt64.init(), fileManager: .default, to: self.BACKEND_URL + "signup", method: .post, interceptor: nil)
                                            .validate(statusCode: 200..<501)
                                            .validate(contentType: ["application/json"])
                                            .uploadProgress(queue: .main, closure: { (progress) in
                                                print("Upload Progress: \(progress.fractionCompleted)")
                                            }).response { response in
                                                
                                                switch response.result {
                                                case .success(let data):
                                                    DispatchQueue.main.async {
                                                        SwiftSpinner.hide()
                                                    }
                                                    if let jsonData = data {
                                                        do {
                                                            let loggedIn = try JSONDecoder().decode(LoggedIn.self, from: jsonData)
                                                            let customerId = loggedIn.customer.id
                                                            let tokenData = loggedIn.token
                                                            let timezone = DateByUserDeviceInitializer.tzone
                                                            let shopOwner = loggedIn.customer.shopOwner
                                                            let expiresInCheck = loggedIn.expiresIn
                                                            let sessionPeriondEndDate = DateInRegion().convertTo(region: regionData) + 7.days
                                                            let sessionPeriodEndString = sessionPeriondEndDate.toFormat("yyyy-MM-dd HH:mm:ss")
                                                            
                                                            self.handleCreateUserData(expiresInCheck: expiresInCheck, sessionPeriodEndString: sessionPeriodEndString, customerId: customerId, tokenData: tokenData, timezone: timezone, shopOwner: shopOwner, authType: "facebook", email: emailString)
                                                            
                                                            self.dismiss(animated: true, completion: {
                                                                NotificationCenter.default.post(name: .didReceiveDataMainNavigationController, object: nil)
                                                            })
                                                        } catch _ {
                                                            if let statusCode = response.response?.statusCode {
                                                                print(statusCode)
                                                                switch statusCode {
                                                                case 404:
                                                                    self.view.makeToast("Service was not found", duration: 3.0, position: .bottom)
                                                                case 500:
                                                                    self.view.makeToast("An error occurred, please try again later", duration: 3.0, position: .bottom)
                                                                default:
                                                                    print("Sky high : update")
                                                                }
                                                            }
                                                        }
                                                    }
                                                    break
                                                case .failure(let error):
                                                    DispatchQueue.main.async {
                                                        SwiftSpinner.hide()
                                                    }
                                                    print(error)
                                                    let message = "Update error: please try again later"
                                                    self.view.makeToast(message, duration: 3.0, position: .bottom)
                                                    break
                                                }
                                        }
                                    }
                                    
                                    
                                } catch {
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                        self.view.makeToast("Facebook auth: try catch error", duration: 3.0, position: .center)
                                    }
                                    
                                }
                            }
                        }
                    })
                    
                } else {
                    DispatchQueue.main.async {
                        SwiftSpinner.hide()
                        self.view.makeToast("Facebook auth: data unavailable", duration: 3.0, position: .center)
                    }
                }
                
            })
        }
    }
    
    //show login view
    @objc func handleShowLoginView(){
        let loginview = LoginViewController()
        loginview.welcomeviewholder = self
        loginview.mainViewController = self.mainViewController
        loginview.modalPresentationStyle = .overCurrentContext
        self.present(loginview, animated: true, completion: nil)
    }
    
    //show create account view
    @objc func handleCreatingAnAccount(){
        let registerview = RegistrationViewController()
        registerview.modalPresentationStyle = .overCurrentContext
        //let navController = UINavigationController(rootViewController: registerview)
        self.present(registerview, animated: true, completion: nil)
    }
    
    //handle createloggedIn details
    @objc private func handleCreateUserData(expiresInCheck: String, sessionPeriodEndString: String, customerId: String, tokenData: String, timezone: String, shopOwner: String, authType: String, email: String){
        if expiresInCheck == "7d" {
            let teams = [CustomerToken(expiresIn: sessionPeriodEndString, customerId: customerId, token: "Bearer \(tokenData)", timezone: timezone, shopOwner: shopOwner, authType: authType, email: email, autoShopSwitch: "NO")]
            
            let userDefaults = UserDefaults.standard
            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: teams)
            userDefaults.set(encodedData, forKey: "token")
            userDefaults.synchronize()
            print("hello there")
        }
    }
    
    //handle returning to main navigation controller
    @objc func handleMoveToNavigationController(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let authTyoe = decodedCustomer.first?.authType {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let DateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region) {
                    if expiredIn.isBeforeDate(DateNow, orEqual: true, granularity: .second){
                        if let validFaceBookAuth = AccessToken.current?.isExpired {
                            if !validFaceBookAuth && authTyoe == "facebook" {
                                LoginManager().logOut()
                            } else {
                                self.dismiss(animated: false, completion: nil)
                            }
                        }
                        
                    }
                }
            }
            
        }
    }
    
    //sub code to print fonts on device
    func printAllFont(){
        for family: String in UIFont.familyNames
        {
            print("\(family)")
            for names: String in UIFont.fontNames(forFamilyName: family)
            {
                print("== \(names)")
            }
        }
    }
    
    //setup view contraints
    func setupViewObjectsAutoContriaints(){
        backgroundImageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImageView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        backgroundImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        backgroundImageView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        
        backgroundImageTintCoverView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImageTintCoverView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        backgroundImageTintCoverView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        backgroundImageTintCoverView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        
        backgroundImageTintCoverView.addSubview(lowerInputContainerView)
        backgroundImageTintCoverView.addSubview(mainLogoImageView)
        
        lowerInputContainerView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        lowerInputContainerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        lowerInputContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        lowerInputContainerView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5, constant: -40).isActive = true
        
        
        mainLogoImageView.bottomAnchor.constraint(equalTo: lowerInputContainerView.topAnchor).isActive = true
        mainLogoImageView.centerXAnchor.constraint(equalTo: backgroundImageTintCoverView.centerXAnchor).isActive = true
        mainLogoImageView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        mainLogoImageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        lowerInputContainerView.addSubview(loginButton)
        lowerInputContainerView.addSubview(createAccountButton)
        lowerInputContainerView.addSubview(facebookLoginButtonContainerView)
        
        facebookLoginButtonContainerView.bottomAnchor.constraint(equalTo: lowerInputContainerView.bottomAnchor, constant: -44).isActive = true
        facebookLoginButtonContainerView.widthAnchor.constraint(equalTo: lowerInputContainerView.widthAnchor, multiplier: 1, constant: -24).isActive = true
        facebookLoginButtonContainerView.centerXAnchor.constraint(equalTo: lowerInputContainerView.centerXAnchor).isActive = true
        facebookLoginButtonContainerView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        facebookLoginButtonContainerView.addSubview(faceBookloginButton)
        
        faceBookloginButton.topAnchor.constraint(equalTo: facebookLoginButtonContainerView.topAnchor).isActive = true
        faceBookloginButton.centerXAnchor.constraint(equalTo: facebookLoginButtonContainerView.centerXAnchor).isActive = true
        faceBookloginButton.widthAnchor.constraint(equalTo: facebookLoginButtonContainerView.widthAnchor).isActive = true
        faceBookloginButton.heightAnchor.constraint(equalTo: facebookLoginButtonContainerView.heightAnchor).isActive = true
        
        createAccountButton.bottomAnchor.constraint(equalTo: facebookLoginButtonContainerView.topAnchor, constant: -11).isActive = true
        createAccountButton.widthAnchor.constraint(equalTo: lowerInputContainerView.widthAnchor, multiplier: 1, constant: -24).isActive = true
        createAccountButton.centerXAnchor.constraint(equalTo: lowerInputContainerView.centerXAnchor).isActive = true
        createAccountButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        loginButton.bottomAnchor.constraint(equalTo: createAccountButton.topAnchor, constant: -11).isActive = true
        loginButton.widthAnchor.constraint(equalTo: lowerInputContainerView.widthAnchor, multiplier: 1, constant: -24).isActive = true
        loginButton.centerXAnchor.constraint(equalTo: lowerInputContainerView.centerXAnchor).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
    }
}
