//
//  HolidayStaffCreated.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 12/05/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation

struct HolidayStaffCreated: Codable {
    let message: String
    let holidayStaff: HolidayStaff
}

struct HolidayStaff: Codable {
    let v: Int
    let id, customer, holidayStaffDateCalendar, holidayStaffDateCreated: String
    let holidayStaffDateLocale, holidayStaffDateTimezone, holidayStaffDescription, holidayStaffEndDate: String
    let holidayStaffIsActive, holidayStaffStartDate, holidayStaffTitle, holidayStaffRepeatStatus, holidayStaffDayOfTheWeek: String
    let shop, staff: String
    
    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case customer, holidayStaffDateCalendar, holidayStaffDateCreated, holidayStaffDateLocale, holidayStaffDateTimezone, holidayStaffDescription, holidayStaffEndDate, holidayStaffIsActive, holidayStaffStartDate, shop, staff, holidayStaffTitle, holidayStaffRepeatStatus, holidayStaffDayOfTheWeek
    }
}
