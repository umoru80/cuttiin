//
//  AddServiceViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/22/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Alamofire
import SwiftDate
import SwiftSpinner

class AddServiceViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var shopID: String?
    var serviceID: String?
    var barberShopBarbersAvailable = [Barber]()
    var barberShopBarbersAvailableStringID = [String]()
    var selectedbarberShopBarbers = [String]()
    var selectedCategoryChosed: String?
    var actionStatusForView: String?
    var serviceHiddenStatus: String?
    var shopCurrency: String?
    var servicePriceString: String?
    var serviceEstimatedTimeString: String?
    var isServiceDeleted = "NO"
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor.clear
        return tcview
    }()
    
    lazy var addShopServiceViewViewIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8shopServieMainLogo")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var addShopServiceViewCloseViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismissView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let addShopServiceViewPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 20) // HelveticaNeue-Medium
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var saveAndUpdateOrCreateButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        st.isEnabled = false
        st.isUserInteractionEnabled = true
        return st
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    let hideButtonTitleLabel: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    lazy var addShopServiceViewHideServiceButton: UISwitch = {
        let st = UISwitch()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.isOn = false
        st.addTarget(self, action: #selector(handleHidingShopService(_:)), for: .touchUpInside)
        return st
    }()
    
    let deleteButtonTitleLabel: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .right
        fnhp.text = NSLocalizedString("AddServiceViewControllerSavedSwitcher", comment: "saved")
        return fnhp
    }()
    
    lazy var addShopServiceViewDeleteServiceButton: UISwitch = {
        let st = UISwitch()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.isOn = false
        st.addTarget(self, action: #selector(handleDeletingShopService(_:)), for: .touchUpInside)
        return st
    }()
    
    let scrollView: UIScrollView = {
        let v = UIScrollView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.showsVerticalScrollIndicator = false
        v.isScrollEnabled = true
        v.backgroundColor = .clear
        return v
    }()
    
    let picker = UIImagePickerController()
    
    let imageAndServiceContainerView: UIView = {
        let iascview = UIView()
        iascview.translatesAutoresizingMaskIntoConstraints = false
        return iascview
    }()
    
    lazy var selectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "add_photo_smallest")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showCameraActionOptions)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let serviceTitleHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        fnhp.textColor = UIColor.black
        fnhp.isHidden = true
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.text = NSLocalizedString("AddServiceViewControllerServiceTitle", comment: "Service title")
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let serviceTitleTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 20)
        em.placeholder = NSLocalizedString("AddServiceViewControllerServiceTitle", comment: "Service title")
        em.addTarget(self, action: #selector(serviceTitletextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(serviceTitletextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let serviceTitleSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return fnsv
    }()
    
    let estimatedTimeHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 9)
        fnhp.textColor = UIColor.black
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.text = NSLocalizedString("AddServiceViewControllerEstimatedMins", comment: "Est time in mins")
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let estimatedTimeTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.keyboardType = .numberPad
        em.textAlignment = .left
        em.placeholder = NSLocalizedString("AddServiceViewControllerEstimatedMins", comment: "Est time in mins")
        em.addTarget(self, action: #selector(estimatedTimetextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(estimatedTimetextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let estimatedTimeSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return fnsv
    }()
    
    let serviceCostHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.text = NSLocalizedString("AddServiceViewControllerServicePrice", comment: "Price")
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let serviceCostTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 17)
        em.textAlignment = .left
        em.keyboardType = .decimalPad
        em.placeholder = NSLocalizedString("AddServiceViewControllerServicePrice", comment: "Price")
        em.addTarget(self, action: #selector(serviceCosttextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(serviceCosttextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let serviceCostSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return fnsv
    }()
    
    let serviceContainerView: UIView = {
        let scview = UIView()
        scview.translatesAutoresizingMaskIntoConstraints = false
        return scview
    }()
    
    lazy var styleSelectionSegmentedControl: UISegmentedControl = {
        let sssegmentcontrol = UISegmentedControl(items: [NSLocalizedString("AddServiceViewControllerLadiesTabTitle", comment: "LADIES"), NSLocalizedString("AddServiceViewControllerGentsTabTitle", comment: "GENTS"), NSLocalizedString("AddServiceViewControllerKidsTabTitle", comment: "KIDS")])
        sssegmentcontrol.translatesAutoresizingMaskIntoConstraints = false
        sssegmentcontrol.tintColor = UIColor(r: 220, g: 220, b: 220)
        sssegmentcontrol.selectedSegmentIndex = 1
        sssegmentcontrol.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: UIControl.State.normal)
        sssegmentcontrol.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: UIControl.State.selected)
        sssegmentcontrol.addTarget(self, action: #selector(handleSegmentedControlSelection), for: .valueChanged)
        return sssegmentcontrol
    }()
    
    let shortDescriptionHiddenPlaceHolder: UILabel = {
        let lnhp = UILabel()
        lnhp.translatesAutoresizingMaskIntoConstraints = false
        lnhp.text = NSLocalizedString("AddServiceViewControllerShortDescription", comment: "Short Description")
        lnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        lnhp.textColor = UIColor.black
        lnhp.isHidden = true
        lnhp.textAlignment = .left
        return lnhp
    }()
    
    let shortDescriptionTextField: UITextView = {
        let em = UITextView()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.lightGray
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.text = NSLocalizedString("AddServiceViewControllerShortDescription", comment: "Short Description")
        em.textAlignment = .justified
        em.isEditable = true
        em.isSelectable = true
        return em
    }()
    
    let descriptionTitle: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("AddServiceViewControllerMessageAddServiceGents", comment: "This will add the service in the Gents section only")
        ht.font = UIFont(name: "OpenSans-Light", size: 15)
        ht.adjustsFontSizeToFitWidth = true
        ht.minimumScaleFactor = 0.1
        ht.baselineAdjustment = .alignCenters
        ht.numberOfLines = 0
        ht.textColor = UIColor.black
        ht.textAlignment = .center
        return ht
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        picker.delegate = self
        navigationItem.rightBarButtonItems?[safe: 0]?.isEnabled = false
        view.addSubview(addShopServiceViewCloseViewButton)
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        view.addSubview(addShopServiceViewHideServiceButton)
        view.addSubview(hideButtonTitleLabel)
        view.addSubview(addShopServiceViewDeleteServiceButton)
        view.addSubview(deleteButtonTitleLabel)
        view.addSubview(scrollView)
        estimatedTimeTextField.delegate = self
        serviceCostTextField.delegate = self
        shortDescriptionTextField.delegate = self
        
        if let status = self.actionStatusForView {
            switch (status) {
            case "editService":
                self.addShopServiceViewPlaceHolder.text = NSLocalizedString("AddServiceViewControllerEditServiceTitle", comment: "Edit Service")
                let heighAchorForHideButton: CGFloat = 30
                self.addShopServiceViewHideServiceButton.isHidden = false
                self.hideButtonTitleLabel.isHidden = false
                self.addShopServiceViewDeleteServiceButton.isHidden = false
                self.deleteButtonTitleLabel.isHidden = false
                self.saveAndUpdateOrCreateButton.setTitle(NSLocalizedString("AddServiceViewControllerSaveButtonTitle", comment: "Save"), for: .normal)
                self.saveAndUpdateOrCreateButton.addTarget(self, action: #selector(postUpdatedServiceDetails), for: .touchUpInside)
                setupViewObjectConstriants(hideHeight: heighAchorForHideButton)
                break
            case "createShop":
                self.addShopServiceViewPlaceHolder.text = NSLocalizedString("AddServiceViewControllerAddServiceWithStepsTitle", comment: "Add a Service: Step 4 of 5")
                self.addShopServiceViewPlaceHolder.font = UIFont(name: "OpenSans-SemiBold", size: 13)
                let heighAchorForHideButton: CGFloat = 0
                self.addShopServiceViewHideServiceButton.isHidden = true
                self.hideButtonTitleLabel.isHidden = true
                self.addShopServiceViewDeleteServiceButton.isHidden = true
                self.deleteButtonTitleLabel.isHidden = true
                self.saveAndUpdateOrCreateButton.setTitle(NSLocalizedString("AddServiceViewControllerDoneButtonTitle", comment: "Done"), for: .normal)
                self.saveAndUpdateOrCreateButton.addTarget(self, action: #selector(handleUserSegementSelection), for: .touchUpInside)
                setupViewObjectConstriants(hideHeight: heighAchorForHideButton)
                break
            case "addService":
                print("adding service")
                let heighAchorForHideButton: CGFloat = 0
                self.addShopServiceViewPlaceHolder.text = NSLocalizedString("AddServiceViewControllerAddAServiceTitle", comment: "Add a service")
                self.addShopServiceViewHideServiceButton.isHidden = true
                self.hideButtonTitleLabel.isHidden = true
                self.addShopServiceViewDeleteServiceButton.isHidden = true
                self.deleteButtonTitleLabel.isHidden = true
                self.saveAndUpdateOrCreateButton.setTitle(NSLocalizedString("AddServiceViewControllerSaveButtonTitle", comment: "Save"), for: .normal)
                self.saveAndUpdateOrCreateButton.addTarget(self, action: #selector(handleUserSegementSelection), for: .touchUpInside)
                setupViewObjectConstriants(hideHeight: heighAchorForHideButton)
                break
            default:
                break
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @objc func postUpdatedServiceDetails(){
        let segmentSelect = self.styleSelectionSegmentedControl.selectedSegmentIndex
        
        switch segmentSelect {
        case 0:
            self.handlePostEditService(selection: "Ladies")
        case 1:
            self.handlePostEditService(selection: "Gents")
        case 2:
            self.handlePostEditService(selection: "Kids")
        default:
            print("Sky High")
        }
    }
    
    @objc func handleDismissView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func setupViewObjectConstriants(hideHeight: CGFloat){
        
        var topDistance: CGFloat = 20
        if UIDevice.current.hasNotch {
            topDistance = 60
        }
        
        addShopServiceViewCloseViewButton.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        addShopServiceViewCloseViewButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -15).isActive = true
        addShopServiceViewCloseViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        addShopServiceViewCloseViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperInputsContainerView.topAnchor.constraint(equalTo: addShopServiceViewCloseViewButton.bottomAnchor, constant: 5).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(addShopServiceViewViewIcon)
        upperInputsContainerView.addSubview(addShopServiceViewPlaceHolder)
        upperInputsContainerView.addSubview(saveAndUpdateOrCreateButton)
        
        addShopServiceViewViewIcon.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        addShopServiceViewViewIcon.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor).isActive = true
        addShopServiceViewViewIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        addShopServiceViewViewIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        saveAndUpdateOrCreateButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        saveAndUpdateOrCreateButton.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        saveAndUpdateOrCreateButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        saveAndUpdateOrCreateButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        addShopServiceViewPlaceHolder.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        addShopServiceViewPlaceHolder.leftAnchor.constraint(equalTo: addShopServiceViewViewIcon.rightAnchor, constant: 10).isActive = true
        addShopServiceViewPlaceHolder.rightAnchor.constraint(equalTo: saveAndUpdateOrCreateButton.leftAnchor).isActive = true
        addShopServiceViewPlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        addShopServiceViewHideServiceButton.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 5).isActive = true
        addShopServiceViewHideServiceButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -12).isActive = true
        addShopServiceViewHideServiceButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        addShopServiceViewHideServiceButton.heightAnchor.constraint(equalToConstant: hideHeight).isActive = true
        
        hideButtonTitleLabel.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 5).isActive = true
        hideButtonTitleLabel.rightAnchor.constraint(equalTo: addShopServiceViewHideServiceButton.leftAnchor, constant: -10).isActive = true
        hideButtonTitleLabel.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.25).isActive = true
        hideButtonTitleLabel.heightAnchor.constraint(equalToConstant: hideHeight).isActive = true
        
        addShopServiceViewDeleteServiceButton.topAnchor.constraint(equalTo: addShopServiceViewHideServiceButton.bottomAnchor, constant: 5).isActive = true
        addShopServiceViewDeleteServiceButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -12).isActive = true
        addShopServiceViewDeleteServiceButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        addShopServiceViewDeleteServiceButton.heightAnchor.constraint(equalToConstant: hideHeight).isActive = true
        
        deleteButtonTitleLabel.topAnchor.constraint(equalTo: hideButtonTitleLabel.bottomAnchor, constant: 5).isActive = true
        deleteButtonTitleLabel.rightAnchor.constraint(equalTo: addShopServiceViewDeleteServiceButton.leftAnchor, constant: -10).isActive = true
        deleteButtonTitleLabel.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.25).isActive = true
        deleteButtonTitleLabel.heightAnchor.constraint(equalToConstant: hideHeight).isActive = true
        
        scrollView.topAnchor.constraint(equalTo: deleteButtonTitleLabel.bottomAnchor).isActive = true
        scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -5).isActive = true
        
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 450)
        
        
        scrollView.addSubview(imageAndServiceContainerView)
        scrollView.addSubview(styleSelectionSegmentedControl)
        scrollView.addSubview(shortDescriptionHiddenPlaceHolder)
        scrollView.addSubview(shortDescriptionTextField)
        scrollView.addSubview(descriptionTitle)
        
        imageAndServiceContainerView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 5).isActive = true
        imageAndServiceContainerView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        imageAndServiceContainerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        imageAndServiceContainerView.heightAnchor.constraint(equalToConstant: 92).isActive = true
        
        imageAndServiceContainerView.addSubview(selectImageView)
        imageAndServiceContainerView.addSubview(serviceContainerView)
        
        selectImageView.topAnchor.constraint(equalTo: imageAndServiceContainerView.topAnchor).isActive = true
        selectImageView.leftAnchor.constraint(equalTo: imageAndServiceContainerView.leftAnchor).isActive = true
        selectImageView.widthAnchor.constraint(equalTo: imageAndServiceContainerView.widthAnchor, multiplier: 0.25).isActive = true
        selectImageView.heightAnchor.constraint(equalTo: imageAndServiceContainerView.heightAnchor).isActive = true
        
        serviceContainerView.topAnchor.constraint(equalTo: imageAndServiceContainerView.topAnchor).isActive = true
        serviceContainerView.leftAnchor.constraint(equalTo: selectImageView.rightAnchor, constant: 10).isActive = true
        serviceContainerView.rightAnchor.constraint(equalTo: imageAndServiceContainerView.rightAnchor).isActive = true
        serviceContainerView.heightAnchor.constraint(equalTo: imageAndServiceContainerView.heightAnchor).isActive = true
        
        serviceContainerView.addSubview(serviceTitleHiddenPlaceHolder)
        serviceContainerView.addSubview(serviceTitleTextField)
        serviceContainerView.addSubview(serviceTitleSeperatorView)
        serviceContainerView.addSubview(estimatedTimeHiddenPlaceHolder)
        serviceContainerView.addSubview(estimatedTimeTextField)
        serviceContainerView.addSubview(estimatedTimeSeperatorView)
        serviceContainerView.addSubview(serviceCostHiddenPlaceHolder)
        serviceContainerView.addSubview(serviceCostTextField)
        serviceContainerView.addSubview(serviceCostSeperatorView)
        
        serviceTitleHiddenPlaceHolder.topAnchor.constraint(equalTo: serviceContainerView.topAnchor).isActive = true
        serviceTitleHiddenPlaceHolder.centerXAnchor.constraint(equalTo: serviceContainerView.centerXAnchor).isActive = true
        serviceTitleHiddenPlaceHolder.widthAnchor.constraint(equalTo: serviceContainerView.widthAnchor).isActive = true
        serviceTitleHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        serviceTitleTextField.topAnchor.constraint(equalTo: serviceTitleHiddenPlaceHolder.bottomAnchor).isActive = true
        serviceTitleTextField.centerXAnchor.constraint(equalTo: serviceContainerView.centerXAnchor).isActive = true
        serviceTitleTextField.widthAnchor.constraint(equalTo: serviceContainerView.widthAnchor).isActive = true
        serviceTitleTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        serviceTitleSeperatorView.leftAnchor.constraint(equalTo: serviceContainerView.leftAnchor).isActive = true
        serviceTitleSeperatorView.topAnchor.constraint(equalTo: serviceTitleTextField.bottomAnchor).isActive = true
        serviceTitleSeperatorView.widthAnchor.constraint(equalTo: serviceContainerView.widthAnchor).isActive = true
        serviceTitleSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        
        estimatedTimeSeperatorView.bottomAnchor.constraint(equalTo: serviceContainerView.bottomAnchor).isActive = true
        estimatedTimeSeperatorView.leftAnchor.constraint(equalTo: serviceContainerView.leftAnchor).isActive = true
        estimatedTimeSeperatorView.widthAnchor.constraint(equalTo: serviceContainerView.widthAnchor, multiplier: 0.5, constant: -5).isActive = true
        estimatedTimeSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        serviceCostSeperatorView.bottomAnchor.constraint(equalTo: serviceContainerView.bottomAnchor).isActive = true
        serviceCostSeperatorView.rightAnchor.constraint(equalTo: serviceContainerView.rightAnchor).isActive = true
        serviceCostSeperatorView.widthAnchor.constraint(equalTo: serviceContainerView.widthAnchor, multiplier: 0.5, constant: -5).isActive = true
        serviceCostSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        
        estimatedTimeTextField.bottomAnchor.constraint(equalTo: estimatedTimeSeperatorView.topAnchor).isActive = true
        estimatedTimeTextField.leftAnchor.constraint(equalTo: serviceContainerView.leftAnchor).isActive = true
        estimatedTimeTextField.widthAnchor.constraint(equalTo: serviceContainerView.widthAnchor, multiplier: 0.5).isActive = true
        estimatedTimeTextField.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        serviceCostTextField.bottomAnchor.constraint(equalTo: serviceCostSeperatorView.topAnchor).isActive = true
        serviceCostTextField.leftAnchor.constraint(equalTo: estimatedTimeTextField.rightAnchor, constant: 20).isActive = true
        serviceCostTextField.rightAnchor.constraint(equalTo: serviceContainerView.rightAnchor).isActive = true
        serviceCostTextField.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        estimatedTimeHiddenPlaceHolder.bottomAnchor.constraint(equalTo: estimatedTimeTextField.topAnchor).isActive = true
        estimatedTimeHiddenPlaceHolder.leftAnchor.constraint(equalTo: serviceContainerView.leftAnchor).isActive = true
        estimatedTimeHiddenPlaceHolder.widthAnchor.constraint(equalTo: estimatedTimeTextField.widthAnchor).isActive = true
        estimatedTimeHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        serviceCostHiddenPlaceHolder.bottomAnchor.constraint(equalTo: serviceCostTextField.topAnchor).isActive = true
        serviceCostHiddenPlaceHolder.rightAnchor.constraint(equalTo: serviceContainerView.rightAnchor).isActive = true
        serviceCostHiddenPlaceHolder.widthAnchor.constraint(equalTo: serviceCostTextField.widthAnchor).isActive = true
        serviceCostHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        styleSelectionSegmentedControl.topAnchor.constraint(equalTo: imageAndServiceContainerView.bottomAnchor, constant: 10).isActive = true
        styleSelectionSegmentedControl.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        styleSelectionSegmentedControl.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        styleSelectionSegmentedControl.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        shortDescriptionHiddenPlaceHolder.topAnchor.constraint(equalTo: styleSelectionSegmentedControl.bottomAnchor, constant: 10).isActive = true
        shortDescriptionHiddenPlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        shortDescriptionHiddenPlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        shortDescriptionHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        shortDescriptionTextField.topAnchor.constraint(equalTo: shortDescriptionHiddenPlaceHolder.bottomAnchor).isActive = true
        shortDescriptionTextField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        shortDescriptionTextField.heightAnchor.constraint(equalToConstant: 100).isActive = true
        shortDescriptionTextField.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        
        descriptionTitle.topAnchor.constraint(equalTo: shortDescriptionTextField.bottomAnchor, constant: 10).isActive = true
        descriptionTitle.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        descriptionTitle.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        descriptionTitle.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        if let status = self.actionStatusForView, status == "editService" {
            self.getService()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return string == numberFiltered
    }
    
    @objc func textFieldDidChange(){
        let imageHold = UIImage(named: "add_picture")
        if serviceTitleTextField.text == "" || shortDescriptionTextField.text == "" || estimatedTimeTextField.text == "" || serviceCostTextField.text == "" || self.selectImageView.image == imageHold {
            //Disable button
            self.saveAndUpdateOrCreateButton.isEnabled = false
            
            
        } else {
            //Enable button
            self.saveAndUpdateOrCreateButton.isEnabled = true
        }
    }
    
    @objc func serviceTitletextFieldDidChange(){
        self.serviceTitleSeperatorView.backgroundColor = UIColor.black
        self.estimatedTimeSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.serviceCostSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.serviceTitleHiddenPlaceHolder.isHidden = false
        self.shortDescriptionHiddenPlaceHolder.isHidden = true
        self.estimatedTimeHiddenPlaceHolder.isHidden = true
        self.serviceCostHiddenPlaceHolder.isHidden = true
    }
    
    @objc func estimatedTimetextFieldDidChange(){
        self.serviceTitleSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.estimatedTimeSeperatorView.backgroundColor = UIColor.black
        self.serviceCostSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.serviceTitleHiddenPlaceHolder.isHidden = true
        self.shortDescriptionHiddenPlaceHolder.isHidden = true
        self.estimatedTimeHiddenPlaceHolder.isHidden = false
        self.serviceCostHiddenPlaceHolder.isHidden = true
        
    }
    
    @objc func serviceCosttextFieldDidChange(){
        self.serviceTitleSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.estimatedTimeSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.serviceCostSeperatorView.backgroundColor = UIColor.black
        self.serviceTitleHiddenPlaceHolder.isHidden = true
        self.shortDescriptionHiddenPlaceHolder.isHidden = true
        self.estimatedTimeHiddenPlaceHolder.isHidden = true
        self.serviceCostHiddenPlaceHolder.isHidden = false
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if shortDescriptionTextField.textColor == UIColor.lightGray {
            shortDescriptionTextField.text = ""
            shortDescriptionTextField.textColor = UIColor.black
        }
        self.serviceTitleSeperatorView.backgroundColor = UIColor.black
        self.estimatedTimeTextField.layer.borderColor = UIColor.black.cgColor
        self.serviceCostTextField.layer.borderColor = UIColor.black.cgColor
        self.serviceTitleHiddenPlaceHolder.isHidden = true
        self.shortDescriptionHiddenPlaceHolder.isHidden = false
        textView.layer.borderColor = UIColor(r: 23, g: 69, b: 90).cgColor
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.serviceTitleSeperatorView.backgroundColor = UIColor.black
        self.estimatedTimeTextField.layer.borderColor = UIColor.black.cgColor
        self.serviceCostTextField.layer.borderColor = UIColor.black.cgColor
        self.serviceTitleHiddenPlaceHolder.isHidden = true
        self.shortDescriptionHiddenPlaceHolder.isHidden = false
        textView.layer.borderColor = UIColor(r: 23, g: 69, b: 90).cgColor
        self.textFieldDidChange()
    }
    
    @objc func showCameraActionOptions(){
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancelButtonAlertOrderDetail", comment: "Cancel"), style: .cancel) { action in
        }
        alertController.addAction(cancelAction)
        
        let takePhotoAction = UIAlertAction(title: NSLocalizedString("takePhotoAlertViewProfileEdit", comment: "Take photo"), style: .default) { action in
            self.handleTakePhoto()
        }
        alertController.addAction(takePhotoAction)
        
        let choosePhotoAction = UIAlertAction(title: NSLocalizedString("choosePhotAlertViewProfileEdit", comment: "Choose photo"), style: .default) { action in
            self.handleChoosePhoto()
        }
        alertController.addAction(choosePhotoAction)
        
        self.present(alertController, animated: true) {
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            
            selectedImageFromPicker = editedImage
            
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            selectImageView.contentMode = .scaleAspectFit
            selectImageView.image = selectedImage
            self.textFieldDidChange()
        }
        
        dismiss(animated:true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func handleTakePhoto(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = true
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            self.view.makeToast(NSLocalizedString("AddServiceViewControllerErrorMessageNoCamera", comment: "Sorry, this device has no camera"), duration: 3.0, position: .bottom)
        }
    }
    
    func handleChoosePhoto(){
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    
    @objc func handleUserSegementSelection(){
        let segmentSelect = self.styleSelectionSegmentedControl.selectedSegmentIndex
        
        switch segmentSelect {
        case 0:
            self.handleAddingServices(selection: "Ladies")
        case 1:
            self.handleAddingServices(selection: "Gents")
        case 2:
            self.handleAddingServices(selection: "Kids")
        default:
            print("Sky High")
        }
        
    }
    
    func getService(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let serveID = self.serviceID, let _ = self.shopCurrency {
                
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    self.dismiss(animated: false, completion: nil)
                    //log out process based on location
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    DispatchQueue.global(qos: .background).async {
                        AF.request(self.BACKEND_URL + "getsingleshopservice/" + serveID, method: .post, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"])
                            .response { (response) in
                                
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    do {
                                        let shopServiceSingleData = try JSONDecoder().decode(ShopServiceCreated.self, from: jsonData)
                                        self.serviceTitleTextField.text = shopServiceSingleData.shopService.serviceTitle
                                        self.estimatedTimeTextField.text = shopServiceSingleData.shopService.serviceEstimatedTotalTime
                                        self.serviceCostTextField.text = shopServiceSingleData.shopService.servicePrice
                                        self.shortDescriptionTextField.text = shopServiceSingleData.shopService.serviceDescription
                                        self.shortDescriptionTextField.textColor = UIColor.black
                                        
                                        self.saveAndUpdateOrCreateButton.isEnabled = true
                                        self.servicePriceString = shopServiceSingleData.shopService.servicePrice
                                        self.serviceEstimatedTimeString = shopServiceSingleData.shopService.serviceEstimatedTotalTime
                                        
                                        let categoryService = shopServiceSingleData.shopService.serviceCategory
                                        let serviceImageFileName = shopServiceSingleData.shopService.serviceImageName
                                        
                                        if let stat = self.serviceHiddenStatus {
                                            switch (stat) {
                                            case "NO":
                                                self.addShopServiceViewHideServiceButton.isOn = true
                                                self.hideButtonTitleLabel.text = NSLocalizedString("AddServiceViewControllerActiveSwitcher", comment: "active")
                                                break
                                            case "YES":
                                                self.addShopServiceViewHideServiceButton.isOn = false
                                                self.hideButtonTitleLabel.text = NSLocalizedString("AddServiceViewControllerInactiveSwitcher", comment: "inactive")
                                                break
                                            default:
                                                print("hidden status not supplied")
                                                break
                                                
                                            }
                                        } else {
                                            self.hideButtonTitleLabel.text = "no data :("
                                        }
                                        
                                        switch categoryService {
                                        case "Ladies":
                                            self.styleSelectionSegmentedControl.selectedSegmentIndex = 0
                                        case "Gents":
                                            self.styleSelectionSegmentedControl.selectedSegmentIndex = 1
                                        case "Kids":
                                            self.styleSelectionSegmentedControl.selectedSegmentIndex = 2
                                        default:
                                            print("Sky High")
                                        }
                                        
                                        
                                        AF.request(self.BACKEND_URL + "images/shopServiceImages/" + serviceImageFileName, headers: headers).responseData { response in
                                            
                                            switch response.result {
                                            case .success(let data):
                                                let image = UIImage(data: data)
                                                self.selectImageView.image = image
                                                self.selectImageView.contentMode = .scaleAspectFit
                                                break
                                            case .failure(let error):
                                                print(error)
                                                self.view.makeToast(NSLocalizedString("AddServiceViewControllerErrorMessageImageNotFound", comment: "Image not found"), duration: 3.0, position: .bottom)
                                                break
                                            }
                                            
                                        }
                                        
                                        
                                        
                                    } catch _ {
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                self.view.makeToast(NSLocalizedString("AddServiceViewControllerErrorMessageServiceNotFound", comment: "Service was not found"), duration: 3.0, position: .bottom)
                                            case 500:
                                                self.view.makeToast(NSLocalizedString("AddServiceViewControllerErrorMessageAnErrorOccurred", comment: "An error occurred, please try again later"), duration: 3.0, position: .bottom)
                                            default:
                                                print("Sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
        }
        
    }
    
    @objc private func handleHidingShopService(_ sender: UISwitch){
        switch sender.isOn {
        case true:
            self.hideButtonTitleLabel.text = NSLocalizedString("AddServiceViewControllerActiveSwitcher", comment: "active")
            self.serviceHiddenStatus = "NO"
            break
        case false:
            self.hideButtonTitleLabel.text = NSLocalizedString("AddServiceViewControllerInactiveSwitcher", comment: "inactive")
            self.serviceHiddenStatus = "YES"
            break
        }
    }
    
    @objc private func handleDeletingShopService(_ sender: UISwitch){
        switch sender.isOn {
        case true:
            self.deleteButtonTitleLabel.text = NSLocalizedString("AddServiceViewControllerDeleteSwitcher", comment: "delete")
            self.isServiceDeleted = "YES"
            break
        case false:
            self.deleteButtonTitleLabel.text = NSLocalizedString("AddServiceViewControllerSavedSwitcher", comment: "saved")
            self.isServiceDeleted = "NO"
            break
        }
    }
    
    
    
    func handlePostEditService(selection: String){
        
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let serveID = self.serviceID, let stat = self.serviceHiddenStatus {
                
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    
                    // self.servicePriceString = shopServiceSingleData.shopService.servicePrice
                    // self.serviceEstimatedTimeString = shopServiceSingleData.shopService.serviceEstimatedTotalTime
                    if let serviceTitle = serviceTitleTextField.text, let estimatedTime = self.estimatedTimeTextField.text, let serviceCost = self.serviceCostTextField.text, let shortDescription = self.shortDescriptionTextField.text, let profileImage = selectImageView.image, let uploadData = profileImage.jpegData(compressionQuality: 0.1)  {
                        
                        let imageName = NSUUID().uuidString
                        SwiftSpinner.show("Loading...")
                        
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        let parameters = [
                            "serviceTitle": serviceTitle,
                            "serviceCategory": selection,
                            "serviceDescription": shortDescription,
                            "servicePrice": serviceCost,
                            "serviceEstimatedTotalTime": String(estimatedTime),
                            "serviceHiddenByShop": stat,
                            "serviceHiddenByAdmin": self.isServiceDeleted
                            ] as [String: Any]
                        
                        DispatchQueue.global(qos: .background).async {
                            
                            AF.upload(multipartFormData: { (multipartFormData) in
                                multipartFormData.append(uploadData, withName: "image",fileName: imageName, mimeType: "image/jpg")
                                for (key, value) in parameters {
                                    if let stringValue = value as? String {
                                        multipartFormData.append(stringValue.data(using: .utf8)!, withName: key)
                                    }
                                }
                            }, usingThreshold: UInt64.init(), fileManager: .default, to: self.BACKEND_URL + "updateshopservice/" + serveID, method: .put, headers: headers, interceptor: nil)
                                .validate(statusCode: 200..<501)
                                .validate(contentType: ["application/json"])
                                .uploadProgress(queue: .main, closure: { (progress) in
                                print("Upload Progress: \(progress.fractionCompleted)")
                            }).response { response in
                                
                                switch response.result {
                                case .success(let data):
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    if let jsonData = data {
                                        do {
                                            let _ = try JSONDecoder().decode(ShopServiceCreated.self, from: jsonData)
                                            self.handleMoveToTheNextView()
                                        } catch _ {
                                            if let statusCode = response.response?.statusCode {
                                                print(statusCode)
                                                switch statusCode {
                                                case 404:
                                                    self.view.makeToast(NSLocalizedString("AddServiceViewControllerErrorMessageServiceNotFound", comment: "Service was not found"), duration: 3.0, position: .bottom)
                                                case 500:
                                                    self.view.makeToast(NSLocalizedString("AddServiceViewControllerErrorMessageAnErrorOccurred", comment: "An error occurred, please try again later"), duration: 3.0, position: .bottom)
                                                default:
                                                    print("Sky high : update")
                                                }
                                            }
                                        }
                                    }
                                    break
                                case .failure(let error):
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    print(error)
                                    let message = NSLocalizedString("AddServiceViewControllerErrorMessageUpdateError", comment: "Update error: please try again later")
                                    self.view.makeToast(message, duration: 3.0, position: .bottom)
                                    break
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    //handle adding service by category from registration process
    func handleAddingServices(selection: String){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let shopID = self.shopID {
                
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    if let serviceTitle = serviceTitleTextField.text, let estimatedTime = self.estimatedTimeTextField.text, let serviceCost = self.serviceCostTextField.text, let shortDescription = self.shortDescriptionTextField.text, let profileImage = selectImageView.image, let uploadData = profileImage.jpegData(compressionQuality: 0.1)  {
                        
                        let imageName = NSUUID().uuidString
                        
                        let userTimezone = DateByUserDeviceInitializer.tzone
                        let userCalender = "gregorian"
                        let userLocale = "en"
                        let regionData = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: userLocale)
                        let newDate = DateInRegion().convertTo(region: regionData).toFormat("yyyy-MM-dd HH:mm:ss")
                        
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        SwiftSpinner.show("Loading...")
                        
                        let parameters = [
                            "serviceTitle": serviceTitle,
                            "serviceCategory": selection,
                            "serviceDescription": shortDescription,
                            "servicePrice": String(serviceCost),
                            "serviceEstimatedTotalTime": String(estimatedTime),
                            "serviceDateCreated": newDate,
                            "serviceDateCreatedTimezone": userTimezone,
                            "serviceDateCreatedCalendar": userCalender,
                            "serviceDateCreatedLocale": userLocale,
                            "shop": shopID
                            ] as [String: Any]
                        
                        DispatchQueue.global(qos: .background).async {
                            
                            AF.upload(multipartFormData: { (multipartFormData) in
                                multipartFormData.append(uploadData, withName: "image",fileName: imageName, mimeType: "image/jpg")
                                for (key, value) in parameters {
                                    if let stringValue = value as? String {
                                        multipartFormData.append(stringValue.data(using: .utf8)!, withName: key)
                                    }
                                }
                            }, usingThreshold: UInt64.init(), fileManager: .default, to: self.BACKEND_URL + "createservice/", method: .post, headers: headers, interceptor: nil)
                                .validate(statusCode: 200..<501)
                                .validate(contentType: ["application/json"])
                                .uploadProgress(queue: .main, closure: { (progress) in
                                    print("Upload Progress: \(progress.fractionCompleted)")
                                })
                                .response { response in
                                    
                                    switch response.result {
                                    case .success(let data):
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        if let jsonData = data {
                                            do {
                                                let serverResponse = try JSONDecoder().decode(ShopServiceCreated.self, from: jsonData)
                                                let message = serverResponse.message
                                                self.view.makeToast(message, duration: 3.0, position: .bottom)
                                                self.shopID = serverResponse.shopService.serviceShop
                                                self.handleMoveToTheNextView()
                                            } catch _ {
                                                if let statusCode = response.response?.statusCode {
                                                    switch statusCode {
                                                    case 404:
                                                        self.view.makeToast(NSLocalizedString("AddServiceViewControllerErrorMessageShopDoesNotexist", comment: "Shop service was not created as shop does exist"), duration: 3.0, position: .bottom)
                                                    case 500:
                                                        self.view.makeToast(NSLocalizedString("AddServiceViewControllerErrorMessageAnErrorOccurred", comment: "An error occurred, please try again later"), duration: 3.0, position: .bottom)
                                                    default:
                                                        print("Sky high")
                                                    }
                                                }
                                            }
                                        }
                                        break
                                    case .failure(let error):
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        print(error)
                                        let message = NSLocalizedString("AddServiceViewControllerErrorMessageUpdateError", comment: "Update error: please try again later")
                                        self.view.makeToast(message, duration: 3.0, position: .bottom)
                                        break
                                    }
                            }
                        }//here
                        
                    }
                }
            }
        }
    }
    
    @objc func handleSegmentedControlSelection(){
        let segmentSelect = self.styleSelectionSegmentedControl.selectedSegmentIndex
        self.textFieldDidChange()
        
        switch segmentSelect {
        case 0:
            self.descriptionTitle.text = NSLocalizedString("AddServiceViewControllerMessageAddServiceLadies", comment: "This will add the service in the Ladies section only")
        case 1:
            self.descriptionTitle.text = NSLocalizedString("AddServiceViewControllerMessageAddServiceGents", comment: "This will add the service in the Gents section only")
        case 2:
            self.descriptionTitle.text = NSLocalizedString("AddServiceViewControllerMessageAddServiceKids", comment: "This will add the service in the Kids section only")
        default:
            print("Sky High")
        }
    }
    
    func handleMoveToTheNextView(){
        
        if let status = self.actionStatusForView {
            switch (status) {
            case "editService":
                if let id = self.shopID {
                    let someDict = ["shopUniqueId" : id ]
                    NotificationCenter.default.post(name: .didReceiveDataStaffAndServices, object: nil, userInfo: someDict)
                    self.dismiss(animated: true, completion: nil)
                }
                break
            case "createShop":
                weak var pvc = self.presentingViewController
                self.dismiss(animated: true) {
                    if let shop = self.shopID {
                        let addBarberView = AddBarberViewController()
                        addBarberView.currentShopID = shop
                        addBarberView.actionStatusForView = "createShop"
                        addBarberView.staffHiddenStatus = "NO"
                        let navController = UINavigationController(rootViewController: addBarberView)
                        pvc?.present(navController, animated: true, completion: nil)
                    }
                }
                break
            case "addService":
                if let id = self.shopID {
                    let someDict = ["shopUniqueId" : id ]
                    NotificationCenter.default.post(name: .didReceiveDataStaffAndServices, object: nil, userInfo: someDict)
                    self.dismiss(animated: true, completion: nil)
                }
                break
            default:
                break
            }
        }
        
    }

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}
