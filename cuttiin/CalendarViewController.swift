//
//  CalendarViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/26/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import SwiftDate

class CalendarViewController: UIViewController {
    var paymentsArrayHold = [Payments]()
    var customDateHolderNoSum = [CustomDateAmountHolder]()
    var customddatesholder = [CustomDateAmountHolder]()
    var customYearHolder = [CustomYearHolder]()
    var amountPaidIntegerHolder = [Int]()
    var firstLoad = true
    var signedUserRole: String?
    var summedUpBookingArrayHolder = [String]()
    var currencyCode: String?
    
    let upperCollectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let upperCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customCalendarViewControllerCollectionViewCell.self, forCellWithReuseIdentifier: "cellIdUP")
        return cv
    }()
    
    let upperCollectionViewSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        return fnsv
    }()
    
    let lowerCollectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let lowerCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customCalendarViewControllerLowerCollectionViewCell.self, forCellWithReuseIdentifier: "cellIdDW")
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.selectedIndex = 1
        view.backgroundColor = UIColor.white
        navigationItem.title = NSLocalizedString("navigationTitleForCalenderView", comment: "Calendar")
        view.addSubview(upperCollectView)
        upperCollectionView.dataSource = self
        upperCollectionView.delegate = self
        view.addSubview(upperCollectionViewSeperatorView)
        view.addSubview(lowerCollectView)
        lowerCollectionView.dataSource = self
        lowerCollectionView.delegate = self
        setupViewObjectContraints()
        self.firstLoad = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func handleUpperCollectionSelction(yearChoice: Int){
        UserDefaults.standard.set(yearChoice, forKey: "initialYearValueToCalculateAccount")
        self.customddatesholder.removeAll()
        self.customDateHolderNoSum.removeAll()
        self.summedUpBookingArrayHolder.removeAll()
        DispatchQueue.main.async {
            self.lowerCollectionView.reloadData()
        }
    }
    
    func handleLowerCollectionViewSelection(monthSelected: String){
        if let userRole = self.signedUserRole, let curr =  self.currencyCode {
            let monthView = MonthlyPaymentViewController()
            monthView.monthChosen = monthSelected
            monthView.userRole = userRole
            monthView.currentUserCurency = curr
            navigationController?.pushViewController(monthView, animated: true)
        }
    }
    
    func setupViewObjectContraints(){
        upperCollectView.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        upperCollectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperCollectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -24).isActive = true
        upperCollectView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        upperCollectView.addSubview(upperCollectionView)
        
        upperCollectionView.topAnchor.constraint(equalTo: upperCollectView.topAnchor).isActive = true
        upperCollectionView.centerXAnchor.constraint(equalTo: upperCollectView.centerXAnchor).isActive = true
        upperCollectionView.widthAnchor.constraint(equalTo: upperCollectView.widthAnchor).isActive = true
        upperCollectionView.heightAnchor.constraint(equalTo: upperCollectView.heightAnchor).isActive = true
        
        
        upperCollectionViewSeperatorView.topAnchor.constraint(equalTo: upperCollectView.bottomAnchor).isActive = true
        upperCollectionViewSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperCollectionViewSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        upperCollectionViewSeperatorView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        lowerCollectView.topAnchor.constraint(equalTo: upperCollectionViewSeperatorView.bottomAnchor).isActive = true
        lowerCollectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        lowerCollectView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        lowerCollectView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        lowerCollectView.addSubview(lowerCollectionView)
        
        lowerCollectionView.topAnchor.constraint(equalTo: lowerCollectView.topAnchor).isActive = true
        lowerCollectionView.centerXAnchor.constraint(equalTo: lowerCollectView.centerXAnchor).isActive = true
        lowerCollectionView.widthAnchor.constraint(equalTo: lowerCollectView.widthAnchor).isActive = true
        lowerCollectionView.heightAnchor.constraint(equalTo: lowerCollectView.heightAnchor).isActive = true
        
    }

}

class customCalendarViewControllerCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let openStartTimeHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        ehp.textColor = UIColor.lightGray //UIColor(r: 118, g: 187, b: 220)
        ehp.textAlignment = .center
        return ehp
    }()
    
    func setupViews(){
        addSubview(openStartTimeHolder)
        
        backgroundColor = UIColor.clear
        addContraintsWithFormat(format: "H:|[v0]|", views: openStartTimeHolder)
        addContraintsWithFormat(format: "V:|-5-[v0]-5-|", views: openStartTimeHolder)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class customCalendarViewControllerLowerCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let monthOfTheYear: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.font = UIFont(name: "BebasNeue", size: 30)
        ehp.textColor = UIColor(r: 11, g: 49, b: 68)
        ehp.textAlignment = .left
        return ehp
    }()
    
    let totalEarningsForTheMonth: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 17)
        ehp.textColor = UIColor(r: 11, g: 49, b: 68)
        ehp.textAlignment = .right
        return ehp
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        return sv
    }()
    
    func setupViews(){
        addSubview(monthOfTheYear)
        addSubview(totalEarningsForTheMonth)
        addSubview(seperatorView)
        
        backgroundColor = UIColor.clear
        addContraintsWithFormat(format: "H:|-16-[v0(125)]-5-[v1(150)]-16-|", views: monthOfTheYear,totalEarningsForTheMonth)
        addContraintsWithFormat(format: "V:|-16-[v0]-16-[v1(1)]|", views: monthOfTheYear,seperatorView)
        addContraintsWithFormat(format: "V:|-16-[v0]-16-[v1(0.5)]|", views: totalEarningsForTheMonth,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

