//
//  AppointmentData.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 25/03/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit
import SwiftDate

struct AppointmentData {
    var id: String
    var appointmentService: [String]
    var appointmentShortUniqueID, appointmentDateStart, appointmentDateEnd, appointmentDateTimezone: String
    var appointmentDateCalendar, appointmentDateLocale, appointmentDateCreated, appointmentDescription: String
    var appointmentPrice, appointmentCurrency, appointmentCustomer, appointmentStaff: String
    var appointmentStaffCustomer, appointmentShop, appointmentServiceQuantity, appointmentStartedBy: String
    var appointmentEndedBy, appointmentStartedAt, appointmentEndedAt, appointmentStartedEndedTimezone: String
    var appointmentStartedEndedCalendar, appointmentStartedEndedLocale, appointmentPaymentMethod, appointmentStatus: String
    var appointmentPaidDate, appointmentPaidTimezone, appointmentPaidCalendar, appointmentPaidLocale: String
    var v: Int
    var appointmentCancelledBy: String
    var appontmentCustomerImage: UIImage
    var appointmentCustomerFirstName: String
    var appointmentStaffFirstName: String
    var appointmentFormatedStartEndTime: String
    var appointmentDateStartObj: DateInRegion
    var appointmentDateEndObj: DateInRegion
    var appointmentServiceWithAmount: [AppointmentServiceAmountData]
    
    init(id: String, appointmentService: [String], appointmentShortUniqueID: String, appointmentDateStart: String, appointmentDateEnd: String, appointmentDateTimezone: String,
        appointmentDateCalendar: String, appointmentDateLocale: String, appointmentDateCreated: String, appointmentDescription: String,
        appointmentPrice: String, appointmentCurrency: String, appointmentCustomer: String, appointmentStaff: String,
        appointmentStaffCustomer: String, appointmentShop: String, appointmentServiceQuantity: String, appointmentStartedBy: String,
        appointmentEndedBy: String, appointmentStartedAt: String, appointmentEndedAt: String, appointmentStartedEndedTimezone: String,
        appointmentStartedEndedCalendar: String, appointmentStartedEndedLocale: String, appointmentPaymentMethod: String, appointmentStatus: String,
        appointmentPaidDate: String, appointmentPaidTimezone: String, appointmentPaidCalendar: String, appointmentPaidLocale: String,
        v: Int, appointmentCancelledBy: String, appontmentCustomerImage: UIImage, appointmentCustomerFirstName: String, appointmentStaffFirstName: String, appointmentFormatedStartEndTime: String, appointmentDateStartObj: DateInRegion, appointmentDateEndObj: DateInRegion, appointmentServiceWithAmount: [AppointmentServiceAmountData]) {
        
        self.id = id
        self.appointmentService = appointmentService
        self.appointmentShortUniqueID = appointmentShortUniqueID
        self.appointmentDateStart = appointmentDateStart
        self.appointmentDateEnd = appointmentDateEnd
        self.appointmentDateTimezone = appointmentDateTimezone
        self.appointmentDateCalendar = appointmentDateCalendar
        self.appointmentDateLocale = appointmentDateLocale
        self.appointmentDateCreated = appointmentDateCreated
        self.appointmentDescription = appointmentDescription
        self.appointmentPrice = appointmentPrice
        self.appointmentCurrency = appointmentCurrency
        self.appointmentCustomer = appointmentCustomer
        self.appointmentStaff = appointmentStaff
        self.appointmentStaffCustomer = appointmentStaffCustomer
        self.appointmentShop = appointmentShop
        self.appointmentServiceQuantity = appointmentServiceQuantity
        self.appointmentStartedBy = appointmentStartedBy
        self.appointmentEndedBy = appointmentEndedBy
        self.appointmentStartedAt = appointmentStartedAt
        self.appointmentEndedAt = appointmentEndedAt
        self.appointmentStartedEndedTimezone = appointmentStartedEndedTimezone
        self.appointmentStartedEndedCalendar = appointmentStartedEndedCalendar
        self.appointmentStartedEndedLocale = appointmentStartedEndedLocale
        self.appointmentPaymentMethod = appointmentPaymentMethod
        self.appointmentStatus = appointmentStatus
        self.appointmentPaidDate = appointmentPaidDate
        self.appointmentPaidTimezone = appointmentPaidTimezone
        self.appointmentPaidCalendar = appointmentPaidCalendar
        self.appointmentPaidLocale = appointmentPaidLocale
        self.v = v
        self.appointmentCancelledBy = appointmentCancelledBy
        self.appontmentCustomerImage = appontmentCustomerImage
        self.appointmentCustomerFirstName = appointmentCustomerFirstName
        self.appointmentStaffFirstName = appointmentStaffFirstName
        self.appointmentFormatedStartEndTime = appointmentFormatedStartEndTime
        self.appointmentDateStartObj = appointmentDateStartObj
        self.appointmentDateEndObj = appointmentDateEndObj
        self.appointmentServiceWithAmount = appointmentServiceWithAmount
    }
}
