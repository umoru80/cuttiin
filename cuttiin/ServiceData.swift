//
//  ServiceData.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 05/03/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit

struct ServiceData {
    var serviceID: String
    var serviceTitle: String
    var serviceEstimatedTime: String
    var serviceCost: String
    var shortDescription: String
    var category: String
    var dateCreated: String
    var timezone: String
    var calendar: String
    var local: String
    var serviceImage: UIImage
    var isHiddenByShop: String
    var isHiddenByAdmin: String
    var numberOfCustomer: Int
    var serviceStaff: [String]
    var isAssigned: String
    var shop: String
    
    init(serviceID: String, serviceTitle: String, serviceEstimatedTime: String, serviceCost: String, shortDescription: String, category: String, dateCreated: String, timezone: String, calendar: String, local: String, serviceImage: UIImage, isHiddenByShop: String, isHiddenByAdmin: String, numberOfCustomer: Int, serviceStaff: [String], isAssigned: String, shop: String) {
        self.serviceID = serviceID
        
        self.serviceTitle = serviceTitle
        self.serviceEstimatedTime = serviceEstimatedTime
        self.serviceCost = serviceCost
        self.shortDescription = shortDescription
        self.category = category
        self.dateCreated = dateCreated
        self.timezone = timezone
        self.calendar = calendar
        self.local = local
        self.serviceImage = serviceImage
        self.isHiddenByShop = isHiddenByShop
        self.isHiddenByAdmin = isHiddenByAdmin
        self.numberOfCustomer = numberOfCustomer
        self.serviceStaff = serviceStaff
        self.isAssigned = isAssigned
        self.shop = shop
    }
}
