//
//  AppointmentCreated.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 06/03/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation

struct AppointmentCreated: Codable {
    let message: String
    let appointment: Appointment
}

struct Appointment: Codable {
    let id: String
    let appointmentService: [String]
    let appointmentShortUniqueID, appointmentDateStart, appointmentDateEnd, appointmentDateTimezone: String
    let appointmentDateCalendar, appointmentDateLocale, appointmentDateCreated, appointmentDescription: String
    let appointmentPrice, appointmentCurrency, appointmentCustomer, appointmentStaff: String
    let appointmentStaffCustomer, appointmentShop, appointmentServiceQuantity, appointmentStartedBy: String
    let appointmentEndedBy, appointmentStartedAt, appointmentEndedAt, appointmentStartedEndedTimezone: String
    let appointmentStartedEndedCalendar, appointmentStartedEndedLocale, appointmentPaymentMethod, appointmentStatus: String
    let appointmentPaidDate, appointmentPaidTimezone, appointmentPaidCalendar, appointmentPaidLocale: String
    let v: Int
    let appointmentCancelledBy: String
    let appointmentServiceWithAmount: [AppointmentServiceWithAmount]
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case appointmentService, appointmentShortUniqueID, appointmentDateStart, appointmentDateEnd, appointmentDateTimezone, appointmentDateCalendar, appointmentDateLocale, appointmentDateCreated, appointmentDescription, appointmentPrice, appointmentCurrency, appointmentCustomer, appointmentStaff, appointmentStaffCustomer, appointmentShop, appointmentServiceQuantity, appointmentStartedBy, appointmentEndedBy, appointmentStartedAt, appointmentEndedAt, appointmentStartedEndedTimezone, appointmentStartedEndedCalendar, appointmentStartedEndedLocale, appointmentPaymentMethod, appointmentStatus, appointmentPaidDate, appointmentPaidTimezone, appointmentPaidCalendar, appointmentPaidLocale, appointmentServiceWithAmount
        case v = "__v"
        case appointmentCancelledBy
    }
}

struct AppointmentServiceWithAmount: Codable {
    let id, appointmentServiceAmount, appointmentService: String
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case appointmentServiceAmount, appointmentService
    }
}
