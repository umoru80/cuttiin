//
//  CollectionViewHelperBarber.swift
//  cuttiin
//
//  Created by Umoru Joseph on 8/19/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate


extension OrderDetailViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.serviceOne.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customServiceCollectionViewCell
        
        cell.barberShopServiceNamePlaceHolder.text = self.serviceOne[indexPath.row].serviceTitle
        cell.barberShopServiceDescriptionPlaceHolder.text = self.serviceOne[indexPath.row].shortDescription
        cell.servicePricePlaceHolder.text = self.serviceOne[indexPath.row].serviceCost + " " + self.shopCurrency
        cell.serviceTimeTakenPlaceHolder.text = self.serviceOne[indexPath.row].serviceEstimatedTime + " min"
        cell.barberShopServiceLogoImageView.image = self.serviceOne[indexPath.row].serviceImage
        cell.upButton.isHidden = true
        cell.downButton.isHidden = true
        if let value = self.serviceOne[safe: indexPath.row]?.numberOfCustomer {
            cell.numberOfCustomerPlaceHolder.text = String(value)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let cell = self.serviceOne[safe: indexPath.row] {
            let dataReel = cell.shortDescription
            let approximateWidthOfDescription = view.frame.width - 124 // - 5 - 60 // - 5 - 5 - 40 - 5
            let size = CGSize(width: approximateWidthOfDescription, height: 1000)
            let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)]
            let estimatedFrame = NSString(string: dataReel).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
            
            return CGSize(width: collectView.frame.width, height: estimatedFrame.height + 140)
            
        }
        return CGSize(width: collectView.frame.width, height: 110)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}

extension OrderDetailCustomerViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.serviceOne.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customServiceCollectionViewCell
        
        cell.barberShopServiceNamePlaceHolder.text = self.serviceOne[indexPath.row].serviceTitle
        cell.barberShopServiceDescriptionPlaceHolder.text = self.serviceOne[indexPath.row].shortDescription
        cell.servicePricePlaceHolder.text = self.serviceOne[indexPath.row].serviceCost + " " + self.shopCurrency
        cell.serviceTimeTakenPlaceHolder.text = self.serviceOne[indexPath.row].serviceEstimatedTime + " min"
        cell.barberShopServiceLogoImageView.image = self.serviceOne[indexPath.row].serviceImage
        cell.upButton.isHidden = true
        cell.downButton.isHidden = true
        if let value = self.serviceOne[safe: indexPath.row]?.numberOfCustomer {
            cell.numberOfCustomerPlaceHolder.text = String(value)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let cell = self.serviceOne[safe: indexPath.row] {
            let dataReel = cell.shortDescription
            let approximateWidthOfDescription = view.frame.width - 124 // - 5 - 60 // - 5 - 5 - 40 - 5
            let size = CGSize(width: approximateWidthOfDescription, height: 1000)
            let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)]
            let estimatedFrame = NSString(string: dataReel).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
            
            return CGSize(width: collectView.frame.width, height: estimatedFrame.height + 140)
            
        }
        return CGSize(width: collectView.frame.width, height: 110)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}


extension BookingHistoryBarberShopDetailViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.serviceBooked.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdXBA", for: indexPath) as! customServiceCollectionViewCell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension BookingHistoryViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch self.upcomingAndCompletedSegmentedControl.selectedSegmentIndex {
        case 0:
            return self.appointmentsCustomer.count
        default:
            return self.appointmentsCustomerCancelled.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdXEB", for: indexPath) as! customUpcomingAndCompletedCollectionViewCell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}

extension UpcomingBarberShopBookingViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.appointmentsCustomer.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdBA", for: indexPath) as! customUpcomingAndCompletedCollectionViewCell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}


extension BookingDetailHistoryViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.serviceBooked.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}


extension CalendarViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.upperCollectionView {
            return self.customYearHolder.count
        } else {
            return self.customddatesholder.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.upperCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdUP", for: indexPath) as! customCalendarViewControllerCollectionViewCell
            if let yearIN = self.customYearHolder[safe: indexPath.row]?.yearStringValue {
               cell.openStartTimeHolder.text = yearIN
            } else {
                cell.openStartTimeHolder.text = nil
            }
            
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdDW", for: indexPath) as! customCalendarViewControllerLowerCollectionViewCell
            cell.monthOfTheYear.text = self.customddatesholder[indexPath.row].monthName
            if let amount = self.customddatesholder[safe: indexPath.row]?.amount, let currCode = self.currencyCode {
                
                cell.totalEarningsForTheMonth.text = String(describing: amount) + currCode
            } else {
                cell.totalEarningsForTheMonth.text = nil
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.upperCollectionView {
            return CGSize(width: 100, height: upperCollectView.frame.height)
        }else {
            return CGSize(width: lowerCollectView.frame.width, height: 80)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == self.upperCollectionView {
            return 0
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.upperCollectionView {
            if let selectedCellUpper = self.customYearHolder[safe: indexPath.row], let yearInt = selectedCellUpper.yearIntValue {
                print(yearInt, "Timbala")
                self.handleUpperCollectionSelction(yearChoice: yearInt)
            }
        } else {
            if let selectedCellLower = self.customddatesholder[safe: indexPath.row]?.monthName {
                self.handleLowerCollectionViewSelection(monthSelected: selectedCellLower)
                
            }
        }
    }
}

extension ShoppingListViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.bookedServiceArry.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customShoppingListCollectionViewCell
        cell.serviceNamePlaceHolder.text = self.bookedServiceArry[indexPath.row].serviceTitle
        cell.servicePricePlaceHolder.text = self.bookedServiceArry[indexPath.row].serviceCost
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

/*extension ProfileViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.settingButtons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customProfileSettingsCollectionViewCell
        cell.settingButtonTitlePlaceHolder.text = self.settingButtons[indexPath.row].settingsTitle
        cell.thumbnailImageView.image = self.settingButtons[indexPath.row].settingsIcon
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let profilesettingsedit = ProfileSettingsEditorViewController()
        navigationController?.pushViewController(profilesettingsedit, animated: true)
    }
}*/

extension ProfileViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.settingButtons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customProfileSettingsEditorCollectionViewCell
        cell.settingButtonTitlePlaceHolder.setTitle(self.settingButtons[indexPath.row].settingsTitle, for: .normal)
        cell.settingButtonTitlePlaceHolder.tag = indexPath.row
        cell.settingButtonTitlePlaceHolder.addTarget(self, action: #selector(handleCollectionSelction), for: .touchUpInside)
        cell.thumbnailImageView.tag = indexPath.row
        cell.thumbnailImageView.addTarget(self, action: #selector(handleCollectionSelction), for: .touchUpInside)
        cell.thumbnailImageView.setImage(self.settingButtons[indexPath.row].settingsIcon, for: .normal)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
}


extension BarberProfileViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.settingButtons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIDBPRO", for: indexPath) as! customBarbershopProfileSettingsEditorCollectionViewCell
        cell.settingButtonTitlePlaceHolder.setTitle(self.settingButtons[indexPath.row].settingsTitle, for: .normal)
        cell.settingButtonTitlePlaceHolder.tag = indexPath.row
        cell.settingButtonTitlePlaceHolder.addTarget(self, action: #selector(handleCollectionSelction), for: .touchUpInside)
        cell.thumbnailImageView.tag = indexPath.row
        cell.thumbnailImageView.addTarget(self, action: #selector(handleCollectionSelction), for: .touchUpInside)
        cell.thumbnailImageView.setImage(self.settingButtons[indexPath.row].settingsIcon, for: .normal)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension BarberShopProfileSettingsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.settingButtons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customBarbershopProfileSettingsEditorCollectionViewCell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension PayOutViewControllerViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.payCollectionViewData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIDPayoout", for: indexPath) as! customPayoutCollectionViewCellBarbers
        
        cell.payoutKey.text = self.payCollectionViewData[indexPath.row].dataKey
        cell.payoutValue.text = self.payCollectionViewData[indexPath.row].dataValue
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension MonthlyPaymentViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.barberEarningsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdMOnthlyPayment", for: indexPath) as! customMonthlyPaymentCollectionViewCell
        
        cell.barberNamePlaceHolder.text = ""
        if let barberName = self.barberEarningsArray[safe: indexPath.row]?.barberStaffName {
            cell.barberNamePlaceHolder.text = barberName
        }else {
            cell.barberNamePlaceHolder.text = ""
        }
        
        cell.thumbnailImageView.image = UIImage()
        if let barberImg = self.barberEarningsArray[safe: indexPath.row]?.profileImage {
            cell.thumbnailImageView.image = barberImg
        } else {
            cell.thumbnailImageView.image = UIImage()
        }
        
        cell.totalAmountPlaceHolder.text = ""
        if let numTotal = self.barberEarningsArray[safe: indexPath.row]?.totalBarberEarning, let currCode = self.currentUserCurency {
            cell.totalAmountPlaceHolder.text = String(describing: numTotal) + currCode
        } else {
            cell.totalAmountPlaceHolder.text = ""
        }
        
        cell.totalNumberOfClientsPlaceHolder.text = ""
        if let numClients = self.barberEarningsArray[safe: indexPath.row]?.totalBarberNumbers {
            cell.totalNumberOfClientsPlaceHolder.text = String(describing: numClients)
        } else {
            cell.totalNumberOfClientsPlaceHolder.text = ""
        }
        
        cell.creditCardAmountPlaceHolder.text = ""
        if let amCard = self.barberEarningsArray[safe: indexPath.row]?.cardPaymentAmount, let currCode = self.currentUserCurency {
            cell.creditCardAmountPlaceHolder.text = String(describing: amCard) + currCode
            
            self.creditCardAmount = Int(amCard)
            
        } else {
            cell.creditCardAmountPlaceHolder.text = ""
        }
        
        cell.creditCardNumberOfClientsPlaceHolder.text = ""
        if let numClients = self.barberEarningsArray[indexPath.row].cardPaymentNumber {
            cell.creditCardNumberOfClientsPlaceHolder.text = String(describing: numClients)
        } else {
            cell.creditCardNumberOfClientsPlaceHolder.text = ""
        }
        
        cell.mobilePayAmountPlaceHolder.text = ""
        if let numMobile = self.barberEarningsArray[safe: indexPath.row]?.mobilePaymentAmount, let currCode = self.currentUserCurency  {
            print("Heror worship never ends", numMobile)
            cell.mobilePayAmountPlaceHolder.text = String(describing: numMobile) + currCode
            
            self.mobilePayAmount = Int(numMobile)
            
        } else {
            cell.mobilePayAmountPlaceHolder.text = ""
        }
        
        cell.mobilePayNumberOfClientsPlaceHolder.text = ""
        if let numClients = self.barberEarningsArray[indexPath.row].mobilePaymentNumber {
            cell.mobilePayNumberOfClientsPlaceHolder.text = String(describing: numClients)
        } else {
            cell.mobilePayNumberOfClientsPlaceHolder.text = ""
        }
        
        cell.cashPaymentAmountPlaceHolder.text = ""
        if let numCash = self.barberEarningsArray[safe: indexPath.row]?.cashPaymentAmount , let currCode = self.currentUserCurency  {
            print("Heror worship begins", numCash)
            cell.cashPaymentAmountPlaceHolder.text = String(describing: numCash) + currCode
        } else {
            cell.cashPaymentAmountPlaceHolder.text = ""
        }
        
        cell.cashPaymentNumberOfClientsPlaceHolder.text = ""
        if let numCashClients = self.barberEarningsArray[safe: indexPath.row]?.cashPaymentNumber {
            cell.cashPaymentNumberOfClientsPlaceHolder.text = String(describing: numCashClients)
        } else {
            cell.cashPaymentNumberOfClientsPlaceHolder.text = ""
        }
        
        cell.refundAmountPlaceHolder.text = ""
        if let numRefund = self.barberEarningsArray[safe: indexPath.row]?.totalRefundsAmount, let currCode = self.currentUserCurency  {
            cell.refundAmountPlaceHolder.text = String(describing: numRefund) + currCode
        } else {
            cell.refundAmountPlaceHolder.text = ""
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 300) //260
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}


