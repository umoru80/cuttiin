//
//  GooglePlaceAddressData.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 29/03/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation

struct GooglePlaceAddressData: Codable {
    let plusCode: PlusCode
    let results: [Result]
    let status: String
    
    enum CodingKeys: String, CodingKey {
        case plusCode = "plus_code"
        case results, status
    }
}

struct PlusCode: Codable {
    let compoundCode, globalCode: String
    
    enum CodingKeys: String, CodingKey {
        case compoundCode = "compound_code"
        case globalCode = "global_code"
    }
}

struct Result: Codable {
    let addressComponents: [AddressComponent]
    let formattedAddress: String
    let geometry: Geometry
    let placeID: String
    let types: [String]
    let postcodeLocalities: [String]?
    
    enum CodingKeys: String, CodingKey {
        case addressComponents = "address_components"
        case formattedAddress = "formatted_address"
        case geometry
        case placeID = "place_id"
        case types
        case postcodeLocalities = "postcode_localities"
    }
}

struct AddressComponent: Codable {
    let longName, shortName: String
    let types: [String]
    
    enum CodingKeys: String, CodingKey {
        case longName = "long_name"
        case shortName = "short_name"
        case types
    }
}

struct Geometry: Codable {
    let location: Location
    let locationType: String
    let viewport: Bounds
    let bounds: Bounds?
    
    enum CodingKeys: String, CodingKey {
        case location
        case locationType = "location_type"
        case viewport, bounds
    }
}

struct Bounds: Codable {
    let northeast, southwest: Location
}

struct Location: Codable {
    let lat, lng: Double
}
