//
//  AvailableTimeCreated.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 13/05/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation

struct AvailableTimeCreated: Codable {
    let availableTime: [AvailableTime]
    let holidayShop: [HolidayShop]
    let holidayStaff: [HolidayStaff]
}
