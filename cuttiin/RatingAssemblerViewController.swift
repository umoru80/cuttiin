//
//  RatingAssemblerViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 8/31/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import MGStarRatingView
import SwiftDate

class RatingAssemblerViewController: UIViewController, StarRatingDelegate, UITextViewDelegate {
    var bookingCompletedHolder: BookingCompleted?
    var customerCompletedBooking: AppointmentsCustomer?
    var bookingviewcontroler: BookingsViewController?
    var specificBooking: Bookings?
    var selectedBookingIDDAX: String?
    var indexPathSelected: Int?
    
    
    let scrollView: UIScrollView = {
        let v = UIScrollView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.showsVerticalScrollIndicator = false
        v.isScrollEnabled = true
        v.backgroundColor = .clear
        return v
    }()
    
    let starView = StarRatingView()
    let attribute = StarRatingAttribute(type: .rate,point: 20,spacing: 5, emptyColor: .black, fillColor: .white)
    var navBar: UINavigationBar = UINavigationBar()
    
    let barberShopHeaderDetailsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var barberShopLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.white //(r: 23, g: 69, b: 90)
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 25
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    lazy var barberShopNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var barberShopAddressPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    //main view objects
    let headerTitle: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("headerTitleTextTatingAssemblerView", comment: "Did the barber finish the selected service?")
        ht.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        ht.textColor = UIColor.white
        ht.textAlignment = .center
        return ht
    }()
    
    let buttonContanerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var yesServedButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        st.setTitle(NSLocalizedString("yesButtonAlertOrderDetail", comment: "Yes"), for: .normal)
        st.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 25)
        st.tag = 0
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.layer.borderWidth = 2
        st.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        st.addTarget(self, action: #selector(youWereServedoOrNot), for: .touchUpInside)
        return st
    }()
    
    lazy var noServedButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        st.setTitle(NSLocalizedString("noButtonTitleTextRatingAssemblerView", comment: "NO"), for: .normal)
        st.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 25)
        st.tag = 1
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.layer.borderWidth = 2
        st.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        st.addTarget(self, action: #selector(youWereServedoOrNot), for: .touchUpInside)
        return st
    }()
    
    let noServedButtonLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        ht.textColor = UIColor.white
        ht.textAlignment = .center
        ht.isHidden = true
        return ht
    }()
    
    let barberStarButton = StarRatingView()
    let barberStarAttribute = StarRatingAttribute(type: .rate,point: 25,spacing: 20, emptyColor: UIColor(r: 11, g: 49, b: 68), fillColor: UIColor(r: 118, g: 187, b: 220))
    
    let barberRatingValueLabel: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .center
        fnhp.isHidden = true
        return fnhp
    }()
    
    let barberRatingDescriptionTextField: UITextView = {
        let em = UITextView()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        em.textColor = UIColor(r: 118, g: 187, b: 220)
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        em.text = NSLocalizedString("ratingDescriptionTextTitleRatingAssemblerView", comment: "Please enter text")
        em.tag = 5
        em.textAlignment = .justified
        em.isEditable = true
        em.isSelectable = true
        em.dataDetectorTypes = UIDataDetectorTypes.link
        em.autocorrectionType = UITextAutocorrectionType.yes
        em.spellCheckingType = UITextSpellCheckingType.yes
        em.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        em.layer.borderWidth = 2
        em.layer.masksToBounds = true
        em.layer.cornerRadius = 5
        em.isHidden = true
        return em
    }()
    
    let barberShopRatingLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("barberShopRatingLabel", comment: "Please rate your Barber Shop")
        ht.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        ht.textColor = UIColor.white
        ht.textAlignment = .center
        ht.isHidden = true
        return ht
    }()
    
    let barberShopStarButton = StarRatingView()
    let barberShopStarAttribute = StarRatingAttribute(type: .rate,point: 25,spacing: 20, emptyColor: UIColor(r: 11, g: 49, b: 68), fillColor: UIColor(r: 118, g: 187, b: 220))
    
    let barberShopRatingValueLabel: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .center
        fnhp.isHidden = true
        return fnhp
    }()
    
    let barberShopRatingDescriptionTextField: UITextView = {
        let em = UITextView()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        em.textColor = UIColor(r: 118, g: 187, b: 220)
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        em.text = NSLocalizedString("ratingDescriptionTextTitleRatingAssemblerView", comment: "Please enter text")
        em.tag = 6
        em.textAlignment = .justified
        em.isEditable = true
        em.isSelectable = true
        em.dataDetectorTypes = UIDataDetectorTypes.link
        em.autocorrectionType = UITextAutocorrectionType.yes
        em.spellCheckingType = UITextSpellCheckingType.yes
        em.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        em.layer.borderWidth = 2
        em.layer.masksToBounds = true
        em.layer.cornerRadius = 5
        em.isHidden = true
        return em
    }()
    
    lazy var submitButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        st.setTitle(NSLocalizedString("submitButtonTitleTextratingAssemblerView", comment: "SUBMIT"), for: .normal)
        st.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 25)
        st.tag = 0
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.layer.borderWidth = 2
        st.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        st.isHidden = true
        return st
    }()
    
    let errorMessagePlaceHolder: UILabel = {
        let emhp = UILabel()
        emhp.translatesAutoresizingMaskIntoConstraints = false
        emhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        emhp.textColor = UIColor.white
        emhp.textAlignment = .center
        return emhp
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleBaction))
        setNavBarToTheView()
        starView.translatesAutoresizingMaskIntoConstraints = false
        starView.isUserInteractionEnabled = false
        starView.type = .half
        starView.tag = 0
        starView.delegate = self
        view.addSubview(scrollView)
        barberStarButton.configure(barberStarAttribute, current: 0, max: 5)
        barberStarButton.translatesAutoresizingMaskIntoConstraints = false
        barberStarButton.isUserInteractionEnabled = true
        barberStarButton.type = .fill
        barberStarButton.tag = 1
        barberStarButton.delegate = self
        barberStarButton.isHidden = true
        barberShopStarButton.configure(barberShopStarAttribute, current: 0, max: 5)
        barberShopStarButton.translatesAutoresizingMaskIntoConstraints = false
        barberShopStarButton.isUserInteractionEnabled = true
        barberShopStarButton.type = .fill
        barberShopStarButton.tag = 2
        barberShopStarButton.delegate = self
        barberShopStarButton.isHidden = true
        setupViewObjects()
        self.barberRatingDescriptionTextField.delegate = self
        self.barberShopRatingDescriptionTextField.delegate = self
    }
    
    @objc func handleBaction(){
      self.dismiss(animated: true, completion: nil)
    }
    
    func handleDismisView(){
        /*if let itemPoint = self.indexPathSelected, let specialObject = self.bookingviewcontroler {
            let indexPathHolder = IndexPath(row: itemPoint, section: 0)
            specialObject.collectionView.performBatchUpdates({
                specialObject.collectionView.deleteItems(at: [indexPathHolder])
                specialObject.appointmentsCustomerIsCompleted.remove(at: itemPoint)
            }, completion: { (finished) in
                specialObject.collectionView.reloadItems(at: (specialObject.collectionView.indexPathsForVisibleItems))
                self.dismiss(animated: true, completion: nil)
            })
        }*/
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.barberRatingDescriptionTextField.resignFirstResponder()
        self.barberShopRatingDescriptionTextField.resignFirstResponder()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
    }
    
    func setNavBarToTheView() {
        self.navBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 150)
        self.view.addSubview(navBar)
        let coverLayer = CALayer()
        coverLayer.frame = navBar.bounds;
        coverLayer.backgroundColor = UIColor(r: 23, g: 69, b: 90).cgColor
        navBar.layer.addSublayer(coverLayer)
        navBar.addSubview(barberShopHeaderDetailsContainerView)
        
        barberShopHeaderDetailsContainerView.topAnchor.constraint(equalTo: navBar.topAnchor, constant: 25).isActive = true
        barberShopHeaderDetailsContainerView.centerXAnchor.constraint(equalTo: navBar.centerXAnchor).isActive = true
        barberShopHeaderDetailsContainerView.widthAnchor.constraint(equalTo: navBar.widthAnchor, constant: -96).isActive = true
        barberShopHeaderDetailsContainerView.heightAnchor.constraint(equalTo: navBar.heightAnchor, constant: -35).isActive = true
        
        barberShopHeaderDetailsContainerView.addSubview(barberShopLogoImageView)
        barberShopHeaderDetailsContainerView.addSubview(barberShopNamePlaceHolder)
        barberShopHeaderDetailsContainerView.addSubview(barberShopAddressPlaceHolder)
        barberShopHeaderDetailsContainerView.addSubview(starView)
        
        
        barberShopLogoImageView.topAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.topAnchor).isActive = true
        barberShopLogoImageView.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopLogoImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        barberShopLogoImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        barberShopNamePlaceHolder.topAnchor.constraint(equalTo: barberShopLogoImageView.bottomAnchor, constant: 5).isActive = true
        barberShopNamePlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopNamePlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberShopNamePlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        barberShopAddressPlaceHolder.topAnchor.constraint(equalTo: barberShopNamePlaceHolder.bottomAnchor, constant: 5).isActive = true
        barberShopAddressPlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopAddressPlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberShopAddressPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        starView.topAnchor.constraint(equalTo: barberShopAddressPlaceHolder.bottomAnchor, constant: 5).isActive = true
        starView.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
    }
    
    var barberShopRatingLabelHeightAnchor: NSLayoutConstraint?
    var barberShopRatingDescriptionTextFieldHeightAnchor: NSLayoutConstraint?
    
    func setupViewObjects(){
        
        scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 160).isActive = true
        scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -5).isActive = true
        
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 350)
        
        
        scrollView.addSubview(headerTitle)
        scrollView.addSubview(buttonContanerView)
        scrollView.addSubview(noServedButtonLabel)
        scrollView.addSubview(barberStarButton)
        
        scrollView.addSubview(barberRatingValueLabel)
        scrollView.addSubview(barberRatingDescriptionTextField)
        scrollView.addSubview(barberShopRatingLabel)
        scrollView.addSubview(barberShopStarButton)
        
        scrollView.addSubview(barberShopRatingValueLabel)
        scrollView.addSubview(barberShopRatingDescriptionTextField)
        scrollView.addSubview(submitButton)
        scrollView.addSubview(errorMessagePlaceHolder)
        
        headerTitle.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 5).isActive = true
        headerTitle.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        headerTitle.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        headerTitle.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        buttonContanerView.topAnchor.constraint(equalTo: headerTitle.bottomAnchor, constant: 5).isActive = true
        buttonContanerView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        buttonContanerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        buttonContanerView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        buttonContanerView.addSubview(yesServedButton)
        buttonContanerView.addSubview(noServedButton)
        
        yesServedButton.topAnchor.constraint(equalTo: buttonContanerView.topAnchor).isActive = true
        yesServedButton.leftAnchor.constraint(equalTo: buttonContanerView.leftAnchor).isActive = true
        yesServedButton.widthAnchor.constraint(equalTo: buttonContanerView.widthAnchor, multiplier: 0.5,  constant: -5).isActive = true
        yesServedButton.heightAnchor.constraint(equalTo: buttonContanerView.heightAnchor).isActive = true
        
        noServedButton.topAnchor.constraint(equalTo: buttonContanerView.topAnchor).isActive = true
        noServedButton.rightAnchor.constraint(equalTo: buttonContanerView.rightAnchor).isActive = true
        noServedButton.widthAnchor.constraint(equalTo: buttonContanerView.widthAnchor, multiplier: 0.5,  constant: -5).isActive = true
        noServedButton.heightAnchor.constraint(equalTo: buttonContanerView.heightAnchor).isActive = true
        
        noServedButtonLabel.topAnchor.constraint(equalTo: buttonContanerView.bottomAnchor, constant: 5).isActive = true
        noServedButtonLabel.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        noServedButtonLabel.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -12).isActive = true
        noServedButtonLabel.heightAnchor.constraint(equalToConstant: 18).isActive = true
        
        barberStarButton.topAnchor.constraint(equalTo: noServedButtonLabel.bottomAnchor, constant: 5).isActive = true
        barberStarButton.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        
        barberRatingValueLabel.topAnchor.constraint(equalTo: noServedButtonLabel.bottomAnchor, constant: 5).isActive = true
        barberRatingValueLabel.leftAnchor.constraint(equalTo: barberStarButton.rightAnchor, constant: 5).isActive = true
        barberRatingValueLabel.widthAnchor.constraint(equalToConstant: 50).isActive = true
        barberRatingValueLabel.heightAnchor.constraint(equalTo: barberStarButton.heightAnchor, multiplier: 1).isActive = true
        
        barberRatingDescriptionTextField.topAnchor.constraint(equalTo: barberStarButton.bottomAnchor, constant: 5).isActive = true
        barberRatingDescriptionTextField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        barberRatingDescriptionTextField.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -72).isActive = true
        barberRatingDescriptionTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        barberShopRatingLabel.topAnchor.constraint(equalTo: barberRatingDescriptionTextField.bottomAnchor, constant: 5).isActive = true
        barberShopRatingLabel.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        barberShopRatingLabel.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -72).isActive = true
        barberShopRatingLabelHeightAnchor = barberShopRatingLabel.heightAnchor.constraint(equalToConstant: 20)
        barberShopRatingLabelHeightAnchor?.isActive = true
        
        barberShopStarButton.topAnchor.constraint(equalTo: barberShopRatingLabel.bottomAnchor, constant: 5).isActive = true
        barberShopStarButton.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        
        barberShopRatingValueLabel.topAnchor.constraint(equalTo: barberShopRatingLabel.bottomAnchor, constant: 5).isActive = true
        barberShopRatingValueLabel.leftAnchor.constraint(equalTo: barberShopStarButton.rightAnchor, constant: 5).isActive = true
        barberShopRatingValueLabel.widthAnchor.constraint(equalToConstant: 50).isActive = true
        barberShopRatingValueLabel.heightAnchor.constraint(equalTo: barberShopStarButton.heightAnchor, multiplier: 1)
        
        barberShopRatingDescriptionTextField.topAnchor.constraint(equalTo: barberShopStarButton.bottomAnchor, constant: 5).isActive = true
        barberShopRatingDescriptionTextField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        barberShopRatingDescriptionTextField.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -72).isActive = true
        barberShopRatingDescriptionTextFieldHeightAnchor = barberShopRatingDescriptionTextField.heightAnchor.constraint(equalToConstant: 50)
        barberShopRatingDescriptionTextFieldHeightAnchor?.isActive = true
        
        submitButton.topAnchor.constraint(equalTo: barberShopRatingDescriptionTextField.bottomAnchor, constant: 15).isActive = true
        submitButton.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1, constant: -72).isActive = true
        submitButton.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        submitButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        errorMessagePlaceHolder.topAnchor.constraint(equalTo: submitButton.bottomAnchor, constant: 5).isActive = true
        errorMessagePlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        errorMessagePlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
        errorMessagePlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -48).isActive = true
    }
    
    func StarRatingValueChanged(view: StarRatingView, value: CGFloat) {
        // use value
        switch view.tag {
        case 0:
            print(value)
        case 1:
            let dataVal = String(describing: value)
            self.barberRatingValueLabel.text = dataVal
        case 2:
            let dataSVal = String(describing: value)
            self.barberShopRatingValueLabel.text = dataSVal
        default:
            print("sky shigh")
        }
    }
    var barberStarButtonHeightAnchor: NSLayoutConstraint?
    var barberShopStarButtonHeightAnchor: NSLayoutConstraint?
    
    @objc func youWereServedoOrNot(sender: UIButton){
        switch sender.tag {
        case 0:
            //print("Tapped YES")
            UserDefaults.standard.set(0, forKey: "dataWhichButtonWasChosenByUserAboutRatingView")
            self.noServedButtonLabel.text = NSLocalizedString("servedButtonTitleTextAssemblerView", comment: "Please rate your Barber.")
            self.noServedButtonLabel.isHidden = false
            
            self.barberStarButtonHeightAnchor?.isActive = false
            self.barberStarButton.isHidden = false
            
            self.barberRatingValueLabel.isHidden = false
            
            self.barberRatingDescriptionTextField.isHidden = false
            
            self.barberShopRatingLabel.isHidden = false
            self.barberShopRatingLabelHeightAnchor?.isActive = false
            self.barberShopRatingLabelHeightAnchor = barberShopRatingLabel.heightAnchor.constraint(equalToConstant: 20)
            self.barberShopRatingLabelHeightAnchor?.isActive = true
            
            self.barberShopRatingLabelHeightAnchor?.isActive = false
            
            self.barberShopStarButtonHeightAnchor?.isActive = false
            self.barberShopStarButton.isHidden = false
            
            self.barberShopRatingValueLabel.isHidden = false
            
            self.barberShopRatingDescriptionTextField.isHidden = false
            self.barberShopRatingDescriptionTextFieldHeightAnchor?.isActive = false
            self.barberShopRatingDescriptionTextFieldHeightAnchor = barberShopRatingDescriptionTextField.heightAnchor.constraint(equalToConstant: 50)
            self.barberShopRatingDescriptionTextFieldHeightAnchor?.isActive = true
            
            self.submitButton.isHidden = false
            
        case 1:
            //print("Tapped NO")
            UserDefaults.standard.set(1, forKey: "dataWhichButtonWasChosenByUserAboutRatingView")
            self.noServedButtonLabel.isHidden = false
            self.noServedButtonLabel.text = NSLocalizedString("notServedButtonTitleTextAssemblerView", comment: "Please tell us what happened")
            
            self.barberStarButtonHeightAnchor?.isActive = false
            self.barberStarButtonHeightAnchor = barberStarButton.heightAnchor.constraint(equalToConstant: 0)
            self.barberStarButtonHeightAnchor?.isActive = true
            self.barberStarButton.isHidden = true
            
            self.barberRatingValueLabel.isHidden = true
            
            self.barberRatingDescriptionTextField.isHidden = false
            
            self.barberShopRatingLabelHeightAnchor?.isActive = false
            self.barberShopRatingLabelHeightAnchor = barberShopRatingLabel.heightAnchor.constraint(equalToConstant: 0)
            self.barberShopRatingLabelHeightAnchor?.isActive = true
            
            self.barberShopStarButton.isHidden = true
            self.barberShopStarButtonHeightAnchor?.isActive = false
            self.barberShopStarButtonHeightAnchor = barberShopStarButton.heightAnchor.constraint(equalToConstant: 0)
            self.barberShopStarButtonHeightAnchor?.isActive = true
            
            self.barberShopRatingValueLabel.isHidden = true
            
            self.barberRatingDescriptionTextField.isHidden = false
            
            self.barberShopRatingDescriptionTextFieldHeightAnchor?.isActive = false
            self.barberShopRatingDescriptionTextFieldHeightAnchor = barberShopRatingDescriptionTextField.heightAnchor.constraint(equalToConstant: 0)
            self.barberShopRatingDescriptionTextFieldHeightAnchor?.isActive = true
            
            self.submitButton.isHidden = false
        default:
            print("sky high")
        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        switch textView.tag {
        case 5:
            if textView.text == NSLocalizedString("ratingDescriptionTextTitleRatingAssemblerView", comment: "Please enter text") {
                textView.text = ""
            }
        case 6:
            if textView.text == NSLocalizedString("ratingDescriptionTextTitleRatingAssemblerView", comment: "Please enter text") {
                textView.text = ""
            }
        default:
            print("Sky High")
        }
    }
    
    
    
    
    func calculateRatingScore(fiveStr: Int, fourStr: Int, threeStr: Int, twoStr: Int, oneStr: Int) -> Int{
        let numerator = 5 * fiveStr + 4 * fourStr + 3 * threeStr + 2 * twoStr + 1 * oneStr
        let denominator = fiveStr + fourStr + threeStr + twoStr + oneStr
        
        let wAverage = Double(numerator / denominator)
        let intData = Int(wAverage.roundTo(places: 1))
        return intData
    }

}
