//
//  BarberShopsViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/13/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import MGStarRatingView
import UserNotifications
import SwiftDate
import FBSDKLoginKit
import Alamofire
import SwiftyJSON
import SwiftSpinner

class BarberShopsViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    let network = NetworkManager.sharedInstance
    var mapMarkers = [MarkerforMap]()
    var shopsDataArray = [MarkerforMap]()
    
    var locationManager = CLLocationManager()
    var cordinatesPlaces: CLLocationCoordinate2D?
    var mapView = GMSMapView()
    var bMarkers = [GMSMarker]()
    var barbermarkers = [BarberMarker]()
    var searchedLocMarker = [GMSMarker]()
    var shopRatingRequestArray = [ShopRatingRequest]()
    let mySpecialNotificationKey = "com.teckdkapps.specialNotificationKey"
    var appearOnce = false
    
    var accountDeactivated: String?
    var refreshMarkersWithoutMovingCamera = false
    var didFindLocation = false
    
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    lazy var profileViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "avatar_icon")
        imageView.setImageColor(color: UIColor(r: 11, g: 49, b: 68))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .center
        imageView.layer.cornerRadius = 25
        imageView.layer.masksToBounds = true
        imageView.backgroundColor = UIColor.white
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showProfileView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let searchBarContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 245, g: 245, b: 245)
        view.layer.masksToBounds = true
        view.layer.cornerRadius =  5
        view.layer.borderColor = UIColor.black.cgColor
        view.layer.borderWidth = 0.2
        return view
    }()
    
    lazy var searchBarTextInputPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 17)
        fnhp.text = NSLocalizedString("searchBarTextBarberShopsView", comment: "Search")
        fnhp.textColor = UIColor.black
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        fnhp.isUserInteractionEnabled = true
        fnhp.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowPlaceAutoComplete)))
        return fnhp
    }()
    
    lazy var searchBarShowNearbyImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "icons8appointmentHistory")
        imageView.setImageColor(color: UIColor.black)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .center
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showAllCurrentBookings)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    lazy var selectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let googleAutoCompleteButton: UIButton = {
        let gAutoComp = UIButton()
        gAutoComp.translatesAutoresizingMaskIntoConstraints = false
        gAutoComp.setTitle(NSLocalizedString("barberShopViewPlacesButtonTitle", comment: "search using google places"), for: .normal)
        gAutoComp.setTitleColor(UIColor.black, for: .normal)
        gAutoComp.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 14)
        gAutoComp.addTarget(self, action: #selector(handleAutoCompleteTrigger), for: .touchUpInside)
        gAutoComp.backgroundColor = UIColor(r: 245, g: 245, b: 245)
        gAutoComp.layer.masksToBounds = true
        gAutoComp.layer.cornerRadius =  5
        gAutoComp.layer.borderColor = UIColor.black.cgColor
        gAutoComp.layer.borderWidth = 0.2
        gAutoComp.isEnabled = true
        return gAutoComp
    }()
    
    
    var dictionaryTotalCount = 0
    var calledCount = 0
    
    
    // modal bottom view
    let modalViewBarContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.clear
        view.isHidden = true
        return view
    }()
    
    lazy var swipeDownIconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleHideModalContianer)))
        imageView.isHidden = true
        return imageView
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.isUserInteractionEnabled = true
        cv.register(customSlideUpCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()
    
    // modal bottom view
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = UIColor.white
        mapView = GMSMapView(frame: self.view.bounds)
        mapView.delegate = self
        mapView.accessibilityElementsHidden = true
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        mapView.settings.scrollGestures = true
        mapView.settings.zoomGestures = true
        view.addSubview(mapView)
        view.addSubview(profileViewButton)
        view.addSubview(searchBarContainerView)
        view.addSubview(googleAutoCompleteButton)
        view.addSubview(modalViewBarContainerView)
        view.addSubview(swipeDownIconImageView)
        view.bringSubviewToFront(profileViewButton)
        view.bringSubviewToFront(searchBarContainerView)
        view.bringSubviewToFront(googleAutoCompleteButton)
        
        view.bringSubviewToFront(modalViewBarContainerView)
        view.bringSubviewToFront(swipeDownIconImageView)
        
        setupObjectContraints()
        //location manager to fetch current location
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        didFindLocation = false
        network.reachability.whenUnreachable = { reachability in
            self.view.makeToast("No internet connection", duration: 2.0, position: .bottom)
        }
        
        collectionView.dataSource = self
        collectionView.delegate = self
        getCustomerDetails()
        self.appearOnce = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .didReceiveDataMapViewRefresher, object: nil)
    }
    
    @objc private func onDidReceiveData(_ notification:Notification) {
        if let dataBack = notification.userInfo as? [String: AnyObject], let _ = dataBack["shopUniqueId"] as? String {
            self.getCustomerDetails()
            self.refreshMarkersWithoutMovingCamera = true
            self.locationManager.startUpdatingLocation()
            didFindLocation = false
            network.reachability.whenUnreachable = { reachability in
                self.view.makeToast("No internet connection", duration: 2.0, position: .bottom)
            }
        }
    }
    
    @objc func saveDeviceToken(){
        var deviceTokensData = [String]()
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let customerId = decodedCustomer.first?.customerId, let devicetoken = UserDefaults.standard.object(forKey: "pushyToken") as? String {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                deviceTokensData.append(devicetoken)
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    DispatchQueue.global(qos: .background).async {
                        
                        let regionData = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                        let newDate = DateInRegion().convertTo(region: regionData).toFormat("yyyy-MM-dd HH:mm:ss")
                        
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        let parameters: Parameters = [
                            "deviceToken": deviceTokensData,
                            "customer": customerId,
                            "dateCreated": newDate,
                            "dateCreatedTimezone": timeZone,
                            "dateCreatedCalendar": "gregorian",
                            "dateCreatedLocale": "en"
                        ]
                        AF.request(self.BACKEND_URL + "devicetoken", method: .post, parameters: parameters, encoding: JSONEncoding(options: []), headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"])
                            .response (completionHandler: { (response) in
                                
                                if let error = response.error {
                                    print("Error: ",error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    do {
                                        let serverResponse = try JSONDecoder().decode(DeviceTokenCreated.self, from: jsonData)
                                        let message = serverResponse.message
                                        print(message)
                                        
                                    } catch _ {
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 409:
                                                print("unique validation error with device token")
                                            case 500:
                                                print("An error occurred with device token")
                                            default:
                                                print("sky high")
                                            }
                                        }
                                    }
                                }
                            })
                    }
                }
            }
        }
    }

    
    @objc func showProfileView(){
        if let accDeac = self.accountDeactivated, accDeac == "NO"  {
            let profileView = ProfileViewController()
            profileView.barberShopsViewController = self
            profileView.modalPresentationStyle = .overCurrentContext
            self.present(profileView, animated: true, completion: nil)
            
        } else {
            self.view.makeToast("Account has been deactivated please contact StaySharp Customer care", duration: 5.0, position: .center)
        }
    }
    
    @objc func handleShowPlaceAutoComplete(){
        
        let nearByShops = NearByBarberShopViewController()
        nearByShops.barberShopViewController = self
        nearByShops.modalPresentationStyle = .overCurrentContext
        present(nearByShops, animated: true, completion: nil)
    }
    
    @objc func handleAutoCompleteTrigger() {
        let autocompleteController = GMSAutocompleteViewController()
         autocompleteController.delegate = self
         autocompleteController.modalPresentationStyle = .overCurrentContext
         present(autocompleteController, animated: true, completion: nil)
        
    }
    
    @objc func showAllCurrentBookings(){
        let bookingView = BookingsViewController()
        bookingView.modalPresentationStyle = .overCurrentContext
        self.present(bookingView, animated: true, completion: nil)
        
    }
    
    @objc func getCustomerDetails(){
        DispatchQueue.global(qos: .background).async {
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let customerID = decodedCustomer.first?.customerId, let token = decodedCustomer.first?.token {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        AF.request(self.BACKEND_URL + customerID, method: .get, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    do {
                                        let customerSingleData = try JSONDecoder().decode(CustomerData.self, from: jsonData)
                                        self.accountDeactivated = customerSingleData.customer.accountDeactivated
                                        
                                        if (customerSingleData.customer.accountDeactivated == "YES")  {
                                            self.view.makeToast("Account has been deactivated please contact StaySharp Customer care", duration: 5.0, position: .center)
                                        }
                                        
                                        let profileImageFileName = customerSingleData.customer.profileImageName
                                        self.saveDeviceToken()
                                        AF.request(self.BACKEND_URL + "images/customerImages/" + profileImageFileName, headers: headers).responseData { response in
                                            
                                            switch response.result {
                                            case .success(let data):
                                                let image = UIImage(data: data)
                                                DispatchQueue.main.async {
                                                    self.profileViewButton.image = image
                                                    self.profileViewButton.contentMode = .scaleAspectFit
                                                }
                                                break
                                            case .failure(let error):
                                                print(error)
                                                self.view.makeToast("Image not found", duration: 3.0, position: .bottom)
                                                break
                                            }
                                            
                                        }
                                        
                                    } catch _ {
                                        
                                        if let statusCode = response.response?.statusCode{
                                            switch (statusCode) {
                                            case 404:
                                                self.view.makeToast("Customer not found", duration: 2.0, position: .bottom)
                                            case 500:
                                                self.view.makeToast("an error ocurred", duration: 2.0, position: .bottom)
                                            default:
                                                print("Sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                        
                    }
                }
            }
        }
        
    }
    
    func getAllMarkers(deviceLocation: CLLocation, countryCode: String){
        mapView.clear()
        self.mapMarkers.removeAll()
        self.bMarkers.removeAll()
        DispatchQueue.global(qos: .background).async {
            
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        AF.request(self.BACKEND_URL + "getallshopsByCountryCode/" + countryCode, method: .post, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"])
                            .response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    do {
                                        let shopArrayData = try JSONDecoder().decode([Shop].self, from: jsonData)
                                        for shop in shopArrayData {
                                            let shopLogoImageFileName = shop.shopLogoImageName
                                            var ratingIneter = 0
                                            if let val = Int(shop.shopRating) {
                                                ratingIneter = val
                                            }
                                            
                                            AF.request(self.BACKEND_URL + "images/shopImages/" + shopLogoImageFileName, headers: headers).responseData { response in
                                                
                                                switch response.result {
                                                case .success(let data):
                                                    if let image = UIImage(data: data)  {
                                                        if let addLong = Double(shop.shopAddressLongitude), let addLati = Double(shop.shopAddressLatitude) {
                                                            let locValue:CLLocationCoordinate2D = CLLocationCoordinate2DMake(addLati, addLong)
                                                            let locValueForDistance: CLLocation = CLLocation(latitude: addLati, longitude: addLong)
                                                            let shopDistanceFromDevice = deviceLocation.distance(from: locValueForDistance)
                                                            
                                                            
                                                            if(self.mapMarkers.isEmpty) {
                                                                
                                                                let singleMapMarker = MarkerforMap(shopId: shop.id, shopName: shop.shopName, shopAddress: shop.shopAddress, shopCordinates: locValue, shopGenderCategory: shop.shopCustomerGender, locationCLL: locValueForDistance, longitudeDouble: addLong, latitudeDouble: addLati, shopLogoImage: image, rating: ratingIneter, shopCurrency: shop.shopCountryCurrencyCode, shopVerification: shop.shopVerificationStatus, shopCategory: shop.shopCategory, shopDistanceFromDevice: shopDistanceFromDevice)
                                                                self.mapMarkers.append(singleMapMarker)
                                                                self.mapMarkers.sort { (first, second) -> Bool in
                                                                    first.shopName.lowercased() < second.shopName.lowercased()
                                                                }
                                                                
                                                                var stringArrayID = [String]()
                                                                stringArrayID.append(shop.id)
                                                                let imageName = self.getCategoryIconImageName(category: shop.shopCategory)
                                                                let markerImage = UIImage(named: imageName)
                                                                let markerView = UIImageView(image: markerImage)
                                                                let desiredMarker = GMSMarker(position: locValue)
                                                                desiredMarker.iconView = markerView
                                                                desiredMarker.userData = stringArrayID
                                                                desiredMarker.map = self.mapView
                                                                self.bMarkers.append(desiredMarker)
                                                                stringArrayID.removeAll()
                                                            } else {
                                                                if let firstWhere = self.mapMarkers.first(where: {$0.locationCLL.distance(from: locValueForDistance) <= 100 }) {
                                                                    
                                                                    for x in self.bMarkers {
                                                                        if var shopIdData = x.userData as? [String]{
                                                                            if (shopIdData.contains(firstWhere.shopId)) {
                                                                                shopIdData.append(shop.id)
                                                                                x.userData = shopIdData
                                                                                let imageNamefirst = self.getCategoryIconImageName(category: "multiple")
                                                                                let markerImage = UIImage(named: imageNamefirst)
                                                                                let markerView = UIImageView(image: markerImage)
                                                                                x.iconView = markerView
                                                                                
                                                                                let singleMapMarker = MarkerforMap(shopId: shop.id, shopName: shop.shopName, shopAddress: shop.shopAddress, shopCordinates: locValue, shopGenderCategory: shop.shopCustomerGender, locationCLL: locValueForDistance, longitudeDouble: addLong, latitudeDouble: addLati, shopLogoImage: image, rating: ratingIneter, shopCurrency: shop.shopCountryCurrencyCode, shopVerification: shop.shopVerificationStatus, shopCategory: shop.shopCategory, shopDistanceFromDevice: shopDistanceFromDevice)
                                                                                self.mapMarkers.append(singleMapMarker)
                                                                                self.mapMarkers.sort { (first, second) -> Bool in
                                                                                    first.shopName.lowercased() < second.shopName.lowercased()
                                                                                }
                                                                                
                                                                            }
                                                                        }
                                                                    }
                                                                } else {
                                                                    
                                                                    let singleMapMarker = MarkerforMap(shopId: shop.id, shopName: shop.shopName, shopAddress: shop.shopAddress, shopCordinates: locValue, shopGenderCategory: shop.shopCustomerGender, locationCLL: locValueForDistance, longitudeDouble: addLong, latitudeDouble: addLati, shopLogoImage: image, rating: ratingIneter, shopCurrency: shop.shopCountryCurrencyCode, shopVerification: shop.shopVerificationStatus, shopCategory: shop.shopCategory, shopDistanceFromDevice: shopDistanceFromDevice)
                                                                    self.mapMarkers.append(singleMapMarker)
                                                                    self.mapMarkers.sort { (first, second) -> Bool in
                                                                        first.shopName.lowercased() < second.shopName.lowercased()
                                                                    }
                                                                    
                                                                    var stringArrayIDX = [String]()
                                                                    stringArrayIDX.append(shop.id)
                                                                    let imageName = self.getCategoryIconImageName(category: shop.shopCategory)
                                                                    let markerImage = UIImage(named: imageName)
                                                                    let markerView = UIImageView(image: markerImage)
                                                                    let desiredMarker = GMSMarker(position: locValue)
                                                                    desiredMarker.iconView = markerView
                                                                    desiredMarker.userData = stringArrayIDX
                                                                    desiredMarker.map = self.mapView
                                                                    self.bMarkers.append(desiredMarker)
                                                                    stringArrayIDX.removeAll()
                                                                }
                                                                
                                                            }
                                                            
                                                            
                                                            
                                                        }
                                                    } else {
                                                        if let image = UIImage(named: "icon_shop")  {
                                                            if let addLong = Double(shop.shopAddressLongitude), let addLati = Double(shop.shopAddressLatitude) {
                                                                let locValue:CLLocationCoordinate2D = CLLocationCoordinate2DMake(addLati, addLong)
                                                                let locValueForDistance: CLLocation = CLLocation(latitude: addLati, longitude: addLong)
                                                                let shopDistanceFromDevice = deviceLocation.distance(from: locValueForDistance)
                                                                
                                                                
                                                                if(self.mapMarkers.isEmpty) {
                                                                    
                                                                    let singleMapMarker = MarkerforMap(shopId: shop.id, shopName: shop.shopName, shopAddress: shop.shopAddress, shopCordinates: locValue, shopGenderCategory: shop.shopCustomerGender, locationCLL: locValueForDistance, longitudeDouble: addLong, latitudeDouble: addLati, shopLogoImage: image, rating: ratingIneter, shopCurrency: shop.shopCountryCurrencyCode, shopVerification: shop.shopVerificationStatus, shopCategory: shop.shopCategory, shopDistanceFromDevice: shopDistanceFromDevice)
                                                                    self.mapMarkers.append(singleMapMarker)
                                                                    self.mapMarkers.sort { (first, second) -> Bool in
                                                                        first.shopName.lowercased() < second.shopName.lowercased()
                                                                    }
                                                                    
                                                                    var stringArrayID = [String]()
                                                                    stringArrayID.append(shop.id)
                                                                    let imageName = self.getCategoryIconImageName(category: shop.shopCategory)
                                                                    let markerImage = UIImage(named: imageName)
                                                                    let markerView = UIImageView(image: markerImage)
                                                                    let desiredMarker = GMSMarker(position: locValue)
                                                                    desiredMarker.iconView = markerView
                                                                    desiredMarker.userData = stringArrayID
                                                                    desiredMarker.map = self.mapView
                                                                    self.bMarkers.append(desiredMarker)
                                                                    stringArrayID.removeAll()
                                                                } else {
                                                                    if let firstWhere = self.mapMarkers.first(where: {$0.locationCLL.distance(from: locValueForDistance) <= 100 }) {
                                                                        
                                                                        for x in self.bMarkers {
                                                                            if var shopIdData = x.userData as? [String]{
                                                                                if (shopIdData.contains(firstWhere.shopId)) {
                                                                                    shopIdData.append(shop.id)
                                                                                    x.userData = shopIdData
                                                                                    let imageNamefirst = self.getCategoryIconImageName(category: "multiple")
                                                                                    let markerImage = UIImage(named: imageNamefirst)
                                                                                    let markerView = UIImageView(image: markerImage)
                                                                                    x.iconView = markerView
                                                                                    
                                                                                    let singleMapMarker = MarkerforMap(shopId: shop.id, shopName: shop.shopName, shopAddress: shop.shopAddress, shopCordinates: locValue, shopGenderCategory: shop.shopCustomerGender, locationCLL: locValueForDistance, longitudeDouble: addLong, latitudeDouble: addLati, shopLogoImage: image, rating: ratingIneter, shopCurrency: shop.shopCountryCurrencyCode, shopVerification: shop.shopVerificationStatus, shopCategory: shop.shopCategory, shopDistanceFromDevice: shopDistanceFromDevice)
                                                                                    self.mapMarkers.append(singleMapMarker)
                                                                                    self.mapMarkers.sort { (first, second) -> Bool in
                                                                                        first.shopName.lowercased() < second.shopName.lowercased()
                                                                                    }
                                                                                    
                                                                                }
                                                                            }
                                                                        }
                                                                    } else {
                                                                        
                                                                        let singleMapMarker = MarkerforMap(shopId: shop.id, shopName: shop.shopName, shopAddress: shop.shopAddress, shopCordinates: locValue, shopGenderCategory: shop.shopCustomerGender, locationCLL: locValueForDistance, longitudeDouble: addLong, latitudeDouble: addLati, shopLogoImage: image, rating: ratingIneter, shopCurrency: shop.shopCountryCurrencyCode, shopVerification: shop.shopVerificationStatus, shopCategory: shop.shopCategory, shopDistanceFromDevice: shopDistanceFromDevice)
                                                                        self.mapMarkers.append(singleMapMarker)
                                                                        self.mapMarkers.sort { (first, second) -> Bool in
                                                                            first.shopName.lowercased() < second.shopName.lowercased()
                                                                        }
                                                                        
                                                                        var stringArrayIDX = [String]()
                                                                        stringArrayIDX.append(shop.id)
                                                                        let imageName = self.getCategoryIconImageName(category: shop.shopCategory)
                                                                        let markerImage = UIImage(named: imageName)
                                                                        let markerView = UIImageView(image: markerImage)
                                                                        let desiredMarker = GMSMarker(position: locValue)
                                                                        desiredMarker.iconView = markerView
                                                                        desiredMarker.userData = stringArrayIDX
                                                                        desiredMarker.map = self.mapView
                                                                        self.bMarkers.append(desiredMarker)
                                                                        stringArrayIDX.removeAll()
                                                                    }
                                                                    
                                                                }
                                                            }
                                                        }
                                                    }
                                                    break
                                                case .failure(let error):
                                                    print(error)
                                                    self.view.makeToast("Image not found", duration: 3.0, position: .bottom)
                                                    if let image = UIImage(named: "icon_shop")  {
                                                        if let addLong = Double(shop.shopAddressLongitude), let addLati = Double(shop.shopAddressLatitude) {
                                                            let locValue:CLLocationCoordinate2D = CLLocationCoordinate2DMake(addLati, addLong)
                                                            let locValueForDistance: CLLocation = CLLocation(latitude: addLati, longitude: addLong)
                                                            let shopDistanceFromDevice = deviceLocation.distance(from: locValueForDistance)
                                                            
                                                            if(self.mapMarkers.isEmpty) {
                                                                
                                                                let singleMapMarker = MarkerforMap(shopId: shop.id, shopName: shop.shopName, shopAddress: shop.shopAddress, shopCordinates: locValue, shopGenderCategory: shop.shopCustomerGender, locationCLL: locValueForDistance, longitudeDouble: addLong, latitudeDouble: addLati, shopLogoImage: image, rating: ratingIneter, shopCurrency: shop.shopCountryCurrencyCode, shopVerification: shop.shopVerificationStatus, shopCategory: shop.shopCategory, shopDistanceFromDevice: shopDistanceFromDevice)
                                                                self.mapMarkers.append(singleMapMarker)
                                                                self.mapMarkers.sort { (first, second) -> Bool in
                                                                    first.shopName.lowercased() < second.shopName.lowercased()
                                                                }
                                                                
                                                                var stringArrayID = [String]()
                                                                stringArrayID.append(shop.id)
                                                                let imageName = self.getCategoryIconImageName(category: shop.shopCategory)
                                                                let markerImage = UIImage(named: imageName)
                                                                let markerView = UIImageView(image: markerImage)
                                                                let desiredMarker = GMSMarker(position: locValue)
                                                                desiredMarker.iconView = markerView
                                                                desiredMarker.userData = stringArrayID
                                                                desiredMarker.map = self.mapView
                                                                self.bMarkers.append(desiredMarker)
                                                                stringArrayID.removeAll()
                                                            } else {
                                                                if let firstWhere = self.mapMarkers.first(where: {$0.locationCLL.distance(from: locValueForDistance) <= 100 }) {
                                                                    
                                                                    for x in self.bMarkers {
                                                                        if var shopIdData = x.userData as? [String]{
                                                                            if (shopIdData.contains(firstWhere.shopId)) {
                                                                                shopIdData.append(shop.id)
                                                                                x.userData = shopIdData
                                                                                let imageNamefirst = self.getCategoryIconImageName(category: "multiple")
                                                                                let markerImage = UIImage(named: imageNamefirst)
                                                                                let markerView = UIImageView(image: markerImage)
                                                                                x.iconView = markerView
                                                                                
                                                                                let singleMapMarker = MarkerforMap(shopId: shop.id, shopName: shop.shopName, shopAddress: shop.shopAddress, shopCordinates: locValue, shopGenderCategory: shop.shopCustomerGender, locationCLL: locValueForDistance, longitudeDouble: addLong, latitudeDouble: addLati, shopLogoImage: image, rating: ratingIneter, shopCurrency: shop.shopCountryCurrencyCode, shopVerification: shop.shopVerificationStatus, shopCategory: shop.shopCategory, shopDistanceFromDevice: shopDistanceFromDevice)
                                                                                self.mapMarkers.append(singleMapMarker)
                                                                                self.mapMarkers.sort { (first, second) -> Bool in
                                                                                    first.shopName.lowercased() < second.shopName.lowercased()
                                                                                }
                                                                                
                                                                            }
                                                                        }
                                                                    }
                                                                } else {
                                                                    
                                                                    let singleMapMarker = MarkerforMap(shopId: shop.id, shopName: shop.shopName, shopAddress: shop.shopAddress, shopCordinates: locValue, shopGenderCategory: shop.shopCustomerGender, locationCLL: locValueForDistance, longitudeDouble: addLong, latitudeDouble: addLati, shopLogoImage: image, rating: ratingIneter, shopCurrency: shop.shopCountryCurrencyCode, shopVerification: shop.shopVerificationStatus, shopCategory: shop.shopCategory, shopDistanceFromDevice: shopDistanceFromDevice)
                                                                    self.mapMarkers.append(singleMapMarker)
                                                                    self.mapMarkers.sort { (first, second) -> Bool in
                                                                        first.shopName.lowercased() < second.shopName.lowercased()
                                                                    }
                                                                    
                                                                    var stringArrayIDX = [String]()
                                                                    stringArrayIDX.append(shop.id)
                                                                    let imageName = self.getCategoryIconImageName(category: shop.shopCategory)
                                                                    let markerImage = UIImage(named: imageName)
                                                                    let markerView = UIImageView(image: markerImage)
                                                                    let desiredMarker = GMSMarker(position: locValue)
                                                                    desiredMarker.iconView = markerView
                                                                    desiredMarker.userData = stringArrayIDX
                                                                    desiredMarker.map = self.mapView
                                                                    self.bMarkers.append(desiredMarker)
                                                                    stringArrayIDX.removeAll()
                                                                }
                                                                
                                                            }
                                                        }
                                                    }
                                                    break
                                                }
                                                
                                                
                                            }
                                        }
                                        
                                    } catch _ {
                                        
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                self.view.makeToast("No Shops available in your country", duration: 2.0, position: .bottom)
                                            case 500:
                                                self.view.makeToast("An error occurred, please try again a little later", duration: 2.0, position: .bottom)
                                            default:
                                                print("sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
    }
    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let shopsData = marker.userData as? [String] {
            if let accDeac = self.accountDeactivated, accDeac == "NO"  {
                var tapMarkerData = [MarkerforMap]()
                let arrLen = shopsData.count
                var arrCount = 0
                for data in shopsData  {
                    arrCount = arrCount + 1
                    if let first = self.mapMarkers.first(where: { $0.shopId == data }){
                        tapMarkerData.append(first)
                    }
                    if ( arrCount == arrLen) {
                        self.shopsDataArray.removeAll()
                        self.shopsDataArray = tapMarkerData
                        self.shopsDataArray.sort { (first, second) -> Bool in
                            first.shopName.lowercased() < second.shopName.lowercased()
                        }
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                        }
                        self.modalViewBarContainerView.isHidden = false
                        self.swipeDownIconImageView.isHidden = false
                    }
                }
                
            } else {
                self.view.makeToast("Account has been deactivated please contact StaySharp Customer care", duration: 4.0, position: .center)
            }
            
        }
        return true
    }
    
    @objc func getCategoryIconImageName(category: String) -> String {
        switch category {
        case "Barbershop":
            return "icons8barbershop"
        case "Hair salon":
            return "icons8womanshair"
        case "Nail salon":
            return "icons8manicure"
        case "Spa":
            return "icons8spaflower"
        case "Tattoo":
            return "icons8tattoomachine"
        case "Massage":
            return "icons8woodenmassagetable"
        case "Piercings":
            return "icons8earpiercing"
        case "Beauty salon":
            return "icons8hairwashingsink"
        case "multiple":
            return "icons8building"
        default:
            return "icons8barbershop"
        }
    }
    
    @objc func getCategoryDanishTranslation(category: String) -> String {
        switch category {
        case "Barbershop":
            return NSLocalizedString("barberShopViewCategoryBarbershop", comment: "Barbershop")
        case "Hair salon":
            return NSLocalizedString("barberShopViewCategoryHairSalon", comment: "Frisør")
        case "Nail salon":
            return NSLocalizedString("barberShopViewCategoryNailSalon", comment: "Negle salon")
        case "Spa":
            return NSLocalizedString("barberShopViewCategorySpa", comment: "Spa")
        case "Tattoo":
            return NSLocalizedString("barberShopViewCategoryTattoo", comment: "Tatovering")
        case "Massage":
            return NSLocalizedString("barberShopViewCategoryMasssage", comment: "Massage")
        case "Piercings":
            return NSLocalizedString("barberShopViewCategoryPiercing", comment: "Piercinger")
        case "Beauty salon":
            return NSLocalizedString("barberShopViewCategoryBeautySalon", comment: "skønhedssalon")
        default:
            return "Barbershop"
        }
    }
    
    @objc func handleHideModalContianer(){
        self.modalViewBarContainerView.isHidden = true
        self.swipeDownIconImageView.isHidden = true
    }
        
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        
        if let latDegrees = mapView.myLocation?.coordinate.latitude, let lonDegress = mapView.myLocation?.coordinate.longitude {
            
            let addressData = CLLocation(latitude: latDegrees, longitude: lonDegress)
            let camera = GMSCameraPosition.camera(withLatitude: latDegrees, longitude: lonDegress, zoom: 18)
            self.mapView.camera = camera
            self.mapView.animate(to: camera)
            self.reverseGeoCoding(locationData: addressData)
            return true
        } else {
            return false
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if didFindLocation == false {
            if let location = locations.last {
                if (self.refreshMarkersWithoutMovingCamera == true) {
                    let deviceLocValueForDistance: CLLocation = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                    self.reverseDeviceLocaleByLocData(locationData: location, deviceLocation: deviceLocValueForDistance)
                    self.locationManager.stopUpdatingLocation()
                    didFindLocation = true
                    self.refreshMarkersWithoutMovingCamera = false
                } else {
                    let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 18)
                    self.mapView.camera = camera
                    self.mapView.animate(to: camera)
                    self.reverseGeoCoding(locationData: location)
                    let deviceLocValueForDistance: CLLocation = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                    self.reverseDeviceLocaleByLocData(locationData: location, deviceLocation: deviceLocValueForDistance)
                    self.locationManager.stopUpdatingLocation()
                    didFindLocation = true
                }
            }
        }
    }
    
    
    //properly the event of a user logging out
    @objc func handleUpdateUserAuthData(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let customerId = decodedCustomer.first?.customerId, let shopOwner = decodedCustomer.first?.shopOwner, let timeZone = decodedCustomer.first?.timezone, let authType = decodedCustomer.first?.authType, let email = decodedCustomer.first?.email, let tokenData = decodedCustomer.first?.token, let autoSwitch = decodedCustomer.first?.autoShopSwitch {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let expiredTime = DateInRegion().convertTo(region: region) - 1.seconds
                let expiredTimeString = expiredTime.toFormat("yyyy-MM-dd HH:mm:ss")
                
                let teams = [CustomerToken(expiresIn: expiredTimeString, customerId: customerId, token: tokenData, timezone: timeZone, shopOwner: shopOwner, authType: authType, email: email, autoShopSwitch: autoSwitch)]
                
                let userDefaults = UserDefaults.standard
                let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: teams)
                userDefaults.set(encodedData, forKey: "token")
                userDefaults.synchronize()
                LoginManager().logOut()
                let welcomeviewcontroller = WelcomeViewController()
                self.present(welcomeviewcontroller, animated: true, completion: nil)
                
            }
            
        }
    }
    
    func reverseGeoCoding(locationData: CLLocation){
        let longitude = locationData.coordinate.longitude.description
        let latitude = locationData.coordinate.latitude.description
        let placesApiKey = "AIzaSyCzQUcGTvZo3ZAfDg4SDZWEOvm2mV6Co-Y"
        
        
        AF.request("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&key=" + placesApiKey)
            .validate(statusCode: 200..<500)
            .validate(contentType: ["application/json"])
            .response { response in
            
                if let error = response.error {
                    print("zones", error.localizedDescription)
                    return
                }
                
                var streetnumber = ""
                var route = ""
                var locality = ""
                var administrative_area_level_2 = ""
                var administrative_area_level_1 = ""
                var countryName = ""
                
                
                
                if let jsonData = response.data {
                    do {
                        let addressData = try JSONDecoder().decode(GooglePlaceAddressData.self, from: jsonData)
                        let counterVal = addressData.results.count
                        var counterAddCheck = 0
                        for addComp in addressData.results {
                            counterAddCheck = counterAddCheck + 1
                            
                            if let types = addComp.types.first {
                                switch (types) {
                                case "street_address":
                                    if let streetnum = addComp.addressComponents[safe: 0]?.longName {
                                        streetnumber = String(streetnum)
                                    }
                                case "route":
                                    if let routename = addComp.addressComponents[safe: 0]?.longName {
                                        route = routename
                                    }
                                case "locality":
                                    if let localityname = addComp.addressComponents[safe: 0]?.longName {
                                        locality = localityname
                                    }
                                case "administrative_area_level_2":
                                    if let adminAreaLevel2 = addComp.addressComponents[safe: 0]?.longName {
                                        administrative_area_level_2 = adminAreaLevel2
                                    }
                                case "administrative_area_level_1":
                                    if let adminAreaLevel1 = addComp.addressComponents[safe: 0]?.longName {
                                        administrative_area_level_1 = adminAreaLevel1
                                    }
                                case "country":
                                    if let countryNames = addComp.addressComponents[safe: 0]?.longName {
                                        countryName = countryNames
                                    }
                                default:
                                    print("Sky high")
                                }
                                
                            }
                            
                            if (counterAddCheck == counterVal) {
                                let formalAddress = streetnumber + " " + route + " " + locality + " " + administrative_area_level_2 + " " + administrative_area_level_1 + " " + countryName
                                self.searchBarTextInputPlaceHolder.text = formalAddress
                            }
                            
                        }
                        
                    } catch _ {
                        print("An error occured")
                    }
                    
                }
        }
    }
    
    func reverseDeviceLocaleByLocData(locationData: CLLocation, deviceLocation: CLLocation){
        let longitude = locationData.coordinate.longitude.description
        let latitude = locationData.coordinate.latitude.description
        let placesApiKey = "AIzaSyCzQUcGTvZo3ZAfDg4SDZWEOvm2mV6Co-Y"
        
        
        AF.request("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&key=" + placesApiKey)
            .validate(statusCode: 200..<500)
            .validate(contentType: ["application/json"])
            .response { response in
                
                if let error = response.error {
                    print("zones", error.localizedDescription)
                    return
                }
                
                var countryName = ""
                if let jsonData = response.data {
                    do {
                        let addressData = try JSONDecoder().decode(GooglePlaceAddressData.self, from: jsonData)
                        let counterVal = addressData.results.count
                        var counterAddCheck = 0
                        for addComp in addressData.results {
                            counterAddCheck = counterAddCheck + 1
                            
                            if let types = addComp.types.first {
                                
                                if types == "country" {
                                    if let countryNames = addComp.addressComponents[safe: 0]?.shortName {
                                        countryName = countryNames
                                    }
                                }
                                
                            }
                            
                            if (counterAddCheck == counterVal) {
                                let countryCodeID = countryName
                                self.getAllMarkers(deviceLocation: deviceLocation, countryCode: countryCodeID)
                            }
                            
                        }
                        
                    } catch _ {
                        print("An error occured")
                    }
                    
                }
        }
    }
    
    public func getUnpaidAppointments(shopId: String, shopCurrency: String){
        DispatchQueue.global(qos: .background).async {
            
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let customer = decodedCustomer.first?.customerId {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        
                        DispatchQueue.main.async {
                            SwiftSpinner.show("Loading...")
                        }
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        let parameters: Parameters = [
                            "shopCurrency": shopCurrency,
                        ]
                        
                        AF.request(self.BACKEND_URL + "getAllCancelledAppointmentByCustomer/" + customer, method: .post, parameters: parameters, encoding: JSONEncoding(options: []),  headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"])
                            .response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    DispatchQueue.main.async {
                                    SwiftSpinner.hide()
                                    }
                                    do {
                                        let unpaidAppointmentData = try JSONDecoder().decode(UnpaidAppointment.self, from: jsonData)
                                        let cancelledAppointmentData = unpaidAppointmentData.appointmentCancelled
                                        var chargesArrayHolder = [Double]()
                                        for cancel in cancelledAppointmentData{
                                            if let priceDouble = Double(cancel.appointmentCancelledCharges) {
                                                chargesArrayHolder.append(priceDouble)
                                            }
                                            
                                            if(chargesArrayHolder.count == cancelledAppointmentData.count){
                                                let totalDoubleSum = chargesArrayHolder.reduce(0, +).roundTo(places: 2)
                                                if (totalDoubleSum > 0) {
                                                    
                                                    let createAppointmentView = CreateAppointmentViewController()
                                                    createAppointmentView.modalPresentationStyle = .overCurrentContext
                                                    createAppointmentView.shopID = shopId
                                                    createAppointmentView.barberShopsViewController = self
                                                    self.present(createAppointmentView, animated: true, completion: nil)
                                                } else {
                                                    let createAppointmentView = CreateAppointmentViewController()
                                                    createAppointmentView.modalPresentationStyle = .overCurrentContext
                                                    createAppointmentView.shopID = shopId
                                                    createAppointmentView.barberShopsViewController = self
                                                    self.present(createAppointmentView, animated: true, completion: nil)
                                                }
                                            }
                                        }
                                        
                                    } catch _ {
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                let createAppointmentView = CreateAppointmentViewController()
                                                createAppointmentView.modalPresentationStyle = .overCurrentContext
                                                createAppointmentView.shopID = shopId
                                                createAppointmentView.barberShopsViewController = self
                                                self.present(createAppointmentView, animated: true, completion: nil)
                                            case 500:
                                                print("An error occurred, please try again later")
                                            default:
                                                print("sky high", statusCode)
                                            }
                                        }
                                        
                                    }
                                } else {
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
    }
    
    public func handleMoveCamera(latitude: CLLocationDegrees, longitude: CLLocationDegrees){
        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 18)
        self.mapView.camera = camera
        self.mapView.animate(to: camera)
        let location = CLLocation(latitude: latitude, longitude: longitude)
        self.reverseGeoCoding(locationData: location)
    }
    
    func setupObjectContraints(){
        
        var profileViewButtonTopDistance = CGFloat(integerLiteral: 60)
        var profileButtonHeight = CGFloat(integerLiteral: 50)
        var profileButtonWidth = CGFloat(integerLiteral: 50)
        var profileButtonCornerRadius = CGFloat(integerLiteral: 25)
        var searchBarContainerViewHeight = CGFloat(integerLiteral: 50)
        var searchBarShowNearbyImageViewWidth = CGFloat(integerLiteral: 50)
        var swipeDownIconImageViewHeight = CGFloat(integerLiteral: 30)
        var swipeDownIconImageViewWidth = CGFloat(integerLiteral: 30)
        var modularViewAddedHeight = CGFloat(integerLiteral: 1)
        var placesSearchViewAddedWidth = CGFloat(integerLiteral: 1)
        
        switch UIDevice.current.screenType {
        case UIDevice.ScreenType.iPhones_4_4S:
            profileViewButtonTopDistance = 20
            profileButtonHeight = 40
            profileButtonWidth = 40
            profileButtonCornerRadius = 20
            searchBarContainerViewHeight = 40
            searchBarShowNearbyImageViewWidth = 40
            swipeDownIconImageViewHeight = 20
            swipeDownIconImageViewWidth = 20
            modularViewAddedHeight = 55
            placesSearchViewAddedWidth = 30
            break
        case UIDevice.ScreenType.iPhones_5_5s_5c_SE:
            profileViewButtonTopDistance = 20
            profileButtonHeight = 40
            profileButtonWidth = 40
            profileButtonCornerRadius = 20
            searchBarContainerViewHeight = 40
            searchBarShowNearbyImageViewWidth = 40
            swipeDownIconImageViewHeight = 20
            swipeDownIconImageViewWidth = 20
            modularViewAddedHeight = 50
            placesSearchViewAddedWidth = 25
            break
        case UIDevice.ScreenType.iPhones_X_XS:
            profileViewButtonTopDistance = 60
            profileButtonHeight = 50
            profileButtonWidth = 50
            profileButtonCornerRadius = 25
            searchBarContainerViewHeight = 50
            searchBarShowNearbyImageViewWidth = 50
            swipeDownIconImageViewHeight = 30
            swipeDownIconImageViewWidth = 30
            modularViewAddedHeight = 29
            break
        case UIDevice.ScreenType.iPhone_XR:
            profileViewButtonTopDistance = 60
            profileButtonHeight = 50
            profileButtonWidth = 50
            profileButtonCornerRadius = 25
            searchBarContainerViewHeight = 50
            searchBarShowNearbyImageViewWidth = 50
            swipeDownIconImageViewHeight = 30
            swipeDownIconImageViewWidth = 30
            modularViewAddedHeight = -20
            break
        case UIDevice.ScreenType.iPhone_XSMax:
            profileViewButtonTopDistance = 60
            profileButtonHeight = 50
            profileButtonWidth = 50
            profileButtonCornerRadius = 25
            searchBarContainerViewHeight = 50
            searchBarShowNearbyImageViewWidth = 50
            swipeDownIconImageViewHeight = 30
            swipeDownIconImageViewWidth = 30
            modularViewAddedHeight = -25
            break
        case UIDevice.ScreenType.iPhones_6_6s_7_8:
            profileViewButtonTopDistance = 20
            profileButtonHeight = 50
            profileButtonWidth = 50
            profileButtonCornerRadius = 25
            searchBarContainerViewHeight = 50
            searchBarShowNearbyImageViewWidth = 50
            swipeDownIconImageViewHeight = 30
            swipeDownIconImageViewWidth = 30
            modularViewAddedHeight = 25
            break
        case UIDevice.ScreenType.iPhones_6Plus_6sPlus_7Plus_8Plus:
            profileViewButtonTopDistance = 20
            profileButtonHeight = 50
            profileButtonWidth = 50
            profileButtonCornerRadius = 25
            searchBarContainerViewHeight = 50
            searchBarShowNearbyImageViewWidth = 50
            swipeDownIconImageViewHeight = 30
            swipeDownIconImageViewWidth = 30
            modularViewAddedHeight = 15
            break
        default:
            profileViewButtonTopDistance = 20
            profileButtonHeight = 50
            profileButtonWidth = 50
            profileButtonCornerRadius = 25
            searchBarContainerViewHeight = 50
            searchBarShowNearbyImageViewWidth = 50
            swipeDownIconImageViewHeight = 30
            swipeDownIconImageViewWidth = 30
            break
        }
        
        profileViewButton.topAnchor.constraint(equalTo: mapView.topAnchor, constant: profileViewButtonTopDistance).isActive = true
        profileViewButton.leftAnchor.constraint(equalTo: mapView.leftAnchor, constant: 10).isActive = true
        profileViewButton.widthAnchor.constraint(equalToConstant: profileButtonHeight).isActive = true
        profileViewButton.heightAnchor.constraint(equalToConstant: profileButtonWidth).isActive = true
        profileViewButton.layer.cornerRadius = profileButtonCornerRadius
        
        searchBarContainerView.topAnchor.constraint(equalTo: profileViewButton.bottomAnchor, constant: 10).isActive = true
        searchBarContainerView.centerXAnchor.constraint(equalTo: mapView.centerXAnchor).isActive = true
        searchBarContainerView.widthAnchor.constraint(equalTo: mapView.widthAnchor, constant: -20).isActive = true
        searchBarContainerView.heightAnchor.constraint(equalToConstant: searchBarContainerViewHeight).isActive = true
        
        searchBarContainerView.addSubview(searchBarShowNearbyImageView)
        searchBarContainerView.addSubview(searchBarTextInputPlaceHolder)
        
        searchBarShowNearbyImageView.topAnchor.constraint(equalTo: searchBarContainerView.topAnchor).isActive = true
        searchBarShowNearbyImageView.leftAnchor.constraint(equalTo: searchBarContainerView.leftAnchor).isActive = true
        searchBarShowNearbyImageView.widthAnchor.constraint(equalToConstant: searchBarShowNearbyImageViewWidth).isActive = true
        searchBarShowNearbyImageView.heightAnchor.constraint(equalTo: searchBarContainerView.heightAnchor).isActive = true
        
        searchBarTextInputPlaceHolder.topAnchor.constraint(equalTo: searchBarContainerView.topAnchor).isActive = true
        searchBarTextInputPlaceHolder.rightAnchor.constraint(equalTo: searchBarContainerView.rightAnchor, constant: -5).isActive = true
        searchBarTextInputPlaceHolder.leftAnchor.constraint(equalTo: searchBarShowNearbyImageView.rightAnchor).isActive = true
        searchBarTextInputPlaceHolder.heightAnchor.constraint(equalTo: searchBarContainerView.heightAnchor).isActive = true
        
        googleAutoCompleteButton.topAnchor.constraint(equalTo: searchBarContainerView.bottomAnchor, constant: 10).isActive = true
        googleAutoCompleteButton.centerXAnchor.constraint(equalTo: mapView.centerXAnchor).isActive = true
        googleAutoCompleteButton.widthAnchor.constraint(equalTo: mapView.widthAnchor, multiplier: 0.5, constant: placesSearchViewAddedWidth).isActive = true
        googleAutoCompleteButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        modalViewBarContainerView.heightAnchor.constraint(equalTo: mapView.heightAnchor, multiplier: 0.25, constant: modularViewAddedHeight).isActive = true
        modalViewBarContainerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -40).isActive = true
        modalViewBarContainerView.centerXAnchor.constraint(equalTo: mapView.centerXAnchor).isActive = true
        modalViewBarContainerView.widthAnchor.constraint(equalTo: mapView.widthAnchor, constant: -20).isActive = true
        
        swipeDownIconImageView.rightAnchor.constraint(equalTo: mapView.rightAnchor, constant: -10).isActive = true
        swipeDownIconImageView.widthAnchor.constraint(equalToConstant: swipeDownIconImageViewWidth).isActive = true
        swipeDownIconImageView.heightAnchor.constraint(equalToConstant: swipeDownIconImageViewHeight).isActive = true
        swipeDownIconImageView.bottomAnchor.constraint(equalTo: modalViewBarContainerView.topAnchor, constant: 10).isActive = true
        
        modalViewBarContainerView.addSubview(collectView)
        
        collectView.topAnchor.constraint(equalTo: modalViewBarContainerView.topAnchor, constant: 10).isActive = true
        collectView.centerXAnchor.constraint(equalTo: modalViewBarContainerView.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: modalViewBarContainerView.widthAnchor).isActive = true
        collectView.bottomAnchor.constraint(equalTo: modalViewBarContainerView.bottomAnchor).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
    }

}


extension BarberShopsViewController: GMSAutocompleteViewControllerDelegate, StarRatingDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 18)
        self.mapView.camera = camera
        self.mapView.animate(to: camera)
        let location = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        self.reverseGeoCoding(locationData: location)
        
        // dismiss(animated: true, completion: nil)
        self.dismiss(animated: true) {
            let someDict = ["shopUniqueId" : "refresh view please" ]
            NotificationCenter.default.post(name: .didReceiveDataMapViewRefresher, object: nil, userInfo: someDict)
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        //dismiss(animated: true, completion: nil)
        self.dismiss(animated: true) {
            let someDict = ["shopUniqueId" : "refresh view please" ]
            NotificationCenter.default.post(name: .didReceiveDataMapViewRefresher, object: nil, userInfo: someDict)
        }
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.shopsDataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customSlideUpCollectionViewCell
        cell.barberShopLogoImageView.image = shopsDataArray[indexPath.row].shopLogoImage
        cell.barberShopNamePlaceHolder.text = shopsDataArray[indexPath.row].shopName
        let distance = self.shopsDataArray[indexPath.row].shopDistanceFromDevice / 1000
        cell.barberShopAddressPlaceHolder.text = shopsDataArray[indexPath.row].shopAddress + " || " + String(distance.roundTo(places: 1)) + " km"
        let categoryTag = self.getCategoryDanishTranslation(category: shopsDataArray[indexPath.row].shopCategory)
        if (shopsDataArray[indexPath.row].shopVerification == "YES") {
            cell.barberShopVerificationPlaceHolder.text =  categoryTag + " || " + NSLocalizedString("barberShopViewVerified", comment: "verified")
        } else {
            cell.barberShopVerificationPlaceHolder.text = categoryTag + " || " + NSLocalizedString("barberShopViewNotVerified", comment: "not verified")
        }
        cell.starView.current = CGFloat(integerLiteral: shopsDataArray[indexPath.row].rating)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch UIDevice.current.screenType {
        case UIDevice.ScreenType.iPhones_4_4S:
            return CGSize(width: 170, height: collectView.frame.height)
        case UIDevice.ScreenType.iPhones_5_5s_5c_SE:
            return CGSize(width: 170, height: collectView.frame.height)
        case UIDevice.ScreenType.iPhones_X_XS:
            return CGSize(width: 220, height: collectView.frame.height)
        case UIDevice.ScreenType.iPhones_6_6s_7_8:
            return CGSize(width: 220, height: collectView.frame.height)
        case UIDevice.ScreenType.iPhone_XSMax:
            return CGSize(width: 220, height: collectView.frame.height)
        default:
            return CGSize(width: 220, height: collectView.frame.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.getUnpaidAppointments(shopId: shopsDataArray[indexPath.row].shopId, shopCurrency: shopsDataArray[indexPath.row].shopCurrency)
        
    }
    
    func StarRatingValueChanged(view: StarRatingView, value: CGFloat) {
        // use value
    }
    
}

class customSlideUpCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    
    let starView = StarRatingView()
    let attribute = StarRatingAttribute(type: .rate,point: 10,spacing: 5, emptyColor: .gray, fillColor: UIColor(r: 244, g: 222, b: 172))
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    let coverImageHolderView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    lazy var barberShopCoverImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = UIView.ContentMode.scaleAspectFill
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 5
        imageView.image = UIImage(named: "bigversionoffronimage")
        return imageView
    }()
    
    lazy var barberShopLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.layer.masksToBounds = true
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = UIColor.white.cgColor
        return imageView
    }()
    
    lazy var barberShopNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 13)
        fnhp.textColor = UIColor.black
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var barberShopVerificationPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 11)
        fnhp.textColor = UIColor.black
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var barberShopAddressPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 11)
        fnhp.textColor = UIColor.black
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()

    
    func setupViews(){
        addSubview(coverImageHolderView)
        addSubview(barberShopCoverImageView)
        addSubview(barberShopLogoImageView)
        bringSubviewToFront(barberShopLogoImageView)
        addSubview(barberShopNamePlaceHolder)
        addSubview(barberShopVerificationPlaceHolder)
        addSubview(barberShopAddressPlaceHolder)
        addSubview(starView)
        addSubview(seperatorView)
        
        starView.configure(attribute, current: 0, max: 5)
        starView.translatesAutoresizingMaskIntoConstraints = false
        starView.isUserInteractionEnabled = false
        
        backgroundColor = UIColor.white
        layer.cornerRadius = 5
        layer.masksToBounds = true
        layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 0.2
        
        switch UIDevice.current.screenType {
        case  UIDevice.ScreenType.iPhones_4_4S:
            barberShopLogoImageView.layer.cornerRadius = 25
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: coverImageHolderView)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopCoverImageView)
            addContraintsWithFormat(format: "H:|-15-[v0(50)]-5-|", views: barberShopLogoImageView)
            addContraintsWithFormat(format: "V:|-5-[v0(50)]-5-|", views: coverImageHolderView)
            addContraintsWithFormat(format: "V:|-5-[v0(50)]-5-|", views: barberShopCoverImageView)
            addContraintsWithFormat(format: "V:|-25-[v0(50)]-5-[v1(20)]-5-[v2(20)]-5-[v3(20)]-5-[v4(15)]-5-[v5]-5-|", views: barberShopLogoImageView, barberShopNamePlaceHolder, barberShopVerificationPlaceHolder, barberShopAddressPlaceHolder, starView, seperatorView)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopNamePlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopVerificationPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopAddressPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: starView)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: seperatorView)
            break
        case UIDevice.ScreenType.iPhones_5_5s_5c_SE:
            barberShopLogoImageView.layer.cornerRadius = 25
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: coverImageHolderView)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopCoverImageView)
            addContraintsWithFormat(format: "H:|-15-[v0(50)]-5-|", views: barberShopLogoImageView)
            addContraintsWithFormat(format: "V:|-5-[v0(50)]-5-|", views: coverImageHolderView)
            addContraintsWithFormat(format: "V:|-5-[v0(50)]-5-|", views: barberShopCoverImageView)
            addContraintsWithFormat(format: "V:|-25-[v0(50)]-5-[v1(20)]-5-[v2(20)]-5-[v3(20)]-5-[v4(15)]-5-[v5]-5-|", views: barberShopLogoImageView, barberShopNamePlaceHolder, barberShopVerificationPlaceHolder, barberShopAddressPlaceHolder, starView, seperatorView)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopNamePlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopVerificationPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopAddressPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: starView)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: seperatorView)
            break
        case UIDevice.ScreenType.iPhones_X_XS:
            barberShopLogoImageView.layer.cornerRadius = 35
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: coverImageHolderView)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopCoverImageView)
            addContraintsWithFormat(format: "H:|-15-[v0(70)]-5-|", views: barberShopLogoImageView)
            addContraintsWithFormat(format: "V:|-5-[v0(70)]-5-|", views: coverImageHolderView)
            addContraintsWithFormat(format: "V:|-5-[v0(70)]-5-|", views: barberShopCoverImageView)
            addContraintsWithFormat(format: "V:|-35-[v0(70)]-5-[v1(20)]-5-[v2(20)]-5-[v3(30)]-5-[v4(15)]-5-[v5]-5-|", views: barberShopLogoImageView, barberShopNamePlaceHolder, barberShopVerificationPlaceHolder, barberShopAddressPlaceHolder, starView, seperatorView)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopNamePlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopVerificationPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopAddressPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: starView)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: seperatorView)
                
            break
        case UIDevice.ScreenType.iPhones_6_6s_7_8:
            barberShopLogoImageView.layer.cornerRadius = 25
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: coverImageHolderView)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopCoverImageView)
            addContraintsWithFormat(format: "H:|-15-[v0(50)]-5-|", views: barberShopLogoImageView)
            addContraintsWithFormat(format: "V:|-5-[v0(50)]-5-|", views: coverImageHolderView)
            addContraintsWithFormat(format: "V:|-5-[v0(50)]-5-|", views: barberShopCoverImageView)
            addContraintsWithFormat(format: "V:|-25-[v0(50)]-5-[v1(20)]-5-[v2(20)]-5-[v3(20)]-5-[v4(15)]-5-[v5]-5-|", views: barberShopLogoImageView, barberShopNamePlaceHolder, barberShopVerificationPlaceHolder, barberShopAddressPlaceHolder, starView, seperatorView)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopNamePlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopVerificationPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopAddressPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: starView)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: seperatorView)
            break
        case UIDevice.ScreenType.iPhones_6Plus_6sPlus_7Plus_8Plus:
            barberShopLogoImageView.layer.cornerRadius = 25
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: coverImageHolderView)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopCoverImageView)
            addContraintsWithFormat(format: "H:|-15-[v0(50)]-5-|", views: barberShopLogoImageView)
            addContraintsWithFormat(format: "V:|-5-[v0(50)]-5-|", views: coverImageHolderView)
            addContraintsWithFormat(format: "V:|-5-[v0(50)]-5-|", views: barberShopCoverImageView)
            addContraintsWithFormat(format: "V:|-25-[v0(50)]-5-[v1(20)]-5-[v2(20)]-5-[v3(20)]-5-[v4(15)]-5-[v5]-5-|", views: barberShopLogoImageView, barberShopNamePlaceHolder, barberShopVerificationPlaceHolder, barberShopAddressPlaceHolder, starView, seperatorView)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopNamePlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopVerificationPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopAddressPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: starView)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: seperatorView)
            break
        default:
            barberShopLogoImageView.layer.cornerRadius = 25
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: coverImageHolderView)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopCoverImageView)
            addContraintsWithFormat(format: "H:|-15-[v0(50)]-5-|", views: barberShopLogoImageView)
            addContraintsWithFormat(format: "V:|-5-[v0(50)]-5-|", views: coverImageHolderView)
            addContraintsWithFormat(format: "V:|-5-[v0(50)]-5-|", views: barberShopCoverImageView)
            addContraintsWithFormat(format: "V:|-25-[v0(50)]-5-[v1(20)]-5-[v2(20)]-5-[v3(20)]-5-[v4(15)]-5-[v5]-5-|", views: barberShopLogoImageView, barberShopNamePlaceHolder, barberShopVerificationPlaceHolder, barberShopAddressPlaceHolder, starView, seperatorView)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopNamePlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopVerificationPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: barberShopAddressPlaceHolder)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: starView)
            addContraintsWithFormat(format: "H:|-5-[v0]-5-|", views: seperatorView)
            break
        }
        
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

















