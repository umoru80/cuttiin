//
//  BarberProfileViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/26/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate
import FBSDKLoginKit
import MGStarRatingView

class BarberProfileViewController: UIViewController {
    var settingButtons = [Settings]()
    var calledOnceHolder = true
    var userRoleGotten: String?
    var barberRatingCounterHold = [Ratings]()
    var selectedShopUniqueID: String?
    var selectedShopCurrency: String?
    var appointmentView = AppointmentsViewController()
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    lazy var BarbershopSettingsViewIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8settingsIcon")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var shopProfileCloseViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(exitThisView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let editProfilePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("barberProfileViewControllerTitle", comment: "Settings")
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 20) // HelveticaNeue-Medium
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customBarbershopProfileSettingsEditorCollectionViewCell.self, forCellWithReuseIdentifier: "cellIDBPRO")
        return cv
    }()
    
    let buildVersionDescriptionPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjects()
        settingViewAlign()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    @objc func exitThisView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupViewObjects(){
        var topDistance: CGFloat = 20
        if UIDevice.current.hasNotch {
            topDistance = 60
        }
        
        upperInputsContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(BarbershopSettingsViewIcon)
        upperInputsContainerView.addSubview(editProfilePlaceHolder)
        upperInputsContainerView.addSubview(shopProfileCloseViewButton)
        
        BarbershopSettingsViewIcon.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        BarbershopSettingsViewIcon.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor).isActive = true
        BarbershopSettingsViewIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        BarbershopSettingsViewIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        shopProfileCloseViewButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        shopProfileCloseViewButton.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        shopProfileCloseViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        shopProfileCloseViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        editProfilePlaceHolder.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        editProfilePlaceHolder.leftAnchor.constraint(equalTo: BarbershopSettingsViewIcon.rightAnchor, constant: 10).isActive = true
        editProfilePlaceHolder.rightAnchor.constraint(equalTo: shopProfileCloseViewButton.leftAnchor).isActive = true
        editProfilePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        collectView.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 10).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        collectView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
    }
    
    
    
    @objc private func handleSwitchBackToCustomerSection(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let customerId = decodedCustomer.first?.customerId, let shopOwner = decodedCustomer.first?.shopOwner, let timeZone = decodedCustomer.first?.timezone, let authType = decodedCustomer.first?.authType, let email = decodedCustomer.first?.email, let tokenData = decodedCustomer.first?.token, let expiredTimeString = decodedCustomer.first?.expiresIn  {
                
                let teams = [CustomerToken(expiresIn: expiredTimeString, customerId: customerId, token: tokenData, timezone: timeZone, shopOwner: shopOwner, authType: authType, email: email, autoShopSwitch: "NO")]
                
                let userDefaults = UserDefaults.standard
                let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: teams)
                userDefaults.set(encodedData, forKey: "token")
                userDefaults.synchronize()
                print("switch process")
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                
                if (appDelegate.window?.rootViewController?.presentedViewController) != nil
                {
                    let mainView = appDelegate.window?.rootViewController?.children
                    let viewControllers = mainView
                    if let _ = viewControllers?.first(where: {$0.isKind(of: BarberShopsViewController.classForCoder())}) {
                        self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                        let someDict = ["shopUniqueId" : "refresh view please" ]
                        NotificationCenter.default.post(name: .didReceiveDataMapViewRefresher, object: nil, userInfo: someDict)
                    } else {
                        if let viewController = UIApplication.shared.keyWindow!.rootViewController as? MainNavigationController {
                            viewController.viewControllers.removeAll()
                            let customerController = BarberShopsViewController()
                            viewController.viewControllers = [customerController]
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                }
            }
        } else {
            print("could not get userdefault data")
        }
    }
    
    func settingViewAlign(){
        let valueone = Settings()
        valueone.settingsTitle = NSLocalizedString("barberProfileViewControllerShopProfileButton", comment: "Shop Profile")
        valueone.settingsIcon = UIImage(named: "icons8editProfile")
        self.settingButtons.append(valueone)
        
        let valuetwo = Settings()
        valuetwo.settingsTitle = NSLocalizedString("barberProfileViewControllerTimeScheduleButton", comment: "Time Schedule")
        valuetwo.settingsIcon = UIImage(named: "icons8timeschedule")
        self.settingButtons.append(valuetwo)
        
        let valuethree = Settings()
        valuethree.settingsTitle = NSLocalizedString("barberProfileViewControllerStaffAndServicesButton", comment: "Staff and Services")
        valuethree.settingsIcon = UIImage(named: "icons8managementShopServices")
        self.settingButtons.append(valuethree)
        
        let valueFour = Settings()
        valueFour.settingsTitle = NSLocalizedString("barberProfileViewControllerAppointmentsButton", comment: "Appointments")
        valueFour.settingsIcon = UIImage(named: "icons8appointmentHistory")
        self.settingButtons.append(valueFour)
        
        let valueFive = Settings()
        valueFive.settingsTitle = NSLocalizedString("barberProfileViewControllerHolidayAndBreaksButton", comment: "Holidays and Breaks")
        valueFive.settingsIcon = UIImage(named: "icons8holidayshopstaff")
        self.settingButtons.append(valueFive)
        
        let valueSix = Settings()
        valueSix.settingsTitle = NSLocalizedString("barberProfileViewControllerCreateShopButton", comment: "Create a shop")
        valueSix.settingsIcon = UIImage(named: "icon_shop")
        self.settingButtons.append(valueSix)
        
        let valueSeven = Settings()
        valueSeven.settingsTitle = NSLocalizedString("barberProfileViewControllerSwitchCustomerButton", comment: "Switch to customer")
        valueSeven.settingsIcon = UIImage(named: "icons8switchBackIcon")
        self.settingButtons.append(valueSeven)
        
        let valueEight = Settings()
        valueEight.settingsTitle = NSLocalizedString("barberProfileViewControllerShopBillingButton", comment: "Shop billings")
        valueEight.settingsIcon = UIImage(named: "icons8shopbill")
        self.settingButtons.append(valueEight)
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    @objc func handleCollectionSelction(sender: UIButton){
        if let shopId = self.selectedShopUniqueID, let curr = self.selectedShopCurrency {
            switch sender.tag {
            case 0:
                let baberprofileEdit = BarberProfileEditorViewController()
                baberprofileEdit.selectedShopUniqueID = shopId
                baberprofileEdit.modalPresentationStyle = .overCurrentContext
                self.present(baberprofileEdit, animated: true, completion: nil)
                break
            case 1:
                let openinghours = OpeningHoursViewController()
                openinghours.notRegisteringANewBarberShop = false
                openinghours.shopID = shopId
                openinghours.modalPresentationStyle = .overCurrentContext
                self.present(openinghours, animated: true, completion: nil)
                break
            case 2:
                let barberservice = BarberShopBarbersAndServicesViewController()
                barberservice.currentSelectedShop = shopId
                barberservice.modalPresentationStyle = .overCurrentContext
                self.present(barberservice, animated: true, completion: nil)
                break
            case 3:
                let boookingHistory = BookingHistoryBarberShopViewController()
                boookingHistory.selectedShopID = shopId
                boookingHistory.shopCurrencyCode = curr
                boookingHistory.modalPresentationStyle = .overCurrentContext
                self.present(boookingHistory, animated: true, completion: nil)
                break
            case 4:
                let boookingHistory = ShopStaffHolidayViewController()
                boookingHistory.selectedShopUniqueID = shopId
                boookingHistory.modalPresentationStyle = .overCurrentContext
                self.present(boookingHistory, animated: true, completion: nil)
                break
            case 5:
                let registerview = RegisterBarberViewController()
                registerview.modalPresentationStyle = .overCurrentContext
                self.present(registerview, animated: true, completion: nil)
                break
            case 6:
                handleSwitchBackToCustomerSection()
                break
            case 7:
                let shopBilling = ShopBillingViewController()
                shopBilling.shopUniqueID = shopId
                shopBilling.modalPresentationStyle = .overCurrentContext
                self.present(shopBilling, animated: true, completion: nil)
                break
            default:
                print("Sky High")
            }
        }
    }

}

class customBarbershopProfileSettingsEditorCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    lazy var thumbnailImageView: UIButton = {
        let tniv = UIButton()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.contentMode = .scaleAspectFit
        return tniv
    }()
    
    let settingButtonTitlePlaceHolder: UIButton = {
        let fnhp = UIButton()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 15)
        fnhp.setTitleColor(UIColor.black, for: .normal)
        fnhp.contentHorizontalAlignment = .left
        fnhp.isUserInteractionEnabled = true
        return fnhp
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    func setupViews(){
        addSubview(thumbnailImageView)
        addSubview(settingButtonTitlePlaceHolder)
        addSubview(seperatorView)
        
        addContraintsWithFormat(format: "H:|[v0(50)]-10-[v1]|", views: thumbnailImageView, settingButtonTitlePlaceHolder)
        addContraintsWithFormat(format: "V:|-5-[v0]-5-[v1(1)]|", views: settingButtonTitlePlaceHolder,seperatorView)
        addContraintsWithFormat(format: "V:|-5-[v0]-5-[v1(1)]|", views: thumbnailImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
