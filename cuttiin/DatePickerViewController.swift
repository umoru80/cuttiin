//
//  DatePickerViewController.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 19/04/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit
import SwiftDate

class DatePickerViewController: UIViewController {
    
    var bookingViewController = BookingsViewController()
    var bookingHistoryBarberShopViewController = BookingHistoryBarberShopViewController()
    var shopStaffHolidayViewController = CreateEditShopStaffHolidayViewController()
    var tagButton: Int?
    var chosenDate: Date?
    var viewWhoInitiatedAction: String?
    
    let appointmentDatePicker: UIDatePicker = {
        let opentime = UIDatePicker()
        opentime.translatesAutoresizingMaskIntoConstraints = false
        opentime.datePickerMode = .date
        let currentDate = Date()
        opentime.date = currentDate
        opentime.backgroundColor = UIColor.white
        opentime.layer.masksToBounds = true
        opentime.layer.cornerRadius = 5
        opentime.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        return opentime
    }()
    
    let selectButtonsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.isUserInteractionEnabled = true
        return tcview
    }() // NSLocalizedString("bookingViewControllerDateFilterFrom", comment: "From")
    
    lazy var continueButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        st.setTitle(NSLocalizedString("bookingViewControllerDateFilterContinueButton", comment: "Continue"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.addTarget(self, action: #selector(handleContinueAction(_:)), for: .touchUpInside)
        st.isEnabled = true
        return st
    }()
    
    lazy var cancelButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        st.setTitle(NSLocalizedString("bookingViewControllerDateFilterCancelButton", comment: "Cancel"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.addTarget(self, action: #selector(handleCancelAction(_:)), for: .touchUpInside)
        st.isEnabled = true
        return st
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        view.addSubview(appointmentDatePicker)
        view.bringSubviewToFront(appointmentDatePicker)
        view.addSubview(selectButtonsContainerView)
        view.bringSubviewToFront(selectButtonsContainerView)
        if let action = self.viewWhoInitiatedAction, action == "shopStaffView" {
            self.appointmentDatePicker.datePickerMode = .dateAndTime
            let currentDateTime = DateInRegion()
            self.appointmentDatePicker.date = currentDateTime.date
            self.appointmentDatePicker.minimumDate = currentDateTime.date
        }
        setupConstraints()
        dateChanged(appointmentDatePicker)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    @objc private func setupConstraints(){
        appointmentDatePicker.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        appointmentDatePicker.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        selectButtonsContainerView.topAnchor.constraint(equalTo: appointmentDatePicker.bottomAnchor, constant: 10).isActive = true
        selectButtonsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        selectButtonsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -10).isActive = true
        selectButtonsContainerView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        selectButtonsContainerView.addSubview(continueButton)
        selectButtonsContainerView.addSubview(cancelButton)
        
        continueButton.topAnchor.constraint(equalTo: selectButtonsContainerView.topAnchor).isActive = true
        continueButton.leftAnchor.constraint(equalTo: selectButtonsContainerView.leftAnchor).isActive = true
        continueButton.rightAnchor.constraint(equalTo: selectButtonsContainerView.centerXAnchor).isActive = true
        continueButton.heightAnchor.constraint(equalTo: selectButtonsContainerView.heightAnchor).isActive = true
        
        cancelButton.topAnchor.constraint(equalTo: selectButtonsContainerView.topAnchor).isActive = true
        cancelButton.rightAnchor.constraint(equalTo: selectButtonsContainerView.rightAnchor).isActive = true
        cancelButton.leftAnchor.constraint(equalTo: selectButtonsContainerView.centerXAnchor, constant: 20).isActive = true
        cancelButton.heightAnchor.constraint(equalTo: selectButtonsContainerView.heightAnchor).isActive = true
    }
    
    
    @objc private func dateChanged(_ datePicker: UIDatePicker){
        self.chosenDate = datePicker.date
        
    }
    
    @objc private func handleContinueAction(_ sender: UIButton){
        if let action = self.viewWhoInitiatedAction, let tagValue = self.tagButton, let dateSelected = self.chosenDate {
            switch action {
            case "bookingView":
                self.dismiss(animated: false) {
                    self.bookingViewController.dateChanged(dateSelected: dateSelected, buttonTag: tagValue)
                }
                break
            case "bookingViewHistory":
                self.dismiss(animated: false) {
                    //self.bookingHistoryBarberShopViewController.dateChanged(dateSelected: dateSelected, buttonTag: tagValue)
                    let someDict = ["dateSelected" : dateSelected, "buttonTag": tagValue ] as [String : Any]
                    NotificationCenter.default.post(name: .didReceiveDataAppointmentHistoryDate, object: nil, userInfo: someDict)
                }
                break
            case "shopStaffView":
                self.dismiss(animated: false) {
                    self.shopStaffHolidayViewController.dateChanged(dateSelected: dateSelected, buttonTag: tagValue)
                }
                break
            default:
                print("no action")
                break
            }
        }
    }
    
    @objc private func handleCancelAction(_ sender: UIButton){
        self.dismiss(animated: false, completion: nil)
    }
}
