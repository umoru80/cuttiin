//
//  ShopStaffHolidayViewController.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 11/05/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit
import SwiftDate
import Alamofire
import SwiftSpinner

class ShopStaffHolidayViewController: UIViewController {
    
    var selectedShopUniqueID: String?
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    var shopTimeZone: String?
    var shopCalendar: String?
    var shopLocale: String?
    var shopstaffholidayList = [HolidayShopStaffData]()
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    lazy var shopStaffHolidayShopPicker: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8shoppicker")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(shopStaffHolidayShopSelectorView)))
        return imageView
    }()
    
    lazy var shopStaffHolidayCloseViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(exitThisView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let shopStaffHolidayTitlePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("shopStaffHolidayViewControllerTitle", comment: "Holiday and Breaks")
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 17)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    
    lazy var saveAndUpdateDoneButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8addstaffandservice")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShoWCreateEditView)))
        return imageView
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    
    let curvedCollectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        cview.layer.masksToBounds = true
        cview.layer.cornerRadius = 5
        return cview
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.alwaysBounceVertical = true
        cv.register(customShopStaffHolidayCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        view.addSubview(shopStaffHolidayCloseViewButton)
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        view.addSubview(curvedCollectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewContraintsHoliday()
        getShopDetails()
        getShopHolidays()
        getStaffHolidays()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .didReceiveDataShopStaffHoliday, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @objc private func onDidReceiveData(_ notification:Notification) {
        
        if let dataBack = notification.userInfo as? [String: AnyObject], let shopId = dataBack["shopUniqueId"] as? String {
            self.selectedShopUniqueID = shopId
            self.getShopDetails()
            self.getShopHolidays()
            self.getStaffHolidays()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func exitThisView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @objc private func shopStaffHolidayShopSelectorView(){
        
        let customerShops = CustomerShopsViewController()
        customerShops.shopStaffHolidayViewController = self
        customerShops.viewControllerWhoInitiatedAction = "shopstaffholiday"
        customerShops.modalPresentationStyle = .overCurrentContext
        present(customerShops, animated: true, completion: nil)
        
    }
    
    @objc private func handleShoWCreateEditView(){
        print("add")
        if let id = self.selectedShopUniqueID, let spTimeZone = self.shopTimeZone, let spCalendar = self.shopCalendar, let spLocale = self.shopLocale {
            let createEditHoliday = CreateEditShopStaffHolidayViewController()
            createEditHoliday.selectedShopID = id
            createEditHoliday.initiatorAction = "addHoliday"
            createEditHoliday.shopTimeZone = spTimeZone
            createEditHoliday.shopCalendar = spCalendar
            createEditHoliday.shopLocale = spLocale
            createEditHoliday.modalPresentationStyle = .overCurrentContext
            self.present(createEditHoliday, animated: true, completion: nil)
        }
    }
    
    @objc private func getShopDetails(){
        DispatchQueue.global(qos: .background).async {
            
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let shopUniqueID = self.selectedShopUniqueID {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        AF.request(self.BACKEND_URL + "getsingleshop/" + shopUniqueID, method: .post, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    do {
                                        let shopData = try JSONDecoder().decode(Shop.self, from: jsonData)
                                        self.shopTimeZone = shopData.shopDateCreatedTimezone
                                        self.shopCalendar = shopData.shopDateCreatedCalendar
                                        self.shopLocale = shopData.shopDateCreatedLocale
                                        
                                    } catch _ {
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                self.view.makeToast(NSLocalizedString("shopStaffHolidayViewControllerErrorMessageShopNotFound", comment: "Shops not found"), duration: 2.0, position: .bottom)
                                            case 500:
                                                self.view.makeToast(NSLocalizedString("shopStaffHolidayViewControllerErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                            default:
                                                print("sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
    }
    
    @objc func getShopHolidays() {
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let shop = self.selectedShopUniqueID  {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    self.shopstaffholidayList.removeAll()
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                    
                    DispatchQueue.global(qos: .background).async {
                        AF.request(self.BACKEND_URL + "getAllShopHoliday/" + shop, method: .post, headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"])
                            .response { (response) in
                                
                                if let jsonData = response.data {
                                    do {
                                        
                                        let serverResponse = try JSONDecoder().decode([HolidayShop].self, from: jsonData)
                                        
                                        for xs in serverResponse {
                                            if(xs.holidayShopIsActive == "YES"){
                                                let regionDataShop = DateByUserDeviceInitializer.getRegion(TZoneName: xs.holidayShopDateTimezone, calenName: xs.holidayShopDateCalendar, LocName: xs.holidayShopDateLocale)
                                                if let startDateShop = xs.holidayShopStartDate.toDate("yyyy-MM-dd HH:mm:ss", region: regionDataShop), let endDateShop = xs.holidayShopEndDate.toDate("yyyy-MM-dd HH:mm:ss", region: regionDataShop){
                                                    let singleHolidayShopStaffHop = HolidayShopStaffData(id: xs.id, holidayStartDate: xs.holidayShopStartDate, holidayStartDateObject: startDateShop, holidayEndDate: xs.holidayShopEndDate, holidayEndDateObject: endDateShop, holidayDescription: xs.holidayShopDescription, holidayDateTimezone: xs.holidayShopDateTimezone, holidayDateCalendar: xs.holidayShopDateCalendar, holidayDateLocale: xs.holidayShopDateLocale, holidayDesignation: "shop", holidayShopId: xs.shop, holidayActive: xs.holidayShopIsActive, staffName: "shop", holidayTitle: xs.holidayShopTitle)
                                                    self.shopstaffholidayList.append(singleHolidayShopStaffHop)
                                                    DispatchQueue.main.async {
                                                        self.collectionView.reloadData()
                                                    }
                                                }
                                            }
                                        }
                                    } catch _ {
                                        DispatchQueue.main.async {
                                            if let statusCode = response.response?.statusCode {
                                                switch statusCode {
                                                case 404:
                                                    self.view.makeToast(NSLocalizedString("shopStaffHolidayViewControllerErrorMessageNoShopAvailable", comment: "No shop holidays available"), duration: 2.0, position: .bottom)
                                                case 500:
                                                    self.view.makeToast(NSLocalizedString("shopStaffHolidayViewControllerErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                                default:
                                                    print("sky high")
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                    }
                    
                    
                    
                }
            }
        }
    }
    
    @objc func getStaffHolidays() {
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let shop = self.selectedShopUniqueID  {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    self.shopstaffholidayList.removeAll()
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                    
                    DispatchQueue.global(qos: .background).async {
                        AF.request(self.BACKEND_URL + "getAllStaffHoliday/" + shop, method: .post, headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"])
                            .response { (response) in
                                
                                if let jsonData = response.data {
                                    do {
                                        
                                        let serverResponse = try JSONDecoder().decode(HolidayShopStaff.self, from: jsonData)
                                        let staffHoliday = serverResponse.holidayStaff
                                        let customerData = serverResponse.customer
                                        
                                        for x in staffHoliday {
                                            if let firstCustomer = customerData.first(where: {$0.id == x.customer}), x.holidayStaffIsActive == "YES" {
                                                let regionData = DateByUserDeviceInitializer.getRegion(TZoneName: x.holidayStaffDateTimezone, calenName: x.holidayStaffDateCalendar, LocName: x.holidayStaffDateLocale)
                                                if let startDate = x.holidayStaffStartDate.toDate("yyyy-MM-dd HH:mm:ss", region: regionData), let endDate = x.holidayStaffEndDate.toDate("yyyy-MM-dd HH:mm:ss", region: regionData){
                                                    
                                                    let singleHolidayShopStaff = HolidayShopStaffData(id: x.id, holidayStartDate: x.holidayStaffStartDate, holidayStartDateObject: startDate, holidayEndDate: x.holidayStaffEndDate, holidayEndDateObject: endDate, holidayDescription: x.holidayStaffDescription, holidayDateTimezone: x.holidayStaffDateTimezone, holidayDateCalendar: x.holidayStaffDateCalendar, holidayDateLocale: x.holidayStaffDateLocale, holidayDesignation: "staff", holidayShopId: x.shop, holidayActive: x.holidayStaffIsActive, staffName: firstCustomer.firstName, holidayTitle: x.holidayStaffTitle)
                                                    self.shopstaffholidayList.append(singleHolidayShopStaff)
                                                    DispatchQueue.main.async {
                                                        self.collectionView.reloadData()
                                                    }
                                                }
                                            }
                                            
                                        }
                                    } catch _ {
                                        DispatchQueue.main.async {
                                            if let statusCode = response.response?.statusCode {
                                                switch statusCode {
                                                case 404:
                                                    self.view.makeToast(NSLocalizedString("shopStaffHolidayViewControllerErrorMessageNoStaffAvailable", comment: "No staff holidays available"), duration: 2.0, position: .bottom)
                                                case 500:
                                                    self.view.makeToast(NSLocalizedString("shopStaffHolidayViewControllerErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                                default:
                                                    print("sky high")
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                    }
                    
                    
                    
                }
            }
        }
    }
    
    private func setupViewContraintsHoliday(){
        var topDistance: CGFloat = 20
        var bottomDistance: CGFloat = -10
        if UIDevice.current.hasNotch {
            topDistance = 60
            bottomDistance = -24
        }
        
        shopStaffHolidayCloseViewButton.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        shopStaffHolidayCloseViewButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -15).isActive = true
        shopStaffHolidayCloseViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        shopStaffHolidayCloseViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperInputsContainerView.topAnchor.constraint(equalTo: shopStaffHolidayCloseViewButton.bottomAnchor).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(shopStaffHolidayShopPicker)
        upperInputsContainerView.addSubview(shopStaffHolidayTitlePlaceHolder)
        upperInputsContainerView.addSubview(saveAndUpdateDoneButton)
        
        shopStaffHolidayShopPicker.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        shopStaffHolidayShopPicker.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor).isActive = true
        shopStaffHolidayShopPicker.widthAnchor.constraint(equalToConstant: 45).isActive = true
        shopStaffHolidayShopPicker.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        saveAndUpdateDoneButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        saveAndUpdateDoneButton.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        saveAndUpdateDoneButton.widthAnchor.constraint(equalToConstant: 35).isActive = true
        saveAndUpdateDoneButton.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        shopStaffHolidayTitlePlaceHolder.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        shopStaffHolidayTitlePlaceHolder.leftAnchor.constraint(equalTo: shopStaffHolidayShopPicker.rightAnchor, constant: 10).isActive = true
        shopStaffHolidayTitlePlaceHolder.rightAnchor.constraint(equalTo: saveAndUpdateDoneButton.leftAnchor).isActive = true
        shopStaffHolidayTitlePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        curvedCollectView.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 10).isActive = true
        curvedCollectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        curvedCollectView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        curvedCollectView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: bottomDistance).isActive = true
        
        curvedCollectView.addSubview(collectView)
        
        collectView.topAnchor.constraint(equalTo: curvedCollectView.topAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: curvedCollectView.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: curvedCollectView.widthAnchor, constant: -10).isActive = true
        collectView.heightAnchor.constraint(equalTo: curvedCollectView.heightAnchor, constant: -10).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
    }

}

class customShopStaffHolidayCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let pusherView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    lazy var holidayTitlePlaceHolderLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8titleicon")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let holidayTitlePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 13)
        fnhp.textColor = UIColor.black
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        fnhp.backgroundColor = UIColor.white
        return fnhp
    }()
    
    lazy var holidayDatePlaceHolderLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8timeSpanLogo")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let holidayDatePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 13)
        fnhp.textColor = UIColor.black
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        fnhp.backgroundColor = UIColor.white
        return fnhp
    }()
    
    lazy var holidayCreatedByPlaceHolderLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8initiatorlogo")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let holidayCreatedByPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 13)
        fnhp.textColor = UIColor.black
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        fnhp.backgroundColor = UIColor.white
        return fnhp
    }()
    
    lazy var holidayShortDescriptionPlaceHolderLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8servicedescription")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let holidayShortDescriptionPlaceHolder: UITextView = {
        let fnhp = UITextView()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 13)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        fnhp.isEditable = false
        fnhp.isScrollEnabled = false
        fnhp.isUserInteractionEnabled = false
        fnhp.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return fnhp
    }()
    
    func setupViews(){
        
        addSubview(holidayTitlePlaceHolderLogo)
        addSubview(holidayTitlePlaceHolder)
        addSubview(holidayDatePlaceHolderLogo)
        addSubview(holidayDatePlaceHolder)
        addSubview(holidayCreatedByPlaceHolderLogo)
        addSubview(holidayCreatedByPlaceHolder)
        addSubview(holidayShortDescriptionPlaceHolderLogo)
        addSubview(holidayShortDescriptionPlaceHolder)
        addSubview(pusherView)
        
        backgroundColor = UIColor.white
        layer.masksToBounds = true
        layer.cornerRadius = 5
        
        addContraintsWithFormat(format: "H:|-10-[v0(20)]-5-[v1]-5-|", views: holidayTitlePlaceHolderLogo, holidayTitlePlaceHolder)
        addContraintsWithFormat(format: "H:|-10-[v0(20)]-5-[v1]-5-|", views: holidayDatePlaceHolderLogo, holidayDatePlaceHolder)
        addContraintsWithFormat(format: "H:|-10-[v0(20)]-5-[v1]-5-|", views: holidayCreatedByPlaceHolderLogo, holidayCreatedByPlaceHolder)
        addContraintsWithFormat(format: "H:|-10-[v0(20)]-5-[v1]-5-|", views: holidayShortDescriptionPlaceHolderLogo, holidayShortDescriptionPlaceHolder)
        addContraintsWithFormat(format: "H:|-10-[v0(20)]-5-[v1]-5-|", views: pusherView, holidayShortDescriptionPlaceHolder)
        
        addContraintsWithFormat(format: "V:|-10-[v0(20)]-5-[v1(20)]-5-[v2(20)]-5-[v3(20)]-5-[v4]-5-|", views: holidayTitlePlaceHolderLogo, holidayDatePlaceHolderLogo, holidayCreatedByPlaceHolderLogo, holidayShortDescriptionPlaceHolderLogo, pusherView)
        addContraintsWithFormat(format: "V:|-10-[v0(20)]-5-[v1(20)]-5-[v2(20)]-5-[v3]-5-|", views: holidayTitlePlaceHolder, holidayDatePlaceHolder, holidayCreatedByPlaceHolder, holidayShortDescriptionPlaceHolder)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ShopStaffHolidayViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.shopstaffholidayList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customShopStaffHolidayCollectionViewCell
        cell.holidayTitlePlaceHolder.text = self.shopstaffholidayList[indexPath.row].holidayTitle
        cell.holidayCreatedByPlaceHolder.text = self.shopstaffholidayList[indexPath.row].staffName
        let start = self.shopstaffholidayList[indexPath.row].holidayStartDateObject.toString(DateToStringStyles.dateTime(.short))
        let end = self.shopstaffholidayList[indexPath.row].holidayEndDateObject.toString(DateToStringStyles.dateTime(.short))
        let finalDateString = start + " - " + end
        cell.holidayDatePlaceHolder.text = finalDateString
        cell.holidayShortDescriptionPlaceHolder.text = self.shopstaffholidayList[indexPath.row].holidayDescription
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if let cell = self.shopstaffholidayList[safe: indexPath.row] {
            let dataReel = cell.holidayDescription
            let approximateWidthOfDescription = view.frame.width - 124 // - 5 - 60 // - 5 - 5 - 40 - 5
            let size = CGSize(width: approximateWidthOfDescription, height: 1000)
            let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)]
            let estimatedFrame = NSString(string: dataReel).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
            print("lukaku")
            return CGSize(width: collectView.frame.width, height: estimatedFrame.height + 110) // 140
            
        }
        return CGSize(width: collectView.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let id = self.selectedShopUniqueID, let spTimeZone = self.shopTimeZone, let spCalendar = self.shopCalendar, let spLocale = self.shopLocale {
            let createEditHoliday = CreateEditShopStaffHolidayViewController()
            createEditHoliday.selectedShopID = id
            createEditHoliday.initiatorAction = "editHoliday"
            createEditHoliday.shopTimeZone = spTimeZone
            createEditHoliday.shopCalendar = spCalendar
            createEditHoliday.shopLocale = spLocale
            createEditHoliday.editDesignation = self.shopstaffholidayList[indexPath.row].holidayDesignation
            createEditHoliday.holidayIsActiveStatus = self.shopstaffholidayList[indexPath.row].holidayActive
            createEditHoliday.selectedHolidayId = self.shopstaffholidayList[indexPath.row].id
            createEditHoliday.modalPresentationStyle = .overCurrentContext
            self.present(createEditHoliday, animated: true, completion: nil)
        }
    }
    
    
}
