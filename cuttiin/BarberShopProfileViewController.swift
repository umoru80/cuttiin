//
//  BarberShopProfileViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/30/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import MGStarRatingView
import SwiftDate

class BarberShopProfileViewController: UIViewController, StarRatingDelegate {
    let starView = StarRatingView()
    let attribute = StarRatingAttribute(type: .rate,point: 20,spacing: 5, emptyColor: .black, fillColor: .white)
    var barberShopUUID: String?
    var navBar: UINavigationBar = UINavigationBar()
    var barberOne = BarberShop()
    var serviceOne = [Service]()
    var barberlistone = [Barber]()
    var serviceCategoryLadies = [Service]()
    var serviceCategoryGents = [Service]()
    var serviceCategoryKids = [Service]()
    var StringListOfBarbers = [String]()
    var selectedServiceOne = [String]()
    var selectedBarberOne = [String]()
    var ladiesSelectedList = [String]()
    var gentsSelectedList = [String]()
    var kidsSelectedList = [String]()
    var availableSelectedList = [String]()
    var topRatedSelectedList = [String]()
    var category = "Gents"
    var selectedLadiesGentsKidsButton = "Gents"
    var selectedAvailableTopRated = "Available"
    var selectedButtonBeforeSwitchingView = "Gents"
    var selectedButtonAfterSwitchingView = "Available"
    var openingTimeString: String?
    
    let barberShopHeaderDetailsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var barberShopLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.white //(r: 23, g: 69, b: 90)
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 25
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    lazy var barberShopNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var barberShopAddressPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var serviceBarberSegmentedControl: UISegmentedControl = {
        let ocsegmentcontrol = UISegmentedControl(items: [NSLocalizedString("barberSegmentButtonBarberShopProfile", comment: "BARBERS"), NSLocalizedString("servicesSegmentButtonBarberShopProfile", comment: "SERVICES")])
        ocsegmentcontrol.translatesAutoresizingMaskIntoConstraints = false
        ocsegmentcontrol.tintColor = UIColor.white
        ocsegmentcontrol.selectedSegmentIndex = 0
        ocsegmentcontrol.addTarget(self, action: #selector(handleServiceBarberChange), for: .valueChanged)
        return ocsegmentcontrol
    }()
    
    let categoryButtonContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var ladiesButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor.clear
        fbutton.setTitle("TOP RATED(0)", for: .normal)
        fbutton.setTitleColor(UIColor.white, for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 20)
        fbutton.addTarget(self, action: #selector(handleShowAvailableFirst), for: .touchUpInside)
        return fbutton
    }()
    
    let ladiesButtonSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor.white
        return fnsv
    }()
    
    lazy var gentsButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor.clear
        fbutton.setTitle(NSLocalizedString("gentsTabCategoryBarberShopProfile", comment: "GENTS(0)"), for: .normal)
        fbutton.setTitleColor(UIColor.white, for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 20)
        fbutton.addTarget(self, action: #selector(handleShowGentsList), for: .touchUpInside)
        fbutton.isHidden = true
        return fbutton
    }()
    
    let gentsButtonSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor.clear
        return fnsv
    }()
    
    lazy var kidsButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor.clear
        fbutton.setTitle("AVAILABLE(0)", for: .normal)
        fbutton.setTitleColor(UIColor.white, for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 20)
        fbutton.addTarget(self, action: #selector(handleShowTopRated), for: .touchUpInside)
        return fbutton
    }()
    
    let kidsButtonSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor.clear
        return fnsv
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.allowsMultipleSelection = true
        cv.register(customBarberShopProfileCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        cv.register(customBarberShopProfileCollectionViewBarberSelectedCell.self, forCellWithReuseIdentifier: "cellId2")
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(r: 11, g: 49, b: 68)  //UIColor(r: 23, g: 69, b: 90)
        setNavBarToTheView()
        starView.translatesAutoresizingMaskIntoConstraints = false
        starView.isUserInteractionEnabled = false
        starView.delegate = self
        view.addSubview(serviceBarberSegmentedControl)
        view.addSubview(categoryButtonContainerView)
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjectContraints()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        if let bookComplete = UserDefaults.standard.object(forKey: "theBookingProcessIsComplete") as? Bool{
            if bookComplete == true {
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    func setNavBarToTheView() {
        self.navBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 150)
        self.view.addSubview(navBar)
        let coverLayer = CALayer()
        coverLayer.frame = navBar.bounds;
        coverLayer.backgroundColor = UIColor(r: 23, g: 69, b: 90).cgColor
        navBar.layer.addSublayer(coverLayer)
        navBar.addSubview(barberShopHeaderDetailsContainerView)
        
        barberShopHeaderDetailsContainerView.topAnchor.constraint(equalTo: navBar.topAnchor, constant: 25).isActive = true
        barberShopHeaderDetailsContainerView.centerXAnchor.constraint(equalTo: navBar.centerXAnchor).isActive = true
        barberShopHeaderDetailsContainerView.widthAnchor.constraint(equalTo: navBar.widthAnchor, constant: -96).isActive = true
        barberShopHeaderDetailsContainerView.heightAnchor.constraint(equalTo: navBar.heightAnchor, constant: -35).isActive = true
        
        barberShopHeaderDetailsContainerView.addSubview(barberShopLogoImageView)
        barberShopHeaderDetailsContainerView.addSubview(barberShopNamePlaceHolder)
        barberShopHeaderDetailsContainerView.addSubview(barberShopAddressPlaceHolder)
        barberShopHeaderDetailsContainerView.addSubview(starView)
        
        
        barberShopLogoImageView.topAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.topAnchor).isActive = true
        barberShopLogoImageView.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopLogoImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        barberShopLogoImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        barberShopNamePlaceHolder.topAnchor.constraint(equalTo: barberShopLogoImageView.bottomAnchor, constant: 5).isActive = true
        barberShopNamePlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopNamePlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberShopNamePlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        barberShopAddressPlaceHolder.topAnchor.constraint(equalTo: barberShopNamePlaceHolder.bottomAnchor, constant: 5).isActive = true
        barberShopAddressPlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopAddressPlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberShopAddressPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        starView.topAnchor.constraint(equalTo: barberShopAddressPlaceHolder.bottomAnchor, constant: 5).isActive = true
        starView.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        starView.widthAnchor.constraint(equalToConstant: 120).isActive = true
        starView.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    var categoryButtonContainerViewHeightAnchor: NSLayoutConstraint?
    
    func setupViewObjectContraints(){
        serviceBarberSegmentedControl.topAnchor.constraint(equalTo: view.topAnchor, constant: 165).isActive = true
        serviceBarberSegmentedControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        serviceBarberSegmentedControl.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        serviceBarberSegmentedControl.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        categoryButtonContainerView.topAnchor.constraint(equalTo: serviceBarberSegmentedControl.bottomAnchor, constant: 10).isActive = true
        categoryButtonContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        categoryButtonContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        categoryButtonContainerViewHeightAnchor = categoryButtonContainerView.heightAnchor.constraint(equalToConstant: 0)
        categoryButtonContainerViewHeightAnchor?.isActive = true
        
        categoryButtonContainerView.addSubview(ladiesButton)
        categoryButtonContainerView.addSubview(ladiesButtonSeperatorView)
        categoryButtonContainerView.addSubview(gentsButton)
        categoryButtonContainerView.addSubview(gentsButtonSeperatorView)
        categoryButtonContainerView.addSubview(kidsButton)
        categoryButtonContainerView.addSubview(kidsButtonSeperatorView)
        
        ladiesButton.topAnchor.constraint(equalTo: categoryButtonContainerView.topAnchor).isActive = true
        ladiesButton.leftAnchor.constraint(equalTo: categoryButtonContainerView.leftAnchor).isActive = true
        ladiesButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        ladiesButton.heightAnchor.constraint(equalTo: categoryButtonContainerView.heightAnchor, constant: -2).isActive = true
        
        ladiesButtonSeperatorView.topAnchor.constraint(equalTo: ladiesButton.bottomAnchor).isActive = true
        ladiesButtonSeperatorView.leftAnchor.constraint(equalTo: categoryButtonContainerView.leftAnchor).isActive = true
        ladiesButtonSeperatorView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        ladiesButtonSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        kidsButton.topAnchor.constraint(equalTo: categoryButtonContainerView.topAnchor).isActive = true
        kidsButton.rightAnchor.constraint(equalTo: categoryButtonContainerView.rightAnchor).isActive = true
        kidsButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        kidsButton.heightAnchor.constraint(equalTo: categoryButtonContainerView.heightAnchor, constant: -2).isActive = true
        
        kidsButtonSeperatorView.topAnchor.constraint(equalTo: kidsButton.bottomAnchor).isActive = true
        kidsButtonSeperatorView.rightAnchor.constraint(equalTo: categoryButtonContainerView.rightAnchor).isActive = true
        kidsButtonSeperatorView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        kidsButtonSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        gentsButton.topAnchor.constraint(equalTo: categoryButtonContainerView.topAnchor).isActive = true
        gentsButton.widthAnchor.constraint(equalToConstant: 110).isActive = true
        gentsButton.leftAnchor.constraint(equalTo: ladiesButton.rightAnchor).isActive = true
        gentsButton.rightAnchor.constraint(equalTo: kidsButton.leftAnchor).isActive = true
        gentsButton.heightAnchor.constraint(equalTo: categoryButtonContainerView.heightAnchor, constant: -2).isActive = true
        
        gentsButtonSeperatorView.topAnchor.constraint(equalTo: gentsButton.bottomAnchor).isActive = true
        gentsButtonSeperatorView.widthAnchor.constraint(equalToConstant: 110).isActive = true
        gentsButtonSeperatorView.leftAnchor.constraint(equalTo: ladiesButtonSeperatorView.rightAnchor).isActive = true
        gentsButtonSeperatorView.rightAnchor.constraint(equalTo: kidsButtonSeperatorView.leftAnchor).isActive = true
        gentsButtonSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        self.categoryButtonContainerView.isHidden = true
        
        collectView.topAnchor.constraint(equalTo: categoryButtonContainerView.bottomAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -8).isActive = true
        collectView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
    }
    
    func StarRatingValueChanged(view: StarRatingView, value: CGFloat) {
        // use value
    }
    
    @objc func handleServiceBarberChange(){
        self.selectedButtonBeforeSwitchingView = self.selectedLadiesGentsKidsButton
        if self.serviceBarberSegmentedControl.selectedSegmentIndex == 1 {
            self.selectedBarberOne.removeAll()
            self.availableSelectedList.removeAll()
            self.topRatedSelectedList.removeAll()
            
            self.categoryButtonContainerView.isHidden = false
            self.categoryButtonContainerViewHeightAnchor?.isActive = false
            self.categoryButtonContainerViewHeightAnchor = categoryButtonContainerView.heightAnchor.constraint(equalToConstant: 40)
            self.categoryButtonContainerViewHeightAnchor?.isActive = true
            
            self.selectedLadiesGentsKidsButton = self.selectedButtonBeforeSwitchingView
            
            self.ladiesButton.isHidden = false
            self.ladiesButton.removeTarget(nil, action: nil, for: .allEvents)
            self.ladiesButton.setTitle(NSLocalizedString("ladiesCartegoryTabBarberShopProfileView", comment: "LADIES") + "(\(self.serviceCategoryLadies.count))", for: .normal)
            self.ladiesButton.addTarget(self, action: #selector(handleShowLadiesList), for: .touchUpInside)
            
            self.gentsButton.isHidden = false
            self.gentsButton.setTitle(NSLocalizedString("gentsCartegoryTabBarberShopProfileView", comment: "GENTS") + "(\(self.serviceCategoryGents.count))", for: .normal)
            
            self.kidsButton.isHidden = false
            self.kidsButton.removeTarget(nil, action: nil, for: .allEvents)
            self.kidsButton.setTitle(NSLocalizedString("kidsCartegoryTabBarberShopProfileView", comment: "KIDS") + "(\(self.serviceCategoryKids.count))", for: .normal)
            self.kidsButton.addTarget(self, action: #selector(handleShowKidsList), for: .touchUpInside)
            self.changeButtonCounter()
            
        }else {
            self.selectedServiceOne.removeAll()
            self.ladiesSelectedList.removeAll()
            self.gentsSelectedList.removeAll()
            self.kidsSelectedList.removeAll()
            
            self.categoryButtonContainerView.isHidden = true
            self.categoryButtonContainerViewHeightAnchor?.isActive = false
            self.categoryButtonContainerViewHeightAnchor = categoryButtonContainerView.heightAnchor.constraint(equalToConstant: 0)
            self.categoryButtonContainerViewHeightAnchor?.isActive = true
            
            self.ladiesButton.removeTarget(nil, action: nil, for: .allEvents)
            self.ladiesButton.setTitle("TOP RATED (\(self.barberlistone.count))", for: .normal)
            self.ladiesButton.addTarget(self, action: #selector(handleShowAvailableFirst), for: .touchUpInside)
            
            self.gentsButton.isHidden = true
            
            self.kidsButton.removeTarget(nil, action: nil, for: .allEvents)
            self.kidsButton.setTitle("AVAILABLE (\(self.barberlistone.count))", for: .normal)
            self.kidsButton.addTarget(self, action: #selector(handleShowTopRated), for: .touchUpInside)
            self.changeButtonCounterBarber()
            
        }
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    @objc func handleShowAvailableFirst(){
        self.selectedAvailableTopRated = "Available"
        self.ladiesButtonSeperatorView.backgroundColor = UIColor.white
        self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
        self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
        self.changeButtonCounterBarber()
    }
    
    @objc func handleShowTopRated(){
        self.selectedAvailableTopRated = "Top Rated"
        self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
        self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
        self.kidsButtonSeperatorView.backgroundColor = UIColor.white
        self.changeButtonCounterBarber()
    }
    
    @objc func handleShowLadiesList(){
        self.selectedLadiesGentsKidsButton = "Ladies"
        self.ladiesButtonSeperatorView.backgroundColor = UIColor.white
        self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
        self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
        self.serviceOne.removeAll()
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
        if self.serviceCategoryLadies.count > 0 {
            self.selectedServiceOne.removeAll()
            self.ladiesSelectedList.removeAll()
            self.gentsSelectedList.removeAll()
            self.kidsSelectedList.removeAll()
            for serv in self.serviceCategoryLadies {
                if serv.category == "Ladies" {
                    self.serviceOne.append(serv)
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }else {
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            }
        }
    }
    
    @objc func handleShowGentsList(){
        self.selectedLadiesGentsKidsButton = "Gents"
        self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
        self.gentsButtonSeperatorView.backgroundColor = UIColor.white
        self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
        self.serviceOne.removeAll()
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
        if self.serviceCategoryGents.count > 0 {
            self.selectedServiceOne.removeAll()
            self.ladiesSelectedList.removeAll()
            self.gentsSelectedList.removeAll()
            self.kidsSelectedList.removeAll()
            for serv in self.serviceCategoryGents {
                if serv.category == "Gents" {
                    self.serviceOne.append(serv)
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }else {
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            }
        }
        
    }
    
    @objc func handleShowKidsList(){
        self.selectedLadiesGentsKidsButton = "Kids"
        self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
        self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
        self.kidsButtonSeperatorView.backgroundColor = UIColor.white
        self.serviceOne.removeAll()
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
        if self.serviceCategoryKids.count > 0 {
            self.selectedServiceOne.removeAll()
            self.ladiesSelectedList.removeAll()
            self.gentsSelectedList.removeAll()
            self.kidsSelectedList.removeAll()
            for serv in self.serviceCategoryKids {
                if serv.category == "Kids" {
                    self.serviceOne.append(serv)
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }else {
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            }
        }
        
    }
    
    func changeButtonCounter(){
        switch self.selectedLadiesGentsKidsButton {
        case "Ladies":
            let buttonTitle = NSLocalizedString("ladiesCartegoryTabBarberShopProfileView", comment: "LADIES") + "(\(self.serviceCategoryLadies.count))"
            self.ladiesButton.setTitle(buttonTitle, for: .normal)
            self.selectedLadiesGentsKidsButton = "Ladies"
            self.ladiesButtonSeperatorView.backgroundColor = UIColor.white
            self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
            self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
            
        case "Gents":
            let buttonTitle = NSLocalizedString("gentsCartegoryTabBarberShopProfileView", comment: "GENTS") + "(\(self.serviceCategoryGents.count))"
            self.gentsButton.setTitle(buttonTitle, for: .normal)
            self.selectedLadiesGentsKidsButton = "Gents"
            self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
            self.gentsButtonSeperatorView.backgroundColor = UIColor.white
            self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
        case "Kids":
            let buttonTitle = NSLocalizedString("kidsCartegoryTabBarberShopProfileView", comment: "KIDS") + "(\(self.serviceCategoryKids.count))"
            self.kidsButton.setTitle(buttonTitle, for: .normal)
            self.selectedLadiesGentsKidsButton = "Kids"
            self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
            self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
            self.kidsButtonSeperatorView.backgroundColor = UIColor.white
        default:
            print("Sky high")
        }
    }
    
    func changeButtonCounterBarber(){
        switch self.selectedAvailableTopRated {
        case "Available":
            let buttonTitle = "TOP RATED (\(self.barberlistone.count))"
            self.ladiesButton.setTitle(buttonTitle, for: .normal)
            self.selectedAvailableTopRated = "Available"
            self.ladiesButtonSeperatorView.backgroundColor = UIColor.white
            self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
            self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
        case "Top Rated":
            let buttonTitle = "AVAILABLE (\(self.barberlistone.count))"
            self.kidsButton.setTitle(buttonTitle, for: .normal)
            self.selectedAvailableTopRated = "Top Rated"
            self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
            self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
            self.kidsButtonSeperatorView.backgroundColor = UIColor.white
        default:
            print("Sky high")
        }
    }
    
    func handleMoveToTheNextView(){
        if self.availableSelectedList.count > 0 || self.topRatedSelectedList.count > 0 {
            let profileService = AfterChoiceBarberShopProfileServiceViewController()
            profileService.barberShopUUID = self.barberShopUUID
            profileService.availableSelectedList = self.availableSelectedList
            profileService.topRatedSelectedList = self.topRatedSelectedList
            let navController = UINavigationController(rootViewController: profileService)
            present(navController, animated: true, completion: nil)
        } else if self.ladiesSelectedList.count > 0 || self.gentsSelectedList.count > 0 || self.kidsSelectedList.count > 0 {
            let profileBarber = AfterChoiceBarberShopProfileBarberViewController()
            profileBarber.barberShopUUID = self.barberShopUUID
            profileBarber.ladiesSelectedList = self.ladiesSelectedList
            profileBarber.gentsSelectedList = self.gentsSelectedList
            profileBarber.kidsSelectedList = self.kidsSelectedList
            let navController = UINavigationController(rootViewController: profileBarber)
            present(navController, animated: true, completion: nil)
        }
    }

}

class customBarberShopProfileCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    let barberShopCoverImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "babershop0")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.layer.borderColor = UIColor.clear.cgColor
        imageView.layer.borderWidth = 3
        return imageView
    }()
    
    let textBackgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        return view
    }()
    
    let barberShopServiceLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let barberShopServiceNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let barberShopServiceDescriptionPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let serviceTimeTakenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 11)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let servicePricePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 11)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let thumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.clear
        tniv.contentMode = .scaleAspectFit
        return tniv
    }()
    
    func setupViews(){
        addSubview(barberShopCoverImageView)
        addSubview(textBackgroundView)
        addSubview(seperatorView)
        addSubview(barberShopServiceLogoImageView)
        addSubview(barberShopServiceNamePlaceHolder)
        addSubview(barberShopServiceDescriptionPlaceHolder)
        addSubview(serviceTimeTakenPlaceHolder)
        addSubview(servicePricePlaceHolder)
        
        backgroundColor = UIColor.clear
        addContraintsWithFormat(format: "H:|[v0]|", views: barberShopCoverImageView)
        addContraintsWithFormat(format: "V:|[v0][v1(5)]|", views: barberShopCoverImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: textBackgroundView)
        addContraintsWithFormat(format: "V:|[v0]-5-|", views: textBackgroundView)
        addContraintsWithFormat(format: "H:|-30-[v0(100)]|", views: barberShopServiceLogoImageView)
        addContraintsWithFormat(format: "V:|[v0]-5-|", views: barberShopServiceLogoImageView)
        addContraintsWithFormat(format: "H:|-150-[v0]-30-|", views: barberShopServiceNamePlaceHolder)
        addContraintsWithFormat(format: "V:|-5-[v0(20)][v1]-5-[v2(20)]-10-|", views: barberShopServiceNamePlaceHolder,barberShopServiceDescriptionPlaceHolder, serviceTimeTakenPlaceHolder)
        addContraintsWithFormat(format: "V:|-5-[v0(20)][v1]-5-[v2(20)]-10-|", views: barberShopServiceNamePlaceHolder,barberShopServiceDescriptionPlaceHolder, servicePricePlaceHolder)
        addContraintsWithFormat(format: "H:|-150-[v0]-30-|", views: barberShopServiceDescriptionPlaceHolder)
        addContraintsWithFormat(format: "H:|-150-[v0(50)][v1(50)]-30-|", views: serviceTimeTakenPlaceHolder,servicePricePlaceHolder)
        //addContraintsWithFormat(format: "V:|-70-[v0(20)]-5-|", views: serviceTimeTakenPlaceHolder)
        //addContraintsWithFormat(format: "V:|-70-[v0(20)]-5-|", views: servicePricePlaceHolder)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class customBarberShopProfileCollectionViewBarberSelectedCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    let barberShopBarberCoverImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "babershop0")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.layer.borderColor = UIColor.clear.cgColor
        imageView.layer.borderWidth = 3
        return imageView
    }()
    
    let textBackgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        return view
    }()
    
    let barberShopBarberLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let barberShopBarberNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let barberAvailableFromPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.text = NSLocalizedString("availableFromBarberShopProfile", comment: "Available from")
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let barberTimeToBeAvailablePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let starView = StarRatingView()
    let attribute = StarRatingAttribute(type: .rate,point: 15,spacing: 5, emptyColor: .black, fillColor: .white)
    
    let thumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.clear
        tniv.contentMode = .scaleAspectFit
        return tniv
    }()
    
    func setupViews(){
        addSubview(barberShopBarberCoverImageView)
        addSubview(textBackgroundView)
        addSubview(seperatorView)
        addSubview(barberShopBarberLogoImageView)
        addSubview(barberShopBarberNamePlaceHolder)
        addSubview(barberAvailableFromPlaceHolder)
        addSubview(barberTimeToBeAvailablePlaceHolder)
        starView.configure(attribute, current: 3, max: 5)
        starView.translatesAutoresizingMaskIntoConstraints = false
        starView.isUserInteractionEnabled = false
        addSubview(starView)
        
        backgroundColor = UIColor.clear
        addContraintsWithFormat(format: "H:|[v0]|", views: barberShopBarberCoverImageView)
        addContraintsWithFormat(format: "V:|[v0][v1(5)]|", views: barberShopBarberCoverImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: textBackgroundView)
        addContraintsWithFormat(format: "V:|[v0]-5-|", views: textBackgroundView)
        addContraintsWithFormat(format: "H:|-30-[v0(100)]|", views: barberShopBarberLogoImageView)
        addContraintsWithFormat(format: "V:|[v0]-5-|", views: barberShopBarberLogoImageView)
        addContraintsWithFormat(format: "H:|-150-[v0]-30-|", views: barberShopBarberNamePlaceHolder)
        addContraintsWithFormat(format: "H:|-150-[v0]-30-|", views: barberAvailableFromPlaceHolder)
        addContraintsWithFormat(format: "H:|-150-[v0]-30-|", views: barberTimeToBeAvailablePlaceHolder)
        addContraintsWithFormat(format: "H:|-170-[v0]-5-|", views: starView)
        addContraintsWithFormat(format: "V:|-5-[v0(30)][v1(20)][v2(20)][v3(20)]-10-|", views: barberShopBarberNamePlaceHolder,barberAvailableFromPlaceHolder,barberTimeToBeAvailablePlaceHolder,starView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
