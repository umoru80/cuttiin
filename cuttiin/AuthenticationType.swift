//
//  AuthenticationType.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 14/02/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation

struct AuthenticationType: Codable {
    let message, authenticationType: String
}
