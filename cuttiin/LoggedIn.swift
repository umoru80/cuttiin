//
//  LoggedIn.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 27/12/2018.
//  Copyright © 2018 teckdk. All rights reserved.
//
import Foundation

struct LoggedIn: Codable {
    let expiresIn: String
    let message: String
    let customer: Customer
    let token: String
}
