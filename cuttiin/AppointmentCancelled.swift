//
//  AppointmentCancelled.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 17/04/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation

struct AppointmentCancelled: Codable {
    let v: Int
    let id, appointment, appointmentCancelledCalendar, appointmentCancelledCharges: String
    let appointmentCancelledDate, appointmentCancelledLocale, appointmentCancelledReason, appointmentCancelledStatus: String
    let appointmentCancelledTimezone, customer, initiator: String
    let appointmentShop, appointmentCurrency: String
    
    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case appointment, appointmentCancelledCalendar, appointmentCancelledCharges, appointmentCancelledDate, appointmentCancelledLocale, appointmentCancelledReason, appointmentCancelledStatus, appointmentCancelledTimezone, customer, initiator,appointmentShop, appointmentCurrency
    }
}
