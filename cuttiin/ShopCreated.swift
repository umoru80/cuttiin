//
//  ShopCreated.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 08/02/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation

struct ShopCreated: Codable {
    let message: String
    let shop: Shop
}

struct Shop: Codable {
    let v: Int
    let id, shopAddress, shopAddressLatitude, shopAddressLongitude: String
    let shopCanViewBookings, shopCountry, shopCountryCode, shopCountryCurrencyCode: String
    let shopCustomerGender, shopDateCreated, shopDateCreatedCalendar, shopDateCreatedLocale: String
    let shopDateCreatedTimezone, shopDescription, shopEmail, shopHiddenByAdmin: String
    let shopHiddenByCustomer, shopLogoImageName: String
    let shopLogoImageURL: String
    let shopMobileNumber, shopName, shopOwner, shopRating: String
    let shortUniqueID: String
    let workhour: String
    let shopCategory: String
    let shopVerificationStatus, shopVerificationDate, shopVerificationDateTimeZone, shopVerificationDateCalendar, shopVerificationDateLocale: String
    
    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case shopAddress, shopAddressLatitude, shopAddressLongitude, shopCanViewBookings, shopCountry, shopCountryCode, shopCountryCurrencyCode, shopCustomerGender, shopDateCreated, shopDateCreatedCalendar, shopDateCreatedLocale, shopDateCreatedTimezone, shopDescription, shopEmail, shopHiddenByAdmin, shopHiddenByCustomer, shopLogoImageName, shopVerificationStatus, shopVerificationDate, shopVerificationDateTimeZone, shopVerificationDateCalendar, shopVerificationDateLocale, workhour, shopCategory
        case shopLogoImageURL = "shopLogoImageUrl"
        case shopMobileNumber, shopName, shopOwner, shopRating
        case shortUniqueID = "shortUniqueId"
    }
}

