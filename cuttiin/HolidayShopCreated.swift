//
//  HolidayShopCreated.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 12/05/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation

struct HolidayShopCreated: Codable {
    let message: String
    let holidayShop: HolidayShop
}

struct HolidayShop: Codable {
    let v: Int
    let id, holidayShopDateCalendar, holidayShopDateCreated, holidayShopDateLocale: String
    let holidayShopDateTimezone, holidayShopDescription, holidayShopEndDate, holidayShopEndDateObj: String
    let holidayShopIsActive, holidayShopStartDate, holidayShopStartDateObj, shop, holidayShopTitle, holidayShopRepeatStatus, holidayShopDayOfTheWeek: String
    
    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case holidayShopDateCalendar, holidayShopDateCreated, holidayShopDateLocale, holidayShopDateTimezone, holidayShopDescription, holidayShopEndDate, holidayShopEndDateObj, holidayShopIsActive, holidayShopStartDate, holidayShopStartDateObj, shop, holidayShopTitle, holidayShopRepeatStatus, holidayShopDayOfTheWeek
    }
}


