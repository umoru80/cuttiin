//
//  CustomerShopsViewController.swift
//  cuttiin
//
//  Created by Joseph Umoru on 22/03/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit
import Alamofire
import SwiftDate
import SwiftyJSON
import SwiftSpinner

class CustomerShopsViewController: UIViewController {
    
    var viewControllerWhoInitiatedAction: String?
    var shopDataArrayHold = [ShopData]()
    //view callers
    var appointmentViewController = AppointmentsViewController()
    var barberProfileEditorViewController = BarberProfileEditorViewController()
    var barberShopProfileEditorView = BarberProfileEditorViewController()
    var openingHoursViewController = OpeningHoursViewController()
    var staffAndServiceViewController = BarberShopBarbersAndServicesViewController()
    var bookingHistoryBarberShopViewController = BookingHistoryBarberShopViewController()
    var shopStaffHolidayViewController = ShopStaffHolidayViewController()
    var shopBillingViewController = ShopBillingViewController()
    
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    lazy var closeViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor(r: 11, g: 49, b: 68))
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(exitThisView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.alwaysBounceVertical = true
        cv.backgroundColor = UIColor.clear
        cv.register(customerShopsCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        view.addSubview(closeViewButton)
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewContraints()
        getCustomerShops()
    }
    
    @objc func getCustomerShops(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let customerID = decodedCustomer.first?.customerId, let token = decodedCustomer.first?.token {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    SwiftSpinner.show("Loading...")
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    DispatchQueue.global(qos: .background).async {
                        AF.request(self.BACKEND_URL + "shops/" + customerID, method: .post, headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    do {
                                        let customerShopData = try JSONDecoder().decode([Shop].self, from: jsonData)
                                        
                                        let shopCount = customerShopData.count
                                        var shopCountCheck = 0
                                        for singleShop in customerShopData {
                                            shopCountCheck = shopCountCheck + 1
                                            
                                            let shop = ShopData(id: singleShop.id, shopAddress: singleShop.shopAddress, shopAddressLatitude: singleShop.shopAddressLatitude, shopAddressLongitude: singleShop.shopAddressLongitude, shopCanViewBookings: singleShop.shopCanViewBookings, shopCountry: singleShop.shopCountry, shopCountryCode: singleShop.shopCountryCode, shopCountryCurrencyCode: singleShop.shopCountryCurrencyCode, shopCustomerGender: singleShop.shopCustomerGender, shopDateCreated: singleShop.shopDateCreated, shopDateCreatedCalendar: singleShop.shopDateCreatedCalendar, shopDateCreatedLocale: singleShop.shopDateCreatedLocale, shopDateCreatedTimezone: singleShop.shopDateCreatedTimezone, shopDescription: singleShop.shopDescription, shopEmail: singleShop.shopEmail, shopHiddenByAdmin: singleShop.shopHiddenByAdmin, shopHiddenByCustomer: singleShop.shopHiddenByCustomer, shopLogoImageName: singleShop.shopLogoImageName, shopLogoImageURL: singleShop.shopLogoImageURL, shopMobileNumber: singleShop.shopMobileNumber, shopName: singleShop.shopName, shopOwner: singleShop.shopOwner, shopRating: singleShop.shopRating, shortUniqueID: singleShop.shortUniqueID)
                                            self.shopDataArrayHold.append(shop)
                                            DispatchQueue.main.async {
                                                self.collectionView.reloadData()
                                            }
                                            
                                            if(shopCountCheck == shopCount){
                                                
                                                DispatchQueue.main.async {
                                                    SwiftSpinner.hide()
                                                }
                                                self.appointmentViewController.shopDataArray = self.shopDataArrayHold
                                            }
                                            
                                        }
                                        
                                        
                                    } catch _ {
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        if let statusCode = response.response?.statusCode{
                                            switch (statusCode){
                                            case 404:
                                                print("Shop not found")
                                            case 500:
                                                print("An error occurred")
                                            default:
                                                print("Sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
        }
    }
    
    @objc private func handleShopSelection(_ sender: UIButton){
        if let shopId = shopDataArrayHold[safe: sender.tag]?.id, let custOwner = shopDataArrayHold[safe: sender.tag]?.shopOwner, let initiatorData = self.viewControllerWhoInitiatedAction, let shopCurrency = shopDataArrayHold[safe: sender.tag]?.shopCountryCurrencyCode, let shopName = shopDataArrayHold[safe: sender.tag]?.shopName {
            
            switch (initiatorData) {
            case "appointmentView":
                self.dismiss(animated: true) {
                    
                    self.appointmentViewController.chosenShopID = shopId
                    self.appointmentViewController.currentShopOwnerCustomerid = custOwner
                    self.appointmentViewController.getTodaysAppointment(firstShop: shopId)
                    self.appointmentViewController.appointmentViewTitlePlaceHolder.text = shopName
                }
                break
            case "barberShopProfileEdit":
                self.dismiss(animated: true) {
                    let someDict = ["shopUniqueId" : shopId ]
                    NotificationCenter.default.post(name: .didReceiveData, object: nil, userInfo: someDict)
                }
            case "openingHoursView":
                self.dismiss(animated: true) {
                    let someDict = ["shopUniqueId" : shopId ]
                    NotificationCenter.default.post(name: .didReceiveDataOpeningHours, object: nil, userInfo: someDict)
                }
                break
            case "staffAndServiceView":
                self.dismiss(animated: true) {
                    let someDict = ["shopUniqueId" : shopId ]
                    NotificationCenter.default.post(name: .didReceiveDataStaffAndServices, object: nil, userInfo: someDict)
                }
                break
            case "appointmentHistory":
                self.dismiss(animated: true) {
                    let someDict = ["shopUniqueId" : shopId, "shopCurrency": shopCurrency ]
                    NotificationCenter.default.post(name: .didReceiveDataAppointmentHistory, object: nil, userInfo: someDict)
                }
                break
            case "shopstaffholiday":
                self.dismiss(animated: true) {
                    let someDict = ["shopUniqueId" : shopId]
                    NotificationCenter.default.post(name: .didReceiveDataShopStaffHoliday, object: nil, userInfo: someDict)
                }
                break
            case "shopBilling":
                self.dismiss(animated: true) {
                    let someDict = ["shopUniqueId" : shopId]
                    NotificationCenter.default.post(name: .didReceiveDataShopBillingView, object: nil, userInfo: someDict)
                }
                break
            default:
                print("no initiator included")
                break
            }
            
            
        }
    }
    
    @objc func setupViewContraints(){
        
        var topDistance: CGFloat = 20
         if UIDevice.current.hasNotch {
         topDistance = 60
         }
        
        closeViewButton.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        closeViewButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -12).isActive = true
        closeViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        closeViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        collectView.topAnchor.constraint(equalTo: closeViewButton.bottomAnchor, constant: 10).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        collectView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
    }
    
    @objc private func exitThisView(){
        self.dismiss(animated: true, completion: nil)
    }

}



class customerShopsCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let shopNamePlaceHolder: UIButton = {
        let fnhp = UIButton()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.setTitleColor(UIColor.black, for: .normal)
        fnhp.contentHorizontalAlignment = .left
        fnhp.titleLabel?.font = UIFont(name: "OpenSans-Bold", size: 15)
        return fnhp
    }()
    
    let goToShopButton: UIButton = {
        let butt = UIButton()
        butt.setImage(UIImage(named: "icon_shop"), for: .normal)
        return butt
    }()
    
    func setupViews(){
        
        addSubview(shopNamePlaceHolder)
        addSubview(goToShopButton)
        
        backgroundColor = UIColor.white
        layer.cornerRadius = 5
        
        addContraintsWithFormat(format: "H:|-5-[v0(30)]-5-[v1]-5-|", views: goToShopButton, shopNamePlaceHolder)
        addContraintsWithFormat(format: "V:|-5-[v0(30)]-5-|", views: goToShopButton)
        addContraintsWithFormat(format: "V:|-5-[v0(30)]-5-|", views: shopNamePlaceHolder)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CustomerShopsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return shopDataArrayHold.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customerShopsCollectionViewCell
        cell.shopNamePlaceHolder.setTitle(shopDataArrayHold[indexPath.row].shopName, for: .normal)
        cell.shopNamePlaceHolder.tag = indexPath.row
        cell.goToShopButton.tag = indexPath.row
        cell.shopNamePlaceHolder.addTarget(self, action: #selector(handleShopSelection(_:)), for: .touchUpInside)
        cell.goToShopButton.addTarget(self, action: #selector(handleShopSelection(_:)), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}
