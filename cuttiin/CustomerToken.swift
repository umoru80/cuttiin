//
//  CustomerToken.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 10/12/2018.
//  Copyright © 2018 teckdk. All rights reserved.
//

import UIKit

class CustomerToken: NSObject, NSCoding {
    
    var expiresIn: String
    var customerId: String
    var token: String
    var timezone: String
    var shopOwner: String
    var authType: String
    var email: String
    var autoShopSwitch: String
    
    
    init(expiresIn: String, customerId: String, token: String, timezone: String, shopOwner: String, authType: String, email: String, autoShopSwitch: String) {
        self.expiresIn = expiresIn
        self.customerId = customerId
        self.token = token
        self.timezone = timezone
        self.shopOwner = shopOwner
        self.authType = authType
        self.email = email
        self.autoShopSwitch = autoShopSwitch
        
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let exp = aDecoder.decodeObject(forKey: "expiresIn") as! String
        let cus = aDecoder.decodeObject(forKey: "customerId") as! String
        let tkn = aDecoder.decodeObject(forKey: "token") as! String
        let tz = aDecoder.decodeObject(forKey: "timezone") as! String
        let sOwn = aDecoder.decodeObject(forKey: "shopOwner") as! String
        let auth = aDecoder.decodeObject(forKey: "authType") as! String
        let emailString = aDecoder.decodeObject(forKey: "email") as! String
        let autoShpSwitchString = aDecoder.decodeObject(forKey: "autoShopSwitch") as! String
        self.init(expiresIn: exp, customerId: cus, token: tkn, timezone: tz, shopOwner: sOwn, authType: auth, email: emailString, autoShopSwitch: autoShpSwitchString)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(expiresIn, forKey: "expiresIn")
        aCoder.encode(customerId, forKey: "customerId")
        aCoder.encode(token, forKey: "token")
        aCoder.encode(timezone, forKey: "timezone")
        aCoder.encode(shopOwner, forKey: "shopOwner")
        aCoder.encode(authType, forKey: "authType")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(autoShopSwitch, forKey: "autoShopSwitch")
    }
}
