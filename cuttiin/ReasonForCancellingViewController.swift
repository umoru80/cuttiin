//
//  ReasonForCancellingViewController.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 30/05/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit
import SwiftSpinner
import SwiftDate
import Alamofire

class ReasonForCancellingViewController: UIViewController, UITextViewDelegate {
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    var appointmentID: String?
    var shopTimeZone: String?
    var shopLocale: String?
    var shopCalender: String?
    var shopOwnerCustomerID: String?
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor.clear
        return tcview
    }()
    
    lazy var cancelAppointmentViewIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8cancelappointment")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var cancelAppointmentCloseViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismissView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let cancelAppointmentViewPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("reasonForCancellingAppointmentsViewTitle", comment: "Cancel appointment")
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 20)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var cancelAppointmentDoneButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitleColor(UIColor.black, for: .normal)
        st.setTitle(NSLocalizedString("reasonForCancellingAppointmentsViewDoneButton", comment: "Done"), for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        st.isEnabled = true
        st.isUserInteractionEnabled = true
        st.addTarget(self, action: #selector(handleDoneAction), for: .touchUpInside)
        return st
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    lazy var firstReasonForCancellingButton: UIButton = {
        let cbutton = UIButton()
        cbutton.translatesAutoresizingMaskIntoConstraints = false
        cbutton.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        cbutton.setTitle(NSLocalizedString("reasonForCancellingAppointmentsViewReasonOtherEngage", comment: "I had other engagements"), for: .normal)
        cbutton.setTitleColor(UIColor.black, for: .normal)
        cbutton.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 15)
        cbutton.layer.cornerRadius = 5
        cbutton.layer.masksToBounds = true
        cbutton.addTarget(self, action: #selector(firstReason(_:)), for: .touchUpInside)
        cbutton.tag = 1
        return cbutton
    }()
    
    lazy var secondReasonForCancellingButton: UIButton = {
        let cbutton = UIButton()
        cbutton.translatesAutoresizingMaskIntoConstraints = false
        cbutton.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        cbutton.setTitle(NSLocalizedString("reasonForCancellingAppointmentsViewReasonWrongShop", comment: "I chose the wrong shop"), for: .normal)
        cbutton.setTitleColor(UIColor.black, for: .normal)
        cbutton.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 15)
        cbutton.layer.cornerRadius = 5
        cbutton.layer.masksToBounds = true
        cbutton.addTarget(self, action: #selector(firstReason(_:)), for: .touchUpInside)
        cbutton.tag = 2
        return cbutton
    }()
    
    lazy var thirdReasonForCancellingButton: UIButton = {
        let cbutton = UIButton()
        cbutton.translatesAutoresizingMaskIntoConstraints = false
        cbutton.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        cbutton.setTitle(NSLocalizedString("reasonForCancellingAppointmentsViewReasonWrongStaff", comment: "I chose the wrong staff"), for: .normal)
        cbutton.setTitleColor(UIColor.black, for: .normal)
        cbutton.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 15)
        cbutton.layer.cornerRadius = 5
        cbutton.layer.masksToBounds = true
        cbutton.addTarget(self, action: #selector(firstReason(_:)), for: .touchUpInside)
        cbutton.tag = 3
        return cbutton
    }()
    
    lazy var fourthReasonForCancellingButton: UIButton = {
        let cbutton = UIButton()
        cbutton.translatesAutoresizingMaskIntoConstraints = false
        cbutton.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        cbutton.setTitle(NSLocalizedString("reasonForCancellingAppointmentsViewReasonWrongTime", comment: "I chose the wrong time"), for: .normal)
        cbutton.setTitleColor(UIColor.black, for: .normal)
        cbutton.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 15)
        cbutton.layer.cornerRadius = 5
        cbutton.layer.masksToBounds = true
        cbutton.addTarget(self, action: #selector(firstReason(_:)), for: .touchUpInside)
        cbutton.tag = 4
        return cbutton
    }()
    
    let otherReasonsTextField: UITextView = {
        let em = UITextView()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.lightGray
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.text = NSLocalizedString("reasonForCancellingAppointmentsViewReasonOtherReason", comment: "other reasons")
        em.textAlignment = .justified
        em.isEditable = true
        em.isSelectable = true
        em.layer.borderColor = UIColor.black.cgColor
        em.layer.borderWidth = 0.5
        return em
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        view.addSubview(cancelAppointmentCloseViewButton)
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        
        view.addSubview(firstReasonForCancellingButton)
        view.addSubview(secondReasonForCancellingButton)
        view.addSubview(thirdReasonForCancellingButton)
        view.addSubview(fourthReasonForCancellingButton)
        view.addSubview(otherReasonsTextField)
        otherReasonsTextField.delegate = self
        setupViewObjectConstriants()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if otherReasonsTextField.textColor == UIColor.lightGray {
            otherReasonsTextField.text = ""
            otherReasonsTextField.textColor = UIColor.black
        }
    }
    
    @objc private func firstReason(_ sender: UIButton){
        switch sender.tag {
        case 1:
            self.cancelCustomerAppointment(reason: "I had other engagements")
            break
        case 2:
            self.cancelCustomerAppointment(reason: "I chose the wrong shop")
            break
        case 3:
            self.cancelCustomerAppointment(reason: "I chose the wrong staff")
            break
        case 4:
            self.cancelCustomerAppointment(reason: "I chose the wrong time")
            break
        default:
            print("sky high")
            break
        }
        
    }
    
    @objc private func handleDoneAction(){
        if self.otherReasonsTextField.text == "" || self.otherReasonsTextField.text == "other reasons" {
            self.view.makeToast(NSLocalizedString("reasonForCancellingAppointmentsViewErrorMessageSelectReason", comment: "please type in a reason or select one of the four reasons for cancelling this appointment"), duration: 2.0, position: .bottom)
        } else {
            if let reason = self.otherReasonsTextField.text {
                self.self.cancelCustomerAppointment(reason: reason)
            } else {
                self.view.makeToast(NSLocalizedString("reasonForCancellingAppointmentsViewErrorMessageSelectReason", comment: "please type in a reason or select one of the four reasons for cancelling this appointment"), duration: 2.0, position: .bottom)
            }
        }
    }
    
    @objc private func handleDismissView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func cancelCustomerAppointment(reason: String){
        DispatchQueue.global(qos: .background).async {
            
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let appointid = self.appointmentID, let customerid = decodedCustomer.first?.customerId, let shopTim = self.shopTimeZone, let shopLoc = self.shopLocale, let shopCal = self.shopCalender, let custOwner = self.shopOwnerCustomerID {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        
                        DispatchQueue.main.async {
                            SwiftSpinner.show("Loading...")
                        }
                        
                        let shopRegion = DateByUserDeviceInitializer.getRegion(TZoneName: shopTim, calenName: shopCal, LocName: shopLoc)
                        
                        let startDate = DateInRegion().convertTo(region: shopRegion).toFormat("yyyy-MM-dd HH:mm:ss")
                        
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        let parameters: Parameters = [
                            "appointmentCancelledBy": customerid,
                            "appointmentCancelledDate": startDate,
                            "initiator": "customer",
                            "appointmentCancelledReason": reason
                        ]
                        
                        AF.request(self.BACKEND_URL + "cancelSingleAppointment/" + appointid, method: .post, parameters: parameters, encoding: JSONEncoding(options: []), headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    if let statusCode = response.response?.statusCode {
                                        
                                        do {
                                            
                                            let shopData = try JSONDecoder().decode(ServerMessage.self, from: jsonData)
                                            DispatchQueue.main.async {
                                                SwiftSpinner.hide()
                                                
                                                switch statusCode {
                                                case 200:
                                                    self.handleSendNotificationAfter(customerid: custOwner, message: "your appointment was cancelled by the customer", designation: "cancelappointment", appointmentID: appointid)
                                                    self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
                                                case 404:
                                                    self.view.makeToast(NSLocalizedString("reasonForCancellingAppointmentsViewErrorMessageAppointmentNotFound", comment: "Appointment not found"), duration: 2.0, position: .bottom)
                                                case 500:
                                                    self.view.makeToast(NSLocalizedString("reasonForCancellingAppointmentsViewErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                                default:
                                                    print("sky high")
                                                }
                                            }
                                            
                                        } catch _ {
                                            DispatchQueue.main.async {
                                                SwiftSpinner.hide()
                                                if let statusCode = response.response?.statusCode {
                                                    switch statusCode {
                                                    case 404:
                                                        self.view.makeToast(NSLocalizedString("reasonForCancellingAppointmentsViewErrorMessageAppointmentNotFound", comment: "Appointment not found"), duration: 2.0, position: .bottom)
                                                    case 500:
                                                        self.view.makeToast(NSLocalizedString("reasonForCancellingAppointmentsViewErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                                    default:
                                                        print("sky high")
                                                    }
                                                }
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
        }
    }
    
    
    @objc private func handleSendNotificationAfter(customerid: String, message: String, designation: String, appointmentID: String){
        
        DispatchQueue.global(qos: .background).async {
            
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        let parameters: Parameters = [
                            "customerid": customerid,
                            "designation": designation,
                            "appointmentID": appointmentID,
                            "message": message
                        ]
                        
                        print("message data check before sending: ", message)
                        
                        AF.request(self.BACKEND_URL + "onetoonepushnotification/" + customerid, method: .post, parameters: parameters, encoding: JSONEncoding(options: []), headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    if let statusCode = response.response?.statusCode {
                                        
                                        do {
                                            
                                            let _ = try JSONDecoder().decode(ServerMessage.self, from: jsonData)
                                            switch statusCode {
                                            case 200:
                                                print("notification sent")
                                            case 404:
                                                print("notification not sent")
                                            case 500:
                                                print("An error occurred, please try again later")
                                            default:
                                                print("sky high")
                                            }
                                            
                                        } catch _ {
                                            switch statusCode {
                                            case 404:
                                                print("notification not sent")
                                            case 500:
                                                print("An error occurred, please try again later")
                                            default:
                                                print("sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
        
    }
    
    func setupViewObjectConstriants(){
        
        var topDistance: CGFloat = 20
        if UIDevice.current.hasNotch {
            topDistance = 60
        }
        
        cancelAppointmentCloseViewButton.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        cancelAppointmentCloseViewButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -15).isActive = true
        cancelAppointmentCloseViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        cancelAppointmentCloseViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperInputsContainerView.topAnchor.constraint(equalTo: cancelAppointmentCloseViewButton.bottomAnchor, constant: 5).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(cancelAppointmentViewIcon)
        upperInputsContainerView.addSubview(cancelAppointmentViewPlaceHolder)
        upperInputsContainerView.addSubview(cancelAppointmentDoneButton)
        
        cancelAppointmentViewIcon.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        cancelAppointmentViewIcon.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor).isActive = true
        cancelAppointmentViewIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        cancelAppointmentViewIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        cancelAppointmentDoneButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        cancelAppointmentDoneButton.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        cancelAppointmentDoneButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        cancelAppointmentDoneButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        cancelAppointmentViewPlaceHolder.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        cancelAppointmentViewPlaceHolder.leftAnchor.constraint(equalTo: cancelAppointmentViewIcon.rightAnchor, constant: 10).isActive = true
        cancelAppointmentViewPlaceHolder.rightAnchor.constraint(equalTo: cancelAppointmentDoneButton.leftAnchor).isActive = true
        cancelAppointmentViewPlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        firstReasonForCancellingButton.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 10).isActive = true
        firstReasonForCancellingButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        firstReasonForCancellingButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        firstReasonForCancellingButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        secondReasonForCancellingButton.topAnchor.constraint(equalTo: firstReasonForCancellingButton.bottomAnchor, constant: 5).isActive = true
        secondReasonForCancellingButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        secondReasonForCancellingButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        secondReasonForCancellingButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        thirdReasonForCancellingButton.topAnchor.constraint(equalTo: secondReasonForCancellingButton.bottomAnchor, constant: 5).isActive = true
        thirdReasonForCancellingButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        thirdReasonForCancellingButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        thirdReasonForCancellingButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        fourthReasonForCancellingButton.topAnchor.constraint(equalTo: thirdReasonForCancellingButton.bottomAnchor, constant: 5).isActive = true
        fourthReasonForCancellingButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        fourthReasonForCancellingButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        fourthReasonForCancellingButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        otherReasonsTextField.topAnchor.constraint(equalTo: fourthReasonForCancellingButton.bottomAnchor, constant: 10).isActive = true
        otherReasonsTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        otherReasonsTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        otherReasonsTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }

}
