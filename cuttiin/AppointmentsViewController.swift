//
//  AppointmentsViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/26/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate
import FBSDKLoginKit
import SwiftSpinner
import Alamofire

class AppointmentsViewController: UIViewController, UITabBarControllerDelegate {
    var firstCall = true
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    var chosenShopID: String?
    
    var chosenShopTimeZoneID: String?
    var chosenShopCalendarID: String?
    var chosenShopLocaleID: String?
    
    var chosenShopCurrencyCode: String?
    
    var currentShopOwnerCustomerid: String?
    
    var shopDataArray = [ShopData]()
    var tempShopDataArray = [ShopData]()
    var shopWorkHourArray = [WorkhourData]()
    
    var shopOpeningDateForTheDay: String?
    
    var todaysAppointments = [AppointmentData]()
    var alwaynewAppointment = [AppointmentData]()
    var alwaysNewAppointmentForStaff = [AppointmentData]()
    
    var serviceAmountTodayArray = [AppointmentServiceAmountData]()
    
    //staff variable
    var staffID: String?
    var staffShopID: String?
    var staffCustomerID: String?
    //
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    lazy var shopSelectionButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icon_shop")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowingShops)))
        return imageView
    }()
    
    lazy var shopProfileViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8settingsIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowShopProfile)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let appointmentViewTitlePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 20)
        //fnhp.text = "Todays Appointments"
        fnhp.textColor = UIColor.black
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    let curvedCollectViewHolder: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        cview.layer.cornerRadius = 5
        return cview
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.alwaysBounceVertical = true
        cv.backgroundColor = UIColor.clear
        cv.register(customAppointmentViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()
    
    let refresherController: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        let title = NSLocalizedString("PullToRefreshBookingView", comment: "Pull to refresh")
        refreshControl.attributedTitle = NSAttributedString(string: title)
        refreshControl.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        refreshControl.addTarget(self, action: #selector(refreshOptions(sender:)), for: .valueChanged)
        return refreshControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        view.addSubview(curvedCollectViewHolder)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewContraints()
        firstCall = true
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_shop"), style: .done, target: self, action: #selector(handleShowingShops))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "icons8settingsIcon"), landscapeImagePhone: UIImage(named: "icons8settingsIcon"), style: .done, target: self, action: #selector(handleShowShopProfile))
        navigationItem.title = "Todays Appointments"
        
        switch UIDevice.current.screenType {
        case UIDevice.ScreenType.iPhones_4_4S:
            self.navigationController?.navigationBar.titleTextAttributes =
                [NSAttributedString.Key.foregroundColor: UIColor.black,
                 NSAttributedString.Key.font: UIFont(name: "HelveticaNeue", size: 13)!]
            break
        case UIDevice.ScreenType.iPhones_5_5s_5c_SE:
            self.navigationController?.navigationBar.titleTextAttributes =
                [NSAttributedString.Key.foregroundColor: UIColor.black,
                 NSAttributedString.Key.font: UIFont(name: "HelveticaNeue", size: 13)!]
            break
        default:
            self.navigationController?.navigationBar.titleTextAttributes =
                [NSAttributedString.Key.foregroundColor: UIColor.black,
                 NSAttributedString.Key.font: UIFont(name: "HelveticaNeue", size: 21)!]
        }
        
        if let firstShop = self.shopDataArray.first?.id, let custOwner = self.shopDataArray.first?.shopOwner, let curr = self.shopDataArray.first?.shopCountryCurrencyCode, let tmzone = self.shopDataArray.first?.shopDateCreatedTimezone, let calen = self.shopDataArray.first?.shopDateCreatedCalendar, let localell = self.shopDataArray.first?.shopDateCreatedLocale, let name = self.shopDataArray.first?.shopName {
            self.chosenShopID = firstShop
            self.chosenShopCurrencyCode = curr
            self.currentShopOwnerCustomerid = custOwner
            self.chosenShopTimeZoneID = tmzone
            self.chosenShopCalendarID = calen
            self.chosenShopLocaleID = localell
            self.appointmentViewTitlePlaceHolder.text = name
            
            
            self.getTodaysAppointment(firstShop: firstShop)
            
            
        } else {
            if let _ = self.staffCustomerID {
                //here
                self.checkIfCustomerIsAStaff()
            } else {
                self.getShopDataForAppointments()
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .didReceiveDataAppointmentViewRefresher, object: nil)
    }
    
    @objc private func onDidReceiveData(_ notification:Notification) {
        if let dataBack = notification.userInfo as? [String: AnyObject], let shopID = dataBack["shopUniqueId"] as? String {
            self.getTodaysAppointment(firstShop: shopID)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    @objc private func refreshOptions(sender: UIRefreshControl) {
        if let chSh = self.chosenShopID {
            self.getTodaysAppointment(firstShop: chSh)
        }
        sender.endRefreshing()
    }
    
    @objc private func handleShowingShops(){
        if let _ = self.staffShopID {
            self.dismiss(animated: true, completion: nil)
            handleSwitchBackToCustomerSection()
        } else {
            if (shopDataArray.count > 0){
                let customerShops = CustomerShopsViewController()
                customerShops.appointmentViewController = self
                customerShops.viewControllerWhoInitiatedAction = "appointmentView"
                customerShops.modalPresentationStyle = .overCurrentContext
                present(customerShops, animated: true, completion: nil)
            }
        }
    }
    
    @objc private func handleShowShopProfile(){
        //showing shop profile
        if let _ = self.staffShopID {
            self.dismiss(animated: true, completion: nil)
            handleSwitchBackToCustomerSection()
        } else {
            if let id = self.chosenShopID, let cur = self.chosenShopCurrencyCode {
                let shopProfile = BarberProfileViewController()
                shopProfile.selectedShopUniqueID = id
                shopProfile.selectedShopCurrency = cur
                shopProfile.modalPresentationStyle = .overCurrentContext
                self.present(shopProfile, animated: true, completion: nil)
            }
        }
        
    }
    
    @objc private func handleSwitchBackToCustomerSection(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let customerId = decodedCustomer.first?.customerId, let shopOwner = decodedCustomer.first?.shopOwner, let timeZone = decodedCustomer.first?.timezone, let authType = decodedCustomer.first?.authType, let email = decodedCustomer.first?.email, let tokenData = decodedCustomer.first?.token, let expiredTimeString = decodedCustomer.first?.expiresIn  {
                
                let teams = [CustomerToken(expiresIn: expiredTimeString, customerId: customerId, token: tokenData, timezone: timeZone, shopOwner: shopOwner, authType: authType, email: email, autoShopSwitch: "NO")]
                
                let userDefaults = UserDefaults.standard
                let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: teams)
                userDefaults.set(encodedData, forKey: "token")
                userDefaults.synchronize()
                print("switch process")
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                
                if (appDelegate.window?.rootViewController?.presentedViewController) != nil
                {
                    let mainView = appDelegate.window?.rootViewController?.children
                    let viewControllers = mainView
                    if let _ = viewControllers?.first(where: {$0.isKind(of: BarberShopsViewController.classForCoder())}) {
                        self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                        let someDict = ["shopUniqueId" : "refresh view please" ]
                        NotificationCenter.default.post(name: .didReceiveDataMapViewRefresher, object: nil, userInfo: someDict)
                    } else {
                        print("customer section does not exist")
                        if let viewController = UIApplication.shared.keyWindow!.rootViewController as? MainNavigationController {
                            viewController.viewControllers.removeAll()
                            let customerController = BarberShopsViewController()
                            viewController.viewControllers = [customerController]
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                } else {
                    print("staff presented appointment view")
                    if let viewController = UIApplication.shared.keyWindow!.rootViewController as? MainNavigationController {
                        viewController.viewControllers.removeAll()
                        let customerController = BarberShopsViewController()
                        viewController.viewControllers = [customerController]
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        } else {
            print("could not get userdefault data")
        }
    }
    
    @objc func getTodaysAppointment(firstShop: String){
        print("function called")
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let shopTimeZone = self.chosenShopTimeZoneID, let shopLocale = self.chosenShopLocaleID, let shopCalendar = self.chosenShopCalendarID {
                print("optionals passed")
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    let regionData = DateByUserDeviceInitializer.getRegion(TZoneName: shopTimeZone, calenName: shopCalendar, LocName: shopLocale)
                    let newDate = DateInRegion().convertTo(region: regionData).dateAtStartOf(.day) + 1.hours
                    let newDateString = newDate.toFormat("yyyy-MM-dd HH:mm:ss")
                    
                    let parameters = ["currentDate": newDateString,
                                      "currentTimezone": shopTimeZone] as [String : Any]
                    
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    DispatchQueue.global(qos: .background).async {
                        AF.request(self.BACKEND_URL + "getTodaysAppointments/" + firstShop, method: .post, parameters: parameters, headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    do {
                                        let customerShopData = try JSONDecoder().decode(AppointmentStaffCustomer.self, from: jsonData)
                                        let staffCustomerList = customerShopData.staffcustomer
                                        let appointmentCustomerList = customerShopData.customer
                                        let appointmentList = customerShopData.appointment
                                        self.alwaynewAppointment.removeAll()
                                        self.alwaysNewAppointmentForStaff.removeAll()
                                        let customMainServerArraySize = customerShopData.appointment.count
                                        
                                        if (appointmentList.isEmpty){
                                            DispatchQueue.main.async {
                                                self.todaysAppointments.removeAll()
                                                self.collectionView.reloadData()
                                            }
                                        }
                                        
                                        for appoint in appointmentList {
                                            let appointmentRegion = DateByUserDeviceInitializer.getRegion(TZoneName: appoint.appointmentDateTimezone, calenName: appoint.appointmentDateCalendar, LocName: appoint.appointmentDateLocale)
                                            guard let startDate = appoint.appointmentDateStart.toDate("yyyy-MM-dd HH:mm:ss", region: appointmentRegion),
                                                let endDate = appoint.appointmentDateEnd.toDate("yyyy-MM-dd HH:mm:ss", region: appointmentRegion) else {
                                                    return
                                            }
                                            
                                            let startDateString = startDate.toString(DateToStringStyles.time(.short))
                                            let endDateString = endDate.toString(DateToStringStyles.time(.short))
                                            
                                            let completDateString = startDateString + " - " + endDateString
                                            
                                            
                                            guard let firstCustomer = appointmentCustomerList.first(where: {$0.id == appoint.appointmentCustomer}) else {
                                                return
                                            }
                                            guard let firstStaff = staffCustomerList.first(where: {$0.id == appoint.appointmentStaffCustomer}) else {
                                                return
                                            }
                                            let appointmentClientImageName = firstCustomer.profileImageName
                                            
                                            AF.request(self.BACKEND_URL + "images/customerImages/" + appointmentClientImageName, headers: headers).responseData { response in
                                                
                                                let customerFullName = firstCustomer.firstName + " " + firstCustomer.lastName
                                                switch response.result {
                                                case .success(let data):
                                                    if let imageData = UIImage(data: data) {
                                                        let staffName = firstStaff.firstName + " " + firstStaff.lastName
                                                        
                                                        
                                                        for dataAmount in appoint.appointmentServiceWithAmount {
                                                            let singleServiceAmount = AppointmentServiceAmountData(id: dataAmount.id, appointmentService: dataAmount.appointmentService, appointmentServiceAmount: dataAmount.appointmentServiceAmount)
                                                            self.serviceAmountTodayArray.append(singleServiceAmount)
                                                            if (appoint.appointmentServiceWithAmount.count == self.serviceAmountTodayArray.count && self.serviceAmountTodayArray.count > 0){
                                                                
                                                                let singleAppointment = AppointmentData(id: appoint.id, appointmentService: appoint.appointmentService, appointmentShortUniqueID: appoint.appointmentShortUniqueID, appointmentDateStart: appoint.appointmentDateStart, appointmentDateEnd: appoint.appointmentDateEnd, appointmentDateTimezone: appoint.appointmentDateTimezone, appointmentDateCalendar: appoint.appointmentDateCalendar, appointmentDateLocale: appoint.appointmentDateLocale, appointmentDateCreated: appoint.appointmentDateCreated, appointmentDescription: appoint.appointmentDescription, appointmentPrice: appoint.appointmentPrice, appointmentCurrency: appoint.appointmentCurrency, appointmentCustomer: appoint.appointmentCustomer, appointmentStaff: appoint.appointmentStaff, appointmentStaffCustomer: appoint.appointmentStaffCustomer, appointmentShop: appoint.appointmentShop, appointmentServiceQuantity: appoint.appointmentServiceQuantity, appointmentStartedBy: appoint.appointmentStartedBy, appointmentEndedBy: appoint.appointmentEndedBy, appointmentStartedAt: appoint.appointmentStartedAt, appointmentEndedAt: appoint.appointmentEndedAt, appointmentStartedEndedTimezone: appoint.appointmentStartedEndedTimezone, appointmentStartedEndedCalendar: appoint.appointmentStartedEndedCalendar, appointmentStartedEndedLocale: appoint.appointmentStartedEndedLocale, appointmentPaymentMethod: appoint.appointmentPaymentMethod, appointmentStatus: appoint.appointmentStatus, appointmentPaidDate: appoint.appointmentPaidDate, appointmentPaidTimezone: appoint.appointmentPaidTimezone, appointmentPaidCalendar: appoint.appointmentPaidCalendar, appointmentPaidLocale: appoint.appointmentPaidLocale, v: appoint.v, appointmentCancelledBy: appoint.appointmentCancelledBy, appontmentCustomerImage: imageData, appointmentCustomerFirstName: customerFullName, appointmentStaffFirstName: staffName, appointmentFormatedStartEndTime: completDateString, appointmentDateStartObj: startDate, appointmentDateEndObj: endDate, appointmentServiceWithAmount: self.serviceAmountTodayArray)
                                                                self.alwaysNewAppointmentForStaff.append(singleAppointment)
                                                                
                                                                
                                                                if let staffCust = self.staffCustomerID {
                                                                    if staffCust == appoint.appointmentStaffCustomer {
                                                                        self.handleAddingandUpdatingAppointmentArray(newAppointment: singleAppointment, arraySize: appointmentList.count )
                                                                        self.serviceAmountTodayArray.removeAll()
                                                                    } else {
                                                                        self.serviceAmountTodayArray.removeAll()
                                                                    }
                                                                    
                                                                    if (self.alwaysNewAppointmentForStaff.count == customMainServerArraySize){
                                                                        for data in self.todaysAppointments {
                                                                            
                                                                            if (!self.alwaysNewAppointmentForStaff.contains(where: {$0.id == data.id })){
                                                                                if let firstIndex = self.todaysAppointments.firstIndex(where: {$0.id == data.id}){
                                                                                    self.todaysAppointments.remove(at: firstIndex)
                                                                                    DispatchQueue.main.async {
                                                                                        self.collectionView.reloadData()
                                                                                    }
                                                                                }
                                                                            }
                                                                            
                                                                            
                                                                        }
                                                                    }
                                                                    
                                                                } else {
                                                                    print(appoint.appointmentStaffCustomer)
                                                                    
                                                                    self.handleAddingandUpdatingAppointmentArray(newAppointment: singleAppointment, arraySize: appointmentList.count )
                                                                    self.serviceAmountTodayArray.removeAll()
                                                                    
                                                                }
                                                                
                                                            }
                                                        }
                                                    } else {
                                                        if let blankImage = UIImage(named: "avatar_icon"){
                                                            
                                                            let staffName = firstStaff.firstName + " " + firstStaff.lastName
                                                            
                                                            
                                                            for dataAmount in appoint.appointmentServiceWithAmount {
                                                                let singleServiceAmount = AppointmentServiceAmountData(id: dataAmount.id, appointmentService: dataAmount.appointmentService, appointmentServiceAmount: dataAmount.appointmentServiceAmount)
                                                                self.serviceAmountTodayArray.append(singleServiceAmount)
                                                                if (appoint.appointmentServiceWithAmount.count == self.serviceAmountTodayArray.count && self.serviceAmountTodayArray.count > 0){
                                                                    let singleAppointment = AppointmentData(id: appoint.id, appointmentService: appoint.appointmentService, appointmentShortUniqueID: appoint.appointmentShortUniqueID, appointmentDateStart: appoint.appointmentDateStart, appointmentDateEnd: appoint.appointmentDateEnd, appointmentDateTimezone: appoint.appointmentDateTimezone, appointmentDateCalendar: appoint.appointmentDateCalendar, appointmentDateLocale: appoint.appointmentDateLocale, appointmentDateCreated: appoint.appointmentDateCreated, appointmentDescription: appoint.appointmentDescription, appointmentPrice: appoint.appointmentPrice, appointmentCurrency: appoint.appointmentCurrency, appointmentCustomer: appoint.appointmentCustomer, appointmentStaff: appoint.appointmentStaff, appointmentStaffCustomer: appoint.appointmentStaffCustomer, appointmentShop: appoint.appointmentShop, appointmentServiceQuantity: appoint.appointmentServiceQuantity, appointmentStartedBy: appoint.appointmentStartedBy, appointmentEndedBy: appoint.appointmentEndedBy, appointmentStartedAt: appoint.appointmentStartedAt, appointmentEndedAt: appoint.appointmentEndedAt, appointmentStartedEndedTimezone: appoint.appointmentStartedEndedTimezone, appointmentStartedEndedCalendar: appoint.appointmentStartedEndedCalendar, appointmentStartedEndedLocale: appoint.appointmentStartedEndedLocale, appointmentPaymentMethod: appoint.appointmentPaymentMethod, appointmentStatus: appoint.appointmentStatus, appointmentPaidDate: appoint.appointmentPaidDate, appointmentPaidTimezone: appoint.appointmentPaidTimezone, appointmentPaidCalendar: appoint.appointmentPaidCalendar, appointmentPaidLocale: appoint.appointmentPaidLocale, v: appoint.v, appointmentCancelledBy: appoint.appointmentCancelledBy, appontmentCustomerImage: blankImage, appointmentCustomerFirstName: customerFullName, appointmentStaffFirstName: staffName, appointmentFormatedStartEndTime: completDateString, appointmentDateStartObj: startDate, appointmentDateEndObj: endDate, appointmentServiceWithAmount: self.serviceAmountTodayArray)
                                                                    if let staffCust = self.staffCustomerID {
                                                                        if staffCust == appoint.appointmentStaffCustomer {
                                                                            self.handleAddingandUpdatingAppointmentArray(newAppointment: singleAppointment, arraySize: appointmentList.count )
                                                                            self.serviceAmountTodayArray.removeAll()
                                                                        } else {
                                                                            self.serviceAmountTodayArray.removeAll()
                                                                        }
                                                                        
                                                                        if (self.alwaysNewAppointmentForStaff.count == customMainServerArraySize){
                                                                            for data in self.todaysAppointments {
                                                                                
                                                                                if (!self.alwaysNewAppointmentForStaff.contains(where: {$0.id == data.id })){
                                                                                    if let firstIndex = self.todaysAppointments.firstIndex(where: {$0.id == data.id}){
                                                                                        self.todaysAppointments.remove(at: firstIndex)
                                                                                        DispatchQueue.main.async {
                                                                                            self.collectionView.reloadData()
                                                                                        }
                                                                                    }
                                                                                }
                                                                                
                                                                                
                                                                            }
                                                                        }
                                                                        
                                                                    } else {
                                                                        self.handleAddingandUpdatingAppointmentArray(newAppointment: singleAppointment, arraySize: appointmentList.count )
                                                                        self.serviceAmountTodayArray.removeAll()
                                                                        
                                                                    }
                                                                    
                                                                }
                                                            }
                                                            
                                                        }
                                                    }
                                                    break
                                                case .failure(let error):
                                                    print(error)
                                                    self.view.makeToast("Image not found", duration: 3.0, position: .bottom)
                                                    if let blankImage = UIImage(named: "avatar_icon"){
                                                        
                                                        let staffName = firstStaff.firstName + " " + firstStaff.lastName
                                                        
                                                        
                                                        for dataAmount in appoint.appointmentServiceWithAmount {
                                                            let singleServiceAmount = AppointmentServiceAmountData(id: dataAmount.id, appointmentService: dataAmount.appointmentService, appointmentServiceAmount: dataAmount.appointmentServiceAmount)
                                                            self.serviceAmountTodayArray.append(singleServiceAmount)
                                                            if (appoint.appointmentServiceWithAmount.count == self.serviceAmountTodayArray.count && self.serviceAmountTodayArray.count > 0){
                                                                let singleAppointment = AppointmentData(id: appoint.id, appointmentService: appoint.appointmentService, appointmentShortUniqueID: appoint.appointmentShortUniqueID, appointmentDateStart: appoint.appointmentDateStart, appointmentDateEnd: appoint.appointmentDateEnd, appointmentDateTimezone: appoint.appointmentDateTimezone, appointmentDateCalendar: appoint.appointmentDateCalendar, appointmentDateLocale: appoint.appointmentDateLocale, appointmentDateCreated: appoint.appointmentDateCreated, appointmentDescription: appoint.appointmentDescription, appointmentPrice: appoint.appointmentPrice, appointmentCurrency: appoint.appointmentCurrency, appointmentCustomer: appoint.appointmentCustomer, appointmentStaff: appoint.appointmentStaff, appointmentStaffCustomer: appoint.appointmentStaffCustomer, appointmentShop: appoint.appointmentShop, appointmentServiceQuantity: appoint.appointmentServiceQuantity, appointmentStartedBy: appoint.appointmentStartedBy, appointmentEndedBy: appoint.appointmentEndedBy, appointmentStartedAt: appoint.appointmentStartedAt, appointmentEndedAt: appoint.appointmentEndedAt, appointmentStartedEndedTimezone: appoint.appointmentStartedEndedTimezone, appointmentStartedEndedCalendar: appoint.appointmentStartedEndedCalendar, appointmentStartedEndedLocale: appoint.appointmentStartedEndedLocale, appointmentPaymentMethod: appoint.appointmentPaymentMethod, appointmentStatus: appoint.appointmentStatus, appointmentPaidDate: appoint.appointmentPaidDate, appointmentPaidTimezone: appoint.appointmentPaidTimezone, appointmentPaidCalendar: appoint.appointmentPaidCalendar, appointmentPaidLocale: appoint.appointmentPaidLocale, v: appoint.v, appointmentCancelledBy: appoint.appointmentCancelledBy, appontmentCustomerImage: blankImage, appointmentCustomerFirstName: customerFullName, appointmentStaffFirstName: staffName, appointmentFormatedStartEndTime: completDateString, appointmentDateStartObj: startDate, appointmentDateEndObj: endDate, appointmentServiceWithAmount: self.serviceAmountTodayArray)
                                                                if let staffCust = self.staffCustomerID {
                                                                    if staffCust == appoint.appointmentStaffCustomer {
                                                                        self.handleAddingandUpdatingAppointmentArray(newAppointment: singleAppointment, arraySize: appointmentList.count )
                                                                        self.serviceAmountTodayArray.removeAll()
                                                                    } else {
                                                                        self.serviceAmountTodayArray.removeAll()
                                                                    }
                                                                    
                                                                    if (self.alwaysNewAppointmentForStaff.count == customMainServerArraySize){
                                                                        for data in self.todaysAppointments {
                                                                            
                                                                            if (!self.alwaysNewAppointmentForStaff.contains(where: {$0.id == data.id })){
                                                                                if let firstIndex = self.todaysAppointments.firstIndex(where: {$0.id == data.id}){
                                                                                    self.todaysAppointments.remove(at: firstIndex)
                                                                                    DispatchQueue.main.async {
                                                                                        self.collectionView.reloadData()
                                                                                    }
                                                                                }
                                                                            }
                                                                            
                                                                            
                                                                        }
                                                                    }
                                                                    
                                                                } else {
                                                                    self.handleAddingandUpdatingAppointmentArray(newAppointment: singleAppointment, arraySize: appointmentList.count )
                                                                    self.serviceAmountTodayArray.removeAll()
                                                                }
                                                                
                                                            }
                                                        }
                                                        
                                                    }
                                                    break
                                                }
                                                
                                                
                                                
                                                
                                                
                                                
                                            }
                                        }
                                        
                                        
                                    } catch _ {
                                        if let statusCode = response.response?.statusCode {
                                            print(statusCode)
                                            switch (statusCode){
                                            case 404:
                                                self.view.makeToast("no appointments today", duration: 2.0, position: .center)
                                                DispatchQueue.main.async {
                                                    self.todaysAppointments.removeAll()
                                                    self.collectionView.reloadData()
                                                }
                                            case 500:
                                                print("An error occurred")
                                            default:
                                                print("Sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
        }
    }
    
    
    private func handleAddingandUpdatingAppointmentArray(newAppointment: AppointmentData, arraySize: Int ){
        self.alwaynewAppointment.append(newAppointment)
        
        if (self.todaysAppointments.isEmpty){
            self.todaysAppointments.append(newAppointment)
            self.todaysAppointments.sort { (first, second) -> Bool in
                first.appointmentDateStartObj.isBeforeDate(second.appointmentDateStartObj, granularity: .minute)
            }
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        } else {
            if let row = self.todaysAppointments.index(where: {$0.id == newAppointment.id}) {
                self.todaysAppointments[row] = newAppointment
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            } else {
                self.todaysAppointments.append(newAppointment)
                self.todaysAppointments.sort { (first, second) -> Bool in
                    first.appointmentDateStartObj.isBeforeDate(second.appointmentDateStartObj, granularity: .minute)
                }
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
        }
        
        if (self.alwaynewAppointment.count == arraySize){
            for data in todaysAppointments {
                
                if (!self.alwaynewAppointment.contains(where: {$0.id == data.id })){
                    if let firstIndex = self.todaysAppointments.firstIndex(where: {$0.id == data.id}){
                        self.todaysAppointments.remove(at: firstIndex)
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                        }
                    }
                }
                
                
            }
        }
    }
    
    
    private func getShopDataForAppointments() {
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let customerID = decodedCustomer.first?.customerId, let token = decodedCustomer.first?.token {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    SwiftSpinner.show("Loading...")
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    DispatchQueue.global(qos: .background).async {
                        AF.request(self.BACKEND_URL + "shops/" + customerID, method: .post, headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    do {
                                        let customerShopData = try JSONDecoder().decode([Shop].self, from: jsonData)
                                        
                                        let shopCount = customerShopData.count
                                        var shopCountCheck = 0
                                        for singleShop in customerShopData {
                                            shopCountCheck = shopCountCheck + 1
                                            
                                            let shop = ShopData(id: singleShop.id, shopAddress: singleShop.shopAddress, shopAddressLatitude: singleShop.shopAddressLatitude, shopAddressLongitude: singleShop.shopAddressLongitude, shopCanViewBookings: singleShop.shopCanViewBookings, shopCountry: singleShop.shopCountry, shopCountryCode: singleShop.shopCountryCode, shopCountryCurrencyCode: singleShop.shopCountryCurrencyCode, shopCustomerGender: singleShop.shopCustomerGender, shopDateCreated: singleShop.shopDateCreated, shopDateCreatedCalendar: singleShop.shopDateCreatedCalendar, shopDateCreatedLocale: singleShop.shopDateCreatedLocale, shopDateCreatedTimezone: singleShop.shopDateCreatedTimezone, shopDescription: singleShop.shopDescription, shopEmail: singleShop.shopEmail, shopHiddenByAdmin: singleShop.shopHiddenByAdmin, shopHiddenByCustomer: singleShop.shopHiddenByCustomer, shopLogoImageName: singleShop.shopLogoImageName, shopLogoImageURL: singleShop.shopLogoImageURL, shopMobileNumber: singleShop.shopMobileNumber, shopName: singleShop.shopName, shopOwner: singleShop.shopOwner, shopRating: singleShop.shopRating, shortUniqueID: singleShop.shortUniqueID)
                                            self.tempShopDataArray.append(shop)
                                            
                                            if(shopCountCheck == shopCount){
                                                
                                                self.shopDataArray = self.tempShopDataArray
                                                if let firstShop = self.shopDataArray.first?.id, let custOwner = self.shopDataArray.first?.shopOwner, let curren = self.shopDataArray.first?.shopCountryCurrencyCode, let tmzone = self.shopDataArray.first?.shopDateCreatedTimezone, let calen = self.shopDataArray.first?.shopDateCreatedCalendar, let localell = self.shopDataArray.first?.shopDateCreatedLocale, let name = self.shopDataArray.first?.shopName {
                                                    self.chosenShopID = firstShop
                                                    self.chosenShopCurrencyCode = curren
                                                    self.currentShopOwnerCustomerid = custOwner
                                                    self.chosenShopTimeZoneID = tmzone
                                                    self.chosenShopCalendarID = calen
                                                    self.chosenShopLocaleID = localell
                                                    self.appointmentViewTitlePlaceHolder.text = name
                                                    
                                                    self.getTodaysAppointment(firstShop: firstShop)
                                                }
                                            }
                                            
                                        }
                                        
                                        
                                    } catch _ {
                                        if let statusCode = response.response?.statusCode{
                                            switch (statusCode){
                                            case 404:
                                                print("Shop not found")
                                                self.checkIfCustomerIsAStaff()
                                            case 500:
                                                print("An error occurred")
                                            default:
                                                print("Sky high")
                                            }
                                        }
                                        
                                    }
                                } else {
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                }
                        }
                    }
                }
            }
        }
    }
    
    @objc func checkIfCustomerIsAStaff(){
        DispatchQueue.global(qos: .background).async {
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let customerID = decodedCustomer.first?.customerId, let token = decodedCustomer.first?.token {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        AF.request(self.BACKEND_URL + "getStaffByCustomer/" + customerID, method: .post, headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    do {
                                        print("gotten staff data")
                                        let customerSingleData = try JSONDecoder().decode(Staff.self, from: jsonData)
                                        self.staffID = customerSingleData.id
                                        self.staffShopID = customerSingleData.shop
                                        self.staffCustomerID = customerSingleData.customer
                                        self.chosenShopID = customerSingleData.shop
                                        self.getShopData(shopUniqueID: customerSingleData.shop)
                                        
                                    } catch _ {
                                        
                                        if let statusCode = response.response?.statusCode{
                                            switch (statusCode) {
                                            case 404:
                                                self.view.makeToast("Staff not found", duration: 2.0, position: .bottom)
                                            case 500:
                                                self.view.makeToast("an error ocurred", duration: 2.0, position: .bottom)
                                            default:
                                                print("Sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                        
                    }
                }
            }
        }
        
    }
    
    @objc private func getShopData(shopUniqueID: String){
        DispatchQueue.global(qos: .background).async {
            
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            SwiftSpinner.hide()
                            UtilityManager.alertView(view: self)
                            print("error in data request")
                        }
                    } else {
                        
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        AF.request(self.BACKEND_URL + "getsingleshop/" + shopUniqueID, method: .post, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    
                                    do {
                                        let shopData = try JSONDecoder().decode(Shop.self, from: jsonData)
                                        self.chosenShopID = shopData.id
                                        self.chosenShopTimeZoneID = shopData.shopDateCreatedTimezone
                                        self.chosenShopCalendarID = shopData.shopDateCreatedCalendar
                                        self.chosenShopLocaleID = shopData.shopDateCreatedLocale
                                        self.chosenShopCurrencyCode = shopData.shopCountryCurrencyCode
                                        self.appointmentViewTitlePlaceHolder.text = shopData.shopName
                                        self.getTodaysAppointment(firstShop: shopData.id)
                                        
                                        
                                    } catch _ {
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                self.view.makeToast("Shops not found", duration: 2.0, position: .bottom)
                                            case 500:
                                                self.view.makeToast("An error occurred, please try again later", duration: 2.0, position: .bottom)
                                            default:
                                                print("sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
    }
    
    func setupViewContraints(){
        var topDistance: CGFloat = 20
        var bottomDistance: CGFloat = -10
        if UIDevice.current.hasNotch {
            topDistance = 60
            bottomDistance = -24
        }
        
        upperInputsContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(shopSelectionButton)
        upperInputsContainerView.addSubview(appointmentViewTitlePlaceHolder)
        upperInputsContainerView.addSubview(shopProfileViewButton)
        
        shopSelectionButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        shopSelectionButton.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor).isActive = true
        shopSelectionButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        shopSelectionButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        shopProfileViewButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        shopProfileViewButton.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        shopProfileViewButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        shopProfileViewButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        appointmentViewTitlePlaceHolder.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        appointmentViewTitlePlaceHolder.leftAnchor.constraint(equalTo: shopSelectionButton.rightAnchor).isActive = true
        appointmentViewTitlePlaceHolder.rightAnchor.constraint(equalTo: shopProfileViewButton.leftAnchor).isActive = true
        appointmentViewTitlePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        curvedCollectViewHolder.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 10).isActive = true
        curvedCollectViewHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        curvedCollectViewHolder.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -24).isActive = true
        curvedCollectViewHolder.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: bottomDistance).isActive = true
        
        curvedCollectViewHolder.addSubview(collectView)
        curvedCollectViewHolder.bringSubviewToFront(collectView)
        
        collectView.topAnchor.constraint(equalTo: curvedCollectViewHolder.topAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: curvedCollectViewHolder.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: curvedCollectViewHolder.widthAnchor, constant: -10).isActive = true
        collectView.bottomAnchor.constraint(equalTo: curvedCollectViewHolder.bottomAnchor, constant: -5).isActive = true
        
        collectView.addSubview(collectionView)
        collectView.bringSubviewToFront(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refresherController
        } else {
            collectionView.addSubview(refresherController)
        }
    }

}


class customAppointmentViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let customerProfileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 45
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    lazy var clientNamePlaceHolderLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8CustomerLogo")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let clientNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 12)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var startTimePlaceHolderLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8timeSpanLogo")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let startTimePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 12)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var staffNamePlaceHolderLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8staffLogo")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let staffNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 12)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var servicePricePlaceHolderLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8cashLogo")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let servicePricePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 12)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    func setupViews(){
        addSubview(customerProfileImageView)
        addSubview(clientNamePlaceHolder)
        addSubview(clientNamePlaceHolderLogo)
        addSubview(startTimePlaceHolder)
        addSubview(startTimePlaceHolderLogo)
        addSubview(staffNamePlaceHolder)
        addSubview(staffNamePlaceHolderLogo)
        addSubview(servicePricePlaceHolder)
        addSubview(servicePricePlaceHolderLogo)
        
        
        backgroundColor = UIColor.white
        layer.cornerRadius = 5
        
        addContraintsWithFormat(format: "H:|-5-[v0(90)]-10-[v1(20)]-5-[v2]-5-|", views: customerProfileImageView, clientNamePlaceHolderLogo, clientNamePlaceHolder)
        addContraintsWithFormat(format: "H:|-5-[v0(90)]-10-[v1(20)]-5-[v2]-5-|", views: customerProfileImageView, startTimePlaceHolderLogo, startTimePlaceHolder)
        addContraintsWithFormat(format: "H:|-5-[v0(90)]-10-[v1(20)]-5-[v2]-5-|", views: customerProfileImageView, staffNamePlaceHolderLogo, staffNamePlaceHolder)
        addContraintsWithFormat(format: "H:|-5-[v0(90)]-10-[v1(20)]-5-[v2]-5-|", views: customerProfileImageView, servicePricePlaceHolderLogo, servicePricePlaceHolder)
        
        addContraintsWithFormat(format: "V:|-5-[v0(90)]-5-|", views: customerProfileImageView)
        addContraintsWithFormat(format: "V:|-5-[v0(20)]-5-[v1(20)]-5-[v2(20)]-5-[v3(20)]-5-|", views: clientNamePlaceHolderLogo, startTimePlaceHolderLogo, staffNamePlaceHolderLogo, servicePricePlaceHolderLogo)
        addContraintsWithFormat(format: "V:|-5-[v0(20)]-5-[v1(20)]-5-[v2(20)]-5-[v3(20)]-5-|", views: clientNamePlaceHolder, startTimePlaceHolder, staffNamePlaceHolder, servicePricePlaceHolder)
        
    }
    
    override func prepareForReuse() {
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension AppointmentsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.todaysAppointments.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customAppointmentViewCell
        cell.customerProfileImageView.image = self.todaysAppointments[indexPath.row].appontmentCustomerImage
        cell.clientNamePlaceHolder.text = self.todaysAppointments[indexPath.row].appointmentCustomerFirstName
        cell.startTimePlaceHolder.text = self.todaysAppointments[indexPath.row].appointmentFormatedStartEndTime
        cell.staffNamePlaceHolder.text = self.todaysAppointments[indexPath.row].appointmentStaffFirstName
        let curr = self.todaysAppointments[indexPath.row].appointmentCurrency
        cell.servicePricePlaceHolder.text = self.todaysAppointments[indexPath.row].appointmentPrice + " " + curr
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 105)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let appointmentDetail = OrderDetailViewController()
        appointmentDetail.appointmentID = self.todaysAppointments[indexPath.row].id
        appointmentDetail.staffID = self.todaysAppointments[indexPath.row].appointmentStaffCustomer
        appointmentDetail.customerID = self.todaysAppointments[indexPath.row].appointmentCustomer
        appointmentDetail.shopID = self.todaysAppointments[indexPath.row].appointmentShop
        appointmentDetail.serviceList = self.todaysAppointments[indexPath.row].appointmentService
        appointmentDetail.modalPresentationStyle = .overCurrentContext
        if let shopOwn = self.currentShopOwnerCustomerid{
            appointmentDetail.shopOwnerCustomerID = shopOwn
        }
        present(appointmentDetail, animated: true, completion: nil)
        
    }
}
