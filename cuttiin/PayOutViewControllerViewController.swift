//
//  PayOutViewControllerViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/24/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate

class PayOutViewControllerViewController: UIViewController {
    var payCollectionViewData = [PayOutCollectionDataHolder]()
    var totalAmountHolder =  [Int]()
    var bookingSartDates = [DateInRegion]()
    var arrayPayoutDataHold = [Payouts]()
    var amountPayoutSumUp = [Int]()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.white
        cv.allowsMultipleSelection = true
        cv.register(customPayoutCollectionViewCellBarbers.self, forCellWithReuseIdentifier: "cellIDPayoout")
        return cv
    }()
    
    let accountDetailsContainerView: UIView = {
        let adcview = UIView()
        adcview.translatesAutoresizingMaskIntoConstraints = false
        return adcview
    }()
    
    let regNrHiddenPlaceHolder: UILabel = {
        let lnhp = UILabel()
        lnhp.translatesAutoresizingMaskIntoConstraints = false
        lnhp.text = "Reg nr og Konto nr"
        lnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        lnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        lnhp.adjustsFontSizeToFitWidth = true
        lnhp.minimumScaleFactor = 0.1
        lnhp.baselineAdjustment = .alignCenters
        lnhp.textAlignment = .left
        return lnhp
    }()
    
    let regNrTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.keyboardType = .numberPad
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.placeholder = "Reg nr"
        em.addTarget(self, action: #selector(regNrtextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(charMarLimit), for: .editingChanged)
        em.addTarget(self, action: #selector(regNrtextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let regNrSeperatorView: UIView = {
        let lnsv = UIView()
        lnsv.translatesAutoresizingMaskIntoConstraints = false
        lnsv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return lnsv
    }()
    
    let kontoNrTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.keyboardType = .numberPad
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.placeholder = "Konto nr."
        em.addTarget(self, action: #selector(kontoNrtextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(kontoNrtextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let kontoNrSeperatorView: UIView = {
        let lnsv = UIView()
        lnsv.translatesAutoresizingMaskIntoConstraints = false
        lnsv.backgroundColor = UIColor(r: 43, g: 42, b: 41)
        return lnsv
    }()
    
    lazy var continueToPayBooking: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        st.setTitle(NSLocalizedString("continueToPaymentButtonTextPayOutView", comment: "CONTINUE"), for: .normal)
        st.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 30)
        st.tag = 1
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.layer.borderWidth = 2
        st.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        st.isUserInteractionEnabled = false
        return st
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = NSLocalizedString("navigationTitleTextPayoutView", comment: "Payout")
        view.backgroundColor = UIColor.white
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleDismissView))
        view.addSubview(collectView)
        view.addSubview(accountDetailsContainerView)
        view.addSubview(continueToPayBooking)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjectsConstraints()
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func setupViewObjectsConstraints(){
        collectView.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -12).isActive = true
        collectView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
        accountDetailsContainerView.topAnchor.constraint(equalTo: collectView.bottomAnchor, constant: 5).isActive = true
        accountDetailsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        accountDetailsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -12).isActive = true
        accountDetailsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        accountDetailsContainerView.addSubview(regNrHiddenPlaceHolder)
        accountDetailsContainerView.addSubview(regNrTextField)
        accountDetailsContainerView.addSubview(regNrSeperatorView)
        accountDetailsContainerView.addSubview(kontoNrTextField)
        accountDetailsContainerView.addSubview(kontoNrSeperatorView)
        
        regNrHiddenPlaceHolder.topAnchor.constraint(equalTo: accountDetailsContainerView.topAnchor).isActive = true
        regNrHiddenPlaceHolder.centerXAnchor.constraint(equalTo: accountDetailsContainerView.centerXAnchor).isActive = true
        regNrHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        regNrHiddenPlaceHolder.widthAnchor.constraint(equalTo: accountDetailsContainerView.widthAnchor).isActive = true
        
        regNrTextField.topAnchor.constraint(equalTo: regNrHiddenPlaceHolder.bottomAnchor).isActive = true
        regNrTextField.leftAnchor.constraint(equalTo: accountDetailsContainerView.leftAnchor).isActive = true
        regNrTextField.widthAnchor.constraint(equalToConstant: 80).isActive = true
        regNrTextField.heightAnchor.constraint(equalTo: accountDetailsContainerView.heightAnchor, constant: -15).isActive = true
        
        regNrSeperatorView.leftAnchor.constraint(equalTo: accountDetailsContainerView.leftAnchor).isActive = true
        regNrSeperatorView.topAnchor.constraint(equalTo: regNrTextField.bottomAnchor).isActive = true
        regNrSeperatorView.widthAnchor.constraint(equalTo: regNrTextField.widthAnchor).isActive = true
        regNrSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        
        kontoNrTextField.topAnchor.constraint(equalTo: regNrHiddenPlaceHolder.bottomAnchor).isActive = true
        kontoNrTextField.leftAnchor.constraint(equalTo: regNrTextField.rightAnchor, constant: 10).isActive = true
        kontoNrTextField.rightAnchor.constraint(equalTo: accountDetailsContainerView.rightAnchor).isActive = true
        kontoNrTextField.heightAnchor.constraint(equalTo: accountDetailsContainerView.heightAnchor, constant: -15).isActive = true
        
        kontoNrSeperatorView.leftAnchor.constraint(equalTo: regNrSeperatorView.rightAnchor, constant: 10).isActive = true
        kontoNrSeperatorView.topAnchor.constraint(equalTo: kontoNrTextField.bottomAnchor).isActive = true
        kontoNrSeperatorView.widthAnchor.constraint(equalTo: kontoNrTextField.widthAnchor).isActive = true
        kontoNrSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        continueToPayBooking.topAnchor.constraint(equalTo: kontoNrSeperatorView.bottomAnchor, constant: 10).isActive = true
        continueToPayBooking.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        continueToPayBooking.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -12).isActive = true
        continueToPayBooking.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    @objc func handleDismissView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func textFieldDidChange(){
        if regNrTextField.text == "" || kontoNrTextField.text == "" {
            //Disable button
            self.continueToPayBooking.isEnabled = false
            self.continueToPayBooking.setTitleColor(UIColor.darkGray, for: .normal)
            
        } else {
            //Enable button
            self.continueToPayBooking.isEnabled = true
            navigationItem.rightBarButtonItem?.isEnabled = true
            self.continueToPayBooking.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
        }
    }
    
    @objc func regNrtextFieldDidChange(){
        self.regNrSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.kontoNrSeperatorView.backgroundColor = UIColor.black
        
    }
    
    @objc func kontoNrtextFieldDidChange(){
        self.kontoNrSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.regNrSeperatorView.backgroundColor = UIColor.black
    }
    
    @objc func charMarLimit(){
        if let character = self.regNrTextField.text {
            if character.count  == 4 {
                self.kontoNrTextField.becomeFirstResponder()
            }
        }
    }

}

class customPayoutCollectionViewCellBarbers: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let payoutKey: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.text = "Key"
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let payoutValue: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.text = "Value"
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.black
        return sv
    }()
    
    func setupViews(){
        addSubview(payoutKey)
        addSubview(payoutValue)
        addSubview(seperatorView)
        
        backgroundColor = UIColor.white
        addContraintsWithFormat(format: "H:|-16-[v0]-16-[v1(120)]-16-|", views: payoutKey, payoutValue)
        addContraintsWithFormat(format: "V:|-16-[v0(20)]-16-[v1(1)]|", views: payoutKey,seperatorView)
        addContraintsWithFormat(format: "V:|-16-[v0(20)]-16-[v1(1)]|", views: payoutValue,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
