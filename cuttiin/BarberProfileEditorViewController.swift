//
//  BarberProfileEditorViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/25/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GooglePlaces
import SwiftDate
import Alamofire
import SwiftSpinner

class BarberProfileEditorViewController: UIViewController, UIImagePickerControllerDelegate, UITextFieldDelegate, UINavigationControllerDelegate {
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    //Barber editor section
    var userRoleFromProfile: String?
    var shopAddress: String?
    var companyAddressLong: String?
    var companyAddressLat: String?
    var selectedShopUniqueID: String?
    
    var country: String?
    var countryCode: String?
    var countryCurrencyCode: String?
    
    var currentAddress: String?
    var placeMarkData: CLPlacemark?
    var currentUserLocation: CLLocation?
    
    let picker = UIImagePickerController()
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    var emailValid = false
    var passwordValid = false
    
    var genderSelected: String?
    var genderTypes = ["male", "female", "unisex"]
    var shopCategoryTypes = [GenderTrans] ()
    var shopCategorySelelcted: String?
    var shopCategory = ["Barbershop", "Hair salon", "Nail salon", "Spa", "Tattoo", "Massage", "Piercings", "Beauty salon"];
    var locationManager = CLLocationManager()
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    lazy var BarbershopSettingsViewIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8shoppicker")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showShopSelectorView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    lazy var shopProfileCloseViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(exitProfileEditView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let editProfilePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("barberShopProfileEditorViewControllerTitle", comment: "Shop Profile")
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 20) // HelveticaNeue-Medium
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var saveAndUpdateButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitle(NSLocalizedString("barberShopProfileEditorViewControllerSaveButton", comment: "Save"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        st.addTarget(self, action: #selector(textFieldDidChangeBarberShop), for: .touchUpInside)
        st.isEnabled = true
        st.isUserInteractionEnabled = true
        return st
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    let imageAndFirstLastContainerView: UIView = {
        let iascview = UIView()
        iascview.translatesAutoresizingMaskIntoConstraints = false
        return iascview
    }()
    
    lazy var selectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "avatar_icon")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .center
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showCameraActionOptions)))
        imageView.isUserInteractionEnabled = true
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 5
        return imageView
    }()
    
    lazy var selectImageIconButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8editphotoicon")
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = UIColor.white
        return imageView
    }()
    
    let firstNameLastNameContainerView: UIView = {
        let fnlncview = UIView()
        fnlncview.translatesAutoresizingMaskIntoConstraints = false
        return fnlncview
    }()
    
    //BarberShop profile Data
    let companyNameHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("barberShopProfileEditorViewControllerShopName", comment: "Shop name")
        fnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.isHidden = true
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let companyNameTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.placeholder = NSLocalizedString("barberShopProfileEditorViewControllerShopName", comment: "Shop name")
        em.addTarget(self, action: #selector(companyNametextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(companyNametextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let companyNameSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return fnsv
    }()
    
    let companyEmailHiddenPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("barberShopProfileEditorViewControllerEmail", comment: "Email")
        ehp.font = UIFont(name: "OpenSans-Light", size: 10)
        ehp.textColor = UIColor(r: 129, g: 129, b: 129)
        ehp.isHidden = true
        ehp.adjustsFontSizeToFitWidth = true
        ehp.minimumScaleFactor = 0.1
        ehp.baselineAdjustment = .alignCenters
        ehp.textAlignment = .left
        return ehp
    }()
    
    let companyEmailTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.placeholder = NSLocalizedString("barberShopProfileEditorViewControllerEmail", comment: "Email")
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.autocapitalizationType = .none
        em.keyboardType = .emailAddress
        em.addTarget(self, action: #selector(companyEmailtextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(companyEmailtextFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(isValidEmailBarberShop), for: .editingChanged)
        return em
    }()
    
    let companyEmailSeperatorView: UIView = {
        let esv = UIView()
        esv.translatesAutoresizingMaskIntoConstraints = false
        esv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return esv
    }()
    
    lazy var addressSearchLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("barberShopProfileEditorViewControllerAddress", comment: "Address")
        ht.textColor = UIColor(r: 232, g: 232, b: 232)
        ht.font = UIFont(name: "OpenSans-Light", size: 15)
        ht.isUserInteractionEnabled = true
        ht.numberOfLines = 0
        ht.adjustsFontSizeToFitWidth = true
        ht.minimumScaleFactor = 0.1
        ht.baselineAdjustment = .alignCenters
        ht.textAlignment = .left
        ht.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowGoogleAutocomple)))
        ht.isEnabled = false
        ht.isUserInteractionEnabled = false
        return ht
    }()
    
    lazy var currentLocationButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.setImage(UIImage(named: "locationmapmarker"), for: .normal)
        st.backgroundColor = UIColor.clear
        st.addTarget(self, action: #selector(hanldeGettingCurrentocation), for: .touchUpInside)
        st.isEnabled = false
        st.isHidden = true
        return st
    }()
    
    let addressSearchSeperatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return view
    }()
    
    let companyMobileNumberHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("barberShopProfileEditorViewControllerMobileNumber", comment: "Mobile Number")
        fnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.isHidden = true
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let companyMobileNumberTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.placeholder = NSLocalizedString("barberShopProfileEditorViewControllerMobileNumber", comment: "Mobile Number")
        em.keyboardType = .phonePad
        em.addTarget(self, action: #selector(companyMobileNumbertextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(companyMobileNumbertextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let companyMobileNumberSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return fnsv
    }()
    
    let shopCustomerGenderHiddenPlaceHolder: UILabel = {
        let lnhp = UILabel()
        lnhp.translatesAutoresizingMaskIntoConstraints = false
        lnhp.text = NSLocalizedString("barberShopProfileEditorViewControllerShopCategory", comment: "shop category")
        lnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        lnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        lnhp.isHidden = true
        lnhp.adjustsFontSizeToFitWidth = true
        lnhp.minimumScaleFactor = 0.1
        lnhp.baselineAdjustment = .alignCenters
        lnhp.textAlignment = .left
        return lnhp
    }()
    
    let shopCustomerGenderTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.placeholder = NSLocalizedString("barberShopProfileEditorViewControllerShopCategory", comment: "shop category")
        em.addTarget(self, action: #selector(gendertextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(gendertextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let shopCustomerGenderSeperatorView: UIView = {
        let lnsv = UIView()
        lnsv.translatesAutoresizingMaskIntoConstraints = false
        lnsv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return lnsv
    }()
    
    let genderPicker: UIPickerView = {
        let gender = UIPickerView()
        gender.translatesAutoresizingMaskIntoConstraints = false
        return gender
    }()
    
    let errorMessagePlaceHolder: UILabel = {
        let emhp = UILabel()
        emhp.translatesAutoresizingMaskIntoConstraints = false
        emhp.font = UIFont(name: "OpenSans-Bold", size: 15)
        emhp.textColor = UIColor(r: 23, g: 69, b: 90)
        emhp.adjustsFontSizeToFitWidth = true
        emhp.minimumScaleFactor = 0.1
        emhp.numberOfLines = 0
        emhp.baselineAdjustment = .alignCenters
        emhp.textAlignment = .center
        return emhp
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        view.backgroundColor = UIColor.white
        self.companyMobileNumberTextField.delegate = self
        
        self.genderPicker.delegate = self
        self.shopCustomerGenderTextField.inputView = self.genderPicker
        
        
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        
        view.addSubview(shopProfileCloseViewButton)
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        view.addSubview(selectImageView)
        view.addSubview(firstNameLastNameContainerView)
        view.addSubview(currentLocationButton)
        view.addSubview(addressSearchLabel)
        view.addSubview(addressSearchSeperatorView)
        view.addSubview(companyMobileNumberHiddenPlaceHolder)
        view.addSubview(companyMobileNumberTextField)
        view.addSubview(companyMobileNumberSeperatorView)
        view.addSubview(shopCustomerGenderHiddenPlaceHolder)
        view.addSubview(shopCustomerGenderTextField)
        view.addSubview(shopCustomerGenderSeperatorView)
        view.addSubview(errorMessagePlaceHolder)
        setupViewContriantsBarberShop()
        handleGettingCategoryData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .didReceiveData, object: nil)
    }
    
    @objc func handleGettingCategoryData() {
        let valueCategoryBarbershop = GenderTrans(translated: NSLocalizedString("barberShopProfileEditorViewControllerCategoryBarberShop", comment: "Barbershop"), original: "Barbershop")
        let valueCategoryHairSalon = GenderTrans(translated: NSLocalizedString("barberShopProfileEditorViewControllerCategoryHairSalon", comment: "Hair salon"), original: "Hair salon")
        let valueCategoryNailSalon = GenderTrans(translated: NSLocalizedString("barberShopProfileEditorViewControllerCategoryNailSalon", comment: "Nail salon"), original: "Nail salon")
        let valueCategorySpa = GenderTrans(translated: NSLocalizedString("barberShopProfileEditorViewControllerCategorySpa", comment: "Spa"), original: "Spa")
        let valueCategoryTattoo = GenderTrans(translated: NSLocalizedString("barberShopProfileEditorViewControllerCategoryTattoo", comment: "Tattoo"), original: "Tattoo")
        let valueCategoryMassage = GenderTrans(translated: NSLocalizedString("barberShopProfileEditorViewControllerCategoryMassage", comment: "Massage"), original: "Massage")
        let valueCategoryPiercings = GenderTrans(translated: NSLocalizedString("barberShopProfileEditorViewControllerCategoryPiercing", comment: "Piercings"), original: "Piercings")
        let valueCategoryBeautySalon = GenderTrans(translated: NSLocalizedString("barberShopProfileEditorViewControllerCategoryBeautySalon", comment: "Beauty salon"), original: "Beauty salon")
        self.shopCategoryTypes.append(valueCategoryBarbershop)
        self.shopCategoryTypes.append(valueCategoryHairSalon)
        self.shopCategoryTypes.append(valueCategoryNailSalon)
        self.shopCategoryTypes.append(valueCategorySpa)
        self.shopCategoryTypes.append(valueCategoryTattoo)
        self.shopCategoryTypes.append(valueCategoryMassage)
        self.shopCategoryTypes.append(valueCategoryPiercings)
        self.shopCategoryTypes.append(valueCategoryBeautySalon)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    @objc private func onDidReceiveData(_ notification:Notification) {
        if let dataBack = notification.userInfo as? [String: AnyObject], let shopId = dataBack["shopUniqueId"] as? String {
            self.selectedShopUniqueID = shopId
            self.getShopDetails()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    /*func switchShopOfChoice() {
        if let _ = self.selectedShopUniqueID {
            self.viewDidLoad()
        }
    }*/
    
    @objc func getShopGenderByCategory(category: String) -> String {
        switch category {
        case "Barbershop":
            return "male"
        case "Hair salon":
            return "unisex"
        case "Nail salon":
            return "female"
        case "Spa":
            return "unisex"
        case "Tattoo":
            return "unisex"
        case "Massage":
            return "unisex"
        case "Piercings":
            return "unisex"
        case "Beauty salon":
            return "unisex"
        default:
            return "unisex"
        }
    }
    
    
    private func getShopDetails(){
        DispatchQueue.global(qos: .background).async {
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let shopUniqueID = self.selectedShopUniqueID {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    print("shopid: ", shopUniqueID)
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        AF.request(self.BACKEND_URL + "getsingleshop/" + shopUniqueID, method: .post, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    print("got data back")
                                    do {
                                        let shopData = try JSONDecoder().decode(Shop.self, from: jsonData)
                                        
                                        let profileImageFileName = shopData.shopLogoImageName
                                        DispatchQueue.main.async {
                                            
                                            self.addressSearchLabel.text = shopData.shopAddress
                                            
                                            self.shopAddress = shopData.shopAddress
                                            self.companyAddressLat = shopData.shopAddressLatitude
                                            self.companyAddressLong = shopData.shopAddressLongitude
                                            
                                            self.addressSearchLabel.textColor = UIColor.black
                                            self.companyNameTextField.text = shopData.shopName
                                            
                                            self.selectedShopUniqueID = shopData.id
                                            self.companyEmailTextField.text = shopData.shopEmail
                                            self.companyMobileNumberTextField.text = shopData.shopMobileNumber
                                            
                                            if let singleCategory = self.shopCategoryTypes.first(where: {$0.original == shopData.shopCategory }) {
                                                self.shopCustomerGenderTextField.text = singleCategory.translated
                                                self.shopCategorySelelcted = shopData.shopCategory
                                            }
                                            
                                            if let _ = self.genderTypes.first(where: {$0 == shopData.shopCustomerGender }) {
                                                self.genderSelected = shopData.shopCustomerGender
                                            }
                                            
                                            self.companyAddressLong = shopData.shopAddressLongitude
                                            self.companyAddressLat = shopData.shopAddressLatitude
                                            self.country = shopData.shopCountry
                                            self.countryCode = shopData.shopCountryCode
                                            self.countryCurrencyCode = shopData.shopCountryCurrencyCode
                                        }
                                        
                                        print("image name: ", profileImageFileName)
                                        AF.request(self.BACKEND_URL + "images/shopImages/" + profileImageFileName, headers: headers).responseData { response in
                                            
                                            switch response.result {
                                            case .success(let data):
                                                if let image = UIImage(data: data) {
                                                    DispatchQueue.main.async {
                                                        
                                                        self.selectImageView.image = image
                                                        self.selectImageView.contentMode = .scaleAspectFit
                                                    }
                                                }
                                                break
                                            case .failure(let error):
                                                print(error)
                                                self.view.makeToast("Image not found", duration: 3.0, position: .bottom)
                                                break
                                            }
                                            
                                            
                                        }
                                        
                                    } catch _ {
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                self.view.makeToast(NSLocalizedString("barberShopProfileEditorViewControllerErrorMessageShopNotFound", comment: "Shops not found"), duration: 2.0, position: .bottom)
                                            case 500:
                                                self.view.makeToast(NSLocalizedString("barberShopProfileEditorViewControllerErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                            default:
                                                print("sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
    }
    
    
    @objc func updateShopData(){
        print("hello")
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token {
                print("hello first optionals")
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
 
                    if self.companyNameTextField.text != ""
                        && self.addressSearchLabel.text != ""
                        && self.companyEmailTextField.text != ""
                        && self.companyMobileNumberTextField.text != ""
                        && self.shopCustomerGenderTextField.text != ""
                        && self.shopCategorySelelcted != ""
                        && self.genderSelected != "" {
                        if let companyName = self.companyNameTextField.text,
                            let addressString = self.addressSearchLabel.text,
                            let gender = self.genderSelected,
                            let shopCate = self.shopCategorySelelcted,
                            let mNumber = self.companyMobileNumberTextField.text,
                            let longValue = self.companyAddressLong,
                            let latValue = self.companyAddressLat,
                            let ctry = self.country,
                            let ctryCode = self.countryCode,
                            let ctryCurrencyCode = self.countryCurrencyCode,
                            let profileImage = self.selectImageView.image,
                            let shopUniqueID = self.selectedShopUniqueID,
                            let emailText = self.companyEmailTextField.text,
                            let uploadData = profileImage.jpegData(compressionQuality: 0.1) {
                            
                            SwiftSpinner.show("Loading...")
                            
                            let imageName = NSUUID().uuidString
                            
                            let parameters = ["shopName": companyName,
                                              "shopEmail": emailText,
                                              "shopDescription": "not available",
                                              "shopMobileNumber": mNumber,
                                              "shopAddress": addressString,
                                              "shopAddressLongitude": longValue,
                                              "shopAddressLatitude": latValue,
                                              "shopCountryCode": ctryCode,
                                              "shopCountry": ctry,
                                              "shopCountryCurrencyCode": ctryCurrencyCode,
                                              "shopCustomerGender": gender,
                                              "shopCategory": shopCate] as [String : Any]
                            
                            DispatchQueue.global(qos: .background).async {
                                print(parameters)
                                
                                AF.upload(multipartFormData: { (multipartFormData) in
                                    multipartFormData.append(uploadData, withName: "image",fileName: imageName, mimeType: "image/jpg")
                                    for (key, value) in parameters {
                                        if let stringValue = value as? String {
                                            multipartFormData.append(stringValue.data(using: .utf8)!, withName: key)
                                        }
                                    }
                                }, usingThreshold: UInt64.init(), fileManager: .default, to: self.BACKEND_URL + "updateSingleshopCategory/" + shopUniqueID, method: .put, headers: headers, interceptor: nil)
                                    .validate(statusCode: 200..<501)
                                    .validate(contentType: ["application/json"])
                                    .uploadProgress(queue: .main, closure: { (progress) in
                                        print("Upload Progress: \(progress.fractionCompleted)")
                                    }).response { response in
                                        
                                        switch response.result {
                                        case .success(let data):
                                            DispatchQueue.main.async {
                                                SwiftSpinner.hide()
                                            }
                                            if let jsonData = data {
                                                do {
                                                    let serverResponse = try JSONDecoder().decode(ServerMessage.self, from: jsonData)
                                                    let message = serverResponse.message
                                                    self.view.makeToast(message, duration: 3.0, position: .bottom)
                                                } catch _ {
                                                    if let statusCode = response.response?.statusCode {
                                                        print(statusCode)
                                                        switch statusCode {
                                                        case 404:
                                                            self.view.makeToast(NSLocalizedString("barberShopProfileEditorViewControllerErrorMessageShopNotFound", comment: "Shop was not found"), duration: 3.0, position: .bottom)
                                                        case 500:
                                                            self.view.makeToast(NSLocalizedString("barberShopProfileEditorViewControllerErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 3.0, position: .bottom)
                                                        default:
                                                            print("Sky high : update")
                                                        }
                                                    }
                                                }
                                            }
                                            break
                                        case .failure(let error):
                                            DispatchQueue.main.async {
                                                SwiftSpinner.hide()
                                            }
                                            print(error)
                                            let message = NSLocalizedString("barberShopProfileEditorViewControllerErrorMessageUpdateError", comment: "Update error: please try again later")
                                            self.view.makeToast(message, duration: 3.0, position: .bottom)
                                            break
                                        }
                                }
                            }
                            
                        }
                    }
                }
                
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return string == numberFiltered
    }
    
    func setupViewContriantsBarberShop(){
        
        var topDistance: CGFloat = 20
        if UIDevice.current.hasNotch {
            topDistance = 60
        }
        
        shopProfileCloseViewButton.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        shopProfileCloseViewButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -15).isActive = true
        shopProfileCloseViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        shopProfileCloseViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperInputsContainerView.topAnchor.constraint(equalTo: shopProfileCloseViewButton.bottomAnchor).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(BarbershopSettingsViewIcon)
        upperInputsContainerView.addSubview(editProfilePlaceHolder)
        upperInputsContainerView.addSubview(saveAndUpdateButton)
        
        BarbershopSettingsViewIcon.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        BarbershopSettingsViewIcon.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor).isActive = true
        BarbershopSettingsViewIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        BarbershopSettingsViewIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        saveAndUpdateButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        saveAndUpdateButton.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        saveAndUpdateButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        saveAndUpdateButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        editProfilePlaceHolder.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        editProfilePlaceHolder.leftAnchor.constraint(equalTo: BarbershopSettingsViewIcon.rightAnchor, constant: 10).isActive = true
        editProfilePlaceHolder.rightAnchor.constraint(equalTo: saveAndUpdateButton.leftAnchor).isActive = true
        editProfilePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        //here
        selectImageView.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 10).isActive = true
        selectImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        selectImageView.widthAnchor.constraint(equalToConstant: 120).isActive = true
        selectImageView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        
        selectImageView.addSubview(selectImageIconButton)
        selectImageView.bringSubviewToFront(selectImageIconButton)
        
        selectImageIconButton.bottomAnchor.constraint(equalTo: selectImageView.bottomAnchor).isActive = true
        selectImageIconButton.rightAnchor.constraint(equalTo: selectImageView.rightAnchor).isActive = true
        selectImageIconButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        selectImageIconButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        firstNameLastNameContainerView.topAnchor.constraint(equalTo: selectImageView.bottomAnchor, constant: 10).isActive = true
        firstNameLastNameContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        firstNameLastNameContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        firstNameLastNameContainerView.heightAnchor.constraint(equalToConstant: 90).isActive = true
        
        firstNameLastNameContainerView.addSubview(companyNameHiddenPlaceHolder)
        firstNameLastNameContainerView.addSubview(companyNameTextField)
        firstNameLastNameContainerView.addSubview(companyNameSeperatorView)
        firstNameLastNameContainerView.addSubview(companyEmailHiddenPlaceHolder)
        firstNameLastNameContainerView.addSubview(companyEmailTextField)
        firstNameLastNameContainerView.addSubview(companyEmailSeperatorView)
        
        companyNameHiddenPlaceHolder.topAnchor.constraint(equalTo: firstNameLastNameContainerView.topAnchor).isActive = true
        companyNameHiddenPlaceHolder.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        companyNameHiddenPlaceHolder.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        companyNameHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        companyNameTextField.topAnchor.constraint(equalTo: companyNameHiddenPlaceHolder.bottomAnchor).isActive = true
        companyNameTextField.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        companyNameTextField.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        companyNameTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        companyNameSeperatorView.leftAnchor.constraint(equalTo: firstNameLastNameContainerView.leftAnchor).isActive = true
        companyNameSeperatorView.topAnchor.constraint(equalTo: companyNameTextField.bottomAnchor).isActive = true
        companyNameSeperatorView.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        companyNameSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        companyEmailHiddenPlaceHolder.topAnchor.constraint(equalTo: companyNameSeperatorView.bottomAnchor, constant: 5).isActive = true
        companyEmailHiddenPlaceHolder.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        companyEmailHiddenPlaceHolder.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        companyEmailHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        companyEmailTextField.topAnchor.constraint(equalTo: companyEmailHiddenPlaceHolder.bottomAnchor).isActive = true
        companyEmailTextField.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        companyEmailTextField.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        companyEmailTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        companyEmailSeperatorView.leftAnchor.constraint(equalTo: firstNameLastNameContainerView.leftAnchor).isActive = true
        companyEmailSeperatorView.topAnchor.constraint(equalTo: companyEmailTextField.bottomAnchor).isActive = true
        companyEmailSeperatorView.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        companyEmailSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        //hello
        
        currentLocationButton.topAnchor.constraint(equalTo: firstNameLastNameContainerView.bottomAnchor, constant: 5).isActive = true
        currentLocationButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -12).isActive = true
        currentLocationButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        currentLocationButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        addressSearchLabel.topAnchor.constraint(equalTo: firstNameLastNameContainerView.bottomAnchor, constant: 5).isActive = true
        addressSearchLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 12).isActive = true
        addressSearchLabel.rightAnchor.constraint(equalTo: currentLocationButton.leftAnchor, constant: 5).isActive = true
        addressSearchLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        addressSearchSeperatorView.topAnchor.constraint(equalTo: addressSearchLabel.bottomAnchor).isActive = true
        addressSearchSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        addressSearchSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        addressSearchSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        companyMobileNumberHiddenPlaceHolder.topAnchor.constraint(equalTo: addressSearchSeperatorView.bottomAnchor, constant: 5).isActive = true
        companyMobileNumberHiddenPlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        companyMobileNumberHiddenPlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        companyMobileNumberHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        companyMobileNumberTextField.topAnchor.constraint(equalTo: companyMobileNumberHiddenPlaceHolder.bottomAnchor).isActive = true
        companyMobileNumberTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        companyMobileNumberTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        companyMobileNumberTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        companyMobileNumberSeperatorView.topAnchor.constraint(equalTo: companyMobileNumberTextField.bottomAnchor).isActive = true
        companyMobileNumberSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        companyMobileNumberSeperatorView.widthAnchor.constraint(equalTo: companyMobileNumberTextField.widthAnchor).isActive = true
        companyMobileNumberSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        shopCustomerGenderHiddenPlaceHolder.topAnchor.constraint(equalTo: companyMobileNumberSeperatorView.bottomAnchor, constant: 5).isActive = true
        shopCustomerGenderHiddenPlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        shopCustomerGenderHiddenPlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        shopCustomerGenderHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        shopCustomerGenderTextField.topAnchor.constraint(equalTo: shopCustomerGenderHiddenPlaceHolder.bottomAnchor).isActive = true
        shopCustomerGenderTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        shopCustomerGenderTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        shopCustomerGenderTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        shopCustomerGenderSeperatorView.topAnchor.constraint(equalTo: shopCustomerGenderTextField.bottomAnchor).isActive = true
        shopCustomerGenderSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        shopCustomerGenderSeperatorView.widthAnchor.constraint(equalTo: shopCustomerGenderTextField.widthAnchor).isActive = true
        shopCustomerGenderSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        errorMessagePlaceHolder.topAnchor.constraint(equalTo: companyMobileNumberSeperatorView.bottomAnchor, constant: 10).isActive = true
        errorMessagePlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        errorMessagePlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        errorMessagePlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        getShopDetails()
    }
    
    @objc func exitProfileEditView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func showShopSelectorView(){
        
        let customerShops = CustomerShopsViewController()
        customerShops.barberShopProfileEditorView = self
        customerShops.viewControllerWhoInitiatedAction = "barberShopProfileEdit"
        customerShops.modalPresentationStyle = .overCurrentContext
        present(customerShops, animated: true, completion: nil)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @objc func handleShowGoogleAutocomple(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        autocompleteController.modalPresentationStyle = .overCurrentContext
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @objc private func textFieldDidChangeBarberShop(){
        if companyNameTextField.text == "" || companyEmailTextField.text == "" || addressSearchLabel.text == "" || companyMobileNumberTextField.text == "" || shopCustomerGenderTextField.text == "" {
            //Disable button
            self.view.makeToast(NSLocalizedString("barberShopProfileEditorViewControllerErrorMessageIncompleteForm", comment: "incomplete form please fill properly"), duration: 3.0, position: .bottom)
            
        } else {
            //Enable button
            self.isValidEmailBarberShop()
            if emailValid == true {
                self.updateShopData()
            }
        }
    }
    
    //BarberShop
    
    @objc func companyNametextFieldDidChange(){
        self.companyNameSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.companyEmailSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.addressSearchSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.companyMobileNumberSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.shopCustomerGenderSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.companyNameHiddenPlaceHolder.isHidden = false
        self.companyEmailHiddenPlaceHolder.isHidden = true
        self.companyMobileNumberHiddenPlaceHolder.isHidden = true
        self.shopCustomerGenderHiddenPlaceHolder.isHidden = true
    }
    
    @objc func companyEmailtextFieldDidChange(){
        self.companyNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.companyEmailSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.addressSearchSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.companyMobileNumberSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.shopCustomerGenderSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.companyNameHiddenPlaceHolder.isHidden = true
        self.companyEmailHiddenPlaceHolder.isHidden = false
        self.companyMobileNumberHiddenPlaceHolder.isHidden = true
        self.shopCustomerGenderHiddenPlaceHolder.isHidden = true
    }
    
    @objc func companyMobileNumbertextFieldDidChange(){
        self.companyNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.companyEmailSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.addressSearchSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.shopCustomerGenderSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.companyMobileNumberSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.companyNameHiddenPlaceHolder.isHidden = true
        self.companyEmailHiddenPlaceHolder.isHidden = true
        self.companyMobileNumberHiddenPlaceHolder.isHidden = false
        self.shopCustomerGenderHiddenPlaceHolder.isHidden = true
    }
    
    @objc func gendertextFieldDidChange(){
        self.companyNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.companyEmailSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.addressSearchSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.companyMobileNumberSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.shopCustomerGenderSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.companyNameHiddenPlaceHolder.isHidden = true
        self.companyEmailHiddenPlaceHolder.isHidden = true
        self.companyMobileNumberHiddenPlaceHolder.isHidden = false
        self.shopCustomerGenderHiddenPlaceHolder.isHidden = false
    }
    
    @objc func isValidEmailBarberShop() {
        let testStr = companyEmailTextField.text ?? ""
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        if result == true {
            self.emailValid = true
            self.companyEmailHiddenPlaceHolder.text = NSLocalizedString("barberShopProfileEditorViewControllerEmail", comment: "Email")
        }else{
            self.emailValid = false
        }
    }
    
    @objc func showCameraActionOptions(){
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("barberShopProfileEditorViewControllerCancelButton", comment: "Cancel"), style: .cancel) { action in
        }
        alertController.addAction(cancelAction)
        
        let takePhotoAction = UIAlertAction(title: NSLocalizedString("barberShopProfileEditorViewControllerTakePhotoButton", comment: "Take photo"), style: .default) { action in
            self.handleTakePhoto()
        }
        alertController.addAction(takePhotoAction)
        
        let choosePhotoAction = UIAlertAction(title: NSLocalizedString("barberShopProfileEditorViewControllerChoosePhotoButton", comment: "Choose photo"), style: .default) { action in
            self.handleChoosePhoto()
        }
        alertController.addAction(choosePhotoAction)
        
        self.present(alertController, animated: true) {
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            
            selectedImageFromPicker = editedImage
            
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            self.selectImageView.contentMode = .scaleAspectFit
            self.selectImageView.image = selectedImage
            self.saveAndUpdateButton.isEnabled = true
        }else {
            print("image not selected")
        }
        
        dismiss(animated:true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func handleTakePhoto(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = true
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            self.view.makeToast(NSLocalizedString("barberShopProfileEditorViewControllerErrorMessageNoCamera", comment: "Sorry, this device has no camera"), duration: 3.0, position: .bottom)
            
        }
    }
    
    func handleChoosePhoto(){
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    
    func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { completion($0?.first, $1) }
    }
    
    @objc private func hanldeGettingCurrentocation(){
        if let place = self.placeMarkData, let locationData = self.currentUserLocation {
            if let countryCode = place.isoCountryCode, let ctry = place.country, let ctryCurrencyCode = Locale.currency[countryCode] as? (String, String)  {
                let cllocationdata = CLLocation(latitude: locationData.coordinate.latitude, longitude: locationData.coordinate.longitude)
                self.reverseGeoCoding(locationData: cllocationdata, ctry: ctry, countryCode: countryCode, ctryCurrencyCode: ctryCurrencyCode.0)
            }
            
        }
    }
    
    @objc private func reverseGeoCoding(locationData: CLLocation, ctry: String, countryCode: String, ctryCurrencyCode: String){
        let longitude = locationData.coordinate.longitude.description
        let latitude = locationData.coordinate.latitude.description
        let placesApiKey = "AIzaSyD0ZRN_cfgRKTV2IWyYQcvcVqw_e6Czy-0"
        print(longitude, latitude)
        
        
        AF.request("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&key=" + placesApiKey)
            .validate(statusCode: 200..<500)
            .validate(contentType: ["application/json"])
            .response { response in
                
                if let error = response.error {
                    print("zones", error.localizedDescription)
                    return
                }
                
                var streetnumber = ""
                var route = ""
                var locality = ""
                var administrative_area_level_2 = ""
                var administrative_area_level_1 = ""
                var countryName = ""
                
                
                
                if let jsonData = response.data {
                    do {
                        let addressData = try JSONDecoder().decode(GooglePlaceAddressData.self, from: jsonData)
                        let counterVal = addressData.results.count
                        var counterAddCheck = 0
                        for addComp in addressData.results {
                            counterAddCheck = counterAddCheck + 1
                            
                            if let types = addComp.types.first {
                                switch (types) {
                                case "street_address":
                                    if let streetnum = addComp.addressComponents[safe: 0]?.longName {
                                        streetnumber = String(streetnum)
                                    }
                                case "route":
                                    if let routename = addComp.addressComponents[safe: 0]?.longName {
                                        route = routename
                                    }
                                case "locality":
                                    if let localityname = addComp.addressComponents[safe: 0]?.longName {
                                        locality = localityname
                                    }
                                case "administrative_area_level_2":
                                    if let adminAreaLevel2 = addComp.addressComponents[safe: 0]?.longName {
                                        administrative_area_level_2 = adminAreaLevel2
                                    }
                                case "administrative_area_level_1":
                                    if let adminAreaLevel1 = addComp.addressComponents[safe: 0]?.longName {
                                        administrative_area_level_1 = adminAreaLevel1
                                    }
                                case "country":
                                    if let countryNames = addComp.addressComponents[safe: 0]?.longName {
                                        countryName = countryNames
                                    }
                                default:
                                    print("Sky high")
                                }
                                
                            }
                            
                            if (counterAddCheck == counterVal) {
                                let formalAddress = streetnumber + " " + route + " " + locality + " " + administrative_area_level_2 + " " + administrative_area_level_1 + " " + countryName
                                self.addressSearchLabel.textColor = UIColor.black
                                self.addressSearchLabel.text = formalAddress
                                self.shopAddress = formalAddress
                                self.companyAddressLong = locationData.coordinate.longitude.description
                                self.companyAddressLat = locationData.coordinate.latitude.description
                                self.country = ctry
                                self.countryCode = countryCode
                                self.countryCurrencyCode = ctryCurrencyCode
                            }
                            
                        }
                        
                    } catch _ {
                        print("An error occured")
                    }
                    
                }
        }
    }

}



extension BarberProfileEditorViewController: GMSAutocompleteViewControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate, CLLocationManagerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        //print("Place name: \(place.name)")
        //print("Place address: \(place.formattedAddress)")
        //print("Place attributions: \(place.attributions)")
        if let lat = Double(place.coordinate.latitude.description), let long = Double(place.coordinate.longitude.description)  {
            self.geocode(latitude: lat, longitude: long) { (placeMarkData, errrror) in
                if let error = errrror {
                    print(error.localizedDescription)
                    return
                }
                
                if let countryCodeData = placeMarkData?.isoCountryCode, let country = placeMarkData?.country, let data = Locale.currency[countryCodeData] as? (String, String) {
                    
                    let cllocationdata = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
                    self.reverseGeoCoding(locationData: cllocationdata, ctry: country, countryCode: countryCodeData, ctryCurrencyCode: data.0)
                    
                    
                }
                
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    //picker view
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.shopCategoryTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.shopCategoryTypes[row].translated
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.genderSelected = self.getShopGenderByCategory(category: self.shopCategoryTypes[row].original)
        self.shopCategorySelelcted = self.shopCategoryTypes[row].original
        self.shopCustomerGenderTextField.text = self.shopCategoryTypes[row].translated
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            self.currentUserLocation = location
            CLGeocoder().reverseGeocodeLocation(location) { (placeMark, errorr) in
                if let error = errorr {
                    print(error.localizedDescription)
                }
                
                if let place = placeMark?.first {
                    self.placeMarkData = place
                }
            }
            
            self.locationManager.stopUpdatingLocation()
        }
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}
