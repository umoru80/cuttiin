//
//  AppointmentStaffCustomer.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 25/03/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation

struct AppointmentStaffCustomer: Codable {
    let message: String
    let staffcustomer: [Customer]
    let customer: [Customer]
    let appointment: [Appointment]
}


