//
//  ShoppingListViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/7/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftyJSON
import SwiftDate
import LocalAuthentication

class ShoppingListViewController: UIViewController {
    var navBar: UINavigationBar = UINavigationBar()
    
    var BarberShopUUID: String?
    var bookingUUID: String?
    var bookedServiceArry = [Service]()
    var bookingDateViewDataObject = BookingDateViewController()
    
    var strCardID = ""
    
    var arrAllCards : JSON?
    
    
    let scrollView: UIScrollView = {
        let v = UIScrollView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.showsVerticalScrollIndicator = false
        v.isScrollEnabled = true
        v.backgroundColor = UIColor.clear
        return v
    }()
    
    let barberShopHeaderDetailsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var headerNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        fnhp.textColor = UIColor.white
        fnhp.text = NSLocalizedString("paymentButtonTitleShoppingListView", comment: "Payment")
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var barberShopNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var barberNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    //main view objects
    let paymentDetailsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        return view
    }()
    
    lazy var paymentMethodsImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "payment-methods")
        return imageView
    }()
    
    let headerViewDescriptionPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "BebasNeue", size: 20)
        fnhp.textColor = UIColor(r: 23, g: 69, b: 90)
        fnhp.text = NSLocalizedString("summaryHeaderNumberOfCustomer", comment: "SUMMARY")
        fnhp.backgroundColor = UIColor.clear
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customShoppingListCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()
    
    let collectionViewSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        return fnsv
    }()
    
    let costTotalContanerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let totalTextHeaderPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor(r: 23, g: 69, b: 90)
        fnhp.text = NSLocalizedString("totalTextHolderNumberOfCustomer", comment: "Total")
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let totalPricePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 16)
        fnhp.textColor = UIColor(r: 23, g: 69, b: 90)
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let buttonContanerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var payFoBookingButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false

//        st.setImage(UIImage(named: "credit_card"), for: .normal)
        st.setImage(UIImage(named: "mobile_phone_outline"), for: .normal)
        st.imageEdgeInsets = UIEdgeInsets(top: 8,left: 130,bottom: 8,right: 15)
        st.titleEdgeInsets = UIEdgeInsets(top: 0,left: -45,bottom: 0,right: 30) //-180 34
        
        st.backgroundColor = UIColor.white //(r: 11, g: 49, b: 68)
        st.setTitle(NSLocalizedString("payButtonTitleShoppingListViewPictureButton", comment: "MOBILEPAY"), for: .normal)
        st.setTitleColor(UIColor(r: 11, g: 49, b: 68), for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 19)
        st.contentHorizontalAlignment = .left
        st.tag = 0
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.layer.borderWidth = 2
        st.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        
        //check here
        st.isEnabled = false
        st.isHidden = true
        return st
    }()
    
    lazy var payFoSavedCardButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        
        st.setImage(UIImage(named: "credit_card"), for: .normal)
        st.imageEdgeInsets = UIEdgeInsets(top: 8,left: 100,bottom: 8,right: 15)
        st.titleEdgeInsets = UIEdgeInsets(top: 0,left: -125,bottom: 0,right: 30) //-180 34

        st.backgroundColor = UIColor.white //(r: 11, g: 49, b: 68)
        st.setTitle(NSLocalizedString("payCreditCardButtonTitleShoppingListViewPictureButton", comment: "CREDIT CARD"), for: .normal)
        st.setTitleColor(UIColor(r: 11, g: 49, b: 68), for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 19)
        st.contentHorizontalAlignment = .left
        st.tag = 2
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.layer.borderWidth = 2
        st.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        
        //check here
        st.isEnabled = false
        st.isHidden = true
        return st
    }()
    
    lazy var payFoBookingButtonWithCash: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        
        st.setImage(UIImage(named: "cash_"), for: .normal)
        st.imageEdgeInsets = UIEdgeInsets(top: 8,left: 100,bottom: 8,right: 15)
        st.titleEdgeInsets = UIEdgeInsets(top: 0,left: -125,bottom: 0,right: 30)
        //st.titleEdgeInsets = UIEdgeInsets(top: 0,left: -180,bottom: 0,right: 34)
        st.backgroundColor = UIColor.white //(r: 11, g: 49, b: 68)
        st.setTitle(NSLocalizedString("payWithCashButtonTitleShoppingListView", comment: "CASH") , for: .normal)
        st.setTitleColor(UIColor(r: 11, g: 49, b: 68), for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 19)
        st.contentHorizontalAlignment = .left
        st.tag = 1
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.layer.borderWidth = 2
        st.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        
        //check here
        st.isEnabled = false
        st.isHidden = true
        return st
    }()
    
    var didShoppingVCBackButtonPressedBlock : (() -> Void)!
    


    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        view.addSubview(scrollView)
        setNavBarToTheView()
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViews()

        UserDefaults.standard.set(false, forKey: "alertViewForMobileNumbersPresentedShoppingListView")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    
    func setNavBarToTheView() {
        self.navBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80)
        self.view.addSubview(navBar)
        let coverLayer = CALayer()
        coverLayer.frame = navBar.bounds;
        coverLayer.backgroundColor = UIColor(r: 23, g: 69, b: 90).cgColor
        navBar.layer.addSublayer(coverLayer)
        navBar.addSubview(barberShopHeaderDetailsContainerView)
        
        barberShopHeaderDetailsContainerView.topAnchor.constraint(equalTo: navBar.topAnchor, constant: 5).isActive = true
        barberShopHeaderDetailsContainerView.centerXAnchor.constraint(equalTo: navBar.centerXAnchor).isActive = true
        barberShopHeaderDetailsContainerView.widthAnchor.constraint(equalTo: navBar.widthAnchor, constant: -96).isActive = true
        barberShopHeaderDetailsContainerView.heightAnchor.constraint(equalTo: navBar.heightAnchor, constant: -35).isActive = true
        
        barberShopHeaderDetailsContainerView.addSubview(headerNamePlaceHolder)
        barberShopHeaderDetailsContainerView.addSubview(barberShopNamePlaceHolder)
        barberShopHeaderDetailsContainerView.addSubview(barberNamePlaceHolder)
        
        headerNamePlaceHolder.topAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.topAnchor).isActive = true
        headerNamePlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        headerNamePlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        headerNamePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        barberShopNamePlaceHolder.topAnchor.constraint(equalTo: headerNamePlaceHolder.bottomAnchor, constant: 5).isActive = true
        barberShopNamePlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopNamePlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberShopNamePlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        barberNamePlaceHolder.topAnchor.constraint(equalTo: barberShopNamePlaceHolder.bottomAnchor, constant: 5).isActive = true
        barberNamePlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberNamePlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberNamePlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
    }
    
    func setupViews(){
        
        
        scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 100).isActive = true
        scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -5).isActive = true
        
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 325)
        
        
        scrollView.addSubview(paymentDetailsContainerView)
        scrollView.addSubview(buttonContanerView)
        
        paymentDetailsContainerView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        paymentDetailsContainerView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        paymentDetailsContainerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        paymentDetailsContainerView.heightAnchor.constraint(equalToConstant: 250).isActive = true
        
        paymentDetailsContainerView.addSubview(paymentMethodsImageView)
        paymentDetailsContainerView.addSubview(headerViewDescriptionPlaceHolder)
        paymentDetailsContainerView.addSubview(collectView)
        paymentDetailsContainerView.addSubview(collectionViewSeperatorView)
        paymentDetailsContainerView.addSubview(costTotalContanerView)
        
        paymentMethodsImageView.topAnchor.constraint(equalTo: paymentDetailsContainerView.topAnchor).isActive = true
        paymentMethodsImageView.centerXAnchor.constraint(equalTo: paymentDetailsContainerView.centerXAnchor).isActive = true
        paymentMethodsImageView.widthAnchor.constraint(equalTo: paymentDetailsContainerView.widthAnchor, multiplier: 1, constant: -24).isActive = true
        paymentMethodsImageView.heightAnchor.constraint(equalToConstant: 55).isActive = true
        
        headerViewDescriptionPlaceHolder.topAnchor.constraint(equalTo: paymentMethodsImageView.bottomAnchor, constant: 10).isActive = true
        headerViewDescriptionPlaceHolder.centerXAnchor.constraint(equalTo: paymentDetailsContainerView.centerXAnchor).isActive = true
        headerViewDescriptionPlaceHolder.widthAnchor.constraint(equalTo: paymentDetailsContainerView.widthAnchor, multiplier: 1, constant: -24).isActive = true
        headerViewDescriptionPlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        collectView.topAnchor.constraint(equalTo: headerViewDescriptionPlaceHolder.bottomAnchor).isActive = true
        collectView.centerXAnchor.constraint(equalTo: paymentDetailsContainerView.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: paymentDetailsContainerView.widthAnchor, multiplier: 1, constant: -24).isActive = true
        collectView.heightAnchor.constraint(equalToConstant: 90).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
        collectionViewSeperatorView.topAnchor.constraint(equalTo: collectView.bottomAnchor, constant: 10).isActive = true
        collectionViewSeperatorView.centerXAnchor.constraint(equalTo: paymentDetailsContainerView.centerXAnchor).isActive = true
        collectionViewSeperatorView.widthAnchor.constraint(equalTo: paymentDetailsContainerView.widthAnchor, constant: -24).isActive = true
        collectionViewSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        costTotalContanerView.topAnchor.constraint(equalTo: collectionViewSeperatorView.bottomAnchor, constant: 10).isActive = true
        costTotalContanerView.centerXAnchor.constraint(equalTo: paymentDetailsContainerView.centerXAnchor).isActive = true
        costTotalContanerView.widthAnchor.constraint(equalTo: paymentDetailsContainerView.widthAnchor, constant: -24).isActive = true
        costTotalContanerView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        costTotalContanerView.addSubview(totalTextHeaderPlaceHolder)
        costTotalContanerView.addSubview(totalPricePlaceHolder)
        
        totalTextHeaderPlaceHolder.topAnchor.constraint(equalTo: costTotalContanerView.topAnchor).isActive = true
        totalTextHeaderPlaceHolder.leftAnchor.constraint(equalTo: costTotalContanerView.leftAnchor).isActive = true
        totalTextHeaderPlaceHolder.widthAnchor.constraint(equalToConstant: 70).isActive = true
        totalTextHeaderPlaceHolder.heightAnchor.constraint(equalTo: costTotalContanerView.heightAnchor).isActive = true
        
        totalPricePlaceHolder.topAnchor.constraint(equalTo: costTotalContanerView.topAnchor).isActive = true
        totalPricePlaceHolder.rightAnchor.constraint(equalTo: costTotalContanerView.rightAnchor).isActive = true
        totalPricePlaceHolder.widthAnchor.constraint(equalToConstant: 75).isActive = true
        totalPricePlaceHolder.heightAnchor.constraint(equalTo: costTotalContanerView.heightAnchor).isActive = true
        
        buttonContanerView.topAnchor.constraint(equalTo: paymentDetailsContainerView.bottomAnchor, constant: 10).isActive = true
        buttonContanerView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        buttonContanerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        buttonContanerView.heightAnchor.constraint(equalToConstant: 55).isActive = true
        
        buttonContanerView.addSubview(payFoBookingButton)
//        self.scrollView.addSubview(self.lblPayForBooking)
//        self.scrollView.addSubview(self.imgPayForBooking)
        
        buttonContanerView.addSubview(payFoBookingButtonWithCash)
        
        payFoBookingButton.topAnchor.constraint(equalTo: buttonContanerView.topAnchor).isActive = true
        payFoBookingButton.leftAnchor.constraint(equalTo: buttonContanerView.leftAnchor).isActive = true
        payFoBookingButton.widthAnchor.constraint(equalTo: buttonContanerView.widthAnchor, multiplier: 0.5, constant: -5).isActive = true
        payFoBookingButton.heightAnchor.constraint(equalTo: buttonContanerView.heightAnchor).isActive = true
        
//        self.lblPayForBooking.leftAnchor.constraint(equalTo: payFoBookingButton.leftAnchor).isActive = true
//        self.lblPayForBooking.centerXAnchor.constraint(equalTo: self.payFoBookingButton.centerXAnchor).isActive = true
//
//        self.imgPayForBooking.topAnchor.constraint(equalTo: payFoBookingButton.topAnchor).isActive = true
//        self.imgPayForBooking.heightAnchor.constraint(equalTo: payFoBookingButton.heightAnchor).isActive = true
//
//        self.imgPayForBooking.frame = CGRect(x: self.imgPayForBooking.frame.size.width * 0.7, y: self.imgPayForBooking.frame.origin.y, width: self.payFoBookingButton.frame.size.height, height: self.payFoBookingButton.frame.size.height)
        
        payFoBookingButtonWithCash.topAnchor.constraint(equalTo: buttonContanerView.topAnchor).isActive = true
        payFoBookingButtonWithCash.rightAnchor.constraint(equalTo: buttonContanerView.rightAnchor).isActive = true
        payFoBookingButtonWithCash.widthAnchor.constraint(equalTo: buttonContanerView.widthAnchor, multiplier: 0.5, constant: -5).isActive = true
        payFoBookingButtonWithCash.heightAnchor.constraint(equalTo: buttonContanerView.heightAnchor).isActive = true
        
        
        self.scrollView.addSubview(self.payFoSavedCardButton)
        
        self.payFoSavedCardButton.topAnchor.constraint(equalTo: self.buttonContanerView.bottomAnchor, constant: 10).isActive = true
        self.payFoSavedCardButton.centerXAnchor.constraint(equalTo: self.scrollView.centerXAnchor).isActive = true
        self.payFoSavedCardButton.widthAnchor.constraint(equalTo: self.buttonContanerView.widthAnchor, multiplier: 0.5, constant: -5).isActive = true
        self.payFoSavedCardButton.heightAnchor.constraint(equalToConstant: 55).isActive = true
    }

}



class customShoppingListCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let serviceNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor(r: 23, g: 69, b: 90)
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let servicePricePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor(r: 23, g: 69, b: 90)
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.white
        return sv
    }()
    
    func setupViews(){
        addSubview(serviceNamePlaceHolder)
        addSubview(servicePricePlaceHolder)
        addSubview(seperatorView)
        
        addContraintsWithFormat(format: "H:|-5-[v0(100)]-15-[v1(100)]-5-|", views: serviceNamePlaceHolder, servicePricePlaceHolder)
        addContraintsWithFormat(format: "V:|[v0]-5-[v1(1)]|", views: serviceNamePlaceHolder,seperatorView)
        addContraintsWithFormat(format: "V:|[v0]-5-[v1(1)]|", views: servicePricePlaceHolder,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
