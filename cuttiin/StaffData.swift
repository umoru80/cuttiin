//
//  StaffData.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 05/03/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit

struct StaffData {
    var staffID: String
    var shopID: String
    var email: String
    var firstName: String
    var lastName: String
    var profileImage: UIImage
    var rating: Int
    var staffHiddenShop: String
    var staffHiddenByAdmin: String
    var dateAccountCreated: String
    var timezone: String
    var calendar: String
    var local: String
    var serviceStaff: [String]
    var staffCustomerId: String
    var staffCustomerShortId: String
    
    init(staffID: String, shopID: String, email: String, firstName: String, lastName: String, profileImage: UIImage, rating: Int, staffHiddenShop: String, staffHiddenByAdmin: String, dateAccountCreated: String, timezone: String, calendar: String, local: String, serviceStaff: [String], staffCustomerId: String, staffCustomerShortId: String) {
        self.staffID = staffID
        self.shopID = shopID
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
        self.profileImage = profileImage
        self.rating = rating
        self.staffHiddenShop = staffHiddenShop
        self.staffHiddenByAdmin = staffHiddenByAdmin
        self.dateAccountCreated = dateAccountCreated
        self.timezone = timezone
        self.calendar = calendar
        self.local = local
        self.serviceStaff = serviceStaff
        self.staffCustomerId = staffCustomerId
        self.staffCustomerShortId = staffCustomerShortId
    }
}
