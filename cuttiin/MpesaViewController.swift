//
//  MpesaViewController.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 19/07/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit
import Alamofire
import SwiftDate
import SwiftyJSON

class MpesaViewController: UIViewController {
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    var shopShortUniqueNumber: String?
    var finalPrice: String?
    
    lazy var mainContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor.white
        tcview.layer.masksToBounds = true
        tcview.layer.cornerRadius = 5
        return tcview
    }()
    
    let mpesaViewHeaderTitlePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 15) // HelveticaNeue-Medium
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .center
        fnhp.text = "Make payment"
        return fnhp
    }()
    
    let mpesaViewDescriptionPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 15) // HelveticaNeue-Medium
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .center
        fnhp.text = "Please make payment via mpesa paybill"
        return fnhp
    }()
    
    lazy var mpesaViewLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "lipanampesa")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let mpesaViewPayBillNoPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 15) // HelveticaNeue-Medium
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let mpesaViewPaybillAccountNoPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 15) // HelveticaNeue-Medium
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let mpesaViewPaybillTotalAmountPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 15) // HelveticaNeue-Medium
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var okButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitleColor(UIColor.purple, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        st.isEnabled = true
        st.isUserInteractionEnabled = true
        st.setTitle("OK", for: .normal)
        st.addTarget(self, action: #selector(exitView), for: .touchUpInside)
        return st
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        view.addSubview(mainContainerView)
        setupViewObjectsConstraints()
        getMpesaDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    @objc private func exitView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    private func getMpesaDetails(){
        DispatchQueue.global(qos: .background).async {
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token {
                    
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        AF.request(self.BACKEND_URL + "getsinglempesapayment/" + "staysharp", method: .post, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    print("got data back")
                                    do {
                                        let mpesaData = try JSONDecoder().decode(MpesaRequest.self, from: jsonData)
                                        DispatchQueue.main.async {
                                            self.mpesaViewPayBillNoPlaceHolder.text = "PAYBILL NO: " + mpesaData.mpesa.businessNumber
                                            
                                            if let data = self.shopShortUniqueNumber, let priceData = self.finalPrice {
                                                self.mpesaViewPaybillAccountNoPlaceHolder.text = "ACCOUNT NO: " + data
                                                self.mpesaViewPaybillTotalAmountPlaceHolder.text = priceData
                                            }
                                            
                                        }
                                        
                                    } catch _ {
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                print("mpesa detail not found")
                                            case 500:
                                                print("An error occurred, please try again later")
                                            default:
                                                print("sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
    }
    
    private func setupViewObjectsConstraints(){
        mainContainerView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        mainContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        mainContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        mainContainerView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5).isActive = true
        
        mainContainerView.addSubview(mpesaViewHeaderTitlePlaceHolder)
        mainContainerView.addSubview(mpesaViewDescriptionPlaceHolder)
        mainContainerView.addSubview(mpesaViewLogo)
        mainContainerView.addSubview(mpesaViewPayBillNoPlaceHolder)
        mainContainerView.addSubview(mpesaViewPaybillAccountNoPlaceHolder)
        mainContainerView.addSubview(mpesaViewPaybillTotalAmountPlaceHolder)
        mainContainerView.addSubview(okButton)
        
        mpesaViewHeaderTitlePlaceHolder.topAnchor.constraint(equalTo: mainContainerView.topAnchor, constant: 10).isActive = true
        mpesaViewHeaderTitlePlaceHolder.centerXAnchor.constraint(equalTo: mainContainerView.centerXAnchor).isActive = true
        mpesaViewHeaderTitlePlaceHolder.widthAnchor.constraint(equalTo: mainContainerView.widthAnchor, constant: -10).isActive = true
        mpesaViewHeaderTitlePlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        mpesaViewDescriptionPlaceHolder.topAnchor.constraint(equalTo: mpesaViewHeaderTitlePlaceHolder.bottomAnchor, constant: 5).isActive = true
        mpesaViewDescriptionPlaceHolder.centerXAnchor.constraint(equalTo: mainContainerView.centerXAnchor).isActive = true
        mpesaViewDescriptionPlaceHolder.widthAnchor.constraint(equalTo: mainContainerView.widthAnchor, constant: -10).isActive = true
        mpesaViewDescriptionPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        mpesaViewLogo.topAnchor.constraint(equalTo: mpesaViewDescriptionPlaceHolder.bottomAnchor, constant: 5).isActive = true
        mpesaViewLogo.centerXAnchor.constraint(equalTo: mainContainerView.centerXAnchor).isActive = true
        mpesaViewLogo.widthAnchor.constraint(equalTo: mainContainerView.widthAnchor, constant: -10).isActive = true
        mpesaViewLogo.heightAnchor.constraint(equalToConstant: 120).isActive = true
        
        mpesaViewPayBillNoPlaceHolder.topAnchor.constraint(equalTo: mpesaViewLogo.bottomAnchor, constant: 15).isActive = true
        mpesaViewPayBillNoPlaceHolder.centerXAnchor.constraint(equalTo: mainContainerView.centerXAnchor).isActive = true
        mpesaViewPayBillNoPlaceHolder.widthAnchor.constraint(equalTo: mainContainerView.widthAnchor, constant: -10).isActive = true
        mpesaViewPayBillNoPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        mpesaViewPaybillAccountNoPlaceHolder.topAnchor.constraint(equalTo: mpesaViewPayBillNoPlaceHolder.bottomAnchor, constant: 5).isActive = true
        mpesaViewPaybillAccountNoPlaceHolder.centerXAnchor.constraint(equalTo: mainContainerView.centerXAnchor).isActive = true
        mpesaViewPaybillAccountNoPlaceHolder.widthAnchor.constraint(equalTo: mainContainerView.widthAnchor, constant: -10).isActive = true
        mpesaViewPaybillAccountNoPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        //mpesaViewPaybillTotalAmountPlaceHolder
        
        mpesaViewPaybillTotalAmountPlaceHolder.topAnchor.constraint(equalTo: mpesaViewPaybillAccountNoPlaceHolder.bottomAnchor, constant: 10).isActive = true
        mpesaViewPaybillTotalAmountPlaceHolder.centerXAnchor.constraint(equalTo: mainContainerView.centerXAnchor).isActive = true
        mpesaViewPaybillTotalAmountPlaceHolder.widthAnchor.constraint(equalTo: mainContainerView.widthAnchor, constant: -10).isActive = true
        mpesaViewPaybillTotalAmountPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        okButton.topAnchor.constraint(equalTo: mpesaViewPaybillTotalAmountPlaceHolder.bottomAnchor, constant: 10).isActive = true
        okButton.centerXAnchor.constraint(equalTo: mainContainerView.centerXAnchor).isActive = true
        okButton.widthAnchor.constraint(equalTo: mainContainerView.widthAnchor, constant: -10).isActive = true
        okButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
    }

}
