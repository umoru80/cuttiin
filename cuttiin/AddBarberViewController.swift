//
//  AddBarberViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/24/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate
import Alamofire
import SwiftSpinner

class AddBarberViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    let picker = UIImagePickerController()
    var addserviceviewcontrl = AddServiceViewController()
    var serviceCerated: String?
    var currentShopID: String?
    var StaffcustomerID: String?
    var StaffcustomerUniqueID: String?
    var staffUniqueID: String?
    var tokenData: String?
    var actionStatusForView: String?
    var staffHiddenStatus: String?
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var services = [Service]()
    var selectedServices = [String]()
    var selectedServiceID = [String]()
    var serviceBarberArrayForLink = [ServiceBarbers]()
    var barberServiceArrayForLink = [BarberServices]()
    var shopServicesAvailable = [ServiceData]()
    var shopServicesAvailableByStaff = [ServiceData]()
    var isStaffDeleted = "NO"
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    var emailValid = false
    var passwordValid = false
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor.clear
        return tcview
    }()
    
    lazy var addBarberViewViewIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8managementShopServices")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var addBarberViewCloseViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismissView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let addBarberViewPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 20) // HelveticaNeue-Medium
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var saveAndUpdateOrCreateButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        st.isEnabled = false
        st.isUserInteractionEnabled = true
        return st
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    let hideButtonTitleLabel: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    lazy var addBarberViewHideStaffButton: UISwitch = {
        let st = UISwitch()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.isOn = false
        st.addTarget(self, action: #selector(handleHidingStaff(_:)), for: .touchUpInside)
        return st
    }()
    
    
    let deleteButtonTitleLabel: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .right
        fnhp.text = NSLocalizedString("AddBarberViewControllerSavedSwitcher", comment: "saved")
        return fnhp
    }()
    
    lazy var addBarberViewDeleteStaffButton: UISwitch = {
        let st = UISwitch()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.isOn = false
        st.addTarget(self, action: #selector(handleDeletingStaff(_:)), for: .touchUpInside)
        return st
    }()
    
    
    let uniqueICustomerIDContainerView: UIView = {
        let iascview = UIView()
        iascview.translatesAutoresizingMaskIntoConstraints = false
        return iascview
    }()
    
    let uniqueIDHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("AddBarberViewControllerUniqueIDPlaceholderText", comment: "Unique id - case sensitive")
        fnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.isHidden = true
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let uniqueIDTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.placeholder = NSLocalizedString("AddBarberViewControllerUniqueIDEditText", comment: "unique - id")
        em.autocapitalizationType = .none
        em.addTarget(self, action: #selector(uniqueIdtextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(uniqueIdtextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    lazy var uniqueIDCheckButton: UIButton = {
        let sptfb = UIButton()
        sptfb.translatesAutoresizingMaskIntoConstraints = false
        sptfb.isUserInteractionEnabled = true
        sptfb.setTitle(NSLocalizedString("AddBarberViewControllerFindButtonTitle", comment: "Find"), for: .normal)
        sptfb.setTitleColor(UIColor.black, for: .normal)
        sptfb.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 12)
        sptfb.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        sptfb.layer.cornerRadius = 5
        sptfb.layer.masksToBounds = true
        sptfb.addTarget(self, action: #selector(handleuniqueCall), for: .touchUpInside)
        return sptfb
    }()
    
    let uniqueIDSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return fnsv
    }()
    
    let imageAndFirstLastContainerView: UIView = {
        let iascview = UIView()
        iascview.translatesAutoresizingMaskIntoConstraints = false
        return iascview
    }()
    
    lazy var selectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "avatar_icon")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let firstNameHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("AddBarberViewControllerFirstNameEditText", comment: "First Name")
        fnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.isHidden = true
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let firstNameTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.isUserInteractionEnabled = false
        em.placeholder = NSLocalizedString("AddBarberViewControllerFirstNameEditText", comment: "First Name")
        return em
    }()
    
    let firstNameSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return fnsv
    }()
    
    let lastNameHiddenPlaceHolder: UILabel = {
        let lnhp = UILabel()
        lnhp.translatesAutoresizingMaskIntoConstraints = false
        lnhp.text = NSLocalizedString("AddBarberViewControllerLastnameEditText", comment: "Last Name")
        lnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        lnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        lnhp.isHidden = true
        lnhp.adjustsFontSizeToFitWidth = true
        lnhp.minimumScaleFactor = 0.1
        lnhp.baselineAdjustment = .alignCenters
        lnhp.textAlignment = .left
        return lnhp
    }()
    
    let lastNameTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.isUserInteractionEnabled = false
        em.placeholder = NSLocalizedString("AddBarberViewControllerLastnameEditText", comment: "Last Name")
        return em
    }()
    
    let lastNameSeperatorView: UIView = {
        let lnsv = UIView()
        lnsv.translatesAutoresizingMaskIntoConstraints = false
        lnsv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return lnsv
    }()
    
    let firstNameLastNameContainerView: UIView = {
        let fnlncview = UIView()
        fnlncview.translatesAutoresizingMaskIntoConstraints = false
        return fnlncview
    }()
    
    let emailHiddenPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("AddBarberViewControllerEmailEditText", comment: "Email")
        ehp.font = UIFont(name: "OpenSans-Light", size: 10)
        ehp.textColor = UIColor(r: 129, g: 129, b: 129)
        ehp.isHidden = true
        ehp.adjustsFontSizeToFitWidth = true
        ehp.minimumScaleFactor = 0.1
        ehp.baselineAdjustment = .alignCenters
        ehp.textAlignment = .left
        return ehp
    }()
    
    let emailTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.placeholder = NSLocalizedString("AddBarberViewControllerEmailEditText", comment: "Email")
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.autocapitalizationType = .none
        em.isUserInteractionEnabled = false
        return em
    }()
    
    let emailSeperatorView: UIView = {
        let esv = UIView()
        esv.translatesAutoresizingMaskIntoConstraints = false
        esv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return esv
    }()
    
    let offeredServiceTextLabel: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("AddBarberViewControllerOfferedServiceTitle", comment: "OFFERED SERVICE")
        fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
        fnhp.textColor = UIColor.black
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let curvedCollectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        cview.layer.masksToBounds = true
        cview.layer.cornerRadius = 5
        return cview
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.allowsMultipleSelection = true
        cv.register(customCollectionViewCellBarbers.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        picker.delegate = self
        view.addSubview(addBarberViewCloseViewButton)
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        view.addSubview(addBarberViewHideStaffButton)
        view.addSubview(hideButtonTitleLabel)
        view.addSubview(addBarberViewDeleteStaffButton)
        view.addSubview(deleteButtonTitleLabel)
        view.addSubview(uniqueICustomerIDContainerView)
        view.addSubview(imageAndFirstLastContainerView)
        view.addSubview(offeredServiceTextLabel)
        view.addSubview(curvedCollectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        
        if let status = self.actionStatusForView {
            switch (status) {
            case "editStaff":
                self.addBarberViewPlaceHolder.text = NSLocalizedString("AddBarberViewControllerEditStaffTitile", comment: "Edit Staff")
                let heighAchorForHideButton: CGFloat = 30
                self.addBarberViewHideStaffButton.isHidden = false
                self.hideButtonTitleLabel.isHidden = false
                self.addBarberViewDeleteStaffButton.isHidden = false
                self.deleteButtonTitleLabel.isHidden = false
                self.saveAndUpdateOrCreateButton.setTitle(NSLocalizedString("AddBarberViewControllerSaveButtonTitle", comment: "Save"), for: .normal)
                self.saveAndUpdateOrCreateButton.addTarget(self, action: #selector(handleCreateStaff), for: .touchUpInside)
                self.uniqueICustomerIDContainerView.isHidden = false
                self.uniqueIDCheckButton.isHidden = true
                self.uniqueIDHiddenPlaceHolder.isHidden = true
                self.uniqueIDTextField.isHidden = true
                self.uniqueIDSeperatorView.isHidden = true
                let heighAchor: CGFloat = 1
                setupViewObjectsConstraints(uniqueHeight: heighAchor, hideHeight: heighAchorForHideButton)
                self.checkAuthStatus(action: status)
                break
            case "createShop":
                self.addBarberViewPlaceHolder.text = NSLocalizedString("AddBarberViewControllerAddAStaffStepTitle", comment: "Add a staff: Step 5 of 5")
                self.addBarberViewPlaceHolder.font = UIFont(name: "OpenSans-SemiBold", size: 13)
                let heighAchorForHideButton: CGFloat = 0
                self.addBarberViewHideStaffButton.isHidden = true
                self.hideButtonTitleLabel.isHidden = true
                self.addBarberViewDeleteStaffButton.isHidden = true
                self.deleteButtonTitleLabel.isHidden = true
                self.saveAndUpdateOrCreateButton.setTitle(NSLocalizedString("AddBarberViewControllerDoneButtonTitlw", comment: "Done"), for: .normal)
                self.saveAndUpdateOrCreateButton.addTarget(self, action: #selector(handleCreateStaff), for: .touchUpInside)
                let heighAchor: CGFloat = 40
                setupViewObjectsConstraints(uniqueHeight: heighAchor, hideHeight: heighAchorForHideButton)
                break
            case "addStaff":
                print("adding staff")
                let heighAchor: CGFloat = 40
                let heighAchorForHideButton: CGFloat = 0
                self.addBarberViewPlaceHolder.text = NSLocalizedString("AddBarberViewControllerAddAStaffTitle", comment: "Add a staff")
                self.addBarberViewHideStaffButton.isHidden = true
                self.hideButtonTitleLabel.isHidden = true
                self.addBarberViewDeleteStaffButton.isHidden = true
                self.deleteButtonTitleLabel.isHidden = true
                self.saveAndUpdateOrCreateButton.setTitle(NSLocalizedString("AddBarberViewControllerSaveButtonTitle", comment: "Save"), for: .normal)
                self.saveAndUpdateOrCreateButton.addTarget(self, action: #selector(handleCreateStaff), for: .touchUpInside)
                setupViewObjectsConstraints(uniqueHeight: heighAchor, hideHeight: heighAchorForHideButton)
                break
            default:
                break
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @objc func handleDismissView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func handleuniqueCall(){
        if let idData = self.uniqueIDTextField.text {
            self.StaffcustomerUniqueID = idData
            self.checkAuthStatus(action: "get customer details")
        }
    }
    
    @objc func handleCreateStaff(){
        self.checkAuthStatus(action: "create a staff")
    }
    
    @objc func checkAuthStatus(action: String){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token {
                
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    switch (action) {
                    case "get service":
                        self.getStaffAndAssociatedServices(token: token)
                        break
                    case "get customer details":
                        self.handleCheckUniqueID(token: token)
                        break
                    case "create a staff":
                        self.handleCreatingAStaff(token: token)
                        break
                    case "editStaff":
                        self.handleCheckUniqueID(token: token)
                        break
                    default:
                        print("Sky high")
                        break
                    }
                    
                }
            }
        }
    }
    
    @objc func handleCheckUniqueID(token: String){
        
        if let uniqueIID = self.StaffcustomerUniqueID {
            SwiftSpinner.show("Loading...")
            DispatchQueue.global(qos: .background).async {
                let headers: HTTPHeaders = [
                    "authorization": token
                ]
                
                let parameters: Parameters = ["customerId": uniqueIID]
                AF.request(self.BACKEND_URL + "uniquecustomer", method: .post, parameters: parameters, headers: headers)
                    .validate(statusCode: 200..<500)
                    .validate(contentType: ["application/json"])
                    .response (completionHandler: { (response) in
                        
                        if let error = response.error {
                            DispatchQueue.main.async {
                                SwiftSpinner.hide()
                            }
                            print(error.localizedDescription)
                            return
                        }
                        
                        if let jsonData = response.data {
                            do {
                                DispatchQueue.main.async {
                                    SwiftSpinner.hide()
                                }
                                let serverResponse = try JSONDecoder().decode(CustomerData.self, from: jsonData)
                                let message = serverResponse.message
                                self.view.makeToast(message, duration: 3.0, position: .bottom)
                                self.firstNameTextField.text = serverResponse.customer.firstName
                                self.lastNameTextField.text = serverResponse.customer.lastName
                                self.emailTextField.text = serverResponse.customer.email
                                let imageName = serverResponse.customer.profileImageName
                                self.StaffcustomerID = serverResponse.customer.id
                                self.handleGetShopServices(token: token)
                                self.saveAndUpdateOrCreateButton.isEnabled = true
                                
                                
                                if let stat = self.staffHiddenStatus {
                                    switch (stat) {
                                    case "NO":
                                        self.addBarberViewHideStaffButton.isOn = true
                                        self.hideButtonTitleLabel.text = NSLocalizedString("AddBarberViewControllerActiveButtonSwitcher", comment: "active")
                                        break
                                    case "YES":
                                        self.addBarberViewHideStaffButton.isOn = false
                                        self.hideButtonTitleLabel.text = NSLocalizedString("AddBarberViewControllerInactiveButtonSwitcher", comment: "inactive")
                                        break
                                    default:
                                        print("hidden status not supplied")
                                        break
                                        
                                    }
                                } else {
                                    self.hideButtonTitleLabel.text = "no data :("
                                }
                                
                                AF.request(self.BACKEND_URL + "images/customerImages/" + imageName, headers: headers).responseData { response in
                                    
                                    switch response.result {
                                    case .success(let data):
                                        let image = UIImage(data: data)
                                        self.selectImageView.image = image
                                        self.selectImageView.contentMode = .scaleAspectFit
                                        break
                                    case .failure(let error):
                                        print("error occured " + error.localizedDescription)
                                        break
                                    }
                                }
                                
                            } catch _ {
                                DispatchQueue.main.async {
                                    SwiftSpinner.hide()
                                }
                                if let statusCode = response.response?.statusCode {
                                    switch statusCode {
                                    case 404:
                                        self.view.makeToast(NSLocalizedString("AddBarberViewControllerErrorMessageNotFound", comment: "Customer not found"), duration: 2.0, position: .bottom)
                                    case 500:
                                        self.view.makeToast(NSLocalizedString("AddBarberViewControllerErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                    default:
                                        print("sky high get customer id")
                                    }
                                }
                            }
                        }
                    })
            }
        }
    }
    
    @objc func handleGetShopServices(token: String){
        if let shop = self.currentShopID {
            
            DispatchQueue.global(qos: .background).async {
                let headers: HTTPHeaders = [
                    "authorization": token
                ]
                
                self.shopServicesAvailable.removeAll()
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
                
                AF.request(self.BACKEND_URL + "getshopservice/" + shop, method: .post, headers: headers)
                    .validate(statusCode: 200..<500)
                    .validate(contentType: ["application/json"])
                    .response (completionHandler: { (response) in
                        
                        if let error = response.error {
                            print(error.localizedDescription)
                            return
                        }
                        
                        if let jsonData = response.data {
                            do {
                                print(jsonData)
                                let serverResponse = try JSONDecoder().decode([ShopService].self, from: jsonData)
                                
                                var counterObserver = 0
                                for shopService in serverResponse {
                                    counterObserver = counterObserver + 1
                                    
                                    if shopService.serviceHiddenByAdmin == "NO" && shopService.serviceHiddenByShop == "NO" {
                                        
                                        if let image = UIImage(named: "avatar_icon") {
                                            let serviceDataSingle = ServiceData(serviceID: shopService.id, serviceTitle: shopService.serviceTitle, serviceEstimatedTime: shopService.serviceEstimatedTotalTime, serviceCost: shopService.servicePrice, shortDescription: shopService.serviceDescription, category: shopService.serviceCategory, dateCreated: shopService.serviceDateCreated, timezone: shopService.serviceDateCreatedTimezone, calendar: shopService.serviceDateCreatedCalendar, local: shopService.serviceDateCreatedLocale, serviceImage: image, isHiddenByShop: shopService.serviceHiddenByShop, isHiddenByAdmin: shopService.serviceHiddenByAdmin, numberOfCustomer: 0, serviceStaff: shopService.serviceStaff, isAssigned: "NO", shop: shopService.serviceShop)
                                            self.shopServicesAvailable.append(serviceDataSingle)
                                            print("added")
                                            DispatchQueue.main.async {
                                                self.collectionView.reloadData()
                                            }
                                        }
                                    }
                                    
                                    if (counterObserver > 0 && counterObserver == serverResponse.count) {
                                        DispatchQueue.main.async {
                                            self.collectionView.reloadData()
                                        }
                                        if let staffId = self.staffUniqueID, let reason = self.actionStatusForView, reason == "editStaff" {
                                            self.handleGettingAllShopService(UniqueShopID: shop, staffID: staffId)
                                        }
                                    }
                                }
                                
                            } catch _ {
                                if let statusCode = response.response?.statusCode {
                                    switch statusCode {
                                    case 404:
                                        self.view.makeToast(NSLocalizedString("AddBarberViewControllerErrorMessageShopServiceNotFound", comment: "Shop serices not found"), duration: 2.0, position: .bottom)
                                    case 500:
                                        self.view.makeToast(NSLocalizedString("AddBarberViewControllerErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                    default:
                                        print("sky high get shop service", statusCode)
                                    }
                                }
                            }
                        }
                    })
            }
            
        }
    }
    
    @objc private func handleGettingAllShopService(UniqueShopID: String, staffID: String){
        DispatchQueue.global(qos: .background).async {
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        let parameters = [
                            "staffId": staffID
                            ] as [String: Any]
                        
                        AF.request(self.BACKEND_URL + "getshopservicebystaffid/" + UniqueShopID , method: .post, parameters: parameters, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    return
                                }
                                if let jsonData = response.data {
                                    do {
                                        
                                        let shopServiceArrayData = try JSONDecoder().decode([ShopService].self, from: jsonData)
                                        
                                        for shopService in shopServiceArrayData {
                                            if let image = UIImage(named: "avatar_icon") {
                                                let serviceDataSingle = ServiceData(serviceID: shopService.id, serviceTitle: shopService.serviceTitle, serviceEstimatedTime: shopService.serviceEstimatedTotalTime, serviceCost: shopService.servicePrice, shortDescription: shopService.serviceDescription, category: shopService.serviceCategory, dateCreated: shopService.serviceDateCreated, timezone: shopService.serviceDateCreatedTimezone, calendar: shopService.serviceDateCreatedCalendar, local: shopService.serviceDateCreatedLocale, serviceImage: image, isHiddenByShop: shopService.serviceHiddenByShop, isHiddenByAdmin: shopService.serviceHiddenByAdmin, numberOfCustomer: 0, serviceStaff: shopService.serviceStaff, isAssigned: "NO", shop: shopService.serviceShop)
                                                self.shopServicesAvailableByStaff.append(serviceDataSingle)
                                            }
                                            
                                            if (self.shopServicesAvailableByStaff.count == shopServiceArrayData.count) {
                                                for dataCheck in self.shopServicesAvailableByStaff {
                                                    if let firstServiceIndex = self.shopServicesAvailable.firstIndex(where: {$0.serviceID == dataCheck.serviceID}) {
                                                        self.shopServicesAvailable[firstServiceIndex].isAssigned = "YES"
                                                        if !self.selectedServices.contains(dataCheck.serviceTitle) {
                                                            self.selectedServices.append(dataCheck.serviceTitle)
                                                        }
                                                        
                                                        if !self.selectedServiceID.contains(dataCheck.serviceID) {
                                                            self.selectedServiceID.append(dataCheck.serviceID)
                                                        }
                                                        DispatchQueue.main.async {
                                                            self.collectionView.reloadData()
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        
                                        DispatchQueue.main.async {
                                            self.collectionView.reloadData()
                                        }
                                        
                                    } catch _ {
                                        
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                print("Shops not found")
                                            case 500:
                                                print("An error occurred, please try again later")
                                            default:
                                                print("sky high get shop service by staff", statusCode)
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
    }
    
    @objc func handleCreatingAStaff(token: String){
        print("hello")
        if let shop = self.currentShopID, let customerID = self.StaffcustomerID, let stat = self.staffHiddenStatus {
            print("mango rain")
            SwiftSpinner.show("Loading...")
            DispatchQueue.global(qos: .background).async {
                
                
                let userTimezone = DateByUserDeviceInitializer.tzone
                let userCalender = "gregorian"
                let locale = "en"
                let regionData = DateByUserDeviceInitializer.getRegion(TZoneName: userTimezone, calenName: userCalender, LocName: locale)
                let newDate = DateInRegion().convertTo(region: regionData).toFormat("yyyy-MM-dd HH:mm:ss")
                
                let headers: HTTPHeaders = [
                    "authorization": token
                ]
                
                let parameters: Parameters = [
                    "customerId": customerID,
                    "dateCreated": newDate,
                    "dateCreatedTimezone": userTimezone,
                    "dateCreatedCalendar": userCalender,
                    "dateCreatedLocale": locale,
                    "staffService": self.selectedServiceID,
                    "staffHiddenByAdmin": self.isStaffDeleted,
                    "staffHiddenShop": stat,
                    "shop": shop
                ]
                
                AF.request(self.BACKEND_URL + "createstaff", method: .post, parameters: parameters, encoding: JSONEncoding(options: []), headers: headers)
                    .validate(statusCode: 200..<500)
                    .validate(contentType: ["application/json"])
                    .response (completionHandler: { (response) in
                        
                        if let error = response.error {
                            DispatchQueue.main.async {
                                SwiftSpinner.hide()
                            }
                            print(error.localizedDescription)
                            return
                        }
                        DispatchQueue.main.async {
                            SwiftSpinner.hide()
                        }
                        
                        if let jsonData = response.data {
                            do {
                                
                                let serverResponse = try JSONDecoder().decode(ServerMessage.self, from: jsonData)
                                let message = serverResponse.message
                                self.view.makeToast(message, duration: 3.0, position: .bottom)
                                self.moveToNextView(shopId: shop)
                                
                            } catch _ {
                                if let statusCode = response.response?.statusCode {
                                    switch statusCode {
                                    case 404:
                                        self.view.makeToast(NSLocalizedString("AddBarberViewControllerErrorMessageStaff", comment: "Staff and Service failed to update"), duration: 2.0, position: .bottom)
                                    case 500:
                                        self.view.makeToast(NSLocalizedString("AddBarberViewControllerErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                    default:
                                        print("sky high")
                                    }
                                }
                            }
                        }
                    })
            }
        }
    }
    
    @objc private func handleUpdatingStaff(){
        
    }
    
    @objc private func handleHidingStaff(_ sender: UISwitch){
        switch sender.isOn {
        case true:
            self.hideButtonTitleLabel.text = NSLocalizedString("AddBarberViewControllerActiveButtonSwitcher", comment: "active")
            self.staffHiddenStatus = "NO"
            break
        case false:
            self.hideButtonTitleLabel.text = NSLocalizedString("AddBarberViewControllerInactiveButtonSwitcher", comment: "inactive")
            self.staffHiddenStatus = "YES"
            break
        }
    }
    
    @objc private func handleDeletingStaff(_ sender: UISwitch){
        switch sender.isOn {
        case true:
            self.deleteButtonTitleLabel.text = NSLocalizedString("AddBarberViewControllerDeleteSwitcher", comment: "delete")
            self.isStaffDeleted = "YES"
            break
        case false:
            self.deleteButtonTitleLabel.text = NSLocalizedString("AddBarberViewControllerSavedSwitcher", comment: "saved")
            self.isStaffDeleted = "NO"
            break
        }
    }
    
    @objc func getStaffAndAssociatedServices(token: String) {}
    
    var uniqueICustomerIDContainerViewHeightAnchor: NSLayoutConstraint?
    
    func setupViewObjectsConstraints(uniqueHeight: CGFloat, hideHeight: CGFloat){
        var topDistance: CGFloat = 20
        var bottomDistance: CGFloat = -10
        if UIDevice.current.hasNotch {
            topDistance = 60
            bottomDistance = -24
            
        }
        
        addBarberViewCloseViewButton.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        addBarberViewCloseViewButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -15).isActive = true
        addBarberViewCloseViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        addBarberViewCloseViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperInputsContainerView.topAnchor.constraint(equalTo: addBarberViewCloseViewButton.bottomAnchor, constant: 5).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(addBarberViewViewIcon)
        upperInputsContainerView.addSubview(addBarberViewPlaceHolder)
        upperInputsContainerView.addSubview(saveAndUpdateOrCreateButton)
        
        addBarberViewViewIcon.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        addBarberViewViewIcon.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor).isActive = true
        addBarberViewViewIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        addBarberViewViewIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        saveAndUpdateOrCreateButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        saveAndUpdateOrCreateButton.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        saveAndUpdateOrCreateButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        saveAndUpdateOrCreateButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        addBarberViewPlaceHolder.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        addBarberViewPlaceHolder.leftAnchor.constraint(equalTo: addBarberViewViewIcon.rightAnchor, constant: 10).isActive = true
        addBarberViewPlaceHolder.rightAnchor.constraint(equalTo: saveAndUpdateOrCreateButton.leftAnchor).isActive = true
        addBarberViewPlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        addBarberViewHideStaffButton.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 5).isActive = true
        addBarberViewHideStaffButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -12).isActive = true
        addBarberViewHideStaffButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        addBarberViewHideStaffButton.heightAnchor.constraint(equalToConstant: hideHeight).isActive = true
        
        hideButtonTitleLabel.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 5).isActive = true
        hideButtonTitleLabel.rightAnchor.constraint(equalTo: addBarberViewHideStaffButton.leftAnchor, constant: -10).isActive = true
        hideButtonTitleLabel.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.25).isActive = true
        hideButtonTitleLabel.heightAnchor.constraint(equalToConstant: hideHeight).isActive = true
        
        addBarberViewDeleteStaffButton.topAnchor.constraint(equalTo: addBarberViewHideStaffButton.bottomAnchor, constant: 5).isActive = true
        addBarberViewDeleteStaffButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -12).isActive = true
        addBarberViewDeleteStaffButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        addBarberViewDeleteStaffButton.heightAnchor.constraint(equalToConstant: hideHeight).isActive = true
        
        deleteButtonTitleLabel.topAnchor.constraint(equalTo: hideButtonTitleLabel.bottomAnchor, constant: 5).isActive = true
        deleteButtonTitleLabel.rightAnchor.constraint(equalTo: addBarberViewDeleteStaffButton.leftAnchor, constant: -10).isActive = true
        deleteButtonTitleLabel.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.25).isActive = true
        deleteButtonTitleLabel.heightAnchor.constraint(equalToConstant: hideHeight).isActive = true
        
        //here
        uniqueICustomerIDContainerView.topAnchor.constraint(equalTo: addBarberViewDeleteStaffButton.bottomAnchor, constant: 5).isActive = true
        uniqueICustomerIDContainerView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -12).isActive = true
        uniqueICustomerIDContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5, constant: -24).isActive = true
        uniqueICustomerIDContainerView.heightAnchor.constraint(equalToConstant: uniqueHeight).isActive = true
        
        uniqueICustomerIDContainerView.addSubview(uniqueIDCheckButton)
        uniqueICustomerIDContainerView.addSubview(uniqueIDHiddenPlaceHolder)
        uniqueICustomerIDContainerView.addSubview(uniqueIDTextField)
        uniqueICustomerIDContainerView.addSubview(uniqueIDSeperatorView)
        
        uniqueIDCheckButton.bottomAnchor.constraint(equalTo: uniqueICustomerIDContainerView.bottomAnchor).isActive = true
        uniqueIDCheckButton.rightAnchor.constraint(equalTo: uniqueICustomerIDContainerView.rightAnchor).isActive = true
        uniqueIDCheckButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        uniqueIDCheckButton.heightAnchor.constraint(equalTo: uniqueICustomerIDContainerView.heightAnchor).isActive = true
        
        uniqueIDSeperatorView.bottomAnchor.constraint(equalTo: uniqueICustomerIDContainerView.bottomAnchor).isActive = true
        uniqueIDSeperatorView.rightAnchor.constraint(equalTo: uniqueIDCheckButton.leftAnchor, constant: -5).isActive = true
        uniqueIDSeperatorView.leftAnchor.constraint(equalTo: uniqueICustomerIDContainerView.leftAnchor).isActive = true
        uniqueIDSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        uniqueIDTextField.bottomAnchor.constraint(equalTo: uniqueIDSeperatorView.bottomAnchor).isActive = true
        uniqueIDTextField.leftAnchor.constraint(equalTo: uniqueICustomerIDContainerView.leftAnchor).isActive = true
        uniqueIDTextField.rightAnchor.constraint(equalTo: uniqueIDCheckButton.leftAnchor, constant: -5).isActive = true
        uniqueIDTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        uniqueIDHiddenPlaceHolder.bottomAnchor.constraint(equalTo: uniqueIDTextField.topAnchor).isActive = true
        uniqueIDHiddenPlaceHolder.leftAnchor.constraint(equalTo: uniqueICustomerIDContainerView.leftAnchor).isActive = true
        uniqueIDHiddenPlaceHolder.rightAnchor.constraint(equalTo: uniqueIDCheckButton.leftAnchor, constant: -5).isActive = true
        uniqueIDHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        //here
        imageAndFirstLastContainerView.topAnchor.constraint(equalTo: uniqueICustomerIDContainerView.bottomAnchor, constant: 5).isActive = true
        imageAndFirstLastContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageAndFirstLastContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        imageAndFirstLastContainerView.heightAnchor.constraint(equalToConstant: 115).isActive = true
        
        imageAndFirstLastContainerView.addSubview(selectImageView)
        imageAndFirstLastContainerView.addSubview(firstNameLastNameContainerView)
        
        selectImageView.topAnchor.constraint(equalTo: imageAndFirstLastContainerView.topAnchor).isActive = true
        selectImageView.leftAnchor.constraint(equalTo: imageAndFirstLastContainerView.leftAnchor).isActive = true
        selectImageView.widthAnchor.constraint(equalTo: imageAndFirstLastContainerView.widthAnchor, multiplier: 0.25).isActive = true
        selectImageView.heightAnchor.constraint(equalTo: imageAndFirstLastContainerView.heightAnchor).isActive = true
        
        firstNameLastNameContainerView.topAnchor.constraint(equalTo: imageAndFirstLastContainerView.topAnchor).isActive = true
        firstNameLastNameContainerView.leftAnchor.constraint(equalTo: selectImageView.rightAnchor, constant: 10).isActive = true
        firstNameLastNameContainerView.rightAnchor.constraint(equalTo: imageAndFirstLastContainerView.rightAnchor).isActive = true
        firstNameLastNameContainerView.heightAnchor.constraint(equalTo: imageAndFirstLastContainerView.heightAnchor).isActive = true
        
        firstNameLastNameContainerView.addSubview(firstNameHiddenPlaceHolder)
        firstNameLastNameContainerView.addSubview(firstNameTextField)
        firstNameLastNameContainerView.addSubview(firstNameSeperatorView)
        firstNameLastNameContainerView.addSubview(lastNameHiddenPlaceHolder)
        firstNameLastNameContainerView.addSubview(lastNameTextField)
        firstNameLastNameContainerView.addSubview(lastNameSeperatorView)
        firstNameLastNameContainerView.addSubview(emailHiddenPlaceHolder)
        firstNameLastNameContainerView.addSubview(emailTextField)
        firstNameLastNameContainerView.addSubview(emailSeperatorView)
        
        firstNameHiddenPlaceHolder.topAnchor.constraint(equalTo: firstNameLastNameContainerView.topAnchor).isActive = true
        firstNameHiddenPlaceHolder.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        firstNameHiddenPlaceHolder.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        firstNameHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        firstNameTextField.topAnchor.constraint(equalTo: firstNameHiddenPlaceHolder.bottomAnchor).isActive = true
        firstNameTextField.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        firstNameTextField.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        firstNameTextField.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        firstNameSeperatorView.leftAnchor.constraint(equalTo: firstNameLastNameContainerView.leftAnchor).isActive = true
        firstNameSeperatorView.topAnchor.constraint(equalTo: firstNameTextField.bottomAnchor).isActive = true
        firstNameSeperatorView.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        firstNameSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        lastNameHiddenPlaceHolder.topAnchor.constraint(equalTo: firstNameSeperatorView.bottomAnchor).isActive = true
        lastNameHiddenPlaceHolder.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        lastNameHiddenPlaceHolder.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        lastNameHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        lastNameTextField.topAnchor.constraint(equalTo: lastNameHiddenPlaceHolder.bottomAnchor).isActive = true
        lastNameTextField.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        lastNameTextField.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        lastNameTextField.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        lastNameSeperatorView.leftAnchor.constraint(equalTo: firstNameLastNameContainerView.leftAnchor).isActive = true
        lastNameSeperatorView.topAnchor.constraint(equalTo: lastNameTextField.bottomAnchor).isActive = true
        lastNameSeperatorView.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        lastNameSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        emailHiddenPlaceHolder.topAnchor.constraint(equalTo: lastNameSeperatorView.bottomAnchor).isActive = true
        emailHiddenPlaceHolder.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        emailHiddenPlaceHolder.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        emailHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        emailTextField.topAnchor.constraint(equalTo: emailHiddenPlaceHolder.bottomAnchor).isActive = true
        emailTextField.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        emailSeperatorView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor).isActive = true
        emailSeperatorView.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        emailSeperatorView.widthAnchor.constraint(equalTo: emailTextField.widthAnchor).isActive = true
        emailSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        
        offeredServiceTextLabel.topAnchor.constraint(equalTo: firstNameLastNameContainerView.bottomAnchor, constant: 5).isActive = true
        offeredServiceTextLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        offeredServiceTextLabel.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        offeredServiceTextLabel.heightAnchor.constraint(equalToConstant: 25)
        
        curvedCollectView.topAnchor.constraint(equalTo: offeredServiceTextLabel.bottomAnchor, constant: 5).isActive = true
        curvedCollectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        curvedCollectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -24).isActive = true
        //curvedCollectView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        curvedCollectView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: bottomDistance).isActive = true
        
        curvedCollectView.addSubview(collectView)
        
        collectView.topAnchor.constraint(equalTo: curvedCollectView.topAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: curvedCollectView.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: curvedCollectView.widthAnchor, multiplier: 1, constant: -10).isActive = true
        collectView.heightAnchor.constraint(equalTo: curvedCollectView.heightAnchor, constant: -10).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
    }
    
    func hideKeyboard(){
        view.endEditing(true)
    }
    
    func moveToNextView(shopId: String){
        
        if let status = self.actionStatusForView {
            switch (status) {
            case "editStaff":
                let someDict = ["shopUniqueId" : shopId ]
                NotificationCenter.default.post(name: .didReceiveDataStaffAndServices, object: nil, userInfo: someDict)
                self.dismiss(animated: true, completion: nil)
                break
            case "createShop":
                self.dismiss(animated: true) {
                    let someDict = ["shopUniqueId" : "id" ]
                    NotificationCenter.default.post(name: .didReceiveDataCustomerProfileViewRefresher, object: nil, userInfo: someDict)
                }
                break
            case "addStaff":
                let someDict = ["shopUniqueId" : shopId ]
                NotificationCenter.default.post(name: .didReceiveDataStaffAndServices, object: nil, userInfo: someDict)
                self.dismiss(animated: true, completion: nil)
                break
            default:
                break
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @objc func uniqueIdtextFieldDidChange(){
        self.firstNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.lastNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.emailSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.uniqueIDSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = true
        self.uniqueIDHiddenPlaceHolder.isHidden = false
        
    }
    
    @objc func firstNametextFieldDidChange(){
        self.firstNameSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.lastNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.emailSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.uniqueIDSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.firstNameHiddenPlaceHolder.isHidden = false
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = true
        self.uniqueIDHiddenPlaceHolder.isHidden = true
    }
    
    @objc func lastNametextFieldDidChange(){
        self.lastNameSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.firstNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.emailSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.uniqueIDSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = false
        self.emailHiddenPlaceHolder.isHidden = true
        self.uniqueIDHiddenPlaceHolder.isHidden = true
    }
    
    @objc func emailtextFieldDidChange(){
        self.emailSeperatorView.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        self.firstNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.lastNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.uniqueIDSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = false
        self.uniqueIDHiddenPlaceHolder.isHidden = true
    }
    
    

}

class customCollectionViewCellBarbers: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let thumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.white
        tniv.contentMode = .scaleAspectFit
        tniv.image = UIImage(named: "check_box_inactive")
        return tniv
    }()
    
    let servicesAvailable: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    func setupViews(){
        addSubview(servicesAvailable)
        addSubview(thumbnailImageView)
        addSubview(seperatorView)
        
        backgroundColor = UIColor.white
        layer.masksToBounds = true
        layer.cornerRadius = 5
        
        addContraintsWithFormat(format: "H:|-16-[v0]-16-[v1(20)]-16-|", views: servicesAvailable, thumbnailImageView)
        addContraintsWithFormat(format: "V:|-16-[v0(20)]-16-[v1(1)]|", views: servicesAvailable,seperatorView)
        addContraintsWithFormat(format: "V:|-16-[v0(20)]-16-[v1(1)]|", views: thumbnailImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

extension AddBarberViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.shopServicesAvailable.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customCollectionViewCellBarbers
        cell.servicesAvailable.text = self.shopServicesAvailable[indexPath.row].serviceTitle
        if self.selectedServices.contains(shopServicesAvailable[indexPath.row].serviceTitle) && self.selectedServiceID.contains(shopServicesAvailable[indexPath.row].serviceID){
            collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .bottom)
            cell.thumbnailImageView.image = UIImage(named: "check_box_active")
        } else {
            collectionView.deselectItem(at: indexPath, animated: false)
            cell.thumbnailImageView.image = UIImage(named: "check_box_inactive")
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if !self.selectedServices.contains(shopServicesAvailable[indexPath.row].serviceTitle) {
            self.selectedServices.append(shopServicesAvailable[indexPath.row].serviceTitle)
        }
        
        if !self.selectedServiceID.contains(shopServicesAvailable[indexPath.row].serviceID) {
            self.selectedServiceID.append(shopServicesAvailable[indexPath.row].serviceID)
        }
        
        guard let selectedCell = collectionView.cellForItem(at: indexPath) as? customCollectionViewCellBarbers else {
            print("failed")
            return
        }
        selectedCell.thumbnailImageView.image = UIImage(named: "check_box_active")
        print(self.selectedServiceID.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        let selSer =  shopServicesAvailable[indexPath.row].serviceTitle
        let selSerID =  shopServicesAvailable[indexPath.row].serviceID
        
        if self.selectedServices.count >= 1 && self.selectedServiceID.count >= 1{
            selectedServices = selectedServices.filter { $0 != selSer }
            selectedServiceID = selectedServiceID.filter { $0 != selSerID }
        }else {
            print("out of range")
        }
        
        guard let selectedCell = collectionView.cellForItem(at: indexPath) as? customCollectionViewCellBarbers else {
            print("failed")
            return
        }
        selectedCell.thumbnailImageView.image = UIImage(named: "check_box_inactive")
        print(self.selectedServiceID.count)
    }
}
