//
//  ServerMessage.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 30/01/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation

struct ServerMessage: Codable {
    let message: String
}
