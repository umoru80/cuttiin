//
//  UnpaidAppointment.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 21/04/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit

struct UnpaidAppointment: Codable {
    let appointmentCancelled: [AppointmentCancelled]
    let appointment: [Appointment]
    let shop: [Shop]
}
