//
//  DesignableSlider.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 30/03/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit

class DesignableSlider: UISlider {
    
    var thumbImage: UIImage? {
        didSet {
            setThumbImage(thumbImage, for: .normal)
        }
    }
}
