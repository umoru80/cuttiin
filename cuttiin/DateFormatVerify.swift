//
//  DateFormatVerify.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 02/11/2018.
//  Copyright © 2018 teckdk. All rights reserved.
//

import UIKit
import SwiftDate

class DateFormatVerify {
    static func checkFormat(dateString: String, RegionData: Region) -> DateInRegion?  {
        if let custom = dateString.toDate("yyyy-MM-dd HH:mm:ss", region: RegionData) {
            return custom
        } else if let extended = dateString.toDate(style: .extended, region: RegionData) {
            return extended
        } else {
            return nil
        }
    }
}
