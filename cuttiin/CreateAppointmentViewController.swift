//
//  CreateAppointmentViewController.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 25/02/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit
import SwiftDate
import Alamofire
import FBSDKLoginKit
import SwiftSpinner

class CreateAppointmentViewController: UIViewController, UITextFieldDelegate {
    var numberOfCustomer = ["1", "2", "3", "4", "5"]
    var shopID: String?
    var shopPreDeterminedCurrency: String?
    var numberOfCustomerSelected: String?
    var selectedDateString: String?
    var selectService = [ServiceData]()
    var selectedStaff =  [StaffData]()
    var selectedAppointmentTime: String?
    var shopTimeZone: String?
    var shopCalendar: String?
    var shopLocale: String?
    var selectedServicesID = [String]()
    var totalServicePrice: String?
    var totalServiceEstimatedTime: String?
    var mainWorkHourDataArray = [WorkhourData]()
    var maxNummberOfPeople: Int?
    var serviceParamArray = [Parameters]()
    var shopOwnerCustomerUiqueID: String?
    var barberShopsViewController = BarberShopsViewController()
    
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    lazy var createAppointmentViewIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8createappointment")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var closeViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(exitThisView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let viewTitleNameLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("createAppointmentViewControllerTitle", comment: "Create an appointment")
        ht.font = UIFont(name: "OpenSans-SemiBold", size: 17)
        ht.textColor = UIColor.black
        ht.textAlignment = .left
        return ht
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    
    
    let inputViewContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let shopHeaderDetailsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var shopLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 25
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    lazy var shopNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        fnhp.textColor = UIColor.black
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var shopAddressPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 14)
        fnhp.textColor = UIColor.black
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var selectServiceButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        fbutton.setTitle(NSLocalizedString("createAppointmentViewControllerTitleSelectService", comment: "Select a service - Step:2"), for: .normal)
        fbutton.setTitleColor(UIColor.black, for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 15)
        fbutton.layer.cornerRadius = 5
        fbutton.layer.masksToBounds = true
        fbutton.addTarget(self, action: #selector(handleShowServiceCollection), for: .touchUpInside)
        return fbutton
    }()
    
    lazy var selectStaffButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        fbutton.setTitle(NSLocalizedString("createAppointmentViewControllerTitleSelectStaff", comment: "Select a staff - Step:1"), for: .normal)
        fbutton.setTitleColor(UIColor.black, for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 15)
        fbutton.layer.cornerRadius = 5
        fbutton.layer.masksToBounds = true
        fbutton.addTarget(self, action: #selector(handleShowStaffCollection), for: .touchUpInside)
        return fbutton
    }()
    
    let numberOfCustomerAndPriceContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let numberOfCustomerTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.placeholder = NSLocalizedString("createAppointmentViewControllerTitleSelectNumberOfPeople", comment: "Number of people")
        em.textAlignment = .center
        em.layer.cornerRadius = 5
        em.layer.masksToBounds = true
        em.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(setFirstNumberSelector), for: .editingDidBegin)
        return em
    }()
    
    let numberOfCustomerPicker: UIPickerView = {
        let gender = UIPickerView()
        gender.translatesAutoresizingMaskIntoConstraints = false
        return gender
    }()
    
    let appointmentPriceTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        em.keyboardType = .decimalPad
        em.placeholder = NSLocalizedString("createAppointmentViewControllerTitlePrice", comment: "Price")
        em.textAlignment = .center
        em.layer.masksToBounds = true
        em.layer.cornerRadius = 5
        em.isUserInteractionEnabled = false
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let appointmentDateTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        em.placeholder = NSLocalizedString("createAppointmentViewControllerTitleSelectDate", comment: "Appointment Date - Step:4")
        em.textAlignment = .center
        em.layer.cornerRadius = 5
        em.layer.masksToBounds = true
        em.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(setFirstDate), for: .editingDidBegin)
        return em
    }()
    
    let appointmentDateDatePicker: UIDatePicker = {
        let opentime = UIDatePicker()
        opentime.translatesAutoresizingMaskIntoConstraints = false
        opentime.datePickerMode = .date
        let currentDate = Date()
        opentime.date = currentDate
        opentime.minimumDate = currentDate
        opentime.addTarget(self, action: #selector(dateChanged(datePicker:)), for: .valueChanged)
        return opentime
    }()
    
    lazy var selectAppointmentTimeButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        fbutton.setTitle(NSLocalizedString("createAppointmentViewControllerTitleSelectTime", comment: "Appointment time - Step:5"), for: .normal)
        fbutton.setTitleColor(UIColor.black, for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 15)
        fbutton.layer.cornerRadius = 5
        fbutton.layer.masksToBounds = true
        fbutton.addTarget(self, action: #selector(handleShowAppointmentTimeCollection), for: .touchUpInside)
        return fbutton
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        view.addSubview(inputViewContainerView)
        self.numberOfCustomerPicker.delegate = self
        self.numberOfCustomerTextField.inputView = self.numberOfCustomerPicker
        appointmentDateTextField.inputView = appointmentDateDatePicker
        setupViewConstriants()
        getShopDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    @objc func exitThisView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func showToastForSuccessfulAppointmentCreation(appointment: Appointment){
        self.view.makeToast(NSLocalizedString("createAppointmentViewControllerTitleCreatedSuccess", comment: "Appointment created sucessfully"), duration: 3.0, position: .bottom)
        self.dismiss(animated: true) {
            let appointmentDetail = OrderDetailCustomerViewController()
            appointmentDetail.appointmentID = appointment.id
            appointmentDetail.staffID = appointment.appointmentStaffCustomer
            appointmentDetail.customerID = appointment.appointmentCustomer
            appointmentDetail.shopID = appointment.appointmentShop
            appointmentDetail.serviceList = appointment.appointmentService
            appointmentDetail.modalPresentationStyle = .overCurrentContext
            self.barberShopsViewController.present(appointmentDetail, animated: true, completion: nil)
        }
        print("activites came through")
    }
    
    
    @objc func handleShowServiceCollection(){
        guard let curr = self.shopPreDeterminedCurrency, let shopId = self.shopID, let selSt = self.selectedStaff[safe: 0]?.staffID else {
            self.view.makeToast(NSLocalizedString("createAppointmentViewControllerTitleSelectStaff", comment: "Please select a staff"), duration: 2.0, position: .center)
            return
        }
        
        let selStaff = selSt
        let modalViewController = PopOverCollectionViewController()
        modalViewController.collectionViewSelected = 1
        modalViewController.shopCurrency = curr
        modalViewController.shopID = shopId
        modalViewController.selectedBarberOne = selStaff
        modalViewController.createAppointmentViewController = self
        modalViewController.modalPresentationStyle = .overCurrentContext
        present(modalViewController, animated: false, completion: nil)
        
    }
    
    @objc func handleShowStaffCollection(){
        guard let curr = self.shopPreDeterminedCurrency, let shopId = self.shopID else {
            return
        }
        let modalViewController = PopOverCollectionViewController()
        modalViewController.collectionViewSelected = 2
        modalViewController.shopCurrency = curr
        modalViewController.shopID = shopId
        modalViewController.createAppointmentViewController = self
        modalViewController.modalPresentationStyle = .overCurrentContext
        present(modalViewController, animated: false, completion: nil)
        
    }
    
    @objc func handleSwitchingStaff(){
        // service
        self.totalServiceEstimatedTime = ""
        self.selectedServicesID.removeAll();
        self.selectServiceButton.setTitle(NSLocalizedString("createAppointmentViewControllerTitleSelectService", comment: "Select a service - Step:2"), for: .normal)
        self.selectService.removeAll()
        self.serviceParamArray.removeAll()
        
        // number of people
        self.numberOfCustomerTextField.text = ""
        self.numberOfCustomerSelected = nil
        
        // price
        self.totalServicePrice = nil
        self.maxNummberOfPeople = 0
        self.appointmentPriceTextField.text = ""
        
        // date
        self.selectedDateString = nil
        self.appointmentDateTextField.text = ""
    }
    
    @objc func handleShowAppointmentTimeCollection(){
        if let shopID = self.shopID, let staffID = self.selectedStaff[safe: 0]?.staffID, let staffCustomerId = self.selectedStaff[safe: 0]?.staffCustomerId, let numberPeople = self.numberOfCustomerSelected, let price = self.totalServicePrice, let dateSel = self.selectedDateString, let curr = self.shopPreDeterminedCurrency, let shpTimezne = self.shopTimeZone, let shpCalendar = self.shopCalendar, let shpLocale = self.shopLocale, let tttime = self.totalServiceEstimatedTime, !self.selectedServicesID.isEmpty, !self.serviceParamArray.isEmpty, let shopIdCus = self.shopOwnerCustomerUiqueID  {
            let modalViewController = PopOverCollectionViewController()
            modalViewController.collectionViewSelected = 3
            modalViewController.shopID = shopID
            modalViewController.shopCurrency = curr
            modalViewController.staffID = staffID
            modalViewController.numberOfCustomer = numberPeople
            modalViewController.totalPrice = price
            modalViewController.dateChoseByUser = dateSel
            modalViewController.shopCalendar = shpCalendar
            modalViewController.shopTimeZone = shpTimezne
            modalViewController.shopLocal = shpLocale
            modalViewController.servicesSelectedByUser = self.selectedServicesID
            modalViewController.staffCustomerId = staffCustomerId
            modalViewController.serviceTotalTimeTaken = tttime
            modalViewController.shopServiceArrayParameter = self.serviceParamArray
            modalViewController.shopOwnerCustomerId = shopIdCus
            modalViewController.createAppointmentViewController = self
            modalViewController.modalPresentationStyle = .overCurrentContext
            present(modalViewController, animated: false, completion: nil)
            
        }
    }
    
    
    //create parameter object for each of the selected Service
    @objc func textFieldDidChange() {
        print("hello")
        if !self.selectService.isEmpty {
            var sumOfPrice = [Double]()
            var numberOfPeopleArray = [Int]()
            self.serviceParamArray.removeAll()
            for service in self.selectService {
                if let price = Double(service.serviceCost), let curr = self.shopPreDeterminedCurrency {
                    
                    let serviceParam: Parameters = [
                        "appointmentService": service.serviceID,
                        "appointmentServiceAmount": String(service.numberOfCustomer)
                    ]
                    self.serviceParamArray.append(serviceParam)
                    
                    let numberOfPeopleString = service.numberOfCustomer
                    let numOfPeopleDouble = Double(numberOfPeopleString)
                    let numOfPeopleInt = Int(numberOfPeopleString)
                    let totalPrice = numOfPeopleDouble * price
                    sumOfPrice.append(totalPrice)
                    numberOfPeopleArray.append(numOfPeopleInt)
                    
                    if (sumOfPrice.count == self.selectService.count){
                        self.appointmentPriceTextField.text = String(sumOfPrice.reduce(0, +).roundTo(places: 2)) + " " + curr
                        self.maxNummberOfPeople = numberOfPeopleArray.reduce(0, +)
                        self.totalServicePrice = String(sumOfPrice.reduce(0, +).roundTo(places: 2))
                        self.selectService.removeAll()
                    }
                }
            }
        }
    }
    
    // check if the shop opening time is accordance with user selected time
    
    @objc func dateChanged(datePicker: UIDatePicker) {
        print("red moon")
        let englishRegion = DateByUserDeviceInitializer.getRegion(TZoneName: DateByUserDeviceInitializer.tzone, calenName: "gregorian", LocName: "en_US")
        let dateSelectedEnglish = datePicker.date.convertTo(region: englishRegion)
        let dayOfTheWeek = dateSelectedEnglish.weekdayName(.default)
        
        if let firstWorkHour = self.mainWorkHourDataArray.first(where: { $0.workhourWeekDay == dayOfTheWeek}) {
            let workhourRegion = DateByUserDeviceInitializer.getRegion(TZoneName: firstWorkHour.workhourTimezone, calenName: firstWorkHour.workhourCalendar, LocName: firstWorkHour.workhourLocale)
            let currentDate = datePicker.date.convertTo(region: workhourRegion)
            
            if let openDate = firstWorkHour.workhourOpenDate.toDate("yyyy-MM-dd HH:mm:ss", region: workhourRegion) {
                
                let openDateCurrent = DateInRegion(year: currentDate.year, month: currentDate.month, day: currentDate.day, hour: openDate.hour, minute: openDate.minute, second: openDate.second, nanosecond: openDate.nanosecond, region: workhourRegion)
                self.selectedDateString = openDateCurrent.toFormat("yyyy-MM-dd HH:mm:ss")
                self.appointmentDateTextField.text = openDateCurrent.toFormat("dd / MM / yyyy")
                self.textFieldDidChange()
                
            } else {
                self.view.makeToast(NSLocalizedString("createAppointmentViewControllerTitleErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .center)
            }
        } else{
            self.view.makeToast(NSLocalizedString("createAppointmentViewControllerTitleErrorMessageErrorShopNotAvailable", comment: "This shop is not available on this day"), duration: 2.0, position: .center)
            self.appointmentDateTextField.text = ""
        }
    }
    
    @objc func setFirstDate(){
        self.dateChanged(datePicker: self.appointmentDateDatePicker)
    }
    
    @objc func setFirstNumberSelector(){
        guard let valueChosen = numberOfCustomer.first else {
            return
        }
        
        self.numberOfCustomerTextField.text = valueChosen
        self.numberOfCustomerSelected = valueChosen
    }
    
    @objc func getShopDetails(){
        DispatchQueue.global(qos: .background).async {
            
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let shopUniqueID = self.shopID {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        AF.request(self.BACKEND_URL + "getsingleshop/" + shopUniqueID, method: .post, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    print("got data back")
                                    do {
                                        let shopData = try JSONDecoder().decode(Shop.self, from: jsonData)
                                        self.shopAddressPlaceHolder.text = shopData.shopAddress
                                        self.shopNamePlaceHolder.text = shopData.shopName
                                        let profileImageFileName = shopData.shopLogoImageName
                                        self.shopID = shopData.id
                                        self.shopPreDeterminedCurrency = shopData.shopCountryCurrencyCode
                                        self.shopTimeZone = shopData.shopDateCreatedTimezone
                                        self.shopCalendar = shopData.shopDateCreatedCalendar
                                        self.shopLocale = shopData.shopDateCreatedLocale
                                        self.shopOwnerCustomerUiqueID = shopData.shopOwner
                                        self.handleGettingWorkHourSchedule()
                                        print("image name: ", profileImageFileName)
                                        AF.request(self.BACKEND_URL + "images/shopImages/" + profileImageFileName, headers: headers).responseData { response in
                                            
                                            switch response.result {
                                            case .success(let data):
                                                let image = UIImage(data: data)
                                                DispatchQueue.main.async {
                                                    self.shopLogoImageView.image = image
                                                    self.shopLogoImageView.contentMode = .scaleAspectFit
                                                }
                                                break
                                            case .failure(let error):
                                                print("error occured: " + error.localizedDescription)
                                                break
                                            }
                                            
                                        }
                                        
                                    } catch _ {
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                self.view.makeToast(NSLocalizedString("createAppointmentViewControllerTitleErrorMessageErrorShopNotFound", comment: "Shops not found"), duration: 2.0, position: .bottom)
                                            case 500:
                                                self.view.makeToast(NSLocalizedString("createAppointmentViewControllerTitleErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                            default:
                                                print("sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
    }
    
    @objc func handleGettingWorkHourSchedule(){
        DispatchQueue.global(qos: .background).async {
            
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let shopUniqueID = self.shopID {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        AF.request(self.BACKEND_URL + "shopWorkHours/" + shopUniqueID, method: .post, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    print("got work hour data back")
                                    do {
                                        let workhourResponse = try JSONDecoder().decode(WorkhourCreated.self, from: jsonData)
                                        
                                        let workTimezone = workhourResponse.workHour.workhourTimezone
                                        let workCalendar = workhourResponse.workHour.workhourCalendar
                                        let workLocale = workhourResponse.workHour.workhourLocale
                                        let workid = workhourResponse.workHour.id
                                        let workLastUpdate = workhourResponse.workHour.workhourLastUpdated
                                        let shopId = workhourResponse.workHour.shop
                                        
                                        for x in workhourResponse.workHour.workhourSchedule {
                                            
                                            let workhourRegion = DateByUserDeviceInitializer.getRegion(TZoneName: workTimezone, calenName: workCalendar, LocName: workLocale)
                                            if let lastUpated = workhourResponse.workHour.workhourLastUpdated.toDate("yyyy-MM-dd HH:mm:ss", region: workhourRegion) {
                                                
                                                let workhourdata = WorkhourData(id: workid, workhourCloseDate: x.workhourCloseDate, workhourOpenDate: x.workhourOpenDate, workhourWeekDay: x.workhourWeekDay, workhourLastUpdated: workLastUpdate, workhourLastUpdatedDateObj: lastUpated, workhourCalendar: workCalendar, workhourLocale: workLocale, workhourTimezone: workTimezone, shopId: shopId)
                                                self.mainWorkHourDataArray.append(workhourdata)
                                                
                                            }
                                        }
                                        
                                    } catch _ {
                                        
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                self.view.makeToast(NSLocalizedString("createAppointmentViewControllerTitleErrorMessageErrorWorkHourNotFound", comment: "Workhour not found"), duration: 2.0, position: .bottom)
                                            case 500:
                                                self.view.makeToast(NSLocalizedString("createAppointmentViewControllerTitleErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                            default:
                                                print("sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
    }
    
    @objc func handleUpdateUserAuthData(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let customerId = decodedCustomer.first?.customerId, let shopOwner = decodedCustomer.first?.shopOwner, let timeZone = decodedCustomer.first?.timezone, let authType = decodedCustomer.first?.authType, let email = decodedCustomer.first?.email, let tokenData = decodedCustomer.first?.token, let autoSwitch = decodedCustomer.first?.autoShopSwitch {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let expiredTime = DateInRegion().convertTo(region: region) - 1.seconds
                let expiredTimeString = expiredTime.toFormat("yyyy-MM-dd HH:mm:ss")
                
                let teams = [CustomerToken(expiresIn: expiredTimeString, customerId: customerId, token: tokenData, timezone: timeZone, shopOwner: shopOwner, authType: authType, email: email, autoShopSwitch: autoSwitch)]
                
                let userDefaults = UserDefaults.standard
                let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: teams)
                userDefaults.set(encodedData, forKey: "token")
                userDefaults.synchronize()
                LoginManager().logOut()
                let welcomeviewcontroller = WelcomeViewController()
                self.present(welcomeviewcontroller, animated: true, completion: nil)
                
            }
            
        }
    }
    
    @objc func setupViewConstriants(){
        var topDistance: CGFloat = 20
        if UIDevice.current.hasNotch {
            topDistance = 60
        }
        
        upperInputsContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(createAppointmentViewIcon)
        upperInputsContainerView.addSubview(viewTitleNameLabel)
        upperInputsContainerView.addSubview(closeViewButton)
        
        createAppointmentViewIcon.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        createAppointmentViewIcon.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor).isActive = true
        createAppointmentViewIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        createAppointmentViewIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        closeViewButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        closeViewButton.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        closeViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        closeViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        viewTitleNameLabel.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        viewTitleNameLabel.leftAnchor.constraint(equalTo: createAppointmentViewIcon.rightAnchor, constant: 10).isActive = true
        viewTitleNameLabel.rightAnchor.constraint(equalTo: closeViewButton.leftAnchor).isActive = true
        viewTitleNameLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        
        inputViewContainerView.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 5).isActive = true
        inputViewContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        inputViewContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        inputViewContainerView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        
        inputViewContainerView.addSubview(shopHeaderDetailsContainerView)
        inputViewContainerView.addSubview(selectServiceButton)
        inputViewContainerView.addSubview(selectStaffButton)
        inputViewContainerView.addSubview(numberOfCustomerAndPriceContainerView)
        inputViewContainerView.addSubview(appointmentDateTextField)
        inputViewContainerView.addSubview(selectAppointmentTimeButton)
        
        shopHeaderDetailsContainerView.topAnchor.constraint(equalTo: inputViewContainerView.topAnchor).isActive = true
        shopHeaderDetailsContainerView.centerXAnchor.constraint(equalTo: inputViewContainerView.centerXAnchor).isActive = true
        shopHeaderDetailsContainerView.widthAnchor.constraint(equalTo: inputViewContainerView.widthAnchor).isActive = true
        shopHeaderDetailsContainerView.heightAnchor.constraint(equalToConstant: 90).isActive = true
        
        shopHeaderDetailsContainerView.addSubview(shopLogoImageView)
        shopHeaderDetailsContainerView.addSubview(shopNamePlaceHolder)
        shopHeaderDetailsContainerView.addSubview(shopAddressPlaceHolder)
        
        shopLogoImageView.topAnchor.constraint(equalTo: shopHeaderDetailsContainerView.topAnchor).isActive = true
        shopLogoImageView.leftAnchor.constraint(equalTo: shopHeaderDetailsContainerView.leftAnchor).isActive = true
        shopLogoImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        shopLogoImageView.bottomAnchor.constraint(equalTo: shopHeaderDetailsContainerView.bottomAnchor).isActive = true
        
        shopNamePlaceHolder.topAnchor.constraint(equalTo: shopHeaderDetailsContainerView.topAnchor).isActive = true
        shopNamePlaceHolder.rightAnchor.constraint(equalTo: shopHeaderDetailsContainerView.rightAnchor).isActive = true
        shopNamePlaceHolder.leftAnchor.constraint(equalTo: shopLogoImageView.rightAnchor, constant: 5).isActive = true
        shopNamePlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        shopAddressPlaceHolder.topAnchor.constraint(equalTo: shopNamePlaceHolder.bottomAnchor).isActive = true
        shopAddressPlaceHolder.rightAnchor.constraint(equalTo: shopHeaderDetailsContainerView.rightAnchor).isActive = true
        shopAddressPlaceHolder.leftAnchor.constraint(equalTo: shopLogoImageView.rightAnchor, constant: 5).isActive = true
        shopAddressPlaceHolder.heightAnchor.constraint(equalToConstant: 65).isActive = true
        
        //
        
        selectStaffButton.topAnchor.constraint(equalTo: shopHeaderDetailsContainerView.bottomAnchor, constant: 10).isActive = true
        selectStaffButton.centerXAnchor.constraint(equalTo: inputViewContainerView.centerXAnchor).isActive = true
        selectStaffButton.widthAnchor.constraint(equalTo: inputViewContainerView.widthAnchor).isActive = true
        selectStaffButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        selectServiceButton.topAnchor.constraint(equalTo: selectStaffButton.bottomAnchor, constant: 10).isActive = true
        selectServiceButton.centerXAnchor.constraint(equalTo: inputViewContainerView.centerXAnchor).isActive = true
        selectServiceButton.widthAnchor.constraint(equalTo: inputViewContainerView.widthAnchor).isActive = true
        selectServiceButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        //
        
        numberOfCustomerAndPriceContainerView.topAnchor.constraint(equalTo: selectServiceButton.bottomAnchor, constant: 10).isActive = true
        numberOfCustomerAndPriceContainerView.centerXAnchor.constraint(equalTo: inputViewContainerView.centerXAnchor).isActive = true
        numberOfCustomerAndPriceContainerView.widthAnchor.constraint(equalTo: inputViewContainerView.widthAnchor).isActive = true
        numberOfCustomerAndPriceContainerView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        numberOfCustomerAndPriceContainerView.addSubview(numberOfCustomerTextField)
        numberOfCustomerAndPriceContainerView.addSubview(appointmentPriceTextField)
        
        numberOfCustomerTextField.topAnchor.constraint(equalTo: numberOfCustomerAndPriceContainerView.topAnchor).isActive = true
        numberOfCustomerTextField.leftAnchor.constraint(equalTo: numberOfCustomerAndPriceContainerView.leftAnchor).isActive = true
        numberOfCustomerTextField.widthAnchor.constraint(equalTo: numberOfCustomerAndPriceContainerView.widthAnchor, multiplier: 0.5, constant: 10).isActive = true
        numberOfCustomerTextField.heightAnchor.constraint(equalTo: numberOfCustomerAndPriceContainerView.heightAnchor).isActive = true
        
        appointmentPriceTextField.topAnchor.constraint(equalTo: numberOfCustomerAndPriceContainerView.topAnchor).isActive = true
        appointmentPriceTextField.rightAnchor.constraint(equalTo: numberOfCustomerAndPriceContainerView.rightAnchor).isActive = true
        appointmentPriceTextField.leftAnchor.constraint(equalTo: numberOfCustomerTextField.rightAnchor, constant: 10).isActive = true
        appointmentPriceTextField.heightAnchor.constraint(equalTo: numberOfCustomerAndPriceContainerView.heightAnchor).isActive = true
        
        appointmentDateTextField.topAnchor.constraint(equalTo: numberOfCustomerAndPriceContainerView.bottomAnchor, constant: 10).isActive = true
        appointmentDateTextField.centerXAnchor.constraint(equalTo: inputViewContainerView.centerXAnchor).isActive = true
        appointmentDateTextField.widthAnchor.constraint(equalTo: inputViewContainerView.widthAnchor).isActive = true
        appointmentDateTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        selectAppointmentTimeButton.topAnchor.constraint(equalTo: appointmentDateTextField.bottomAnchor, constant: 10).isActive = true
        selectAppointmentTimeButton.centerXAnchor.constraint(equalTo: inputViewContainerView.centerXAnchor).isActive = true
        selectAppointmentTimeButton.widthAnchor.constraint(equalTo: inputViewContainerView.widthAnchor).isActive = true
        selectAppointmentTimeButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }


}

extension CreateAppointmentViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    //picker view functions
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.numberOfCustomer.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.numberOfCustomer[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if let maxPeople = self.maxNummberOfPeople, row + 1 <= maxPeople {
            self.numberOfCustomerSelected = self.numberOfCustomer[row]
            self.numberOfCustomerTextField.text = self.numberOfCustomerSelected
            self.textFieldDidChange()
        } else {
            self.numberOfCustomerTextField.text = "";
            self.numberOfCustomerSelected = ""
            self.view.makeToast(NSLocalizedString("createAppointmentViewControllerTitleErrorMessageErrorNumberExceed", comment: "Number of customers cant exceed number of services"), duration: 2.0, position: .center)
        }
    }
    
}
