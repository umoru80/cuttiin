//
//  ShopBillingRequest.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 30/06/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation

// MARK: - ShopBillingRequest
struct ShopBillingRequest: Codable {
    let message: String
    let shopBilling: [ShopBilling]
}

// MARK: - ShopBilling
struct ShopBilling: Codable {
    let id, customer, shop, shopCountryCurrencyCode: String
    let billingStatus, billingPaidDate, billingPaidDateTimezone, billingPaidDateCalendar: String
    let billingPaidDateLocale, appointmentNewTotalCost, appointmentNewTotalAmount, appointmentStartedTotalCost: String
    let appointmentStartedTotalAmount, appointmentEndedTotalCost, appointmentEndedTotalAmount, appointmentCancelledTotalCost: String
    let appointmentCancelledTotalAmount, appointmentPaidTotalCost, appointmentPaidTotalAmount, billingStarted: String
    let billingEnded, dateCreated, dateCreatedTimezone, dateCreatedCalendar: String
    let dateCreatedLocale: String
    let v: Int
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case customer, shop, shopCountryCurrencyCode, billingStatus, billingPaidDate, billingPaidDateTimezone, billingPaidDateCalendar, billingPaidDateLocale, appointmentNewTotalCost, appointmentNewTotalAmount, appointmentStartedTotalCost, appointmentStartedTotalAmount, appointmentEndedTotalCost, appointmentEndedTotalAmount, appointmentCancelledTotalCost, appointmentCancelledTotalAmount, appointmentPaidTotalCost, appointmentPaidTotalAmount, billingStarted, billingEnded, dateCreated, dateCreatedTimezone, dateCreatedCalendar, dateCreatedLocale
        case v = "__v"
    }
}
