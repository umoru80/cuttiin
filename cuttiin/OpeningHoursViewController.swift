//
//  OpeningHoursViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/19/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate
import Alamofire
import SwiftSpinner

class OpeningHoursViewController: UIViewController {
    var daysOfTheWeekBeforePresenting = ["Monday", "Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
    var notRegisteringANewBarberShop = true
    var currentDayWorkHour = [WorkHourButtons]()
    
    var shopID: String?
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    var currentWorkID: String?
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    lazy var OpeningHoursSettingsViewIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8shoppicker")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openHourshowShopSelectorView)))
        return imageView
    }()
    
    let OpeningHoursPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Bold", size: 20)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var saveAndUpdateDoneButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitle(NSLocalizedString("openHourViewControllerButtonDoneButton", comment: "Done"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-Bold", size: 15)
        st.isEnabled = true
        st.isUserInteractionEnabled = true
        return st
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    let InputContainerView: UIView = {
        let icview = UIView()
        icview.translatesAutoresizingMaskIntoConstraints = false
        icview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        icview.layer.masksToBounds = true
        icview.layer.cornerRadius = 5
        return icview
    }()
    
    lazy var mondayHourButton: UIButton = {
        let mbutton = UIButton()
        mbutton.translatesAutoresizingMaskIntoConstraints = false
        mbutton.backgroundColor = UIColor.white
        mbutton.setTitle(NSLocalizedString("mondayButtonOpeningHoursView", comment: "MON, CLOSED"), for: .normal)
        mbutton.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 11)
        mbutton.setTitleColor(UIColor.black, for: .normal)
        mbutton.layer.masksToBounds = true
        mbutton.layer.cornerRadius = 5
        mbutton.tag = 1
        mbutton.addTarget(self, action: #selector(handleHourSelected(_:)), for: .touchUpInside)
        return mbutton
    }()
    
    lazy var tuesdayHourButton: UIButton = {
        let tbutton = UIButton()
        tbutton.translatesAutoresizingMaskIntoConstraints = false
        tbutton.backgroundColor = UIColor.white
        tbutton.setTitle(NSLocalizedString("tuesdayButtonOpeningHoursView", comment: "TUE, CLOSED"), for: .normal)
        tbutton.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 11)
        tbutton.setTitleColor(UIColor.black, for: .normal)
        tbutton.layer.masksToBounds = true
        tbutton.layer.cornerRadius = 5
        tbutton.tag = 2
        tbutton.addTarget(self, action: #selector(handleHourSelected(_:)), for: .touchUpInside)
        return tbutton
    }()
    
    lazy var wednesdayHourButton: UIButton = {
        let wbutton = UIButton()
        wbutton.translatesAutoresizingMaskIntoConstraints = false
        wbutton.backgroundColor = UIColor.white
        wbutton.setTitle(NSLocalizedString("wednesdayButtonOpeningHoursView", comment: "WED, CLOSED"), for: .normal)
        wbutton.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 11)
        wbutton.setTitleColor(UIColor.black, for: .normal)
        wbutton.layer.masksToBounds = true
        wbutton.layer.cornerRadius = 5
        wbutton.tag = 3
        wbutton.addTarget(self, action: #selector(handleHourSelected(_:)), for: .touchUpInside)
        return wbutton
    }()
    
    lazy var thursdayHourButton: UIButton = {
        let thbutton = UIButton()
        thbutton.translatesAutoresizingMaskIntoConstraints = false
        thbutton.backgroundColor = UIColor.white
        thbutton.setTitle(NSLocalizedString("thursdayButtonOpeningHoursView", comment: "THU, CLOSED"), for: .normal)
        thbutton.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 11)
        thbutton.setTitleColor(UIColor.black, for: .normal)
        thbutton.layer.masksToBounds = true
        thbutton.layer.cornerRadius = 5
        thbutton.tag = 4
        thbutton.addTarget(self, action: #selector(handleHourSelected(_:)), for: .touchUpInside)
        return thbutton
    }()
    
    lazy var fridayHourButton: UIButton = {
        let frbutton = UIButton()
        frbutton.translatesAutoresizingMaskIntoConstraints = false
        frbutton.backgroundColor = UIColor.white
        frbutton.setTitle(NSLocalizedString("fridayButtonOpeningHoursView", comment: "FRI, CLOSED"), for: .normal)
        frbutton.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 11)
        frbutton.setTitleColor(UIColor.black, for: .normal)
        frbutton.layer.masksToBounds = true
        frbutton.layer.cornerRadius = 5
        frbutton.tag = 5
        frbutton.addTarget(self, action: #selector(handleHourSelected(_:)), for: .touchUpInside)
        return frbutton
    }()
    
    lazy var saturdayHourButton: UIButton = {
        let sabutton = UIButton()
        sabutton.translatesAutoresizingMaskIntoConstraints = false
        sabutton.backgroundColor = UIColor.white
        sabutton.setTitle(NSLocalizedString("saturdayButtonOpeningHoursView", comment: "SAT, CLOSED"), for: .normal)
        sabutton.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 11)
        sabutton.setTitleColor(UIColor.black, for: .normal)
        sabutton.layer.masksToBounds = true
        sabutton.layer.cornerRadius = 5
        sabutton.tag = 6
        sabutton.addTarget(self, action: #selector(handleHourSelected(_:)), for: .touchUpInside)
        return sabutton
    }()
    
    lazy var sundayHourButton: UIButton = {
        let sunbutton = UIButton()
        sunbutton.translatesAutoresizingMaskIntoConstraints = false
        sunbutton.backgroundColor = UIColor.white
        sunbutton.setTitle(NSLocalizedString("sundayButtonOpeningHoursView", comment: "SUN, CLOSED"), for: .normal)
        sunbutton.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 11)
        sunbutton.setTitleColor(UIColor.black, for: .normal)
        sunbutton.layer.masksToBounds = true
        sunbutton.layer.cornerRadius = 5
        sunbutton.tag = 7
        sunbutton.addTarget(self, action: #selector(handleHourSelected(_:)), for: .touchUpInside)
        return sunbutton
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        if !notRegisteringANewBarberShop {
            self.OpeningHoursPlaceHolder.text = NSLocalizedString("navigationTitleTextFirstOpeningHourView", comment: "Work hours")
            self.saveAndUpdateDoneButton.addTarget(self, action: #selector(handleDismissView), for: .touchUpInside)
        } else {
            self.OpeningHoursPlaceHolder.text = NSLocalizedString("navigationTitleTextSecondOpeningHours", comment: "Work Hours: Step 3 of 5")
            self.saveAndUpdateDoneButton.addTarget(self, action: #selector(handleDoneAction), for: .touchUpInside)
        }
        
        
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        view.addSubview(InputContainerView)
        setupViewConstraints()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .didReceiveDataOpeningHours, object: nil)
    }
    
    @objc private func onDidReceiveData(_ notification:Notification) {
        
        if let dataBack = notification.userInfo as? [String: AnyObject], let shopId = dataBack["shopUniqueId"] as? String {
            self.shopID = shopId
            UserDefaults.standard.set(self.daysOfTheWeekBeforePresenting, forKey: "workHoursDeleted")
            self.workHoursToBeDeleted()
            self.getShopWorkHours()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func handleDismissView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("pressure")
        self.navigationController?.navigationBar.isHidden = true
        UserDefaults.standard.set(self.daysOfTheWeekBeforePresenting, forKey: "workHoursDeleted")
        self.workHoursToBeDeleted()
        self.getShopWorkHours()
    }
    
    
    @objc private func openHourshowShopSelectorView(){
        
        let customerShops = CustomerShopsViewController()
        customerShops.openingHoursViewController = self
        customerShops.viewControllerWhoInitiatedAction = "openingHoursView"
        customerShops.modalPresentationStyle = .overCurrentContext
        present(customerShops, animated: true, completion: nil)
        
    }
    
    @objc func getShopWorkHours() {
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let shop = self.shopID  {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    DispatchQueue.global(qos: .background).async {
                        AF.request(self.BACKEND_URL + "shopWorkHours/" + shop, method: .post, headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"])
                            .response { (response) in
                                
                                if let jsonData = response.data {
                                    do {
                                        let serverResponse = try JSONDecoder().decode(WorkhourCreated.self, from: jsonData)
                                        DispatchQueue.main.async {
                                            self.currentWorkID = serverResponse.workHour.id
                                            for  dayWorkHour in serverResponse.workHour.workhourSchedule {
                                                let region = DateByUserDeviceInitializer.getRegion(TZoneName: serverResponse.workHour.workhourTimezone, calenName: serverResponse.workHour.workhourCalendar, LocName: serverResponse.workHour.workhourLocale)
                                                if let opendate = dayWorkHour.workhourOpenDate.toDate("yyyy-MM-dd HH:mm:ss", region: region)?.toString(DateToStringStyles.time(DateFormatter.Style.short)), let closedate = dayWorkHour.workhourCloseDate.toDate("yyyy-MM-dd HH:mm:ss", region: region)?.toString(DateToStringStyles.time(DateFormatter.Style.short)) {
                                                    let workhourbuttonHold = WorkHourButtons()
                                                    workhourbuttonHold.dayOfTheWeek = dayWorkHour.workhourWeekDay
                                                    workhourbuttonHold.openingClosingTimeString = opendate + " - " + closedate
                                                    self.navigationItem.rightBarButtonItem?.isEnabled = true
                                                    self.setupButtonValuesForWorkHour(daySchedule: workhourbuttonHold)
                                                }
                                            }
                                            
                                            //success
                                        }
                                    } catch _ {
                                        DispatchQueue.main.async {
                                            if let statusCode = response.response?.statusCode {
                                                switch statusCode {
                                                case 404:
                                                    print("No Workhours available")
                                                case 500:
                                                    self.view.makeToast(NSLocalizedString("openHourViewControllerErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                                default:
                                                    print("sky high")
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                    }
                    
                    
                    
                }
            }
        }
    }
    
    
    func workHoursToBeDeleted(){
        print("mad lion cluase")
        if let netAvail = UserDefaults.standard.object(forKey: "workHoursDeleted") as? [String]{
            for daysCho in netAvail {
                switch daysCho {
                case "Monday":
                    self.mondayHourButton.setTitle(NSLocalizedString("mondayButtonOpeningHoursView", comment: "MON, CLOSED"), for: .normal)
                    UserDefaults.standard.set("", forKey: "workHoursDeleted")
                    UserDefaults.standard.removeObject(forKey: "workHoursDeleted")
                case "Tuesday":
                    self.tuesdayHourButton.setTitle(NSLocalizedString("tuesdayButtonOpeningHoursView", comment: "TUE, CLOSED"), for: .normal)
                    UserDefaults.standard.set("", forKey: "workHoursDeleted")
                    UserDefaults.standard.removeObject(forKey: "workHoursDeleted")
                case "Wednesday":
                    self.wednesdayHourButton.setTitle(NSLocalizedString("wednesdayButtonOpeningHoursView", comment: "WED, CLOSED"), for: .normal)
                    UserDefaults.standard.set("", forKey: "workHoursDeleted")
                    UserDefaults.standard.removeObject(forKey: "workHoursDeleted")
                case "Thursday":
                    self.thursdayHourButton.setTitle(NSLocalizedString("thursdayButtonOpeningHoursView", comment: "THU, CLOSED"), for: .normal)
                    UserDefaults.standard.set("", forKey: "workHoursDeleted")
                    UserDefaults.standard.removeObject(forKey: "workHoursDeleted")
                case "Friday":
                    self.fridayHourButton.setTitle(NSLocalizedString("fridayButtonOpeningHoursView", comment: "FRI, CLOSED"), for: .normal)
                    UserDefaults.standard.set("", forKey: "workHoursDeleted")
                    UserDefaults.standard.removeObject(forKey: "workHoursDeleted")
                case "Saturday":
                    self.saturdayHourButton.setTitle(NSLocalizedString("saturdayButtonOpeningHoursView", comment: "SAT, CLOSED"), for: .normal)
                    UserDefaults.standard.set("", forKey: "workHoursDeleted")
                    UserDefaults.standard.removeObject(forKey: "workHoursDeleted")
                case "Sunday":
                    self.sundayHourButton.setTitle(NSLocalizedString("sundayButtonOpeningHoursView", comment: "SUN, CLOSED"), for: .normal)
                    UserDefaults.standard.set("", forKey: "workHoursDeleted")
                    UserDefaults.standard.removeObject(forKey: "workHoursDeleted")
                default:
                    print("Sky high")
                }
            }
        }
    }
    
    func setupButtonValuesForWorkHour(daySchedule: WorkHourButtons){
        if let day = daySchedule.dayOfTheWeek, let timeHold = daySchedule.openingClosingTimeString {
            switch day {
            case "Monday":
                self.mondayHourButton.setTitle(NSLocalizedString("mondayUpdateTextOpeningTimeView", comment: "MON, ") + timeHold, for: .normal)
            case "Tuesday":
                self.tuesdayHourButton.setTitle(NSLocalizedString("tuesdayUpdateTextOpeningTimeView", comment: "TUE, ") + timeHold, for: .normal)
            case "Wednesday":
                self.wednesdayHourButton.setTitle(NSLocalizedString("wednesdayUpdateTextOpeningTimeView", comment: "WED, ") + timeHold, for: .normal)
            case "Thursday":
                self.thursdayHourButton.setTitle(NSLocalizedString("thursdayUpdateTextOpeningTimeView", comment: "THUR, ") + timeHold, for: .normal)
            case "Friday":
                self.fridayHourButton.setTitle(NSLocalizedString("fridayUpdateTextOpeningTimeView", comment: "FRI, ") + timeHold, for: .normal)
            case "Saturday":
                self.saturdayHourButton.setTitle(NSLocalizedString("saturdayUpdateTextOpeningTimeView", comment: "SAT, ") + timeHold, for: .normal)
            case "Sunday":
                self.sundayHourButton.setTitle(NSLocalizedString("sundayUpdateTextOpeningTimeView", comment: "SUN, ") + timeHold, for: .normal)
            default:
                print("Sky high")
            }
        }
    }
    
    func setupViewConstraints(){
        var topDistance: CGFloat = 20
        if UIDevice.current.hasNotch {
            topDistance = 60
        }
        
        
        upperInputsContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(OpeningHoursSettingsViewIcon)
        upperInputsContainerView.addSubview(OpeningHoursPlaceHolder)
        upperInputsContainerView.addSubview(saveAndUpdateDoneButton)
        
        OpeningHoursSettingsViewIcon.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        OpeningHoursSettingsViewIcon.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor).isActive = true
        OpeningHoursSettingsViewIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        OpeningHoursSettingsViewIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        saveAndUpdateDoneButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        saveAndUpdateDoneButton.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        saveAndUpdateDoneButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        saveAndUpdateDoneButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        OpeningHoursPlaceHolder.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        OpeningHoursPlaceHolder.leftAnchor.constraint(equalTo: OpeningHoursSettingsViewIcon.rightAnchor, constant: 10).isActive = true
        OpeningHoursPlaceHolder.rightAnchor.constraint(equalTo: saveAndUpdateDoneButton.leftAnchor).isActive = true
        OpeningHoursPlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        //here
        
        InputContainerView.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 5).isActive = true
        InputContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        InputContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        InputContainerView.heightAnchor.constraint(equalToConstant: 320).isActive = true
        
        InputContainerView.addSubview(mondayHourButton)
        InputContainerView.addSubview(tuesdayHourButton)
        InputContainerView.addSubview(wednesdayHourButton)
        InputContainerView.addSubview(thursdayHourButton)
        InputContainerView.addSubview(fridayHourButton)
        InputContainerView.addSubview(saturdayHourButton)
        InputContainerView.addSubview(sundayHourButton)
        
        mondayHourButton.topAnchor.constraint(equalTo: InputContainerView.topAnchor, constant: 5).isActive = true
        mondayHourButton.widthAnchor.constraint(equalTo: InputContainerView.widthAnchor, constant: -10).isActive = true
        mondayHourButton.centerXAnchor.constraint(equalTo: InputContainerView.centerXAnchor).isActive = true
        mondayHourButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        tuesdayHourButton.topAnchor.constraint(equalTo: mondayHourButton.bottomAnchor, constant: 5).isActive = true
        tuesdayHourButton.widthAnchor.constraint(equalTo: InputContainerView.widthAnchor, constant: -10).isActive = true
        tuesdayHourButton.centerXAnchor.constraint(equalTo: InputContainerView.centerXAnchor).isActive = true
        tuesdayHourButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        wednesdayHourButton.topAnchor.constraint(equalTo: tuesdayHourButton.bottomAnchor, constant: 5).isActive = true
        wednesdayHourButton.widthAnchor.constraint(equalTo: InputContainerView.widthAnchor, constant: -10).isActive = true
        wednesdayHourButton.centerXAnchor.constraint(equalTo: InputContainerView.centerXAnchor).isActive = true
        wednesdayHourButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        thursdayHourButton.topAnchor.constraint(equalTo: wednesdayHourButton.bottomAnchor, constant: 5).isActive = true
        thursdayHourButton.widthAnchor.constraint(equalTo: InputContainerView.widthAnchor, constant: -10).isActive = true
        thursdayHourButton.centerXAnchor.constraint(equalTo: InputContainerView.centerXAnchor).isActive = true
        thursdayHourButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        fridayHourButton.topAnchor.constraint(equalTo: thursdayHourButton.bottomAnchor, constant: 5).isActive = true
        fridayHourButton.widthAnchor.constraint(equalTo: InputContainerView.widthAnchor, constant: -10).isActive = true
        fridayHourButton.centerXAnchor.constraint(equalTo: InputContainerView.centerXAnchor).isActive = true
        fridayHourButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        saturdayHourButton.topAnchor.constraint(equalTo: fridayHourButton.bottomAnchor, constant: 5).isActive = true
        saturdayHourButton.widthAnchor.constraint(equalTo: InputContainerView.widthAnchor, constant: -10).isActive = true
        saturdayHourButton.centerXAnchor.constraint(equalTo: InputContainerView.centerXAnchor).isActive = true
        saturdayHourButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        sundayHourButton.topAnchor.constraint(equalTo: saturdayHourButton.bottomAnchor, constant: 5).isActive = true
        sundayHourButton.widthAnchor.constraint(equalTo: InputContainerView.widthAnchor, constant: -10).isActive = true
        sundayHourButton.centerXAnchor.constraint(equalTo: InputContainerView.centerXAnchor).isActive = true
        sundayHourButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    @objc func handleHourSelected(_ sender: UIButton){
        
        switch sender.tag {
        case 1:
            self.openingAndClosingTimeView(originalDay: "Monday", specificDay: NSLocalizedString("mondayNavigationTitleOpeningTimeView", comment: "Monday"))
        case 2:
            self.openingAndClosingTimeView(originalDay: "Tuesday", specificDay: NSLocalizedString("tuesdayNavigationTitleOpeningTimeView", comment: "Tuesday"))
        case 3:
            self.openingAndClosingTimeView(originalDay: "Wednesday", specificDay: NSLocalizedString("wednesdayNavigationTitleOpeningTimeView", comment: "Wednesday"))
        case 4:
            self.openingAndClosingTimeView(originalDay: "Thursday", specificDay: NSLocalizedString("thursdayNavigationTitleOpeningTimeView", comment: "Thursday"))
        case 5:
            self.openingAndClosingTimeView(originalDay: "Friday", specificDay: NSLocalizedString("fridayNavigationTitleOpeningTimeView", comment: "Friday"))
        case 6:
            self.openingAndClosingTimeView(originalDay: "Saturday", specificDay: NSLocalizedString("saturdayNavigationTitleOpeningTimeView", comment: "Saturday"))
        case 7:
            self.openingAndClosingTimeView(originalDay: "Sunday", specificDay: NSLocalizedString("sundayNavigationTitleOpeningTimeView", comment: "Sunday"))
        default:
            print("Another button")
        }
        
    }
    
    //remeber to change shop
    func openingAndClosingTimeView(originalDay: String, specificDay: String){
        guard let shopUD = self.shopID else {
            return
        }
        
        let daytimeChoice = DayTimeChoiceViewController()
        
        switch specificDay {
        case "Monday":
            daytimeChoice.navigationItem.title = NSLocalizedString("mondayNavigationTitleOpeningTimeView", comment: "Monday")
            break
        case "Tuesday":
            daytimeChoice.navigationItem.title = NSLocalizedString("tuesdayNavigationTitleOpeningTimeView", comment: "Tuesday")
            break
        case "Wednesday":
            daytimeChoice.navigationItem.title = NSLocalizedString("wednesdayNavigationTitleOpeningTimeView", comment: "Wednesday")
            break
        case "Thursday":
            daytimeChoice.navigationItem.title = NSLocalizedString("thursdayNavigationTitleOpeningTimeView", comment: "Thursday")
            break
        case "Friday":
            daytimeChoice.navigationItem.title = NSLocalizedString("fridayNavigationTitleOpeningTimeView", comment: "Friday")
            break
        case "Saturday":
            daytimeChoice.navigationItem.title = NSLocalizedString("saturdayNavigationTitleOpeningTimeView", comment: "Saturday")
            break
        case "Sunday":
            daytimeChoice.navigationItem.title = NSLocalizedString("sundayNavigationTitleOpeningTimeView", comment: "Sunday")
            break
        default:
            print("Sky high")
        }
        
        daytimeChoice.specificDayData = specificDay
        daytimeChoice.originalDayData = originalDay
        self.daysOfTheWeekBeforePresenting = daysOfTheWeekBeforePresenting.filter { $0 != originalDay }
        daytimeChoice.daysOfTheWeek = self.daysOfTheWeekBeforePresenting
        daytimeChoice.currentShopId = shopUD
        
        if let workhourID = self.currentWorkID {
            daytimeChoice.currentWorkId = workhourID
        }
        daytimeChoice.modalPresentationStyle = .overCurrentContext
        self.present(daytimeChoice, animated: true, completion: nil)
        self.daysOfTheWeekBeforePresenting = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    }
    
    @objc func handleDoneAction(sender: Any){
        if let idData = self.shopID {
            weak var pvc = self.presentingViewController
            self.dismiss(animated: true) {
                let addServiceView = AddServiceViewController()
                addServiceView.shopID = idData
                addServiceView.actionStatusForView = "createShop"
                let navController = UINavigationController(rootViewController: addServiceView)
                pvc?.present(navController, animated: true, completion: nil)
            }
            
        }
        
    }

}
