//
//  ShopStaffViewController.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 08/05/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit
import SwiftSpinner
import Alamofire
import SwiftDate

class ShopStaffViewController: UIViewController {
    
    var viewControllerWhoInitiatedAction: String?
    var staffCustomerData = [Customer]()
    var staffData = [Staff]()
    var shopUniqueID: String?
    var appointmentArray = [AppointmentData]()
    var staffCustomerUniqueIDs = [String]()
    //view callers
    var bookingHistoryBarberShopViewController = BookingHistoryBarberShopViewController()
    var createEditShopStaffHolidayViewController = CreateEditShopStaffHolidayViewController()
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    lazy var closeViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor(r: 11, g: 49, b: 68))
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(exitThisView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.alwaysBounceVertical = true
        cv.backgroundColor = UIColor.clear
        cv.register(customerShopsCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        view.addSubview(closeViewButton)
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewContraints()
        extractStaffCustomerID()
    }
    
    
    @objc func extractStaffCustomerID(){
        if let initiatorData = self.viewControllerWhoInitiatedAction {
            switch initiatorData {
            case "appointmentHistory":
                if !self.appointmentArray.isEmpty {
                    var x = 0
                    for appointSingle in self.appointmentArray {
                        x = x + 1
                        if !staffCustomerUniqueIDs.contains(appointSingle.appointmentStaffCustomer) {
                            staffCustomerUniqueIDs.append(appointSingle.appointmentStaffCustomer)
                        }
                        
                        if(x == self.appointmentArray.count) {
                            self.getStaffCustomerData(stringID: self.staffCustomerUniqueIDs)
                        }
                    }
                }
                break
            case "shopStaffHoliday":
                self.handleGettingShopStaffs()
                break
            default:
                print("sky high")
                break
            }
        }
    }
    
    @objc func getStaffCustomerData(stringID: [String]){
        if !stringID.isEmpty {
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token {
                    
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        //self.dismiss(animated: false, completion: nil)
                        DispatchQueue.main.async {
                            SwiftSpinner.hide()
                            UtilityManager.alertView(view: self)
                        }
                        //log out process based on location
                    } else {
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        let parameters: Parameters = [
                            "staffcustomerIdArray": stringID
                        ]
                        
                        DispatchQueue.global(qos: .background).async {
                            AF.request(self.BACKEND_URL + "getManyStaffById" , method: .post, parameters: parameters, encoding: JSONEncoding(options: []), headers: headers)
                                .validate(statusCode: 200..<500)
                                .validate(contentType: ["application/json"])
                                .response { (response) in
                                    
                                    if let error = response.error {
                                        print("zones", error.localizedDescription)
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        return
                                    }
                                    
                                    if let jsonData = response.data {
                                        do {
                                            DispatchQueue.main.async {
                                                SwiftSpinner.hide()
                                            }
                                            
                                            let staffCustomers = try JSONDecoder().decode([Customer].self, from: jsonData)
                                            self.staffCustomerData = staffCustomers
                                            DispatchQueue.main.async {
                                                self.collectionView.reloadData()
                                            }
                                        } catch _ {
                                            DispatchQueue.main.async {
                                                SwiftSpinner.hide()
                                            }
                                            if let statusCode = response.response?.statusCode {
                                                switch statusCode {
                                                case 404:
                                                    self.view.makeToast("Service was not found", duration: 3.0, position: .bottom)
                                                case 500:
                                                    self.view.makeToast("An error occurred, please try again later", duration: 3.0, position: .bottom)
                                                default:
                                                    print("Sky high")
                                                }
                                            }
                                            
                                        }
                                    }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @objc func handleGettingShopStaffs(){
        print("red one")
        if let UniqueShopID = self.shopUniqueID {
            DispatchQueue.global(qos: .background).async {
                if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                    let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                    if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token {
                        let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                        let dateNow = DateInRegion()
                        print("expiry")
                        if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                            // self.handleUpdateUserAuthData()
                            DispatchQueue.main.async {
                                UtilityManager.alertView(view: self)
                            }
                        } else {
                            DispatchQueue.main.async {
                                SwiftSpinner.show("Loading...")
                            }
                            let headers: HTTPHeaders = [
                                "authorization": token
                            ]
                            print("before request")
                            AF.request(self.BACKEND_URL + "getshopstaff/" + UniqueShopID , method: .post, headers: headers)
                                .validate(statusCode: 200..<500)
                                .validate(contentType: ["application/json"]).response { (response) in
                                    if let error = response.error {
                                        print(error.localizedDescription)
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        return
                                    }
                                    
                                    if let jsonData = response.data {
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        do {
                                            
                                            let staffVustomerArrayData = try JSONDecoder().decode(StaffCustomerList.self, from: jsonData)
                                            let customerDataBeforeCheck = staffVustomerArrayData.customer
                                            self.staffData = staffVustomerArrayData.staff
                                            
                                            for custIn in customerDataBeforeCheck {
                                                if let firstStaff = self.staffData.first(where: { $0.customer == custIn.id}) {
                                                    if (firstStaff.staffHiddenShop == "NO" && firstStaff.staffHiddenByAdmin == "NO") {
                                                        self.staffCustomerData.append(custIn)
                                                        DispatchQueue.main.async {
                                                            self.collectionView.reloadData()
                                                        }
                                                    }
                                                }
                                            }
                                            
                                        } catch _ {
                                            
                                            DispatchQueue.main.async {
                                                SwiftSpinner.hide()
                                            }
                                            
                                            if let statusCode = response.response?.statusCode {
                                                switch statusCode {
                                                case 404:
                                                    self.view.makeToast("Shops not found", duration: 2.0, position: .bottom)
                                                case 500:
                                                    self.view.makeToast("An error occurred, please try again later", duration: 2.0, position: .bottom)
                                                default:
                                                    print("sky high")
                                                }
                                            }
                                            
                                        }
                                    }
                            }
                        }
                    }
                }
                
                
            }
        }
    }
    
    @objc private func handleShopSelection(_ sender: UIButton){
        if let staffCustomerID = staffCustomerData[safe: sender.tag]?.id,
            let initiatorData = self.viewControllerWhoInitiatedAction, let staffFirstName = staffCustomerData[safe: sender.tag]?.firstName {
            
            switch (initiatorData) {
            case "appointmentHistory":
                self.dismiss(animated: true) {
                    let nameString = staffFirstName
                    let someDict = ["staffCustomerUniqueId" : staffCustomerID, "fullName": nameString ]
                    NotificationCenter.default.post(name: .didReceiveDataAppointmentHistoryStaff, object: nil, userInfo: someDict)
                }
                break
            case "shopStaffHoliday":
                if !staffData.isEmpty {
                    if let firstStaff = self.staffData.first(where: {$0.customer == staffCustomerID}){
                        self.dismiss(animated: true) {
                            let nameString = staffFirstName
                            let someDict = ["staffCustomerUniqueId" : staffCustomerID, "fullName": nameString, "staffUniqueID": firstStaff.id ]
                            NotificationCenter.default.post(name: .didReceiveDataShopStaffHolidayEditAdd, object: nil, userInfo: someDict)
                        }
                    }
                }
                break
            default:
                print("no initiator included")
                break
            }
            
            
        }
    }
    
    @objc func setupViewContraints(){
        
        var topDistance: CGFloat = 20
        if UIDevice.current.hasNotch {
            topDistance = 60
        }
        
        closeViewButton.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        closeViewButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -12).isActive = true
        closeViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        closeViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        collectView.topAnchor.constraint(equalTo: closeViewButton.bottomAnchor, constant: 10).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        collectView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
    }
    
    @objc private func exitThisView(){
        self.dismiss(animated: true, completion: nil)
    }

}

extension ShopStaffViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return staffCustomerData.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customerShopsCollectionViewCell
        cell.shopNamePlaceHolder.setTitle(staffCustomerData[indexPath.row].firstName + " " + staffCustomerData[indexPath.row].lastName, for: .normal)
        cell.shopNamePlaceHolder.tag = indexPath.row
        cell.goToShopButton.tag = indexPath.row
        cell.shopNamePlaceHolder.addTarget(self, action: #selector(handleShopSelection(_:)), for: .touchUpInside)
        cell.goToShopButton.addTarget(self, action: #selector(handleShopSelection(_:)), for: .touchUpInside)
        cell.goToShopButton.setImage(UIImage(named: "icons8staffLogoLarge"), for: .normal)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}
