//
//  TestViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 8/20/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate
import LocalAuthentication
import SwiftyJSON

class TestViewController: UIViewController {
    var counterInitValueForPostToServer = 0
    var timeer = [1,2,3]
    
    let timer = TimerModel.sharedTimer
    let chosenTimeInterval = 30
    
    
    let timerSpecialNotificationCenterKeyInactive = "com.teckdkapps.timerSpecialNotificationKeyInactive"
    
    let timerSpecialNotificationCenterKeyActive = "com.teckdkapps.timerSpecialNotificationKeyActive"

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.green
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "show", style: .done, target: self, action: #selector(authenticateUser))
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotificationWhenInactive(notification:)), name: Notification.Name(timerSpecialNotificationCenterKeyInactive), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotificationWhenActive(notification:)), name: Notification.Name(timerSpecialNotificationCenterKeyActive), object: nil)
        
        timer.startTimer(withInterval: 1) {
            self.testing()
        }
        self.perform(#selector(stopTimerForTimeComplete), with: self, afterDelay: 30)
        saveAndStartTimerBeforePhoneInactive()
        
    }
    
    
    @objc func methodOfReceivedNotificationWhenInactive(notification: Notification){
        print("Inactive State activated")
        self.stopTimerForInactive()
        self.saveAndStopTimerWhenPhoneInactive()
        NSObject.cancelPreviousPerformRequests(withTarget: self)
    }
    
    @objc func methodOfReceivedNotificationWhenActive(notification: Notification){
        print("Inactive State deactivated")
        self.resumeTimeWhenPhoneActive()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(timerSpecialNotificationCenterKeyInactive), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(timerSpecialNotificationCenterKeyActive), object: nil)
    }
    
    var counter = 0
    func testing(){
        counter += 1
        print("this is a test \(counter)")
    }
    
    @objc func stopTimer(){
        timer.stopTimer()
        print("time stopped good not cool")
    }
    
    @objc func stopTimerForInactive(){
        timer.stopTimer()
        print("time stopped due to inactive mode activated")
    }
    
    @objc func stopTimerForTimeComplete(){
        timer.stopTimer()
        print("time stopped due time completely elapsed")
        self.showAlertForMoreTime()
    }
    
    @objc func stopTimerForErrors(){
        timer.stopTimer()
        print("time stopped due to errors")
    }
    
    func saveAndStartTimerBeforePhoneInactive(){
        let newDate = DateInRegion().toString(.extended)
        let userTimezone = DateByUserDeviceInitializer.tzone
        let userCalender = DateByUserDeviceInitializer.calenderNow
        let userLocal = DateByUserDeviceInitializer.localCode
        
        UserDefaults.standard.set(newDate, forKey: "timerTimeStartedDateTime")
        UserDefaults.standard.set(userTimezone, forKey: "timerTimeStartedTimeZone")
        UserDefaults.standard.set(userCalender, forKey: "timerTimeStartedCalendar")
        UserDefaults.standard.set(userLocal, forKey: "timerTimeStartedLocal")
    }
    
    func saveAndStopTimerWhenPhoneInactive(){
        let newDate = DateInRegion().toString(.extended)
        let userTimezone = DateByUserDeviceInitializer.tzone
        let userCalender = DateByUserDeviceInitializer.calenderNow
        let userLocal = DateByUserDeviceInitializer.localCode
        
        UserDefaults.standard.set(newDate, forKey: "timerTimeStoppedDateTime")
        UserDefaults.standard.set(userTimezone, forKey: "timerTimeStoppedTimeZone")
        UserDefaults.standard.set(userCalender, forKey: "timerTimeStoppedCalendar")
        UserDefaults.standard.set(userLocal, forKey: "timerTimeStoppedLocal")
    }
    
    func resumeTimeWhenPhoneActive(){
        if let dateStartString = UserDefaults.standard.object(forKey: "timerTimeStartedDateTime") as? String, let dateStartTimeZone = UserDefaults.standard.object(forKey: "timerTimeStartedTimeZone") as? String,let dateStartCalendar = UserDefaults.standard.object(forKey: "timerTimeStartedCalendar") as? String,let dateStartLocal = UserDefaults.standard.object(forKey: "timerTimeStartedLocal") as? String, let dateString = UserDefaults.standard.object(forKey: "timerTimeStoppedDateTime") as? String, let dateTimeZone = UserDefaults.standard.object(forKey: "timerTimeStoppedTimeZone") as? String,let dateCalendar = UserDefaults.standard.object(forKey: "timerTimeStoppedCalendar") as? String,let dateLocal = UserDefaults.standard.object(forKey: "timerTimeStoppedLocal") as? String  {
            
            let dateStartRegion = DateByUserDeviceInitializer.getRegion(TZoneName: dateStartTimeZone, calenName: dateStartCalendar, LocName: dateStartLocal)
            let dateStoppedRegion = DateByUserDeviceInitializer.getRegion(TZoneName: dateTimeZone, calenName: dateCalendar, LocName: dateLocal)
            
            //dateStartString.toDate(style: .extended, region: dateStartRegion)
            //dateString.toDate(style: .extended, region: dateStoppedRegion)
            if let dateTimeStartedNew = DateFormatVerify.checkFormat(dateString: dateStartString, RegionData: dateStartRegion)  , let dateTimeStoppped = DateFormatVerify.checkFormat(dateString: dateString, RegionData: dateStoppedRegion) {
                let newDateTimeStoppped = dateTimeStartedNew + chosenTimeInterval.seconds
                let currentDate = DateInRegion()
                
                
                if newDateTimeStoppped.isAfterDate(currentDate, orEqual: true, granularity: .second) {
                    let timeStartedCount = dateTimeStartedNew.second
                    let timeStoppedCount = dateTimeStoppped.second
                    let remainderSeconds = timeStoppedCount - timeStartedCount
                    let remainderTotal = chosenTimeInterval - remainderSeconds
                    print(remainderTotal)
                    if remainderTotal > 0 {
                        timer.startTimer(withInterval: 1) {
                            self.testing()
                        }
                        let doubleRemainderSeconds = Double(remainderTotal)
                        self.counter = 0
                        self.perform(#selector(stopTimerForTimeComplete), with: self, afterDelay: doubleRemainderSeconds)
                    } else {
                        self.stopTimerForTimeComplete()
                    }
                    
                } else {
                    self.stopTimerForTimeComplete()
                }
            } else {
                self.stopTimerForErrors()
            }
        } else {
            self.stopTimerForErrors()
        }
    }
    
    func showAlertForMoreTime(){
        let alertController = UIAlertController(title: "Destructive", message: "Simple alertView demo with Destructive and Ok.", preferredStyle: UIAlertController.Style.alert) //Replace UIAlertControllerStyle.Alert by UIAlertControllerStyle.alert
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        
        // Replace UIAlertActionStyle.Default by UIAlertActionStyle.default
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            (result : UIAlertAction) -> Void in
            print("OK")
            self.askForExtebsion()
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func askForExtebsion(){
        self.counter = 0
        timer.startTimer(withInterval: 1) {
            self.testing()
        }
        self.perform(#selector(stopTimerForTimeComplete), with: self, afterDelay: 30)
        saveAndStartTimerBeforePhoneInactive()
    }
    
    @objc func authenticateUser() {
        let context = LAContext()
        context.evaluatePolicy(LAPolicy.deviceOwnerAuthentication, localizedReason: "Please authenticate to proceed.") { (success, errorrrr) in
            guard success else {
                DispatchQueue.main.async() {
                    // show something here to block the user from continuing
                    print("signed to your boss")
                }
                
                return
            }
            DispatchQueue.main.async() {
                // do something here to continue loading your app, e.g. call a delegate method
                print("we got soul")
            }
        }
    }
}
