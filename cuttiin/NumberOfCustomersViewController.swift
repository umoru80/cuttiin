//
//  NumberOfCustomersViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 8/4/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate

class NumberOfCustomersViewController: UIViewController {
    
    var barberShopUUID: String?
    var barberShopLocale: String?
    var barberShopTimezone: String?
    var barberShopCalendar: String?
    
    var navBar: UINavigationBar = UINavigationBar()
    var barberOne = BarberShop()
    var selectedStringServiceOneFinal = [String]()
    var selectedStringBarbersOneFinal = [String]()
    var barberlistone = [Users]()
    var serviceOneBasedNew = [Service]()
    var totalPriceService  = [Int]()
    var totalTimeService = [Int]()
    var totalQuantity = [Int]()
    var totalQuantityDouble = [Double]()
    var totalServceCostAtTheEnd: String?
    var totalServiceTimeTaken: String?
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    let barberShopHeaderDetailsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var barberShopLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.white //(r: 23, g: 69, b: 90)
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 25
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    lazy var barberShopNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var barberShopAddressPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var headerViewDescriptionPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "BebasNeue", size: 20)
        fnhp.textColor = UIColor.white
        fnhp.text = NSLocalizedString("summaryHeaderNumberOfCustomer", comment: "SUMMARY")
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customNumberOfCustomerCollectionViewCell.self, forCellWithReuseIdentifier: "cellId5")
        return cv
    }()
    
    let pictureAndDescriptionUploadContainerView: UIView = {
        let paduview = UIView()
        paduview.translatesAutoresizingMaskIntoConstraints = false
        paduview.isHidden = true
        return paduview
    }()
    
    let pictureSelectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "add_photo_smallest")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let shortDescriptionContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    let shortDescriptionHiddenPlaceHolder: UILabel = {
        let lnhp = UILabel()
        lnhp.translatesAutoresizingMaskIntoConstraints = false
        //lnhp.text = NSLocalizedString("shortDescriptionTextNumberOfCustomer", comment: "Short Description")
        lnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        lnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        lnhp.textAlignment = .left
        return lnhp
    }()
    
    let shortDescriptionTextField: UITextView = {
        let em = UITextView()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        em.text = NSLocalizedString("shortDescriptionTextNumberOfCustomer", comment: "Short Description")
        em.textColor = UIColor(r: 118, g: 187, b: 220)
        em.textAlignment = .justified
        em.backgroundColor = UIColor.clear
        em.isEditable = false
        em.isSelectable = true
        em.dataDetectorTypes = UIDataDetectorTypes.link
        em.autocorrectionType = UITextAutocorrectionType.yes
        em.spellCheckingType = UITextSpellCheckingType.yes
        em.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        em.layer.borderWidth = 2
        em.layer.masksToBounds = true
        em.layer.cornerRadius = 5
        return em
    }()
    
    let lowerInputContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.lightGray
        return view
    }()
    
    let totalPriceAndTimeContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    let totalTextHeaderPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.textColor = UIColor(r: 23, g: 69, b: 90)
        fnhp.text = NSLocalizedString("totalTextHolderNumberOfCustomer", comment: "Total")
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let totalTimePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.textColor = UIColor(r: 23, g: 69, b: 90)
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let totalPricePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.textColor = UIColor(r: 23, g: 69, b: 90)
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    lazy var selectDateButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        fbutton.setTitle(NSLocalizedString("uploadPictureButtonTitleNumberOfCustomer", comment: "UPLOAD PICTURE"), for: .normal)
        fbutton.setTitleColor(UIColor.white, for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 30)
        fbutton.layer.cornerRadius = 5
        fbutton.layer.masksToBounds = true
        fbutton.layer.borderWidth = 2
        fbutton.layer.borderColor = UIColor.black.cgColor
        fbutton.addTarget(self, action: #selector(handleShowBookingView), for: .touchUpInside)
        return fbutton
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleExit))
        navigationItem.rightBarButtonItem?.isEnabled = true
        setNavBarToTheView()
        view.addSubview(headerViewDescriptionPlaceHolder)
        view.addSubview(lowerInputContainerView)
        view.addSubview(pictureAndDescriptionUploadContainerView)
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjectContraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        if let bookComplete = UserDefaults.standard.object(forKey: "theBookingProcessIsComplete") as? Bool{
            if bookComplete == true {
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    func handleTimeAddSummation(){
        DispatchQueue.main.async {
            var sum = 0
            var counter = 0
            
            // Enter your code below
            while counter < self.totalTimeService.count {
                
                let newValue = self.totalTimeService[counter]
                sum += newValue * self.totalQuantity[counter]
                counter += 1
                self.totalTimePlaceHolder.text = String(sum) + "min"
                self.totalServiceTimeTaken = "\(sum)"
            }
        }
    }
    
    func handlePriceAddSummation(){
        DispatchQueue.main.async {
            var sume = 0
            var counterx = 0
            
            // Enter your code below
            while counterx < self.totalPriceService.count {
                
                let newValuexx = self.totalPriceService[counterx]
                sume += newValuexx * Int(self.totalQuantity[counterx])
                counterx += 1
                self.totalPricePlaceHolder.text = String(sume) + "-"
                self.totalServceCostAtTheEnd = "\(sume)"
            }
        }
    }
    
    @objc func handleCollectionCellAddition(sender: UIButton){
        if self.totalQuantity.count > 0 {
            if self.totalQuantity[sender.tag] < 50 {
                self.totalQuantity[sender.tag] = self.totalQuantity[sender.tag] + 1
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                    self.handlePriceAddSummation()
                    self.handleTimeAddSummation()
                }
            }
        }
    }
    
    @objc func handleCollectionCellSubtraction(sender: UIButton){
        if self.totalQuantity.count > 0 {
            if self.totalQuantity[sender.tag] > 1 {
                self.totalQuantity[sender.tag] = self.totalQuantity[sender.tag] - 1
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                    self.handlePriceAddSummation()
                    self.handleTimeAddSummation()
                }
            }
        }
    }
    
    
    
    @objc func handleShowBookingView(){
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        let modalViewController = UploadPictureAndDescriptionModalViewController()
        modalViewController.numberofcustomer = self
        modalViewController.shortDescriptionTextField.text = self.shortDescriptionTextField.text
        modalViewController.modalPresentationStyle = .overCurrentContext
        present(modalViewController, animated: true, completion: nil)
    }
    
    @objc func handleExit(){
        dismiss(animated: true, completion: nil)
    }
    
    func setNavBarToTheView() {
        self.navBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 150)
        self.view.addSubview(navBar)
        let coverLayer = CALayer()
        coverLayer.frame = navBar.bounds;
        coverLayer.backgroundColor = UIColor(r: 23, g: 69, b: 90).cgColor
        navBar.layer.addSublayer(coverLayer)
        navBar.addSubview(barberShopHeaderDetailsContainerView)
        
        barberShopHeaderDetailsContainerView.topAnchor.constraint(equalTo: navBar.topAnchor, constant: 25).isActive = true
        barberShopHeaderDetailsContainerView.centerXAnchor.constraint(equalTo: navBar.centerXAnchor).isActive = true
        barberShopHeaderDetailsContainerView.widthAnchor.constraint(equalTo: navBar.widthAnchor, constant: -96).isActive = true
        barberShopHeaderDetailsContainerView.heightAnchor.constraint(equalTo: navBar.heightAnchor, constant: -35).isActive = true
        
        barberShopHeaderDetailsContainerView.addSubview(barberShopLogoImageView)
        barberShopHeaderDetailsContainerView.addSubview(barberShopNamePlaceHolder)
        barberShopHeaderDetailsContainerView.addSubview(barberShopAddressPlaceHolder)
        
        
        barberShopLogoImageView.topAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.topAnchor).isActive = true
        barberShopLogoImageView.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopLogoImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        barberShopLogoImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        barberShopNamePlaceHolder.topAnchor.constraint(equalTo: barberShopLogoImageView.bottomAnchor, constant: 5).isActive = true
        barberShopNamePlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopNamePlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberShopNamePlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        barberShopAddressPlaceHolder.topAnchor.constraint(equalTo: barberShopNamePlaceHolder.bottomAnchor, constant: 5).isActive = true
        barberShopAddressPlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopAddressPlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberShopAddressPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
    }
    
    func setupViewObjectContraints(){
        headerViewDescriptionPlaceHolder.topAnchor.constraint(equalTo: view.topAnchor, constant: 165).isActive = true
        headerViewDescriptionPlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        headerViewDescriptionPlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        headerViewDescriptionPlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        lowerInputContainerView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        lowerInputContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        lowerInputContainerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        lowerInputContainerView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.25, constant: -45).isActive = true
        
        lowerInputContainerView.addSubview(totalPriceAndTimeContainerView)
        lowerInputContainerView.addSubview(selectDateButton)
        
        
        selectDateButton.bottomAnchor.constraint(equalTo: lowerInputContainerView.bottomAnchor, constant: -5).isActive = true
        selectDateButton.widthAnchor.constraint(equalTo: lowerInputContainerView.widthAnchor, constant: -48).isActive = true
        selectDateButton.centerXAnchor.constraint(equalTo: lowerInputContainerView.centerXAnchor).isActive = true
        selectDateButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        totalPriceAndTimeContainerView.bottomAnchor.constraint(equalTo: selectDateButton.topAnchor).isActive = true
        totalPriceAndTimeContainerView.widthAnchor.constraint(equalTo: lowerInputContainerView.widthAnchor, multiplier: 1, constant: -48).isActive = true
        totalPriceAndTimeContainerView.centerXAnchor.constraint(equalTo: lowerInputContainerView.centerXAnchor).isActive = true
        totalPriceAndTimeContainerView.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        totalPriceAndTimeContainerView.addSubview(totalTextHeaderPlaceHolder)
        totalPriceAndTimeContainerView.addSubview(totalTimePlaceHolder)
        totalPriceAndTimeContainerView.addSubview(totalPricePlaceHolder)
        
        totalTextHeaderPlaceHolder.topAnchor.constraint(equalTo: totalPriceAndTimeContainerView.topAnchor).isActive = true
        totalTextHeaderPlaceHolder.leftAnchor.constraint(equalTo: totalPriceAndTimeContainerView.leftAnchor).isActive = true
        totalTextHeaderPlaceHolder.widthAnchor.constraint(equalToConstant: 80).isActive = true
        totalTextHeaderPlaceHolder.heightAnchor.constraint(equalTo: totalPriceAndTimeContainerView.heightAnchor).isActive = true
        
        totalPricePlaceHolder.topAnchor.constraint(equalTo: totalPriceAndTimeContainerView.topAnchor).isActive = true
        totalPricePlaceHolder.rightAnchor.constraint(equalTo: totalPriceAndTimeContainerView.rightAnchor).isActive = true
        totalPricePlaceHolder.widthAnchor.constraint(equalToConstant: 90).isActive = true
        totalPricePlaceHolder.heightAnchor.constraint(equalTo: totalPriceAndTimeContainerView.heightAnchor).isActive = true
        
        totalTimePlaceHolder.topAnchor.constraint(equalTo: totalPriceAndTimeContainerView.topAnchor).isActive = true
        totalTimePlaceHolder.rightAnchor.constraint(equalTo: totalPricePlaceHolder.leftAnchor).isActive = true
        totalTimePlaceHolder.widthAnchor.constraint(equalToConstant: 90).isActive = true
        totalTimePlaceHolder.heightAnchor.constraint(equalTo: totalPriceAndTimeContainerView.heightAnchor).isActive = true
        
        
        
        pictureAndDescriptionUploadContainerView.bottomAnchor.constraint(equalTo: lowerInputContainerView.topAnchor, constant: -5).isActive = true
        pictureAndDescriptionUploadContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        pictureAndDescriptionUploadContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -24).isActive = true
        pictureAndDescriptionUploadContainerView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        pictureAndDescriptionUploadContainerView.addSubview(pictureSelectImageView)
        pictureAndDescriptionUploadContainerView.addSubview(shortDescriptionContainerView)
        
        pictureSelectImageView.topAnchor.constraint(equalTo: pictureAndDescriptionUploadContainerView.topAnchor).isActive = true
        pictureSelectImageView.leftAnchor.constraint(equalTo: pictureAndDescriptionUploadContainerView.leftAnchor).isActive = true
        pictureSelectImageView.widthAnchor.constraint(equalTo: pictureAndDescriptionUploadContainerView.widthAnchor, multiplier: 0.25, constant: 10).isActive = true
        pictureSelectImageView.heightAnchor.constraint(equalTo: pictureAndDescriptionUploadContainerView.heightAnchor).isActive = true
        
        shortDescriptionContainerView.topAnchor.constraint(equalTo: pictureAndDescriptionUploadContainerView.topAnchor).isActive = true
        shortDescriptionContainerView.leftAnchor.constraint(equalTo: pictureSelectImageView.rightAnchor, constant: 5).isActive = true
        shortDescriptionContainerView.rightAnchor.constraint(equalTo: pictureAndDescriptionUploadContainerView.rightAnchor).isActive = true
        shortDescriptionContainerView.heightAnchor.constraint(equalTo: pictureAndDescriptionUploadContainerView.heightAnchor).isActive = true
        
        shortDescriptionContainerView.addSubview(shortDescriptionHiddenPlaceHolder)
        shortDescriptionContainerView.addSubview(shortDescriptionTextField)
        
        shortDescriptionHiddenPlaceHolder.topAnchor.constraint(equalTo: shortDescriptionContainerView.topAnchor).isActive = true
        shortDescriptionHiddenPlaceHolder.centerXAnchor.constraint(equalTo: shortDescriptionContainerView.centerXAnchor).isActive = true
        shortDescriptionHiddenPlaceHolder.widthAnchor.constraint(equalTo: shortDescriptionContainerView.widthAnchor).isActive = true
        shortDescriptionHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        shortDescriptionTextField.topAnchor.constraint(equalTo: shortDescriptionHiddenPlaceHolder.bottomAnchor).isActive = true
        shortDescriptionTextField.centerXAnchor.constraint(equalTo: shortDescriptionContainerView.centerXAnchor).isActive = true
        shortDescriptionTextField.widthAnchor.constraint(equalTo: shortDescriptionContainerView.widthAnchor).isActive = true
        shortDescriptionTextField.bottomAnchor.constraint(equalTo: shortDescriptionContainerView.bottomAnchor).isActive = true
        
        collectView.topAnchor.constraint(equalTo: headerViewDescriptionPlaceHolder.bottomAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -24).isActive = true
        collectView.bottomAnchor.constraint(equalTo: pictureAndDescriptionUploadContainerView.topAnchor, constant: -5).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
    }

}

class customNumberOfCustomerCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let thumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.black
        return tniv
    }()
    
    lazy var collectionServiceNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var collectionAdditionButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor.clear
        fbutton.setImage(UIImage(named: "plus_btn"), for: .normal)
        fbutton.tag = 8
        //fbutton.addTarget(self, action: #selector(handleCollectionAdditionSubtraction), for: .touchUpInside)
        return fbutton
    }()
    
    lazy var collectionAmountPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var collctionSubtractionButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor.clear
        fbutton.setImage(UIImage(named: "minus_btn"), for: .normal)
        fbutton.tag = 9
        //fbutton.addTarget(self, action: #selector(handleCollectionAdditionSubtraction), for: .touchUpInside)
        return fbutton
    }()
    
    lazy var collectionTimeTakenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.text = "Time"
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    lazy var collectionPricePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.text = "Price"
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    var madeMen = NumberOfCustomersViewController()
    
    
    func setupViews(){
        addSubview(collectionServiceNamePlaceHolder)
        addSubview(collectionAdditionButton)
        addSubview(collectionAmountPlaceHolder)
        addSubview(collctionSubtractionButton)
        addSubview(collectionTimeTakenPlaceHolder)
        addSubview(collectionPricePlaceHolder)
        addSubview(seperatorView)
        
        backgroundColor = UIColor.clear
        addContraintsWithFormat(format: "H:|-5-[v0(110)]-5-[v1(30)][v2(30)][v3(30)]-5-[v4(50)]-5-[v5(50)]-5-|", views: collectionServiceNamePlaceHolder, collctionSubtractionButton,collectionAmountPlaceHolder, collectionAdditionButton, collectionTimeTakenPlaceHolder, collectionPricePlaceHolder)
        addContraintsWithFormat(format: "V:|-5-[v0]-10-[v1(2)]|", views: collectionServiceNamePlaceHolder,seperatorView)
        addContraintsWithFormat(format: "V:|-5-[v0]-12-|", views: collctionSubtractionButton)
        addContraintsWithFormat(format: "V:|-5-[v0]-12-|", views: collectionAmountPlaceHolder)
        addContraintsWithFormat(format: "V:|-5-[v0]-12-|", views: collectionAdditionButton)
        addContraintsWithFormat(format: "V:|-5-[v0]-12-|", views: collectionTimeTakenPlaceHolder)
        addContraintsWithFormat(format: "V:|-5-[v0]-12-|", views: collectionPricePlaceHolder)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}










