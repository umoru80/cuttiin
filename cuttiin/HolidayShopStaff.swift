//
//  HolidayShopStaff.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 12/05/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation

struct HolidayShopStaff: Codable {
    let message: String
    let holidayStaff: [HolidayStaff]
    let staff: [Staff]
    let customer: [Customer]
}
