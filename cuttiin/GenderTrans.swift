//
//  GenderTrans.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 09/12/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit

struct GenderTrans {
    var translated: String
    var original: String
    
    init(translated: String, original: String) {
        self.translated = translated
        self.original = original
    }
}
