//
//  ShopStaffRatingViewController.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 01/06/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit
import SwiftDate
import Alamofire
import FBSDKLoginKit
import MGStarRatingView
import SwiftSpinner

class ShopStaffRatingViewController: UIViewController, StarRatingDelegate {
    
    var shopID: String?
    var staffCustomerID: String?
    var customerID: String?
    var appointmentID: String?
    var shopRatingValue: String?
    var staffRatingValue: String?
    var barberShopsViewController = BarberShopsViewController()
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    let attributeA = StarRatingAttribute(type: .fill,point: 50,spacing: 5, emptyColor: .gray, fillColor: UIColor(r: 244, g: 222, b: 172))
    let attributeB = StarRatingAttribute(type: .fill,point: 50,spacing: 5, emptyColor: .gray, fillColor: UIColor(r: 244, g: 222, b: 172))
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor.clear
        return tcview
    }()
    
    lazy var shopstaffratingViewIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8starrating")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var shopstaffratingCloseViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismissView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let shopstaffratingViewPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("shopStaffRatingViewControllerTitle", comment: "Ratings")
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 20)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var shopstaffratingDoneButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitleColor(UIColor.black, for: .normal)
        st.setTitle(NSLocalizedString("shopStaffRatingViewControllerDoneButton", comment: "Done"), for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        st.isEnabled = true
        st.isUserInteractionEnabled = true
        st.addTarget(self, action: #selector(handleDoneAction), for: .touchUpInside)
        return st
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    
    let shopRatingHeaderTitle: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("shopStaffRatingViewControllerShopRatingTitle", comment: "Shop rating")
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let shopStarView = StarRatingView()
    
    
    let staffRatingHeaderTitle: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("shopStaffRatingViewControllerStaffRatingTitle", comment: "Staff rating")
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let staffStarView = StarRatingView()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        shopStarView.tag = 1
        shopStarView.configure(attributeA, current: 0, max: 5)
        shopStarView.min = 1
        shopStarView.translatesAutoresizingMaskIntoConstraints = false
        shopStarView.isUserInteractionEnabled = true
        shopStarView.delegate = self
        staffStarView.tag = 2
        staffStarView.configure(attributeB, current: 0, max: 5)
        staffStarView.min = 1
        staffStarView.translatesAutoresizingMaskIntoConstraints = false
        staffStarView.isUserInteractionEnabled = true
        staffStarView.delegate = self
        view.addSubview(shopstaffratingCloseViewButton)
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        view.addSubview(shopStarView)
        view.addSubview(shopRatingHeaderTitle)
        view.addSubview(staffStarView)
        view.addSubview(staffRatingHeaderTitle)
        setupViewObjectConstriants()
        handleGetAppointmentData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @objc private func handleDismissView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func handleDoneAction(){
        if let valShop = self.shopRatingValue, let valStaff = self.staffRatingValue {
            print("nayama")
            
            self.dismiss(animated: true) {
                self.handleSavingShopRating(ratingVlue: valShop)
                self.handleSavingCustomerRating(ratingVlue: valStaff)
            }
        } else {
            self.view.makeToast(NSLocalizedString("shopStaffRatingViewControllerErrorMessageChooseRating", comment: "please choose a rating value of both shop and staff"), duration: 2.0, position: .bottom)
        }
    }
    
    func StarRatingValueChanged(view: StarRatingView, value: CGFloat) {
        switch view.tag {
        case 1:
            self.shopRatingValue = value.description
            if let val1 = self.shopRatingValue {
                print("shop", val1)
            }
            break
        case 2:
            self.staffRatingValue = value.description
            if let val2 = self.staffRatingValue {
                print("staff",val2)
            }
            break
        default:
            print("sky high")
            break
        }
    }
    
    @objc private func handleGetAppointmentData(){
        DispatchQueue.global(qos: .background).async {
            
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let appointmentID = self.appointmentID {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        DispatchQueue.main.async {
                            SwiftSpinner.show("Loading...")
                        }
                        
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        AF.request(self.BACKEND_URL + "getSingleAppointment/" + appointmentID, method: .post, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    do {
                                        let appointmentData = try JSONDecoder().decode(Appointment.self, from: jsonData)
                                        self.customerID = appointmentData.appointmentCustomer
                                        self.staffCustomerID = appointmentData.appointmentStaffCustomer
                                        self.appointmentID = appointmentData.id
                                        self.shopID = appointmentData.appointmentShop
                                        
                                        self.getShopData(shopUniqueID: appointmentData.appointmentShop)
                                        self.getStaffCustomer(customerID: appointmentData.appointmentStaffCustomer)
                                    } catch _ {
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                self.view.makeToast(NSLocalizedString("shopStaffRatingViewControllerErrorMessageAppointmentNotFound", comment: "Appointment not found"), duration: 2.0, position: .bottom)
                                            case 500:
                                                self.view.makeToast(NSLocalizedString("shopStaffRatingViewControllerErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                            default:
                                                print("sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
        
    }
    
    @objc private func getShopData(shopUniqueID: String){
        DispatchQueue.global(qos: .background).async {
            
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        AF.request(self.BACKEND_URL + "getsingleshop/" + shopUniqueID, method: .post, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    print("got data back")
                                    do {
                                        let shopData = try JSONDecoder().decode(Shop.self, from: jsonData)
                                        DispatchQueue.main.async {
                                            self.shopRatingHeaderTitle.text = shopData.shopName
                                        }
                                        
                                    } catch _ {
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                self.view.makeToast(NSLocalizedString("shopStaffRatingViewControllerErrorMessageShopNotFound", comment: "Shop not found"), duration: 2.0, position: .bottom)
                                            case 500:
                                                self.view.makeToast(NSLocalizedString("shopStaffRatingViewControllerErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                            default:
                                                print("sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
    }
    
    @objc private func getStaffCustomer(customerID: String){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    DispatchQueue.global(qos: .background).async {
                        AF.request(self.BACKEND_URL + customerID, method: .get, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    do {
                                        let customerSingleData = try JSONDecoder().decode(CustomerData.self, from: jsonData)
                                        DispatchQueue.main.async {
                                            self.staffRatingHeaderTitle.text = customerSingleData.customer.firstName + " " + customerSingleData.customer.lastName
                                        }
                                        
                                    } catch _ {
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                self.view.makeToast(NSLocalizedString("shopStaffRatingViewControllerErrorMessageStaffNotFound", comment: "Staff not found"), duration: 2.0, position: .bottom)
                                            case 500:
                                                self.view.makeToast(NSLocalizedString("shopStaffRatingViewControllerErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                            default:
                                                print("sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                    
                }
            }
        }
    }
    
    @objc private func handleSavingCustomerRating(ratingVlue: String){
        if let customerWhoCreated = self.customerID{
            DispatchQueue.global(qos: .background).async {
                
                if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                    let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                    if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let appointid = self.appointmentID, let staff = self.staffCustomerID {
                        let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                        let dateNow = DateInRegion()
                        print("no optionals failed customer rating")
                        
                        if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                            DispatchQueue.main.async {
                                UtilityManager.alertView(view: self)
                            }
                        } else {
                            
                            let startDate = DateInRegion().convertTo(region: region).toFormat("yyyy-MM-dd HH:mm:ss")
                            
                            let headers: HTTPHeaders = [
                                "authorization": token
                            ]
                            
                            let parameters: Parameters = [
                                "ratingValue": ratingVlue,
                                "ratingComment": "not available",
                                "customerWhoGaveRating": customerWhoCreated,
                                "customerWhoRecievedRating": staff,
                                "customerWhoRecievedRatingRole": "staff",
                                "appointmentRated": appointid,
                                "ratingDateCreated": startDate,
                                "ratingDateTimezone": timeZone,
                                "ratingDateCalendar": "gregorian",
                                "ratingDateLocale": "en"
                                
                            ]
                            
                            AF.request(self.BACKEND_URL + "saveCustomerRating" , method: .post, parameters: parameters, encoding: JSONEncoding(options: []), headers: headers)
                                .validate(statusCode: 200..<501)
                                .validate(contentType: ["application/json"]).response { (response) in
                                    if let error = response.error {
                                        print("zones", error.localizedDescription)
                                        return
                                    }
                                    
                                    if let jsonData = response.data {
                                        if let statusCode = response.response?.statusCode {
                                            
                                            do {
                                                
                                                let shopData = try JSONDecoder().decode(ServerMessage.self, from: jsonData)
                                                DispatchQueue.main.async {
                                                    
                                                    switch statusCode {
                                                    case 201:
                                                        print(shopData.message)
                                                    case 409:
                                                        print(shopData.message)
                                                    case 500:
                                                        print(shopData.message)
                                                    default:
                                                        print("sky high")
                                                    }
                                                }
                                                
                                            } catch _ {
                                                DispatchQueue.main.async {
                                                    if let statusCode = response.response?.statusCode {
                                                        switch statusCode {
                                                        case 409:
                                                            print("device unique validation failed")
                                                        case 500:
                                                            print("An error occurred")
                                                        default:
                                                            print("sky high")
                                                        }
                                                    }
                                                }
                                            }
                                            
                                        }
                                    }
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    @objc private func handleSavingShopRating(ratingVlue: String){
        if let customerWhoCreated = self.customerID, let shop = self.shopID {
            DispatchQueue.global(qos: .background).async {
                
                if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                    let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                    if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let appointid = self.appointmentID {
                        let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                        let dateNow = DateInRegion()
                        print("no optionals failed shop rating")
                        
                        if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                            DispatchQueue.main.async {
                                UtilityManager.alertView(view: self)
                            }
                        } else {
                            
                            let startDate = DateInRegion().convertTo(region: region).toFormat("yyyy-MM-dd HH:mm:ss")
                            
                            let headers: HTTPHeaders = [
                                "authorization": token
                            ]
                            
                            let parameters: Parameters = [
                                "ratingValue": ratingVlue,
                                "ratingComment": "not available",
                                "customerWhoGaveRating": customerWhoCreated,
                                "shopRecievedRating": shop,
                                "appointmentRated": appointid,
                                "ratingDateCreated": startDate,
                                "ratingDateTimezone": timeZone,
                                "ratingDateCalendar": "gregorian",
                                "ratingDateLocale": "en"
                                
                            ]
                            
                            AF.request(self.BACKEND_URL + "saveShopRating" , method: .post, parameters: parameters, encoding: JSONEncoding(options: []), headers: headers)
                                .validate(statusCode: 200..<501)
                                .validate(contentType: ["application/json"]).response { (response) in
                                    if let error = response.error {
                                        print("zones", error.localizedDescription)
                                        return
                                    }
                                    
                                    if let jsonData = response.data {
                                        if let statusCode = response.response?.statusCode {
                                            
                                            do {
                                                
                                                let shopData = try JSONDecoder().decode(ServerMessage.self, from: jsonData)
                                                DispatchQueue.main.async {
                                                    
                                                    switch statusCode {
                                                    case 201:
                                                        print(shopData.message)
                                                    case 409:
                                                        print(shopData.message)
                                                    case 500:
                                                        print(shopData.message)
                                                    default:
                                                        print("sky high")
                                                    }
                                                }
                                                
                                            } catch _ {
                                                DispatchQueue.main.async {
                                                    if let statusCode = response.response?.statusCode {
                                                        switch statusCode {
                                                        case 409:
                                                            print("device unique validation failed")
                                                        case 500:
                                                            print("An error occurred")
                                                        default:
                                                            print("sky high")
                                                        }
                                                    }
                                                }
                                            }
                                            
                                        }
                                    }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func setupViewObjectConstriants(){
        
        var topDistance: CGFloat = 20
        if UIDevice.current.hasNotch {
            topDistance = 60
        }
        
        shopstaffratingCloseViewButton.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        shopstaffratingCloseViewButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -15).isActive = true
        shopstaffratingCloseViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        shopstaffratingCloseViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperInputsContainerView.topAnchor.constraint(equalTo: shopstaffratingCloseViewButton.bottomAnchor, constant: 5).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(shopstaffratingViewIcon)
        upperInputsContainerView.addSubview(shopstaffratingViewPlaceHolder)
        upperInputsContainerView.addSubview(shopstaffratingDoneButton)
        
        shopstaffratingViewIcon.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        shopstaffratingViewIcon.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor).isActive = true
        shopstaffratingViewIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        shopstaffratingViewIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        shopstaffratingDoneButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        shopstaffratingDoneButton.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        shopstaffratingDoneButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        shopstaffratingDoneButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        shopstaffratingViewPlaceHolder.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        shopstaffratingViewPlaceHolder.leftAnchor.constraint(equalTo: shopstaffratingViewIcon.rightAnchor, constant: 10).isActive = true
        shopstaffratingViewPlaceHolder.rightAnchor.constraint(equalTo: shopstaffratingDoneButton.leftAnchor).isActive = true
        shopstaffratingViewPlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        shopRatingHeaderTitle.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 20).isActive = true
        shopRatingHeaderTitle.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        shopRatingHeaderTitle.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        shopRatingHeaderTitle.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        shopStarView.topAnchor.constraint(equalTo: shopRatingHeaderTitle.bottomAnchor, constant: 10).isActive = true
        shopStarView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        shopStarView.widthAnchor.constraint(equalToConstant: 275).isActive = true
        shopStarView.heightAnchor.constraint(equalToConstant: 55).isActive = true
        
        staffRatingHeaderTitle.topAnchor.constraint(equalTo: shopStarView.bottomAnchor, constant: 10).isActive = true
        staffRatingHeaderTitle.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        staffRatingHeaderTitle.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        staffRatingHeaderTitle.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        staffStarView.topAnchor.constraint(equalTo: staffRatingHeaderTitle.bottomAnchor, constant: 10).isActive = true
        staffStarView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        staffStarView.widthAnchor.constraint(equalToConstant: 275).isActive = true
        staffStarView.heightAnchor.constraint(equalToConstant: 55).isActive = true
    }

}
