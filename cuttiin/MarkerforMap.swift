//
//  MarkerforMap.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 28/02/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

struct MarkerforMap {
    var shopId: String
    var shopName: String
    var shopAddress: String
    var shopCordinates: CLLocationCoordinate2D
    var shopGenderCategory: String
    var locationCLL: CLLocation
    var longitudeDouble: Double
    var latitudeDouble: Double
    var shopLogoImage: UIImage
    var rating: Int
    var shopCurrency: String
    var shopVerification: String
    var shopCategory: String
    var shopDistanceFromDevice: Double
    
    init(
        
        shopId: String,
        shopName: String,
        shopAddress: String,
        shopCordinates: CLLocationCoordinate2D,
        shopGenderCategory: String,
        locationCLL: CLLocation,
        longitudeDouble: Double,
        latitudeDouble: Double,
        shopLogoImage: UIImage,
        rating: Int,
        shopCurrency: String,
        shopVerification: String,
        shopCategory: String,
        shopDistanceFromDevice: Double
        
        ) {
        
        self.shopId = shopId
        self.shopName = shopName
        self.shopAddress = shopAddress
        self.shopCordinates = shopCordinates
        self.shopGenderCategory = shopGenderCategory
        self.locationCLL = locationCLL
        self.longitudeDouble = longitudeDouble
        self.latitudeDouble = latitudeDouble
        self.shopLogoImage = shopLogoImage
        self.rating = rating
        self.shopCurrency = shopCurrency
        self.shopVerification = shopVerification
        self.shopCategory = shopCategory
        self.shopDistanceFromDevice = shopDistanceFromDevice
        
    }
}
