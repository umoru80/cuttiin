//
//  ProfileEditorViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/7/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import SwiftDate
import Alamofire
import Navajo_Swift
import SwiftSpinner

class ProfileEditorViewController: UIViewController, UIImagePickerControllerDelegate, UITextFieldDelegate, UINavigationControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    let picker = UIImagePickerController()
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    var emailValid = false
    var passwordValid = false
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    var authenticationType = "email"
    var dateSelected: String?
    var genderSelected: String?
    var genderTypes = ["male", "female"]
    var genderTransTypes = [GenderTrans] ()
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    lazy var customerProfileViewIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8profileiconplaceholder")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var customerProfileCloseViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(exitThisView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let customerProfilePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("profileEditorViewControllerTitle", comment: "Profile")
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 20) // HelveticaNeue-Medium
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var saveAndUpdateButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitle(NSLocalizedString("profileEditorViewControllerSaveButton", comment: "Save"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        st.isEnabled = true
        st.isUserInteractionEnabled = true
        return st
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    let imageAndFirstLastContainerView: UIView = {
        let iascview = UIView()
        iascview.translatesAutoresizingMaskIntoConstraints = false
        return iascview
    }()
    
    lazy var selectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .center
        imageView.image = UIImage(named: "profile_passive")
        imageView.setImageColor(color: UIColor(r: 11, g: 49, b: 68))
        imageView.layer.cornerRadius = 5
        imageView.layer.masksToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showCameraActionOptions)))
        return imageView
    }()
    
    lazy var selectImageIconButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8editphotoicon")
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = UIColor.white
        return imageView
    }()
    
    let firstNameHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("profileEditorViewControllerFirstNameEdit", comment: "first name")
        fnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.isHidden = true
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let firstNameTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.placeholder = NSLocalizedString("profileEditorViewControllerFirstNameEdit", comment: "first name")
        em.addTarget(self, action: #selector(firstNametextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(firstNametextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let firstNameSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return fnsv
    }()
    
    let lastNameHiddenPlaceHolder: UILabel = {
        let lnhp = UILabel()
        lnhp.translatesAutoresizingMaskIntoConstraints = false
        lnhp.text = NSLocalizedString("profileEditorViewControllerLastNameEdit", comment: "last name")
        lnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        lnhp.textColor = UIColor.black
        lnhp.isHidden = true
        lnhp.adjustsFontSizeToFitWidth = true
        lnhp.minimumScaleFactor = 0.1
        lnhp.baselineAdjustment = .alignCenters
        lnhp.textAlignment = .left
        return lnhp
    }()
    
    let lastNameTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.placeholder = NSLocalizedString("profileEditorViewControllerLastNameEdit", comment: "last name")
        em.addTarget(self, action: #selector(lastNametextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(lastNametextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let lastNameSeperatorView: UIView = {
        let lnsv = UIView()
        lnsv.translatesAutoresizingMaskIntoConstraints = false
        lnsv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return lnsv
    }()
    
    let firstNameLastNameContainerView: UIView = {
        let fnlncview = UIView()
        fnlncview.translatesAutoresizingMaskIntoConstraints = false
        return fnlncview
    }()
    
    let emailHiddenPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("profileEditorViewControllerEmailEdit", comment: "email")
        ehp.font = UIFont(name: "OpenSans-Light", size: 10)
        ehp.textColor = UIColor.black
        ehp.isHidden = true
        ehp.adjustsFontSizeToFitWidth = true
        ehp.minimumScaleFactor = 0.1
        ehp.baselineAdjustment = .alignCenters
        ehp.textAlignment = .left
        return ehp
    }()
    
    let emailTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.placeholder = NSLocalizedString("profileEditorViewControllerEmailEdit", comment: "email")
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.autocapitalizationType = .none
        em.keyboardType = .emailAddress
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(isValidEmail), for: .editingChanged)
        return em
    }()
    
    let emailSeperatorView: UIView = {
        let esv = UIView()
        esv.translatesAutoresizingMaskIntoConstraints = false
        esv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return esv
    }()
    
    let passwordHiddenPlaceHolder: UILabel = {
        let php = UILabel()
        php.translatesAutoresizingMaskIntoConstraints = false
        php.text = NSLocalizedString("profileEditorViewControllerPasswordEditMessage", comment: "password must have 8 characters or more with characters and digits")
        php.font = UIFont(name: "OpenSans-Light", size: 10)
        php.textColor = UIColor.black
        php.isHidden = true
        php.adjustsFontSizeToFitWidth = true
        php.minimumScaleFactor = 0.1
        php.baselineAdjustment = .alignCenters
        php.textAlignment = .left
        return php
    }()
    
    let passwordTextField: UITextField = {
        let ps = UITextField()
        ps.translatesAutoresizingMaskIntoConstraints = false
        ps.textColor = UIColor.black
        ps.placeholder = NSLocalizedString("profileEditorViewControllerPasswordEdit", comment: "password")
        ps.font = UIFont(name: "OpenSans-Light", size: 15)
        ps.isSecureTextEntry = true
        ps.addTarget(self, action: #selector(isValidPassword), for: .editingChanged)
        ps.addTarget(self, action: #selector(passwordtextFieldDidChange), for: .editingDidBegin)
        ps.addTarget(self, action: #selector(passwordtextFieldDidChange), for: .editingChanged)
        return ps
    }()
    
    lazy var showPassordTextFieldButton: UIButton = {
        let sptfb = UIButton()
        sptfb.translatesAutoresizingMaskIntoConstraints = false
        sptfb.isUserInteractionEnabled = true
        sptfb.setTitle(NSLocalizedString("profileEditorViewControllerShowPasswordEdit", comment: "show"), for: .normal)
        sptfb.setTitleColor(UIColor.black, for: .normal)
        sptfb.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 12)
        sptfb.backgroundColor = UIColor.white
        sptfb.addTarget(self, action: #selector(handleShowPasswordText), for: .touchUpInside)
        return sptfb
    }()
    
    let passwordSeperatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return view
    }()
    
    let mobileNumberHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("profileEditorViewControllerMobileNumberEdit", comment: "mobile number")
        fnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        fnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        fnhp.isHidden = true
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let mobileNumberTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.placeholder = NSLocalizedString("profileEditorViewControllerMobileNumberEdit", comment: "mobile number")
        em.keyboardType = .phonePad
        em.addTarget(self, action: #selector(mobileNumbertextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(mobileNumbertextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let mobileNumberSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return fnsv
    }()
    
    let dateOfBirthHiddenPlaceHolder: UILabel = {
        let lnhp = UILabel()
        lnhp.translatesAutoresizingMaskIntoConstraints = false
        lnhp.text = NSLocalizedString("profileEditorViewControllerDateOfBirthEdit", comment: "date of birth")
        lnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        lnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        lnhp.isHidden = true
        lnhp.adjustsFontSizeToFitWidth = true
        lnhp.minimumScaleFactor = 0.1
        lnhp.baselineAdjustment = .alignCenters
        lnhp.textAlignment = .left
        return lnhp
    }()
    
    let dateOfBirthTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.placeholder = NSLocalizedString("profileEditorViewControllerDateOfBirthEdit", comment: "date of birth")
        em.addTarget(self, action: #selector(dateOfBirthtextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(dateOfBirthtextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let dateOfBirthSeperatorView: UIView = {
        let lnsv = UIView()
        lnsv.translatesAutoresizingMaskIntoConstraints = false
        lnsv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return lnsv
    }()
    
    let genderHiddenPlaceHolder: UILabel = {
        let lnhp = UILabel()
        lnhp.translatesAutoresizingMaskIntoConstraints = false
        lnhp.text = NSLocalizedString("profileEditorViewControllerGenderEdit", comment: "Gender")
        lnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        lnhp.textColor = UIColor(r: 129, g: 129, b: 129)
        lnhp.isHidden = true
        lnhp.adjustsFontSizeToFitWidth = true
        lnhp.minimumScaleFactor = 0.1
        lnhp.baselineAdjustment = .alignCenters
        lnhp.textAlignment = .left
        return lnhp
    }()
    
    let genderTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.placeholder = NSLocalizedString("profileEditorViewControllerGenderEdit", comment: "Gender")
        em.addTarget(self, action: #selector(gendertextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(gendertextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let genderSeperatorView: UIView = {
        let lnsv = UIView()
        lnsv.translatesAutoresizingMaskIntoConstraints = false
        lnsv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return lnsv
    }()
    
    let dateOfBirthDatePicker: UIDatePicker = {
        let opentime = UIDatePicker()
        opentime.translatesAutoresizingMaskIntoConstraints = false
        opentime.datePickerMode = .date
        let currentDate = Date()
        opentime.date = currentDate
        opentime.addTarget(self, action: #selector(dateChanged(datePicker:)), for: .valueChanged)
        return opentime
    }()
    
    let genderPicker: UIPickerView = {
        let gender = UIPickerView()
        gender.translatesAutoresizingMaskIntoConstraints = false
        return gender
    }()
    
    let errorMessagePlaceHolder: UILabel = {
        let emhp = UILabel()
        emhp.translatesAutoresizingMaskIntoConstraints = false
        emhp.font = UIFont(name: "OpenSans-Light", size: 12)
        emhp.textColor = UIColor.black
        emhp.adjustsFontSizeToFitWidth = true
        emhp.minimumScaleFactor = 0.1
        emhp.numberOfLines = 0
        emhp.baselineAdjustment = .alignCenters
        emhp.textAlignment = .center
        return emhp
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        self.mobileNumberTextField.delegate = self
        view.backgroundColor = UIColor.white
        
        view.addSubview(customerProfileCloseViewButton)
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        
        view.addSubview(imageAndFirstLastContainerView)
        view.addSubview(firstNameLastNameContainerView)
        view.addSubview(errorMessagePlaceHolder)
        setupViewConstriants()
        dateOfBirthTextField.inputView = dateOfBirthDatePicker
        self.genderPicker.delegate = self
        self.genderTextField.inputView = self.genderPicker
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(gestureRecognizer:)))
        view.addGestureRecognizer(tapGesture)
        self.handleGenderData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @objc func exitThisView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func handleGenderData() {
        let valueGenderFirst = GenderTrans(translated: NSLocalizedString("profileEditorViewControllerGenderMale", comment: "male"), original: "male")
        let valueGenderSecond = GenderTrans(translated: NSLocalizedString("profileEditorViewControllerGenderFemale", comment: "female"), original: "female")
        self.genderTransTypes.append(valueGenderFirst)
        self.genderTransTypes.append(valueGenderSecond)
    }
    
    @objc func dateChanged(datePicker: UIDatePicker) {
        let userRegion = DateByUserDeviceInitializer.getRegion(TZoneName: DateByUserDeviceInitializer.tzone, calenName: "gregorian", LocName: "en")
        self.dateSelected = DateInRegion(datePicker.date, region: userRegion).toFormat("yyyy-MM-dd HH:mm:ss")
        let dateSelectedToShow = DateInRegion(datePicker.date, region: userRegion).toFormat("yyyy-MM-dd")
        self.dateOfBirthTextField.text = dateSelectedToShow
    }
    
    @objc func dismissKeyboard(gestureRecognizer: UITapGestureRecognizer){
        view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return string == numberFiltered
    }
    
    //picker view functions
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.genderTransTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.genderTransTypes[row].translated
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.genderSelected = self.genderTransTypes[row].original
        self.genderTextField.text = self.genderTransTypes[row].translated
    }
    
    
    @objc func getUserData(){
        DispatchQueue.global(qos: .background).async {
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let customerID = decodedCustomer.first?.customerId, let token = decodedCustomer.first?.token {
                    
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        DispatchQueue.main.async {
                            SwiftSpinner.show("Loading...")
                        }
                        AF.request(self.BACKEND_URL + customerID, method: .get, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    do {
                                        let customerSingleData = try JSONDecoder().decode(CustomerData.self, from: jsonData)
                                        let profileImageFileName = customerSingleData.customer.profileImageName
                                        
                                        DispatchQueue.main.async {
                                            self.firstNameTextField.text = customerSingleData.customer.firstName
                                            self.lastNameTextField.text = customerSingleData.customer.lastName
                                            self.emailTextField.text = customerSingleData.customer.email
                                            let authType = customerSingleData.customer.authenticationType
                                            if authType == "facebook" {
                                                print("facebook hello")
                                                self.showPassordTextFieldButton.isHidden = true
                                                self.showPassordTextFieldButton.heightAnchor.constraint(equalToConstant: 0).isActive = true
                                                self.passwordTextField.isHidden = true
                                                self.passwordTextField.heightAnchor.constraint(equalToConstant: 0).isActive = true
                                                self.passwordSeperatorView.isHidden = true
                                                self.passwordSeperatorView.heightAnchor.constraint(equalToConstant: 0).isActive = true
                                                self.passwordHiddenPlaceHolder.isHidden = true
                                                self.passwordHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 0).isActive = true
                                                self.saveAndUpdateButton.addTarget(self, action: #selector(self.textFieldDidChangeFacebook), for: .touchUpInside)
                                            } else {
                                                print("email hello")
                                                self.saveAndUpdateButton.addTarget(self, action: #selector(self.textFieldDidChange), for: .touchUpInside)
                                            }
                                            
                                            if customerSingleData.customer.dateOfBirthTimezone != "not available" || customerSingleData.customer.gender != "not available" || customerSingleData.customer.mobileNumber != "not available"{
                                                let region = DateByUserDeviceInitializer.getRegion(TZoneName: customerSingleData.customer.dateOfBirthTimezone, calenName: customerSingleData.customer.dateOfBirthCalendar, LocName: customerSingleData.customer.dateOfBirthLocale)
                                                let dob = customerSingleData.customer.dateOfBirth.toDate("yyyy-MM-dd HH:mm:ss", region: region)
                                                self.dateSelected = dob?.toFormat("yyyy-MM-dd HH:mm:ss")
                                                self.dateOfBirthTextField.text = dob?.toFormat("yyyy-MM-dd")
                                                
                                                if let customerGender = self.genderTransTypes.first(where: {$0.original == customerSingleData.customer.gender}) {
                                                    self.genderTextField.text = customerGender.translated
                                                    self.genderSelected = customerGender.original
                                                }
                                                self.mobileNumberTextField.text = customerSingleData.customer.mobileNumber
                                            }
                                        }
                                        
                                        
                                        AF.request(self.BACKEND_URL + "images/customerImages/" + profileImageFileName, headers: headers).responseData { response in
                                            
                                            DispatchQueue.main.async {
                                                SwiftSpinner.hide()
                                            }
                                            
                                            switch response.result {
                                            case .success(let data):
                                                let image = UIImage(data: data)
                                                DispatchQueue.main.async {
                                                    self.selectImageView.image = image
                                                    self.selectImageView.contentMode = .scaleAspectFit
                                                }
                                                break
                                            case .failure(let error):
                                                print(error)
                                                self.view.makeToast(NSLocalizedString("profileEditorViewControllerErrorMessageImageNotFound", comment: "Image not found"), duration: 3.0, position: .bottom)
                                                break
                                            }
                                            
                                        }
                                        
                                    } catch _ {
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        
                                    }
                                }
                        }
                        
                    }
                }
            }
        }
    }
    
    @objc func updateDateCustomerData(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let customerID = decodedCustomer.first?.customerId, let token = decodedCustomer.first?.token {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    var pass = "not available"
                    if let passwordString = self.passwordTextField.text {
                        pass = passwordString
                    }
                    
                    if self.firstNameTextField.text != "" && self.lastNameTextField.text != "" && self.emailTextField.text != "" && self.mobileNumberTextField.text != "" && self.dateOfBirthTextField.text != "" && self.genderTextField.text != "" {
                        if let fname = firstNameTextField.text, let lname = lastNameTextField.text, let mnumber = mobileNumberTextField.text, let dob = self.dateSelected, let gender = self.genderSelected, let email = self.emailTextField.text, let profileImage = self.selectImageView.image, let uploadData = profileImage.jpegData(compressionQuality: 0.1) {
                            SwiftSpinner.show("Loading...")
                            
                            let imageName = NSUUID().uuidString
                            let userTimezone = DateByUserDeviceInitializer.tzone
                            let userCalender = "gregorian"
                            
                            let parameters = ["firstName": fname,
                                              "lastName": lname,
                                              "email": email,
                                              "password": pass,
                                              "mobileNumber": mnumber,
                                              "dateOfBirth": dob,
                                              "dateOfBirthTimezone": userTimezone,
                                              "dateOfBirthCalendar": userCalender,
                                              "dateOfBirthLocale": "en",
                                              "gender": gender] as [String : Any]
                            
                            print(parameters)
                            print(gender)
                            DispatchQueue.global(qos: .background).async {
                                
                                AF.upload(multipartFormData: { (multipartFormData) in
                                    multipartFormData.append(uploadData, withName: "image",fileName: imageName, mimeType: "image/jpg")
                                    for (key, value) in parameters {
                                        if let stringValue = value as? String {
                                            multipartFormData.append(stringValue.data(using: .utf8)!, withName: key)
                                        }
                                    }
                                }, usingThreshold: UInt64.init(), fileManager: .default, to: self.BACKEND_URL + customerID, method: .put, headers: headers, interceptor: nil)
                                    .validate(statusCode: 200..<501)
                                    .validate(contentType: ["application/json"])
                                    .uploadProgress(queue: .main, closure: { (progress) in
                                        print("Upload Progress: \(progress.fractionCompleted)")
                                    }).response { response in
                                        
                                        switch response.result {
                                        case .success(let data):
                                            DispatchQueue.main.async {
                                                SwiftSpinner.hide()
                                            }
                                            if let jsonData = data {
                                                do {
                                                    //let serverResponse = try JSONDecoder().decode(ServerMessage.self, from: jsonData)
                                                    // profileEditorViewControllerMessageSuccesss
                                                    let message = NSLocalizedString("profileEditorViewControllerMessageSuccesss", comment: "profile successfully updated")
                                                    self.view.makeToast(message, duration: 3.0, position: .bottom)
                                                } catch _ {
                                                    if let statusCode = response.response?.statusCode {
                                                        print(statusCode)
                                                        switch statusCode {
                                                        case 404:
                                                            self.view.makeToast(NSLocalizedString("profileEditorViewControllerErrorMessageCustomereNotFound", comment: "Customer was not found"), duration: 3.0, position: .bottom)
                                                        case 500:
                                                            self.view.makeToast(NSLocalizedString("profileEditorViewControllerErrorMessageErrorOccured", comment: "An error occurred, please try again later"), duration: 3.0, position: .bottom)
                                                        default:
                                                            print("Sky high : update")
                                                        }
                                                    }
                                                }
                                            }
                                            break
                                        case .failure(let error):
                                            DispatchQueue.main.async {
                                                SwiftSpinner.hide()
                                            }
                                            print(error)
                                            let message = NSLocalizedString("profileEditorViewControllerErrorMessageUpdateError", comment: "Update error: please try again later")
                                            self.view.makeToast(message, duration: 3.0, position: .bottom)
                                            break
                                        }
                                }
                            }
                            
                        }
                    }
                }
                
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @objc private func textFieldDidChange(){
        self.errorMessagePlaceHolder.text = ""
        view.endEditing(true)
        self.isValidEmail()
        if self.firstNameTextField.text == "" && self.lastNameTextField.text == "" && self.emailTextField.text == "" && self.passwordTextField.text == "" && self.mobileNumberTextField.text == "" && self.dateOfBirthTextField.text == "" && self.genderTextField.text == "" {
            self.errorMessagePlaceHolder.text = NSLocalizedString("profileEditorViewControllerErrorMessageIncompleteForm", comment: "incomplete form")
            
        } else {
            //Enable button
            if self.emailValid == true && self.passwordValid == true {
                self.updateDateCustomerData()
            } else {
                var message = NSLocalizedString("profileEditorViewControllerErrorMessagePleaseInsert", comment: "please insert ")
                
                if (self.emailValid == false) {
                    message = message + NSLocalizedString("profileEditorViewControllerErrorMessageValidEmail", comment: " * a valid email ")
                }
                
                if (self.passwordValid == false ) {
                    message = message + NSLocalizedString("profileEditorViewControllerErrorMessageValidPassword", comment: " * a valid password ")
                }
                
                if (self.firstNameTextField.text == ""){
                    message = message + NSLocalizedString("profileEditorViewControllerErrorMessageValidFirstName", comment: " * a first name ")
                }
                
                if (self.lastNameTextField.text == ""){
                    message = message + NSLocalizedString("profileEditorViewControllerErrorMessageValidLastName", comment: " * a last name ")
                }
                
                if (self.mobileNumberTextField.text == ""){
                    message = message + NSLocalizedString("profileEditorViewControllerErrorMessageValidMobileNumber", comment: " * a mobile number ")
                }
                
                if (self.dateOfBirthTextField.text == ""){
                    message = message + NSLocalizedString("profileEditorViewControllerErrorMessageValidDateOfBirth", comment: " * a date of birth ")
                }
                
                if (self.genderTextField.text == ""){
                    message = message + NSLocalizedString("profileEditorViewControllerErrorMessageValidGender", comment: " * a gender ")
                }
                
                self.errorMessagePlaceHolder.text = message
            }
        }
    }
    
    @objc func textFieldDidChangeFacebook(){
        print("yams")
        self.errorMessagePlaceHolder.text = ""
        view.endEditing(true)
        self.isValidEmail()
        if self.firstNameTextField.text == "" && self.lastNameTextField.text == "" && self.emailTextField.text == "" && self.mobileNumberTextField.text == "" && self.dateOfBirthTextField.text == "" && self.genderTextField.text == "" {
            self.errorMessagePlaceHolder.text = NSLocalizedString("profileEditorViewControllerErrorMessageIncompleteForm", comment: "incomplete form")
            
        } else {
            //Enable button
            if self.emailValid == true {
                self.updateDateCustomerData()
            } else {
                var message = NSLocalizedString("profileEditorViewControllerErrorMessagePleaseInsert", comment: "please insert ")
                
                if (self.emailValid == false) {
                    message = message + NSLocalizedString("profileEditorViewControllerErrorMessageValidEmail", comment: " * a valid email ")
                }
                
                if (self.firstNameTextField.text == ""){
                    message = message + NSLocalizedString("profileEditorViewControllerErrorMessageValidFirstName", comment: " * a first name ")
                }
                
                if (self.lastNameTextField.text == ""){
                    message = message + NSLocalizedString("profileEditorViewControllerErrorMessageValidLastName", comment: " * a last name ")
                }
                
                if (self.mobileNumberTextField.text == ""){
                    message = message + NSLocalizedString("profileEditorViewControllerErrorMessageValidMobileNumber", comment: " * a mobile number ")
                }
                
                if (self.dateOfBirthTextField.text == ""){
                    message = message + NSLocalizedString("profileEditorViewControllerErrorMessageValidDateOfBirth", comment: " * a date of birth ")
                }
                
                if (self.genderTextField.text == ""){
                    message = message + NSLocalizedString("profileEditorViewControllerErrorMessageValidGender", comment: " * a gender ")
                }
                
                self.errorMessagePlaceHolder.text = message
            }
        }
    }
    
    @objc func firstNametextFieldDidChange(){
        self.firstNameSeperatorView.backgroundColor = UIColor.black
        self.lastNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.emailSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.mobileNumberSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.dateOfBirthSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.genderSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.passwordSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.firstNameHiddenPlaceHolder.isHidden = false
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = true
        self.mobileNumberHiddenPlaceHolder.isHidden = true
        self.dateOfBirthHiddenPlaceHolder.isHidden = true
        self.genderHiddenPlaceHolder.isHidden = true
        self.passwordHiddenPlaceHolder.isHidden = true
    }
    
    @objc func lastNametextFieldDidChange(){
        self.lastNameSeperatorView.backgroundColor = UIColor.black
        self.firstNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.emailSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.mobileNumberSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.dateOfBirthSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.genderSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.passwordSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = false
        self.emailHiddenPlaceHolder.isHidden = true
        self.mobileNumberHiddenPlaceHolder.isHidden = true
        self.dateOfBirthHiddenPlaceHolder.isHidden = true
        self.genderHiddenPlaceHolder.isHidden = true
        self.passwordHiddenPlaceHolder.isHidden = true
    }
    
    @objc func emailtextFieldDidChange(){
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.firstNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.lastNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.mobileNumberSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.dateOfBirthSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.genderSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.passwordSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = false
        self.mobileNumberHiddenPlaceHolder.isHidden = true
        self.dateOfBirthHiddenPlaceHolder.isHidden = true
        self.genderHiddenPlaceHolder.isHidden = true
        self.passwordHiddenPlaceHolder.isHidden = true
    }
    
    @objc func passwordtextFieldDidChange(){
        self.passwordSeperatorView.backgroundColor = UIColor.black
        self.emailSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.firstNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.lastNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.mobileNumberSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.dateOfBirthSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.genderSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = true
        self.mobileNumberHiddenPlaceHolder.isHidden = true
        self.dateOfBirthHiddenPlaceHolder.isHidden = true
        self.genderHiddenPlaceHolder.isHidden = true
        self.passwordHiddenPlaceHolder.isHidden = false
        
    }
    
    @objc func mobileNumbertextFieldDidChange(){
        self.emailSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.firstNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.lastNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.mobileNumberSeperatorView.backgroundColor = UIColor.black
        self.dateOfBirthSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.genderSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.passwordSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = true
        self.mobileNumberHiddenPlaceHolder.isHidden = false
        self.dateOfBirthHiddenPlaceHolder.isHidden = true
        self.genderHiddenPlaceHolder.isHidden = true
        self.passwordHiddenPlaceHolder.isHidden = true
    }
    
    @objc func dateOfBirthtextFieldDidChange(){
        self.emailSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.firstNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.lastNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.mobileNumberSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.dateOfBirthSeperatorView.backgroundColor = UIColor.black
        self.genderSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.passwordSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = true
        self.mobileNumberHiddenPlaceHolder.isHidden = true
        self.dateOfBirthHiddenPlaceHolder.isHidden = false
        self.genderHiddenPlaceHolder.isHidden = true
        self.passwordHiddenPlaceHolder.isHidden = true
    }
    
    @objc func gendertextFieldDidChange(){
        self.emailSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.firstNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.lastNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.mobileNumberSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.dateOfBirthSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.genderSeperatorView.backgroundColor = UIColor.black
        self.passwordSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.firstNameHiddenPlaceHolder.isHidden = true
        self.lastNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = true
        self.mobileNumberHiddenPlaceHolder.isHidden = true
        self.dateOfBirthHiddenPlaceHolder.isHidden = true
        self.genderHiddenPlaceHolder.isHidden = false
        self.passwordHiddenPlaceHolder.isHidden = true
    }
    
    @objc func isValidEmail() {
        let testStr = emailTextField.text ?? ""
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        if result == true {
            self.emailValid = true
            self.emailHiddenPlaceHolder.text = NSLocalizedString("profileEditorViewControllerEmailEdit", comment: "email")
        }else{
            self.emailValid = false
            self.emailHiddenPlaceHolder.text = NSLocalizedString("profileEditorViewControllerErrorMessageEmailNotFormattedProperly", comment: "Email is not properly formatted")
        }
    }
    
    @objc func handleShowPasswordText(){
        self.passwordTextField.isSecureTextEntry = false
    }
    
    @objc func isValidPassword(){
        let password = passwordTextField.text ?? ""
        let strength = Navajo.strength(ofPassword: password)
        let strengthValue = Navajo.localizedString(forStrength: strength)
        
        if (strengthValue == "Strong" || strengthValue == "Very Strong" || strengthValue == "Reasonable"){
            print("shoes")
            self.passwordValid = true
            switch strengthValue {
            case "Strong":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("passwordStrengthCustomerRegistrationView", comment: "Strong password")
            case "Very Strong":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("veryStrongPasswordStrengthCustomerRegistrationView", comment: "Very Strong password")
            case "Reasonable":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("reasonablePasswordStrengthCustomerRegistrationView", comment: "Reasonable password")
            default:
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("weakPasswordStrengthCustomerRegistrationView", comment: "weak password - password must have 6 characters or more with letters and numbers")
            }
        }else{
            self.passwordValid = false
            switch strengthValue {
            case "Strong":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("passwordStrengthCustomerRegistrationView", comment: "Strong password")
            case "Very Strong":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("veryStrongPasswordStrengthCustomerRegistrationView", comment: "Very Strong password")
            case "Reasonable":
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("reasonablePasswordStrengthCustomerRegistrationView", comment: "Reasonable password")
            default:
                self.passwordHiddenPlaceHolder.text = NSLocalizedString("weakPasswordStrengthCustomerRegistrationView", comment: "weak password - password must have 6 characters or more with letters and numbers")
            }
        }
    }
    
    @objc func showCameraActionOptions(){
        self.errorMessagePlaceHolder.text = ""
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("cancelButtonAlertOrderDetail", comment: "Cancel"), style: .cancel) { action in
        }
        alertController.addAction(cancelAction)
        
        let takePhotoAction = UIAlertAction(title: NSLocalizedString("takePhotoAlertViewProfileEdit", comment: "Take photo"), style: .default) { action in
            self.handleTakePhoto()
        }
        alertController.addAction(takePhotoAction)
        
        let choosePhotoAction = UIAlertAction(title: NSLocalizedString("choosePhotAlertViewProfileEdit", comment: "Choose photo"), style: .default) { action in
            self.handleChoosePhoto()
        }
        alertController.addAction(choosePhotoAction)
        
        self.present(alertController, animated: true) {
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            
            selectedImageFromPicker = editedImage
            
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            
            selectedImageFromPicker = originalImage
        }
        
        if let selectedImage = selectedImageFromPicker {
            self.selectImageView.contentMode = .scaleAspectFit
            self.selectImageView.image = selectedImage
            navigationItem.rightBarButtonItem?.isEnabled = true
        }
        
        dismiss(animated:true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func handleTakePhoto(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = true
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            self.errorMessagePlaceHolder.text = NSLocalizedString("errorMessageNoCameraChoosePhotoView", comment: "Sorry, this device has no camera")
        }
    }
    
    func handleChoosePhoto(){
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    
    func setupViewConstriants(){
        var topDistance: CGFloat = 20
        if UIDevice.current.hasNotch {
            topDistance = 60
        }
        
        
        customerProfileCloseViewButton.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        customerProfileCloseViewButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -15).isActive = true
        customerProfileCloseViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        customerProfileCloseViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperInputsContainerView.topAnchor.constraint(equalTo: customerProfileCloseViewButton.bottomAnchor).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(customerProfileViewIcon)
        upperInputsContainerView.addSubview(customerProfilePlaceHolder)
        upperInputsContainerView.addSubview(saveAndUpdateButton)
        
        customerProfileViewIcon.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        customerProfileViewIcon.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor).isActive = true
        customerProfileViewIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        customerProfileViewIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        saveAndUpdateButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        saveAndUpdateButton.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        saveAndUpdateButton.widthAnchor.constraint(equalToConstant: 60).isActive = true
        saveAndUpdateButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        customerProfilePlaceHolder.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        customerProfilePlaceHolder.leftAnchor.constraint(equalTo: customerProfileViewIcon.rightAnchor, constant: 10).isActive = true
        customerProfilePlaceHolder.rightAnchor.constraint(equalTo: saveAndUpdateButton.leftAnchor).isActive = true
        customerProfilePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        
        
        imageAndFirstLastContainerView.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 10).isActive = true
        imageAndFirstLastContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageAndFirstLastContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        imageAndFirstLastContainerView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        imageAndFirstLastContainerView.addSubview(selectImageView)
        
        selectImageView.topAnchor.constraint(equalTo: imageAndFirstLastContainerView.topAnchor).isActive = true
        selectImageView.centerXAnchor.constraint(equalTo: imageAndFirstLastContainerView.centerXAnchor).isActive = true
        selectImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        selectImageView.heightAnchor.constraint(equalTo: imageAndFirstLastContainerView.heightAnchor).isActive = true
        
        selectImageView.addSubview(selectImageIconButton)
        selectImageView.bringSubviewToFront(selectImageIconButton)
        
        selectImageIconButton.bottomAnchor.constraint(equalTo: selectImageView.bottomAnchor).isActive = true
        selectImageIconButton.rightAnchor.constraint(equalTo: selectImageView.rightAnchor).isActive = true
        selectImageIconButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        selectImageIconButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        firstNameLastNameContainerView.topAnchor.constraint(equalTo: imageAndFirstLastContainerView.bottomAnchor, constant: 5).isActive = true
        firstNameLastNameContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        firstNameLastNameContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        firstNameLastNameContainerView.heightAnchor.constraint(equalToConstant: 325).isActive = true
        
        firstNameLastNameContainerView.addSubview(firstNameHiddenPlaceHolder)
        firstNameLastNameContainerView.addSubview(firstNameTextField)
        firstNameLastNameContainerView.addSubview(firstNameSeperatorView)
        firstNameLastNameContainerView.addSubview(lastNameHiddenPlaceHolder)
        firstNameLastNameContainerView.addSubview(lastNameTextField)
        firstNameLastNameContainerView.addSubview(lastNameSeperatorView)
        firstNameLastNameContainerView.addSubview(emailHiddenPlaceHolder)
        firstNameLastNameContainerView.addSubview(emailTextField)
        firstNameLastNameContainerView.addSubview(emailSeperatorView)
        firstNameLastNameContainerView.addSubview(passwordHiddenPlaceHolder)
        firstNameLastNameContainerView.addSubview(passwordTextField)
        firstNameLastNameContainerView.addSubview(showPassordTextFieldButton)
        firstNameLastNameContainerView.addSubview(passwordSeperatorView)
        firstNameLastNameContainerView.addSubview(mobileNumberHiddenPlaceHolder)
        firstNameLastNameContainerView.addSubview(mobileNumberTextField)
        firstNameLastNameContainerView.addSubview(mobileNumberSeperatorView)
        firstNameLastNameContainerView.addSubview(dateOfBirthHiddenPlaceHolder)
        firstNameLastNameContainerView.addSubview(dateOfBirthTextField)
        firstNameLastNameContainerView.addSubview(dateOfBirthSeperatorView)
        firstNameLastNameContainerView.addSubview(genderHiddenPlaceHolder)
        firstNameLastNameContainerView.addSubview(genderTextField)
        firstNameLastNameContainerView.addSubview(genderSeperatorView)
        
        firstNameHiddenPlaceHolder.topAnchor.constraint(equalTo: firstNameLastNameContainerView.topAnchor).isActive = true
        firstNameHiddenPlaceHolder.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        firstNameHiddenPlaceHolder.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        firstNameHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        firstNameTextField.topAnchor.constraint(equalTo: firstNameHiddenPlaceHolder.bottomAnchor).isActive = true
        firstNameTextField.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        firstNameTextField.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        firstNameTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        firstNameSeperatorView.leftAnchor.constraint(equalTo: firstNameLastNameContainerView.leftAnchor).isActive = true
        firstNameSeperatorView.topAnchor.constraint(equalTo: firstNameTextField.bottomAnchor).isActive = true
        firstNameSeperatorView.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        firstNameSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        lastNameHiddenPlaceHolder.topAnchor.constraint(equalTo: firstNameSeperatorView.bottomAnchor, constant: 5).isActive = true
        lastNameHiddenPlaceHolder.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        lastNameHiddenPlaceHolder.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        lastNameHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        lastNameTextField.topAnchor.constraint(equalTo: lastNameHiddenPlaceHolder.bottomAnchor).isActive = true
        lastNameTextField.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        lastNameTextField.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        lastNameTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        lastNameSeperatorView.leftAnchor.constraint(equalTo: firstNameLastNameContainerView.leftAnchor).isActive = true
        lastNameSeperatorView.topAnchor.constraint(equalTo: lastNameTextField.bottomAnchor).isActive = true
        lastNameSeperatorView.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        lastNameSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        emailHiddenPlaceHolder.topAnchor.constraint(equalTo: lastNameSeperatorView.bottomAnchor, constant: 5).isActive = true
        emailHiddenPlaceHolder.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        emailHiddenPlaceHolder.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        emailHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        emailTextField.topAnchor.constraint(equalTo: emailHiddenPlaceHolder.bottomAnchor).isActive = true
        emailTextField.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        emailSeperatorView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor).isActive = true
        emailSeperatorView.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        emailSeperatorView.widthAnchor.constraint(equalTo: emailTextField.widthAnchor).isActive = true
        emailSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        passwordHiddenPlaceHolder.topAnchor.constraint(equalTo: emailSeperatorView.bottomAnchor, constant: 5).isActive = true
        passwordHiddenPlaceHolder.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        passwordHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        passwordHiddenPlaceHolder.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        
        passwordTextField.topAnchor.constraint(equalTo: passwordHiddenPlaceHolder.bottomAnchor).isActive = true
        passwordTextField.leftAnchor.constraint(equalTo: firstNameLastNameContainerView.leftAnchor).isActive = true
        passwordTextField.rightAnchor.constraint(equalTo: firstNameLastNameContainerView.rightAnchor, constant: -20).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        showPassordTextFieldButton.bottomAnchor.constraint(equalTo: passwordTextField.bottomAnchor).isActive = true
        showPassordTextFieldButton.rightAnchor.constraint(equalTo: passwordTextField.rightAnchor).isActive = true
        showPassordTextFieldButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        passwordSeperatorView.leftAnchor.constraint(equalTo: firstNameLastNameContainerView.leftAnchor).isActive = true
        passwordSeperatorView.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor).isActive = true
        passwordSeperatorView.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        passwordSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        
        
        
        mobileNumberHiddenPlaceHolder.topAnchor.constraint(equalTo: passwordSeperatorView.bottomAnchor, constant: 5).isActive = true
        mobileNumberHiddenPlaceHolder.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        mobileNumberHiddenPlaceHolder.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        mobileNumberHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        mobileNumberTextField.topAnchor.constraint(equalTo: mobileNumberHiddenPlaceHolder.bottomAnchor).isActive = true
        mobileNumberTextField.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        mobileNumberTextField.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        mobileNumberTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        mobileNumberSeperatorView.topAnchor.constraint(equalTo: mobileNumberTextField.bottomAnchor).isActive = true
        mobileNumberSeperatorView.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        mobileNumberSeperatorView.widthAnchor.constraint(equalTo: mobileNumberTextField.widthAnchor).isActive = true
        mobileNumberSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        dateOfBirthHiddenPlaceHolder.topAnchor.constraint(equalTo: mobileNumberSeperatorView.bottomAnchor, constant: 5).isActive = true
        dateOfBirthHiddenPlaceHolder.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        dateOfBirthHiddenPlaceHolder.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        dateOfBirthHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        dateOfBirthTextField.topAnchor.constraint(equalTo: dateOfBirthHiddenPlaceHolder.bottomAnchor).isActive = true
        dateOfBirthTextField.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        dateOfBirthTextField.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        dateOfBirthTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        dateOfBirthSeperatorView.topAnchor.constraint(equalTo: dateOfBirthTextField.bottomAnchor).isActive = true
        dateOfBirthSeperatorView.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        dateOfBirthSeperatorView.widthAnchor.constraint(equalTo: dateOfBirthTextField.widthAnchor).isActive = true
        dateOfBirthSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        genderHiddenPlaceHolder.topAnchor.constraint(equalTo: dateOfBirthSeperatorView.bottomAnchor, constant: 5).isActive = true
        genderHiddenPlaceHolder.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        genderHiddenPlaceHolder.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        genderHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        genderTextField.topAnchor.constraint(equalTo: genderHiddenPlaceHolder.bottomAnchor).isActive = true
        genderTextField.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        genderTextField.widthAnchor.constraint(equalTo: firstNameLastNameContainerView.widthAnchor).isActive = true
        genderTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        genderSeperatorView.topAnchor.constraint(equalTo: genderTextField.bottomAnchor).isActive = true
        genderSeperatorView.centerXAnchor.constraint(equalTo: firstNameLastNameContainerView.centerXAnchor).isActive = true
        genderSeperatorView.widthAnchor.constraint(equalTo: genderTextField.widthAnchor).isActive = true
        genderSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        errorMessagePlaceHolder.topAnchor.constraint(equalTo: firstNameLastNameContainerView.bottomAnchor, constant: 10).isActive = true
        errorMessagePlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        errorMessagePlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        errorMessagePlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        getUserData()
        
    }

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}
