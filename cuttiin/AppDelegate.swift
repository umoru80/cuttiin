//
//  AppDelegate.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/12/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FacebookCore
import FBSDKCoreKit
import FBSDKLoginKit
import GooglePlaces
import GoogleMaps
import SwiftDate
import Pushy
import Alamofire
import NotificationBannerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // facebook
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        // TESTING id
        GMSPlacesClient.provideAPIKey("AIzaSyD0ZRN_cfgRKTV2IWyYQcvcVqw_e6Czy-0") //AIzaSyAAihXBu8GqOKm33Y0iHYjzWMxpGhj4YjM
        GMSServices.provideAPIKey("AIzaSyD0ZRN_cfgRKTV2IWyYQcvcVqw_e6Czy-0") //AIzaSyAAihXBu8GqOKm33Y0iHYjzWMxpGhj4YjM   AIzaSyAD2rXeUq1JnobNVY0OVti-YOlckJ-GIIU
        
        
        //Rootview controller song and dance
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.rootViewController = MainNavigationController()
        //UINavigationController(rootViewController: TestViewController())
        
        //navigation bar tint color affects the buttons
        UINavigationBar.appearance().tintColor = UIColor.black
        
        //naviagtion bar title text font
        UINavigationBar.appearance().titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: "OpenSans-SemiBold", size: 17)!]
        
        //naviagtion bar opaque
        UINavigationBar.appearance().isTranslucent = false
        
        //IQRKeyboard setup
        IQKeyboardManager.shared.enable = true
        
        //swiftdate
        SwiftDate.defaultRegion = Region.local
        
        //push notification registration pushy
        
        // Initialize Pushy SDK
        let pushy = Pushy(application)
        // Register the device for push notifications
        
        pushy.register { (error, deviceToken) in
            
            // Handle registration errors
            if error != nil {
                return print ("Registration failed: \(error!)")
            }
            
            // Print device token to console
            print("Pushy device token: \(deviceToken)")
            
            // Persist the token locally and send it to your backend later
            UserDefaults.standard.set(deviceToken, forKey: "pushyToken")
        }
        
        pushy.setNotificationHandler { (data, completionHandler) in
            // Print notification payload data
            print("Received notification: \(data)")
            
            // Fallback message containing data payload
            var message = "\(data)"
            
            if let des = data["designation"] as? String, let id = data["appointmentID"] as? String {
                print("designation: \(id)")
                //UserDefaults.standard.set(1, forKey: "applicationSetBadgeNumberCouter")
                if (des == "appointmentCompletion") {
                    UserDefaults.standard.set(id, forKey: "appointmentRatingStarterIDHold")
                    if let topController = UIApplication.topViewController() {
                        if let appointmentID = UserDefaults.standard.object(forKey: "appointmentRatingStarterIDHold") as? String {
                            print("Appointment id ", appointmentID)
                            let shopstaffrating = ShopStaffRatingViewController()
                            shopstaffrating.appointmentID = appointmentID
                            shopstaffrating.modalPresentationStyle = .overCurrentContext
                            topController.present(shopstaffrating, animated: true, completion: nil)
                            UserDefaults.standard.removeObject(forKey: "appointmentRatingStarterIDHold")
                        }
                    }
                }
            }
            
            // Attempt to extract "message" key from APNs payload
            if let aps = data["aps"] as? [AnyHashable : Any],
                let payloadMessage = aps["alert"] as? String {
                message = payloadMessage
            }
            
            let banner = GrowingNotificationBanner(title: "incoming message", subtitle: message, style: .info)
            banner.show()
            completionHandler(UIBackgroundFetchResult.newData)
        }
        
        print("pushy check: ", pushy.isRegistered())
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return ApplicationDelegate.shared.application(app, open: url, options: options)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        application.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

