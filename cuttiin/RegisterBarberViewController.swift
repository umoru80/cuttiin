//
//  RegisterBarberViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/17/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Navajo_Swift
import SwiftDate
import GooglePlaces
import Alamofire

class RegisterBarberViewController: UIViewController, UITextFieldDelegate {
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    var emailValid = false
    var companyAddressLong: String?
    var companyAddressLat: String?
    var country: String?
    var countryCode: String?
    var countryCurrencyCode: String?
    var customerID: String?
    var genderSelected: String?
    var shopCategorySelelcted: String?
    var genderTypes = ["male", "female", "unisex"]
    var shopCategoryTypes = [GenderTrans] ()
    var shopCategory = ["Barbershop", "Hair salon", "Nail salon", "Spa", "Tattoo", "Massage", "Piercings", "Beauty salon"];
    var locationManager = CLLocationManager()
    var currentAddress: String?
    var placeMarkData: CLPlacemark?
    var currentUserLocation: CLLocation?
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    lazy var registerBarberViewCloseViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleBackAction)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let registerBarberViewTitlePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("barberShopProfileEditorViewControllerViewTitle", comment: "shop registration")
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    let inputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    let companyNameHiddenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("barberShopProfileEditorViewControllerShopName", comment: "shop name")
        fnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        fnhp.textColor = UIColor.black
        fnhp.isHidden = true
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let companyNameTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.placeholder = NSLocalizedString("barberShopProfileEditorViewControllerShopName", comment: "shop name")
        em.addTarget(self, action: #selector(companyNametextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(companyNametextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let companyNameSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return fnsv
    }()
    
    let emailHiddenPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("barberShopProfileEditorViewControllerEmail", comment: "email")
        ehp.font = UIFont(name: "OpenSans-Light", size: 10)
        ehp.textColor = UIColor.black
        ehp.isHidden = true
        ehp.adjustsFontSizeToFitWidth = true
        ehp.minimumScaleFactor = 0.1
        ehp.baselineAdjustment = .alignCenters
        ehp.textAlignment = .left
        return ehp
    }()
    
    let emailTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.placeholder = NSLocalizedString("barberShopProfileEditorViewControllerEmail", comment: "email")
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.autocapitalizationType = .none
        em.keyboardType = .emailAddress
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingChanged)
        em.addTarget(self, action: #selector(isValidEmail), for: .editingChanged)
        return em
    }()
    
    let emailSeperatorView: UIView = {
        let esv = UIView()
        esv.translatesAutoresizingMaskIntoConstraints = false
        esv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return esv
    }()
    
    let mobileNumberInputView: UIView = {
        let mniv = UIView()
        mniv.translatesAutoresizingMaskIntoConstraints = false
        return mniv
    }()
    
    let mobileNumberHiddenPlaceHolder: UILabel = {
        let cvrhp = UILabel()
        cvrhp.translatesAutoresizingMaskIntoConstraints = false
        cvrhp.text = NSLocalizedString("barberShopProfileEditorViewControllerMobileNumber", comment: "Mobile Number")
        cvrhp.font = UIFont(name: "OpenSans-Light", size: 10)
        cvrhp.textColor = UIColor.black
        cvrhp.isHidden = true
        cvrhp.adjustsFontSizeToFitWidth = true
        cvrhp.minimumScaleFactor = 0.1
        cvrhp.baselineAdjustment = .alignCenters
        cvrhp.textAlignment = .left
        return cvrhp
    }()
    
    let mobileNumberTextField: UITextField = {
        let mntf = UITextField()
        mntf.translatesAutoresizingMaskIntoConstraints = false
        mntf.textColor = UIColor.black
        mntf.placeholder = NSLocalizedString("barberShopProfileEditorViewControllerMobileNumber", comment: "Mobile Number")
        mntf.textAlignment = .left
        mntf.font = UIFont(name: "OpenSans-Light", size: 15)
        mntf.keyboardType = .phonePad
        mntf.addTarget(self, action: #selector(mobileNumberTextFieldDidChange), for: .editingDidBegin)
        mntf.addTarget(self, action: #selector(mobileNumberTextFieldDidChange), for: .editingChanged)
        return mntf
    }()
    
    let mobileNumberSeperatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return view
    }()
    
    lazy var addressSearchLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("barberShopProfileEditorViewControllerAddress", comment: "address")
        ht.textColor = UIColor.lightGray
        ht.font = UIFont(name: "OpenSans-Light", size: 15)
        ht.isUserInteractionEnabled = true
        ht.numberOfLines = 0
        ht.adjustsFontSizeToFitWidth = true
        ht.minimumScaleFactor = 0.1
        ht.baselineAdjustment = .alignCenters
        ht.textAlignment = .left
        ht.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowGoogleAutocomple)))
        return ht
    }()
    
    lazy var currentLocationButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.setImage(UIImage(named: "locationmapmarker"), for: .normal)
        st.backgroundColor = UIColor.clear
        st.addTarget(self, action: #selector(hanldeGettingCurrentocation), for: .touchUpInside)
        st.isEnabled = true
        return st
    }()
    
    let addressSearchSeperatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return view
    }()
    
    let shopCustomerGenderHiddenPlaceHolder: UILabel = {
        let lnhp = UILabel()
        lnhp.translatesAutoresizingMaskIntoConstraints = false
        lnhp.text = NSLocalizedString("barberShopProfileEditorViewControllerShopCategory", comment: "shop category")
        lnhp.font = UIFont(name: "OpenSans-Light", size: 10)
        lnhp.textColor = UIColor.black
        lnhp.isHidden = true
        lnhp.adjustsFontSizeToFitWidth = true
        lnhp.minimumScaleFactor = 0.1
        lnhp.baselineAdjustment = .alignCenters
        lnhp.textAlignment = .left
        return lnhp
    }()
    
    let shopCustomerGenderTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.placeholder = NSLocalizedString("barberShopProfileEditorViewControllerShopCategory", comment: "shop category")
        em.addTarget(self, action: #selector(gendertextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(gendertextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let shopCustomerGenderSeperatorView: UIView = {
        let lnsv = UIView()
        lnsv.translatesAutoresizingMaskIntoConstraints = false
        lnsv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return lnsv
    }()
    
    let genderPicker: UIPickerView = {
        let gender = UIPickerView()
        gender.translatesAutoresizingMaskIntoConstraints = false
        return gender
    }()
    
    lazy var registerButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        st.setTitle(NSLocalizedString("barberShopProfileEditorViewControllerContinueButton", comment: "continue"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 15)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.addTarget(self, action: #selector(textFieldDidChange), for: .touchUpInside)
        st.isEnabled = true
        st.isUserInteractionEnabled = true
        return st
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        
        view.addSubview(inputsContainerView)
        view.addSubview(registerButton)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
        self.mobileNumberTextField.delegate = self
        self.genderPicker.delegate = self
        self.shopCustomerGenderTextField.inputView = self.genderPicker
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        setupInputContainerViews()
        handleGettingCategoryData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
        // self.companyNameTextField.becomeFirstResponder()
    }
    
    @objc func getShopGenderByCategory(category: String) -> String {
        switch category {
        case "Barbershop":
            return "male"
        case "Hair salon":
            return "unisex"
        case "Nail salon":
            return "female"
        case "Spa":
            return "unisex"
        case "Tattoo":
            return "unisex"
        case "Massage":
            return "unisex"
        case "Piercings":
            return "unisex"
        case "Beauty salon":
            return "unisex"
        default:
            return "unisex"
        }
    }
    
    @objc func handleGettingCategoryData() {
        let valueCategoryBarbershop = GenderTrans(translated: NSLocalizedString("barberShopProfileEditorViewControllerCategoryBarberShop", comment: "Barbershop"), original: "Barbershop")
        let valueCategoryHairSalon = GenderTrans(translated: NSLocalizedString("barberShopProfileEditorViewControllerCategoryHairSalon", comment: "Hair salon"), original: "Hair salon")
        let valueCategoryNailSalon = GenderTrans(translated: NSLocalizedString("barberShopProfileEditorViewControllerCategoryNailSalon", comment: "Nail salon"), original: "Nail salon")
        let valueCategorySpa = GenderTrans(translated: NSLocalizedString("barberShopProfileEditorViewControllerCategorySpa", comment: "Spa"), original: "Spa")
        let valueCategoryTattoo = GenderTrans(translated: NSLocalizedString("barberShopProfileEditorViewControllerCategoryTattoo", comment: "Tattoo"), original: "Tattoo")
        let valueCategoryMassage = GenderTrans(translated: NSLocalizedString("barberShopProfileEditorViewControllerCategoryMassage", comment: "Massage"), original: "Massage")
        let valueCategoryPiercings = GenderTrans(translated: NSLocalizedString("barberShopProfileEditorViewControllerCategoryPiercing", comment: "Piercings"), original: "Piercings")
        let valueCategoryBeautySalon = GenderTrans(translated: NSLocalizedString("barberShopProfileEditorViewControllerCategoryBeautySalon", comment: "Beauty salon"), original: "Beauty salon")
        self.shopCategoryTypes.append(valueCategoryBarbershop)
        self.shopCategoryTypes.append(valueCategoryHairSalon)
        self.shopCategoryTypes.append(valueCategoryNailSalon)
        self.shopCategoryTypes.append(valueCategorySpa)
        self.shopCategoryTypes.append(valueCategoryTattoo)
        self.shopCategoryTypes.append(valueCategoryMassage)
        self.shopCategoryTypes.append(valueCategoryPiercings)
        self.shopCategoryTypes.append(valueCategoryBeautySalon)
    }
    
    
    @objc func textFieldDidChange(){
        hideKeyboard()
        if companyNameTextField.text != "" && emailTextField.text != "" && mobileNumberTextField.text != "" && addressSearchLabel.text != "address" && shopCustomerGenderTextField.text != "" && emailValid == true {
            self.handleCheckEmail()
        } else {
            if companyNameTextField.text == "" && emailTextField.text == "" && mobileNumberTextField.text == "" && addressSearchLabel.text == "address" && shopCustomerGenderTextField.text == "" {
                //Disable button
                self.view.makeToast(NSLocalizedString("barberShopProfileEditorViewControllerErrorMessageCompleteForm", comment: "please complete form"), duration: 2.0, position: .bottom)
            } else {
                var message = NSLocalizedString("barberShopProfileEditorViewControllerErrorMessagePleaseInsert", comment: "Please insert your ");
                
                if (companyNameTextField.text == ""){
                    message = NSLocalizedString("barberShopProfileEditorViewControllerErrorMessageInsertShopName", comment: "* shop name ");
                }
                
                if emailValid == false {
                    message = message + NSLocalizedString("barberShopProfileEditorViewControllerErrorMessageInsertEmail", comment: "* valid email ");
                }
                
                if (mobileNumberTextField.text == ""){
                    message = message + NSLocalizedString("barberShopProfileEditorViewControllerErrorMessageInsertMobileNumber", comment: "*mobile number ");
                }
                
                if (addressSearchLabel.text == "Address"){
                    message = message + NSLocalizedString("barberShopProfileEditorViewControllerErrorMessageInsertAddresse", comment: "* shop address ");
                }
                
                if (shopCustomerGenderTextField.text == ""){
                    message = message + NSLocalizedString("barberShopProfileEditorViewControllerErrorMessageSelectCategory", comment: "* Please select a category for your shop ");
                }
                
                self.view.makeToast(message, duration: 2.0, position: .bottom)
            }
        }
    }
    
    @objc func companyNametextFieldDidChange(){
        self.companyNameSeperatorView.backgroundColor = UIColor.black
        self.emailSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.mobileNumberSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.addressSearchSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.companyNameHiddenPlaceHolder.isHidden = false
        self.emailHiddenPlaceHolder.isHidden = true
        self.mobileNumberHiddenPlaceHolder.isHidden = true
    }
    
    @objc func emailtextFieldDidChange(){
        
        self.companyNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.mobileNumberSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.addressSearchSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.companyNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = false
        self.mobileNumberHiddenPlaceHolder.isHidden = true
        
    }
    
    @objc func mobileNumberTextFieldDidChange(){
        self.companyNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.emailSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.mobileNumberSeperatorView.backgroundColor = UIColor.black
        self.addressSearchSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.companyNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = true
        self.mobileNumberHiddenPlaceHolder.isHidden = false
    }
    
    @objc func gendertextFieldDidChange(){
        self.companyNameSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.emailSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.mobileNumberSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.shopCustomerGenderSeperatorView.backgroundColor = UIColor.black
        self.addressSearchSeperatorView.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        self.companyNameHiddenPlaceHolder.isHidden = true
        self.emailHiddenPlaceHolder.isHidden = true
        self.mobileNumberHiddenPlaceHolder.isHidden = true
        self.shopCustomerGenderHiddenPlaceHolder.isHidden = false
        
        
        if self.shopCustomerGenderTextField.text == "" {
            self.genderPicker.selectRow(0, inComponent: 0, animated: true)
            self.shopCategorySelelcted = self.shopCategoryTypes[0].original
            self.genderSelected = getShopGenderByCategory(category: self.shopCategoryTypes[0].original)
            self.shopCustomerGenderTextField.text = self.shopCategoryTypes[0].translated
        }
        
    }
    
    @objc func isValidEmail() {
        let testStr = emailTextField.text ?? ""
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        if result == true {
            self.emailValid = true
            self.emailHiddenPlaceHolder.text = NSLocalizedString("emailTextLabelAndTextFieldPlaceholderLoginView", comment: "Email")
        }else{
            self.emailValid = false
            self.emailHiddenPlaceHolder.text = NSLocalizedString("createAccountErrorMessageEmailNotProperlyFormatted", comment: "Email is not properly formatted")
        }
    }
    
    @objc func handleBackAction(){
        self.hideKeyboard()
        dismiss(animated: true, completion: nil)
    }
    
    @objc func hideKeyboard(){
        view.endEditing(true)
    }
    
    @objc func handleShowGoogleAutocomple(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @objc func handleCheckEmail(){
        
        if let shopName = self.companyNameTextField.text, let emailtext = emailTextField.text, let mobileNumber = mobileNumberTextField.text, let address = self.addressSearchLabel.text, let addLong = self.companyAddressLong, let addLat = self.companyAddressLat, let ctry = self.country, let ctryCode = self.countryCode, let ctryCurrencyCode = self.countryCurrencyCode, let genderSel = self.genderSelected, let shopCate = self.shopCategorySelelcted  {
            
            let values = ["shopName": shopName ,
                          "email":emailtext,
                          "mobileNumber": mobileNumber,
                          "address": address,
                          "addressLongitude": addLong,
                          "addressLatitude": addLat,
                          "country":ctry,
                          "countryCode":ctryCode,
                          "countryCurrencyCode":ctryCurrencyCode,
                          "shopCustomerGender": genderSel, "shopCategory": shopCate] as [String: AnyObject]
            print("data values", values)
            self.registerButton.isEnabled = false
            self.registerButton.setTitleColor(UIColor.black, for: .normal)
            self.registerButton.backgroundColor = UIColor.clear
            self.emailTextField.text = ""
            self.mobileNumberTextField.text = ""
            self.emailTextField.text = ""
            self.addressSearchLabel.text = ""
            self.shopCustomerGenderTextField.text = ""
            weak var pvc = self.presentingViewController
            self.dismiss(animated: true) {
                let choosePView = ChoosePhotoViewController()
                choosePView.customerOrBarberChoice = "shop"
                choosePView.customerValues = values
                let navController = UINavigationController(rootViewController: choosePView)
                pvc?.present(navController, animated: true, completion: nil)
            }
            
            
        }
    }
    
    func setupInputContainerViews(){
        
        var topDistance: CGFloat = 20
        var bottomDistance: CGFloat = -10
        if UIDevice.current.hasNotch {
            topDistance = 60
            bottomDistance = -24
        }
        
        upperInputsContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(registerBarberViewCloseViewButton)
        upperInputsContainerView.addSubview(registerBarberViewTitlePlaceHolder)
        
        registerBarberViewCloseViewButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        registerBarberViewCloseViewButton.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        registerBarberViewCloseViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        registerBarberViewCloseViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        registerBarberViewTitlePlaceHolder.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        registerBarberViewTitlePlaceHolder.rightAnchor.constraint(equalTo: registerBarberViewCloseViewButton.leftAnchor).isActive = true
        registerBarberViewTitlePlaceHolder.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor, constant: 10).isActive = true
        registerBarberViewTitlePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        inputsContainerView.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 10).isActive = true
        inputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        inputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        inputsContainerView.heightAnchor.constraint(equalToConstant: 220).isActive = true
        
        
        
        inputsContainerView.addSubview(companyNameHiddenPlaceHolder)
        inputsContainerView.addSubview(companyNameTextField)
        inputsContainerView.addSubview(companyNameSeperatorView)
        inputsContainerView.addSubview(emailHiddenPlaceHolder)
        inputsContainerView.addSubview(emailTextField)
        inputsContainerView.addSubview(emailSeperatorView)
        inputsContainerView.addSubview(mobileNumberInputView)
        inputsContainerView.addSubview(currentLocationButton)
        inputsContainerView.addSubview(addressSearchLabel)
        inputsContainerView.addSubview(addressSearchSeperatorView)
        inputsContainerView.addSubview(shopCustomerGenderHiddenPlaceHolder)
        inputsContainerView.addSubview(shopCustomerGenderTextField)
        inputsContainerView.addSubview(shopCustomerGenderSeperatorView)
        
        companyNameHiddenPlaceHolder.topAnchor.constraint(equalTo: inputsContainerView.topAnchor).isActive = true
        companyNameHiddenPlaceHolder.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        companyNameHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        companyNameHiddenPlaceHolder.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        companyNameTextField.topAnchor.constraint(equalTo: companyNameHiddenPlaceHolder.bottomAnchor).isActive = true
        companyNameTextField.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        companyNameTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        companyNameTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        companyNameSeperatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        companyNameSeperatorView.topAnchor.constraint(equalTo: companyNameTextField.bottomAnchor).isActive = true
        companyNameSeperatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        companyNameSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        emailHiddenPlaceHolder.topAnchor.constraint(equalTo: companyNameSeperatorView.bottomAnchor, constant: 5).isActive = true
        emailHiddenPlaceHolder.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        emailHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        emailHiddenPlaceHolder.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        emailTextField.topAnchor.constraint(equalTo: emailHiddenPlaceHolder.bottomAnchor).isActive = true
        emailTextField.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        emailSeperatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        emailSeperatorView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor).isActive = true
        emailSeperatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        emailSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        mobileNumberInputView.topAnchor.constraint(equalTo: emailSeperatorView.bottomAnchor, constant: 5).isActive = true
        mobileNumberInputView.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        mobileNumberInputView.heightAnchor.constraint(equalToConstant: 45).isActive = true
        mobileNumberInputView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        mobileNumberInputView.addSubview(mobileNumberHiddenPlaceHolder)
        mobileNumberInputView.addSubview(mobileNumberTextField)
        mobileNumberInputView.addSubview(mobileNumberSeperatorView)
        
        mobileNumberHiddenPlaceHolder.topAnchor.constraint(equalTo: mobileNumberInputView.topAnchor).isActive = true
        mobileNumberHiddenPlaceHolder.centerXAnchor.constraint(equalTo: mobileNumberInputView.centerXAnchor).isActive = true
        mobileNumberHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        mobileNumberHiddenPlaceHolder.widthAnchor.constraint(equalTo: mobileNumberInputView.widthAnchor).isActive = true
        
        mobileNumberTextField.topAnchor.constraint(equalTo: mobileNumberHiddenPlaceHolder.bottomAnchor).isActive = true
        mobileNumberTextField.leftAnchor.constraint(equalTo: mobileNumberInputView.leftAnchor).isActive = true
        mobileNumberTextField.rightAnchor.constraint(equalTo: mobileNumberInputView.rightAnchor).isActive = true
        mobileNumberTextField.heightAnchor.constraint(equalTo: mobileNumberInputView.heightAnchor, constant: -15).isActive = true
        
        mobileNumberSeperatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        mobileNumberSeperatorView.topAnchor.constraint(equalTo: mobileNumberInputView.bottomAnchor).isActive = true
        mobileNumberSeperatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        mobileNumberSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        currentLocationButton.topAnchor.constraint(equalTo: mobileNumberSeperatorView.bottomAnchor, constant: 5).isActive = true
        currentLocationButton.rightAnchor.constraint(equalTo: inputsContainerView.rightAnchor).isActive = true
        currentLocationButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        currentLocationButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        addressSearchLabel.topAnchor.constraint(equalTo: mobileNumberSeperatorView.bottomAnchor, constant: 5).isActive = true
        addressSearchLabel.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        addressSearchLabel.rightAnchor.constraint(equalTo: currentLocationButton.leftAnchor, constant: 5).isActive = true
        addressSearchLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        
        
        addressSearchSeperatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        addressSearchSeperatorView.topAnchor.constraint(equalTo: addressSearchLabel.bottomAnchor).isActive = true
        addressSearchSeperatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        addressSearchSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        shopCustomerGenderHiddenPlaceHolder.topAnchor.constraint(equalTo: addressSearchSeperatorView.bottomAnchor, constant: 5).isActive = true
        shopCustomerGenderHiddenPlaceHolder.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        shopCustomerGenderHiddenPlaceHolder.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        shopCustomerGenderHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        shopCustomerGenderTextField.topAnchor.constraint(equalTo: shopCustomerGenderHiddenPlaceHolder.bottomAnchor).isActive = true
        shopCustomerGenderTextField.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        shopCustomerGenderTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        shopCustomerGenderTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        shopCustomerGenderSeperatorView.topAnchor.constraint(equalTo: shopCustomerGenderTextField.bottomAnchor).isActive = true
        shopCustomerGenderSeperatorView.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        shopCustomerGenderSeperatorView.widthAnchor.constraint(equalTo: shopCustomerGenderTextField.widthAnchor).isActive = true
        shopCustomerGenderSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        registerButton.topAnchor.constraint(equalTo: shopCustomerGenderSeperatorView.bottomAnchor, constant: 10).isActive = true
        registerButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        registerButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -48).isActive = true
        registerButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
    
    func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { completion($0?.first, $1) }
    }
    
    @objc private func hanldeGettingCurrentocation(){
        if let place = self.placeMarkData, let locationData = self.currentUserLocation {
            if let countryCode = place.isoCountryCode, let ctry = place.country, let ctryCurrencyCode = Locale.currency[countryCode] as? (String, String)  {
                let cllocationdata = CLLocation(latitude: locationData.coordinate.latitude, longitude: locationData.coordinate.longitude)
                self.reverseGeoCoding(locationData: cllocationdata, ctry: ctry, countryCode: countryCode, ctryCurrencyCode: ctryCurrencyCode.0)
            }
            
        }
    }
    
    @objc private func reverseGeoCoding(locationData: CLLocation, ctry: String, countryCode: String, ctryCurrencyCode: String){
        let longitude = locationData.coordinate.longitude.description
        let latitude = locationData.coordinate.latitude.description
        let placesApiKey = "AIzaSyCzQUcGTvZo3ZAfDg4SDZWEOvm2mV6Co-Y"
        print(longitude, latitude)
        
        
        AF.request("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&key=" + placesApiKey)
            .validate(statusCode: 200..<500)
            .validate(contentType: ["application/json"])
            .response { response in
                
                if let error = response.error {
                    print("zones", error.localizedDescription)
                    return
                }
                
                var streetnumber = ""
                var route = ""
                var locality = ""
                var administrative_area_level_2 = ""
                var administrative_area_level_1 = ""
                var countryName = ""
                
                
                
                if let jsonData = response.data {
                    do {
                        let addressData = try JSONDecoder().decode(GooglePlaceAddressData.self, from: jsonData)
                        let counterVal = addressData.results.count
                        var counterAddCheck = 0
                        for addComp in addressData.results {
                            counterAddCheck = counterAddCheck + 1
                            
                            if let types = addComp.types.first {
                                switch (types) {
                                case "street_address":
                                    if let streetnum = addComp.addressComponents[safe: 0]?.longName {
                                        streetnumber = String(streetnum)
                                    }
                                case "route":
                                    if let routename = addComp.addressComponents[safe: 0]?.longName {
                                        route = routename
                                    }
                                case "locality":
                                    if let localityname = addComp.addressComponents[safe: 0]?.longName {
                                        locality = localityname
                                    }
                                case "administrative_area_level_2":
                                    if let adminAreaLevel2 = addComp.addressComponents[safe: 0]?.longName {
                                        administrative_area_level_2 = adminAreaLevel2
                                    }
                                case "administrative_area_level_1":
                                    if let adminAreaLevel1 = addComp.addressComponents[safe: 0]?.longName {
                                        administrative_area_level_1 = adminAreaLevel1
                                    }
                                case "country":
                                    if let countryNames = addComp.addressComponents[safe: 0]?.longName {
                                        countryName = countryNames
                                    }
                                default:
                                    print("Sky high")
                                }
                                
                            }
                            
                            if (counterAddCheck == counterVal) {
                                let formalAddress = streetnumber + " " + route + " " + locality + " " + administrative_area_level_2 + " " + administrative_area_level_1 + " " + countryName
                                self.addressSearchLabel.textColor = UIColor.black
                                self.addressSearchLabel.text = formalAddress
                                self.companyAddressLong = locationData.coordinate.longitude.description
                                self.companyAddressLat = locationData.coordinate.latitude.description
                                self.country = ctry
                                self.countryCode = countryCode
                                self.countryCurrencyCode = ctryCurrencyCode
                            }
                            
                        }
                        
                    } catch _ {
                        print("An error occured")
                    }
                    
                }
        }
    }

}


extension RegisterBarberViewController: GMSAutocompleteViewControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate, CLLocationManagerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        //print("Place name: \(place.name)")
        //print("Place address: \(place.formattedAddress)")
        //print("Place attributions: \(place.attributions)")
        if let lat = Double(place.coordinate.latitude.description), let long = Double(place.coordinate.longitude.description)  {
            self.geocode(latitude: lat, longitude: long) { (placeMarkData, errrror) in
                if let error = errrror {
                    print(error.localizedDescription)
                    return
                }
                
                if let countryCodeData = placeMarkData?.isoCountryCode, let country = placeMarkData?.country, let data = Locale.currency[countryCodeData] as? (String, String) {
                    
                    let cllocationdata = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
                    self.reverseGeoCoding(locationData: cllocationdata, ctry: country, countryCode: countryCodeData, ctryCurrencyCode: data.0)
                    
                    
                }
                
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    //picker view functions
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.shopCategoryTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.shopCategoryTypes[row].translated
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.genderSelected =  self.getShopGenderByCategory(category: self.shopCategoryTypes[row].original)
        self.shopCategorySelelcted = self.shopCategoryTypes[row].original
        self.shopCustomerGenderTextField.text = self.shopCategoryTypes[row].translated
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            print("red yellow")
            self.currentUserLocation = location
            CLGeocoder().reverseGeocodeLocation(location) { (placeMark, errorr) in
                if let error = errorr {
                    print(error.localizedDescription)
                }
                
                if let place = placeMark?.first {
                    self.placeMarkData = place
                }
            }
            
            self.locationManager.stopUpdatingLocation()
        }
    }
    
}
