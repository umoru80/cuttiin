//
//  MonthlyPaymentViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 10/1/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate

class MonthlyPaymentViewController: UIViewController {
    var monthChosen: String?
    var userRole: String?
    var barberShopBarbersUUIDS = [String]()
    var barberShopEarning = [Double]()
    var barberShopRefunds = [Double]()
    var barberEarningsArray = [BarberEarnings]()
    var totalEarning = [Int]()
    var totalCardEarning = [Int]()
    var totalMobilePayEarning = [Int]()
    var totalRefundEarning = [Double]()
    var currentUserCurency: String?
    
    var totalWithdrawAmount = 0
    var creditCardAmount = 0
    var mobilePayAmount = 0
    
    
    let totalUpperContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        return tcview
    }()
    
    let totalEarningsPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("totalEarningTextMonthlyPaymentView", comment: "Total Earning")
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        ehp.textColor = UIColor.white
        ehp.adjustsFontSizeToFitWidth = true
        ehp.minimumScaleFactor = 0.1
        ehp.baselineAdjustment = .alignCenters
        ehp.textAlignment = .left
        return ehp
    }()
    
    let totalEarningsAmountPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        ehp.textColor = UIColor.white
        ehp.adjustsFontSizeToFitWidth = true
        ehp.minimumScaleFactor = 0.1
        ehp.baselineAdjustment = .alignCenters
        ehp.textAlignment = .right
        return ehp
    }()
    
    let totalRefundsPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("totalRefundsTextMonthlyPaymentView", comment: "Total Refunds")
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        ehp.textColor = UIColor.white
        ehp.adjustsFontSizeToFitWidth = true
        ehp.minimumScaleFactor = 0.1
        ehp.baselineAdjustment = .alignCenters
        ehp.textAlignment = .left
        return ehp
    }()
    
    let totalRefundsAmountPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        ehp.textColor = UIColor.white
        ehp.adjustsFontSizeToFitWidth = true
        ehp.minimumScaleFactor = 0.1
        ehp.baselineAdjustment = .alignCenters
        ehp.textAlignment = .right
        return ehp
    }()
    
    let totalSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor.white
        return fnsv
    }()
    
    let totalLowerContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        return tcview
    }()
    
    let totalIncomePlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("totalTextHolderNumberOfCustomer", comment: "Total")
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        ehp.textColor = UIColor.white
        ehp.adjustsFontSizeToFitWidth = true
        ehp.minimumScaleFactor = 0.1
        ehp.baselineAdjustment = .alignCenters
        ehp.textAlignment = .left
        return ehp
    }()
    
    let totalIncomeAmountPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 20)
        ehp.textColor = UIColor.white
        ehp.textAlignment = .right
        return ehp
    }()
    
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.white
        cv.showsVerticalScrollIndicator = false
        cv.register(customMonthlyPaymentCollectionViewCell.self, forCellWithReuseIdentifier: "cellIdMOnthlyPayment")
        return cv
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        navigationItem.title = monthChosen
        
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        view.addSubview(totalUpperContainerView)
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupviewContraints()
    }
    
    func setupviewContraints(){
        totalUpperContainerView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        totalUpperContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        totalUpperContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -12).isActive = true
        totalUpperContainerView.heightAnchor.constraint(equalToConstant: 155).isActive = true
        
        totalUpperContainerView.addSubview(totalEarningsPlaceHolder)
        totalUpperContainerView.addSubview(totalEarningsAmountPlaceHolder)
        totalUpperContainerView.addSubview(totalRefundsPlaceHolder)
        totalUpperContainerView.addSubview(totalRefundsAmountPlaceHolder)
        totalUpperContainerView.addSubview(totalSeperatorView)
        totalUpperContainerView.addSubview(totalLowerContainerView)
        
        totalEarningsPlaceHolder.topAnchor.constraint(equalTo: totalUpperContainerView.topAnchor).isActive = true
        totalEarningsPlaceHolder.leftAnchor.constraint(equalTo: totalUpperContainerView.leftAnchor, constant: 5).isActive = true
        totalEarningsPlaceHolder.widthAnchor.constraint(equalTo: totalUpperContainerView.widthAnchor, multiplier: 0.5).isActive = true
        totalEarningsPlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        totalEarningsAmountPlaceHolder.topAnchor.constraint(equalTo: totalUpperContainerView.topAnchor).isActive = true
        totalEarningsAmountPlaceHolder.rightAnchor.constraint(equalTo: totalUpperContainerView.rightAnchor, constant: -5).isActive = true
        totalEarningsAmountPlaceHolder.leftAnchor.constraint(equalTo: totalEarningsPlaceHolder.rightAnchor).isActive = true
        totalEarningsAmountPlaceHolder.widthAnchor.constraint(equalTo: totalUpperContainerView.widthAnchor, multiplier: 0.5).isActive = true
        totalEarningsAmountPlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        totalRefundsPlaceHolder.topAnchor.constraint(equalTo: totalEarningsPlaceHolder.bottomAnchor).isActive = true
        totalRefundsPlaceHolder.leftAnchor.constraint(equalTo: totalUpperContainerView.leftAnchor, constant: 5).isActive = true
        totalRefundsPlaceHolder.widthAnchor.constraint(equalTo: totalUpperContainerView.widthAnchor, multiplier: 0.5).isActive = true
        totalRefundsPlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        totalRefundsAmountPlaceHolder.topAnchor.constraint(equalTo: totalEarningsPlaceHolder.bottomAnchor).isActive = true
        totalRefundsAmountPlaceHolder.rightAnchor.constraint(equalTo: totalUpperContainerView.rightAnchor, constant: -5).isActive = true
        totalRefundsAmountPlaceHolder.leftAnchor.constraint(equalTo: totalRefundsPlaceHolder.rightAnchor).isActive = true
        totalRefundsAmountPlaceHolder.widthAnchor.constraint(equalTo: totalUpperContainerView.widthAnchor, multiplier: 0.5).isActive = true
        totalRefundsAmountPlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        totalSeperatorView.topAnchor.constraint(equalTo: totalRefundsAmountPlaceHolder.bottomAnchor).isActive = true
        totalSeperatorView.centerXAnchor.constraint(equalTo: totalUpperContainerView.centerXAnchor).isActive = true
        totalSeperatorView.widthAnchor.constraint(equalTo: totalUpperContainerView.widthAnchor, constant: -20).isActive = true
        totalSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        //
        totalLowerContainerView.topAnchor.constraint(equalTo: totalSeperatorView.bottomAnchor).isActive = true
        totalLowerContainerView.centerXAnchor.constraint(equalTo: totalUpperContainerView.centerXAnchor).isActive = true
        totalLowerContainerView.widthAnchor.constraint(equalTo: totalUpperContainerView.widthAnchor, constant: -12).isActive = true
        totalLowerContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        totalLowerContainerView.addSubview(totalIncomePlaceHolder)
        totalLowerContainerView.addSubview(totalIncomeAmountPlaceHolder)
        
        totalIncomePlaceHolder.topAnchor.constraint(equalTo: totalLowerContainerView.topAnchor).isActive = true
        totalIncomePlaceHolder.leftAnchor.constraint(equalTo: totalLowerContainerView.leftAnchor, constant: 5).isActive = true
        totalIncomePlaceHolder.widthAnchor.constraint(equalTo: totalLowerContainerView.widthAnchor, multiplier: 0.5).isActive = true
        totalIncomePlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        totalIncomeAmountPlaceHolder.topAnchor.constraint(equalTo: totalLowerContainerView.topAnchor).isActive = true
        totalIncomeAmountPlaceHolder.rightAnchor.constraint(equalTo: totalLowerContainerView.rightAnchor, constant: -5).isActive = true
        totalIncomeAmountPlaceHolder.leftAnchor.constraint(equalTo: totalIncomePlaceHolder.rightAnchor).isActive = true
        totalIncomeAmountPlaceHolder.widthAnchor.constraint(equalTo: totalLowerContainerView.widthAnchor, multiplier: 0.5).isActive = true
        totalIncomeAmountPlaceHolder.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        collectView.topAnchor.constraint(equalTo: totalLowerContainerView.bottomAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -24).isActive = true
        collectView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
    }


}



class customMonthlyPaymentCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let thumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.contentMode = .scaleAspectFit
        tniv.backgroundColor = UIColor.clear
        return tniv
    }()
    
    let barberNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor(r: 11, g: 49, b: 68)
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let totalHeaderPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.text = NSLocalizedString("totalTextHolderNumberOfCustomer", comment: "Total")
        fnhp.textColor = UIColor(r: 11, g: 49, b: 68)
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let totalAmountPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor(r: 11, g: 49, b: 68)
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let totalNumberOfClientsPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.textColor = UIColor(r: 11, g: 49, b: 68)
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let totalEarningSeperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        return sv
    }()
    
    //credit card
    let creditCardThumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.image = UIImage(named: "credit_card")
        tniv.contentMode = .scaleAspectFit
        tniv.backgroundColor = UIColor.clear
        return tniv
    }()
    
    let creditCardHeaderPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.text = NSLocalizedString("creditCardHeaderTextMonthlyPaymentView", comment: "Credit cards")
        fnhp.textColor = UIColor(r: 11, g: 49, b: 68)
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let creditCardAmountPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor(r: 11, g: 49, b: 68)
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let creditCardNumberOfClientsPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.textColor = UIColor(r: 11, g: 49, b: 68)
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    //third row
    let mobilePayThumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.image = UIImage(named: "mobile_pay")
        tniv.contentMode = .scaleAspectFit
        tniv.backgroundColor = UIColor.clear
        return tniv
    }()
    
    let mobilePayHeaderPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.text = NSLocalizedString("mobilePayHeaderTextMonthlyPaymentView", comment: "Mobile pay")
        fnhp.textColor = UIColor(r: 11, g: 49, b: 68)
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let mobilePayAmountPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor(r: 11, g: 49, b: 68)
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let mobilePayNumberOfClientsPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.textColor = UIColor(r: 11, g: 49, b: 68)
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    //fourth
    let cashPaymentThumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.image = UIImage(named: "cash_")
        tniv.contentMode = .scaleAspectFit
        tniv.backgroundColor = UIColor.clear
        return tniv
    }()
    
    let cashPaymentHeaderPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.text = NSLocalizedString("cashPaymentHeaderTextMonthlyPaymentView", comment: "Cash")
        fnhp.textColor = UIColor(r: 11, g: 49, b: 68)
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let cashPaymentAmountPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor(r: 11, g: 49, b: 68)
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let cashPaymentNumberOfClientsPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.textColor = UIColor(r: 11, g: 49, b: 68)
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    // fifth
    let refundThumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.image = UIImage(named: "opening_hours")
        tniv.contentMode = .scaleAspectFit
        tniv.backgroundColor = UIColor.clear
        return tniv
    }()
    
    let refundHeaderPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.text = NSLocalizedString("refundsHeaderTextMonthlyPaymentView", comment: "Refunds")
        fnhp.textColor = UIColor(r: 11, g: 49, b: 68)
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let refundAmountPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor(r: 11, g: 49, b: 68)
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        return sv
    }()

    
    
    func setupViews(){
        addSubview(thumbnailImageView)
        addSubview(barberNamePlaceHolder)
        addSubview(totalHeaderPlaceHolder)
        addSubview(totalAmountPlaceHolder)
        addSubview(totalEarningSeperatorView)
        addSubview(totalNumberOfClientsPlaceHolder)
        //second row
        addSubview(creditCardThumbnailImageView)
        addSubview(creditCardHeaderPlaceHolder)
        addSubview(creditCardNumberOfClientsPlaceHolder)
        addSubview(creditCardAmountPlaceHolder)
        //third row
        addSubview(mobilePayThumbnailImageView)
        addSubview(mobilePayHeaderPlaceHolder)
        addSubview(mobilePayNumberOfClientsPlaceHolder)
        addSubview(mobilePayAmountPlaceHolder)
        //fourth row
        addSubview(cashPaymentThumbnailImageView)
        addSubview(cashPaymentHeaderPlaceHolder)
        addSubview(cashPaymentNumberOfClientsPlaceHolder)
        addSubview(cashPaymentAmountPlaceHolder)
        
        //fifth row
        addSubview(refundThumbnailImageView)
        addSubview(refundHeaderPlaceHolder)
        addSubview(refundAmountPlaceHolder)
        
        addSubview(seperatorView)
        
        backgroundColor = UIColor.white
        addContraintsWithFormat(format: "H:|-5-[v0(65)]-5-[v1]-5-|", views: thumbnailImageView, barberNamePlaceHolder)
        addContraintsWithFormat(format: "V:|-16-[v0(25)]|", views: barberNamePlaceHolder)
        addContraintsWithFormat(format: "V:|-16-[v0(65)]-5-[v1(20)]-1-[v2(20)]-5-[v3(1)]-5-[v4(35)]-5-[v5(1)]|", views: thumbnailImageView, totalHeaderPlaceHolder, totalAmountPlaceHolder, totalEarningSeperatorView, creditCardThumbnailImageView, seperatorView)
        addContraintsWithFormat(format: "H:|-5-[v0(90)][v1(90)]|", views: totalHeaderPlaceHolder, totalNumberOfClientsPlaceHolder)
        addContraintsWithFormat(format: "H:|-5-[v0(90)]|", views: totalAmountPlaceHolder)
        
        addContraintsWithFormat(format: "V:|-16-[v0(65)]-5-[v1(25)]-5-[v2(1)]|", views: thumbnailImageView, totalNumberOfClientsPlaceHolder, seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: totalEarningSeperatorView)
        //second row
        
        addContraintsWithFormat(format: "V:|-16-[v0(65)]-5-[v1(20)]-1-[v2(20)]-5-[v3(1)]-15-[v4(15)]-5-[v5(1)]|", views: thumbnailImageView, totalHeaderPlaceHolder, totalAmountPlaceHolder, totalEarningSeperatorView, creditCardHeaderPlaceHolder, seperatorView)
        addContraintsWithFormat(format: "H:|-5-[v0(35)]-5-[v1(100)]-5-[v2(100)]|", views: creditCardThumbnailImageView, creditCardHeaderPlaceHolder, creditCardNumberOfClientsPlaceHolder)
        
        addContraintsWithFormat(format: "V:|-16-[v0(65)]-5-[v1(20)]-1-[v2(20)]-15-[v3(1)]-5-[v4(18)]-5-[v5(1)]|", views: thumbnailImageView, totalHeaderPlaceHolder, totalAmountPlaceHolder, totalEarningSeperatorView, creditCardNumberOfClientsPlaceHolder, seperatorView)
        addContraintsWithFormat(format: "V:|-16-[v0(65)]-5-[v1(20)]-1-[v2(20)]-15-[v3(1)]-5-[v4(15)]-2-[v5(15)]-5-[v6(1)]|", views: thumbnailImageView, totalHeaderPlaceHolder, totalAmountPlaceHolder, totalEarningSeperatorView, creditCardHeaderPlaceHolder, creditCardAmountPlaceHolder, seperatorView)
        
        
        addContraintsWithFormat(format: "H:|-45-[v0(100)]|", views: creditCardAmountPlaceHolder)
        
        //third row
        addContraintsWithFormat(format: "V:|-16-[v0(65)]-5-[v1(20)]-1-[v2(20)]-15-[v3(1)]-5-[v4(15)]-5-[v5(15)]-5-[v6(35)]-5-[v7(1)]|", views: thumbnailImageView, totalHeaderPlaceHolder, totalAmountPlaceHolder, totalEarningSeperatorView, creditCardHeaderPlaceHolder, creditCardAmountPlaceHolder, mobilePayThumbnailImageView, seperatorView)
        
        addContraintsWithFormat(format: "H:|-5-[v0(35)]-5-[v1(100)]-5-[v2(100)]|", views: mobilePayThumbnailImageView, mobilePayHeaderPlaceHolder, mobilePayNumberOfClientsPlaceHolder)
        
        addContraintsWithFormat(format: "V:|-16-[v0(65)]-5-[v1(20)]-1-[v2(20)]-15-[v3(1)]-5-[v4(15)]-5-[v5(15)]-5-[v6(15)]-5-[v7(1)]|", views: thumbnailImageView, totalHeaderPlaceHolder, totalAmountPlaceHolder, totalEarningSeperatorView, creditCardHeaderPlaceHolder, creditCardAmountPlaceHolder, mobilePayHeaderPlaceHolder, seperatorView)
        
        
        addContraintsWithFormat(format: "V:|-16-[v0(65)]-5-[v1(20)]-1-[v2(20)]-15-[v3(1)]-5-[v4(15)]-5-[v5(15)]-5-[v6(15)]-2-[v7(15)]-5-[v8(1)]|", views: thumbnailImageView, totalHeaderPlaceHolder, totalAmountPlaceHolder, totalEarningSeperatorView, creditCardHeaderPlaceHolder, creditCardAmountPlaceHolder, mobilePayHeaderPlaceHolder, mobilePayAmountPlaceHolder, seperatorView)
        
        
        addContraintsWithFormat(format: "V:|-16-[v0(65)]-5-[v1(20)]-1-[v2(20)]-15-[v3(1)]-5-[v4(15)]-5-[v5(15)]-5-[v6(18)]-5-[v7(1)]|", views: thumbnailImageView, totalHeaderPlaceHolder, totalAmountPlaceHolder, totalEarningSeperatorView, creditCardHeaderPlaceHolder, creditCardAmountPlaceHolder, mobilePayNumberOfClientsPlaceHolder, seperatorView)
        
        
        addContraintsWithFormat(format: "H:|-45-[v0(100)]|", views: mobilePayAmountPlaceHolder)
        
        //fourth row
        addContraintsWithFormat(format: "V:|-16-[v0(65)]-5-[v1(20)]-1-[v2(20)]-15-[v3(1)]-5-[v4(15)]-5-[v5(15)]-5-[v6(15)]-5-[v7(15)]-5-[v8(35)]-5-[v9(1)]|", views: thumbnailImageView, totalHeaderPlaceHolder, totalAmountPlaceHolder, totalEarningSeperatorView, creditCardHeaderPlaceHolder, creditCardAmountPlaceHolder, mobilePayHeaderPlaceHolder, mobilePayAmountPlaceHolder, cashPaymentThumbnailImageView, seperatorView)
        
        addContraintsWithFormat(format: "H:|-5-[v0(35)]-5-[v1(100)]-5-[v2(100)]|", views: cashPaymentThumbnailImageView, cashPaymentHeaderPlaceHolder, cashPaymentNumberOfClientsPlaceHolder)

        addContraintsWithFormat(format: "V:|-16-[v0(65)]-5-[v1(20)]-1-[v2(20)]-15-[v3(1)]-5-[v4(15)]-5-[v5(15)]-5-[v6(15)]-5-[v7(15)]-5-[v8(15)]-2-[v9(15)]-5-[v10(1)]|", views: thumbnailImageView, totalHeaderPlaceHolder, totalAmountPlaceHolder, totalEarningSeperatorView, creditCardHeaderPlaceHolder, creditCardAmountPlaceHolder, mobilePayHeaderPlaceHolder, mobilePayAmountPlaceHolder, cashPaymentHeaderPlaceHolder, cashPaymentAmountPlaceHolder, seperatorView)
        
        addContraintsWithFormat(format: "H:|-45-[v0(100)]|", views: cashPaymentHeaderPlaceHolder)

        
        addContraintsWithFormat(format: "V:|-16-[v0(65)]-5-[v1(20)]-1-[v2(20)]-15-[v3(1)]-5-[v4(15)]-5-[v5(15)]-5-[v6(15)]-5-[v7(15)]-5-[v8(15)]-5-[v9(1)]|", views: thumbnailImageView, totalHeaderPlaceHolder, totalAmountPlaceHolder, totalEarningSeperatorView, creditCardHeaderPlaceHolder, creditCardAmountPlaceHolder, mobilePayHeaderPlaceHolder, mobilePayAmountPlaceHolder, cashPaymentNumberOfClientsPlaceHolder, seperatorView)
        
        addContraintsWithFormat(format: "H:|-45-[v0(100)]|", views: cashPaymentAmountPlaceHolder)
        
        
        //fifth row
        addContraintsWithFormat(format: "V:|-16-[v0(65)]-5-[v1(20)]-1-[v2(20)]-15-[v3(1)]-5-[v4(15)]-5-[v5(15)]-5-[v6(15)]-5-[v7(15)]-5-[v8(15)]-5-[v9(15)]-5-[v10(35)]-5-[v11(1)]|", views: thumbnailImageView, totalHeaderPlaceHolder, totalAmountPlaceHolder, totalEarningSeperatorView, creditCardHeaderPlaceHolder, creditCardAmountPlaceHolder, mobilePayHeaderPlaceHolder, mobilePayAmountPlaceHolder, cashPaymentHeaderPlaceHolder, cashPaymentAmountPlaceHolder, refundThumbnailImageView, seperatorView)
        
        addContraintsWithFormat(format: "H:|-5-[v0(35)]|", views: refundThumbnailImageView)
        
        addContraintsWithFormat(format: "V:|-16-[v0(65)]-5-[v1(20)]-1-[v2(20)]-15-[v3(1)]-5-[v4(15)]-5-[v5(15)]-5-[v6(15)]-5-[v7(15)]-5-[v8(15)]-5-[v9(15)]-5-[v10(15)]-5-[v11(1)]|", views: thumbnailImageView, totalHeaderPlaceHolder, totalAmountPlaceHolder, totalEarningSeperatorView, creditCardHeaderPlaceHolder, creditCardAmountPlaceHolder, mobilePayHeaderPlaceHolder, mobilePayAmountPlaceHolder, cashPaymentHeaderPlaceHolder, cashPaymentAmountPlaceHolder, refundHeaderPlaceHolder, seperatorView)
        
        addContraintsWithFormat(format: "H:|-45-[v0(100)]|", views: refundHeaderPlaceHolder)
        
        addContraintsWithFormat(format: "V:|-16-[v0(65)]-5-[v1(20)]-1-[v2(20)]-15-[v3(1)]-5-[v4(15)]-5-[v5(15)]-5-[v6(15)]-5-[v7(15)]-5-[v8(15)]-5-[v9(15)]-5-[v10(15)]-5-[v11(15)]-5-[v12(1)]|", views: thumbnailImageView, totalHeaderPlaceHolder, totalAmountPlaceHolder, totalEarningSeperatorView, creditCardHeaderPlaceHolder, creditCardAmountPlaceHolder, mobilePayHeaderPlaceHolder, mobilePayAmountPlaceHolder, cashPaymentHeaderPlaceHolder, cashPaymentAmountPlaceHolder, refundHeaderPlaceHolder, refundAmountPlaceHolder, seperatorView)
        
        addContraintsWithFormat(format: "H:|-45-[v0(100)]|", views: refundAmountPlaceHolder)
        
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
