//
//  ShopBillingData.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 24/07/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit
import SwiftDate

struct ShopBillingData {
    var id, customer, shop, shopCountryCurrencyCode: String
    var billingStatus, billingPaidDate, billingPaidDateTimezone, billingPaidDateCalendar: String
    var billingPaidDateLocale, appointmentNewTotalCost, appointmentNewTotalAmount, appointmentStartedTotalCost: String
    var appointmentStartedTotalAmount, appointmentEndedTotalCost, appointmentEndedTotalAmount, appointmentCancelledTotalCost: String
    var appointmentCancelledTotalAmount, appointmentPaidTotalCost, appointmentPaidTotalAmount, billingStarted: String
    var billingEnded, dateCreated, dateCreatedTimezone, dateCreatedCalendar: String
    var dateCreatedLocale: String
    var billingStartDateObj: DateInRegion
    var billingEndDateObj: DateInRegion
    var billingCreatedDateObj: DateInRegion
    
    init(id: String, customer: String, shop: String, shopCountryCurrencyCode: String, billingStatus: String, billingPaidDate: String, billingPaidDateTimezone: String, billingPaidDateCalendar: String,billingPaidDateLocale: String, appointmentNewTotalCost: String, appointmentNewTotalAmount: String, appointmentStartedTotalCost: String, appointmentStartedTotalAmount: String, appointmentEndedTotalCost: String, appointmentEndedTotalAmount: String, appointmentCancelledTotalCost: String, appointmentCancelledTotalAmount: String, appointmentPaidTotalCost: String, appointmentPaidTotalAmount: String, billingStarted: String, billingEnded: String, dateCreated: String, dateCreatedTimezone: String, dateCreatedCalendar: String, dateCreatedLocale: String, billingStartDateObj: DateInRegion, billingEndDateObj: DateInRegion, billingCreatedDateObj: DateInRegion) {
        self.id = id
        self.customer = customer
        self.shop = shop
        self.shopCountryCurrencyCode = shopCountryCurrencyCode
        self.billingStatus = billingStatus
        self.billingPaidDate = billingPaidDate
        self.billingPaidDateTimezone = billingPaidDateTimezone
        self.billingPaidDateCalendar = billingPaidDateCalendar
        self.billingPaidDateLocale = billingPaidDateLocale
        self.appointmentNewTotalCost = appointmentNewTotalCost
        self.appointmentNewTotalAmount = appointmentNewTotalAmount
        self.appointmentStartedTotalCost = appointmentStartedTotalCost
        self.appointmentStartedTotalAmount = appointmentStartedTotalAmount
        self.appointmentEndedTotalCost = appointmentEndedTotalCost
        self.appointmentEndedTotalAmount = appointmentEndedTotalAmount
        self.appointmentCancelledTotalCost = appointmentCancelledTotalCost
        self.appointmentCancelledTotalAmount = appointmentCancelledTotalAmount
        self.appointmentPaidTotalCost = appointmentPaidTotalCost
        self.appointmentPaidTotalAmount = appointmentPaidTotalAmount
        self.billingStarted = billingStarted
        self.billingEnded = billingEnded
        self.dateCreated = dateCreated
        self.dateCreatedTimezone = dateCreatedTimezone
        self.dateCreatedCalendar = dateCreatedCalendar
        self.dateCreatedLocale = dateCreatedLocale
        self.billingStartDateObj = billingStartDateObj
        self.billingEndDateObj = billingEndDateObj
        self.billingCreatedDateObj = billingCreatedDateObj
    }
}
