//
//  ChangePasswordViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/8/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import Alamofire
import SwiftDate
import SwiftSpinner

class ChangePasswordViewController: UIViewController {
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    lazy var changePasswordCloseViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleBackAction)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let changePasswordTitlePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("resetNavigationTitleChangePasswordView", comment: "password reset")
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    
    let topHeaderDescriptionPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("topHeaderDescriptionChangePasswordView", comment: "An email will be sent to you with a generated password, remember to change the password immediately after signing in")
        ehp.font = UIFont(name: "OpenSans-Light", size: 15)
        ehp.textColor = UIColor.black
        ehp.numberOfLines = 0
        ehp.textAlignment = .center
        return ehp
    }()
    
    let emailHiddenPlaceHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("changePassordEmailHidden", comment: "email")
        ehp.font = UIFont(name: "OpenSans-Light", size: 10)
        ehp.textColor = UIColor(r: 129, g: 129, b: 129)
        ehp.isHidden = true
        ehp.adjustsFontSizeToFitWidth = true
        ehp.minimumScaleFactor = 0.1
        ehp.baselineAdjustment = .alignCenters
        ehp.textAlignment = .left
        return ehp
    }()
    
    let emailTextField: UITextField = {
        let em = UITextField()
        em.translatesAutoresizingMaskIntoConstraints = false
        em.textColor = UIColor.black
        em.placeholder = NSLocalizedString("changePassordEmailHidden", comment: "email")
        em.font = UIFont(name: "OpenSans-Light", size: 15)
        em.keyboardType = .emailAddress
        em.autocapitalizationType = .none
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingDidBegin)
        em.addTarget(self, action: #selector(emailtextFieldDidChange), for: .editingChanged)
        return em
    }()
    
    let emailSeperatorView: UIView = {
        let esv = UIView()
        esv.translatesAutoresizingMaskIntoConstraints = false
        esv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return esv
    }()
    
    lazy var sendEmailButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        st.setTitle(NSLocalizedString("sendButtonChangePasswordView", comment: "send"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 15)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.addTarget(self, action: #selector(sendEmailChangeRequest), for: .touchUpInside)
        return st
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        view.addSubview(topHeaderDescriptionPlaceHolder)
        view.addSubview(emailHiddenPlaceHolder)
        view.addSubview(emailTextField)
        view.addSubview(emailSeperatorView)
        view.addSubview(sendEmailButton)
        setupViewObjectCOntraints()
    }
    
    @objc func handleBackAction(){
        self.hideKeyboardEmail()
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func emailtextFieldDidChange(){
        self.emailSeperatorView.backgroundColor = UIColor.black
        self.emailHiddenPlaceHolder.isHidden = false
    }
    
    @objc private func hideKeyboardEmail(){
        view.endEditing(true)
    }
    
    @objc private func sendEmailChangeRequest(){
        self.hideKeyboardEmail()
        if let email = self.emailTextField.text, !email.isEmpty {
            SwiftSpinner.show("Loading...")
            DispatchQueue.global(qos: .background).async {
                let parameters: Parameters = ["email": email]
                AF.request(self.BACKEND_URL + "changepassword", method: .post, parameters: parameters)
                    .validate(statusCode: 200..<501)
                    .validate(contentType: ["application/json"])
                    .response { (responseData) in
                        
                        if let _ = responseData.error {
                            let message = "Error: please try again later"
                            DispatchQueue.main.async {
                                SwiftSpinner.hide()
                            }
                            self.view.makeToast(message, duration: 3.0, position: .bottom)
                            return
                        }
                        
                        if let jsonData = responseData.data {
                            DispatchQueue.main.async {
                                SwiftSpinner.hide()
                            }
                            do {
                                let serverResponse = try JSONDecoder().decode(ServerMessage.self, from: jsonData)
                                let message = serverResponse.message
                                
                                if let statusCode = responseData.response?.statusCode {
                                    switch statusCode {
                                    case 200:
                                        self.view.makeToast(message, duration: 2.0, position: .center)
                                        self.dismiss(animated: true, completion: nil)
                                        break
                                    case 409:
                                        self.view.makeToast(message, duration: 2.0, position: .center)
                                        break
                                    case 404:
                                        self.view.makeToast(message, duration: 2.0, position: .center)
                                        break
                                    case 500:
                                        self.view.makeToast(message, duration: 2.0, position: .center)
                                        break
                                    default:
                                        print("sky high")
                                    }
                                }
                                
                            } catch _ {
                                
                                if let statusCode = responseData.response?.statusCode {
                                    switch statusCode {
                                    case 409:
                                        self.view.makeToast( NSLocalizedString("changePassowrdFacebookAuthenticated", comment: "Customer was authenticated using facebook, password cannot be reset."), duration: 2.0, position: .center)
                                    case 404:
                                        self.view.makeToast(NSLocalizedString("changePassowrdErrorMessageCustomerDoesNotExist", comment: "Customer does not exist"), duration: 2.0, position: .center)
                                        break
                                    case 500:
                                        self.view.makeToast(NSLocalizedString("changePassowrdErrorMessageErrorOccured", comment: "An error occurred, please try again later"), duration: 2.0, position: .center)
                                        break
                                    default:
                                        print("sky high")
                                    }
                                }
                            }
                        }
                }
            }
        } else {
            if self.emailTextField.text == "" {
                self.view.makeToast(NSLocalizedString("changePassowrdErrorMessageInsertEmail", comment: "please insert your email"), duration: 2.0, position: .bottom)
            }
        }
    }
    
    func setupViewObjectCOntraints(){
        
        var topDistance: CGFloat = 20
        // var bottomDistance: CGFloat = -10
        if UIDevice.current.hasNotch {
            topDistance = 60
            // bottomDistance = -24
        }
        
        upperInputsContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(changePasswordCloseViewButton)
        upperInputsContainerView.addSubview(changePasswordTitlePlaceHolder)
        
        changePasswordCloseViewButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        changePasswordCloseViewButton.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        changePasswordCloseViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        changePasswordCloseViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        changePasswordTitlePlaceHolder.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        changePasswordTitlePlaceHolder.rightAnchor.constraint(equalTo: changePasswordCloseViewButton.leftAnchor).isActive = true
        changePasswordTitlePlaceHolder.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor, constant: 10).isActive = true
        changePasswordTitlePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        topHeaderDescriptionPlaceHolder.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 5).isActive = true
        topHeaderDescriptionPlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        topHeaderDescriptionPlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        topHeaderDescriptionPlaceHolder.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        emailHiddenPlaceHolder.topAnchor.constraint(equalTo: topHeaderDescriptionPlaceHolder.bottomAnchor, constant: 5).isActive = true
        emailHiddenPlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emailHiddenPlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        emailHiddenPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        emailTextField.topAnchor.constraint(equalTo: emailHiddenPlaceHolder.bottomAnchor).isActive = true
        emailTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        emailSeperatorView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor).isActive = true
        emailSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emailSeperatorView.widthAnchor.constraint(equalTo: emailTextField.widthAnchor).isActive = true
        emailSeperatorView.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        sendEmailButton.topAnchor.constraint(equalTo: emailSeperatorView.bottomAnchor, constant: 15).isActive = true
        sendEmailButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        sendEmailButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        sendEmailButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
    }

}
