//
//  AppointmentAvailableTime.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 07/03/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation
import SwiftDate

struct AppointmentAvailableTime {
    var uniqueID: String
    var startDate: String
    var endDate: String
    var timezone: String
    var calendar: String
    var locale: String
    var dateObjectStart: DateInRegion
    var dateObjectEnd: DateInRegion
    var holiday: String
    var holidayDesignation: String
    var holidayTitle: String
    var holidayDescription: String
    
    init(uniqueID: String, startDate: String, endDate: String, timezone: String, calendar: String, locale: String, dateObjectStart: DateInRegion, dateObjectEnd: DateInRegion, holiday: String, holidayDesignation: String, holidayTitle: String, holidayDescription: String) {
        
        self.uniqueID = uniqueID
        self.startDate = startDate
        self.endDate = endDate
        self.timezone = timezone
        self.calendar = calendar
        self.locale = locale
        self.dateObjectStart = dateObjectStart
        self.dateObjectEnd = dateObjectEnd
        self.holiday = holiday
        self.holidayDesignation = holidayDesignation
        self.holidayTitle = holidayTitle
        self.holidayDescription = holidayDescription
    }
}
