//
//  UpcomingBarberShopBookingViewController.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 26/12/2017.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate

class UpcomingBarberShopBookingViewController: UIViewController {
    var customerMadeBookings = [Bookings]()
    var customerMadeBookingsIsCompleted = [Bookings]()
    var barberShopBookNames = [String]()
    var indexValueMain = 1
    
    var bookingSelectedStartTime = [String]()
    var bookingSelectedCalendar = [String]()
    var bookingSelectedTimeZone = [String]()
    var bookingSelectedLocal = [String]()
    var bookingSelectedTotalTime = [String]()
    var bookingSelectedServiceImageUrl = [String]()
    var bookingSelectedBarberShopName = [String]()
    var bookingSelectedBookingUniqueID = [String]()
    var bookingSelectedBarberShopUniqueID = [String]()
    var appointmentsCustomer = [AppointmentsCustomer]()
    
    var arrayLengthHolder = [Int]()
    var arrayPaymentChecksHolderFOrAppointments = [Payments]()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customUpcomingAndCompletedCollectionViewCell.self, forCellWithReuseIdentifier: "cellIdBAUP")
        cv.register(customUpcomingAndCompletedCancelledCollectionViewCell.self, forCellWithReuseIdentifier: "cellIDXOXBA")
        return cv
    }()
    
    let refresherController: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        let title = NSLocalizedString("PullToRefreshBookingView", comment: "Pull to refresh")
        refreshControl.attributedTitle = NSAttributedString(string: title)
        refreshControl.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        refreshControl.addTarget(self, action: #selector(refreshOptions(sender:)), for: .valueChanged)
        return refreshControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        navigationItem.title = NSLocalizedString("upcomingBookingButtonProfileSettingsEditorView", comment: "Upcoming Bookings")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleBackAction))
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjectContriants()
    }
    
    @objc func handleBackAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func refreshOptions(sender: UIRefreshControl) {
        print("ro money call")
        sender.endRefreshing()
    }
    
    
    
    func arrayDefaultCombineCustomerBooking(serviceImageURL: String, bookedBarberShopName: String , bookingUniqueIDDD: String , bookingTotalTime: String , bookStartTSS: String , bookTzone: String , bookClandar: String , bookLocal: String ,shopUniqueID: String ){
        
        let claxandNow = DateByUserDeviceInitializer.calenderNow
        let tznxNow = DateByUserDeviceInitializer.tzone
        let loclxnNow = DateByUserDeviceInitializer.localCode
        
        let verifyBookingDataCurrent = VerifyDateDetails.checkDateData(timeZone: tznxNow, calendar: claxandNow, locale: loclxnNow)
        
        if verifyBookingDataCurrent == true {
            let currentRegionOfDeviceNow = DateByUserDeviceInitializer.getRegion(TZoneName: tznxNow, calenName: claxandNow, LocName: loclxnNow)
            let bookingUniqueRegionSP = DateByUserDeviceInitializer.getRegion(TZoneName: bookTzone, calenName: bookClandar, LocName: bookLocal)
            
            let appoint = AppointmentsCustomer()
            appoint.serviceImageUrl = serviceImageURL
            appoint.bookedBarberShopName = bookedBarberShopName
            appoint.bookingTotalTime = bookingTotalTime + "min"
            appoint.bookingUniqueID = bookingUniqueIDDD
            appoint.barberShopUniqueID = shopUniqueID
            
            DispatchQueue.global(qos: .background).async {
                if let url = URL(string: serviceImageURL) {
                    do {
                        let data = try Data(contentsOf: url)
                        
                        if let downloadedImage = UIImage(data: data) {
                            appoint.serviceImage = downloadedImage
                            DispatchQueue.main.async {
                                self.collectionView.reloadData()
                            }
                        }
                    } catch {
                        appoint.serviceImage = UIImage()
                    }
                }
            }
            
            //bookStartTSS.toDate(style: .extended, region: bookingUniqueRegionSP)
            
            if let bookst = DateFormatVerify.checkFormat(dateString: bookStartTSS, RegionData: bookingUniqueRegionSP) {
                let dateData = bookst.convertTo(region: currentRegionOfDeviceNow)
                appoint.bookingStartTimeString = dateData.toString(DateToStringStyles.dateTime(.short))
                appoint.bookingStartTime = dateData
            } else {
                appoint.bookingStartTimeString = ""
            }
            
            if let uuiidBook = appoint.bookingUniqueID {
                if let firstNegative = self.appointmentsCustomer.first(where: { $0.bookingUniqueID ==  uuiidBook }) {
                    if let indexValue = self.appointmentsCustomer.index(of: firstNegative) {
                        self.appointmentsCustomer[indexValue] = appoint
                    }
                    print("nano squad")
                } else {
                    self.appointmentsCustomer.append(appoint)
                }
            }
            
            self.appointmentsCustomer.sort(by: { (appstx, appsty) -> Bool in
                return appstx.bookingStartTime < appsty.bookingStartTime
            })
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    func setupViewObjectContriants(){
        
        collectView.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -12).isActive = true
        collectView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refresherController
        } else {
            collectionView.addSubview(refresherController)
            collectionView.sendSubviewToBack(refresherController)
        }
    }

}
