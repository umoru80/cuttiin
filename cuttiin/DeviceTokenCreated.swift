//
//  DeviceTokenCreated.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 28/03/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation

struct DeviceTokenCreated: Codable {
    let message: String
    let devicetoken: Devicetoken
}

struct Devicetoken: Codable {
    let id, customer, dateCreated: String
    let deviceToken: [String]
    let dateCreatedTimezone, dateCreatedCalendar, dateCreatedLocale: String
    let v: Int
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case deviceToken, customer, dateCreated, dateCreatedTimezone, dateCreatedCalendar, dateCreatedLocale
        case v = "__v"
    }
}
