//
//  AppointmentServiceAmountData.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 02/04/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit

struct AppointmentServiceAmountData {
    var id: String
    var appointmentService: String
    var appointmentServiceAmount: String
    
    init(id: String, appointmentService: String, appointmentServiceAmount: String) {
        self.id = id
        self.appointmentService = appointmentService
        self.appointmentServiceAmount = appointmentServiceAmount
    }
}
