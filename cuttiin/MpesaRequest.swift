//
//  MpesaRequest.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 19/07/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation

// MARK: - MpesaRequest
struct MpesaRequest: Codable {
    let message: String
    let mpesa: Mpesa
}

// MARK: - Mpesa
struct Mpesa: Codable {
    let id, appName, typeOfMpesa, businessNumber: String
    let mobileNumber: String
    let v: Int
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case appName, typeOfMpesa, businessNumber, mobileNumber
        case v = "__v"
    }
}
