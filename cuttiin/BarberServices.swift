//
//  BarberServices.swift
//  cuttiin
//
//  Created by Umoru Joseph on 8/4/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit

@objcMembers class BarberServices: NSObject {
    var serviceID: String?
    var dateCreated: String?
    var timezone: String?
    var calendar: String?
    var local: String?
    var objectID: String?
}
