//
//  AvailableTime.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 07/03/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation

struct AvailableTime: Codable {
    let startDate, endDate, timezone, calendar: String
    let locale: String
}
