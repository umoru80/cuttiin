//
//  UtilityManager.swift
//  cuttiin
//
//  Created by Sevenstar Infotech on 23/05/18.
//  Copyright © 2018 teckdk. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class UtilityManager {
    
    static func alertView(view: UIViewController) {
        
        let alertController = UIAlertController(title: "Session expiration", message: "Your authentication has expired please sign in again.", preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            //show welcome view
            LoginManager().logOut()
            let welcomeviewcontroller = WelcomeViewController()
            welcomeviewcontroller.modalPresentationStyle = .overCurrentContext
            view.present(welcomeviewcontroller, animated: true, completion: nil)
        }
        
        alertController.addAction(action1)
        view.present(alertController, animated: true, completion: nil)
    }
    
    
}
