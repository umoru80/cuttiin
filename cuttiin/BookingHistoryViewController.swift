//
//  BookingHistoryViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 11/25/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate

class BookingHistoryViewController: UIViewController {
    var customerMadeBookings = [Bookings]()
    var customerMadeBookingsIsCompleted = [Bookings]()
    var barberShopBookNames = [String]()
    var indexValueMain = 1
    
    var bookingSelectedStartTime = [String]()
    var bookingSelectedCalendar = [String]()
    var bookingSelectedTimeZone = [String]()
    var bookingSelectedLocal = [String]()
    var bookingSelectedTotalTime = [String]()
    var bookingSelectedServiceImageUrl = [String]()
    var bookingSelectedBarberShopName = [String]()
    var bookingSelectedBookingUniqueID = [String]()
    var bookingSelectedBarberShopUniqueID = [String]()
    var appointmentsCustomer = [AppointmentsCustomer]()
    
    //cancelled Booking data holders
    
    var bookingSelectedStartTimeCancelled = [String]()
    var bookingSelectedCalendarCancelled = [String]()
    var bookingSelectedTimeZoneCancelled = [String]()
    var bookingSelectedLocalCancelled = [String]()
    var bookingSelectedTotalTimeCancelled = [String]()
    var bookingSelectedServiceImageUrlCancelled = [String]()
    var bookingSelectedBarberShopNameCancelled = [String]()
    var bookingSelectedBookingUniqueIDCancelled = [String]()
    var bookingSelectedBarberShopUniqueIDCancelled = [String]()
    var appointmentsCustomerCancelled = [AppointmentsCustomer]()
    
    var arrayLengthHolder = [Int]()
    
    lazy var upcomingAndCompletedSegmentedControl: UISegmentedControl = {
        let sssegmentcontrol = UISegmentedControl(items: [NSLocalizedString("bookingHistorySegementBookingHisoryView", comment: "BOOKING HISTORY"), NSLocalizedString("bookingCancelledSegementBookingHisoryView", comment: "BOOKING CANCELLED")])
        sssegmentcontrol.translatesAutoresizingMaskIntoConstraints = false
        sssegmentcontrol.tintColor = UIColor.white
        sssegmentcontrol.selectedSegmentIndex = 0
        sssegmentcontrol.addTarget(self, action: #selector(handleSegmentedControlSelection), for: .valueChanged)
        return sssegmentcontrol
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customUpcomingAndCompletedCollectionViewCell.self, forCellWithReuseIdentifier: "cellIdXEB")
        cv.register(customUpcomingAndCompletedCancelledCollectionViewCell.self, forCellWithReuseIdentifier: "cellIDXOX")
        return cv
    }()
    
    let refresherController: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        let title = NSLocalizedString("PullToRefreshBookingView", comment: "Pull to refresh")
        refreshControl.attributedTitle = NSAttributedString(string: title)
        refreshControl.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        refreshControl.addTarget(self, action: #selector(refreshOptions(sender:)), for: .valueChanged)
        return refreshControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        navigationItem.title = NSLocalizedString("historyButtonProfileSettingsEditorView", comment: "History")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .done, target: self, action: #selector(handleBackAction))
        view.addSubview(upcomingAndCompletedSegmentedControl)
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjectContriants()
    }
    
    @objc func handleBackAction(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func refreshOptions(sender: UIRefreshControl) {
        print("ro money call")
        sender.endRefreshing()
    }
    
    func setupViewObjectContriants(){
        
        upcomingAndCompletedSegmentedControl.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        upcomingAndCompletedSegmentedControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upcomingAndCompletedSegmentedControl.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        upcomingAndCompletedSegmentedControl.heightAnchor.constraint(equalToConstant: 29).isActive = true
        
        collectView.topAnchor.constraint(equalTo: upcomingAndCompletedSegmentedControl.bottomAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -12).isActive = true
        collectView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refresherController
        } else {
            collectionView.addSubview(refresherController)
            collectionView.sendSubviewToBack(refresherController)
        }
    }
    
    @objc func handleSegmentedControlSelection(){
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }

}

class customUpcomingAndCompletedCancelledCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let thumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.clear
        tniv.contentMode = .scaleAspectFill
        return tniv
    }()
    
    let bookedBarberShopNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let bookingStartTimePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let totalTimePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        fnhp.textColor = UIColor.white
        fnhp.textAlignment = .right
        return fnhp
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        return sv
    }()
    
    func setupViews(){
        addSubview(thumbnailImageView)
        addSubview(bookingStartTimePlaceHolder)
        addSubview(bookedBarberShopNamePlaceHolder)
        addSubview(totalTimePlaceHolder)
        addSubview(seperatorView)
        
        backgroundColor = UIColor(r: 23, g: 69, b: 90)
        addContraintsWithFormat(format: "H:|-16-[v0(90)]|", views: thumbnailImageView)
        addContraintsWithFormat(format: "H:|-116-[v0]-60-|", views: bookingStartTimePlaceHolder)
        addContraintsWithFormat(format: "H:|-116-[v0][v1(50)]-10-|", views: bookedBarberShopNamePlaceHolder, totalTimePlaceHolder)
        addContraintsWithFormat(format: "V:|-30-[v0(20)]-10-[v1(20)]-25-|", views: bookingStartTimePlaceHolder, bookedBarberShopNamePlaceHolder)
        addContraintsWithFormat(format: "V:|-30-[v0(20)]-10-[v1(20)]-10-|", views: bookingStartTimePlaceHolder, totalTimePlaceHolder)
        addContraintsWithFormat(format: "V:|-10-[v0]-10-[v1(5)]|", views: thumbnailImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    
    override func prepareForReuse() {
        self.thumbnailImageView.image = UIImage()
        self.bookingStartTimePlaceHolder.text = ""
        self.bookedBarberShopNamePlaceHolder.text = ""
        self.totalTimePlaceHolder.text = ""
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


