//
//  CollectionViewHelper.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/25/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate

extension DayTimeChoiceViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.daysOfTheWeek.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customCollectionViewCell
        //cell.dayOfTheWeek.text = self.daysOfTheWeek[indexPath.row]
        if let dayInput = self.daysOfTheWeek[safe: indexPath.row] {
            switch dayInput {
            case "Monday":
                cell.dayOfTheWeek.text = NSLocalizedString("mondayNavigationTitleOpeningTimeView", comment: "Monday")
            case "Tuesday":
                cell.dayOfTheWeek.text = NSLocalizedString("tuesdayNavigationTitleOpeningTimeView", comment: "Tuesday")
            case "Wednesday":
                cell.dayOfTheWeek.text = NSLocalizedString("wednesdayNavigationTitleOpeningTimeView", comment: "Wednesday")
            case "Thursday":
                cell.dayOfTheWeek.text = NSLocalizedString("thursdayNavigationTitleOpeningTimeView", comment: "Thursday")
            case "Friday":
                cell.dayOfTheWeek.text = NSLocalizedString("fridayNavigationTitleOpeningTimeView", comment: "Friday")
            case "Saturday":
                cell.dayOfTheWeek.text = NSLocalizedString("saturdayNavigationTitleOpeningTimeView", comment: "Saturday")
            case "Sunday":
                cell.dayOfTheWeek.text = NSLocalizedString("sundayNavigationTitleOpeningTimeView", comment: "Sunday")
            default:
                print("Sky high")
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (!self.selectedDaysOfTheWeek.contains(daysOfTheWeek[indexPath.row])) {
            self.selectedDaysOfTheWeek.append(daysOfTheWeek[indexPath.row])
        }
        
        guard let selectedCell = collectionView.cellForItem(at: indexPath) as? customCollectionViewCell else {
            print("failed")
            return
        }
        selectedCell.thumbnailImageView.image = UIImage(named: "check_box_active") //Unchecked-icon
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        print(daysOfTheWeek[indexPath.row], "Diselect")
        
        let dayDateData = daysOfTheWeek[indexPath.row]
        
        if self.selectedDaysOfTheWeek.count >= 1 {
            selectedDaysOfTheWeek = selectedDaysOfTheWeek.filter { $0 != dayDateData }
        }else {
            print("out of range")
        }
        
        guard let selectedCell = collectionView.cellForItem(at: indexPath) as? customCollectionViewCell else {
            print("failed")
            return
        }
        selectedCell.thumbnailImageView.image = UIImage(named: "check_box_inactive") //Unchecked-icon
    }
}

// Barber shop profile collection

extension BarberShopProfileViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.serviceBarberSegmentedControl.selectedSegmentIndex == 1 {
            return self.serviceOne.count
        }else {
            return self.barberlistone.count
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if self.serviceBarberSegmentedControl.selectedSegmentIndex == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customBarberShopProfileCollectionViewCell
            cell.barberShopServiceNamePlaceHolder.text = self.serviceOne[indexPath.row].serviceTitle
            cell.barberShopServiceDescriptionPlaceHolder.text = self.serviceOne[indexPath.row].shortDescription
            cell.servicePricePlaceHolder.text = self.serviceOne[indexPath.row].serviceCost! + "-"
            cell.serviceTimeTakenPlaceHolder.text = self.serviceOne[indexPath.row].estimatedTime! + "min"
            cell.barberShopCoverImageView.layer.borderColor = UIColor.clear.cgColor
            cell.barberShopServiceLogoImageView.image = self.serviceOne[indexPath.row].serviceImage
            
            return cell
        }else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId2", for: indexPath) as! customBarberShopProfileCollectionViewBarberSelectedCell
            cell.barberShopBarberLogoImageView.image = self.barberlistone[indexPath.row].profileImage
            cell.barberShopBarberNamePlaceHolder.text = self.barberlistone[indexPath.row].lastName
            cell.barberShopBarberCoverImageView.layer.borderColor = UIColor.clear.cgColor
            if let vla = self.barberlistone[indexPath.row].ratingValue{
                cell.starView.configure(self.attribute, current: vla, max: 5)
            }
            
            if let stringTime = self.openingTimeString {
                cell.barberTimeToBeAvailablePlaceHolder.text = stringTime
            } else {
                cell.barberTimeToBeAvailablePlaceHolder.text = "closed today"
            }
            
            return cell
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.serviceBarberSegmentedControl.selectedSegmentIndex == 1 {
            self.selectedServiceOne.removeAll()
            self.ladiesSelectedList.removeAll()
            self.gentsSelectedList.removeAll()
            self.kidsSelectedList.removeAll()
            guard let selSerID =  self.serviceOne[indexPath.row].serviceID else {
                return
            }
            
            if self.selectedServiceOne.contains(selSerID) {
                print("No Show")
            }else {
                self.selectedServiceOne.append(selSerID)
            }
        
            switch self.selectedLadiesGentsKidsButton {
            case "Ladies":
                if self.ladiesSelectedList.contains(selSerID) {
                    print("No Show")
                }else {
                    self.ladiesSelectedList.append(selSerID)
                }
            case "Gents":
                if self.gentsSelectedList.contains(selSerID) {
                    print("No Show")
                }else {
                    self.gentsSelectedList.append(selSerID)
                }
            case "Kids":
                if self.kidsSelectedList.contains(selSerID) {
                    print("No Show")
                }else {
                    self.kidsSelectedList.append(selSerID)
                }
            default:
                print("Sky high")
            }
        
            print(self.ladiesSelectedList, "a")
            print(self.gentsSelectedList, "b")
            print(self.kidsSelectedList, "c")
            print(self.selectedServiceOne.count, "Hell razar")
            
            self.handleMoveToTheNextView()
            
        } else {
            
            self.selectedBarberOne.removeAll()
            self.availableSelectedList.removeAll()
            self.topRatedSelectedList.removeAll()
            guard let selSerID =  self.barberlistone[indexPath.row].uniqueID else {
                return
            }
            
            if self.selectedBarberOne.contains(selSerID) {
                print("No Show")
            }else {
                self.selectedBarberOne.append(selSerID)
            }
            
            if self.selectedBarberOne.count > 0 {
                self.navigationItem.rightBarButtonItem?.isEnabled = true
            }else {
                self.navigationItem.rightBarButtonItem?.isEnabled = false
            }
            
            switch self.selectedAvailableTopRated {
            case "Available":
                if self.availableSelectedList.contains(selSerID) {
                    print("No Show")
                }else {
                    self.availableSelectedList.append(selSerID)
                }
            case "Top Rated":
                if self.topRatedSelectedList.contains(selSerID) {
                    print("No Show")
                }else {
                    self.topRatedSelectedList.append(selSerID)
                }
            default:
                print("Sky high")
            }
            
            //print(self.availableSelectedList, "c")
            //print(self.topRatedSelectedList, "d")
            //print(self.selectedBarberOne, "e")
            
            self.handleMoveToTheNextView()

        }
    }
    

}



extension AfterChoiceBarberShopProfileServiceViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.serviceOne.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId3", for: indexPath) as! customBarberShopProfileCollectionViewCell
        cell.barberShopServiceNamePlaceHolder.text = self.serviceOne[indexPath.row].serviceTitle
        cell.barberShopServiceDescriptionPlaceHolder.text = self.serviceOne[indexPath.row].shortDescription
        cell.servicePricePlaceHolder.text = self.serviceOne[indexPath.row].serviceCost! + "-"
        cell.serviceTimeTakenPlaceHolder.text = self.serviceOne[indexPath.row].estimatedTime! + "min"
        cell.barberShopServiceLogoImageView.image = self.serviceOne[indexPath.row].serviceImage
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedServiceOne.removeAll()
        self.ladiesSelectedList.removeAll()
        self.gentsSelectedList.removeAll()
        self.kidsSelectedList.removeAll()
        
        if let selSerID =  self.serviceOne[indexPath.row].serviceID {
            if self.selectedServiceOne.contains(selSerID) {
                print("No Show")
            }else {
                self.selectedServiceOne.append(selSerID)
            }
            
            
            switch self.selectedLadiesGentsKidsButton {
            case "Ladies":
                if self.ladiesSelectedList.contains(selSerID) {
                    print("No Show")
                }else {
                    self.ladiesSelectedList.append(selSerID)
                }
            case "Gents":
                if self.gentsSelectedList.contains(selSerID) {
                    print("No Show")
                }else {
                    self.gentsSelectedList.append(selSerID)
                }
            case "Kids":
                if self.kidsSelectedList.contains(selSerID) {
                    print("No Show")
                }else {
                    self.kidsSelectedList.append(selSerID)
                }
            default:
                print("Sky high")
            }
            
            //print(self.ladiesSelectedList, "a")
            //print(self.gentsSelectedList, "b")
            //print(self.kidsSelectedList, "c")
            self.handleMoveToTheNextView()
        }
    }
}




extension AfterChoiceBarberShopProfileBarberViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.barberlistone.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId2", for: indexPath) as! customBarberShopProfileCollectionViewBarberSelectedCell
        cell.barberShopBarberLogoImageView.image = self.barberlistone[indexPath.row].profileImage
        cell.barberShopBarberNamePlaceHolder.text = self.barberlistone[indexPath.row].lastName
        
        if let vla = self.barberlistone[indexPath.row].ratingValue{
            cell.starView.configure(self.attribute, current: vla, max: 5)
        }
        
        if let stringTime = self.openingTimeString {
            cell.barberTimeToBeAvailablePlaceHolder.text = stringTime
        } else {
            cell.barberTimeToBeAvailablePlaceHolder.text = "closed today"
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedBarberOne.removeAll()
        
        if let selSerID =  self.barberlistone[indexPath.row].uniqueID {
            if self.selectedBarberOne.contains(selSerID) {
                print("No Show")
            }else {
                self.selectedBarberOne.append(selSerID)
            }
            
            self.handleMoveToTheNextView()
        }
    }
}



extension NumberOfCustomersViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.serviceOneBasedNew.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId5", for: indexPath) as! customNumberOfCustomerCollectionViewCell
        cell.collectionServiceNamePlaceHolder.text = self.serviceOneBasedNew[indexPath.row].serviceTitle
        cell.collectionAdditionButton.tag = indexPath.row
        cell.collectionAdditionButton.addTarget(self, action: #selector(handleCollectionCellAddition), for: .touchUpInside)
        cell.collectionAmountPlaceHolder.text = String(self.totalQuantity[indexPath.row])
        cell.collctionSubtractionButton.tag = indexPath.row
        cell.collctionSubtractionButton.addTarget(self, action: #selector(handleCollectionCellSubtraction), for: .touchUpInside)
        cell.collectionTimeTakenPlaceHolder.text = self.serviceOneBasedNew[indexPath.row].estimatedTime! + "min"
        cell.collectionPricePlaceHolder.text = self.serviceOneBasedNew[indexPath.row].serviceCost! + "-"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension BookingDateViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.availableTimesCalled.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellIdxx", for: indexPath) as! customBookingDateCollectionViewCell
        
        cell.openStartTimeHolder.text = ""
        if let timeHold = self.availableTimesCalled[safe: indexPath.row]?.bookingStartTimeEndTimeString {
            cell.openStartTimeHolder.text = timeHold
        } else {
            cell.openStartTimeHolder.text = ""
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 250, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("soul soldier")
        if let selectObject = self.availableTimesCalled[safe: indexPath.row]?.bookingStartTimeFirst {
            let selectObjectNewDate = selectObject + 1.minutes
            let selectObjectDate = selectObjectNewDate.date
        }
    }
}

