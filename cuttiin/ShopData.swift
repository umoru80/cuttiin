//
//  ShopData.swift
//  cuttiin
//
//  Created by Joseph Umoru on 22/03/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit

struct ShopData {
    var id, shopAddress, shopAddressLatitude, shopAddressLongitude: String
    var shopCanViewBookings, shopCountry, shopCountryCode, shopCountryCurrencyCode: String
    var shopCustomerGender, shopDateCreated, shopDateCreatedCalendar, shopDateCreatedLocale: String
    var shopDateCreatedTimezone, shopDescription, shopEmail, shopHiddenByAdmin: String
    var shopHiddenByCustomer, shopLogoImageName: String
    var shopLogoImageURL: String
    var shopMobileNumber, shopName, shopOwner, shopRating: String
    var shortUniqueID: String
    
    init(id: String, shopAddress: String, shopAddressLatitude: String, shopAddressLongitude: String, shopCanViewBookings: String, shopCountry: String, shopCountryCode: String, shopCountryCurrencyCode: String, shopCustomerGender: String, shopDateCreated: String, shopDateCreatedCalendar: String, shopDateCreatedLocale: String, shopDateCreatedTimezone: String, shopDescription: String, shopEmail: String, shopHiddenByAdmin: String, shopHiddenByCustomer: String, shopLogoImageName: String, shopLogoImageURL: String, shopMobileNumber: String, shopName: String, shopOwner: String, shopRating: String, shortUniqueID: String) {
        
        self.id = id
        self.shopAddress = shopAddress
        self.shopAddressLatitude = shopAddressLatitude
        self.shopAddressLongitude = shopAddressLongitude
        self.shopCanViewBookings = shopCanViewBookings
        self.shopCountry = shopCountry
        self.shopCountryCode = shopCountryCode
        self.shopCountryCurrencyCode = shopCountryCurrencyCode
        self.shopCustomerGender = shopCustomerGender
        self.shopDateCreated = shopDateCreated
        self.shopDateCreatedCalendar = shopDateCreatedCalendar
        self.shopDateCreatedLocale = shopDateCreatedLocale
        self.shopDateCreatedTimezone = shopDateCreatedTimezone
        self.shopDescription = shopDescription
        self.shopEmail = shopEmail
        self.shopHiddenByAdmin = shopHiddenByAdmin
        self.shopHiddenByCustomer = shopHiddenByCustomer
        self.shopLogoImageName = shopLogoImageName
        self.shopLogoImageURL = shopLogoImageURL
        self.shopMobileNumber = shopMobileNumber
        self.shopName = shopName
        self.shopOwner = shopOwner
        self.shopRating = shopRating
        self.shortUniqueID = shortUniqueID
        
    }
    
    
}


