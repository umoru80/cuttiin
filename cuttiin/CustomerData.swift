//
//  CustomerData.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 27/01/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation

struct CustomerData: Codable {
    let customer: Customer
    let message: String
}

struct Customer: Codable {
    let v: Int
    let id, accountDeactivated, authenticationType, dateCreated: String
    let dateCreatedCalendar, dateCreatedLocale, dateCreatedTimezone, dateOfBirth: String
    let dateOfBirthCalendar, dateOfBirthLocale, dateOfBirthTimezone, email: String
    let firstName, gender, lastName, loginCalendar: String
    let loginDate, loginLocale, loginTimezone, mobileNumber: String
    let password, profileImageName: String
    let profileImageURL: String
    let shopOwner, shortUniqueID: String
    let customerVerificationStatus, customerVerificationDate, customerVerificationDateTimeZone, customerVerificationDateCalendar, customerVerificationDateLocale: String
    let customerRating, customerStaffRating: String
    
    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case accountDeactivated, authenticationType, dateCreated, dateCreatedCalendar, dateCreatedLocale, dateCreatedTimezone, dateOfBirth, dateOfBirthCalendar, dateOfBirthLocale, dateOfBirthTimezone, email, firstName, gender, lastName, loginCalendar, loginDate, loginLocale, loginTimezone, mobileNumber, password, profileImageName, customerVerificationStatus, customerVerificationDate, customerVerificationDateTimeZone, customerVerificationDateCalendar, customerVerificationDateLocale, customerRating, customerStaffRating
        case profileImageURL = "profileImageUrl"
        case shopOwner
        case shortUniqueID = "shortUniqueId"
    }
}
