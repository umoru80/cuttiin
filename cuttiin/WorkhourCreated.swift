//
//  WorkHourCreated.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 09/02/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//


import Foundation

struct WorkhourCreated: Codable {
    let message: String
    let workHour: WorkHour
}

struct WorkHour: Codable {
    let v: Int
    let id, shop, workhourCalendar, workhourLastUpdated: String
    let workhourLocale: String
    let workhourSchedule: [WorkhourSchedule]
    let workhourTimezone: String
    
    enum CodingKeys: String, CodingKey {
        case v = "__v"
        case id = "_id"
        case shop, workhourCalendar, workhourLastUpdated, workhourLocale, workhourSchedule, workhourTimezone
    }
}

struct WorkhourSchedule: Codable {
    let id, workhourCloseDate, workhourOpenDate, workhourWeekDay: String
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case workhourCloseDate, workhourOpenDate, workhourWeekDay
    }
}
