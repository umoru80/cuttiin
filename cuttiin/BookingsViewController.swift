//
//  BookingsViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/13/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import SwiftDate
import Alamofire

class BookingsViewController: UIViewController, UITextViewDelegate {
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    var customerAppointments = [AppointmentData]()
    var customerAppointmentsReset = [AppointmentData]()
    var alwaynewAppointmentCustomer = [AppointmentData]()
    var serviceAmountCustomerArray = [AppointmentServiceAmountData]()
    
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    lazy var bookingViewSettingsViewIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8appointmentHistory")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var closeViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(exitThisView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let viewTitleNameLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("bookingViewControllerTitle", comment: "All Appointments")
        ht.font = UIFont(name: "OpenSans-SemiBold", size: 17)
        ht.textColor = UIColor.black
        ht.textAlignment = .left
        return ht
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    let dateFilterTitleNameLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("bookingViewControllerDateFilter", comment: "Date filter")
        ht.font = UIFont(name: "OpenSans-Light", size: 12)
        ht.textColor = UIColor.black
        ht.textAlignment = .center
        return ht
    }()
    
    let dateSelectButtonsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.isUserInteractionEnabled = true
        return tcview
    }()
    
    lazy var fromDateButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        st.setTitle(NSLocalizedString("bookingViewControllerDateFilterFrom", comment: "From"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 14)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.tag = 1
        st.addTarget(self, action: #selector(handleSelectingFromDate(_:)), for: .touchUpInside)
        st.isEnabled = true
        return st
    }()
    
    lazy var toDateButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        st.setTitle(NSLocalizedString("bookingViewControllerDateFilterTo", comment: "To"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 14)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.tag = 2
        st.addTarget(self, action: #selector(handleSelectToDate(_:)), for: .touchUpInside)
        st.isEnabled = true
        return st
    }()
    
    let dateFilterButtonsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.isUserInteractionEnabled = true
        return tcview
    }()
    
    lazy var resetDateButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        st.setTitle(NSLocalizedString("bookingViewControllerDateFilterReset", comment: "Reset"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 14)
        st.layer.cornerRadius = 5
        st.layer.borderColor = UIColor(r: 11, g: 49, b: 68).cgColor
        st.addTarget(self, action: #selector(handleResetAppointmentByDate(_:)), for: .touchUpInside)
        st.isEnabled = true
        return st
    }()
    
    let curvedCollectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.layer.masksToBounds = true
        cview.layer.cornerRadius = 5
        cview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return cview
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.alwaysBounceVertical = true
        cv.register(customUpcomingAndCompletedCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()
    
    let refresherController: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        let title = NSLocalizedString("PullToRefreshBookingView", comment: "Pull to refresh")
        refreshControl.attributedTitle = NSAttributedString(string: title)
        refreshControl.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        refreshControl.addTarget(self, action: #selector(refreshOptions(sender:)), for: .valueChanged)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        view.addSubview(dateFilterTitleNameLabel)
        view.addSubview(dateSelectButtonsContainerView)
        view.addSubview(dateFilterButtonsContainerView)
        view.addSubview(curvedCollectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjectContriants()
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    @objc private func refreshOptions(sender: UIRefreshControl) {
        getCustomerAppointment()
        sender.endRefreshing()
    }
    
    @objc func dateChanged(dateSelected: Date, buttonTag: Int) {
        let timezone = DateByUserDeviceInitializer.tzone
        let region = DateByUserDeviceInitializer.getRegion(TZoneName: timezone, calenName: "gregorian", LocName: "en")
        let currentDate = dateSelected.convertTo(region: region)
        
        if (!customerAppointments.isEmpty){
            for appointment in self.customerAppointments {
                
                switch(buttonTag){
                case 1:
                    self.fromDateButton.setTitle(NSLocalizedString("bookingViewControllerDateFilterFrom", comment: "From") + " : " + currentDate.toString(DateToStringStyles.date(.medium)), for: .normal)
                    if appointment.appointmentDateStartObj.isBeforeDate(currentDate, granularity: .day), let index = self.customerAppointments.firstIndex(where: {$0.id == appointment.id}) {
                        print(index, appointment.id)
                        self.customerAppointments.remove(at: index)
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                        }
                    }
                    break
                case 2:
                    self.toDateButton.setTitle(NSLocalizedString("bookingViewControllerDateFilterTo", comment: "To") + " : " + currentDate.toString(DateToStringStyles.date(.medium)), for: .normal)
                    if appointment.appointmentDateStartObj.isAfterDate(currentDate, granularity: .day), let index = self.customerAppointments.firstIndex(where: {$0.id == appointment.id}) {
                        print(index, appointment.id)
                        self.customerAppointments.remove(at: index)
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                        }
                    }
                    break
                default:
                    print("invalid entry")
                    break
                }
                
            }
        }
    }
    
    @objc func getCustomerAppointment(){
        print("function called")
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let customerId = decodedCustomer.first?.customerId {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    print("pro style")
                    DispatchQueue.global(qos: .background).async {
                        AF.request(self.BACKEND_URL + "getAllAppointmentsByCustomer/" + customerId, method: .post, headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    do {
                                        let customerShopData = try JSONDecoder().decode(AppointmentStaffCustomer.self, from: jsonData)
                                        
                                        let staffCustomerList = customerShopData.staffcustomer
                                        let appointmentCustomerList = customerShopData.customer
                                        let appointmentList = customerShopData.appointment
                                        self.alwaynewAppointmentCustomer.removeAll()
                                        
                                        if (appointmentList.isEmpty){
                                            DispatchQueue.main.async {
                                                self.customerAppointments.removeAll()
                                                self.customerAppointmentsReset.removeAll()
                                                self.collectionView.reloadData()
                                            }
                                        }
                                        
                                        for appoint in appointmentList {
                                            let appointmentRegion = DateByUserDeviceInitializer.getRegion(TZoneName: appoint.appointmentDateTimezone, calenName: appoint.appointmentDateCalendar, LocName: appoint.appointmentDateLocale)
                                            guard let startDate = appoint.appointmentDateStart.toDate("yyyy-MM-dd HH:mm:ss", region: appointmentRegion),
                                                let endDate = appoint.appointmentDateEnd.toDate("yyyy-MM-dd HH:mm:ss", region: appointmentRegion) else {
                                                    return
                                            }
                                            
                                            let startDateString = startDate.toString(DateToStringStyles.dateTime(.medium))
                                            
                                            let completDateString = startDateString
                                            
                                            
                                            guard let firstCustomer = appointmentCustomerList.first(where: {$0.id == appoint.appointmentCustomer}) else {
                                                return
                                            }
                                            guard let firstStaff = staffCustomerList.first(where: {$0.id == appoint.appointmentStaffCustomer}) else {
                                                return
                                            }
                                            
                                            if let blankImage = UIImage(named: "avatar_icon"){
                                                
                                                let staffName = firstStaff.firstName + " " + firstStaff.lastName
                                                
                                                
                                                for dataAmount in appoint.appointmentServiceWithAmount {
                                                    let singleServiceAmount = AppointmentServiceAmountData(id: dataAmount.id, appointmentService: dataAmount.appointmentService, appointmentServiceAmount: dataAmount.appointmentServiceAmount)
                                                    self.serviceAmountCustomerArray.append(singleServiceAmount)
                                                    if (appoint.appointmentServiceWithAmount.count == self.serviceAmountCustomerArray.count && self.serviceAmountCustomerArray.count > 0){
                                                        
                                                        let singleAppointment = AppointmentData(id: appoint.id, appointmentService: appoint.appointmentService, appointmentShortUniqueID: appoint.appointmentShortUniqueID, appointmentDateStart: appoint.appointmentDateStart, appointmentDateEnd: appoint.appointmentDateEnd, appointmentDateTimezone: appoint.appointmentDateTimezone, appointmentDateCalendar: appoint.appointmentDateCalendar, appointmentDateLocale: appoint.appointmentDateLocale, appointmentDateCreated: appoint.appointmentDateCreated, appointmentDescription: appoint.appointmentDescription, appointmentPrice: appoint.appointmentPrice, appointmentCurrency: appoint.appointmentCurrency, appointmentCustomer: appoint.appointmentCustomer, appointmentStaff: appoint.appointmentStaff, appointmentStaffCustomer: appoint.appointmentStaffCustomer, appointmentShop: appoint.appointmentShop, appointmentServiceQuantity: appoint.appointmentServiceQuantity, appointmentStartedBy: appoint.appointmentStartedBy, appointmentEndedBy: appoint.appointmentEndedBy, appointmentStartedAt: appoint.appointmentStartedAt, appointmentEndedAt: appoint.appointmentEndedAt, appointmentStartedEndedTimezone: appoint.appointmentStartedEndedTimezone, appointmentStartedEndedCalendar: appoint.appointmentStartedEndedCalendar, appointmentStartedEndedLocale: appoint.appointmentStartedEndedLocale, appointmentPaymentMethod: appoint.appointmentPaymentMethod, appointmentStatus: appoint.appointmentStatus, appointmentPaidDate: appoint.appointmentPaidDate, appointmentPaidTimezone: appoint.appointmentPaidTimezone, appointmentPaidCalendar: appoint.appointmentPaidCalendar, appointmentPaidLocale: appoint.appointmentPaidLocale, v: appoint.v, appointmentCancelledBy: appoint.appointmentCancelledBy, appontmentCustomerImage: blankImage, appointmentCustomerFirstName: firstCustomer.firstName, appointmentStaffFirstName: staffName, appointmentFormatedStartEndTime: completDateString, appointmentDateStartObj: startDate, appointmentDateEndObj: endDate, appointmentServiceWithAmount: self.serviceAmountCustomerArray)
                                                        self.handleAddingandUpdatingAppointmentArray(newAppointment: singleAppointment, arraySize: appointmentList.count )
                                                        self.serviceAmountCustomerArray.removeAll()
                                                        
                                                    }
                                                }
                                                
                                            }
                                        }
                                        
                                        
                                    } catch _ {
                                        if let statusCode = response.response?.statusCode{
                                            print(statusCode)
                                            switch (statusCode){
                                            case 404:
                                                DispatchQueue.main.async {
                                                    self.customerAppointments.removeAll()
                                                    self.customerAppointmentsReset.removeAll()
                                                    self.collectionView.reloadData()
                                                }
                                            case 500:
                                                print("An error occurred")
                                            default:
                                                print("Sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
        }
    }
    
    private func handleAddingandUpdatingAppointmentArray(newAppointment: AppointmentData, arraySize: Int ){
        self.alwaynewAppointmentCustomer.append(newAppointment)
        
        if (self.customerAppointments.isEmpty){
            self.customerAppointments.append(newAppointment)
            self.customerAppointmentsReset.append(newAppointment)
            self.customerAppointments.sort { (first, second) -> Bool in
                first.appointmentDateStartObj.isAfterDate(second.appointmentDateStartObj, granularity: .minute)
            }
            //handle search
            self.customerAppointmentsReset.sort { (first, second) -> Bool in
                first.appointmentDateStartObj.isAfterDate(second.appointmentDateStartObj, granularity: .minute)
            }
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        } else {
            if let row = self.customerAppointments.index(where: {$0.id == newAppointment.id}) {
                self.customerAppointments[row] = newAppointment
                self.customerAppointmentsReset[row] = newAppointment
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            } else {
                self.customerAppointments.append(newAppointment)
                self.customerAppointmentsReset.append(newAppointment)
                self.customerAppointments.sort { (first, second) -> Bool in
                    first.appointmentDateStartObj.isAfterDate(second.appointmentDateStartObj, granularity: .minute)
                }
                //handle search
                self.customerAppointmentsReset.sort { (first, second) -> Bool in
                    first.appointmentDateStartObj.isAfterDate(second.appointmentDateStartObj, granularity: .minute)
                }
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
        }
        
        if (self.alwaynewAppointmentCustomer.count == arraySize){
            print("old data eliminator called")
            for data in customerAppointments {
                
                if (!self.alwaynewAppointmentCustomer.contains(where: {$0.id == data.id })){
                    if let firstIndex = self.customerAppointments.firstIndex(where: {$0.id == data.id}){
                        self.customerAppointments.remove(at: firstIndex)
                        self.customerAppointmentsReset.remove(at: firstIndex)
                        print("data eliminated", firstIndex)
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                        }
                    }
                }
                
                
            }
        }
    }
    
    func setupViewObjectContriants(){
        
        var topDistance: CGFloat = 20
        var bottomDistance: CGFloat = -10
        if UIDevice.current.hasNotch {
            topDistance = 60
            bottomDistance = -24
        }
        
        upperInputsContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(bookingViewSettingsViewIcon)
        upperInputsContainerView.addSubview(viewTitleNameLabel)
        upperInputsContainerView.addSubview(closeViewButton)
        
        bookingViewSettingsViewIcon.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        bookingViewSettingsViewIcon.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor).isActive = true
        bookingViewSettingsViewIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        bookingViewSettingsViewIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        closeViewButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        closeViewButton.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        closeViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        closeViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        viewTitleNameLabel.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        viewTitleNameLabel.leftAnchor.constraint(equalTo: bookingViewSettingsViewIcon.rightAnchor, constant: 10).isActive = true
        viewTitleNameLabel.rightAnchor.constraint(equalTo: closeViewButton.leftAnchor).isActive = true
        viewTitleNameLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        //here
        
        dateFilterTitleNameLabel.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 10).isActive = true
        dateFilterTitleNameLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        dateFilterTitleNameLabel.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        dateFilterTitleNameLabel.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        dateSelectButtonsContainerView.topAnchor.constraint(equalTo: dateFilterTitleNameLabel.bottomAnchor, constant: 10).isActive = true
        dateSelectButtonsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        dateSelectButtonsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        dateSelectButtonsContainerView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        dateSelectButtonsContainerView.addSubview(fromDateButton)
        dateSelectButtonsContainerView.addSubview(toDateButton)
        
        fromDateButton.topAnchor.constraint(equalTo: dateSelectButtonsContainerView.topAnchor).isActive = true
        fromDateButton.leftAnchor.constraint(equalTo: dateSelectButtonsContainerView.leftAnchor).isActive = true
        fromDateButton.rightAnchor.constraint(equalTo: dateSelectButtonsContainerView.centerXAnchor).isActive = true
        fromDateButton.heightAnchor.constraint(equalTo: dateSelectButtonsContainerView.heightAnchor).isActive = true
        
        toDateButton.topAnchor.constraint(equalTo: dateSelectButtonsContainerView.topAnchor).isActive = true
        toDateButton.rightAnchor.constraint(equalTo: dateSelectButtonsContainerView.rightAnchor).isActive = true
        toDateButton.leftAnchor.constraint(equalTo: dateSelectButtonsContainerView.centerXAnchor, constant: 20).isActive = true
        toDateButton.heightAnchor.constraint(equalTo: dateSelectButtonsContainerView.heightAnchor).isActive = true
        
        dateFilterButtonsContainerView.topAnchor.constraint(equalTo: dateSelectButtonsContainerView.bottomAnchor, constant: 10).isActive = true
        dateFilterButtonsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        dateFilterButtonsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        dateFilterButtonsContainerView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        dateFilterButtonsContainerView.addSubview(resetDateButton)
        
        
        resetDateButton.topAnchor.constraint(equalTo: dateFilterButtonsContainerView.topAnchor).isActive = true
        resetDateButton.centerXAnchor.constraint(equalTo: dateFilterButtonsContainerView.centerXAnchor).isActive = true
        resetDateButton.widthAnchor.constraint(equalTo: dateFilterButtonsContainerView.widthAnchor, multiplier: 0.5).isActive = true
        resetDateButton.heightAnchor.constraint(equalTo: dateFilterButtonsContainerView.heightAnchor).isActive = true
        
        curvedCollectView.topAnchor.constraint(equalTo: dateFilterButtonsContainerView.bottomAnchor, constant: 10).isActive = true
        curvedCollectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        curvedCollectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -24).isActive = true
        curvedCollectView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: bottomDistance).isActive = true
        
        curvedCollectView.addSubview(collectView)
        
        collectView.topAnchor.constraint(equalTo: curvedCollectView.topAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: curvedCollectView.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: curvedCollectView.widthAnchor, constant: -10).isActive = true
        collectView.heightAnchor.constraint(equalTo: curvedCollectView.heightAnchor, constant: -10).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refresherController
        } else {
            collectionView.addSubview(refresherController)
            collectionView.sendSubviewToBack(refresherController)
        }
        
        self.getCustomerAppointment()
    }
    
    @objc private func exitThisView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func handleSelectingFromDate(_ sender: UIButton){
        sender.pulsate()
        let datePickerView = DatePickerViewController()
        datePickerView.bookingViewController = self
        datePickerView.tagButton = sender.tag
        datePickerView.viewWhoInitiatedAction = "bookingView"
        present(datePickerView, animated: false, completion: nil)
    }
    
    @objc private func handleSelectToDate(_ sender: UIButton){
        sender.pulsate()
        let datePickerView = DatePickerViewController()
        datePickerView.bookingViewController = self
        datePickerView.tagButton = sender.tag
        datePickerView.viewWhoInitiatedAction = "bookingView"
        present(datePickerView, animated: false, completion: nil)
    }
    
    @objc private func handleFilterAppointmentByDate(_ sender: UIButton){
        sender.pulsate()
    }
    
    @objc private func handleResetAppointmentByDate(_ sender: UIButton){
        sender.pulsate()
        
        if (self.customerAppointmentsReset.isEmpty){
            self.getCustomerAppointment()
            self.fromDateButton.setTitle(NSLocalizedString("bookingViewControllerDateFilterFrom", comment: "From"), for: .normal)
            self.toDateButton.setTitle(NSLocalizedString("bookingViewControllerDateFilterTo", comment: "To"), for: .normal)
        } else {
            self.customerAppointments.removeAll()
            self.customerAppointments = self.customerAppointmentsReset
            self.fromDateButton.setTitle(NSLocalizedString("bookingViewControllerDateFilterFrom", comment: "From"), for: .normal)
            self.toDateButton.setTitle(NSLocalizedString("bookingViewControllerDateFilterTo", comment: "To"), for: .normal)
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
}


class customUpcomingAndCompletedCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    lazy var clientNamePlaceHolderLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8CustomerLogo")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let clientNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var startTimePlaceHolderLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8timeSpanLogo")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let startTimePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var staffNamePlaceHolderLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8staffLogo")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let staffNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var servicePricePlaceHolderLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8cashLogo")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let servicePricePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var appointmentStatusPlaceHolderLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8appointmentStatus")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let appointmentStatusPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    func setupViews(){
        addSubview(startTimePlaceHolder)
        addSubview(startTimePlaceHolderLogo)
        addSubview(staffNamePlaceHolder)
        addSubview(staffNamePlaceHolderLogo)
        addSubview(servicePricePlaceHolder)
        addSubview(servicePricePlaceHolderLogo)
        addSubview(appointmentStatusPlaceHolder)
        addSubview(appointmentStatusPlaceHolderLogo)
        
        backgroundColor = UIColor.white
        layer.cornerRadius = 5
        layer.masksToBounds = true
        
        addContraintsWithFormat(format: "H:|-10-[v0(20)]-5-[v1]-5-|", views: startTimePlaceHolderLogo, startTimePlaceHolder)
        addContraintsWithFormat(format: "H:|-10-[v0(20)]-5-[v1]-5-|", views: staffNamePlaceHolderLogo, staffNamePlaceHolder)
        addContraintsWithFormat(format: "H:|-10-[v0(20)]-5-[v1]-5-|", views: servicePricePlaceHolderLogo, servicePricePlaceHolder)
        addContraintsWithFormat(format: "H:|-10-[v0(20)]-5-[v1]-5-|", views: appointmentStatusPlaceHolderLogo, appointmentStatusPlaceHolder)
        
        addContraintsWithFormat(format: "V:|-5-[v0(20)]-5-[v1(20)]-5-[v2(20)]-5-[v3(20)]-5-|", views: startTimePlaceHolderLogo, staffNamePlaceHolderLogo, servicePricePlaceHolderLogo, appointmentStatusPlaceHolderLogo)
        addContraintsWithFormat(format: "V:|-5-[v0(20)]-5-[v1(20)]-5-[v2(20)]-5-[v3(20)]-5-|", views: startTimePlaceHolder, staffNamePlaceHolder, servicePricePlaceHolder, appointmentStatusPlaceHolder)
        
    }
    
    override func prepareForReuse() {
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension BookingsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.customerAppointments.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customUpcomingAndCompletedCollectionViewCell
        cell.startTimePlaceHolder.text = self.customerAppointments[indexPath.row].appointmentFormatedStartEndTime
        cell.staffNamePlaceHolder.text = self.customerAppointments[indexPath.row].appointmentStaffFirstName
        let curr = self.customerAppointments[indexPath.row].appointmentCurrency
        cell.servicePricePlaceHolder.text = self.customerAppointments[indexPath.row].appointmentPrice + " " + curr
        cell.appointmentStatusPlaceHolder.text = self.customerAppointments[indexPath.row].appointmentStatus
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let appointmentDetail = OrderDetailCustomerViewController()
        appointmentDetail.appointmentID = self.customerAppointments[indexPath.row].id
        appointmentDetail.staffID = self.customerAppointments[indexPath.row].appointmentStaffCustomer
        appointmentDetail.customerID = self.customerAppointments[indexPath.row].appointmentCustomer
        appointmentDetail.shopID = self.customerAppointments[indexPath.row].appointmentShop
        appointmentDetail.serviceList = self.customerAppointments[indexPath.row].appointmentService
        appointmentDetail.modalPresentationStyle = .overCurrentContext
        present(appointmentDetail, animated: true, completion: nil)
    }
}
