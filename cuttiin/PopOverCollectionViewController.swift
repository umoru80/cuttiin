//
//  PopOverCollectionViewController.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 25/02/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit
import MGStarRatingView
import SwiftDate
import Alamofire
import FBSDKLoginKit
import SwiftSpinner
import SwiftyJSON

class PopOverCollectionViewController: UIViewController, StarRatingDelegate {
    let starView = StarRatingView()
    let attribute = StarRatingAttribute(type: .rate,point: 20,spacing: 5, emptyColor: .black, fillColor: .white)
    var collectionViewSelected: Int?
    var barberOne = BarberShop()
    var serviceOne = [ServiceData]()
    var barberlistone = [StaffData]()
    var serviceCategoryLadies = [ServiceData]()
    var serviceCategoryGents = [ServiceData]()
    var serviceCategoryKids = [ServiceData]()
    var StringListOfBarbers = [String]()
    var selectedServiceOne = [String]()
    var selectedBarberOne: String?
    var ladiesSelectedList = [String]()
    var gentsSelectedList = [String]()
    var kidsSelectedList = [String]()
    var availableSelectedList = [String]()
    var topRatedSelectedList = [String]()
    var category = "Gents"
    var selectedLadiesGentsKidsButton = "Gents"
    var selectedAvailableTopRated = "Available"
    var selectedButtonBeforeSwitchingView = "Gents"
    var selectedButtonAfterSwitchingView = "Available"
    var openingTimeString: String?
    var shopCurrency: String?
    var numberOfCustomerValue = "1"
    var createAppointmentViewController = CreateAppointmentViewController()
    var masterServiceListArray = [ServiceData]()
    var staffSelected: String?
    var appointmentAvailableTimeArray = [AppointmentAvailableTime]()
    
    //create booking details
    var staffID: String?
    var customerStaff: String?
    var servicesSelectedByUser = [String]()
    var dateChoseByUser: String?
    var shopTimeZone: String?
    var shopCalendar: String?
    var shopLocal: String?
    var numberOfCustomer: String?
    var totalPrice: String?
    var staffCustomerId: String?
    var serviceTotalTimeTaken: String?
    var shopOwnerCustomerId: String?
    //create booking details
    
    //service array parameter
    var shopServiceArrayParameter = [Parameters]()
    //service array parameter
    
    var shopID: String?
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    lazy var closeViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 2
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor(r: 11, g: 49, b: 68))
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(exitViewAfterStaffChoice)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let categoryButtonContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var ladiesButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor.clear
        fbutton.setTitle(NSLocalizedString("ladiesTapCartegoryAfterBarberShopProfileService", comment: "LADIES(0)"), for: .normal)
        fbutton.setTitleColor(UIColor.black, for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 15)
        fbutton.addTarget(self, action: #selector(handleShowLadiesList), for: .touchUpInside)
        return fbutton
    }()
    
    let ladiesButtonSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor.clear
        return fnsv
    }()
    
    lazy var gentsButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor.clear
        fbutton.setTitle(NSLocalizedString("gentsTabCategoryBarberShopProfile", comment: "GENTS(0)"), for: .normal)
        fbutton.setTitleColor(UIColor.black, for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 15)
        fbutton.addTarget(self, action: #selector(handleShowGentsList), for: .touchUpInside)
        return fbutton
    }()
    
    let gentsButtonSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor.black
        return fnsv
    }()
    
    lazy var kidsButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor.clear
        fbutton.setTitle(NSLocalizedString("kidsTapCartegoryAfterBarberShopProfileService", comment: "KIDS(0)"), for: .normal)
        fbutton.setTitleColor(UIColor.black, for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 15)
        fbutton.addTarget(self, action: #selector(handleShowKidsList), for: .touchUpInside)
        return fbutton
    }()
    
    let kidsButtonSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor.clear
        return fnsv
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.layer.cornerRadius = 5
        cv.layer.masksToBounds = true
        cv.backgroundColor = UIColor.clear
        cv.allowsMultipleSelection = true
        cv.register(customServiceCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        cv.register(customStaffCollectionViewCell.self, forCellWithReuseIdentifier: "cellId2")
        cv.register(customAppointmentAvailableTimeCollectionViewCell.self, forCellWithReuseIdentifier: "cellId3")
        return cv
    }()
    
    lazy var doneViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8checkmarknew")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doneOnThisViewService)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        starView.translatesAutoresizingMaskIntoConstraints = false
        starView.isUserInteractionEnabled = false
        starView.delegate = self
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        
        if let select = self.collectionViewSelected {
            switch select {
            case 1:
                setupViewObjectContraintsService()
            case 2:
                setupViewObjectContraintsStaff()
            default:
                setupViewObjectContraintsAvailableTime()
                
            }
        }
        
    }
    
    @objc func exitViewAfterStaffChoice(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    private func exitThisView(appointmentData: Appointment){
        self.masterServiceListArray.removeAll()
        self.selectedServiceOne.removeAll()
        self.createAppointmentViewController.selectedStaff.removeAll()
        self.createAppointmentViewController.selectServiceButton.setTitle("Select a service - Step 2", for: .normal)
        self.createAppointmentViewController.selectStaffButton.setTitle("Select a staff - Step 1", for: .normal)
        self.createAppointmentViewController.numberOfCustomerTextField.text = ""
        self.createAppointmentViewController.appointmentPriceTextField.text = ""
        self.createAppointmentViewController.appointmentDateTextField.text = ""
        self.dismiss(animated: true) {
            self.createAppointmentViewController.showToastForSuccessfulAppointmentCreation(appointment: appointmentData)
        }
    }
    
    @objc func doneOnThisViewService(){
        self.createAppointmentViewController.selectService = self.masterServiceListArray
        self.createAppointmentViewController.textFieldDidChange()
        self.createAppointmentViewController.numberOfCustomerTextField.text = ""
        let sumString = String(self.masterServiceListArray.reduce(0) { $0 + $1.numberOfCustomer })
        var doubleCompleteServiceTime = [Double]()
        let count = self.masterServiceListArray.count
        var counterCheck = 0
        for serv in self.masterServiceListArray {
            counterCheck = counterCheck + 1
            if let serTime = Double(serv.serviceEstimatedTime) {
                let servNumPeople = Double(serv.numberOfCustomer)
                let total = serTime * servNumPeople
                doubleCompleteServiceTime.append(total)
            }
            
            if (counterCheck == count) {
                let sum = String(doubleCompleteServiceTime.reduce(0) { $0 + $1 })
                self.createAppointmentViewController.totalServiceEstimatedTime = sum
                
            }
            
        }
        self.createAppointmentViewController.selectServiceButton.setTitle("Service: " + sumString, for: .normal)
        self.createAppointmentViewController.selectedServicesID = self.selectedServiceOne
        self.selectedServiceOne.removeAll()
        self.masterServiceListArray.removeAll()
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    func StarRatingValueChanged(view: StarRatingView, value: CGFloat) {
        // use value
    }
    
    @objc func handleShowLadiesList(){
        self.selectedLadiesGentsKidsButton = "Ladies"
        self.ladiesButtonSeperatorView.backgroundColor = UIColor.black
        self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
        self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
        if self.serviceCategoryLadies.count > 0 {
            
            self.serviceOne.removeAll()
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            self.serviceOne.append(contentsOf: self.serviceCategoryLadies)
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        } else {
            self.serviceOne.removeAll()
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    @objc func handleShowGentsList(){
        self.selectedLadiesGentsKidsButton = "Gents"
        self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
        self.gentsButtonSeperatorView.backgroundColor = UIColor.black
        self.kidsButtonSeperatorView.backgroundColor = UIColor.clear
        if self.serviceCategoryGents.count > 0 {
            
            self.serviceOne.removeAll()
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            self.serviceOne.append(contentsOf: self.serviceCategoryGents)
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        } else {
            self.serviceOne.removeAll()
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
        
    }
    
    @objc func handleShowKidsList(){
        self.selectedLadiesGentsKidsButton = "Kids"
        self.ladiesButtonSeperatorView.backgroundColor = UIColor.clear
        self.gentsButtonSeperatorView.backgroundColor = UIColor.clear
        self.kidsButtonSeperatorView.backgroundColor = UIColor.black
        
        if self.serviceCategoryKids.count > 0 {
            self.serviceOne.removeAll()
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            self.serviceOne.append(contentsOf: self.serviceCategoryKids)
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
        } else {
            self.serviceOne.removeAll()
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
        
    }
    
    
    @objc func increaseCustomerPerService( _ sender: UIButton){
        let index = sender.tag
        print(index)
        guard let uniq = self.serviceOne[safe: index]?.serviceID else {
            return
        }
        
        var initialValue = self.serviceOne[index].numberOfCustomer
        let category = self.serviceOne[safe: index]?.category
        
        let arraySum = self.masterServiceListArray.reduce(0) { $0 + $1.numberOfCustomer }
        print("total sum: ", arraySum)
        
        if arraySum < 5 {
            initialValue = initialValue + 1
            self.serviceOne[index].numberOfCustomer = initialValue
            
            switch category {
            case "Gents":
                self.serviceCategoryGents[index].numberOfCustomer = initialValue
            case "Ladies":
                self.serviceCategoryLadies[index].numberOfCustomer = initialValue
            case "Kids":
                self.serviceCategoryKids[index].numberOfCustomer = initialValue
            default:
                print("sky high")
            }
            
            if self.selectedServiceOne.contains(self.serviceOne[index].serviceID) {
                print("No Show")
            }else {
                self.selectedServiceOne.append(self.serviceOne[index].serviceID)
            }
            
            if let _ = self.masterServiceListArray.first(where: { $0.serviceID == uniq }) {
                
                self.masterServiceListArray = self.masterServiceListArray.filter { $0.serviceID != uniq }
                self.masterServiceListArray.append(self.serviceOne[index])
                for x in self.selectedServiceOne {
                    print("id: ",x)
                }
                
                
            } else {
                
                self.masterServiceListArray.append(self.serviceOne[index])
                for x in self.selectedServiceOne {
                    print("id: ",x)
                }
            }
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
        
    }
    
    @objc func decreaseCustomerPerService( _ sender: UIButton){
        let index = sender.tag
        print(index)
        guard let uniq = self.serviceOne[safe: index]?.serviceID else {
            return
        }
        
        var initialValue = self.serviceOne[index].numberOfCustomer
        let category = self.serviceOne[index].category
        
        
        if (initialValue > 0) {
            
            initialValue = initialValue - 1
            self.serviceOne[index].numberOfCustomer = initialValue
            switch category {
            case "Gents":
                self.serviceCategoryGents[index].numberOfCustomer = initialValue
            case "Ladies":
                self.serviceCategoryLadies[index].numberOfCustomer = initialValue
            case "Kids":
                self.serviceCategoryKids[index].numberOfCustomer = initialValue
            default:
                print("sky high")
            }
            
            if let _ = self.masterServiceListArray.first(where: { $0.serviceID == uniq }) {
                if (initialValue == 0) {
                    self.masterServiceListArray = self.masterServiceListArray.filter { $0.serviceID != uniq }
                    if self.selectedServiceOne.contains(self.serviceOne[index].serviceID) {
                        self.selectedServiceOne = self.selectedServiceOne.filter { $0 != self.serviceOne[index].serviceID }
                    }
                    for x in self.selectedServiceOne {
                        print("id: ",x)
                    }
                } else {
                    self.masterServiceListArray = self.masterServiceListArray.filter { $0.serviceID != uniq }
                    self.masterServiceListArray.append(self.serviceOne[index])
                    for x in self.selectedServiceOne {
                        print("id: ",x)
                    }
                    
                }
            }
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    @objc func handleSelectedAvailableTime(_ sender: UIButton) {
        print("available time")
        print("index", sender.tag)
        guard let bookAvailDetail = self.appointmentAvailableTimeArray[safe: sender.tag] else {
            return
        }
        
        
        
        let alertController = UIAlertController(title: "Create an appointment", message: "Start: " + bookAvailDetail.dateObjectStart.toString(DateToStringStyles.time(.medium)) + " Finish: " +  bookAvailDetail.dateObjectEnd.toString(DateToStringStyles.time(.medium)), preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
            self.handleCreatingAppointment(appointStart: bookAvailDetail.startDate, appointEnd: bookAvailDetail.endDate, position: sender.tag)
        }
        
        let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in
            print("You've pressed cancel");
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func handleSelectedHolidayTime(_ sender: UIButton) {
        print("holiday time")
        guard let bookAvailDetail = self.appointmentAvailableTimeArray[safe: sender.tag] else {
            return
        }
        
        var designation = "shop"
        
        if bookAvailDetail.holidayDesignation == "staff" {
            designation = "staff"
        }
        
        
        
        let alertController = UIAlertController(title: "Holiday for " + designation, message: "Start: " + bookAvailDetail.dateObjectStart.toString(DateToStringStyles.time(.medium)) + " Finish: " +  bookAvailDetail.dateObjectEnd.toString(DateToStringStyles.time(.medium)) + " " + bookAvailDetail.holidayDescription , preferredStyle: .alert)
        
        let action2 = UIAlertAction(title: "OK", style: .cancel) { (action:UIAlertAction) in
            print("You've pressed cancel");
        }
        
        alertController.addAction(action2)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @objc func handleGettingAllShopService(){
        if let UniqueShopID = self.shopID, let staffID = self.selectedBarberOne {
            
            DispatchQueue.global(qos: .background).async {
                if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                    let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                    if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token {
                        let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                        let dateNow = DateInRegion()
                        
                        if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                            DispatchQueue.main.async {
                                UtilityManager.alertView(view: self)
                            }
                        } else {
                            DispatchQueue.main.async {
                                SwiftSpinner.show("Loading...")
                            }
                            let headers: HTTPHeaders = [
                                "authorization": token
                            ]
                            
                            let parameters = [
                                "staffId": staffID
                                ] as [String: Any]
                            
                            AF.request(self.BACKEND_URL + "getshopservicebystaffidavailable/" + UniqueShopID , method: .post, parameters: parameters, headers: headers)
                                .validate(statusCode: 200..<500)
                                .validate(contentType: ["application/json"]).response { (response) in
                                    if let error = response.error {
                                        print("zones", error.localizedDescription)
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        return
                                    }
                                    if let jsonData = response.data {
                                        do {
                                            DispatchQueue.main.async {
                                                SwiftSpinner.hide()
                                            }
                                            let shopServiceArrayData = try JSONDecoder().decode([ShopService].self, from: jsonData)
                                            for shopService in shopServiceArrayData {
                                                
                                                if shopService.serviceHiddenByAdmin == "NO" && shopService.serviceHiddenByShop == "NO" {
                                                    let serviceImageName = shopService.serviceImageName
                                                    
                                                    AF.request(self.BACKEND_URL + "images/shopServiceImages/" + serviceImageName, headers: headers).responseData { response in
                                                        
                                                        switch response.result {
                                                        case .success(let data):
                                                            if let image = UIImage(data: data)  {
                                                                switch shopService.serviceCategory {
                                                                case "Gents":
                                                                    let serviceDataGents = ServiceData(serviceID: shopService.id, serviceTitle: shopService.serviceTitle, serviceEstimatedTime: shopService.serviceEstimatedTotalTime, serviceCost: shopService.servicePrice, shortDescription: shopService.serviceDescription, category: shopService.serviceCategory, dateCreated: shopService.serviceDateCreated, timezone: shopService.serviceDateCreatedTimezone, calendar: shopService.serviceDateCreatedCalendar, local: shopService.serviceDateCreatedLocale, serviceImage: image, isHiddenByShop: shopService.serviceHiddenByShop, isHiddenByAdmin: shopService.serviceHiddenByAdmin, numberOfCustomer: 0, serviceStaff: shopService.serviceStaff, isAssigned: "NO", shop: shopService.serviceShop)
                                                                    self.serviceCategoryGents.append(serviceDataGents)
                                                                    self.serviceOne.append(serviceDataGents)
                                                                    self.serviceOne.sort { (first, second) -> Bool in
                                                                        first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                    }
                                                                    self.serviceCategoryGents.sort { (first, second) -> Bool in
                                                                        first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                    }
                                                                    self.gentsButton.setTitle("GENTS" + "(\(self.serviceCategoryGents.count))", for: .normal)
                                                                    self.collectionView.reloadData()
                                                                case "Ladies":
                                                                    let serviceDataLadies = ServiceData(serviceID: shopService.id, serviceTitle: shopService.serviceTitle, serviceEstimatedTime: shopService.serviceEstimatedTotalTime, serviceCost: shopService.servicePrice, shortDescription: shopService.serviceDescription, category: shopService.serviceCategory, dateCreated: shopService.serviceDateCreated, timezone: shopService.serviceDateCreatedTimezone, calendar: shopService.serviceDateCreatedCalendar, local: shopService.serviceDateCreatedLocale, serviceImage: image, isHiddenByShop: shopService.serviceHiddenByShop, isHiddenByAdmin: shopService.serviceHiddenByAdmin, numberOfCustomer: 0, serviceStaff: shopService.serviceStaff, isAssigned: "NO", shop: shopService.serviceShop)
                                                                    self.serviceCategoryLadies.append(serviceDataLadies)
                                                                    self.serviceCategoryLadies.sort { (first, second) -> Bool in
                                                                        first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                    }
                                                                    self.ladiesButton.setTitle("LADIES" + "(\(self.serviceCategoryLadies.count))", for: .normal)
                                                                case "Kids":
                                                                    let serviceDataKids = ServiceData(serviceID: shopService.id, serviceTitle: shopService.serviceTitle, serviceEstimatedTime: shopService.serviceEstimatedTotalTime, serviceCost: shopService.servicePrice, shortDescription: shopService.serviceDescription, category: shopService.serviceCategory, dateCreated: shopService.serviceDateCreated, timezone: shopService.serviceDateCreatedTimezone, calendar: shopService.serviceDateCreatedCalendar, local: shopService.serviceDateCreatedLocale, serviceImage: image, isHiddenByShop: shopService.serviceHiddenByShop, isHiddenByAdmin: shopService.serviceHiddenByAdmin, numberOfCustomer: 0, serviceStaff: shopService.serviceStaff, isAssigned: "NO", shop: shopService.serviceShop)
                                                                    self.serviceCategoryKids.append(serviceDataKids)
                                                                    self.serviceCategoryKids.sort { (first, second) -> Bool in
                                                                        first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                    }
                                                                    self.kidsButton.setTitle("KIDS" + "(\(self.serviceCategoryKids.count))", for: .normal)
                                                                default:
                                                                    print("Sky high")
                                                                }
                                                            } else {
                                                                if let image = UIImage(named: "avatar_icon") {
                                                                    switch shopService.serviceCategory {
                                                                    case "Gents":
                                                                        let serviceDataGents = ServiceData(serviceID: shopService.id, serviceTitle: shopService.serviceTitle, serviceEstimatedTime: shopService.serviceEstimatedTotalTime, serviceCost: shopService.servicePrice, shortDescription: shopService.serviceDescription, category: shopService.serviceCategory, dateCreated: shopService.serviceDateCreated, timezone: shopService.serviceDateCreatedTimezone, calendar: shopService.serviceDateCreatedCalendar, local: shopService.serviceDateCreatedLocale, serviceImage: image, isHiddenByShop: shopService.serviceHiddenByShop, isHiddenByAdmin: shopService.serviceHiddenByAdmin, numberOfCustomer: 0, serviceStaff: shopService.serviceStaff, isAssigned: "NO", shop: shopService.serviceShop)
                                                                        self.serviceCategoryGents.append(serviceDataGents)
                                                                        self.serviceOne.append(serviceDataGents)
                                                                        self.serviceOne.sort { (first, second) -> Bool in
                                                                            first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                        }
                                                                        self.serviceCategoryGents.sort { (first, second) -> Bool in
                                                                            first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                        }
                                                                        self.gentsButton.setTitle("GENTS" + "(\(self.serviceCategoryGents.count))", for: .normal)
                                                                        self.collectionView.reloadData()
                                                                    case "Ladies":
                                                                        let serviceDataLadies = ServiceData(serviceID: shopService.id, serviceTitle: shopService.serviceTitle, serviceEstimatedTime: shopService.serviceEstimatedTotalTime, serviceCost: shopService.servicePrice, shortDescription: shopService.serviceDescription, category: shopService.serviceCategory, dateCreated: shopService.serviceDateCreated, timezone: shopService.serviceDateCreatedTimezone, calendar: shopService.serviceDateCreatedCalendar, local: shopService.serviceDateCreatedLocale, serviceImage: image, isHiddenByShop: shopService.serviceHiddenByShop, isHiddenByAdmin: shopService.serviceHiddenByAdmin, numberOfCustomer: 0, serviceStaff: shopService.serviceStaff, isAssigned: "NO", shop: shopService.serviceShop)
                                                                        self.serviceCategoryLadies.append(serviceDataLadies)
                                                                        self.serviceCategoryLadies.sort { (first, second) -> Bool in
                                                                            first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                        }
                                                                        self.ladiesButton.setTitle("LADIES" + "(\(self.serviceCategoryLadies.count))", for: .normal)
                                                                    case "Kids":
                                                                        let serviceDataKids = ServiceData(serviceID: shopService.id, serviceTitle: shopService.serviceTitle, serviceEstimatedTime: shopService.serviceEstimatedTotalTime, serviceCost: shopService.servicePrice, shortDescription: shopService.serviceDescription, category: shopService.serviceCategory, dateCreated: shopService.serviceDateCreated, timezone: shopService.serviceDateCreatedTimezone, calendar: shopService.serviceDateCreatedCalendar, local: shopService.serviceDateCreatedLocale, serviceImage: image, isHiddenByShop: shopService.serviceHiddenByShop, isHiddenByAdmin: shopService.serviceHiddenByAdmin, numberOfCustomer: 0, serviceStaff: shopService.serviceStaff, isAssigned: "NO", shop: shopService.serviceShop)
                                                                        self.serviceCategoryKids.append(serviceDataKids)
                                                                        self.serviceCategoryKids.sort { (first, second) -> Bool in
                                                                            first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                        }
                                                                        self.kidsButton.setTitle("KIDS" + "(\(self.serviceCategoryKids.count))", for: .normal)
                                                                    default:
                                                                        print("Sky high")
                                                                    }
                                                                }
                                                            }
                                                            break
                                                        case .failure(let error):
                                                            print(error)
                                                            self.view.makeToast(NSLocalizedString("popOverViewControllerErrorMesageImageNotFound", comment: "Image not found"), duration: 3.0, position: .bottom)
                                                            if let image = UIImage(named: "avatar_icon") {
                                                                switch shopService.serviceCategory {
                                                                case "Gents":
                                                                    let serviceDataGents = ServiceData(serviceID: shopService.id, serviceTitle: shopService.serviceTitle, serviceEstimatedTime: shopService.serviceEstimatedTotalTime, serviceCost: shopService.servicePrice, shortDescription: shopService.serviceDescription, category: shopService.serviceCategory, dateCreated: shopService.serviceDateCreated, timezone: shopService.serviceDateCreatedTimezone, calendar: shopService.serviceDateCreatedCalendar, local: shopService.serviceDateCreatedLocale, serviceImage: image, isHiddenByShop: shopService.serviceHiddenByShop, isHiddenByAdmin: shopService.serviceHiddenByAdmin, numberOfCustomer: 0, serviceStaff: shopService.serviceStaff, isAssigned: "NO", shop: shopService.serviceShop)
                                                                    self.serviceCategoryGents.append(serviceDataGents)
                                                                    self.serviceOne.append(serviceDataGents)
                                                                    self.serviceOne.sort { (first, second) -> Bool in
                                                                        first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                    }
                                                                    self.serviceCategoryGents.sort { (first, second) -> Bool in
                                                                        first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                    }
                                                                    self.gentsButton.setTitle("GENTS" + "(\(self.serviceCategoryGents.count))", for: .normal)
                                                                    self.collectionView.reloadData()
                                                                case "Ladies":
                                                                    let serviceDataLadies = ServiceData(serviceID: shopService.id, serviceTitle: shopService.serviceTitle, serviceEstimatedTime: shopService.serviceEstimatedTotalTime, serviceCost: shopService.servicePrice, shortDescription: shopService.serviceDescription, category: shopService.serviceCategory, dateCreated: shopService.serviceDateCreated, timezone: shopService.serviceDateCreatedTimezone, calendar: shopService.serviceDateCreatedCalendar, local: shopService.serviceDateCreatedLocale, serviceImage: image, isHiddenByShop: shopService.serviceHiddenByShop, isHiddenByAdmin: shopService.serviceHiddenByAdmin, numberOfCustomer: 0, serviceStaff: shopService.serviceStaff, isAssigned: "NO", shop: shopService.serviceShop)
                                                                    self.serviceCategoryLadies.append(serviceDataLadies)
                                                                    self.serviceCategoryLadies.sort { (first, second) -> Bool in
                                                                        first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                    }
                                                                    self.ladiesButton.setTitle("LADIES" + "(\(self.serviceCategoryLadies.count))", for: .normal)
                                                                case "Kids":
                                                                    let serviceDataKids = ServiceData(serviceID: shopService.id, serviceTitle: shopService.serviceTitle, serviceEstimatedTime: shopService.serviceEstimatedTotalTime, serviceCost: shopService.servicePrice, shortDescription: shopService.serviceDescription, category: shopService.serviceCategory, dateCreated: shopService.serviceDateCreated, timezone: shopService.serviceDateCreatedTimezone, calendar: shopService.serviceDateCreatedCalendar, local: shopService.serviceDateCreatedLocale, serviceImage: image, isHiddenByShop: shopService.serviceHiddenByShop, isHiddenByAdmin: shopService.serviceHiddenByAdmin, numberOfCustomer: 0, serviceStaff: shopService.serviceStaff, isAssigned: "NO", shop: shopService.serviceShop)
                                                                    self.serviceCategoryKids.append(serviceDataKids)
                                                                    self.serviceCategoryKids.sort { (first, second) -> Bool in
                                                                        first.serviceTitle.lowercased() < second.serviceTitle.lowercased()
                                                                    }
                                                                    self.kidsButton.setTitle("KIDS" + "(\(self.serviceCategoryKids.count))", for: .normal)
                                                                default:
                                                                    print("Sky high")
                                                                }
                                                            }
                                                            break
                                                        }
                                                    }
                                                }
                                            }
                                            
                                        } catch _ {
                                            DispatchQueue.main.async {
                                                SwiftSpinner.hide()
                                            }
                                            if let statusCode = response.response?.statusCode {
                                                switch statusCode {
                                                case 404:
                                                    self.view.makeToast(NSLocalizedString("popOverViewControllerErrorMesageNoService", comment: "Shop does not have a service"), duration: 2.0, position: .center)
                                                case 500:
                                                    self.view.makeToast(NSLocalizedString("popOverViewControllerErrorMesageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .center)
                                                default:
                                                    print("sky high")
                                                }
                                            }
                                            
                                        }
                                    }
                            }
                        }
                    }
                }
                
                
            }
        }
    }
    
    
    @objc func handleGettingShopStaffs(){
        if let UniqueShopID = self.shopID {
            DispatchQueue.global(qos: .background).async {
                if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                    let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                    if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token {
                        let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                        let dateNow = DateInRegion()
                        if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                            DispatchQueue.main.async {
                                UtilityManager.alertView(view: self)
                            }
                        } else {
                            DispatchQueue.main.async {
                                SwiftSpinner.show("Loading...")
                            }
                            let headers: HTTPHeaders = [
                                "authorization": token
                            ]
                            print("before request")
                            AF.request(self.BACKEND_URL + "getshopstaffAvailable/" + UniqueShopID , method: .post, headers: headers)
                                .validate(statusCode: 200..<500)
                                .validate(contentType: ["application/json"]).response { (response) in
                                    if let error = response.error {
                                        print(error.localizedDescription)
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        return
                                    }
                                    
                                    if let jsonData = response.data {
                                        do {
                                            DispatchQueue.main.async {
                                                SwiftSpinner.hide()
                                            }
                                            let staffVustomerArrayData = try JSONDecoder().decode(StaffCustomerList.self, from: jsonData)
                                            
                                            for shopStaff in staffVustomerArrayData.staff {
                                                
                                                if(shopStaff.staffHiddenShop == "NO" && shopStaff.staffHiddenByAdmin == "NO") {
                                                    
                                                    if let customer = staffVustomerArrayData.customer.first(where: {$0.id == shopStaff.customer }) {
                                                        let profileImageName = customer.profileImageName
                                                        var ratingIneter = 0
                                                        if let val = Int(customer.customerStaffRating) {
                                                            ratingIneter = val
                                                        }
                                                        
                                                        AF.request(self.BACKEND_URL + "images/customerImages/" + profileImageName, headers: headers).responseData { response in
                                                            
                                                            switch response.result {
                                                            case .success(let data):
                                                                if let image = UIImage(data: data)  {
                                                                    let staffData = StaffData(staffID: shopStaff.id, shopID: shopStaff.shop, email: customer.email, firstName: customer.firstName, lastName: customer.lastName, profileImage: image, rating: ratingIneter, staffHiddenShop: shopStaff.staffHiddenShop, staffHiddenByAdmin: shopStaff.staffHiddenByAdmin, dateAccountCreated: shopStaff.dateCreated, timezone: shopStaff.dateCreatedTimezone, calendar: shopStaff.dateCreatedCalendar, local: shopStaff.dateCreatedLocale, serviceStaff: shopStaff.staffService, staffCustomerId: shopStaff.customer, staffCustomerShortId: customer.shortUniqueID)
                                                                    self.barberlistone.append(staffData)
                                                                    self.barberlistone.sort { (first, second) -> Bool in
                                                                        first.firstName.lowercased() < second.firstName.lowercased()
                                                                    }
                                                                    DispatchQueue.main.async {
                                                                        self.collectionView.reloadData()
                                                                    }
                                                                } else {
                                                                    if let image = UIImage(named: "avatar_icon") {
                                                                        let staffData = StaffData(staffID: shopStaff.id, shopID: shopStaff.shop, email: customer.email, firstName: customer.firstName, lastName: customer.lastName, profileImage: image, rating: ratingIneter, staffHiddenShop: shopStaff.staffHiddenShop, staffHiddenByAdmin: shopStaff.staffHiddenByAdmin, dateAccountCreated: shopStaff.dateCreated, timezone: shopStaff.dateCreatedTimezone, calendar: shopStaff.dateCreatedCalendar, local: shopStaff.dateCreatedLocale, serviceStaff: shopStaff.staffService, staffCustomerId: shopStaff.customer, staffCustomerShortId: customer.shortUniqueID)
                                                                        self.barberlistone.append(staffData)
                                                                        self.barberlistone.sort { (first, second) -> Bool in
                                                                            first.firstName.lowercased() < second.firstName.lowercased()
                                                                        }
                                                                        DispatchQueue.main.async {
                                                                            self.collectionView.reloadData()
                                                                        }
                                                                    }
                                                                }
                                                                break
                                                            case .failure(let error):
                                                                print(error)
                                                                self.view.makeToast("Image not found", duration: 3.0, position: .bottom)
                                                                if let image = UIImage(named: "avatar_icon") {
                                                                    let staffData = StaffData(staffID: shopStaff.id, shopID: shopStaff.shop, email: customer.email, firstName: customer.firstName, lastName: customer.lastName, profileImage: image, rating: ratingIneter, staffHiddenShop: shopStaff.staffHiddenShop, staffHiddenByAdmin: shopStaff.staffHiddenByAdmin, dateAccountCreated: shopStaff.dateCreated, timezone: shopStaff.dateCreatedTimezone, calendar: shopStaff.dateCreatedCalendar, local: shopStaff.dateCreatedLocale, serviceStaff: shopStaff.staffService, staffCustomerId: shopStaff.customer, staffCustomerShortId: customer.shortUniqueID)
                                                                    self.barberlistone.append(staffData)
                                                                    self.barberlistone.sort { (first, second) -> Bool in
                                                                        first.firstName.lowercased() < second.firstName.lowercased()
                                                                    }
                                                                    DispatchQueue.main.async {
                                                                        self.collectionView.reloadData()
                                                                    }
                                                                }
                                                                break
                                                            }
                                                            
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            
                                        } catch _ {
                                            
                                            DispatchQueue.main.async {
                                                SwiftSpinner.hide()
                                            }
                                            
                                            if let statusCode = response.response?.statusCode {
                                                switch statusCode {
                                                case 404:
                                                    self.view.makeToast(NSLocalizedString("popOverViewControllerErrorMesageNoStaff", comment: "Shop does not have a staff"), duration: 2.0, position: .center)
                                                case 500:
                                                    self.view.makeToast(NSLocalizedString("popOverViewControllerErrorMesageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .center)
                                                default:
                                                    print("sky high")
                                                }
                                            }
                                            
                                        }
                                    }
                            }
                        }
                    }
                }
                
                
            }
        }
    }
    
    @objc func handleCreatingAppointment(appointStart: String, appointEnd: String, position: Int){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let customerId = decodedCustomer.first?.customerId, !self.shopServiceArrayParameter.isEmpty {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    
                    if let shop = self.shopID,
                        let currency = self.shopCurrency,
                        let shpTzone = self.shopTimeZone,
                        let shpCal = self.shopCalendar,
                        let shpLoc = self.shopLocal,
                        let staffId = self.staffID,
                        let numberPeople = self.numberOfCustomer,
                        let totalPriceOfService = self.totalPrice,
                        let staffCustomerUniqueId = self.staffCustomerId,
                        !self.servicesSelectedByUser.isEmpty {
                        print("no optionals failed")
                        SwiftSpinner.show("Loading...")
                        DispatchQueue.global(qos: .background).async {
                            
                            let regionData = DateByUserDeviceInitializer.getRegion(TZoneName: shpTzone, calenName: shpCal, LocName: shpLoc)
                            let newDate = DateInRegion().convertTo(region: regionData).toFormat("yyyy-MM-dd HH:mm:ss")
                            
                            let headers: HTTPHeaders = [
                                "authorization": token
                            ]
                            
                            let parameters: Parameters = [
                                "appointmentDateStart": appointStart,
                                "appointmentDateEnd": appointEnd,
                                "appointmentDateTimezone": shpTzone,
                                "appointmentDateCalendar": shpCal,
                                "appointmentDateLocale": shpLoc,
                                "appointmentDateCreated": newDate,
                                "appointmentDescription": "not available",
                                "appointmentPrice": totalPriceOfService,
                                "appointmentCurrency": currency,
                                "appointmentCustomer": customerId,
                                "appointmentService": self.servicesSelectedByUser,
                                "appointmentStaff": staffId,
                                "appointmentStaffCustomer": staffCustomerUniqueId,
                                "appointmentShop": shop,
                                "appointmentServiceQuantity": numberPeople,
                                "appointmentServiceWithAmount": self.shopServiceArrayParameter
                            ]
                            
                            AF.request(self.BACKEND_URL + "createAppointment", method: .post, parameters: parameters, encoding: JSONEncoding(options: []), headers: headers)
                                .validate(statusCode: 200..<501)
                                .validate(contentType: ["application/json"])
                                .responseJSON (completionHandler: { (response) in
                                    
                                    
                                    if let error = response.error {
                                        print(error.localizedDescription)
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        return
                                    }
                                    
                                    if let jsonData = response.data {
                                        do {
                                            DispatchQueue.main.async {
                                                SwiftSpinner.hide()
                                            }
                                            
                                            let serverResponse = try JSONDecoder().decode(AppointmentCreated.self, from: jsonData)
                                            let _ = serverResponse.message
                                            if let cusID = self.shopOwnerCustomerId {
                                                self.handleSendNotificationAppointment(customerid: cusID, message: "just booked an appointment with your shop", designation: "createappointment", appointmentID: serverResponse.appointment.id)
                                                self.handleSendNotificationAppointment(customerid: staffCustomerUniqueId, message: "you were just booked for an appointment", designation: "createappointment", appointmentID: serverResponse.appointment.id)
                                            }
                                            self.exitThisView(appointmentData: serverResponse.appointment)
                                            
                                        } catch _ {
                                            
                                            DispatchQueue.main.async {
                                                SwiftSpinner.hide()
                                            }
                                            
                                            if let statusCode = response.response?.statusCode {
                                                switch statusCode {
                                                case 204:
                                                    self.view.makeToast(NSLocalizedString("popOverViewControllerErrorMesageErrorOverlapping", comment: "Appointment overlaped please select another time"), duration: 3.0, position: .bottom)
                                                    //self.handleGetAvailableTime()
                                                    let indexPath = IndexPath(row: position, section: 0)
                                                    DispatchQueue.main.async {
                                                        self.collectionView.deleteItems(at: [indexPath])
                                                    }
                                                    self.appointmentAvailableTimeArray.remove(at: position)
                                                case 404:
                                                    self.view.makeToast(NSLocalizedString("popOverViewControllerErrorMesageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                                    // "Appointment verify failed, please try again"
                                                case 409:
                                                    self.view.makeToast(NSLocalizedString("popOverViewControllerErrorMesageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                                    // "Appointment unique identifier, issue please try again"
                                                case 500:
                                                    self.view.makeToast(NSLocalizedString("popOverViewControllerErrorMesageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                                default:
                                                    print("sky high")
                                                }
                                            }
                                        }
                                    }
                                })
                        }
                    }
                }
            }
        }
    }
    
    @objc private func handleSendNotificationAppointment(customerid: String, message: String, designation: String, appointmentID: String){
        
        DispatchQueue.global(qos: .background).async {
            
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        let parameters: Parameters = [
                            "customerid": customerid,
                            "designation": designation,
                            "appointmentID": appointmentID,
                            "message": message
                        ]
                        
                        print("function called and iptionals handled")
                        
                        AF.request(self.BACKEND_URL + "onetoonepushnotification/" + customerid, method: .post, parameters: parameters, encoding: JSONEncoding(options: []), headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    if let statusCode = response.response?.statusCode {
                                        
                                        do {
                                            
                                            print("gotten response")
                                            
                                            let notificationData = try JSONDecoder().decode(ServerMessage.self, from: jsonData)
                                            switch statusCode {
                                            case 200:
                                                print(notificationData.message)
                                            case 404:
                                                print(notificationData.message)
                                            case 500:
                                                print(notificationData.message)
                                            default:
                                                print("sky high")
                                            }
                                            
                                        } catch _ {
                                            switch statusCode {
                                            case 404:
                                                print("notification not sent")
                                            case 500:
                                                print("An error occurred, please try again later")
                                            default:
                                                print("sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
        
    }
    
    @objc func handleGetAvailableTime(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let shpTzone = self.shopTimeZone, let shpCal = self.shopCalendar, let staffIDDD = self.staffCustomerId {
                
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let shopRegion = DateByUserDeviceInitializer.getRegion(TZoneName: shpTzone, calenName: shpCal, LocName: "en")
                let englishShopRegion = DateByUserDeviceInitializer.getRegion(TZoneName: shpTzone, calenName: shpCal, LocName: "en_US")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    if let shop = self.shopID,
                        let dateChose = self.dateChoseByUser, let totalServiceTime = self.serviceTotalTimeTaken, let dateObj = dateChose.toDate("yyyy-MM-dd HH:mm:ss", region: shopRegion) {
                        let newTimeChosen = DateInRegion(year: dateObj.year, month: dateObj.month, day: dateObj.day, hour: dateObj.hour, minute: dateObj.minute, second: dateObj.second, nanosecond: dateObj.nanosecond, region: shopRegion)
                        
                        
                        let currentime = newTimeChosen.toFormat("yyyy-MM-dd HH:mm:ss")
                        let dayOfTheWeek = dateObj.convertTo(region: englishShopRegion)
                        let stringDay = dayOfTheWeek.weekdayName(.default)
                        print("day string: ", stringDay, currentime, totalServiceTime)
                        
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        let parameters: Parameters = [
                            "dayOfTheWeek": stringDay,
                            "dateSelected": currentime,
                            "serviceEstimatedTime": totalServiceTime,
                            "staffId": staffIDDD
                        ]
                        
                        self.appointmentAvailableTimeArray.removeAll()
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                        }
                        
                        SwiftSpinner.show("Loading...")
                        DispatchQueue.global(qos: .background).async {
                            AF.request(self.BACKEND_URL + "availableappointmenttime/" + shop, method: .post, parameters: parameters, encoding: JSONEncoding(options: []), headers: headers)
                                .validate(statusCode: 200..<501)
                                .validate(contentType: ["application/json"])
                                .responseJSON(completionHandler: { (response) in
                                    
                                    if let error = response.error {
                                        print(error.localizedDescription)
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        return
                                    }
                                    
                                    if let jsonData = response.data {
                                        do {
                                            DispatchQueue.main.async {
                                                SwiftSpinner.hide()
                                            }
                                            
                                            let serverResponse = try JSONDecoder().decode(AvailableTimeCreated.self, from: jsonData)
                                            let avail = serverResponse.availableTime
                                            let shopH = serverResponse.holidayShop
                                            let staffH = serverResponse.holidayStaff
                                            
                                            for b in shopH {
                                                let regionData = DateByUserDeviceInitializer.getRegion(TZoneName: b.holidayShopDateTimezone, calenName: b.holidayShopDateCalendar, LocName: b.holidayShopDateLocale)
                                                let currentTime = DateInRegion().convertTo(region: regionData)
                                                if let dtaObjSt = b.holidayShopStartDate.toDate("yyyy-MM-dd HH:mm:ss", region: regionData), let dtaObjEn = b.holidayShopEndDate.toDate("yyyy-MM-dd HH:mm:ss", region: regionData) {
                                                    
                                                    if dtaObjSt.isAfterDate(currentTime, orEqual: true, granularity: .minute) {
                                                        let appData = AppointmentAvailableTime(uniqueID: b.id, startDate: b.holidayShopStartDate, endDate: b.holidayShopEndDate, timezone: b.holidayShopDateTimezone, calendar: b.holidayShopDateCalendar, locale: b.holidayShopDateLocale, dateObjectStart: dtaObjSt, dateObjectEnd: dtaObjEn, holiday: "YES", holidayDesignation: "shop", holidayTitle: b.holidayShopTitle, holidayDescription: b.holidayShopDescription)
                                                        self.appointmentAvailableTimeArray.append(appData)
                                                        
                                                        self.appointmentAvailableTimeArray.sort(by: { (appstx, appsty) -> Bool in
                                                            return appstx.dateObjectStart < appsty.dateObjectStart
                                                        })
                                                        
                                                        DispatchQueue.main.async {
                                                            self.collectionView.reloadData()
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            for c in staffH {
                                                let regionData = DateByUserDeviceInitializer.getRegion(TZoneName: c.holidayStaffDateTimezone, calenName: c.holidayStaffDateCalendar, LocName: c.holidayStaffDateLocale)
                                                let currentTime = DateInRegion().convertTo(region: regionData)
                                                if let dtaObjSt = c.holidayStaffStartDate.toDate("yyyy-MM-dd HH:mm:ss", region: regionData), let dtaObjEn = c.holidayStaffEndDate.toDate("yyyy-MM-dd HH:mm:ss", region: regionData) {
                                                    
                                                    if dtaObjSt.isAfterDate(currentTime, orEqual: true, granularity: .minute) {
                                                        let appData = AppointmentAvailableTime(uniqueID: c.id, startDate: c.holidayStaffStartDate, endDate: c.holidayStaffEndDate, timezone: c.holidayStaffDateTimezone, calendar: c.holidayStaffDateCalendar, locale: c.holidayStaffDateLocale, dateObjectStart: dtaObjSt, dateObjectEnd: dtaObjEn, holiday: "YES", holidayDesignation: "staff", holidayTitle: c.holidayStaffTitle, holidayDescription: c.holidayStaffDescription)
                                                        self.appointmentAvailableTimeArray.append(appData)
                                                        
                                                        self.appointmentAvailableTimeArray.sort(by: { (appstx, appsty) -> Bool in
                                                            return appstx.dateObjectStart < appsty.dateObjectStart
                                                        })
                                                        
                                                        DispatchQueue.main.async {
                                                            self.collectionView.reloadData()
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            if (avail.count > 0) {
                                                var initcounter = 0
                                                for av in avail {
                                                    initcounter += 1
                                                    let regionData = DateByUserDeviceInitializer.getRegion(TZoneName: av.timezone, calenName: av.calendar, LocName: av.locale)
                                                    let currentTime = DateInRegion().convertTo(region: regionData)
                                                    if let dtaObjSt = av.startDate.toDate("yyyy-MM-dd HH:mm:ss", region: regionData), let dtaObjEn = av.endDate.toDate("yyyy-MM-dd HH:mm:ss", region: regionData) {
                                                        if (dtaObjSt.isAfterDate(currentTime, orEqual: true, granularity: .minute)) {
                                                            
                                                            let uqId = UUID().uuidString
                                                            let appData = AppointmentAvailableTime(uniqueID: uqId, startDate: av.startDate, endDate: av.endDate, timezone: av.timezone, calendar: av.calendar, locale: av.locale, dateObjectStart: dtaObjSt, dateObjectEnd: dtaObjEn, holiday: "NO", holidayDesignation: "customer", holidayTitle: "customer", holidayDescription: "customer")
                                                            self.appointmentAvailableTimeArray.append(appData)
                                                            
                                                            self.appointmentAvailableTimeArray.sort(by: { (appstx, appsty) -> Bool in
                                                                return appstx.dateObjectStart < appsty.dateObjectStart
                                                            })
                                                            
                                                            DispatchQueue.main.async {
                                                                self.collectionView.reloadData()
                                                            }
                                                            
                                                        }
                                                    }
                                                    if (avail.count == initcounter) {
                                                        if (self.appointmentAvailableTimeArray.count < 1) {
                                                            self.view.makeToast(NSLocalizedString("popOverViewControllerErrorMesageErrorNoAvailableTime", comment: "The store is closed at this time. Please select another date."), duration: 2.0, position: .bottom)
                                                        }
                                                    }
                                                }
                                            } else {
                                                self.view.makeToast(NSLocalizedString("popOverViewControllerErrorMesageErrorNoAvailableTime", comment: "The store is closed at this time. Please select another date."), duration: 2.0, position: .bottom)
                                            }
                                            
                                            
                                        } catch _ {
                                            DispatchQueue.main.async {
                                                SwiftSpinner.hide()
                                            }
                                            if let statusCode = response.response?.statusCode {
                                                print(statusCode)
                                                switch statusCode {
                                                case 404:
                                                    self.view.makeToast(NSLocalizedString("popOverViewControllerErrorMesageErrorReduceTimeTaken", comment: "All available time have been taken either reduce the amount of services or choose a different date") , duration: 4.0, position: .bottom)
                                                case 500:
                                                    self.view.makeToast(NSLocalizedString("popOverViewControllerErrorMesageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                                default:
                                                    print("sky high")
                                                }
                                            }
                                        }
                                    }
                                })
                        }
                        
                        
                    }
                    
                }
            }
        }
        
    }
    
    @objc func handleUpdateUserAuthData(){
        DispatchQueue.main.async {
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let customerId = decodedCustomer.first?.customerId, let shopOwner = decodedCustomer.first?.shopOwner, let timeZone = decodedCustomer.first?.timezone, let authType = decodedCustomer.first?.authType, let email = decodedCustomer.first?.email, let tokenData = decodedCustomer.first?.token, let autoSwitch = decodedCustomer.first?.autoShopSwitch {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let expiredTime = DateInRegion().convertTo(region: region) - 1.seconds
                    let expiredTimeString = expiredTime.toFormat("yyyy-MM-dd HH:mm:ss")
                    
                    let teams = [CustomerToken(expiresIn: expiredTimeString, customerId: customerId, token: tokenData, timezone: timeZone, shopOwner: shopOwner, authType: authType, email: email, autoShopSwitch: autoSwitch)]
                    
                    let userDefaults = UserDefaults.standard
                    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: teams)
                    userDefaults.set(encodedData, forKey: "token")
                    userDefaults.synchronize()
                    LoginManager().logOut()
                    let welcomeviewcontroller = WelcomeViewController()
                    self.present(welcomeviewcontroller, animated: true, completion: nil)
                    
                }
                
            }
        }
    }
    
    @objc func setupViewObjectContraintsService(){
        
        
        collectView.topAnchor.constraint(equalTo: view.topAnchor, constant: 70).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -70).isActive = true
        
        collectView.addSubview(closeViewButton)
        collectView.addSubview(categoryButtonContainerView)
        collectView.addSubview(doneViewButton)
        collectView.addSubview(collectionView)
        
        closeViewButton.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        closeViewButton.rightAnchor.constraint(equalTo: collectView.rightAnchor).isActive = true
        closeViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        closeViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        
        categoryButtonContainerView.topAnchor.constraint(equalTo: closeViewButton.bottomAnchor, constant: 20).isActive = true
        categoryButtonContainerView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        categoryButtonContainerView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        categoryButtonContainerView.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        categoryButtonContainerView.addSubview(ladiesButton)
        categoryButtonContainerView.addSubview(ladiesButtonSeperatorView)
        categoryButtonContainerView.addSubview(gentsButton)
        categoryButtonContainerView.addSubview(gentsButtonSeperatorView)
        categoryButtonContainerView.addSubview(kidsButton)
        categoryButtonContainerView.addSubview(kidsButtonSeperatorView)
        
        ladiesButton.topAnchor.constraint(equalTo: categoryButtonContainerView.topAnchor).isActive = true
        ladiesButton.leftAnchor.constraint(equalTo: categoryButtonContainerView.leftAnchor).isActive = true
        ladiesButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        ladiesButton.heightAnchor.constraint(equalTo: categoryButtonContainerView.heightAnchor, constant: -2).isActive = true
        
        ladiesButtonSeperatorView.topAnchor.constraint(equalTo: ladiesButton.bottomAnchor).isActive = true
        ladiesButtonSeperatorView.leftAnchor.constraint(equalTo: categoryButtonContainerView.leftAnchor).isActive = true
        ladiesButtonSeperatorView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        ladiesButtonSeperatorView.heightAnchor.constraint(equalToConstant: 5).isActive = true
        
        kidsButton.topAnchor.constraint(equalTo: categoryButtonContainerView.topAnchor).isActive = true
        kidsButton.rightAnchor.constraint(equalTo: categoryButtonContainerView.rightAnchor).isActive = true
        kidsButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        kidsButton.heightAnchor.constraint(equalTo: categoryButtonContainerView.heightAnchor, constant: -2).isActive = true
        
        kidsButtonSeperatorView.topAnchor.constraint(equalTo: kidsButton.bottomAnchor).isActive = true
        kidsButtonSeperatorView.rightAnchor.constraint(equalTo: categoryButtonContainerView.rightAnchor).isActive = true
        kidsButtonSeperatorView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        kidsButtonSeperatorView.heightAnchor.constraint(equalToConstant: 5).isActive = true
        
        gentsButton.topAnchor.constraint(equalTo: categoryButtonContainerView.topAnchor).isActive = true
        gentsButton.leftAnchor.constraint(equalTo: ladiesButton.rightAnchor).isActive = true
        gentsButton.rightAnchor.constraint(equalTo: kidsButton.leftAnchor).isActive = true
        gentsButton.widthAnchor.constraint(equalToConstant: 110).isActive = true
        gentsButton.heightAnchor.constraint(equalTo: categoryButtonContainerView.heightAnchor, constant: -2).isActive = true
        
        gentsButtonSeperatorView.topAnchor.constraint(equalTo: gentsButton.bottomAnchor).isActive = true
        gentsButtonSeperatorView.leftAnchor.constraint(equalTo: ladiesButtonSeperatorView.rightAnchor).isActive = true
        gentsButtonSeperatorView.rightAnchor.constraint(equalTo: kidsButtonSeperatorView.leftAnchor).isActive = true
        gentsButtonSeperatorView.widthAnchor.constraint(equalToConstant: 110).isActive = true
        gentsButtonSeperatorView.heightAnchor.constraint(equalToConstant: 5).isActive = true
        
        doneViewButton.bottomAnchor.constraint(equalTo: collectView.bottomAnchor).isActive = true
        doneViewButton.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        doneViewButton.widthAnchor.constraint(equalToConstant: 55).isActive = true
        doneViewButton.heightAnchor.constraint(equalToConstant: 55).isActive = true
        
        
        collectionView.topAnchor.constraint(equalTo: categoryButtonContainerView.bottomAnchor, constant: 10).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: doneViewButton.topAnchor, constant: -10).isActive = true
        
        
        
        handleGettingAllShopService()
        
    }
    
    @objc func setupViewObjectContraintsStaff(){
        self.doneViewButton.isHidden = true
        
        
        collectView.topAnchor.constraint(equalTo: view.topAnchor, constant: 70).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -70).isActive = true
        
        collectView.addSubview(collectionView)
        collectView.addSubview(closeViewButton)
        collectView.addSubview(doneViewButton)
        
        closeViewButton.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        closeViewButton.rightAnchor.constraint(equalTo: collectView.rightAnchor).isActive = true
        closeViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        closeViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        doneViewButton.bottomAnchor.constraint(equalTo: collectView.bottomAnchor).isActive = true
        doneViewButton.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        doneViewButton.widthAnchor.constraint(equalToConstant: 55).isActive = true
        doneViewButton.heightAnchor.constraint(equalToConstant: 55).isActive = true
        
        collectionView.topAnchor.constraint(equalTo: closeViewButton.bottomAnchor, constant: 10).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: doneViewButton.topAnchor, constant: -10).isActive = true
        
        handleGettingShopStaffs()
        
    }
    
    @objc func setupViewObjectContraintsAvailableTime(){
        self.doneViewButton.isHidden = true
        
        collectView.topAnchor.constraint(equalTo: view.topAnchor, constant: 70).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -70).isActive = true
        
        collectView.addSubview(closeViewButton)
        collectView.addSubview(doneViewButton)
        collectView.addSubview(collectionView)
        
        closeViewButton.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        closeViewButton.rightAnchor.constraint(equalTo: collectView.rightAnchor).isActive = true
        closeViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        closeViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        doneViewButton.bottomAnchor.constraint(equalTo: collectView.bottomAnchor).isActive = true
        doneViewButton.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        doneViewButton.widthAnchor.constraint(equalToConstant: 55).isActive = true
        doneViewButton.heightAnchor.constraint(equalToConstant: 55).isActive = true
        
        
        collectionView.topAnchor.constraint(equalTo: closeViewButton.bottomAnchor, constant: 10).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: doneViewButton.topAnchor, constant: -10).isActive = true
        
        handleGetAvailableTime()
    }
    

}

class customServiceCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    let seperatorPusherView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    let seperatorPusherViewNumberOfCustomer: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    let barberShopServiceLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 30
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    lazy var barberShopServiceNamePlaceHolderLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8servicestyle")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let barberShopServiceNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 13)
        fnhp.textColor = UIColor.black
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        fnhp.backgroundColor = UIColor.white
        return fnhp
    }()
    
    lazy var barberShopServiceDescriptionPlaceHolderLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8servicedescription")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let barberShopServiceDescriptionPlaceHolder: UITextView = {
        let fnhp = UITextView()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 13)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        fnhp.backgroundColor = UIColor.white
        fnhp.isEditable = false
        fnhp.isScrollEnabled = false
        fnhp.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return fnhp
    }()
    
    lazy var serviceTimeTakenPlaceHolderLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8timeSpanLogo")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let serviceTimeTakenPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 13)
        fnhp.textColor = UIColor.black
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        fnhp.backgroundColor = UIColor.white
        return fnhp
    }()
    
    lazy var servicePricePlaceHolderLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8cashLogo")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let servicePricePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 11)
        fnhp.textColor = UIColor.black
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        fnhp.backgroundColor = UIColor.white
        return fnhp
    }()
    
    let thumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.clear
        tniv.contentMode = .scaleAspectFit
        return tniv
    }()
    
    let upButton: UIButton = {
        let butt = UIButton()
        butt.setImage(UIImage(named: "icons8slideup"), for: .normal)
        return butt
    }()
    
    lazy var numberOfCustomerPlaceHolderLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8numberofpeople")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let numberOfCustomerPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 13)
        fnhp.textColor = UIColor(r: 23, g: 69, b: 90)
        fnhp.text = "1"
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        fnhp.backgroundColor = UIColor.white
        return fnhp
    }()
    
    let downButton: UIButton = {
        let butt = UIButton()
        butt.setImage(UIImage(named: "icons8downbutton"), for: .normal)
        return butt
    }()
    
    func setupViews(){
        
        addSubview(seperatorView)
        addSubview(barberShopServiceLogoImageView)
        addSubview(barberShopServiceNamePlaceHolder)
        addSubview(barberShopServiceNamePlaceHolderLogo)
        addSubview(upButton)
        addSubview(numberOfCustomerPlaceHolder)
        addSubview(downButton)
        addSubview(barberShopServiceDescriptionPlaceHolder)
        addSubview(barberShopServiceDescriptionPlaceHolderLogo)
        addSubview(serviceTimeTakenPlaceHolder)
        addSubview(serviceTimeTakenPlaceHolderLogo)
        addSubview(servicePricePlaceHolder)
        addSubview(servicePricePlaceHolderLogo)
        addSubview(seperatorPusherView)
        addSubview(seperatorPusherViewNumberOfCustomer)
        
        backgroundColor = UIColor.white
        layer.masksToBounds = true
        layer.cornerRadius = 5
        
        addContraintsWithFormat(format: "H:|-5-[v0(60)]-15-[v1(20)]-5-[v2]-5-[v3(40)]-5-|", views: barberShopServiceLogoImageView, barberShopServiceNamePlaceHolderLogo, barberShopServiceNamePlaceHolder, upButton)
        addContraintsWithFormat(format: "H:|-5-[v0(60)]-15-[v1(20)]-5-[v2]-5-[v3(40)]-5-|", views: barberShopServiceLogoImageView, serviceTimeTakenPlaceHolderLogo, serviceTimeTakenPlaceHolder, numberOfCustomerPlaceHolder)
        addContraintsWithFormat(format: "H:|-5-[v0(60)]-15-[v1(20)]-5-[v2]-5-[v3(40)]-5-|", views: barberShopServiceLogoImageView, servicePricePlaceHolderLogo, servicePricePlaceHolder, downButton)
        addContraintsWithFormat(format: "H:|-5-[v0(60)]-15-[v1(20)]-5-[v2]-5-[v3(40)]-5-|", views: barberShopServiceLogoImageView, barberShopServiceDescriptionPlaceHolderLogo, barberShopServiceDescriptionPlaceHolder, seperatorPusherViewNumberOfCustomer)
        
        addContraintsWithFormat(format: "V:|-10-[v0(60)]|", views: barberShopServiceLogoImageView)
        
        addContraintsWithFormat(format: "V:|-10-[v0(20)]-5-[v1(20)]-5-[v2(20)]-5-[v3(20)]-5-[v4]-5-[v4(5)]|", views: barberShopServiceNamePlaceHolderLogo, serviceTimeTakenPlaceHolderLogo, servicePricePlaceHolderLogo, barberShopServiceDescriptionPlaceHolderLogo, seperatorPusherView, seperatorView)
        addContraintsWithFormat(format: "V:|-10-[v0(20)]-5-[v1(20)]-5-[v2(20)]-5-[v3]-5-[v4(5)]|", views: barberShopServiceNamePlaceHolder, serviceTimeTakenPlaceHolder, servicePricePlaceHolder, barberShopServiceDescriptionPlaceHolder, seperatorView)
        
        addContraintsWithFormat(format: "V:|-10-[v0(40)]-5-[v1(40)]-5-[v2(40)]-5-[v3]-5-[v4(5)]|", views: upButton, numberOfCustomerPlaceHolder, downButton, seperatorPusherViewNumberOfCustomer, seperatorView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class customStaffCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let seperatorPusherView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    let seperatorPusherViewRating: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    let barberShopBarberLogoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor.gray
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 30
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    lazy var barberShopBarberNamePlaceHolderLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8staffLogo")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let barberShopBarberNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 13)
        fnhp.textColor = UIColor.black
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var barberAvailableFromPlaceHolderLogo: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8timeSpanLogo")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let barberAvailableFromPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 13)
        fnhp.textColor = UIColor.black
        fnhp.text = "Available"
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let starView = StarRatingView()
    let attribute = StarRatingAttribute(type: .rate,point: 10,spacing: 5, emptyColor: .gray, fillColor: UIColor(r: 244, g: 222, b: 172))
    
    func setupViews(){
        addSubview(seperatorPusherView)
        addSubview(barberShopBarberLogoImageView)
        addSubview(barberShopBarberNamePlaceHolder)
        addSubview(barberShopBarberNamePlaceHolderLogo)
        addSubview(barberAvailableFromPlaceHolder)
        addSubview(barberAvailableFromPlaceHolderLogo)
        addSubview(starView)
        addSubview(seperatorPusherViewRating)
        
        backgroundColor = UIColor.white
        layer.cornerRadius = 5
        layer.masksToBounds = true
        
        
        starView.configure(attribute, current: 0, max: 5)
        starView.translatesAutoresizingMaskIntoConstraints = false
        starView.isUserInteractionEnabled = false
        
        addContraintsWithFormat(format: "H:|-5-[v0(60)]-15-[v1(20)]-5-[v2]-5-|", views: barberShopBarberLogoImageView,barberShopBarberNamePlaceHolderLogo, barberShopBarberNamePlaceHolder)
        addContraintsWithFormat(format: "H:|-5-[v0(60)]-15-[v1(20)]-5-[v2]-5-|", views: barberShopBarberLogoImageView,barberAvailableFromPlaceHolderLogo, barberAvailableFromPlaceHolder)
        addContraintsWithFormat(format: "H:|-5-[v0(60)]-15-[v1(20)]-5-[v2]-5-|", views: barberShopBarberLogoImageView,seperatorPusherView, starView)
        addContraintsWithFormat(format: "H:|-5-[v0(60)]-15-[v1(20)]-5-[v2]-5-|", views: barberShopBarberLogoImageView,seperatorPusherView, seperatorPusherViewRating)
        addContraintsWithFormat(format: "V:|-5-[v0(60)]-5-|", views: barberShopBarberLogoImageView)
        
        addContraintsWithFormat(format: "V:|-10-[v0(20)]-5-[v1(20)]-5-[v2]-5-|", views: barberShopBarberNamePlaceHolderLogo, barberAvailableFromPlaceHolderLogo, seperatorPusherView)
        addContraintsWithFormat(format: "V:|-10-[v0(20)]-5-[v1(20)]-5-[v2(20)]-5-[v3]-5-|", views: barberShopBarberNamePlaceHolder, barberAvailableFromPlaceHolder, starView, seperatorPusherViewRating)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class customAppointmentAvailableTimeCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    
    
    let appointmentTimePlaceHolder: UIButton = {
        let fnhp = UIButton()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.setTitleColor(UIColor.black, for: .normal)
        fnhp.contentHorizontalAlignment = .left
        fnhp.titleLabel?.font = UIFont(name: "OpenSans-Bold", size: 15)
        return fnhp
    }()
    
    let BookButton: UIButton = {
        let butt = UIButton()
        butt.setImage(UIImage(named: "icons8today"), for: .normal)
        return butt
    }()
    
    func setupViews(){
        
        addSubview(appointmentTimePlaceHolder)
        addSubview(BookButton)
        
        backgroundColor = UIColor.white
        layer.cornerRadius = 5
        layer.masksToBounds = true
        
        addContraintsWithFormat(format: "H:|-10-[v0(30)]-5-[v1]-5-|", views: BookButton, appointmentTimePlaceHolder )
        addContraintsWithFormat(format: "V:|-5-[v0(30)]-5-|", views: BookButton)
        addContraintsWithFormat(format: "V:|-5-[v0(30)]-5-|", views: appointmentTimePlaceHolder)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension PopOverCollectionViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch self.collectionViewSelected {
        case 1:
            return self.serviceOne.count
        case 2:
            return self.barberlistone.count
        default:
            return self.appointmentAvailableTimeArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch self.collectionViewSelected {
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customServiceCollectionViewCell
            cell.barberShopServiceNamePlaceHolder.text = self.serviceOne[indexPath.row].serviceTitle
            cell.barberShopServiceDescriptionPlaceHolder.text = self.serviceOne[indexPath.row].shortDescription
            cell.servicePricePlaceHolder.text = self.serviceOne[indexPath.row].serviceCost + " " + self.shopCurrency!
            cell.serviceTimeTakenPlaceHolder.text = self.serviceOne[indexPath.row].serviceEstimatedTime + " min"
            cell.barberShopServiceLogoImageView.image = self.serviceOne[indexPath.row].serviceImage
            cell.upButton.addTarget(self, action: #selector(increaseCustomerPerService(_:)), for: .touchUpInside)
            cell.upButton.tag = indexPath.row
            cell.downButton.addTarget(self, action: #selector(decreaseCustomerPerService(_:)), for: .touchUpInside)
            cell.downButton.tag = indexPath.row
            if let value = self.serviceOne[safe: indexPath.row]?.numberOfCustomer {
                cell.numberOfCustomerPlaceHolder.text = String(value)
            }
            return cell
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId2", for: indexPath) as! customStaffCollectionViewCell
            cell.barberShopBarberLogoImageView.image = self.barberlistone[indexPath.row].profileImage
            cell.barberShopBarberNamePlaceHolder.text = self.barberlistone[indexPath.row].firstName
            cell.starView.current = CGFloat(integerLiteral: self.barberlistone[indexPath.row].rating)
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId3", for: indexPath) as! customAppointmentAvailableTimeCollectionViewCell
            switch self.appointmentAvailableTimeArray[indexPath.row].holidayDesignation {
            case "staff":
                let description = "Staff break: " + self.appointmentAvailableTimeArray[indexPath.row].dateObjectStart.toString(DateToStringStyles.time(.short))
                cell.appointmentTimePlaceHolder.setTitle(description, for: .normal)
                cell.appointmentTimePlaceHolder.addTarget(self, action: #selector(handleSelectedHolidayTime(_:)), for: .touchUpInside)
                cell.BookButton.addTarget(self, action: #selector(handleSelectedHolidayTime(_:)), for: .touchUpInside)
                cell.BookButton.tag = indexPath.row
                cell.appointmentTimePlaceHolder.tag = indexPath.row
                break
            case "shop":
                let description = "Shop break: " + self.appointmentAvailableTimeArray[indexPath.row].dateObjectStart.toString(DateToStringStyles.time(.short))
                cell.appointmentTimePlaceHolder.setTitle(description, for: .normal)
                cell.appointmentTimePlaceHolder.addTarget(self, action: #selector(handleSelectedHolidayTime(_:)), for: .touchUpInside)
                cell.BookButton.addTarget(self, action: #selector(handleSelectedHolidayTime(_:)), for: .touchUpInside)
                cell.BookButton.tag = indexPath.row
                cell.appointmentTimePlaceHolder.tag = indexPath.row
                break
            case "customer":
                cell.appointmentTimePlaceHolder.setTitle(self.appointmentAvailableTimeArray[indexPath.row].dateObjectStart.toString(DateToStringStyles.time(.medium)), for: .normal)
                cell.appointmentTimePlaceHolder.addTarget(self, action: #selector(handleSelectedAvailableTime(_:)), for: .touchUpInside)
                cell.BookButton.addTarget(self, action: #selector(handleSelectedAvailableTime(_:)), for: .touchUpInside)
                cell.BookButton.tag = indexPath.row
                cell.appointmentTimePlaceHolder.tag = indexPath.row
                break
            default:
                print("no designation")
                break
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch self.collectionViewSelected {
        case 1:
            if let cell = self.serviceOne[safe: indexPath.row] {
                let dataReel = cell.shortDescription
                let approximateWidthOfDescription = view.frame.width - 124 // - 5 - 60 // - 5 - 5 - 40 - 5
                let size = CGSize(width: approximateWidthOfDescription, height: 1000)
                let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)]
                let estimatedFrame = NSString(string: dataReel).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
                
                return CGSize(width: collectView.frame.width, height: estimatedFrame.height + 140)
                
            }
            return CGSize(width: collectView.frame.width, height: 110)
        case 2:
            return CGSize(width: collectView.frame.width, height: 110)
        default:
            return CGSize(width: collectView.frame.width, height: 40)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch self.collectionViewSelected {
        case 1:
            print("cell tapped 1")
        case 2:
            let selSerID =  self.barberlistone[indexPath.row].staffID
            let firstName =  self.barberlistone[indexPath.row].firstName
            
            self.selectedBarberOne = selSerID
            self.createAppointmentViewController.selectedStaff.removeAll()
            self.createAppointmentViewController.selectedStaff.append(self.barberlistone[indexPath.row])
            self.createAppointmentViewController.selectStaffButton.setTitle("Staff: " + firstName, for: .normal)
            print(selSerID, "staffId")
            self.createAppointmentViewController.handleSwitchingStaff()
            self.exitViewAfterStaffChoice()
            
        default:
            print("cell tapped 2")
        }
    }
    
    
}
