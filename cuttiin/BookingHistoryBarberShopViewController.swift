//
//  BookingHistoryBarberShopViewController.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 23/12/2017.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate
import Alamofire

class BookingHistoryBarberShopViewController: UIViewController, UITextViewDelegate {
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    
    var customerAppointments = [AppointmentData]()
    var customerAppointmentsSecond = [AppointmentData]()
    
    var customerAppointmentsReset = [AppointmentData]()
    
    var alwaynewAppointmentCustomer = [AppointmentData]()
    var alwaynewAppointmentCustomerSecond = [AppointmentData]()
    
    var serviceAmountCustomerArray = [AppointmentServiceAmountData]()
    var selectedShopID: String?
    var fromDateObject: DateInRegion?
    var toDateObject: DateInRegion?
    var staffCustomerUniqueID: String?
    var staffFullNameString: String?
    var shopCurrencyCode: String?
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    lazy var bookingHistoryShopPicker: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8shoppicker")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(bookingHistoryShopSelectorView)))
        return imageView
    }()
    
    lazy var bookingHistoryCloseViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(exitThisView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let bookingHistoryViewTitlePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = NSLocalizedString("bookingHistoryViewControllerTitle", comment: "Appointments")
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 20)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    let dateFilterTitleNameLabel: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("bookingHistoryViewControllerDateStaffFilter", comment: "Date and Staff filter")
        ht.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        ht.textColor = UIColor.black
        ht.textAlignment = .center
        return ht
    }()
    
    let dateSelectButtonsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.isUserInteractionEnabled = true
        return tcview
    }()
    
    lazy var fromDateButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitle(NSLocalizedString("bookingHistoryViewControllerDateFilterFrom", comment: "From"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 14)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.tag = 1
        st.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        st.addTarget(self, action: #selector(handleSelectingFromDate(_:)), for: .touchUpInside)
        st.isEnabled = true
        return st
    }()
    
    lazy var toDateButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitle(NSLocalizedString("bookingHistoryViewControllerDateFilterTo", comment: "To"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 14)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.tag = 2
        st.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        st.addTarget(self, action: #selector(handleSelectToDate(_:)), for: .touchUpInside)
        st.isEnabled = true
        return st
    }()
    
    let dateFilterButtonsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.isUserInteractionEnabled = true
        return tcview
    }()
    
    lazy var resetDateButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitle(NSLocalizedString("bookingHistoryViewControllerAllStaffButton", comment: "All staff"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 14)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        st.addTarget(self, action: #selector(handleResetAppointmentByDate(_:)), for: .touchUpInside)
        st.isEnabled = true
        return st
    }()
    
    let totalEarningPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
        fnhp.textColor = UIColor.black
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.text = NSLocalizedString("bookingHistoryViewControllerTotalEarningLabel", comment: "Total earnings")
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var resetAllDataButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor.clear
        st.setTitle(NSLocalizedString("bookingHistoryViewControllerResetButton", comment: "Reset"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 14)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        st.addTarget(self, action: #selector(resetToOriginalState), for: .touchUpInside)
        st.isEnabled = true
        return st
    }()
    
    let curvedCollectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        cview.layer.masksToBounds = true
        cview.layer.cornerRadius = 5
        return cview
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.alwaysBounceVertical = true
        cv.register(customUpcomingAndCompletedCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()
    
    let refresherController: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        let title = NSLocalizedString("PullToRefreshBookingView", comment: "Pull to refresh")
        refreshControl.attributedTitle = NSAttributedString(string: title)
        refreshControl.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        refreshControl.addTarget(self, action: #selector(refreshOptions(sender:)), for: .valueChanged)
        return refreshControl
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        view.addSubview(dateFilterTitleNameLabel)
        view.addSubview(dateSelectButtonsContainerView)
        view.addSubview(dateFilterButtonsContainerView)
        view.addSubview(totalEarningPlaceHolder)
        view.addSubview(resetAllDataButton)
        view.addSubview(curvedCollectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjectContriants()
        let frmDt = DateInRegion() - 1.days
        self.fromDateObject = frmDt
        self.fromDateButton.setTitle(NSLocalizedString("bookingHistoryViewControllerDateFilterFrom", comment: "From") + ": " + frmDt.toString(DateToStringStyles.date(.medium)), for: .normal)
        
        let toDt = DateInRegion()
        self.toDateObject = toDt
        self.toDateButton.setTitle(NSLocalizedString("bookingHistoryViewControllerDateFilterTo", comment: "To") + ": " + toDt.toString(DateToStringStyles.date(.medium)), for: .normal)
            
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .didReceiveDataAppointmentHistory, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveDataStaff(_:)), name: .didReceiveDataAppointmentHistoryStaff, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveDataDate(_:)), name: .didReceiveDataAppointmentHistoryDate, object: nil)
    }
    
    @objc private func onDidReceiveData(_ notification:Notification) {
        if let dataBack = notification.userInfo as? [String: AnyObject], let shopId = dataBack["shopUniqueId"] as? String, let curr = dataBack["shopCurrency"] as? String {
            self.selectedShopID = shopId
            self.shopCurrencyCode = curr
            DispatchQueue.main.async {
                self.customerAppointments.removeAll()
                self.customerAppointmentsReset.removeAll()
                self.customerAppointmentsSecond.removeAll()
                self.alwaynewAppointmentCustomer.removeAll()
                self.alwaynewAppointmentCustomerSecond.removeAll()
                self.totalEarningPlaceHolder.text = NSLocalizedString("bookingHistoryViewControllerTotalEarningLabel", comment: "Total earnings")
                self.collectionView.reloadData()
                self.getCustomerAppointment()
            }
            
        }
    }
    
    @objc private func onDidReceiveDataStaff(_ notification:Notification) {
        if let dataBack = notification.userInfo as? [String: AnyObject], let staffId = dataBack["staffCustomerUniqueId"] as? String, let staffName = dataBack["fullName"] as? String {
            self.staffFullNameString = staffName
            self.staffCustomerUniqueID = staffId
            self.resetDateButton.setTitle(staffName, for: .normal)
            self.sortAppointmentsByStaff(satffCustomerId: staffId)
        }
    }
    
    @objc private func onDidReceiveDataDate(_ notification:Notification) {
        if let dataBack = notification.userInfo as? [String: Any], let dateGotten = dataBack["dateSelected"] as? Date, let tagValue = dataBack["buttonTag"] as? Int {
            self.dateChanged(dateSelected: dateGotten, buttonTag: tagValue)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    
    @objc private func bookingHistoryShopSelectorView(){
        
        let customerShops = CustomerShopsViewController()
        customerShops.bookingHistoryBarberShopViewController = self
        customerShops.viewControllerWhoInitiatedAction = "appointmentHistory"
        customerShops.modalPresentationStyle = .overCurrentContext
        present(customerShops, animated: true, completion: nil)
        
    }
    
    @objc private func refreshOptions(sender: UIRefreshControl) {
        self.resetDateButton.setTitle(NSLocalizedString("bookingHistoryViewControllerAllStaffButton", comment: "All staff"), for: .normal)
        getCustomerAppointment()
        sender.endRefreshing()
    }
    
    @objc func getCustomerAppointment(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let shopUniqueId = self.selectedShopID {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    //dismiss(animated: false, completion: nil)
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    self.resetDateButton.setTitle(NSLocalizedString("bookingHistoryViewControllerAllStaffButton", comment: "All staff"), for: .normal)
                    self.staffCustomerUniqueID = nil
                    
                    
                    DispatchQueue.global(qos: .background).async {
                        AF.request(self.BACKEND_URL + "getAllAppointmentsByShop/" + shopUniqueId, method: .post, headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    do {
                                        let customerShopData = try JSONDecoder().decode(AppointmentStaffCustomer.self, from: jsonData)
                                        
                                        let staffCustomerList = customerShopData.staffcustomer
                                        let appointmentCustomerList = customerShopData.customer
                                        let appointmentList = customerShopData.appointment
                                        self.alwaynewAppointmentCustomer.removeAll()
                                        self.alwaynewAppointmentCustomerSecond.removeAll()
                                        
                                        if (appointmentList.isEmpty){
                                            DispatchQueue.main.async {
                                                self.customerAppointments.removeAll()
                                                self.customerAppointmentsReset.removeAll()
                                                self.customerAppointmentsSecond.removeAll()
                                                self.collectionView.reloadData()
                                            }
                                        }
                                        
                                        for appoint in appointmentList {
                                            let appointmentRegion = DateByUserDeviceInitializer.getRegion(TZoneName: appoint.appointmentDateTimezone, calenName: appoint.appointmentDateCalendar, LocName: appoint.appointmentDateLocale)
                                            guard let startDate = appoint.appointmentDateStart.toDate("yyyy-MM-dd HH:mm:ss", region: appointmentRegion),
                                                let endDate = appoint.appointmentDateEnd.toDate("yyyy-MM-dd HH:mm:ss", region: appointmentRegion) else {
                                                    return
                                            }
                                            
                                            let startDateString = startDate.toString(DateToStringStyles.dateTime(.medium))
                                            
                                            let completDateString = startDateString
                                            
                                            
                                            guard let firstCustomer = appointmentCustomerList.first(where: {$0.id == appoint.appointmentCustomer}) else {
                                                return
                                            }
                                            guard let firstStaff = staffCustomerList.first(where: {$0.id == appoint.appointmentStaffCustomer}) else {
                                                return
                                            }
                                            
                                            if let blankImage = UIImage(named: "avatar_icon"){
                                                
                                                let staffName = firstStaff.firstName + " " + firstStaff.lastName
                                                
                                                
                                                for dataAmount in appoint.appointmentServiceWithAmount {
                                                    let singleServiceAmount = AppointmentServiceAmountData(id: dataAmount.id, appointmentService: dataAmount.appointmentService, appointmentServiceAmount: dataAmount.appointmentServiceAmount)
                                                    self.serviceAmountCustomerArray.append(singleServiceAmount)
                                                    if (appoint.appointmentServiceWithAmount.count == self.serviceAmountCustomerArray.count && self.serviceAmountCustomerArray.count > 0){
                                                        
                                                        let singleAppointment = AppointmentData(id: appoint.id, appointmentService: appoint.appointmentService, appointmentShortUniqueID: appoint.appointmentShortUniqueID, appointmentDateStart: appoint.appointmentDateStart, appointmentDateEnd: appoint.appointmentDateEnd, appointmentDateTimezone: appoint.appointmentDateTimezone, appointmentDateCalendar: appoint.appointmentDateCalendar, appointmentDateLocale: appoint.appointmentDateLocale, appointmentDateCreated: appoint.appointmentDateCreated, appointmentDescription: appoint.appointmentDescription, appointmentPrice: appoint.appointmentPrice, appointmentCurrency: appoint.appointmentCurrency, appointmentCustomer: appoint.appointmentCustomer, appointmentStaff: appoint.appointmentStaff, appointmentStaffCustomer: appoint.appointmentStaffCustomer, appointmentShop: appoint.appointmentShop, appointmentServiceQuantity: appoint.appointmentServiceQuantity, appointmentStartedBy: appoint.appointmentStartedBy, appointmentEndedBy: appoint.appointmentEndedBy, appointmentStartedAt: appoint.appointmentStartedAt, appointmentEndedAt: appoint.appointmentEndedAt, appointmentStartedEndedTimezone: appoint.appointmentStartedEndedTimezone, appointmentStartedEndedCalendar: appoint.appointmentStartedEndedCalendar, appointmentStartedEndedLocale: appoint.appointmentStartedEndedLocale, appointmentPaymentMethod: appoint.appointmentPaymentMethod, appointmentStatus: appoint.appointmentStatus, appointmentPaidDate: appoint.appointmentPaidDate, appointmentPaidTimezone: appoint.appointmentPaidTimezone, appointmentPaidCalendar: appoint.appointmentPaidCalendar, appointmentPaidLocale: appoint.appointmentPaidLocale, v: appoint.v, appointmentCancelledBy: appoint.appointmentCancelledBy, appontmentCustomerImage: blankImage, appointmentCustomerFirstName: firstCustomer.firstName, appointmentStaffFirstName: staffName, appointmentFormatedStartEndTime: completDateString, appointmentDateStartObj: startDate, appointmentDateEndObj: endDate, appointmentServiceWithAmount: self.serviceAmountCustomerArray)
                                                        self.handleAddingandUpdatingAppointmentArray(newAppointment: singleAppointment, arraySize: appointmentList.count )
                                                        self.handleAddingUpdatingandSortingByDateAppointmentArray(newAppointment: singleAppointment, arraySize: appointmentList.count)
                                                        self.serviceAmountCustomerArray.removeAll()
                                                        
                                                    }
                                                }
                                                
                                            }
                                        }
                                        
                                        
                                    } catch _ {
                                        if let statusCode = response.response?.statusCode{
                                            print(statusCode)
                                            switch (statusCode){
                                            case 404:
                                                DispatchQueue.main.async {
                                                    self.customerAppointments.removeAll()
                                                    self.customerAppointmentsSecond.removeAll()
                                                    self.customerAppointmentsReset.removeAll()
                                                    self.collectionView.reloadData()
                                                }
                                            case 500:
                                                print("An error occurred")
                                            default:
                                                print("Sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
        }
    }
    
    private func handleAddingandUpdatingAppointmentArray(newAppointment: AppointmentData, arraySize: Int ){
        self.alwaynewAppointmentCustomer.append(newAppointment)
        
        if (self.customerAppointments.isEmpty){
            self.customerAppointments.append(newAppointment)
            self.customerAppointmentsReset.append(newAppointment)
            
            self.customerAppointments.sort { (first, second) -> Bool in
                first.appointmentDateStartObj.isAfterDate(second.appointmentDateStartObj, granularity: .minute)
            }
            //handle search
            self.customerAppointmentsReset.sort { (first, second) -> Bool in
                first.appointmentDateStartObj.isAfterDate(second.appointmentDateStartObj, granularity: .minute)
            }
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        } else {
            if let row = self.customerAppointments.index(where: {$0.id == newAppointment.id}) {
                self.customerAppointments[row] = newAppointment
                self.customerAppointmentsReset[row] = newAppointment
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            } else {
                self.customerAppointments.append(newAppointment)
                self.customerAppointmentsReset.append(newAppointment)
                self.customerAppointments.sort { (first, second) -> Bool in
                    first.appointmentDateStartObj.isAfterDate(second.appointmentDateStartObj, granularity: .minute)
                }
                //handle search
                self.customerAppointmentsReset.sort { (first, second) -> Bool in
                    first.appointmentDateStartObj.isAfterDate(second.appointmentDateStartObj, granularity: .minute)
                }
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
        }
        
        if (self.alwaynewAppointmentCustomer.count == arraySize){
            for data in customerAppointments {
                
                if (!self.alwaynewAppointmentCustomer.contains(where: {$0.id == data.id })){
                    if let firstIndex = self.customerAppointments.firstIndex(where: {$0.id == data.id}){
                        self.customerAppointments.remove(at: firstIndex)
                        self.customerAppointmentsReset.remove(at: firstIndex)
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                        }
                    }
                }
                
                
            }
        }
    }
    
    
    private func handleAddingUpdatingandSortingByDateAppointmentArray(newAppointment: AppointmentData, arraySize: Int ){
        
        if let fromDte = self.fromDateObject, let toDte = self.toDateObject, newAppointment.appointmentDateStartObj.isAfterDate(fromDte, orEqual: true, granularity: .day) && newAppointment.appointmentDateStartObj.isBeforeDate(toDte, orEqual: true, granularity: .day) {
            self.alwaynewAppointmentCustomerSecond.append(newAppointment)
            
            if (self.customerAppointmentsSecond.isEmpty){
                self.customerAppointmentsSecond.append(newAppointment)
                self.customerAppointmentsSecond.sort { (first, second) -> Bool in
                    first.appointmentDateStartObj.isAfterDate(second.appointmentDateStartObj, granularity: .minute)
                }
                
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            } else {
                if let row = self.customerAppointmentsSecond.index(where: {$0.id == newAppointment.id}) {
                    self.customerAppointmentsSecond[row] = newAppointment
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                } else {
                    self.customerAppointmentsSecond.append(newAppointment)
                    self.customerAppointmentsSecond.sort { (first, second) -> Bool in
                        first.appointmentDateStartObj.isAfterDate(second.appointmentDateStartObj, granularity: .minute)
                    }
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            }
            
            if (self.alwaynewAppointmentCustomerSecond.count == self.customerAppointmentsSecond.count){
                var x = 0
                for data in customerAppointmentsSecond {
                    x = x + 1
                    if (!self.alwaynewAppointmentCustomerSecond.contains(where: {$0.id == data.id })){
                        if let firstIndex = self.customerAppointmentsSecond.firstIndex(where: {$0.id == data.id}){
                            self.customerAppointmentsSecond.remove(at: firstIndex)
                            DispatchQueue.main.async {
                                self.collectionView.reloadData()
                            }
                        }
                    }
                    
                    if (customerAppointmentsSecond.count == x) {
                        self.calculateTotalSum(totalArray: self.customerAppointmentsSecond)
                    }
                }
            }
        }
    }
    
    @objc func dateChanged(dateSelected: Date, buttonTag: Int) {
        self.totalEarningPlaceHolder.text = "0.00"
        let timezone = DateByUserDeviceInitializer.tzone
        let region = DateByUserDeviceInitializer.getRegion(TZoneName: timezone, calenName: "gregorian", LocName: "en")
        let currentDate = dateSelected.convertTo(region: region)
        switch buttonTag {
        case 1:
            self.fromDateObject = currentDate
            self.fromDateButton.setTitle(NSLocalizedString("bookingHistoryViewControllerDateFilterFrom", comment: "From") + ": " + currentDate.toString(DateToStringStyles.date(.medium)), for: .normal)
            self.customerAppointmentsSecond.removeAll()
            self.self.alwaynewAppointmentCustomerSecond.removeAll()
            self.totalEarningPlaceHolder.text = "0.00"
            self.resetDateButton.setTitle(NSLocalizedString("bookingHistoryViewControllerAllStaffButton", comment: "All staff"), for: .normal)
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            //self.customerAppointmentsSecond = self.customerAppointments
            for single in self.customerAppointments {
                self.handleAddingUpdatingandSortingByDateAppointmentArray(newAppointment: single, arraySize: self.customerAppointments.count)
            }
            
            break
        default:
            self.toDateObject = currentDate
            self.toDateButton.setTitle(NSLocalizedString("bookingHistoryViewControllerDateFilterTo", comment: "To") + ": " + currentDate.toString(DateToStringStyles.date(.medium)), for: .normal)
            self.customerAppointmentsSecond.removeAll()
            self.self.alwaynewAppointmentCustomerSecond.removeAll()
            self.totalEarningPlaceHolder.text = "0.00"
            self.resetDateButton.setTitle(NSLocalizedString("bookingHistoryViewControllerAllStaffButton", comment: "All staff"), for: .normal)
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            for single in self.customerAppointments {
                self.handleAddingUpdatingandSortingByDateAppointmentArray(newAppointment: single, arraySize: self.customerAppointments.count)
            }
            break
        }
    }
    
    @objc private func sortAppointmentsByStaff(satffCustomerId: String){
        self.customerAppointmentsSecond.removeAll()
        self.alwaynewAppointmentCustomerSecond.removeAll()
        self.totalEarningPlaceHolder.text = "0.00"
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
        
        //self.customerAppointmentsSecond = self.customerAppointments
        for appointment in self.customerAppointments {
            
            if appointment.appointmentStaffCustomer == satffCustomerId {
                self.handleAddingUpdatingandSortingByDateAppointmentArray(newAppointment: appointment, arraySize: self.customerAppointmentsSecond.count)
            }
        }
    }
    
    @objc private func resetToOriginalState(){
        self.resetDateButton.setTitle(NSLocalizedString("bookingHistoryViewControllerAllStaffButton", comment: "All staff"), for: .normal)
        self.staffCustomerUniqueID = nil
        
        let frmDt = DateInRegion() - 1.days
        self.fromDateObject = frmDt
        self.fromDateButton.setTitle(NSLocalizedString("bookingHistoryViewControllerDateFilterFrom", comment: "From") + ": " + frmDt.toString(DateToStringStyles.date(.medium)), for: .normal)
        
        let toDt = DateInRegion()
        self.toDateObject = toDt
        self.toDateButton.setTitle(NSLocalizedString("bookingHistoryViewControllerDateFilterTo", comment: "To") + ": " + toDt.toString(DateToStringStyles.date(.medium)), for: .normal)
        
        self.customerAppointmentsSecond.removeAll()
        self.alwaynewAppointmentCustomerSecond.removeAll()
        self.totalEarningPlaceHolder.text = "0.00"
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
        
        self.getCustomerAppointment()
    }
    
    func setupViewObjectContriants(){
        
        var topDistance: CGFloat = 20
        var bottomDistance: CGFloat = -10
        if UIDevice.current.hasNotch {
            topDistance = 60
            bottomDistance = -24
        }
        
        upperInputsContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(bookingHistoryShopPicker)
        upperInputsContainerView.addSubview(bookingHistoryViewTitlePlaceHolder)
        upperInputsContainerView.addSubview(bookingHistoryCloseViewButton)
        
        bookingHistoryShopPicker.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        bookingHistoryShopPicker.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor).isActive = true
        bookingHistoryShopPicker.widthAnchor.constraint(equalToConstant: 45).isActive = true
        bookingHistoryShopPicker.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        bookingHistoryCloseViewButton.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        bookingHistoryCloseViewButton.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor).isActive = true
        bookingHistoryCloseViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        bookingHistoryCloseViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        bookingHistoryViewTitlePlaceHolder.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        bookingHistoryViewTitlePlaceHolder.leftAnchor.constraint(equalTo: bookingHistoryShopPicker.rightAnchor, constant: 10).isActive = true
        bookingHistoryViewTitlePlaceHolder.rightAnchor.constraint(equalTo: bookingHistoryCloseViewButton.leftAnchor).isActive = true
        bookingHistoryViewTitlePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        dateFilterTitleNameLabel.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 10).isActive = true
        dateFilterTitleNameLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        dateFilterTitleNameLabel.widthAnchor.constraint(equalTo: view.widthAnchor, constant: 10).isActive = true
        dateFilterTitleNameLabel.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        dateSelectButtonsContainerView.topAnchor.constraint(equalTo: dateFilterTitleNameLabel.bottomAnchor, constant: 10).isActive = true
        dateSelectButtonsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        dateSelectButtonsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        dateSelectButtonsContainerView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        dateSelectButtonsContainerView.addSubview(fromDateButton)
        dateSelectButtonsContainerView.addSubview(toDateButton)
        dateSelectButtonsContainerView.addSubview(resetDateButton)
        
        fromDateButton.topAnchor.constraint(equalTo: dateSelectButtonsContainerView.topAnchor).isActive = true
        fromDateButton.leftAnchor.constraint(equalTo: dateSelectButtonsContainerView.leftAnchor).isActive = true
        fromDateButton.rightAnchor.constraint(equalTo: dateSelectButtonsContainerView.centerXAnchor).isActive = true
        fromDateButton.heightAnchor.constraint(equalTo: dateSelectButtonsContainerView.heightAnchor).isActive = true
        
        toDateButton.topAnchor.constraint(equalTo: dateSelectButtonsContainerView.topAnchor).isActive = true
        toDateButton.rightAnchor.constraint(equalTo: dateSelectButtonsContainerView.rightAnchor).isActive = true
        toDateButton.leftAnchor.constraint(equalTo: dateSelectButtonsContainerView.centerXAnchor, constant: 20).isActive = true
        toDateButton.heightAnchor.constraint(equalTo: dateSelectButtonsContainerView.heightAnchor).isActive = true
        
        dateFilterButtonsContainerView.topAnchor.constraint(equalTo: dateSelectButtonsContainerView.bottomAnchor, constant: 10).isActive = true
        dateFilterButtonsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        dateFilterButtonsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -10).isActive = true
        dateFilterButtonsContainerView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        dateFilterButtonsContainerView.addSubview(resetDateButton)
        
        
        resetDateButton.topAnchor.constraint(equalTo: dateFilterButtonsContainerView.topAnchor).isActive = true
        resetDateButton.centerXAnchor.constraint(equalTo: dateFilterButtonsContainerView.centerXAnchor).isActive = true
        resetDateButton.widthAnchor.constraint(equalTo: dateFilterButtonsContainerView.widthAnchor, multiplier: 0.5).isActive = true
        resetDateButton.heightAnchor.constraint(equalTo: dateFilterButtonsContainerView.heightAnchor).isActive = true
        
        totalEarningPlaceHolder.topAnchor.constraint(equalTo: dateFilterButtonsContainerView.bottomAnchor, constant: 10).isActive = true
        totalEarningPlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        totalEarningPlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -24).isActive = true
        totalEarningPlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        resetAllDataButton.topAnchor.constraint(equalTo: totalEarningPlaceHolder.bottomAnchor, constant: 10).isActive = true
        resetAllDataButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        resetAllDataButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5).isActive = true
        resetAllDataButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        curvedCollectView.topAnchor.constraint(equalTo: resetAllDataButton.bottomAnchor, constant: 10).isActive = true
        curvedCollectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        curvedCollectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -24).isActive = true
        curvedCollectView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: bottomDistance).isActive = true
        
        curvedCollectView.addSubview(collectView)
        
        collectView.topAnchor.constraint(equalTo: curvedCollectView.topAnchor, constant: 5).isActive = true
        collectView.centerXAnchor.constraint(equalTo: curvedCollectView.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: curvedCollectView.widthAnchor, constant: -10).isActive = true
        collectView.bottomAnchor.constraint(equalTo: curvedCollectView.bottomAnchor, constant: -5).isActive = true
        
        collectView.addSubview(collectionView)
        collectView.bringSubviewToFront(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refresherController
        } else {
            collectionView.addSubview(refresherController)
            collectionView.sendSubviewToBack(refresherController)
        }
        
        self.getCustomerAppointment()
    }
    
    private func calculateTotalSum(totalArray: [AppointmentData]){
        self.totalEarningPlaceHolder.text = ""
        if let curren = self.shopCurrencyCode {
            var summedPrice = [Double]()
            for data in totalArray {
                if let sumData = Double(data.appointmentPrice){
                    summedPrice.append(sumData)
                }
                
                if totalArray.count == summedPrice.count {
                    let totalPrice = summedPrice.reduce(0, +)
                    self.totalEarningPlaceHolder.text = String(totalPrice) + " " + curren
                }
            }
        }
    }
    
    @objc private func exitThisView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func handleSelectingFromDate(_ sender: UIButton){
        sender.pulsate()
        let datePickerView = DatePickerViewController()
        datePickerView.bookingHistoryBarberShopViewController = self
        datePickerView.tagButton = sender.tag
        datePickerView.viewWhoInitiatedAction = "bookingViewHistory"
        present(datePickerView, animated: false, completion: nil)
    }
    
    @objc private func handleSelectToDate(_ sender: UIButton){
        sender.pulsate()
        let datePickerView = DatePickerViewController()
        datePickerView.bookingHistoryBarberShopViewController = self
        datePickerView.tagButton = sender.tag
        datePickerView.viewWhoInitiatedAction = "bookingViewHistory"
        present(datePickerView, animated: false, completion: nil)
    }
    
    @objc private func handleFilterAppointmentByDate(_ sender: UIButton){
        sender.pulsate()
    }
    
    @objc private func handleResetAppointmentByDate(_ sender: UIButton){
        sender.pulsate()
        if let id = self.selectedShopID, self.customerAppointmentsSecond.count > 0 {
            let customerShops = ShopStaffViewController()
            customerShops.bookingHistoryBarberShopViewController = self
            customerShops.viewControllerWhoInitiatedAction = "appointmentHistory"
            customerShops.shopUniqueID = id
            customerShops.appointmentArray = self.customerAppointmentsSecond
            customerShops.modalPresentationStyle = .overCurrentContext
            present(customerShops, animated: true, completion: nil)
        }
    }

}


extension BookingHistoryBarberShopViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.customerAppointmentsSecond.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! customUpcomingAndCompletedCollectionViewCell
        cell.startTimePlaceHolder.text = self.customerAppointmentsSecond[indexPath.row].appointmentFormatedStartEndTime
        cell.staffNamePlaceHolder.text = self.customerAppointmentsSecond[indexPath.row].appointmentStaffFirstName
        let curr = self.customerAppointmentsSecond[indexPath.row].appointmentCurrency
        cell.servicePricePlaceHolder.text = self.customerAppointmentsSecond[indexPath.row].appointmentPrice + " " + curr
        cell.appointmentStatusPlaceHolder.text = self.customerAppointmentsSecond[indexPath.row].appointmentStatus
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectView.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let appointmentDetail = OrderDetailViewController()
        appointmentDetail.appointmentID = self.customerAppointments[indexPath.row].id
        appointmentDetail.staffID = self.customerAppointments[indexPath.row].appointmentStaffCustomer
        appointmentDetail.customerID = self.customerAppointments[indexPath.row].appointmentCustomer
        appointmentDetail.shopID = self.customerAppointments[indexPath.row].appointmentShop
        appointmentDetail.serviceList = self.customerAppointments[indexPath.row].appointmentService
        appointmentDetail.modalPresentationStyle = .overCurrentContext
        if let shopOwn = self.selectedShopID{
            appointmentDetail.shopOwnerCustomerID = shopOwn
        }
        present(appointmentDetail, animated: true, completion: nil)
    }
}
