//
//  PaymentConfirmationViewController.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 16/04/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import UIKit
import MGStarRatingView
import SwiftDate
import FBSDKLoginKit
import Alamofire
import SwiftyJSON
import SwiftSpinner


class PaymentConfirmationViewController: UIViewController, StarRatingDelegate {
    
    var appointmentID: String?
    var shopTimeZone: String?
    var shopLocale: String?
    var shopCalender: String?
    var customerUniqueID: String?
    var shopID: String?
    
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    
    var totalFinalValue: String?
    var totalFinalValueWithoutCharges: String?
    var appointEndateForUnpaid: String?
    var cancelledAppointmentID = [String]()
    
    // rating data
    var customerCreatedAppointment: String?
    var ratingValue: String?
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    lazy var confirmPaymentViewIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "icons8paymentconfirm")
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var paymentViewTitlePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 17)
        fnhp.textColor = UIColor.black
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .left
        fnhp.text = NSLocalizedString("paymentConfirmationViewControllerTitle", comment: "Confirm payment")
        return fnhp
    }()
    
    lazy var upperSeperatorView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return tcview
    }()
    
    lazy var priceTitleLabelPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Light", size: 15)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .center
        fnhp.text = NSLocalizedString("paymentConfirmationViewControllerTitle", comment: "Total price")
        return fnhp
    }()
    
    lazy var paymentViewPricePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-Bold", size: 25)
        fnhp.textColor = UIColor.black
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        fnhp.layer.borderWidth = 1
        fnhp.layer.borderColor = UIColor.black.cgColor
        fnhp.layer.cornerRadius = 5
        return fnhp
    }()
    
    lazy var includedChargesPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue", size: 13)
        fnhp.textColor = UIColor(r: 11, g: 49, b: 68)
        fnhp.textAlignment = .center
        fnhp.text = "* Included Charges *"
        return fnhp
    }()
    
    let starView = StarRatingView()
    let attribute = StarRatingAttribute(type: .fill,point: 50,spacing: 5, emptyColor: .gray, fillColor: UIColor(r: 244, g: 222, b: 172))
    
    let buttonInputsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var confirmPaymentButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 244, g: 222, b: 172)
        st.setTitle(NSLocalizedString("paymentConfirmationViewControllerConfirmButton", comment: "Confirm payment"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.addTarget(self, action: #selector(confirmPayment(_:)), for: .touchUpInside)
        st.isEnabled = true
        return st
    }()
    
    lazy var didNotconfirmPaymentButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 235, g: 81, b: 96)
        st.setTitle(NSLocalizedString("paymentConfirmationViewControllerDidNotConfirmButton", comment: "Did not pay"), for: .normal)
        st.setTitleColor(UIColor.black, for: .normal)
        st.titleLabel?.font = UIFont(name: "OpenSans-SemiBold", size: 15)
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.addTarget(self, action: #selector(didNotConfirmPayment(_:)), for: .touchUpInside)
        st.isEnabled = true
        return st
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        starView.configure(attribute, current: 0, max: 5)
        starView.translatesAutoresizingMaskIntoConstraints = false
        starView.isUserInteractionEnabled = true
        starView.delegate = self
        setupObjectContraints()
    }
    
    
    
    func StarRatingValueChanged(view: StarRatingView, value: CGFloat) {
        self.ratingValue = value.description
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    @objc private func getAppointmentData(){
        DispatchQueue.global(qos: .background).async {
            
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let appointmentID = self.appointmentID {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            SwiftSpinner.hide()
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        AF.request(self.BACKEND_URL + "getSingleAppointment/" + appointmentID, method: .post, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    print("got data back")
                                    do {
                                        let appointmentData = try JSONDecoder().decode(Appointment.self, from: jsonData)
                                        DispatchQueue.main.async {
                                            
                                            self.totalFinalValueWithoutCharges = appointmentData.appointmentPrice
                                            self.shopID = appointmentData.appointmentShop
                                            if let intialPrice = Double(appointmentData.appointmentPrice) {
                                                let curr = appointmentData.appointmentCurrency
                                                let totalPrice = intialPrice
                                                let appointmentCustomer = appointmentData.appointmentCustomer
                                                self.appointEndateForUnpaid = appointmentData.appointmentDateEnd
                                                self.customerCreatedAppointment = appointmentData.appointmentCustomer
                                                self.getCancelledAppointment(price: totalPrice, currency: curr, customer: appointmentCustomer)
                                            }
                                            
                                        }
                                        
                                        
                                    } catch _ {
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                print("Appointments not found")
                                            case 500:
                                                print("An error occurred, please try again later")
                                            default:
                                                print("sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
    }
    
    @objc private func getCancelledAppointment(price: Double, currency: String, customer: String){
        DispatchQueue.global(qos: .background).async {
            
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let appID = self.appointmentID {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            SwiftSpinner.hide()
                            UtilityManager.alertView(view: self)
                            print("error in data request")
                        }
                    } else {
                        
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        let parameters: Parameters = [
                            "shopCurrency": currency,
                        ]
                        
                        AF.request(self.BACKEND_URL + "getAllCancelledAppointmentByCustomer/" + customer, method: .post, parameters: parameters, encoding: JSONEncoding(options: []), headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"])
                            .response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    do {
                                        let unpaidData = try JSONDecoder().decode(UnpaidAppointment.self, from: jsonData)
                                        let appointmentCancelData = unpaidData.appointmentCancelled
                                        var chargesArrayHolder = [Double]()
                                        for cancel in appointmentCancelData {
                                            if cancel.appointmentCancelledStatus == "unpaid", let priceDouble = Double(cancel.appointmentCancelledCharges) {
                                                print(priceDouble)
                                                chargesArrayHolder.append(priceDouble)
                                                self.cancelledAppointmentID.append(cancel.id)
                                            }
                                            
                                            if(chargesArrayHolder.count == appointmentCancelData.count){
                                                self.totalFinalValue = String(price )
                                                self.paymentViewPricePlaceHolder.text = String(chargesArrayHolder.reduce(0, +).roundTo(places: 2) + price ) + " " + currency
                                                let totalValNotification = String(chargesArrayHolder.reduce(0, +).roundTo(places: 2) + price ) + " " + currency
                                                self.handleSendNotificationAppointmentCompletion(customerid: customer, message: "please pay " + totalValNotification, designation: "appointmentPrice", appointmentID: appID)
                                            }
                                        }
                                        
                                    } catch _ {
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        if let statusCode = response.response?.statusCode {
                                            switch statusCode {
                                            case 404:
                                                self.totalFinalValue = String(price)
                                                self.paymentViewPricePlaceHolder.text = String(price) + " " + currency
                                                let totalValNotification2 = String(price) + " " + currency
                                                self.handleSendNotificationAppointmentCompletion(customerid: customer, message: "please pay " + totalValNotification2, designation: "appointmentPrice", appointmentID: appID)
                                            case 500:
                                                print("An error occurred, please try again later")
                                            default:
                                                print("sky high", statusCode)
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
    }
    
    @objc private func confirmPayment(_ sender: UIButton){
        sender.pulsate()
        if let price = totalFinalValue  {
            DispatchQueue.global(qos: .background).async {
                
                if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                    let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                    if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let appointid = self.appointmentID, let shopTim = self.shopTimeZone, let shopLoc = self.shopLocale, let shopCal = self.shopCalender  {
                        let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                        let dateNow = DateInRegion()
                        print(appointid)
                        if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                            DispatchQueue.main.async {
                                UtilityManager.alertView(view: self)
                            }
                        } else {
                            DispatchQueue.main.async {
                                SwiftSpinner.show("Loading...")
                            }
                            
                            let shopRegion = DateByUserDeviceInitializer.getRegion(TZoneName: shopTim, calenName: shopCal, LocName: shopLoc)
                            
                            let startDate = DateInRegion().convertTo(region: shopRegion).toFormat("yyyy-MM-dd HH:mm:ss")
                            
                            let headers: HTTPHeaders = [
                                "authorization": token
                            ]
                            print(self.cancelledAppointmentID)
                            let parameters: Parameters = [
                                "appointmentPaidDate": startDate,
                                "appointmentAmountPaid": price,
                                "appointmentCancelledIDArray": self.cancelledAppointmentID
                            ]
                            
                            AF.request(self.BACKEND_URL + "completeSingleAppointment/" + appointid, method: .post, parameters: parameters, encoding: JSONEncoding(options: []), headers: headers)
                                .validate(statusCode: 200..<501)
                                .validate(contentType: ["application/json"]).response { (response) in
                                    if let error = response.error {
                                        print("zones", error.localizedDescription)
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        return
                                    }
                                    
                                    if let jsonData = response.data {
                                        if let statusCode = response.response?.statusCode {
                                            
                                            do {
                                                
                                                let shopData = try JSONDecoder().decode(ServerMessage.self, from: jsonData)
                                                DispatchQueue.main.async {
                                                    SwiftSpinner.hide()
                                                    
                                                    switch statusCode {
                                                    case 200:
                                                        self.view.makeToast(NSLocalizedString("paymentConfirmationViewControllerErrorMessagePaymentConfirmSuccess", comment: "Payment confirmed successfully"), duration: 2.0, position: .bottom)
                                                        self.handleSavingCustomerRating()
                                                        if let id = self.shopID {
                                                            let someDict = ["shopUniqueId" : id ]
                                                            NotificationCenter.default.post(name: .didReceiveDataAppointmentViewRefresher, object: nil, userInfo: someDict)
                                                        }
                                                        if let cust = self.customerCreatedAppointment {
                                                            self.handleSendNotificationAppointmentCompletion(customerid: cust, message: "please rate your experience with the shop and staff", designation: "appointmentCompletion", appointmentID: appointid)
                                                        }
                                                        self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
                                                        
                                                    case 404:
                                                        self.view.makeToast(NSLocalizedString("paymentConfirmationViewControllerErrorMessageAppointmentNotFound", comment: "Appointment not found"), duration: 2.0, position: .bottom)
                                                    case 500:
                                                        self.view.makeToast(NSLocalizedString("paymentConfirmationViewControllerErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                                    default:
                                                        print("sky high")
                                                    }
                                                }
                                                
                                            } catch _ {
                                                DispatchQueue.main.async {
                                                    SwiftSpinner.hide()
                                                    if let statusCode = response.response?.statusCode {
                                                        switch statusCode {
                                                        case 404:
                                                            self.view.makeToast(NSLocalizedString("paymentConfirmationViewControllerErrorMessageAppointmentNotFound", comment: "Appointment not found"), duration: 2.0, position: .bottom)
                                                        case 500:
                                                            self.view.makeToast(NSLocalizedString("paymentConfirmationViewControllerErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                                        default:
                                                            print("sky high")
                                                        }
                                                    }
                                                }
                                            }
                                            
                                        }
                                    }
                            }
                        }
                    }
                }
                
                
            }
        }
    }
    
    @objc private func didNotConfirmPayment(_ sender: UIButton){
        sender.pulsate()
        if let price = totalFinalValueWithoutCharges {
            DispatchQueue.global(qos: .background).async {
                
                if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                    let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                    if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let appointid = self.appointmentID, let customerid = decodedCustomer.first?.customerId, let shopTim = self.shopTimeZone, let shopLoc = self.shopLocale, let shopCal = self.shopCalender {
                        let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                        let dateNow = DateInRegion()
                        
                        if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                            DispatchQueue.main.async {
                                UtilityManager.alertView(view: self)
                            }
                        } else {
                            
                            DispatchQueue.main.async {
                                SwiftSpinner.show("Loading...")
                            }
                            
                            let shopRegion = DateByUserDeviceInitializer.getRegion(TZoneName: shopTim, calenName: shopCal, LocName: shopLoc)
                            
                            let startDate = DateInRegion().convertTo(region: shopRegion).toFormat("yyyy-MM-dd HH:mm:ss")
                            
                            let headers: HTTPHeaders = [
                                "authorization": token
                            ]
                            
                            let parameters: Parameters = [
                                "appointmentCancelledBy": customerid,
                                "appointmentCancelledDate": startDate,
                                "initiator": "shop",
                                "appointmentCancelledReason":"Did not pay bill",
                                "appointmentCancelledCharges": price
                            ]
                            
                            AF.request(self.BACKEND_URL + "unpaidSingleAppointment/" + appointid, method: .post, parameters: parameters, encoding: JSONEncoding(options: []), headers: headers)
                                .validate(statusCode: 200..<501)
                                .validate(contentType: ["application/json"]).response { (response) in
                                    if let error = response.error {
                                        print("zones", error.localizedDescription)
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                        return
                                    }
                                    
                                    if let jsonData = response.data {
                                        if let statusCode = response.response?.statusCode {
                                            
                                            do {
                                                
                                                let shopData = try JSONDecoder().decode(ServerMessage.self, from: jsonData)
                                                DispatchQueue.main.async {
                                                    SwiftSpinner.hide()
                                                    
                                                    switch statusCode {
                                                    case 200:
                                                        self.handleSavingCustomerRating()
                                                        if let id = self.shopID {
                                                            let someDict = ["shopUniqueId" : id ]
                                                            NotificationCenter.default.post(name: .didReceiveDataAppointmentViewRefresher, object: nil, userInfo: someDict)
                                                        }
                                                        if let cust = self.customerCreatedAppointment {
                                                            self.handleSendNotificationAppointmentCompletion(customerid: cust, message: "please rate your experience with the shop and staff", designation: "appointmentCompletion", appointmentID: appointid)
                                                        }
                                                        self.presentingViewController?.presentingViewController?.dismiss(animated: false, completion: nil)
                                                    case 400:
                                                        self.view.makeToast(NSLocalizedString("paymentConfirmationViewControllerErrorMessageAppointmentNotFound", comment: "Appointment not found"), duration: 2.0, position: .bottom)
                                                    case 409:
                                                        self.view.makeToast(NSLocalizedString("paymentConfirmationViewControllerErrorMessageUniqueValidation", comment: "Unique validation error, please try again"), duration: 2.0, position: .bottom)
                                                    case 500:
                                                        self.view.makeToast(NSLocalizedString("paymentConfirmationViewControllerErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                                    default:
                                                        print("sky high")
                                                    }
                                                }
                                                
                                            } catch _ {
                                                DispatchQueue.main.async {
                                                    SwiftSpinner.hide()
                                                    if let statusCode = response.response?.statusCode {
                                                        switch statusCode {
                                                        case 400:
                                                            self.view.makeToast(NSLocalizedString("paymentConfirmationViewControllerErrorMessageAppointmentNotFound", comment: "Appointment not found"), duration: 2.0, position: .bottom)
                                                        case 409:
                                                            self.view.makeToast(NSLocalizedString("paymentConfirmationViewControllerErrorMessageUniqueValidation", comment: "Unique validation error, please try again"), duration: 2.0, position: .bottom)
                                                        case 500:
                                                            self.view.makeToast(NSLocalizedString("paymentConfirmationViewControllerErrorMessageErrorOccurred", comment: "An error occurred, please try again later"), duration: 2.0, position: .bottom)
                                                        default:
                                                            print("sky high")
                                                        }
                                                    }
                                                }
                                            }
                                            
                                        }
                                    }
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    @objc private func handleSavingCustomerRating(){
        if let ratingVlue = self.ratingValue, let customerWhoCreated = self.customerCreatedAppointment{
            DispatchQueue.global(qos: .background).async {
                
                if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                    let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                    if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token, let appointid = self.appointmentID, let customerid = decodedCustomer.first?.customerId {
                        let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                        let dateNow = DateInRegion()
                        
                        if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                            DispatchQueue.main.async {
                                UtilityManager.alertView(view: self)
                            }
                        } else {
                            
                            let startDate = DateInRegion().convertTo(region: region).toFormat("yyyy-MM-dd HH:mm:ss")
                            
                            let headers: HTTPHeaders = [
                                "authorization": token
                            ]
                            
                            let parameters: Parameters = [
                                "ratingValue": ratingVlue,
                                "ratingComment": "not available",
                                "customerWhoGaveRating": customerid,
                                "customerWhoRecievedRating": customerWhoCreated,
                                "customerWhoRecievedRatingRole": "customer",
                                "appointmentRated": appointid,
                                "ratingDateCreated": startDate,
                                "ratingDateTimezone": timeZone,
                                "ratingDateCalendar": "gregorian",
                                "ratingDateLocale": "en"
                                
                            ]
                            
                            AF.request(self.BACKEND_URL + "saveCustomerRating/" , method: .post, parameters: parameters, encoding: JSONEncoding(options: []), headers: headers)
                                .validate(statusCode: 200..<501)
                                .validate(contentType: ["application/json"]).response { (response) in
                                    if let error = response.error {
                                        print("zones", error.localizedDescription)
                                        return
                                    }
                                    
                                    if let jsonData = response.data {
                                        if let statusCode = response.response?.statusCode {
                                            
                                            do {
                                                
                                                let shopData = try JSONDecoder().decode(ServerMessage.self, from: jsonData)
                                                DispatchQueue.main.async {
                                                    
                                                    switch statusCode {
                                                    case 201:
                                                        print(shopData.message)
                                                    case 409:
                                                        print(shopData.message)
                                                    case 500:
                                                        print(shopData.message)
                                                    default:
                                                        print("sky high")
                                                    }
                                                }
                                                
                                            } catch _ {
                                                DispatchQueue.main.async {
                                                    if let statusCode = response.response?.statusCode {
                                                        switch statusCode {
                                                        case 409:
                                                            print("device unique validation failed")
                                                        case 500:
                                                            print("An error occurred")
                                                        default:
                                                            print("sky high")
                                                        }
                                                    }
                                                }
                                            }
                                            
                                        }
                                    }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @objc private func handleSendNotificationAppointmentCompletion(customerid: String, message: String, designation: String, appointmentID: String){
        
        DispatchQueue.global(qos: .background).async {
            
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let token = decodedCustomer.first?.token {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        
                        let parameters: Parameters = [
                            "customerid": customerid,
                            "designation": designation,
                            "appointmentID": appointmentID,
                            "message": message
                        ]
                        
                        print("message data check before sending: ", message)
                        
                        AF.request(self.BACKEND_URL + "onetoonepushnotification/" + customerid, method: .post, parameters: parameters, encoding: JSONEncoding(options: []), headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    if let statusCode = response.response?.statusCode {
                                        
                                        do {
                                            
                                            let _ = try JSONDecoder().decode(ServerMessage.self, from: jsonData)
                                            switch statusCode {
                                            case 200:
                                                print("notification sent")
                                            case 404:
                                                print("notification not sent")
                                            case 500:
                                                print("An error occurred, please try again later")
                                            default:
                                                print("sky high")
                                            }
                                            
                                        } catch _ {
                                            switch statusCode {
                                            case 404:
                                                print("notification not sent")
                                            case 500:
                                                print("An error occurred, please try again later")
                                            default:
                                                print("sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
            
            
        }
        
    }
    
    private func setupObjectContraints(){
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        view.addSubview(priceTitleLabelPlaceHolder)
        view.addSubview(paymentViewPricePlaceHolder)
        view.addSubview(starView)
        // view.addSubview(inputsContainerView)
        view.addSubview(buttonInputsContainerView)
        
        var topDistance: CGFloat = 20
        if UIDevice.current.hasNotch {
            topDistance = 60
        }
        
        upperInputsContainerView.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        upperInputsContainerView.addSubview(confirmPaymentViewIcon)
        upperInputsContainerView.addSubview(paymentViewTitlePlaceHolder)
        
        confirmPaymentViewIcon.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        confirmPaymentViewIcon.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor).isActive = true
        confirmPaymentViewIcon.widthAnchor.constraint(equalToConstant: 45).isActive = true
        confirmPaymentViewIcon.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        paymentViewTitlePlaceHolder.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        paymentViewTitlePlaceHolder.leftAnchor.constraint(equalTo: confirmPaymentViewIcon.rightAnchor, constant: 10).isActive = true
        paymentViewTitlePlaceHolder.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor, constant: -12).isActive = true
        paymentViewTitlePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -30).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        priceTitleLabelPlaceHolder.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 10).isActive = true
        priceTitleLabelPlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        priceTitleLabelPlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        priceTitleLabelPlaceHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        paymentViewPricePlaceHolder.topAnchor.constraint(equalTo: priceTitleLabelPlaceHolder.bottomAnchor, constant: 10).isActive = true
        paymentViewPricePlaceHolder.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        paymentViewPricePlaceHolder.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        paymentViewPricePlaceHolder.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        starView.topAnchor.constraint(equalTo: paymentViewPricePlaceHolder.bottomAnchor, constant: 10).isActive = true
        starView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        starView.widthAnchor.constraint(equalToConstant: 275).isActive = true
        starView.heightAnchor.constraint(equalToConstant: 55).isActive = true
        
        /*inputsContainerView.topAnchor.constraint(equalTo: starView.bottomAnchor, constant: 10).isActive = true
        inputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        inputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        inputsContainerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        inputsContainerView.addSubview(ratingCommentPlaceHolder)
        inputsContainerView.addSubview(ratingCommentTextField)
        inputsContainerView.addSubview(ratingCommentSeperatorView)
        
        ratingCommentPlaceHolder.topAnchor.constraint(equalTo: inputsContainerView.topAnchor).isActive = true
        ratingCommentPlaceHolder.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        ratingCommentPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        ratingCommentPlaceHolder.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        ratingCommentTextField.topAnchor.constraint(equalTo: ratingCommentPlaceHolder.bottomAnchor).isActive = true
        ratingCommentTextField.centerXAnchor.constraint(equalTo: inputsContainerView.centerXAnchor).isActive = true
        ratingCommentTextField.heightAnchor.constraint(equalToConstant: 30).isActive = true
        ratingCommentTextField.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        
        ratingCommentSeperatorView.leftAnchor.constraint(equalTo: inputsContainerView.leftAnchor).isActive = true
        ratingCommentSeperatorView.topAnchor.constraint(equalTo: ratingCommentTextField.bottomAnchor).isActive = true
        ratingCommentSeperatorView.widthAnchor.constraint(equalTo: inputsContainerView.widthAnchor).isActive = true
        ratingCommentSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true*/
        
        buttonInputsContainerView.topAnchor.constraint(equalTo: starView.bottomAnchor, constant: 10).isActive = true
        buttonInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        buttonInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        buttonInputsContainerView.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        buttonInputsContainerView.addSubview(confirmPaymentButton)
        buttonInputsContainerView.addSubview(didNotconfirmPaymentButton)
        
        confirmPaymentButton.topAnchor.constraint(equalTo: buttonInputsContainerView.topAnchor).isActive = true
        confirmPaymentButton.leftAnchor.constraint(equalTo: buttonInputsContainerView.leftAnchor).isActive = true
        confirmPaymentButton.rightAnchor.constraint(equalTo: buttonInputsContainerView.centerXAnchor).isActive = true
        confirmPaymentButton.heightAnchor.constraint(equalTo: buttonInputsContainerView.heightAnchor).isActive = true
        
        didNotconfirmPaymentButton.topAnchor.constraint(equalTo: buttonInputsContainerView.topAnchor).isActive = true
        didNotconfirmPaymentButton.rightAnchor.constraint(equalTo: buttonInputsContainerView.rightAnchor).isActive = true
        didNotconfirmPaymentButton.leftAnchor.constraint(equalTo: buttonInputsContainerView.centerXAnchor, constant: 20).isActive = true
        didNotconfirmPaymentButton.heightAnchor.constraint(equalTo: buttonInputsContainerView.heightAnchor).isActive = true
        
        self.getAppointmentData()
    }
    
    /*@objc private func textFieldDidChange(){
        if self.ratingCommentTextField.text == "" {
            self.ratingCommentPlaceHolder.isHidden = false
        } else {
            self.ratingCommentPlaceHolder.isHidden = true
        }
    }
    
    @objc private func ratingCommentTextFieldDidChange(){
        self.ratingCommentPlaceHolder.isHidden = false
    }*/

}
