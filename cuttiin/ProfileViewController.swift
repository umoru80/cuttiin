//
//  ProfileViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 7/13/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import SwiftDate
import Alamofire
import SwiftSpinner

class ProfileViewController: UIViewController {
    var settingButtons = [Settings]()
    var calledOnceHolder = true
    let BACKEND_URL = "https://teckdkdev.online/api/customer/"
    var shopAvailable = "NO"
    var shopDataArray = [ShopData]()
    var workHourDataArray = [WorkhourData]()
    var barberShopsViewController = BarberShopsViewController()
    var uniqueStaffID: String?
    var uniqueStaffShopID: String?
    var uniqueStaffCustomerID: String?
    
    lazy var closeViewButton: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "closeIcon")
        imageView.setImageColor(color: UIColor.black)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = .scaleAspectFit
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(exitThisView)))
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    lazy var upperInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        return tcview
    }()
    
    lazy var selectImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .center
        imageView.image = UIImage(named: "profile_passive")
        imageView.setImageColor(color: UIColor(r: 11, g: 49, b: 68))
        imageView.layer.cornerRadius = 50
        imageView.layer.masksToBounds = true
        imageView.isUserInteractionEnabled = false
        return imageView
    }()
    
    let userNameAndTextInputsContainerView: UIView = {
        let tcview = UIView()
        tcview.translatesAutoresizingMaskIntoConstraints = false
        tcview.isUserInteractionEnabled = true
        return tcview
    }()
    
    let userNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "OpenSans-SemiBold", size: 20)
        fnhp.textColor = UIColor.black
        fnhp.textAlignment = .left
        return fnhp
    }()
    
    let uniqueIDPlaceHolder: UIButton = {
        let fnhp = UIButton()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 13)
        fnhp.setTitleColor(UIColor.black, for: .normal)
        fnhp.contentHorizontalAlignment = .left
        fnhp.isUserInteractionEnabled = true
        fnhp.isEnabled = true
        fnhp.addTarget(self, action: #selector(handleShareAction(_:)), for: .touchUpInside)
        return fnhp
    }()
    
    let upperSeperatorView: UIView = {
        let fnsv = UIView()
        fnsv.translatesAutoresizingMaskIntoConstraints = false
        fnsv.backgroundColor = UIColor(r: 232, g: 232, b: 232)
        return fnsv
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customProfileSettingsEditorCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = NSLocalizedString("navigationTitleTextCustomerProfileView", comment: "Profile")
        view.backgroundColor = UIColor.white
        view.addSubview(closeViewButton)
        view.addSubview(upperInputsContainerView)
        view.addSubview(upperSeperatorView)
        view.addSubview(collectView)
        collectionView.dataSource = self
        collectionView.delegate = self
        setupViewObjects()
        settingViewAlign()
        calledOnceHolder = true
        getCustomerProfile()
        getCustomerShops()
        checkIfCustomerAStaff()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .didReceiveDataCustomerProfileViewRefresher, object: nil)
    }
    
    @objc private func onDidReceiveData(_ notification:Notification) {
        if let dataBack = notification.userInfo as? [String: AnyObject], let _ = dataBack["shopUniqueId"] as? String {
            //self.getTodaysAppointment(firstShop: shopID)
            
            //shop clear
            self.shopAvailable = "NO"
            self.shopDataArray.removeAll()
            self.settingButtons[1].settingsTitle = NSLocalizedString("profileViewConrollerShopButton", comment: "Shop")
            
            //staff clear
            self.settingButtons[2].settingsTitle = NSLocalizedString("profileViewConrollerStaffButton", comment: "Staff")
            self.uniqueStaffID = nil
            self.uniqueStaffShopID = nil
            self.uniqueStaffCustomerID = nil
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
            self.getCustomerProfile()
            self.getCustomerShops()
            self.checkIfCustomerAStaff()
        }
    }
    
    @objc func exitThisView(){
        self.dismiss(animated: true) {
            //shop clear
            self.shopAvailable = "NO"
            self.shopDataArray.removeAll()
            self.settingButtons[1].settingsTitle = NSLocalizedString("profileViewConrollerShopButton", comment: "Shop")
            
            //staff clear
            self.settingButtons[2].settingsTitle = NSLocalizedString("profileViewConrollerStaffButton", comment: "Staff")
            self.uniqueStaffID = nil
            self.uniqueStaffShopID = nil
            self.uniqueStaffCustomerID = nil
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
            let someDict = ["shopUniqueId" : "refresh view please" ]
            NotificationCenter.default.post(name: .didReceiveDataMapViewRefresher, object: nil, userInfo: someDict)
        }
    }
    
    @objc private func handleShareAction(_ sender: UIButton) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if let myWebsite = URL(string: "https://itunes.apple.com/dk/app/stay-sharp/id1298493287?mt=8"), let textToShare = sender.titleLabel?.text  {//Enter link to your app here
            let objectsToShare = [textToShare, myWebsite, image ?? #imageLiteral(resourceName: "app-logo")] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = sender
            self.present(activityVC, animated: true, completion: nil)
        } else {
            print("failed optionals")
        }
    }
    
    @objc func getCustomerProfile(){
        DispatchQueue.main.async {
            SwiftSpinner.hide()
        }
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let customerID = decodedCustomer.first?.customerId, let token = decodedCustomer.first?.token {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    SwiftSpinner.show("Loading...")
                    
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    DispatchQueue.global(qos: .background).async {
                        AF.request(self.BACKEND_URL + customerID, method: .get, headers: headers)
                            .validate(statusCode: 200..<500)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                    do {
                                        
                                        let customerSingleData = try JSONDecoder().decode(CustomerData.self, from: jsonData)
                                        let profileImageFileName = customerSingleData.customer.profileImageName
                                        self.userNamePlaceHolder.text = customerSingleData.customer.firstName + " " + customerSingleData.customer.lastName
                                        self.uniqueIDPlaceHolder.setTitle(customerSingleData.customer.shortUniqueID, for: .normal)
                                        
                                        
                                        AF.request(self.BACKEND_URL + "images/customerImages/" + profileImageFileName, headers: headers).responseData { response in
                                            
                                            switch response.result {
                                            case .success(let data):
                                                let image = UIImage(data: data)
                                                self.selectImageView.image = image
                                                self.selectImageView.contentMode = .scaleAspectFit
                                                break
                                            case .failure(let error):
                                                print(error)
                                                self.view.makeToast(NSLocalizedString("profileViewConrollerErrorMesageImageNotFound", comment: "Image not found"), duration: 3.0, position: .bottom)
                                                break
                                            }
                                        }
                                        
                                    } catch _ {
                                        DispatchQueue.main.async {
                                            SwiftSpinner.hide()
                                        }
                                    }
                                } else {
                                    DispatchQueue.main.async {
                                        SwiftSpinner.hide()
                                    }
                                }
                        }
                    }
                    
                }
            }
        }
    }
    
    @objc func getCustomerShops(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let customerID = decodedCustomer.first?.customerId, let token = decodedCustomer.first?.token {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let dateNow = DateInRegion()
                
                if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                    DispatchQueue.main.async {
                        UtilityManager.alertView(view: self)
                    }
                } else {
                    self.shopAvailable = "NO"
                    self.shopDataArray.removeAll()
                    self.settingButtons[1].settingsTitle = NSLocalizedString("profileViewConrollerShopButton", comment: "Shop")
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                    let headers: HTTPHeaders = [
                        "authorization": token
                    ]
                    
                    DispatchQueue.global(qos: .background).async {
                        AF.request(self.BACKEND_URL + "shops/" + customerID, method: .post, headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    do {
                                        let customerShopData = try JSONDecoder().decode([Shop].self, from: jsonData)
                                        let numberOfShops = String(customerShopData.count)
                                        self.settingButtons[1].settingsTitle = NSLocalizedString("profileViewConrollerShopsButton", comment: "Shops") +  " (" + numberOfShops + ")"
                                        self.shopAvailable = "YES";
                                        DispatchQueue.main.async {
                                            self.collectionView.reloadData()
                                        }
                                        
                                        
                                        for singleShop in customerShopData {
                                            
                                            let shop = ShopData(id: singleShop.id, shopAddress: singleShop.shopAddress, shopAddressLatitude: singleShop.shopAddressLatitude, shopAddressLongitude: singleShop.shopAddressLongitude, shopCanViewBookings: singleShop.shopCanViewBookings, shopCountry: singleShop.shopCountry, shopCountryCode: singleShop.shopCountryCode, shopCountryCurrencyCode: singleShop.shopCountryCurrencyCode, shopCustomerGender: singleShop.shopCustomerGender, shopDateCreated: singleShop.shopDateCreated, shopDateCreatedCalendar: singleShop.shopDateCreatedCalendar, shopDateCreatedLocale: singleShop.shopDateCreatedLocale, shopDateCreatedTimezone: singleShop.shopDateCreatedTimezone, shopDescription: singleShop.shopDescription, shopEmail: singleShop.shopEmail, shopHiddenByAdmin: singleShop.shopHiddenByAdmin, shopHiddenByCustomer: singleShop.shopHiddenByCustomer, shopLogoImageName: singleShop.shopLogoImageName, shopLogoImageURL: singleShop.shopLogoImageURL, shopMobileNumber: singleShop.shopMobileNumber, shopName: singleShop.shopName, shopOwner: singleShop.shopOwner, shopRating: singleShop.shopRating, shortUniqueID: singleShop.shortUniqueID)
                                            
                                            self.shopDataArray.append(shop)
                                            
                                        }
                                    } catch _ {
                                        if let statusCode = response.response?.statusCode{
                                            switch (statusCode){
                                            case 404:
                                                print("Shop not found")
                                            case 500:
                                                print("An error occurred")
                                            default:
                                                print("Sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                    }
                }
            }
        }
    }
    
    @objc func checkIfCustomerAStaff(){
        DispatchQueue.global(qos: .background).async {
            if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
                let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
                if decodedCustomer.count > 0, let endDate = decodedCustomer.first?.expiresIn, let timeZone = decodedCustomer.first?.timezone, let customerID = decodedCustomer.first?.customerId, let token = decodedCustomer.first?.token {
                    let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                    let dateNow = DateInRegion()
                    
                    if let expiredIn = endDate.toDate("yyyy-MM-dd HH:mm:ss", region: region), expiredIn.isBeforeDate(dateNow, orEqual: true, granularity: .second) {
                        DispatchQueue.main.async {
                            UtilityManager.alertView(view: self)
                        }
                    } else {
                        self.settingButtons[2].settingsTitle = NSLocalizedString("profileViewConrollerStaffButton", comment: "Staff")
                        self.uniqueStaffID = nil
                        self.uniqueStaffShopID = nil
                        self.uniqueStaffCustomerID = nil
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                        }
                        let headers: HTTPHeaders = [
                            "authorization": token
                        ]
                        AF.request(self.BACKEND_URL + "getStaffByCustomer/" + customerID, method: .post, headers: headers)
                            .validate(statusCode: 200..<501)
                            .validate(contentType: ["application/json"]).response { (response) in
                                if let error = response.error {
                                    print("zones", error.localizedDescription)
                                    return
                                }
                                
                                if let jsonData = response.data {
                                    do {
                                        print("matrix")
                                        let customerSingleData = try JSONDecoder().decode(Staff.self, from: jsonData)
                                        if (customerSingleData.staffHiddenShop == "NO" && customerSingleData.staffHiddenByAdmin == "NO"){
                                            self.settingButtons[2].settingsTitle = NSLocalizedString("profileViewConrollerStaffButton", comment: "Staff") + " (1)"
                                            self.uniqueStaffID = customerSingleData.id
                                            self.uniqueStaffShopID = customerSingleData.shop
                                            self.uniqueStaffCustomerID = customerSingleData.customer
                                            DispatchQueue.main.async {
                                                self.collectionView.reloadData()
                                            }
                                        }
                                        
                                    } catch _ {
                                        
                                        if let statusCode = response.response?.statusCode{
                                            switch (statusCode) {
                                            case 404:
                                                print("Staff not found")
                                            case 500:
                                                print("an error ocurred")
                                            default:
                                                print("Sky high")
                                            }
                                        }
                                        
                                    }
                                }
                        }
                        
                    }
                }
            }
        }
        
    }
    
    func setupViewObjects(){
        var topDistance: CGFloat = 20
        if UIDevice.current.hasNotch {
            topDistance = 60
        }
        
        closeViewButton.topAnchor.constraint(equalTo: view.topAnchor, constant: topDistance).isActive = true
        closeViewButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -5).isActive = true
        closeViewButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        closeViewButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        upperInputsContainerView.topAnchor.constraint(equalTo: closeViewButton.bottomAnchor, constant: 5).isActive = true
        upperInputsContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperInputsContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        upperInputsContainerView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        upperInputsContainerView.addSubview(selectImageView)
        upperInputsContainerView.addSubview(userNamePlaceHolder)
        upperInputsContainerView.addSubview(uniqueIDPlaceHolder)
        
        selectImageView.topAnchor.constraint(equalTo: upperInputsContainerView.topAnchor).isActive = true
        selectImageView.leftAnchor.constraint(equalTo: upperInputsContainerView.leftAnchor).isActive = true
        selectImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        selectImageView.bottomAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor).isActive = true
        
        userNamePlaceHolder.topAnchor.constraint(equalTo: upperInputsContainerView.topAnchor, constant: 5).isActive = true
        userNamePlaceHolder.leftAnchor.constraint(equalTo: selectImageView.rightAnchor, constant: 10).isActive = true
        userNamePlaceHolder.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor, constant: -10).isActive = true
        userNamePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        uniqueIDPlaceHolder.topAnchor.constraint(equalTo: userNamePlaceHolder.bottomAnchor, constant: 15).isActive = true
        uniqueIDPlaceHolder.leftAnchor.constraint(equalTo: selectImageView.rightAnchor, constant: 10).isActive = true
        uniqueIDPlaceHolder.rightAnchor.constraint(equalTo: upperInputsContainerView.rightAnchor, constant: -10).isActive = true
        uniqueIDPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        
        upperSeperatorView.topAnchor.constraint(equalTo: upperInputsContainerView.bottomAnchor, constant: 5).isActive = true
        upperSeperatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        upperSeperatorView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -20).isActive = true
        upperSeperatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        collectView.topAnchor.constraint(equalTo: upperSeperatorView.bottomAnchor, constant: 10).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -24).isActive = true
        collectView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
    }
    
    func settingViewAlign(){
        let value = Settings()
        value.settingsTitle = NSLocalizedString("profileViewConrollerProfileButton", comment: "Profile")
        value.settingsIcon = UIImage(named: "icons8profileiconplaceholder")
        self.settingButtons.append(value)
        
        let valueShop = Settings()
        valueShop.settingsTitle = NSLocalizedString("profileViewConrollerShopButton", comment: "Shop")
        valueShop.settingsIcon = UIImage(named: "icon_shop")
        self.settingButtons.append(valueShop)
        
        //icons8staffLogoLarge
        let valueStaff = Settings()
        valueStaff.settingsTitle = NSLocalizedString("profileViewConrollerStaffButton", comment: "Staff")
        valueStaff.settingsIcon = UIImage(named: "icons8staffLogoLarge")
        self.settingButtons.append(valueStaff)
        
        let valueNext = Settings()
        valueNext.settingsTitle = NSLocalizedString("profileViewConrollerLogOutButton", comment: "Log Out")
        valueNext.settingsIcon = UIImage(named: "icons8iconexit")
        self.settingButtons.append(valueNext)
    }
    
    @objc func handleCollectionSelction(sender: UIButton){
        switch sender.tag {
        case 0:
            let profileedit = ProfileEditorViewController()
            profileedit.modalPresentationStyle = .overCurrentContext
            self.present(profileedit, animated: true, completion: nil)
            break
        case 1:
            if (shopAvailable == "NO") {
                
                let registerview = RegisterBarberViewController()
                registerview.modalPresentationStyle = .overCurrentContext
                present(registerview, animated: true, completion: nil)
            } else {
                
                if(shopDataArray.count > 0){
                    self.handleSwitchBackToShopSection()
                    let shopView = AppointmentsViewController()
                    shopView.shopDataArray = self.shopDataArray
                    shopView.modalPresentationStyle = .overCurrentContext
                    present(shopView, animated: true, completion: nil)
                }
            }
            break
        case 2:
            if let staffID = self.uniqueStaffID, let staffShop = self.uniqueStaffShopID, let staffCustomer = self.uniqueStaffCustomerID {
                self.handleSwitchBackToShopSection()
                let shopView = AppointmentsViewController()
                shopView.chosenShopID = staffShop
                shopView.staffID = staffID
                shopView.staffShopID = staffShop
                shopView.staffCustomerID = staffCustomer
                shopView.modalPresentationStyle = .overCurrentContext
                present(shopView, animated: true, completion: nil)
            }
            break
        case 3:
            self.handleUpdateUserAuthData()
            break
        default:
            print("Sky High")
            break
        }
    }
    
    @objc private func handleSwitchBackToShopSection(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let customerId = decodedCustomer.first?.customerId, let _ = decodedCustomer.first?.shopOwner, let timeZone = decodedCustomer.first?.timezone, let authType = decodedCustomer.first?.authType, let email = decodedCustomer.first?.email, let tokenData = decodedCustomer.first?.token, let expiredTimeString = decodedCustomer.first?.expiresIn  {
                
                let teams = [CustomerToken(expiresIn: expiredTimeString, customerId: customerId, token: tokenData, timezone: timeZone, shopOwner: "YES", authType: authType, email: email, autoShopSwitch: "YES")]
                
                let userDefaults = UserDefaults.standard
                let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: teams)
                userDefaults.set(encodedData, forKey: "token")
                userDefaults.synchronize()
            } else {
                print("could not get customer data")
            }
        } else {
            print("could not get userdefault data")
        }
    }
    
    //properly the event of a user logging out
    @objc private func handleUpdateUserAuthData(){
        if let decoded  = UserDefaults.standard.object(forKey: "token") as? Data,
            let decodedCustomer = NSKeyedUnarchiver.unarchiveObject(with: decoded) as? [CustomerToken] {
            if decodedCustomer.count > 0, let customerId = decodedCustomer.first?.customerId, let shopOwner = decodedCustomer.first?.shopOwner, let timeZone = decodedCustomer.first?.timezone, let authType = decodedCustomer.first?.authType, let email = decodedCustomer.first?.email, let tokenData = decodedCustomer.first?.token, let autoSwitch = decodedCustomer.first?.autoShopSwitch {
                let region = DateByUserDeviceInitializer.getRegion(TZoneName: timeZone, calenName: "gregorian", LocName: "en")
                let expiredTime = DateInRegion().convertTo(region: region) - 1.seconds
                let expiredTimeString = expiredTime.toFormat("yyyy-MM-dd HH:mm:ss")
                
                let teams = [CustomerToken(expiresIn: expiredTimeString, customerId: customerId, token: tokenData, timezone: timeZone, shopOwner: shopOwner, authType: authType, email: email, autoShopSwitch: autoSwitch)]
                
                let userDefaults = UserDefaults.standard
                let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: teams)
                userDefaults.set(encodedData, forKey: "token")
                userDefaults.synchronize()
                LoginManager().logOut()
                
                //shop clear
                self.shopAvailable = "NO"
                self.shopDataArray.removeAll()
                self.settingButtons[1].settingsTitle = "Shop"
                
                //staff clear
                self.settingButtons[2].settingsTitle = "Staff"
                self.uniqueStaffID = nil
                self.uniqueStaffShopID = nil
                self.uniqueStaffCustomerID = nil
                
                handleRemoveToken(token: tokenData, customerid: customerId)
                let welcomeviewcontroller = WelcomeViewController()
                welcomeviewcontroller.modalPresentationStyle = .overCurrentContext
                self.present(welcomeviewcontroller, animated: true, completion: nil)
            } else {
                print("could not get customer data")
            }
        } else {
            print("could not get userdefault data")
        }
    }
    
    @objc private func handleRemoveToken(token: String, customerid: String){
        guard let devicetoken = UserDefaults.standard.object(forKey: "pushyToken") as?
            String else {
                print("no token")
            return
        }
        DispatchQueue.global(qos: .background).async {
            let headers: HTTPHeaders = [
                "authorization": token
            ]
            
            let parameters: Parameters = [
                "customer": customerid,
                "deviceToken": devicetoken
            ]
            
            AF.request(self.BACKEND_URL + "removedevicetoken", method: .post, parameters: parameters, headers: headers)
                .validate(statusCode: 200..<501)
                .validate(contentType: ["application/json"])
                .response (completionHandler: { (response) in
                    
                    if let error = response.error {
                        print(error.localizedDescription)
                        return
                    }
                    
                    if let jsonData = response.data {
                        do {
                            if let statusCode = response.response?.statusCode {
                                print("statuscode: ", statusCode)
                                switch statusCode {
                                case 200:
                                    let serverResponse = try JSONDecoder().decode(ServerMessage.self, from: jsonData)
                                    let message = serverResponse.message
                                    print(message, "result")
                                case 404:
                                    print("An customer token not found")
                                case 500:
                                    print("An error occured")
                                default:
                                    print("sky high")
                                }
                            }
                            
                        } catch _ {
                            if let statusCode = response.response?.statusCode {
                                print("statuscode: ", statusCode)
                                switch statusCode {
                                case 404:
                                    print("An customer token not found")
                                case 500:
                                    print("An error occurred")
                                default:
                                    print("sky high")
                                }
                            }
                        }
                    }
                })
            
        }
    }
    
    func handlePostLogoutDismiss(){
        UserDefaults.standard.set("NO", forKey: "userHasVerifiedCredantialsAndCanProceed")
        navigationController?.popViewController(animated: true)
    }
}



class customProfileSettingsEditorCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    lazy var thumbnailImageView: UIButton = {
        let tniv = UIButton()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.contentMode = .scaleAspectFit
        tniv.backgroundColor = UIColor.clear
        tniv.isUserInteractionEnabled = true
        return tniv
    }()
    
    let settingButtonTitlePlaceHolder: UIButton = {
        let fnhp = UIButton()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.titleLabel?.font = UIFont(name: "OpenSans-Light", size: 18)
        fnhp.setTitleColor(UIColor.black, for: .normal)
        fnhp.contentHorizontalAlignment = .left
        fnhp.isUserInteractionEnabled = true
        return fnhp
    }()
    
    let seperatorView: UIView = {
        let sv = UIView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.backgroundColor = UIColor.clear
        return sv
    }()
    
    func setupViews(){
        addSubview(thumbnailImageView)
        addSubview(settingButtonTitlePlaceHolder)
        addSubview(seperatorView)
        
        addContraintsWithFormat(format: "H:|-16-[v0(165)][v1(50)]-16-|", views: settingButtonTitlePlaceHolder, thumbnailImageView)
        addContraintsWithFormat(format: "V:|-5-[v0]-5-[v1(1)]|", views: settingButtonTitlePlaceHolder,seperatorView)
        addContraintsWithFormat(format: "V:|-5-[v0]-5-[v1(1)]|", views: thumbnailImageView,seperatorView)
        addContraintsWithFormat(format: "H:|[v0]|", views: seperatorView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
