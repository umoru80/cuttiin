//
//  BookingDateViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 8/7/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate
import Alamofire



class BookingDateViewController: UIViewController {
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    var barberShopUUID: String?
    var bookingID: String?
    var barberID: String?
    var pictureFileName: String?
    var barberOne = BarberShop()
    var navBar: UINavigationBar = UINavigationBar()
    var presentDay: Int?
    var newDay: Int?
    var totalTimeForServicePushed: Int?
    var totalTimeForService: Int?
    var isDateClashing = false
    var counterInitValueForPostToServer = 0
    var firstLoad = false
    var bookingStartStopHold = [Bookings]()
    var availableTimesCalled = [AvailableTimes]()
    var availableTimesCalledSecond = [AvailableTimes]()
    var availableTimesCalledSecondCall = [AvailableTimes]()
    var inbetweenavailableTimes = [IntermediaryAvailableTimes]()
    var bookingTimeIntervalMainArray = [BookingsTimeInterval]()
    var bookingTimeIntervalMainArraySecondCall = [BookingsTimeInterval]()
    var bookingTimeIntervalMainArraySecond = [BookingsTimeInterval]()
    
    
    var barbershopTimezone: String?
    var barbershopCalendar: String?
    var barbershopLocale: String?
    
    var presentDayVeri: Int?
    var newDayVeri: Int?
    
    var todaysBooking = [Bookings]()
    var todaysBookingsForVerification = [Bookings]()
    var thisBooking = Bookings()
    var timeSelectorCurrentDayWorkHour = Workhours()
    
    
    
    let scrollView: UIScrollView = {
        let v = UIScrollView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.showsVerticalScrollIndicator = false
        v.isScrollEnabled = true
        v.backgroundColor = UIColor.clear
        return v
    }()
    
    let barberShopHeaderDetailsContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var barberShopNamePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var barberShopAddressPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        fnhp.textColor = UIColor.white
        fnhp.numberOfLines = 0
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let instructionHeaderHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.text = NSLocalizedString("intructionHeaderTextHold", comment: "Suggested available hours, slide left for more")
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 10)
        ehp.textColor = UIColor(r: 118, g: 187, b: 220)
        ehp.textAlignment = .center
        return ehp
    }()
    
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        cview.layer.borderWidth = 2
        return cview
    }()
    
    let collectViewBackgroundImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "button_slider_updated")
        imageView.backgroundColor = UIColor.clear
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customBookingDateCollectionViewCell.self, forCellWithReuseIdentifier: "cellIdxx")
        return cv
    }()
    
    let statusOfAvailableBookingLable: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        ehp.textColor = UIColor(r: 118, g: 187, b: 220)
        ehp.textAlignment = .center
        return ehp
    }()
    
    let estimatedServiceTimeHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        ehp.textColor = UIColor(r: 118, g: 187, b: 220)
        ehp.textAlignment = .center
        return ehp
    }()
    
    let serviceTimeStartTimePicker: UIDatePicker = {
        let opentime = UIDatePicker()
        opentime.translatesAutoresizingMaskIntoConstraints = false
        opentime.datePickerMode = .dateAndTime
        
        //
        opentime.setValue(UIColor.white, forKey: "textColor")
        opentime.setValue(false, forKeyPath: "highlightsToday")
        //
        let currentDate = Date()
        opentime.date = currentDate
        opentime.minimumDate = currentDate
        return opentime
    }()
    
    let errorMessagePlaceHolder: UILabel = {
        let emhp = UILabel()
        emhp.translatesAutoresizingMaskIntoConstraints = false
        emhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        emhp.textColor = UIColor(r: 216, g: 127, b: 15)
        emhp.adjustsFontSizeToFitWidth = true
        emhp.minimumScaleFactor = 0.1
        emhp.numberOfLines = 0
        emhp.baselineAdjustment = .alignCenters
        emhp.textAlignment = .center
        return emhp
    }()
    
    let descriptionTitle: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("descriptionTitleBookingDateView", comment: "Cancellation Policy!")
        ht.font = UIFont(name: "HelveticaNeue-Bold", size: 20)
        ht.textColor = UIColor(r: 118, g: 187, b: 220)
        ht.textAlignment = .center
        return ht
    }()
    
    let descriptionBody: UILabel = {
        let ht = UILabel()
        ht.translatesAutoresizingMaskIntoConstraints = false
        ht.text = NSLocalizedString("descriptionBodyBookingDateView", comment: "Remember if you cancel the same day as your appointment is due, you will still be charged for the cancellation(50% of your total transaction)")
        ht.numberOfLines = 0
        ht.font = UIFont(name: "HelveticaNeue", size: 13)
        ht.textColor = UIColor(r: 118, g: 187, b: 220)
        ht.adjustsFontSizeToFitWidth = true
        ht.minimumScaleFactor = 0.1
        ht.baselineAdjustment = .alignCenters
        ht.textAlignment = .center
        return ht
    }()
    
    let chosenDatePlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.text = ""
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        fnhp.textColor = UIColor.white
        fnhp.adjustsFontSizeToFitWidth = true
        fnhp.minimumScaleFactor = 0.1
        fnhp.baselineAdjustment = .alignCenters
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    lazy var bookTheDateButton: UIButton = {
        let fbutton = UIButton()
        fbutton.translatesAutoresizingMaskIntoConstraints = false
        fbutton.backgroundColor = UIColor(r: 23, g: 69, b: 90)
        fbutton.setTitle(NSLocalizedString("bookButtonBookingDateView", comment: "BOOK"), for: .normal)
        fbutton.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
        fbutton.titleLabel?.font = UIFont(name: "BebasNeue", size: 30)
        fbutton.layer.cornerRadius = 5
        fbutton.layer.masksToBounds = true
        fbutton.layer.borderWidth = 2
        fbutton.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        fbutton.isEnabled = false
        return fbutton
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setNavBarToTheView() {
        self.navBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80)
        self.view.addSubview(navBar)
        let coverLayer = CALayer()
        coverLayer.frame = navBar.bounds;
        coverLayer.backgroundColor = UIColor(r: 23, g: 69, b: 90).cgColor
        navBar.layer.addSublayer(coverLayer)
        navBar.addSubview(barberShopHeaderDetailsContainerView)
        
        barberShopHeaderDetailsContainerView.topAnchor.constraint(equalTo: navBar.topAnchor, constant: 25).isActive = true
        barberShopHeaderDetailsContainerView.centerXAnchor.constraint(equalTo: navBar.centerXAnchor).isActive = true
        barberShopHeaderDetailsContainerView.widthAnchor.constraint(equalTo: navBar.widthAnchor, constant: -96).isActive = true
        barberShopHeaderDetailsContainerView.heightAnchor.constraint(equalTo: navBar.heightAnchor, constant: -35).isActive = true
        
        barberShopHeaderDetailsContainerView.addSubview(barberShopNamePlaceHolder)
        barberShopHeaderDetailsContainerView.addSubview(barberShopAddressPlaceHolder)
        
        barberShopNamePlaceHolder.topAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.topAnchor).isActive = true
        barberShopNamePlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopNamePlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberShopNamePlaceHolder.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        barberShopAddressPlaceHolder.topAnchor.constraint(equalTo: barberShopNamePlaceHolder.bottomAnchor, constant: 5).isActive = true
        barberShopAddressPlaceHolder.centerXAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.centerXAnchor).isActive = true
        barberShopAddressPlaceHolder.widthAnchor.constraint(equalTo: barberShopHeaderDetailsContainerView.widthAnchor).isActive = true
        barberShopAddressPlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
    }
    
    func setupViews(){
        
        scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 110).isActive = true
        scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -5).isActive = true
        
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 520) //450
        
        
        scrollView.addSubview(instructionHeaderHolder)
        scrollView.addSubview(collectView)
        scrollView.addSubview(statusOfAvailableBookingLable)
        scrollView.addSubview(estimatedServiceTimeHolder)
        scrollView.addSubview(serviceTimeStartTimePicker)
        scrollView.addSubview(errorMessagePlaceHolder)
        scrollView.addSubview(descriptionTitle)
        scrollView.addSubview(descriptionBody)
        scrollView.addSubview(bookTheDateButton)
        scrollView.addSubview(chosenDatePlaceHolder)
        
        
        instructionHeaderHolder.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 5).isActive = true
        instructionHeaderHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        instructionHeaderHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -48).isActive = true
        instructionHeaderHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        collectView.topAnchor.constraint(equalTo: instructionHeaderHolder.bottomAnchor, constant: 10).isActive = true
        collectView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1, constant: -72).isActive = true
        collectView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        collectView.addSubview(collectViewBackgroundImageView)
        
        collectViewBackgroundImageView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectViewBackgroundImageView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectViewBackgroundImageView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectViewBackgroundImageView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
        
        collectViewBackgroundImageView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectViewBackgroundImageView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectViewBackgroundImageView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectViewBackgroundImageView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectViewBackgroundImageView.heightAnchor).isActive = true
        
        
        statusOfAvailableBookingLable.topAnchor.constraint(equalTo: collectView.bottomAnchor, constant: 5).isActive = true
        statusOfAvailableBookingLable.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        statusOfAvailableBookingLable.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -48).isActive = true
        statusOfAvailableBookingLable.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        //
        estimatedServiceTimeHolder.topAnchor.constraint(equalTo: statusOfAvailableBookingLable.bottomAnchor, constant: 5).isActive = true
        estimatedServiceTimeHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        estimatedServiceTimeHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -48).isActive = true
        estimatedServiceTimeHolder.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        serviceTimeStartTimePicker.topAnchor.constraint(equalTo: estimatedServiceTimeHolder.bottomAnchor, constant: 5).isActive = true
        serviceTimeStartTimePicker.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        serviceTimeStartTimePicker.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -12).isActive = true
        serviceTimeStartTimePicker.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        errorMessagePlaceHolder.topAnchor.constraint(equalTo: serviceTimeStartTimePicker.bottomAnchor, constant: 5).isActive = true
        errorMessagePlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        errorMessagePlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -48).isActive = true
        errorMessagePlaceHolder.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        descriptionTitle.topAnchor.constraint(equalTo: errorMessagePlaceHolder.bottomAnchor, constant: 10).isActive = true
        descriptionTitle.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        descriptionTitle.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        descriptionTitle.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        descriptionBody.topAnchor.constraint(equalTo: descriptionTitle.bottomAnchor).isActive = true
        descriptionBody.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        descriptionBody.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -24).isActive = true
        descriptionBody.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        chosenDatePlaceHolder.topAnchor.constraint(equalTo: descriptionBody.bottomAnchor, constant: 15).isActive = true
        chosenDatePlaceHolder.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1, constant: -24).isActive = true
        chosenDatePlaceHolder.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        chosenDatePlaceHolder.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        bookTheDateButton.topAnchor.constraint(equalTo: chosenDatePlaceHolder.bottomAnchor, constant: 5).isActive = true
        bookTheDateButton.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1, constant: -24).isActive = true
        bookTheDateButton.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        bookTheDateButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
        bookTheDateButton.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -20)
        
    }
}


class customBookingDateCollectionViewCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let openStartTimeHolder: UILabel = {
        let ehp = UILabel()
        ehp.translatesAutoresizingMaskIntoConstraints = false
        ehp.font = UIFont(name: "HelveticaNeue-Medium", size: 13)
        ehp.textColor = UIColor.white
        ehp.textAlignment = .center
        return ehp
    }()
    
    func setupViews(){
        addSubview(openStartTimeHolder)
        backgroundColor = UIColor.clear
        addContraintsWithFormat(format: "H:|[v0]|", views: openStartTimeHolder)
        addContraintsWithFormat(format: "V:|-5-[v0]-5-|", views: openStartTimeHolder)
        
    }
    
    override func prepareForReuse() {
        self.openStartTimeHolder.text = ""
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
