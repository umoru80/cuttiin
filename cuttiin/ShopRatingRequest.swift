//
//  ShopRatingRequest.swift
//  cuttiin
//
//  Created by Qasim Ahmed on 01/06/2019.
//  Copyright © 2019 teckdk. All rights reserved.
//

import Foundation

struct ShopRatingRequest: Codable {
    let shopID, shopRating: String
}
