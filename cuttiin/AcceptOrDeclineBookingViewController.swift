//
//  AcceptOrDeclineBookingViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 8/24/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import SwiftDate
import SwiftyJSON

class AcceptOrDeclineBookingViewController: UIViewController {
    var buttonTag: Int?
    var baeberShoopUUID: String?
    var bookingID: String?
    var barberID: String?
    var paymentIDDSA: String?
    var paymentAmountString: String?
    var orderDetail = OrderDetailViewController()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    lazy var dataObjectsContanerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    let statusPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 25)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.text = NSLocalizedString("suceesPlaceHolderTextAcceptDeclineView", comment: "Success!")
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let statusDescriptionPlaceHolder: UILabel = {
        let fnhp = UILabel()
        fnhp.translatesAutoresizingMaskIntoConstraints = false
        fnhp.font = UIFont(name: "HelveticaNeue-Medium", size: 15)
        fnhp.textColor = UIColor(r: 118, g: 187, b: 220)
        fnhp.text = NSLocalizedString("statusDescriptionTextAcceptDeclineView", comment: "You can find your served clients in the calender section")
        fnhp.numberOfLines = 0
        fnhp.textAlignment = .center
        return fnhp
    }()
    
    let checkImageDescriptionThumbnailImageView: UIImageView = {
        let tniv = UIImageView()
        tniv.translatesAutoresizingMaskIntoConstraints = false
        tniv.backgroundColor = UIColor.clear
        tniv.image = UIImage(named: "checkmark_big")
        tniv.contentMode = .scaleAspectFit
        return tniv
    }()
    
    lazy var appointmentsButton: UIButton = {
        let st = UIButton()
        st.translatesAutoresizingMaskIntoConstraints = false
        st.backgroundColor = UIColor(r: 11, g: 49, b: 68)
        st.setTitle(NSLocalizedString("appointmentAcceptButtonAceptDeclineView", comment: "APPOINTMENTS"), for: .normal)
        st.setTitleColor(UIColor(r: 118, g: 187, b: 220), for: .normal)
        st.titleLabel?.font = UIFont(name: "BebasNeue", size: 30)
        st.tag = 1
        st.layer.cornerRadius = 5
        st.layer.masksToBounds = true
        st.layer.borderWidth = 2
        st.layer.borderColor = UIColor(r: 118, g: 187, b: 220).cgColor
        st.addTarget(self, action: #selector(handleShowAppointView), for: .touchUpInside)
        return st
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        view.addSubview(dataObjectsContanerView)
        //view.addSubview(statusPlaceHolder)
        //view.addSubview(statusDescriptionPlaceHolder)
        //view.addSubview(checkImageDescriptionThumbnailImageView)
        //view.addSubview(appointmentsButton)
        setViewContriants()
    }
    
    func setViewContriants(){
        dataObjectsContanerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 100).isActive = true
        dataObjectsContanerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        dataObjectsContanerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        dataObjectsContanerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10).isActive = true
        
        dataObjectsContanerView.addSubview(statusPlaceHolder)
        dataObjectsContanerView.addSubview(statusDescriptionPlaceHolder)
        dataObjectsContanerView.addSubview(checkImageDescriptionThumbnailImageView)
        dataObjectsContanerView.addSubview(appointmentsButton)
        
        
        statusPlaceHolder.topAnchor.constraint(equalTo: dataObjectsContanerView.topAnchor).isActive = true
        statusPlaceHolder.centerXAnchor.constraint(equalTo: dataObjectsContanerView.centerXAnchor).isActive = true
        statusPlaceHolder.widthAnchor.constraint(equalTo: dataObjectsContanerView.widthAnchor, constant: -24).isActive = true
        statusPlaceHolder.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        statusDescriptionPlaceHolder.topAnchor.constraint(equalTo: statusPlaceHolder.bottomAnchor, constant: 10).isActive = true
        statusDescriptionPlaceHolder.centerXAnchor.constraint(equalTo: dataObjectsContanerView.centerXAnchor).isActive = true
        statusDescriptionPlaceHolder.widthAnchor.constraint(equalTo: dataObjectsContanerView.widthAnchor, constant: -24).isActive = true
        statusDescriptionPlaceHolder.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        checkImageDescriptionThumbnailImageView.topAnchor.constraint(equalTo: statusDescriptionPlaceHolder.bottomAnchor, constant: 30).isActive = true
        checkImageDescriptionThumbnailImageView.centerXAnchor.constraint(equalTo: dataObjectsContanerView.centerXAnchor).isActive = true
        checkImageDescriptionThumbnailImageView.widthAnchor.constraint(equalTo: dataObjectsContanerView.widthAnchor, constant: -72).isActive = true
        checkImageDescriptionThumbnailImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        appointmentsButton.topAnchor.constraint(equalTo: checkImageDescriptionThumbnailImageView.bottomAnchor, constant: 15).isActive = true
        appointmentsButton.centerXAnchor.constraint(equalTo: dataObjectsContanerView.centerXAnchor).isActive = true
        appointmentsButton.widthAnchor.constraint(equalTo: dataObjectsContanerView.widthAnchor, constant: -72).isActive = true
        appointmentsButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    @objc func handleShowAppointView(){
        dismiss(animated: true, completion: nil)
        
    }

}
