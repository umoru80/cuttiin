//
//  ProfileSettingsEditorViewController.swift
//  cuttiin
//
//  Created by Umoru Joseph on 9/8/17.
//  Copyright © 2017 teckdk. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class ProfileSettingsEditorViewController: UIViewController {
    var settingButtons = [Settings]()
    let collectView: UIView = {
        let cview = UIView()
        cview.translatesAutoresizingMaskIntoConstraints = false
        cview.backgroundColor = UIColor.clear
        return cview
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.register(customProfileSettingsEditorCollectionViewCell.self, forCellWithReuseIdentifier: "cellId")
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = NSLocalizedString("navigationTitleTextCustomerProfileView", comment: "Profile")
        view.backgroundColor = UIColor.white
        view.addSubview(collectView)
        //collectionView.dataSource = self
        //collectionView.delegate = self
        setupViewConstraints()
        settingViewAlign()
    }
    
    func settingViewAlign(){
        
        
        let valueFirst = Settings()
        valueFirst.settingsTitle = NSLocalizedString("historyButtonProfileSettingsEditorView", comment: "History")
        valueFirst.settingsIcon = UIImage(named: "password")
        self.settingButtons.append(valueFirst)
        
        let value = Settings()
        value.settingsTitle = NSLocalizedString("changePasswordButtonProfileSettingsEditor", comment: "Change Password")
        value.settingsIcon = UIImage(named: "password")
        self.settingButtons.append(value)
        
        let valueNext = Settings()
        valueNext.settingsTitle = NSLocalizedString("logOutButtonProfileSettingsEditor", comment: "Log Out")
        valueNext.settingsIcon = UIImage(named: "logout")
        self.settingButtons.append(valueNext)
    }
    
    @objc func handleCollectionSelction(sender: UIButton){
        switch sender.tag {
        case 0:
            let bookListHis = BookingHistoryViewController()
            let shopListController = UINavigationController(rootViewController: bookListHis)
            present(shopListController, animated: true, completion: nil)
        case 1:
            let changepassword = ChangePasswordViewController()
            navigationController?.pushViewController(changepassword, animated: true)
        case 2:
            self.handleAll()
        default:
            print("Sky High")
        }
    }
    
    func handleAll(){
        LoginManager().logOut()
        self.handlePostLogoutDismiss()
    }
    
    func handlePostLogoutDismiss(){
        UserDefaults.standard.set("NO", forKey: "userHasVerifiedCredantialsAndCanProceed")
        navigationController?.popViewController(animated: true)
    }
    
    func setupViewConstraints(){
        
        collectView.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        collectView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -24).isActive = true
        collectView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        collectView.addSubview(collectionView)
        
        collectionView.topAnchor.constraint(equalTo: collectView.topAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: collectView.centerXAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: collectView.widthAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: collectView.heightAnchor).isActive = true
    }

}
